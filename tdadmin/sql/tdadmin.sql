/*
Navicat MySQL Data Transfer

Source Server         : mysql_local
Source Server Version : 50637
Source Host           : localhost:3306
Source Database       : tdadmin

Target Server Type    : MYSQL
Target Server Version : 50637
File Encoding         : 65001

Date: 2018-05-16 11:22:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmt_sms_checkcode
-- ----------------------------
DROP TABLE IF EXISTS `cmt_sms_checkcode`;
CREATE TABLE `cmt_sms_checkcode` (
  `cc_id` int(11) NOT NULL DEFAULT '0' COMMENT '证码验id',
  `cc_codevalue` varchar(50) DEFAULT NULL COMMENT '验证码值',
  `valid_flag` int(11) DEFAULT '1',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `send_time` timestamp NULL DEFAULT NULL,
  `sms_id` int(11) DEFAULT NULL COMMENT '短信id',
  PRIMARY KEY (`cc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='短信验证码';

-- ----------------------------
-- Records of cmt_sms_checkcode
-- ----------------------------

-- ----------------------------
-- Table structure for cmt_sms_record
-- ----------------------------
DROP TABLE IF EXISTS `cmt_sms_record`;
CREATE TABLE `cmt_sms_record` (
  `sms_id` int(11) NOT NULL COMMENT '短信id',
  `sms_phone` varchar(20) DEFAULT NULL,
  `sms_content` varchar(1000) DEFAULT NULL,
  `send_time` timestamp NULL DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) DEFAULT NULL COMMENT '0 发送短信平台成功 1发送短信成功 其他:发送失败',
  `remark` varchar(100) DEFAULT NULL,
  `tmp_id` varchar(20) DEFAULT NULL COMMENT '短信模板ID',
  `backsmsId` varchar(50) DEFAULT NULL COMMENT '短信平台短信id',
  `valid_flag` int(11) DEFAULT '1',
  `jsid` int(11) DEFAULT NULL COMMENT '所属加盟店',
  `respcode` varchar(20) DEFAULT NULL,
  `respmsg` varchar(100) DEFAULT NULL,
  `smstype` int(11) DEFAULT NULL COMMENT ' 2 普通短信 1验证码短信',
  PRIMARY KEY (`sms_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='短信发送记录表';

-- ----------------------------
-- Records of cmt_sms_record
-- ----------------------------

-- ----------------------------
-- Table structure for cmt_sms_smart
-- ----------------------------
DROP TABLE IF EXISTS `cmt_sms_smart`;
CREATE TABLE `cmt_sms_smart` (
  `smart_id` int(11) NOT NULL COMMENT '智能短信id',
  `sms_name` varchar(200) DEFAULT NULL COMMENT '短信名称',
  `sms_content` varchar(1000) DEFAULT NULL COMMENT '短信内容',
  `send_time` timestamp NULL DEFAULT NULL COMMENT '发送',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `smart_flag` int(11) DEFAULT '1' COMMENT '是否开启，1开启，0关闭',
  `valid_flag` int(11) DEFAULT '1',
  `sms_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`smart_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='智能短信表';

-- ----------------------------
-- Records of cmt_sms_smart
-- ----------------------------

-- ----------------------------
-- Table structure for cmt_sms_template
-- ----------------------------
DROP TABLE IF EXISTS `cmt_sms_template`;
CREATE TABLE `cmt_sms_template` (
  `tpid` int(11) NOT NULL DEFAULT '0' COMMENT '短信模板id',
  `platTmpId` varchar(20) DEFAULT '' COMMENT '短信平台的模板id',
  `platTmpName` varchar(200) DEFAULT NULL COMMENT '模板名称',
  `tmpContent` varchar(500) DEFAULT NULL COMMENT '模板内容',
  `valid_flag` int(11) DEFAULT '1',
  PRIMARY KEY (`tpid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='短信发送模板表';

-- ----------------------------
-- Records of cmt_sms_template
-- ----------------------------

-- ----------------------------
-- Table structure for cmt_wx_inf_log
-- ----------------------------
DROP TABLE IF EXISTS `cmt_wx_inf_log`;
CREATE TABLE `cmt_wx_inf_log` (
  `wx_inf_id` int(11) NOT NULL DEFAULT '0',
  `request_xml` varchar(1000) DEFAULT NULL COMMENT '发送xml',
  `result_xml` varchar(1000) DEFAULT NULL COMMENT ' 接收xml',
  `inf_name` varchar(100) DEFAULT NULL COMMENT '接口名称',
  `return_code` varchar(100) DEFAULT NULL COMMENT '通信标识',
  `return_msg` varchar(500) DEFAULT NULL COMMENT '返回信息',
  `result_code` varchar(100) DEFAULT NULL COMMENT '业务结果',
  `err_code` varchar(100) DEFAULT NULL COMMENT '错误代码',
  `err_code_des` varchar(500) DEFAULT NULL COMMENT '错误代码描述',
  `pay_biz_no` varchar(100) DEFAULT NULL COMMENT '支付订单id',
  `wx_biz_no` varchar(100) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `valid_flag` int(11) DEFAULT '1',
  PRIMARY KEY (`wx_inf_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='微信接口调用表';

-- ----------------------------
-- Records of cmt_wx_inf_log
-- ----------------------------

-- ----------------------------
-- Table structure for cmt_wx_pay
-- ----------------------------
DROP TABLE IF EXISTS `cmt_wx_pay`;
CREATE TABLE `cmt_wx_pay` (
  `preorder_id` int(11) NOT NULL DEFAULT '0',
  `jsid` int(11) DEFAULT NULL,
  `wx_order_no` varchar(100) DEFAULT NULL COMMENT '微信订单号',
  `pay_bizno` varchar(100) DEFAULT NULL COMMENT '户商订单编号',
  `pay_money` float DEFAULT NULL,
  `biz_desc` varchar(200) DEFAULT NULL COMMENT '订单描述',
  `prepay_id` varchar(100) DEFAULT NULL COMMENT '预支付交易会话标识',
  `code_url` varchar(200) DEFAULT NULL COMMENT '二维码链接',
  `pay_status` varchar(20) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `valid_flag` int(11) DEFAULT '1',
  PRIMARY KEY (`preorder_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='微信支付记录表';

-- ----------------------------
-- Records of cmt_wx_pay
-- ----------------------------

-- ----------------------------
-- Table structure for cmt_wx_refund
-- ----------------------------
DROP TABLE IF EXISTS `cmt_wx_refund`;
CREATE TABLE `cmt_wx_refund` (
  `wx_refend_id` int(11) NOT NULL DEFAULT '0',
  `jsid` int(11) DEFAULT NULL,
  `wx_order_no` varchar(100) DEFAULT NULL COMMENT '微信支付订单号',
  `wx_refund_no` varchar(100) DEFAULT NULL COMMENT '微信退款单号',
  `pay_bizno` varchar(100) DEFAULT NULL COMMENT '订单编号',
  `pay_refund_no` varchar(0) DEFAULT NULL,
  `total_fee` float DEFAULT NULL,
  `refund_fee` float DEFAULT NULL,
  `biz_desc` varchar(200) DEFAULT NULL COMMENT '订单描述',
  `prepay_id` varchar(100) DEFAULT NULL COMMENT '预支付交易会话标识',
  `refund_status` varchar(20) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `valid_flag` int(11) DEFAULT '1',
  PRIMARY KEY (`wx_refend_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='微信退款记录表';

-- ----------------------------
-- Records of cmt_wx_refund
-- ----------------------------

-- ----------------------------
-- Table structure for hy_mem_baseinfo
-- ----------------------------
DROP TABLE IF EXISTS `hy_mem_baseinfo`;
CREATE TABLE `hy_mem_baseinfo` (
  `memid` int(11) NOT NULL COMMENT '会员编码 ',
  `mem_no` varchar(20) DEFAULT NULL COMMENT '会员唯一码',
  `mem_name` varchar(50) DEFAULT NULL COMMENT '会员姓名',
  `mem_pinyin` varchar(50) DEFAULT NULL COMMENT '会员拼音码',
  `mem_level` int(11) DEFAULT NULL COMMENT '会员级别',
  `mem_quarity` int(11) DEFAULT NULL,
  `sex` char(1) DEFAULT NULL COMMENT '会员性别  1: 男  2： 女  0：未知',
  `active_way` int(1) DEFAULT NULL,
  `mem_ver_type` int(1) DEFAULT NULL COMMENT '证件类型',
  `mem_ver_no` varchar(20) DEFAULT NULL COMMENT '证件号',
  `mem_prov` varchar(20) DEFAULT NULL COMMENT '会员所在省',
  `mem_city` varchar(20) DEFAULT NULL COMMENT '会员所在市',
  `mem_area` varchar(200) DEFAULT NULL COMMENT '所在行政区划',
  `mem_address` varchar(200) DEFAULT NULL COMMENT '地址',
  `mem_email` varchar(50) DEFAULT NULL COMMENT '会员邮箱',
  `mem_phone` varchar(15) DEFAULT NULL COMMENT '电话',
  `mem_card_status` int(2) DEFAULT NULL COMMENT '会员卡状态',
  `mem_status` int(1) DEFAULT '1' COMMENT '会员状态',
  `mem_addtype` int(11) DEFAULT NULL COMMENT '是否通过购买卡方式注册',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `udpate_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '最新更新时间',
  `valid_flag` int(11) DEFAULT '1' COMMENT '有效标志（1：有效  0：无效）',
  `bank_code` varchar(50) DEFAULT NULL,
  `createor` int(11) DEFAULT NULL COMMENT '记录创建人',
  `bank_num` varchar(50) DEFAULT NULL,
  `bank_openuser_name` varchar(50) DEFAULT NULL,
  `bank_open_address` varchar(200) DEFAULT NULL,
  `nickname` varchar(200) DEFAULT NULL,
  `tags` varchar(200) DEFAULT NULL,
  `avatarpic_id` int(11) DEFAULT NULL,
  `md_detail` varchar(500) DEFAULT NULL COMMENT '详细地址',
  `memapplyid` int(11) DEFAULT NULL COMMENT '预申请id',
  `init_flag` int(11) DEFAULT NULL,
  `idcard_photoid` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`memid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员基本信息';

-- ----------------------------
-- Records of hy_mem_baseinfo
-- ----------------------------
INSERT INTO `hy_mem_baseinfo` VALUES ('1', 'zmkm111111', '蒋总', null, '3', '2', '', null, null, '', null, null, null, '', '', '13737725463', null, '1', null, null, '2016-05-02 08:57:18', '2018-03-14 15:54:11', '0', '', null, '', '', '', null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('2', 'zmkm222222', '蒋总', null, '7', '2', '1', null, null, '45242719930504314x', null, null, null, null, '', '13737725463', null, '1', null, '已发锁', '2016-06-17 10:16:39', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('3', 'zmkm333333', '蒋总3', null, '7', '2', '1', null, null, '5555678786', null, null, null, '', '', '1556555446', null, '1', null, null, '2016-06-17 10:17:22', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('4', 'zmkm674949', '汪鹏', null, '7', '2', '1', null, null, '430381198801187794', null, null, null, null, '', '15367811408', null, '0', null, null, '2016-06-17 10:17:35', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('5', 'zmkm812126', '汪鹏', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '0', null, null, '2016-06-17 10:17:47', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('6', 'zmkm473129', '汪鹏', null, '7', '2', '2', null, null, '', null, null, null, null, '', '', null, '0', null, null, '2016-06-17 10:18:00', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('7', 'zmkm785230', '张宏生', null, '7', '2', '1', null, null, '', null, null, null, null, '', '18978674899', null, '0', null, null, '2016-06-17 10:19:32', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('8', 'zmkm685717', '唐艳玲', null, '7', '2', '2', null, null, '432901197208141021', null, null, null, null, '', '13538583719', null, '1', null, null, '2016-06-17 10:19:44', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('9', 'zmkm426704', '周巍巍', null, '7', '2', '1', null, null, '111111111111111111', null, null, null, null, '', '15367811408', null, '0', null, null, '2016-06-17 10:19:57', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('10', 'zmkm348235', '李建华', null, '7', '2', '1', null, null, '430521196410187012', null, null, null, null, '', '', null, '0', null, null, '2016-06-17 10:20:10', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('11', 'zmkm203285', '李建华', null, '7', '2', '1', null, null, '430521196410187012', null, null, null, null, '', '18956589856', null, '1', null, null, '2016-06-17 10:20:22', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('12', 'zmkm873208', '罗杜鹃', null, '7', '2', '1', null, null, '', null, null, null, null, '', '13823423422', null, '0', null, null, '2016-06-17 10:20:35', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('13', 'zmkm948120', '唐艳玲', null, '3', '2', '1', null, null, '432901197208141021', null, null, null, null, '', '13538583719', null, '1', null, null, '2016-06-17 10:21:26', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('14', 'zmkm899328', '刘翔', null, '7', '2', '2', null, null, '', null, null, null, null, '', '18677397902', null, '1', null, null, '2016-06-17 10:21:50', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('15', 'zmkm921777', '隆锦', null, '7', '2', '0', null, null, '430522198002075885', null, null, null, null, '', '13975937535', null, '1', null, '', '2016-06-17 10:22:03', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('16', 'zmkm599108', '易文斌', null, '7', '2', '2', null, null, '', null, null, null, null, '', '15077306462', null, '0', null, null, '2016-06-17 10:22:32', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('17', 'zmkm158816', '邓艳珍', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '0', null, null, '2016-06-17 10:25:18', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('18', 'zmkm183218', '袁辉军', null, '7', '2', '1', null, null, '430521196410187012', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:25:29', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('19', 'zmkm685278', '龙云', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '0', null, null, '2016-06-17 10:25:39', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('20', 'zmkm891106', '李立新', null, '7', '2', '1', null, null, '', null, null, null, null, '', '13812311231', null, '1', null, null, '2016-06-17 10:25:50', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('21', 'zmkm997917', '于强', null, '7', '2', '1', null, null, '', null, null, null, null, '', '13333333333', null, '2', null, null, '2016-06-17 10:26:01', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('22', 'zmkm644398', '李利军', null, '3', '2', '1', null, null, '432922197808250012', null, null, null, null, '', '13717777103', null, '1', null, null, '2016-06-17 10:26:15', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('23', 'zmkm819929', '陈梅正', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:26:28', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('24', 'zmkm960937', '易莉', null, '7', '2', '1', null, null, '432922197207280822', null, null, null, null, '', '13974682968', null, '1', null, null, '2016-06-17 10:26:38', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('25', 'zmkm067261', '莫春风', null, '7', '2', '1', null, null, '440524196802141241', null, null, null, null, '', '13087733101', null, '1', null, null, '2016-06-17 10:26:50', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('26', 'zmkm514944', '于海琦', null, '7', '2', '1', null, null, '', null, null, null, null, '', '18978674899', null, '0', null, null, '2016-06-17 10:27:03', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('27', 'zmkm904202', '何桂易', null, '7', '2', '0', null, null, '431124197011301861', null, null, null, null, '', '13627738055', null, '1', null, null, '2016-06-17 10:27:15', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('28', 'zmkm620132', '文丽君', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:27:26', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('29', 'zmkm399001', '赵锦琦', null, '7', '2', '1', null, null, '452330196209090041', null, null, null, null, '', '18378391988', null, '1', null, null, '2016-06-17 10:27:37', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('30', 'zmkm399887', '周玉美', null, '7', '2', '1', null, null, '', null, null, null, null, '', '13707830567', null, '1', null, null, '2016-06-17 10:27:48', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('31', 'zmkm112172', '赵桂有', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:27:59', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('32', 'zmkm081939', '龙红云', null, null, '2', '1', null, null, '', null, null, null, null, '', '13576576575', null, '1', null, null, '2016-06-17 10:28:09', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('33', 'zmkm701698', '李元元', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:28:20', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('34', 'zmkm06667380', '李延达', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '2', null, null, '2016-06-17 10:28:31', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('35', 'zmkm88603788', '谢玉情', null, '7', '2', '1', null, null, '', null, null, null, null, '', '13607837996', null, '0', null, null, '2016-06-17 10:28:41', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('36', 'zmkm920504', '刘方', null, '7', '2', '1', null, null, '429001198509046466', null, null, null, null, '', '13681022359', null, '1', null, null, '2016-06-17 10:28:52', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('37', 'zmkm067313', '陆艳飞', null, '3', '2', '2', null, null, '42062119860915806x', null, null, null, null, '', '18972205310', null, '1', null, null, '2016-06-17 10:29:02', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('38', 'zmkm920815', '王建平', null, '7', '2', '1', null, null, '431102197512100023', null, null, null, null, '', '18601302245', null, '1', null, null, '2016-06-17 10:29:13', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('39', 'zmkm689879', '高君云', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '0', null, null, '2016-06-17 10:29:24', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('40', 'zmkm614215', '刘佳', null, '3', '2', '2', null, null, '420604197107100529', null, null, null, null, '', '13607271233', null, '1', null, null, '2016-06-17 10:29:34', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('41', 'zmkm06711517', '蒋甲青', null, '7', '2', '1', null, null, '452323197409291658', null, null, null, null, '', '13768744268', null, '1', null, null, '2016-06-17 10:29:45', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('42', 'zmkm980796', '陈垒', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '0', null, null, '2016-06-17 10:29:56', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('43', 'zmkm465908', '马玉梅', null, '7', '2', '1', null, null, '420621197310130121', null, null, null, null, '', '13707271321', null, '1', null, null, '2016-06-17 10:30:07', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('44', 'zmkm99966203', '曾华容', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:30:17', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('45', 'zmkm25443543', '赵文华', null, '7', '2', '1', null, null, '452323196703180014', null, null, null, null, '', '18176350197', null, '1', null, null, '2016-06-17 10:30:28', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('46', 'zmkm353758', '陈怀山', null, '7', '2', '1', null, null, '42062119600218131x', null, null, null, null, '', '18986360218', null, '1', null, null, '2016-06-17 10:30:39', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('47', 'zmkm200865', '刘强', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:30:49', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('48', 'zmkm505068', '周宏胜', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:31:00', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('49', 'zmkm23695498', '唐大林', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:31:10', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('50', 'zmkm55050865', '李珍林', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:31:21', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('51', 'zmkm168215', '王玉华', null, '3', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:31:32', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('52', 'zmkm377556', '付爱荣', null, '7', '2', '1', null, null, '410611196108153021', null, null, null, null, '', '13939270057', null, '1', null, null, '2016-06-17 10:31:42', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('53', 'zmkm560818', '唐惠', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:31:53', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('54', 'zmkm853331', '王玉华', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:32:04', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('55', 'zmkm639613', '唐惠', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:32:14', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('56', 'zmkm186429', '王庆忠', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:32:25', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('57', 'zmkm022249', '盛红桃', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:32:35', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('58', 'zmkm49720969', '岳宝珍', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:32:46', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('59', 'zmkm020920', '邓逢平', null, '7', '2', '1', null, null, '61252619821017773X', null, null, null, null, '', '13207254999', null, '1', null, null, '2016-06-17 10:32:56', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('60', 'zmkm638600', '周合忠', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:33:07', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('61', 'zmkm064829', '盛红桃', null, '7', '2', '0', null, null, '320826197203060024', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:33:18', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('62', 'zmkm18466907', '陈辉锦', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:33:29', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('63', 'zmkm184050', '李素萍', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:33:39', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('64', 'zmkm828168', '李旭妍', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:33:50', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('65', 'zmkm312832', '陈有凤', null, '7', '2', '1', null, null, '420601196512075028', null, null, null, null, '', '15971125603', null, '1', null, null, '2016-06-17 10:34:01', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('66', 'zmkm763859', '赵娇', null, '3', '2', '1', null, null, '420607198907103625', null, null, null, null, '', '13277118222', null, '1', null, null, '2016-06-17 10:34:11', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('67', 'zmkm74399080', '聂卫萍', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:34:22', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('68', 'zmkm693327', '陈帅', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:34:32', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('69', 'zmkm079808', '刘盛海', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:34:43', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('70', 'zmkm24618041', '赵惠莲', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:34:53', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('71', 'zmkm342989', '董文', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:35:04', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('72', 'zmkm901619', '王正东', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:35:15', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('73', 'zmkm47231362', '唐左龙', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:35:25', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('74', 'zmkm704892', '张燕', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:35:36', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('75', 'zmkm10498759', '马占马', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:35:46', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('76', 'zmkm55255746', '吴丽娟', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:35:57', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('77', 'zmkm83117934', '张婷玉', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:36:07', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('78', 'zmkm75497896', '白中伟', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:36:18', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('79', 'zmkm93955120', '周乐', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:36:29', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('80', 'zmkm87291197', '肖燕', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:36:40', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('81', 'zmkm80335135', '王英武', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:36:52', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('82', 'zmkm309696', '马芸杰', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:37:02', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('83', 'zmkm00081901', '黄美女', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:37:13', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('84', 'zmkm742461', '肖自平', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:37:24', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('85', 'zmkm600847', '夏忠军', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:37:34', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('86', 'zmkm49237496', '毛锋', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:37:45', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('87', 'zmkm522366', '周琳', null, '7', '2', '1', null, null, '431028198301280223', null, null, null, null, '', '13094114344', null, '1', null, null, '2016-06-17 10:37:55', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('88', 'zmkm76980801', '周卫国', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:38:06', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('89', 'zmkm435539', '舒枫', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:38:17', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('90', 'zmkm83410325', '周乐', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:38:27', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('91', 'zmkm962180', '李芬', null, '7', '2', '2', null, null, '430424198101132723', null, null, null, null, '', '13809269418', null, '1', null, null, '2016-06-17 10:38:38', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('92', 'zmkm06738845', '付爱凤', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:38:49', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('93', 'zmkm73710443', '吴云松', null, '7', '2', '1', null, null, '362101197404131320', null, null, null, null, '', '18179224488', null, '1', null, null, '2016-06-17 10:38:59', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('94', 'zmkm34939839', '赵岩军', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:39:10', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('95', 'zmkm111088', '蒋林燕', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:39:20', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('96', 'zmkm24284356', '李赛', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:39:31', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('97', 'zmkm692261', '朱逢莲', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:39:42', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('98', 'zmkm771777', '姚湘黔', null, '7', '2', '1', null, null, '', null, null, null, null, '', '', null, '1', null, null, '2016-06-17 10:39:52', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('99', 'zmkm720555', '陈新生', null, '7', '2', '1', null, null, '321123196905191212', null, null, null, null, '', '13926871339', null, '1', null, null, '2016-06-17 10:40:02', '2017-06-24 16:37:01', '1', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('100', 'zmkm550123', '尚利丽', null, '7', '2', '1', null, null, '420621198607263886', null, null, null, null, '', '15570681999', null, '1', null, null, '2016-06-17 10:40:13', '2017-06-28 16:17:56', '0', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('102', null, '111', null, null, null, '1', null, null, null, null, null, null, null, '11111111111111111', '1111111', null, '1', null, null, '2017-07-03 15:41:46', '2017-07-03 15:44:06', '0', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `hy_mem_baseinfo` VALUES ('103', '', '', '', null, null, '', null, null, '', '', '', '', '', '', '', null, '1', null, '', '2018-03-14 16:09:52', '0000-00-00 00:00:00', '1', '', null, '', '', '', '', '', null, '', null, null, '');

-- ----------------------------
-- Table structure for sys_access
-- ----------------------------
DROP TABLE IF EXISTS `sys_access`;
CREATE TABLE `sys_access` (
  `access_id` int(11) NOT NULL DEFAULT '0' COMMENT '接入编号',
  `app_id` varchar(100) DEFAULT NULL,
  `app_name` varchar(100) DEFAULT NULL COMMENT '接入应用名称',
  `verfiy_name` varchar(20) DEFAULT NULL COMMENT '接入用户名',
  `verfiy_password` varchar(50) DEFAULT NULL COMMENT '接入密码',
  `status` int(1) DEFAULT NULL COMMENT '入接状态',
  `valid_flag` int(11) DEFAULT '1' COMMENT '有效标志',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`access_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='接口用户表';

-- ----------------------------
-- Records of sys_access
-- ----------------------------
INSERT INTO `sys_access` VALUES ('1', 'APP123121', 'weixin', 'test', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '1', '2015-08-25 16:21:24');

-- ----------------------------
-- Table structure for sys_area
-- ----------------------------
DROP TABLE IF EXISTS `sys_area`;
CREATE TABLE `sys_area` (
  `area_code` int(6) NOT NULL COMMENT '行政区划代码',
  `area_name` varchar(50) DEFAULT NULL COMMENT '行政区划名称',
  `area_level` int(1) DEFAULT NULL COMMENT '行政区划等级',
  `area_up_code` int(6) DEFAULT NULL COMMENT '上级行政区划',
  `zipcode` varchar(6) DEFAULT NULL,
  `pinyin` varchar(500) DEFAULT NULL,
  `openflag` int(1) DEFAULT '0' COMMENT '开通标志',
  `valid_flag` int(11) DEFAULT '1',
  PRIMARY KEY (`area_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='行政区划';

-- ----------------------------
-- Records of sys_area
-- ----------------------------
INSERT INTO `sys_area` VALUES ('110000', '北京市', '1', '0', null, 'bjs', '1', '1');
INSERT INTO `sys_area` VALUES ('110100', '北京市(辖区)', '2', '110000', null, 'bjs(xq)', '1', '1');
INSERT INTO `sys_area` VALUES ('110101', '东城区', '3', '110100', '010', 'dcq', null, '1');
INSERT INTO `sys_area` VALUES ('110102', '西城区', '3', '110100', '010', 'xcq', null, '1');
INSERT INTO `sys_area` VALUES ('110105', '朝阳区', '3', '110100', '010', 'cyq', null, '1');
INSERT INTO `sys_area` VALUES ('110106', '丰台区', '3', '110100', '010', 'ftq', null, '1');
INSERT INTO `sys_area` VALUES ('110107', '石景山区', '3', '110100', '010', 'sjsq', null, '1');
INSERT INTO `sys_area` VALUES ('110108', '海淀区', '3', '110100', '010', 'hdq', null, '1');
INSERT INTO `sys_area` VALUES ('110109', '门头沟区', '3', '110100', '010', 'mtgq', null, '1');
INSERT INTO `sys_area` VALUES ('110111', '房山区', '3', '110100', '010', 'fsq', null, '1');
INSERT INTO `sys_area` VALUES ('110112', '通州区', '3', '110100', '010', 'tzq', null, '1');
INSERT INTO `sys_area` VALUES ('110113', '顺义区', '3', '110100', '010', 'syq', null, '1');
INSERT INTO `sys_area` VALUES ('110114', '昌平区', '3', '110100', '010', 'cpq', null, '1');
INSERT INTO `sys_area` VALUES ('110115', '大兴区', '3', '110100', '010', 'dxq', null, '1');
INSERT INTO `sys_area` VALUES ('110116', '怀柔区', '3', '110100', '010', 'hrq', null, '1');
INSERT INTO `sys_area` VALUES ('110117', '平谷区', '3', '110100', '010', 'pgq', null, '1');
INSERT INTO `sys_area` VALUES ('110200', '县', '2', '110000', null, 'x', '0', '1');
INSERT INTO `sys_area` VALUES ('110228', '密云县', '3', '110200', '010', 'myx', null, '1');
INSERT INTO `sys_area` VALUES ('110229', '延庆县', '3', '110200', '010', 'yqx', null, '1');
INSERT INTO `sys_area` VALUES ('120000', '天津市', '1', '0', null, 'tjs', '1', '1');
INSERT INTO `sys_area` VALUES ('120100', '天津市(辖区)', '2', '120000', null, 'tjs(xq)', '1', '1');
INSERT INTO `sys_area` VALUES ('120101', '和平区', '3', '120100', '022', 'hpq', null, '1');
INSERT INTO `sys_area` VALUES ('120102', '河东区', '3', '120100', '022', 'hdq', null, '1');
INSERT INTO `sys_area` VALUES ('120103', '河西区', '3', '120100', '022', 'hxq', null, '1');
INSERT INTO `sys_area` VALUES ('120104', '南开区', '3', '120100', '022', 'nkq', null, '1');
INSERT INTO `sys_area` VALUES ('120105', '河北区', '3', '120100', '022', 'hbq', null, '1');
INSERT INTO `sys_area` VALUES ('120106', '红桥区', '3', '120100', '022', 'hqq', null, '1');
INSERT INTO `sys_area` VALUES ('120110', '东丽区', '3', '120100', '022', 'dlq', null, '1');
INSERT INTO `sys_area` VALUES ('120111', '西青区', '3', '120100', '022', 'xqq', null, '1');
INSERT INTO `sys_area` VALUES ('120112', '津南区', '3', '120100', '022', 'jnq', null, '1');
INSERT INTO `sys_area` VALUES ('120113', '北辰区', '3', '120100', '022', 'bcq', null, '1');
INSERT INTO `sys_area` VALUES ('120114', '武清区', '3', '120100', '022', 'wqq', null, '1');
INSERT INTO `sys_area` VALUES ('120115', '宝坻区', '3', '120100', '022', 'bcq', null, '1');
INSERT INTO `sys_area` VALUES ('120116', '滨海新区', '3', '120100', null, 'bhxq', null, '1');
INSERT INTO `sys_area` VALUES ('120200', '县', '2', '120000', null, 'x', null, '1');
INSERT INTO `sys_area` VALUES ('120221', '宁河县', '3', '120200', '022', 'nhx', null, '1');
INSERT INTO `sys_area` VALUES ('120223', '静海县', '3', '120200', '022', 'jhx', null, '1');
INSERT INTO `sys_area` VALUES ('120225', '蓟县', '3', '120200', '022', 'jx', null, '1');
INSERT INTO `sys_area` VALUES ('130000', '河北省', '1', '0', null, 'hbs', '1', '1');
INSERT INTO `sys_area` VALUES ('130100', '石家庄市', '2', '130000', null, 'sjzs', null, '1');
INSERT INTO `sys_area` VALUES ('130101', '市辖区', '3', '130100', '0311', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('130102', '长安区', '3', '130100', '0311', 'zaq', null, '1');
INSERT INTO `sys_area` VALUES ('130103', '桥东区', '3', '130100', '0311', 'qdq', null, '1');
INSERT INTO `sys_area` VALUES ('130104', '桥西区', '3', '130100', '0311', 'qxq', null, '1');
INSERT INTO `sys_area` VALUES ('130105', '新华区', '3', '130100', '0311', 'xhq', null, '1');
INSERT INTO `sys_area` VALUES ('130107', '井陉矿区', '3', '130100', '0311', 'jxkq', null, '1');
INSERT INTO `sys_area` VALUES ('130108', '裕华区', '3', '130100', '0311', 'yhq', null, '1');
INSERT INTO `sys_area` VALUES ('130121', '井陉县', '3', '130100', '0311', 'jxx', null, '1');
INSERT INTO `sys_area` VALUES ('130123', '正定县', '3', '130100', '0311', 'zdx', null, '1');
INSERT INTO `sys_area` VALUES ('130124', '栾城县', '3', '130100', '0311', 'lcx', null, '1');
INSERT INTO `sys_area` VALUES ('130125', '行唐县', '3', '130100', '0311', 'xtx', null, '1');
INSERT INTO `sys_area` VALUES ('130126', '灵寿县', '3', '130100', '0311', 'lsx', null, '1');
INSERT INTO `sys_area` VALUES ('130127', '高邑县', '3', '130100', '0311', 'gyx', null, '1');
INSERT INTO `sys_area` VALUES ('130128', '深泽县', '3', '130100', '0311', 'szx', null, '1');
INSERT INTO `sys_area` VALUES ('130129', '赞皇县', '3', '130100', '0311', 'zhx', null, '1');
INSERT INTO `sys_area` VALUES ('130130', '无极县', '3', '130100', '0311', 'wjx', null, '1');
INSERT INTO `sys_area` VALUES ('130131', '平山县', '3', '130100', '0311', 'psx', null, '1');
INSERT INTO `sys_area` VALUES ('130132', '元氏县', '3', '130100', '0311', 'ysx', null, '1');
INSERT INTO `sys_area` VALUES ('130133', '赵县', '3', '130100', '0311', 'zx', null, '1');
INSERT INTO `sys_area` VALUES ('130181', '辛集市', '3', '130100', '0311', 'xjs', null, '1');
INSERT INTO `sys_area` VALUES ('130182', '藁城市', '3', '130100', '0311', 'gcs', null, '1');
INSERT INTO `sys_area` VALUES ('130183', '晋州市', '3', '130100', '0311', 'jzs', null, '1');
INSERT INTO `sys_area` VALUES ('130184', '新乐市', '3', '130100', '0311', 'xls', null, '1');
INSERT INTO `sys_area` VALUES ('130185', '鹿泉市', '3', '130100', '0311', 'lqs', null, '1');
INSERT INTO `sys_area` VALUES ('130200', '唐山市', '2', '130000', null, 'tss', null, '1');
INSERT INTO `sys_area` VALUES ('130201', '市辖区', '3', '130200', '0315', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('130202', '路南区', '3', '130200', '0315', 'lnq', null, '1');
INSERT INTO `sys_area` VALUES ('130203', '路北区', '3', '130200', '0315', 'lbq', null, '1');
INSERT INTO `sys_area` VALUES ('130204', '古冶区', '3', '130200', '0315', 'gyq', null, '1');
INSERT INTO `sys_area` VALUES ('130205', '开平区', '3', '130200', '0315', 'kpq', null, '1');
INSERT INTO `sys_area` VALUES ('130207', '丰南区', '3', '130200', '0315', 'fnq', null, '1');
INSERT INTO `sys_area` VALUES ('130208', '丰润区', '3', '130200', '0315', 'frq', null, '1');
INSERT INTO `sys_area` VALUES ('130209', '曹妃甸区', '3', '130200', null, 'cfdq', null, '1');
INSERT INTO `sys_area` VALUES ('130223', '滦县', '3', '130200', '0315', 'lx', null, '1');
INSERT INTO `sys_area` VALUES ('130224', '滦南县', '3', '130200', '0315', 'lnx', null, '1');
INSERT INTO `sys_area` VALUES ('130225', '乐亭县', '3', '130200', '0315', 'ltx', null, '1');
INSERT INTO `sys_area` VALUES ('130227', '迁西县', '3', '130200', '0315', 'qxx', null, '1');
INSERT INTO `sys_area` VALUES ('130229', '玉田县', '3', '130200', '0315', 'ytx', null, '1');
INSERT INTO `sys_area` VALUES ('130281', '遵化市', '3', '130200', '0315', 'zhs', null, '1');
INSERT INTO `sys_area` VALUES ('130283', '迁安市', '3', '130200', '0315', 'qas', null, '1');
INSERT INTO `sys_area` VALUES ('130300', '秦皇岛市', '2', '130000', null, 'qhds', null, '1');
INSERT INTO `sys_area` VALUES ('130301', '市辖区', '3', '130300', '0335', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('130302', '海港区', '3', '130300', '0335', 'hgq', null, '1');
INSERT INTO `sys_area` VALUES ('130303', '山海关区', '3', '130300', '0335', 'shgq', null, '1');
INSERT INTO `sys_area` VALUES ('130304', '北戴河区', '3', '130300', '0335', 'bdhq', null, '1');
INSERT INTO `sys_area` VALUES ('130321', '青龙满族自治县', '3', '130300', '0335', 'qlmzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('130322', '昌黎县', '3', '130300', '0335', 'clx', null, '1');
INSERT INTO `sys_area` VALUES ('130323', '抚宁县', '3', '130300', '0335', 'fnx', null, '1');
INSERT INTO `sys_area` VALUES ('130324', '卢龙县', '3', '130300', '0335', 'llx', null, '1');
INSERT INTO `sys_area` VALUES ('130400', '邯郸市', '2', '130000', null, 'hds', null, '1');
INSERT INTO `sys_area` VALUES ('130401', '市辖区', '3', '130400', '0310', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('130402', '邯山区', '3', '130400', '0310', 'hsq', null, '1');
INSERT INTO `sys_area` VALUES ('130403', '丛台区', '3', '130400', '0310', 'ctq', null, '1');
INSERT INTO `sys_area` VALUES ('130404', '复兴区', '3', '130400', '0310', 'fxq', null, '1');
INSERT INTO `sys_area` VALUES ('130406', '峰峰矿区', '3', '130400', '0310', 'ffkq', null, '1');
INSERT INTO `sys_area` VALUES ('130421', '邯郸县', '3', '130400', '0310', 'hdx', null, '1');
INSERT INTO `sys_area` VALUES ('130423', '临漳县', '3', '130400', '0310', 'lzx', null, '1');
INSERT INTO `sys_area` VALUES ('130424', '成安县', '3', '130400', '0310', 'cax', null, '1');
INSERT INTO `sys_area` VALUES ('130425', '大名县', '3', '130400', '0310', 'dmx', null, '1');
INSERT INTO `sys_area` VALUES ('130426', '涉县', '3', '130400', '0310', 'sx', null, '1');
INSERT INTO `sys_area` VALUES ('130427', '磁县', '3', '130400', '0310', 'cx', null, '1');
INSERT INTO `sys_area` VALUES ('130428', '肥乡县', '3', '130400', '0310', 'fxx', null, '1');
INSERT INTO `sys_area` VALUES ('130429', '永年县', '3', '130400', '0310', 'ynx', null, '1');
INSERT INTO `sys_area` VALUES ('130430', '邱县', '3', '130400', '0310', 'qx', null, '1');
INSERT INTO `sys_area` VALUES ('130431', '鸡泽县', '3', '130400', '0310', 'jzx', null, '1');
INSERT INTO `sys_area` VALUES ('130432', '广平县', '3', '130400', '0310', 'gpx', null, '1');
INSERT INTO `sys_area` VALUES ('130433', '馆陶县', '3', '130400', '0310', 'gtx', null, '1');
INSERT INTO `sys_area` VALUES ('130434', '魏县', '3', '130400', '0310', 'wx', null, '1');
INSERT INTO `sys_area` VALUES ('130435', '曲周县', '3', '130400', '0310', 'qzx', null, '1');
INSERT INTO `sys_area` VALUES ('130481', '武安市', '3', '130400', '0310', 'was', null, '1');
INSERT INTO `sys_area` VALUES ('130500', '邢台市', '2', '130000', null, 'xts', null, '1');
INSERT INTO `sys_area` VALUES ('130501', '市辖区', '3', '130500', '0319', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('130502', '桥东区', '3', '130500', '0319', 'qdq', null, '1');
INSERT INTO `sys_area` VALUES ('130503', '桥西区', '3', '130500', '0319', 'qxq', null, '1');
INSERT INTO `sys_area` VALUES ('130521', '邢台县', '3', '130500', '0319', 'xtx', null, '1');
INSERT INTO `sys_area` VALUES ('130522', '临城县', '3', '130500', '0319', 'lcx', null, '1');
INSERT INTO `sys_area` VALUES ('130523', '内丘县', '3', '130500', '0319', 'nqx', null, '1');
INSERT INTO `sys_area` VALUES ('130524', '柏乡县', '3', '130500', '0319', 'bxx', null, '1');
INSERT INTO `sys_area` VALUES ('130525', '隆尧县', '3', '130500', '0319', 'lyx', null, '1');
INSERT INTO `sys_area` VALUES ('130526', '任县', '3', '130500', '0319', 'rx', null, '1');
INSERT INTO `sys_area` VALUES ('130527', '南和县', '3', '130500', '0319', 'nhx', null, '1');
INSERT INTO `sys_area` VALUES ('130528', '宁晋县', '3', '130500', '0319', 'njx', null, '1');
INSERT INTO `sys_area` VALUES ('130529', '巨鹿县', '3', '130500', '0319', 'jlx', null, '1');
INSERT INTO `sys_area` VALUES ('130530', '新河县', '3', '130500', '0319', 'xhx', null, '1');
INSERT INTO `sys_area` VALUES ('130531', '广宗县', '3', '130500', '0319', 'gzx', null, '1');
INSERT INTO `sys_area` VALUES ('130532', '平乡县', '3', '130500', '0319', 'pxx', null, '1');
INSERT INTO `sys_area` VALUES ('130533', '威县', '3', '130500', '0319', 'wx', null, '1');
INSERT INTO `sys_area` VALUES ('130534', '清河县', '3', '130500', '0319', 'qhx', null, '1');
INSERT INTO `sys_area` VALUES ('130535', '临西县', '3', '130500', '0319', 'lxx', null, '1');
INSERT INTO `sys_area` VALUES ('130581', '南宫市', '3', '130500', '0319', 'ngs', null, '1');
INSERT INTO `sys_area` VALUES ('130582', '沙河市', '3', '130500', '0319', 'shs', null, '1');
INSERT INTO `sys_area` VALUES ('130600', '保定市', '2', '130000', null, 'bds', null, '1');
INSERT INTO `sys_area` VALUES ('130601', '市辖区', '3', '130600', '0312', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('130602', '新市区', '3', '130600', '0312', 'xsq', null, '1');
INSERT INTO `sys_area` VALUES ('130603', '北市区', '3', '130600', '0312', 'bsq', null, '1');
INSERT INTO `sys_area` VALUES ('130604', '南市区', '3', '130600', '0312', 'nsq', null, '1');
INSERT INTO `sys_area` VALUES ('130621', '满城县', '3', '130600', '0312', 'mcx', null, '1');
INSERT INTO `sys_area` VALUES ('130622', '清苑县', '3', '130600', '0312', 'qyx', null, '1');
INSERT INTO `sys_area` VALUES ('130623', '涞水县', '3', '130600', '0312', 'lsx', null, '1');
INSERT INTO `sys_area` VALUES ('130624', '阜平县', '3', '130600', '0312', 'fpx', null, '1');
INSERT INTO `sys_area` VALUES ('130625', '徐水县', '3', '130600', '0312', 'xsx', null, '1');
INSERT INTO `sys_area` VALUES ('130626', '定兴县', '3', '130600', '0312', 'dxx', null, '1');
INSERT INTO `sys_area` VALUES ('130627', '唐县', '3', '130600', '0312', 'tx', null, '1');
INSERT INTO `sys_area` VALUES ('130628', '高阳县', '3', '130600', '0312', 'gyx', null, '1');
INSERT INTO `sys_area` VALUES ('130629', '容城县', '3', '130600', '0312', 'rcx', null, '1');
INSERT INTO `sys_area` VALUES ('130630', '涞源县', '3', '130600', '0312', 'lyx', null, '1');
INSERT INTO `sys_area` VALUES ('130631', '望都县', '3', '130600', '0312', 'wdx', null, '1');
INSERT INTO `sys_area` VALUES ('130632', '安新县', '3', '130600', '0312', 'axx', null, '1');
INSERT INTO `sys_area` VALUES ('130633', '易县', '3', '130600', '0312', 'yx', null, '1');
INSERT INTO `sys_area` VALUES ('130634', '曲阳县', '3', '130600', '0312', 'qyx', null, '1');
INSERT INTO `sys_area` VALUES ('130635', '蠡县', '3', '130600', '0312', 'lx', null, '1');
INSERT INTO `sys_area` VALUES ('130636', '顺平县', '3', '130600', '0312', 'spx', null, '1');
INSERT INTO `sys_area` VALUES ('130637', '博野县', '3', '130600', '0312', 'byx', null, '1');
INSERT INTO `sys_area` VALUES ('130638', '雄县', '3', '130600', '0312', 'xx', null, '1');
INSERT INTO `sys_area` VALUES ('130681', '涿州市', '3', '130600', '0312', 'zzs', null, '1');
INSERT INTO `sys_area` VALUES ('130682', '定州市', '3', '130600', '0312', 'dzs', null, '1');
INSERT INTO `sys_area` VALUES ('130683', '安国市', '3', '130600', '0312', 'ags', null, '1');
INSERT INTO `sys_area` VALUES ('130684', '高碑店市', '3', '130600', '0312', 'gbds', null, '1');
INSERT INTO `sys_area` VALUES ('130700', '张家口市', '2', '130000', null, 'zjks', null, '1');
INSERT INTO `sys_area` VALUES ('130701', '市辖区', '3', '130700', '0313', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('130702', '桥东区', '3', '130700', '0313', 'qdq', null, '1');
INSERT INTO `sys_area` VALUES ('130703', '桥西区', '3', '130700', '0313', 'qxq', null, '1');
INSERT INTO `sys_area` VALUES ('130705', '宣化区', '3', '130700', '0313', 'xhq', null, '1');
INSERT INTO `sys_area` VALUES ('130706', '下花园区', '3', '130700', '0313', 'xhyq', null, '1');
INSERT INTO `sys_area` VALUES ('130721', '宣化县', '3', '130700', '0313', 'xhx', null, '1');
INSERT INTO `sys_area` VALUES ('130722', '张北县', '3', '130700', '0313', 'zbx', null, '1');
INSERT INTO `sys_area` VALUES ('130723', '康保县', '3', '130700', '0313', 'kbx', null, '1');
INSERT INTO `sys_area` VALUES ('130724', '沽源县', '3', '130700', '0313', 'gyx', null, '1');
INSERT INTO `sys_area` VALUES ('130725', '尚义县', '3', '130700', '0313', 'syx', null, '1');
INSERT INTO `sys_area` VALUES ('130726', '蔚县', '3', '130700', '0313', 'yx', null, '1');
INSERT INTO `sys_area` VALUES ('130727', '阳原县', '3', '130700', '0313', 'yyx', null, '1');
INSERT INTO `sys_area` VALUES ('130728', '怀安县', '3', '130700', '0313', 'hax', null, '1');
INSERT INTO `sys_area` VALUES ('130729', '万全县', '3', '130700', '0313', 'wqx', null, '1');
INSERT INTO `sys_area` VALUES ('130730', '怀来县', '3', '130700', '0313', 'hlx', null, '1');
INSERT INTO `sys_area` VALUES ('130731', '涿鹿县', '3', '130700', '0313', 'zlx', null, '1');
INSERT INTO `sys_area` VALUES ('130732', '赤城县', '3', '130700', '0313', 'ccx', null, '1');
INSERT INTO `sys_area` VALUES ('130733', '崇礼县', '3', '130700', '0313', 'clx', null, '1');
INSERT INTO `sys_area` VALUES ('130800', '承德市', '2', '130000', null, 'cds', null, '1');
INSERT INTO `sys_area` VALUES ('130801', '市辖区', '3', '130800', '0314', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('130802', '双桥区', '3', '130800', '0314', 'sqq', null, '1');
INSERT INTO `sys_area` VALUES ('130803', '双滦区', '3', '130800', '0314', 'slq', null, '1');
INSERT INTO `sys_area` VALUES ('130804', '鹰手营子矿区', '3', '130800', '0314', 'ysyzkq', null, '1');
INSERT INTO `sys_area` VALUES ('130821', '承德县', '3', '130800', '0314', 'cdx', null, '1');
INSERT INTO `sys_area` VALUES ('130822', '兴隆县', '3', '130800', '0314', 'xlx', null, '1');
INSERT INTO `sys_area` VALUES ('130823', '平泉县', '3', '130800', '0314', 'pqx', null, '1');
INSERT INTO `sys_area` VALUES ('130824', '滦平县', '3', '130800', '0314', 'lpx', null, '1');
INSERT INTO `sys_area` VALUES ('130825', '隆化县', '3', '130800', '0314', 'lhx', null, '1');
INSERT INTO `sys_area` VALUES ('130826', '丰宁满族自治县', '3', '130800', '0314', 'fnmzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('130827', '宽城满族自治县', '3', '130800', '0314', 'kcmzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('130828', '围场满族蒙古族自治县', '3', '130800', '0314', 'wcmzmgzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('130900', '沧州市', '2', '130000', null, 'czs', null, '1');
INSERT INTO `sys_area` VALUES ('130901', '市辖区', '3', '130900', '0317', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('130902', '新华区', '3', '130900', '0317', 'xhq', null, '1');
INSERT INTO `sys_area` VALUES ('130903', '运河区', '3', '130900', '0317', 'yhq', null, '1');
INSERT INTO `sys_area` VALUES ('130921', '沧县', '3', '130900', '0317', 'cx', null, '1');
INSERT INTO `sys_area` VALUES ('130922', '青县', '3', '130900', '0315', 'qx', null, '1');
INSERT INTO `sys_area` VALUES ('130923', '东光县', '3', '130900', '0317', 'dgx', null, '1');
INSERT INTO `sys_area` VALUES ('130924', '海兴县', '3', '130900', '0317', 'hxx', null, '1');
INSERT INTO `sys_area` VALUES ('130925', '盐山县', '3', '130900', '0317', 'ysx', null, '1');
INSERT INTO `sys_area` VALUES ('130926', '肃宁县', '3', '130900', '0317', 'snx', null, '1');
INSERT INTO `sys_area` VALUES ('130927', '南皮县', '3', '130900', '0317', 'npx', null, '1');
INSERT INTO `sys_area` VALUES ('130928', '吴桥县', '3', '130900', '0317', 'wqx', null, '1');
INSERT INTO `sys_area` VALUES ('130929', '献县', '3', '130900', '0317', 'xx', null, '1');
INSERT INTO `sys_area` VALUES ('130930', '孟村回族自治县', '3', '130900', '0317', 'mchzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('130981', '泊头市', '3', '130900', '0317', 'bts', null, '1');
INSERT INTO `sys_area` VALUES ('130982', '任丘市', '3', '130900', '0317', 'rqs', null, '1');
INSERT INTO `sys_area` VALUES ('130983', '黄骅市', '3', '130900', '0317', 'hhs', null, '1');
INSERT INTO `sys_area` VALUES ('130984', '河间市', '3', '130900', '0317', 'hjs', null, '1');
INSERT INTO `sys_area` VALUES ('131000', '廊坊市', '2', '130000', null, 'lfs', null, '1');
INSERT INTO `sys_area` VALUES ('131001', '市辖区', '3', '131000', '0316', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('131002', '安次区', '3', '131000', '0316', 'acq', null, '1');
INSERT INTO `sys_area` VALUES ('131003', '广阳区', '3', '131000', '0316', 'gyq', null, '1');
INSERT INTO `sys_area` VALUES ('131022', '固安县', '3', '131000', '0316', 'gax', null, '1');
INSERT INTO `sys_area` VALUES ('131023', '永清县', '3', '131000', '0316', 'yqx', null, '1');
INSERT INTO `sys_area` VALUES ('131024', '香河县', '3', '131000', '0316', 'xhx', null, '1');
INSERT INTO `sys_area` VALUES ('131025', '大城县', '3', '131000', '0316', 'dcx', null, '1');
INSERT INTO `sys_area` VALUES ('131026', '文安县', '3', '131000', '0316', 'wax', null, '1');
INSERT INTO `sys_area` VALUES ('131028', '大厂回族自治县', '3', '131000', '0316', 'dchzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('131081', '霸州市', '3', '131000', '0316', 'bzs', null, '1');
INSERT INTO `sys_area` VALUES ('131082', '三河市', '3', '131000', '0316', 'shs', null, '1');
INSERT INTO `sys_area` VALUES ('131100', '衡水市', '2', '130000', null, 'hss', null, '1');
INSERT INTO `sys_area` VALUES ('131101', '市辖区', '3', '131100', '0318', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('131102', '桃城区', '3', '131100', '0318', 'tcq', null, '1');
INSERT INTO `sys_area` VALUES ('131121', '枣强县', '3', '131100', '0318', 'zqx', null, '1');
INSERT INTO `sys_area` VALUES ('131122', '武邑县', '3', '131100', '0318', 'wyx', null, '1');
INSERT INTO `sys_area` VALUES ('131123', '武强县', '3', '131100', '0318', 'wqx', null, '1');
INSERT INTO `sys_area` VALUES ('131124', '饶阳县', '3', '131100', '0318', 'ryx', null, '1');
INSERT INTO `sys_area` VALUES ('131125', '安平县', '3', '131100', '0318', 'apx', null, '1');
INSERT INTO `sys_area` VALUES ('131126', '故城县', '3', '131100', '0318', 'gcx', null, '1');
INSERT INTO `sys_area` VALUES ('131127', '景县', '3', '131100', '0318', 'jx', null, '1');
INSERT INTO `sys_area` VALUES ('131128', '阜城县', '3', '131100', '0318', 'fcx', null, '1');
INSERT INTO `sys_area` VALUES ('131181', '冀州市', '3', '131100', '0318', 'jzs', null, '1');
INSERT INTO `sys_area` VALUES ('131182', '深州市', '3', '131100', '0318', 'szs', null, '1');
INSERT INTO `sys_area` VALUES ('140000', '山西省', '1', '0', null, 'sxs', '0', '1');
INSERT INTO `sys_area` VALUES ('140100', '太原市', '2', '140000', null, 'tys', null, '1');
INSERT INTO `sys_area` VALUES ('140101', '市辖区', '3', '140100', '0351', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('140105', '小店区', '3', '140100', '0351', 'xdq', null, '1');
INSERT INTO `sys_area` VALUES ('140106', '迎泽区', '3', '140100', '0351', 'yzq', null, '1');
INSERT INTO `sys_area` VALUES ('140107', '杏花岭区', '3', '140100', '0351', 'xhlq', null, '1');
INSERT INTO `sys_area` VALUES ('140108', '尖草坪区', '3', '140100', '0351', 'jcpq', null, '1');
INSERT INTO `sys_area` VALUES ('140109', '万柏林区', '3', '140100', '0351', 'wblq', null, '1');
INSERT INTO `sys_area` VALUES ('140110', '晋源区', '3', '140100', '0351', 'jyq', null, '1');
INSERT INTO `sys_area` VALUES ('140121', '清徐县', '3', '140100', '0351', 'qxx', null, '1');
INSERT INTO `sys_area` VALUES ('140122', '阳曲县', '3', '140100', '0351', 'yqx', null, '1');
INSERT INTO `sys_area` VALUES ('140123', '娄烦县', '3', '140100', '0351', 'lfx', null, '1');
INSERT INTO `sys_area` VALUES ('140181', '古交市', '3', '140100', '0351', 'gjs', null, '1');
INSERT INTO `sys_area` VALUES ('140200', '大同市', '2', '140000', null, 'dts', null, '1');
INSERT INTO `sys_area` VALUES ('140201', '市辖区', '3', '140200', '0352', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('140202', '城区', '3', '140200', '0352', 'cq', null, '1');
INSERT INTO `sys_area` VALUES ('140203', '矿区', '3', '140200', '0352', 'kq', null, '1');
INSERT INTO `sys_area` VALUES ('140211', '南郊区', '3', '140200', '0352', 'njq', null, '1');
INSERT INTO `sys_area` VALUES ('140212', '新荣区', '3', '140200', '0352', 'xrq', null, '1');
INSERT INTO `sys_area` VALUES ('140221', '阳高县', '3', '140200', '0352', 'ygx', null, '1');
INSERT INTO `sys_area` VALUES ('140222', '天镇县', '3', '140200', '0352', 'tzx', null, '1');
INSERT INTO `sys_area` VALUES ('140223', '广灵县', '3', '140200', '0352', 'glx', null, '1');
INSERT INTO `sys_area` VALUES ('140224', '灵丘县', '3', '140200', '0352', 'lqx', null, '1');
INSERT INTO `sys_area` VALUES ('140225', '浑源县', '3', '140200', '0352', 'hyx', null, '1');
INSERT INTO `sys_area` VALUES ('140226', '左云县', '3', '140200', '0352', 'zyx', null, '1');
INSERT INTO `sys_area` VALUES ('140227', '大同县', '3', '140200', '0352', 'dtx', null, '1');
INSERT INTO `sys_area` VALUES ('140300', '阳泉市', '2', '140000', null, 'yqs', null, '1');
INSERT INTO `sys_area` VALUES ('140301', '市辖区', '3', '140300', '0353', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('140302', '城区', '3', '140300', '0353', 'cq', null, '1');
INSERT INTO `sys_area` VALUES ('140303', '矿区', '3', '140300', '0353', 'kq', null, '1');
INSERT INTO `sys_area` VALUES ('140311', '郊区', '3', '140300', '0353', 'jq', null, '1');
INSERT INTO `sys_area` VALUES ('140321', '平定县', '3', '140300', '0353', 'pdx', null, '1');
INSERT INTO `sys_area` VALUES ('140322', '盂县', '3', '140300', '0353', 'yx', null, '1');
INSERT INTO `sys_area` VALUES ('140400', '长治市', '2', '140000', null, 'zzs', null, '1');
INSERT INTO `sys_area` VALUES ('140401', '市辖区', '3', '140400', '0355', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('140402', '城区', '3', '140400', '0355', 'cq', null, '1');
INSERT INTO `sys_area` VALUES ('140411', '郊区', '3', '140400', '0355', 'jq', null, '1');
INSERT INTO `sys_area` VALUES ('140421', '长治县', '3', '140400', '0355', 'zzx', null, '1');
INSERT INTO `sys_area` VALUES ('140423', '襄垣县', '3', '140400', '0355', 'xyx', null, '1');
INSERT INTO `sys_area` VALUES ('140424', '屯留县', '3', '140400', '0355', 'tlx', null, '1');
INSERT INTO `sys_area` VALUES ('140425', '平顺县', '3', '140400', '0355', 'psx', null, '1');
INSERT INTO `sys_area` VALUES ('140426', '黎城县', '3', '140400', '0355', 'lcx', null, '1');
INSERT INTO `sys_area` VALUES ('140427', '壶关县', '3', '140400', '0355', 'hgx', null, '1');
INSERT INTO `sys_area` VALUES ('140428', '长子县', '3', '140400', '0355', 'zzx', null, '1');
INSERT INTO `sys_area` VALUES ('140429', '武乡县', '3', '140400', '0355', 'wxx', null, '1');
INSERT INTO `sys_area` VALUES ('140430', '沁县', '3', '140400', '0355', 'qx', null, '1');
INSERT INTO `sys_area` VALUES ('140431', '沁源县', '3', '140400', '0355', 'qyx', null, '1');
INSERT INTO `sys_area` VALUES ('140481', '潞城市', '3', '140400', '0355', 'lcs', null, '1');
INSERT INTO `sys_area` VALUES ('140500', '晋城市', '2', '140000', null, 'jcs', null, '1');
INSERT INTO `sys_area` VALUES ('140501', '晋城市市辖区', '3', '140500', '0356', 'jcssxq', null, '1');
INSERT INTO `sys_area` VALUES ('140502', '城区', '3', '140500', '0356', 'cq', null, '1');
INSERT INTO `sys_area` VALUES ('140521', '沁水县', '3', '140500', '0356', 'qsx', null, '1');
INSERT INTO `sys_area` VALUES ('140522', '阳城县', '3', '140500', '0356', 'ycx', null, '1');
INSERT INTO `sys_area` VALUES ('140524', '陵川县', '3', '140500', '0356', 'lcx', null, '1');
INSERT INTO `sys_area` VALUES ('140525', '泽州县', '3', '140500', '0356', 'zzx', null, '1');
INSERT INTO `sys_area` VALUES ('140581', '高平市', '3', '140500', '0356', 'gps', null, '1');
INSERT INTO `sys_area` VALUES ('140600', '朔州市', '2', '140000', null, 'szs', null, '1');
INSERT INTO `sys_area` VALUES ('140601', '市辖区', '3', '140600', '0349', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('140602', '朔城区', '3', '140600', '0349', 'scq', null, '1');
INSERT INTO `sys_area` VALUES ('140603', '平鲁区', '3', '140600', '0349', 'plq', null, '1');
INSERT INTO `sys_area` VALUES ('140621', '山阴县', '3', '140600', '0349', 'syx', null, '1');
INSERT INTO `sys_area` VALUES ('140622', '应县', '3', '140600', '0349', 'yx', null, '1');
INSERT INTO `sys_area` VALUES ('140623', '右玉县', '3', '140600', '0349', 'yyx', null, '1');
INSERT INTO `sys_area` VALUES ('140624', '怀仁县', '3', '140600', '0349', 'hrx', null, '1');
INSERT INTO `sys_area` VALUES ('140700', '晋中市', '2', '140000', null, 'jzs', null, '1');
INSERT INTO `sys_area` VALUES ('140701', '市辖区', '3', '140700', '0354', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('140702', '榆次区', '3', '140700', '0354', 'ycq', null, '1');
INSERT INTO `sys_area` VALUES ('140721', '榆社县', '3', '140700', '0354', 'ysx', null, '1');
INSERT INTO `sys_area` VALUES ('140722', '左权县', '3', '140700', '0354', 'zqx', null, '1');
INSERT INTO `sys_area` VALUES ('140723', '和顺县', '3', '140700', '0354', 'hsx', null, '1');
INSERT INTO `sys_area` VALUES ('140724', '昔阳县', '3', '140700', '0354', 'xyx', null, '1');
INSERT INTO `sys_area` VALUES ('140725', '寿阳县', '3', '140700', '0354', 'syx', null, '1');
INSERT INTO `sys_area` VALUES ('140726', '太谷县', '3', '140700', '0354', 'tgx', null, '1');
INSERT INTO `sys_area` VALUES ('140727', '祁县', '3', '140700', '0354', 'qx', null, '1');
INSERT INTO `sys_area` VALUES ('140728', '平遥县', '3', '140700', '0354', 'pyx', null, '1');
INSERT INTO `sys_area` VALUES ('140729', '灵石县', '3', '140700', '0354', 'lsx', null, '1');
INSERT INTO `sys_area` VALUES ('140781', '介休市', '3', '140700', '0354', 'jxs', null, '1');
INSERT INTO `sys_area` VALUES ('140800', '运城市', '2', '140000', null, 'ycs', null, '1');
INSERT INTO `sys_area` VALUES ('140801', '市辖区', '3', '140800', '0359', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('140802', '盐湖区', '3', '140800', '0359', 'yhq', null, '1');
INSERT INTO `sys_area` VALUES ('140821', '临猗县', '3', '140800', '0359', 'lyx', null, '1');
INSERT INTO `sys_area` VALUES ('140822', '万荣县', '3', '140800', '0359', 'wrx', null, '1');
INSERT INTO `sys_area` VALUES ('140823', '闻喜县', '3', '140800', '0359', 'wxx', null, '1');
INSERT INTO `sys_area` VALUES ('140824', '稷山县', '3', '140800', '0359', 'jsx', null, '1');
INSERT INTO `sys_area` VALUES ('140825', '新绛县', '3', '140800', '0359', 'xjx', null, '1');
INSERT INTO `sys_area` VALUES ('140826', '绛县', '3', '140800', '0359', 'jx', null, '1');
INSERT INTO `sys_area` VALUES ('140827', '垣曲县', '3', '140800', '0359', 'yqx', null, '1');
INSERT INTO `sys_area` VALUES ('140828', '夏县', '3', '140800', '0359', 'xx', null, '1');
INSERT INTO `sys_area` VALUES ('140829', '平陆县', '3', '140800', '0359', 'plx', null, '1');
INSERT INTO `sys_area` VALUES ('140830', '芮城县', '3', '140800', '0359', 'rcx', null, '1');
INSERT INTO `sys_area` VALUES ('140881', '永济市', '3', '140800', '0359', 'yjs', null, '1');
INSERT INTO `sys_area` VALUES ('140882', '河津市', '3', '140800', '0359', 'hjs', null, '1');
INSERT INTO `sys_area` VALUES ('140900', '忻州市', '2', '140000', null, 'xzs', null, '1');
INSERT INTO `sys_area` VALUES ('140901', '市辖区', '3', '140900', '0350', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('140902', '忻府区', '3', '140900', '0350', 'xfq', null, '1');
INSERT INTO `sys_area` VALUES ('140921', '定襄县', '3', '140900', '0350', 'dxx', null, '1');
INSERT INTO `sys_area` VALUES ('140922', '五台县', '3', '140900', '0350', 'wtx', null, '1');
INSERT INTO `sys_area` VALUES ('140923', '代县', '3', '140900', '0350', 'dx', null, '1');
INSERT INTO `sys_area` VALUES ('140924', '繁峙县', '3', '140900', '0350', 'fzx', null, '1');
INSERT INTO `sys_area` VALUES ('140925', '宁武县', '3', '140900', '0350', 'nwx', null, '1');
INSERT INTO `sys_area` VALUES ('140926', '静乐县', '3', '140900', '0350', 'jlx', null, '1');
INSERT INTO `sys_area` VALUES ('140927', '神池县', '3', '140900', '0350', 'scx', null, '1');
INSERT INTO `sys_area` VALUES ('140928', '五寨县', '3', '140900', '0350', 'wzx', null, '1');
INSERT INTO `sys_area` VALUES ('140929', '岢岚县', '3', '140900', '0350', 'klx', null, '1');
INSERT INTO `sys_area` VALUES ('140930', '河曲县', '3', '140900', '0350', 'hqx', null, '1');
INSERT INTO `sys_area` VALUES ('140931', '保德县', '3', '140900', '0350', 'bdx', null, '1');
INSERT INTO `sys_area` VALUES ('140932', '偏关县', '3', '140900', '0350', 'pgx', null, '1');
INSERT INTO `sys_area` VALUES ('140981', '原平市', '3', '140900', '0350', 'yps', null, '1');
INSERT INTO `sys_area` VALUES ('141000', '临汾市', '2', '140000', null, 'lfs', null, '1');
INSERT INTO `sys_area` VALUES ('141001', '市辖区', '3', '141000', '0350', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('141002', '尧都区', '3', '141000', '0357', 'ydq', null, '1');
INSERT INTO `sys_area` VALUES ('141021', '曲沃县', '3', '141000', '0357', 'qwx', null, '1');
INSERT INTO `sys_area` VALUES ('141022', '翼城县', '3', '141000', '0357', 'ycx', null, '1');
INSERT INTO `sys_area` VALUES ('141023', '襄汾县', '3', '141000', '0357', 'xfx', null, '1');
INSERT INTO `sys_area` VALUES ('141024', '洪洞县', '3', '141000', '0357', 'hdx', null, '1');
INSERT INTO `sys_area` VALUES ('141025', '古县', '3', '141000', '0357', 'gx', null, '1');
INSERT INTO `sys_area` VALUES ('141026', '安泽县', '3', '141000', '0357', 'azx', null, '1');
INSERT INTO `sys_area` VALUES ('141027', '浮山县', '3', '141000', '0357', 'fsx', null, '1');
INSERT INTO `sys_area` VALUES ('141028', '吉县', '3', '141000', '0357', 'jx', null, '1');
INSERT INTO `sys_area` VALUES ('141029', '乡宁县', '3', '141000', '0357', 'xnx', null, '1');
INSERT INTO `sys_area` VALUES ('141030', '大宁县', '3', '141000', '0357', 'dnx', null, '1');
INSERT INTO `sys_area` VALUES ('141031', '隰县', '3', '141000', '0357', 'xx', null, '1');
INSERT INTO `sys_area` VALUES ('141032', '永和县', '3', '141000', '0357', 'yhx', null, '1');
INSERT INTO `sys_area` VALUES ('141033', '蒲县', '3', '141000', '0357', 'px', null, '1');
INSERT INTO `sys_area` VALUES ('141034', '汾西县', '3', '141000', '0357', 'fxx', null, '1');
INSERT INTO `sys_area` VALUES ('141081', '侯马市', '3', '141000', '0357', 'hms', null, '1');
INSERT INTO `sys_area` VALUES ('141082', '霍州市', '3', '141000', '0357', 'hzs', null, '1');
INSERT INTO `sys_area` VALUES ('141100', '吕梁市', '2', '140000', null, 'lls', null, '1');
INSERT INTO `sys_area` VALUES ('141101', '市辖区', '3', '141100', '0358', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('141102', '离石区', '3', '141100', '0358', 'lsq', null, '1');
INSERT INTO `sys_area` VALUES ('141121', '文水县', '3', '141100', '0358', 'wsx', null, '1');
INSERT INTO `sys_area` VALUES ('141122', '交城县', '3', '141100', '0358', 'jcx', null, '1');
INSERT INTO `sys_area` VALUES ('141123', '兴县', '3', '141100', '0358', 'xx', null, '1');
INSERT INTO `sys_area` VALUES ('141124', '临县', '3', '141100', '0358', 'lx', null, '1');
INSERT INTO `sys_area` VALUES ('141125', '柳林县', '3', '141100', '0358', 'llx', null, '1');
INSERT INTO `sys_area` VALUES ('141126', '石楼县', '3', '141100', '0358', 'slx', null, '1');
INSERT INTO `sys_area` VALUES ('141127', '岚县', '3', '141100', '0358', 'lx', null, '1');
INSERT INTO `sys_area` VALUES ('141128', '方山县', '3', '141100', '0358', 'fsx', null, '1');
INSERT INTO `sys_area` VALUES ('141129', '中阳县', '3', '141100', '0358', 'zyx', null, '1');
INSERT INTO `sys_area` VALUES ('141130', '交口县', '3', '141100', '0358', 'jkx', null, '1');
INSERT INTO `sys_area` VALUES ('141181', '孝义市', '3', '141100', '0358', 'xys', null, '1');
INSERT INTO `sys_area` VALUES ('141182', '汾阳市', '3', '141100', '0358', 'fys', null, '1');
INSERT INTO `sys_area` VALUES ('150000', '内蒙古自治区', '1', '0', null, 'nmgzzq', null, '1');
INSERT INTO `sys_area` VALUES ('150100', '呼和浩特市', '2', '150000', null, 'hhhts', null, '1');
INSERT INTO `sys_area` VALUES ('150101', '市辖区', '3', '150100', '0471', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('150102', '新城区', '3', '150100', '0471', 'xcq', null, '1');
INSERT INTO `sys_area` VALUES ('150103', '回民区', '3', '150100', '0471', 'hmq', null, '1');
INSERT INTO `sys_area` VALUES ('150104', '玉泉区', '3', '150100', '0471', 'yqq', null, '1');
INSERT INTO `sys_area` VALUES ('150105', '赛罕区', '3', '150100', '0471', 'shq', null, '1');
INSERT INTO `sys_area` VALUES ('150121', '土默特左旗', '3', '150100', '0471', 'tmtzq', null, '1');
INSERT INTO `sys_area` VALUES ('150122', '托克托县', '3', '150100', '0471', 'tktx', null, '1');
INSERT INTO `sys_area` VALUES ('150123', '和林格尔县', '3', '150100', '0471', 'hlgex', null, '1');
INSERT INTO `sys_area` VALUES ('150124', '清水河县', '3', '150100', '0471', 'qshx', null, '1');
INSERT INTO `sys_area` VALUES ('150125', '武川县', '3', '150100', '0471', 'wcx', null, '1');
INSERT INTO `sys_area` VALUES ('150200', '包头市', '2', '150000', null, 'bts', null, '1');
INSERT INTO `sys_area` VALUES ('150201', '市辖区', '3', '150200', '0472', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('150202', '东河区', '3', '150200', '0472', 'dhq', null, '1');
INSERT INTO `sys_area` VALUES ('150203', '昆都仑区', '3', '150200', '0472', 'kdlq', null, '1');
INSERT INTO `sys_area` VALUES ('150204', '青山区', '3', '150200', '0472', 'qsq', null, '1');
INSERT INTO `sys_area` VALUES ('150205', '石拐区', '3', '150200', '0472', 'sgq', null, '1');
INSERT INTO `sys_area` VALUES ('150206', '白云鄂博矿区', '3', '150200', '0472', 'byebkq', null, '1');
INSERT INTO `sys_area` VALUES ('150207', '九原区', '3', '150200', '0472', 'jyq', null, '1');
INSERT INTO `sys_area` VALUES ('150221', '土默特右旗', '3', '150200', '0472', 'tmtyq', null, '1');
INSERT INTO `sys_area` VALUES ('150222', '固阳县', '3', '150200', '0472', 'gyx', null, '1');
INSERT INTO `sys_area` VALUES ('150223', '达尔罕茂明安联合旗', '3', '150200', '0472', 'dehmmalhq', null, '1');
INSERT INTO `sys_area` VALUES ('150300', '乌海市', '2', '150000', null, 'whs', null, '1');
INSERT INTO `sys_area` VALUES ('150301', '市辖区', '3', '150300', '0473', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('150302', '海勃湾区', '3', '150300', '0473', 'hbwq', null, '1');
INSERT INTO `sys_area` VALUES ('150303', '海南区', '3', '150300', '0473', 'hnq', null, '1');
INSERT INTO `sys_area` VALUES ('150304', '乌达区', '3', '150300', '0473', 'wdq', null, '1');
INSERT INTO `sys_area` VALUES ('150400', '赤峰市', '2', '150000', null, 'cfs', null, '1');
INSERT INTO `sys_area` VALUES ('150401', '市辖区', '3', '150400', '0476', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('150402', '红山区', '3', '150400', '0476', 'hsq', null, '1');
INSERT INTO `sys_area` VALUES ('150403', '元宝山区', '3', '150400', '0476', 'ybsq', null, '1');
INSERT INTO `sys_area` VALUES ('150404', '松山区', '3', '150400', '0476', 'ssq', null, '1');
INSERT INTO `sys_area` VALUES ('150421', '阿鲁科尔沁旗', '3', '150400', '0476', 'alkeqq', null, '1');
INSERT INTO `sys_area` VALUES ('150422', '巴林左旗', '3', '150400', '0476', 'blzq', null, '1');
INSERT INTO `sys_area` VALUES ('150423', '巴林右旗', '3', '150400', '0476', 'blyq', null, '1');
INSERT INTO `sys_area` VALUES ('150424', '林西县', '3', '150400', '0476', 'lxx', null, '1');
INSERT INTO `sys_area` VALUES ('150425', '克什克腾旗', '3', '150400', '0476', 'ksktq', null, '1');
INSERT INTO `sys_area` VALUES ('150426', '翁牛特旗', '3', '150400', '0476', 'wntq', null, '1');
INSERT INTO `sys_area` VALUES ('150428', '喀喇沁旗', '3', '150400', '0476', 'klqq', null, '1');
INSERT INTO `sys_area` VALUES ('150429', '宁城县', '3', '150400', '0476', 'ncx', null, '1');
INSERT INTO `sys_area` VALUES ('150430', '敖汉旗', '3', '150400', '0476', 'ahq', null, '1');
INSERT INTO `sys_area` VALUES ('150500', '通辽市', '2', '150000', null, 'tls', null, '1');
INSERT INTO `sys_area` VALUES ('150501', '市辖区', '3', '150500', '0475', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('150502', '科尔沁区', '3', '150500', '0475', 'keqq', null, '1');
INSERT INTO `sys_area` VALUES ('150521', '科尔沁左翼中旗', '3', '150500', '0475', 'keqzyzq', null, '1');
INSERT INTO `sys_area` VALUES ('150522', '科尔沁左翼后旗', '3', '150500', '0475', 'keqzyhq', null, '1');
INSERT INTO `sys_area` VALUES ('150523', '开鲁县', '3', '150500', '0475', 'klx', null, '1');
INSERT INTO `sys_area` VALUES ('150524', '库伦旗', '3', '150500', '0475', 'klq', null, '1');
INSERT INTO `sys_area` VALUES ('150525', '奈曼旗', '3', '150500', '0475', 'nmq', null, '1');
INSERT INTO `sys_area` VALUES ('150526', '扎鲁特旗', '3', '150500', '0475', 'zltq', null, '1');
INSERT INTO `sys_area` VALUES ('150581', '霍林郭勒市', '3', '150500', '0475', 'hlgls', null, '1');
INSERT INTO `sys_area` VALUES ('150600', '鄂尔多斯市', '2', '150000', null, 'eedss', null, '1');
INSERT INTO `sys_area` VALUES ('150601', '市辖区', '3', '150600', null, 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('150602', '东胜区', '3', '150600', '0477', 'dsq', null, '1');
INSERT INTO `sys_area` VALUES ('150621', '达拉特旗', '3', '150600', '0477', 'dltq', null, '1');
INSERT INTO `sys_area` VALUES ('150622', '准格尔旗', '3', '150600', '0477', 'zgeq', null, '1');
INSERT INTO `sys_area` VALUES ('150623', '鄂托克前旗', '3', '150600', '0477', 'etkqq', null, '1');
INSERT INTO `sys_area` VALUES ('150624', '鄂托克旗', '3', '150600', '0477', 'etkq', null, '1');
INSERT INTO `sys_area` VALUES ('150625', '杭锦旗', '3', '150600', '0477', 'hjq', null, '1');
INSERT INTO `sys_area` VALUES ('150626', '乌审旗', '3', '150600', '0477', 'wsq', null, '1');
INSERT INTO `sys_area` VALUES ('150627', '伊金霍洛旗', '3', '150600', '0477', 'yjhlq', null, '1');
INSERT INTO `sys_area` VALUES ('150700', '呼伦贝尔市', '2', '150000', null, 'hlbes', null, '1');
INSERT INTO `sys_area` VALUES ('150701', '市辖区', '3', '150700', '0470', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('150702', '海拉尔区', '3', '150700', '0470', 'hleq', null, '1');
INSERT INTO `sys_area` VALUES ('150721', '阿荣旗', '3', '150700', '0470', 'arq', null, '1');
INSERT INTO `sys_area` VALUES ('150722', '莫力达瓦达斡尔族自治旗', '3', '150700', '0470', 'mldwdwezzzq', null, '1');
INSERT INTO `sys_area` VALUES ('150723', '鄂伦春自治旗', '3', '150700', '0470', 'elczzq', null, '1');
INSERT INTO `sys_area` VALUES ('150724', '鄂温克族自治旗', '3', '150700', '0470', 'ewkzzzq', null, '1');
INSERT INTO `sys_area` VALUES ('150725', '陈巴尔虎旗', '3', '150700', '0470', 'cbehq', null, '1');
INSERT INTO `sys_area` VALUES ('150726', '新巴尔虎左旗', '3', '150700', '0470', 'xbehzq', null, '1');
INSERT INTO `sys_area` VALUES ('150727', '新巴尔虎右旗', '3', '150700', '0470', 'xbehyq', null, '1');
INSERT INTO `sys_area` VALUES ('150781', '满洲里市', '3', '150700', '0470', 'mzls', null, '1');
INSERT INTO `sys_area` VALUES ('150782', '牙克石市', '3', '150700', '0470', 'ykss', null, '1');
INSERT INTO `sys_area` VALUES ('150783', '扎兰屯市', '3', '150700', '0470', 'zlts', null, '1');
INSERT INTO `sys_area` VALUES ('150784', '额尔古纳市', '3', '150700', '0470', 'eegns', null, '1');
INSERT INTO `sys_area` VALUES ('150785', '根河市', '3', '150700', '0470', 'ghs', null, '1');
INSERT INTO `sys_area` VALUES ('150800', '巴彦淖尔市', '2', '150000', null, 'bynes', null, '1');
INSERT INTO `sys_area` VALUES ('150801', '市辖区', '3', '150800', '0478', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('150802', '临河区', '3', '150800', '0478', 'lhq', null, '1');
INSERT INTO `sys_area` VALUES ('150821', '五原县', '3', '150800', '0478', 'wyx', null, '1');
INSERT INTO `sys_area` VALUES ('150822', '磴口县', '3', '150800', '0478', 'dkx', null, '1');
INSERT INTO `sys_area` VALUES ('150823', '乌拉特前旗', '3', '150800', '0478', 'wltqq', null, '1');
INSERT INTO `sys_area` VALUES ('150824', '乌拉特中旗', '3', '150800', '0478', 'wltzq', null, '1');
INSERT INTO `sys_area` VALUES ('150825', '乌拉特后旗', '3', '150800', '0478', 'wlthq', null, '1');
INSERT INTO `sys_area` VALUES ('150826', '杭锦后旗', '3', '150800', '0478', 'hjhq', null, '1');
INSERT INTO `sys_area` VALUES ('150900', '乌兰察布市', '2', '150000', null, 'wlcbs', null, '1');
INSERT INTO `sys_area` VALUES ('150901', '市辖区', '3', '150900', '0474', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('150902', '集宁区', '3', '150900', '0474', 'jnq', null, '1');
INSERT INTO `sys_area` VALUES ('150921', '卓资县', '3', '150900', '0474', 'zzx', null, '1');
INSERT INTO `sys_area` VALUES ('150922', '化德县', '3', '150900', '0474', 'hdx', null, '1');
INSERT INTO `sys_area` VALUES ('150923', '商都县', '3', '150900', '0474', 'sdx', null, '1');
INSERT INTO `sys_area` VALUES ('150924', '兴和县', '3', '150900', '0474', 'xhx', null, '1');
INSERT INTO `sys_area` VALUES ('150925', '凉城县', '3', '150900', '0474', 'lcx', null, '1');
INSERT INTO `sys_area` VALUES ('150926', '察哈尔右翼前旗', '3', '150900', '0474', 'cheyyqq', null, '1');
INSERT INTO `sys_area` VALUES ('150927', '察哈尔右翼中旗', '3', '150900', '0474', 'cheyyzq', null, '1');
INSERT INTO `sys_area` VALUES ('150928', '察哈尔右翼后旗', '3', '150900', '0474', 'cheyyhq', null, '1');
INSERT INTO `sys_area` VALUES ('150929', '四子王旗', '3', '150900', '0474', 'szwq', null, '1');
INSERT INTO `sys_area` VALUES ('150981', '丰镇市', '3', '150900', '0474', 'fzs', null, '1');
INSERT INTO `sys_area` VALUES ('152200', '兴安盟', '2', '150000', null, 'xam', null, '1');
INSERT INTO `sys_area` VALUES ('152201', '乌兰浩特市', '3', '152200', '0482', 'wlhts', null, '1');
INSERT INTO `sys_area` VALUES ('152202', '阿尔山市', '3', '152200', '0482', 'aess', null, '1');
INSERT INTO `sys_area` VALUES ('152221', '科尔沁右翼前旗', '3', '152200', '0482', 'keqyyqq', null, '1');
INSERT INTO `sys_area` VALUES ('152222', '科尔沁右翼中旗', '3', '152200', '0482', 'keqyyzq', null, '1');
INSERT INTO `sys_area` VALUES ('152223', '扎赉特旗', '3', '152200', '0482', 'zltq', null, '1');
INSERT INTO `sys_area` VALUES ('152224', '突泉县', '3', '152200', '0482', 'tqx', null, '1');
INSERT INTO `sys_area` VALUES ('152500', '锡林郭勒盟', '2', '150000', null, 'xlglm', null, '1');
INSERT INTO `sys_area` VALUES ('152501', '二连浩特市', '3', '152500', '0479', 'elhts', null, '1');
INSERT INTO `sys_area` VALUES ('152502', '锡林浩特市', '3', '152500', '0479', 'xlhts', null, '1');
INSERT INTO `sys_area` VALUES ('152522', '阿巴嘎旗', '3', '152500', '0479', 'abgq', null, '1');
INSERT INTO `sys_area` VALUES ('152523', '苏尼特左旗', '3', '152500', '0479', 'sntzq', null, '1');
INSERT INTO `sys_area` VALUES ('152524', '苏尼特右旗', '3', '152500', '0479', 'sntyq', null, '1');
INSERT INTO `sys_area` VALUES ('152525', '东乌珠穆沁旗', '3', '152500', '0479', 'dwzmqq', null, '1');
INSERT INTO `sys_area` VALUES ('152526', '西乌珠穆沁旗', '3', '152500', '0479', 'xwzmqq', null, '1');
INSERT INTO `sys_area` VALUES ('152527', '太仆寺旗', '3', '152500', '0479', 'tpsq', null, '1');
INSERT INTO `sys_area` VALUES ('152528', '镶黄旗', '3', '152500', '0479', 'xhq', null, '1');
INSERT INTO `sys_area` VALUES ('152529', '正镶白旗', '3', '152500', '0479', 'zxbq', null, '1');
INSERT INTO `sys_area` VALUES ('152530', '正蓝旗', '3', '152500', '0479', 'zlq', null, '1');
INSERT INTO `sys_area` VALUES ('152531', '多伦县', '3', '152500', '0479', 'dlx', null, '1');
INSERT INTO `sys_area` VALUES ('152900', '阿拉善盟', '2', '150000', null, 'alsm', null, '1');
INSERT INTO `sys_area` VALUES ('152921', '阿拉善左旗', '3', '152900', '0483', 'alszq', null, '1');
INSERT INTO `sys_area` VALUES ('152922', '阿拉善右旗', '3', '152900', '0483', 'alsyq', null, '1');
INSERT INTO `sys_area` VALUES ('152923', '额济纳旗', '3', '152900', '0483', 'ejnq', null, '1');
INSERT INTO `sys_area` VALUES ('210000', '辽宁省', '1', '0', null, 'lns', null, '1');
INSERT INTO `sys_area` VALUES ('210100', '沈阳市', '2', '210000', null, 'sys', null, '1');
INSERT INTO `sys_area` VALUES ('210101', '市辖区', '3', '210100', '024', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('210102', '和平区', '3', '210100', '024', 'hpq', null, '1');
INSERT INTO `sys_area` VALUES ('210103', '沈河区', '3', '210100', '024', 'shq', null, '1');
INSERT INTO `sys_area` VALUES ('210104', '大东区', '3', '210100', '024', 'ddq', null, '1');
INSERT INTO `sys_area` VALUES ('210105', '皇姑区', '3', '210100', '024', 'hgq', null, '1');
INSERT INTO `sys_area` VALUES ('210106', '铁西区', '3', '210100', '024', 'txq', null, '1');
INSERT INTO `sys_area` VALUES ('210111', '苏家屯区', '3', '210100', '024', 'sjtq', null, '1');
INSERT INTO `sys_area` VALUES ('210112', '东陵区', '3', '210100', '024', 'dlq', null, '1');
INSERT INTO `sys_area` VALUES ('210113', '沈北新区', '3', '210100', '024', 'sbxq', null, '1');
INSERT INTO `sys_area` VALUES ('210114', '于洪区', '3', '210100', '024', 'yhq', null, '1');
INSERT INTO `sys_area` VALUES ('210122', '辽中县', '3', '210100', '024', 'lzx', null, '1');
INSERT INTO `sys_area` VALUES ('210123', '康平县', '3', '210100', '024', 'kpx', null, '1');
INSERT INTO `sys_area` VALUES ('210124', '法库县', '3', '210100', '024', 'fkx', null, '1');
INSERT INTO `sys_area` VALUES ('210181', '新民市', '3', '210100', '024', 'xms', null, '1');
INSERT INTO `sys_area` VALUES ('210200', '大连市', '2', '210000', null, 'dls', null, '1');
INSERT INTO `sys_area` VALUES ('210201', '市辖区', '3', '210200', '0411', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('210202', '中山区', '3', '210200', '0411', 'zsq', null, '1');
INSERT INTO `sys_area` VALUES ('210203', '西岗区', '3', '210200', '0411', 'xgq', null, '1');
INSERT INTO `sys_area` VALUES ('210204', '沙河口区', '3', '210200', '0411', 'shkq', null, '1');
INSERT INTO `sys_area` VALUES ('210211', '甘井子区', '3', '210200', '0411', 'gjzq', null, '1');
INSERT INTO `sys_area` VALUES ('210212', '旅顺口区', '3', '210200', '0411', 'lskq', null, '1');
INSERT INTO `sys_area` VALUES ('210213', '金州区', '3', '210200', '0411', 'jzq', null, '1');
INSERT INTO `sys_area` VALUES ('210224', '长海县', '3', '210200', '0411', 'zhx', null, '1');
INSERT INTO `sys_area` VALUES ('210281', '瓦房店市', '3', '210200', '0411', 'wfds', null, '1');
INSERT INTO `sys_area` VALUES ('210282', '普兰店市', '3', '210200', '0411', 'plds', null, '1');
INSERT INTO `sys_area` VALUES ('210283', '庄河市', '3', '210200', '0411', 'zhs', null, '1');
INSERT INTO `sys_area` VALUES ('210300', '鞍山市', '2', '210000', null, 'ass', null, '1');
INSERT INTO `sys_area` VALUES ('210301', '市辖区', '3', '210300', '0412', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('210302', '铁东区', '3', '210300', '0412', 'tdq', null, '1');
INSERT INTO `sys_area` VALUES ('210303', '铁西区', '3', '210300', '0412', 'txq', null, '1');
INSERT INTO `sys_area` VALUES ('210304', '立山区', '3', '210300', '0412', 'lsq', null, '1');
INSERT INTO `sys_area` VALUES ('210311', '千山区', '3', '210300', '0412', 'qsq', null, '1');
INSERT INTO `sys_area` VALUES ('210321', '台安县', '3', '210300', '0412', 'tax', null, '1');
INSERT INTO `sys_area` VALUES ('210323', '岫岩满族自治县', '3', '210300', '0412', 'xymzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('210381', '海城市', '3', '210300', '0412', 'hcs', null, '1');
INSERT INTO `sys_area` VALUES ('210400', '抚顺市', '2', '210000', null, 'fss', null, '1');
INSERT INTO `sys_area` VALUES ('210401', '市辖区', '3', '210400', '0413', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('210402', '新抚区', '3', '210400', '0413', 'xfq', null, '1');
INSERT INTO `sys_area` VALUES ('210403', '东洲区', '3', '210400', '0413', 'dzq', null, '1');
INSERT INTO `sys_area` VALUES ('210404', '望花区', '3', '210400', '0413', 'whq', null, '1');
INSERT INTO `sys_area` VALUES ('210411', '顺城区', '3', '210400', '0413', 'scq', null, '1');
INSERT INTO `sys_area` VALUES ('210421', '抚顺县', '3', '210400', '0413', 'fsx', null, '1');
INSERT INTO `sys_area` VALUES ('210422', '新宾满族自治县', '3', '210400', '0413', 'xbmzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('210423', '清原满族自治县', '3', '210400', '0413', 'qymzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('210500', '本溪市', '2', '210000', null, 'bxs', null, '1');
INSERT INTO `sys_area` VALUES ('210501', '市辖区', '3', '210500', '0414', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('210502', '平山区', '3', '210500', '0414', 'psq', null, '1');
INSERT INTO `sys_area` VALUES ('210503', '溪湖区', '3', '210500', '0414', 'xhq', null, '1');
INSERT INTO `sys_area` VALUES ('210504', '明山区', '3', '210500', '0414', 'msq', null, '1');
INSERT INTO `sys_area` VALUES ('210505', '南芬区', '3', '210500', '0414', 'nfq', null, '1');
INSERT INTO `sys_area` VALUES ('210521', '本溪满族自治县', '3', '210500', '0414', 'bxmzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('210522', '桓仁满族自治县', '3', '210500', '0414', 'hrmzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('210600', '丹东市', '2', '210000', null, 'dds', null, '1');
INSERT INTO `sys_area` VALUES ('210601', '市辖区', '3', '210600', '0415', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('210602', '元宝区', '3', '210600', '0415', 'ybq', null, '1');
INSERT INTO `sys_area` VALUES ('210603', '振兴区', '3', '210600', '0415', 'zxq', null, '1');
INSERT INTO `sys_area` VALUES ('210604', '振安区', '3', '210600', '0415', 'zaq', null, '1');
INSERT INTO `sys_area` VALUES ('210624', '宽甸满族自治县', '3', '210600', '0415', 'kdmzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('210681', '东港市', '3', '210600', '0415', 'dgs', null, '1');
INSERT INTO `sys_area` VALUES ('210682', '凤城市', '3', '210600', '0415', 'fcs', null, '1');
INSERT INTO `sys_area` VALUES ('210700', '锦州市', '2', '210000', null, 'jzs', null, '1');
INSERT INTO `sys_area` VALUES ('210701', '市辖区', '3', '210700', '0416', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('210702', '古塔区', '3', '210700', '0416', 'gtq', null, '1');
INSERT INTO `sys_area` VALUES ('210703', '凌河区', '3', '210700', '0416', 'lhq', null, '1');
INSERT INTO `sys_area` VALUES ('210711', '太和区', '3', '210700', '0416', 'thq', null, '1');
INSERT INTO `sys_area` VALUES ('210726', '黑山县', '3', '210700', '0416', 'hsx', null, '1');
INSERT INTO `sys_area` VALUES ('210727', '义县', '3', '210700', '0416', 'yx', null, '1');
INSERT INTO `sys_area` VALUES ('210781', '凌海市', '3', '210700', '0416', 'lhs', null, '1');
INSERT INTO `sys_area` VALUES ('210782', '北镇市', '3', '210700', '0416', 'bzs', null, '1');
INSERT INTO `sys_area` VALUES ('210800', '营口市', '2', '210000', null, 'yks', null, '1');
INSERT INTO `sys_area` VALUES ('210801', '市辖区', '3', '210800', '0417', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('210802', '站前区', '3', '210800', '0417', 'zqq', null, '1');
INSERT INTO `sys_area` VALUES ('210803', '西市区', '3', '210800', '0417', 'xsq', null, '1');
INSERT INTO `sys_area` VALUES ('210804', '鲅鱼圈区', '3', '210800', '0417', 'byqq', null, '1');
INSERT INTO `sys_area` VALUES ('210811', '老边区', '3', '210800', '0417', 'lbq', null, '1');
INSERT INTO `sys_area` VALUES ('210881', '盖州市', '3', '210800', '0417', 'gzs', null, '1');
INSERT INTO `sys_area` VALUES ('210882', '大石桥市', '3', '210800', '0417', 'dsqs', null, '1');
INSERT INTO `sys_area` VALUES ('210900', '阜新市', '2', '210000', null, 'fxs', null, '1');
INSERT INTO `sys_area` VALUES ('210901', '市辖区', '3', '210900', '0418', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('210902', '海州区', '3', '210900', '0418', 'hzq', null, '1');
INSERT INTO `sys_area` VALUES ('210903', '新邱区', '3', '210900', '0418', 'xqq', null, '1');
INSERT INTO `sys_area` VALUES ('210904', '太平区', '3', '210900', '0418', 'tpq', null, '1');
INSERT INTO `sys_area` VALUES ('210905', '清河门区', '3', '210900', '0418', 'qhmq', null, '1');
INSERT INTO `sys_area` VALUES ('210911', '细河区', '3', '210900', '0418', 'xhq', null, '1');
INSERT INTO `sys_area` VALUES ('210921', '阜新蒙古族自治县', '3', '210900', '0418', 'fxmgzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('210922', '彰武县', '3', '210900', '0418', 'zwx', null, '1');
INSERT INTO `sys_area` VALUES ('211000', '辽阳市', '2', '210000', null, 'lys', null, '1');
INSERT INTO `sys_area` VALUES ('211001', '市辖区', '3', '211000', '0419', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('211002', '白塔区', '3', '211000', '0419', 'btq', null, '1');
INSERT INTO `sys_area` VALUES ('211003', '文圣区', '3', '211000', '0419', 'wsq', null, '1');
INSERT INTO `sys_area` VALUES ('211004', '宏伟区', '3', '211000', '0419', 'hwq', null, '1');
INSERT INTO `sys_area` VALUES ('211005', '弓长岭区', '3', '211000', '0419', 'gzlq', null, '1');
INSERT INTO `sys_area` VALUES ('211011', '太子河区', '3', '211000', '0419', 'tzhq', null, '1');
INSERT INTO `sys_area` VALUES ('211021', '辽阳县', '3', '211000', '0419', 'lyx', null, '1');
INSERT INTO `sys_area` VALUES ('211081', '灯塔市', '3', '211000', '0419', 'dts', null, '1');
INSERT INTO `sys_area` VALUES ('211100', '盘锦市', '2', '210000', null, 'pjs', null, '1');
INSERT INTO `sys_area` VALUES ('211101', '市辖区', '3', '211100', '0427', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('211102', '双台子区', '3', '211100', '0427', 'stzq', null, '1');
INSERT INTO `sys_area` VALUES ('211103', '兴隆台区', '3', '211100', '0427', 'xltq', null, '1');
INSERT INTO `sys_area` VALUES ('211121', '大洼县', '3', '211100', '0427', 'dwx', null, '1');
INSERT INTO `sys_area` VALUES ('211122', '盘山县', '3', '211100', '0427', 'psx', null, '1');
INSERT INTO `sys_area` VALUES ('211200', '铁岭市', '2', '210000', null, 'tls', null, '1');
INSERT INTO `sys_area` VALUES ('211201', '市辖区', '3', '211200', '0410', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('211202', '银州区', '3', '211200', '0410', 'yzq', null, '1');
INSERT INTO `sys_area` VALUES ('211204', '清河区', '3', '211200', '0410', 'qhq', null, '1');
INSERT INTO `sys_area` VALUES ('211221', '铁岭县', '3', '211200', '0410', 'tlx', null, '1');
INSERT INTO `sys_area` VALUES ('211223', '西丰县', '3', '211200', '0410', 'xfx', null, '1');
INSERT INTO `sys_area` VALUES ('211224', '昌图县', '3', '211200', '0410', 'ctx', null, '1');
INSERT INTO `sys_area` VALUES ('211281', '调兵山市', '3', '211200', '0410', 'dbss', null, '1');
INSERT INTO `sys_area` VALUES ('211282', '开原市', '3', '211200', '0410', 'kys', null, '1');
INSERT INTO `sys_area` VALUES ('211300', '朝阳市', '2', '210000', null, 'cys', null, '1');
INSERT INTO `sys_area` VALUES ('211301', '市辖区', '3', '211300', '0421', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('211302', '双塔区', '3', '211300', '0421', 'stq', null, '1');
INSERT INTO `sys_area` VALUES ('211303', '龙城区', '3', '211300', '0421', 'lcq', null, '1');
INSERT INTO `sys_area` VALUES ('211321', '朝阳县', '3', '211300', '0421', 'cyx', null, '1');
INSERT INTO `sys_area` VALUES ('211322', '建平县', '3', '211300', '0421', 'jpx', null, '1');
INSERT INTO `sys_area` VALUES ('211324', '喀喇沁左翼蒙古族自治县', '3', '211300', '0421', 'klqzymgzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('211381', '北票市', '3', '211300', '0421', 'bps', null, '1');
INSERT INTO `sys_area` VALUES ('211382', '凌源市', '3', '211300', '0421', 'lys', null, '1');
INSERT INTO `sys_area` VALUES ('211400', '葫芦岛市', '2', '210000', null, 'hlds', null, '1');
INSERT INTO `sys_area` VALUES ('211401', '市辖区', '3', '211400', '0429', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('211402', '连山区', '3', '211400', '0429', 'lsq', null, '1');
INSERT INTO `sys_area` VALUES ('211403', '龙港区', '3', '211400', '0429', 'lgq', null, '1');
INSERT INTO `sys_area` VALUES ('211404', '南票区', '3', '211400', '0429', 'npq', null, '1');
INSERT INTO `sys_area` VALUES ('211421', '绥中县', '3', '211400', '0429', 'szx', null, '1');
INSERT INTO `sys_area` VALUES ('211422', '建昌县', '3', '211400', '0429', 'jcx', null, '1');
INSERT INTO `sys_area` VALUES ('211481', '兴城市', '3', '211400', '0429', 'xcs', null, '1');
INSERT INTO `sys_area` VALUES ('220000', '吉林省', '1', '0', null, 'jls', null, '1');
INSERT INTO `sys_area` VALUES ('220100', '长春市', '2', '220000', null, 'zcs', null, '1');
INSERT INTO `sys_area` VALUES ('220101', '市辖区', '3', '220100', '0431', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('220102', '南关区', '3', '220100', '0431', 'ngq', null, '1');
INSERT INTO `sys_area` VALUES ('220103', '宽城区', '3', '220100', '0431', 'kcq', null, '1');
INSERT INTO `sys_area` VALUES ('220104', '朝阳区', '3', '220100', '0431', 'cyq', null, '1');
INSERT INTO `sys_area` VALUES ('220105', '二道区', '3', '220100', '0431', 'edq', null, '1');
INSERT INTO `sys_area` VALUES ('220106', '绿园区', '3', '220100', '0431', 'lyq', null, '1');
INSERT INTO `sys_area` VALUES ('220112', '双阳区', '3', '220100', '0431', 'syq', null, '1');
INSERT INTO `sys_area` VALUES ('220122', '农安县', '3', '220100', '0431', 'nax', null, '1');
INSERT INTO `sys_area` VALUES ('220181', '九台市', '3', '220100', '0431', 'jts', null, '1');
INSERT INTO `sys_area` VALUES ('220182', '榆树市', '3', '220100', '0431', 'yss', null, '1');
INSERT INTO `sys_area` VALUES ('220183', '德惠市', '3', '220100', '0431', 'dhs', null, '1');
INSERT INTO `sys_area` VALUES ('220200', '吉林市', '2', '220000', null, 'jls', null, '1');
INSERT INTO `sys_area` VALUES ('220201', '市辖区', '3', '220200', '0432', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('220202', '昌邑区', '3', '220200', '0432', 'cyq', null, '1');
INSERT INTO `sys_area` VALUES ('220203', '龙潭区', '3', '220200', '0432', 'ltq', null, '1');
INSERT INTO `sys_area` VALUES ('220204', '船营区', '3', '220200', '0432', 'cyq', null, '1');
INSERT INTO `sys_area` VALUES ('220211', '丰满区', '3', '220200', '0432', 'fmq', null, '1');
INSERT INTO `sys_area` VALUES ('220221', '永吉县', '3', '220200', '0432', 'yjx', null, '1');
INSERT INTO `sys_area` VALUES ('220281', '蛟河市', '3', '220200', '0432', 'jhs', null, '1');
INSERT INTO `sys_area` VALUES ('220282', '桦甸市', '3', '220200', '0432', 'hds', null, '1');
INSERT INTO `sys_area` VALUES ('220283', '舒兰市', '3', '220200', '0432', 'sls', null, '1');
INSERT INTO `sys_area` VALUES ('220284', '磐石市', '3', '220200', '0432', 'pss', null, '1');
INSERT INTO `sys_area` VALUES ('220300', '四平市', '2', '220000', null, 'sps', null, '1');
INSERT INTO `sys_area` VALUES ('220301', '市辖区', '3', '220300', '0434', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('220302', '铁西区', '3', '220300', '0434', 'txq', null, '1');
INSERT INTO `sys_area` VALUES ('220303', '铁东区', '3', '220300', '0434', 'tdq', null, '1');
INSERT INTO `sys_area` VALUES ('220322', '梨树县', '3', '220300', '0434', 'lsx', null, '1');
INSERT INTO `sys_area` VALUES ('220323', '伊通满族自治县', '3', '220300', '0434', 'ytmzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('220381', '公主岭市', '3', '220300', '0434', 'gzls', null, '1');
INSERT INTO `sys_area` VALUES ('220382', '双辽市', '3', '220300', '0434', 'sls', null, '1');
INSERT INTO `sys_area` VALUES ('220400', '辽源市', '2', '220000', null, 'lys', null, '1');
INSERT INTO `sys_area` VALUES ('220401', '市辖区', '3', '220400', '0437', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('220402', '龙山区', '3', '220400', '0437', 'lsq', null, '1');
INSERT INTO `sys_area` VALUES ('220403', '西安区', '3', '220400', '0437', 'xaq', null, '1');
INSERT INTO `sys_area` VALUES ('220421', '东丰县', '3', '220400', '0437', 'dfx', null, '1');
INSERT INTO `sys_area` VALUES ('220422', '东辽县', '3', '220400', '0437', 'dlx', null, '1');
INSERT INTO `sys_area` VALUES ('220500', '通化市', '2', '220000', null, 'ths', null, '1');
INSERT INTO `sys_area` VALUES ('220501', '市辖区', '3', '220500', '0435', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('220502', '东昌区', '3', '220500', '0435', 'dcq', null, '1');
INSERT INTO `sys_area` VALUES ('220503', '二道江区', '3', '220500', '0435', 'edjq', null, '1');
INSERT INTO `sys_area` VALUES ('220521', '通化县', '3', '220500', '0435', 'thx', null, '1');
INSERT INTO `sys_area` VALUES ('220523', '辉南县', '3', '220500', '0448', 'hnx', null, '1');
INSERT INTO `sys_area` VALUES ('220524', '柳河县', '3', '220500', '0448', 'lhx', null, '1');
INSERT INTO `sys_area` VALUES ('220581', '梅河口市', '3', '220500', '0448', 'mhks', null, '1');
INSERT INTO `sys_area` VALUES ('220582', '集安市', '3', '220500', '0435', 'jas', null, '1');
INSERT INTO `sys_area` VALUES ('220600', '白山市', '2', '220000', null, 'bss', null, '1');
INSERT INTO `sys_area` VALUES ('220601', '市辖区', '3', '220600', '0439', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('220602', '浑江区', '3', '220600', '0439', 'hjq', null, '1');
INSERT INTO `sys_area` VALUES ('220605', '江源区', '3', '220600', null, 'jyq', null, '1');
INSERT INTO `sys_area` VALUES ('220621', '抚松县', '3', '220600', '0439', 'fsx', null, '1');
INSERT INTO `sys_area` VALUES ('220622', '靖宇县', '3', '220600', '0439', 'jyx', null, '1');
INSERT INTO `sys_area` VALUES ('220623', '长白朝鲜族自治县', '3', '220600', '0439', 'zbcxzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('220681', '临江市', '3', '220600', '0439', 'ljs', null, '1');
INSERT INTO `sys_area` VALUES ('220700', '松原市', '2', '220000', null, 'sys', null, '1');
INSERT INTO `sys_area` VALUES ('220701', '市辖区', '3', '220700', '0438', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('220702', '宁江区', '3', '220700', '0438', 'njq', null, '1');
INSERT INTO `sys_area` VALUES ('220721', '前郭尔罗斯蒙古族自治县', '3', '220700', '0438', 'qgelsmgzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('220722', '长岭县', '3', '220700', '0438', 'zlx', null, '1');
INSERT INTO `sys_area` VALUES ('220723', '乾安县', '3', '220700', '0438', 'qax', null, '1');
INSERT INTO `sys_area` VALUES ('220724', '扶余县', '3', '220700', '0438', 'fyx', null, '1');
INSERT INTO `sys_area` VALUES ('220800', '白城市', '2', '220000', null, 'bcs', null, '1');
INSERT INTO `sys_area` VALUES ('220801', '市辖区', '3', '220800', '0436', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('220802', '洮北区', '3', '220800', '0436', 'tbq', null, '1');
INSERT INTO `sys_area` VALUES ('220821', '镇赉县', '3', '220800', '0436', 'zlx', null, '1');
INSERT INTO `sys_area` VALUES ('220822', '通榆县', '3', '220800', '0436', 'tyx', null, '1');
INSERT INTO `sys_area` VALUES ('220881', '洮南市', '3', '220800', '0436', 'tns', null, '1');
INSERT INTO `sys_area` VALUES ('220882', '大安市', '3', '220800', '0436', 'das', null, '1');
INSERT INTO `sys_area` VALUES ('222400', '延边朝鲜族自治州', '2', '220000', null, 'ybcxzzzz', null, '1');
INSERT INTO `sys_area` VALUES ('222401', '延吉市', '3', '222400', '0043', 'yjs', null, '1');
INSERT INTO `sys_area` VALUES ('222402', '图们市', '3', '222400', '0043', 'tms', null, '1');
INSERT INTO `sys_area` VALUES ('222403', '敦化市', '3', '222400', '0043', 'dhs', null, '1');
INSERT INTO `sys_area` VALUES ('222404', '珲春市', '3', '222400', '0043', 'hcs', null, '1');
INSERT INTO `sys_area` VALUES ('222405', '龙井市', '3', '222400', '0043', 'ljs', null, '1');
INSERT INTO `sys_area` VALUES ('222406', '和龙市', '3', '222400', '0043', 'hls', null, '1');
INSERT INTO `sys_area` VALUES ('222424', '汪清县', '3', '222400', '0043', 'wqx', null, '1');
INSERT INTO `sys_area` VALUES ('222426', '安图县', '3', '222400', '0043', 'atx', null, '1');
INSERT INTO `sys_area` VALUES ('230000', '黑龙江省', '1', '0', null, 'hljs', null, '1');
INSERT INTO `sys_area` VALUES ('230100', '哈尔滨市', '2', '230000', null, 'hebs', null, '1');
INSERT INTO `sys_area` VALUES ('230101', '市辖区', '3', '230100', '0451', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('230102', '道里区', '3', '230100', '0451', 'dlq', null, '1');
INSERT INTO `sys_area` VALUES ('230103', '南岗区', '3', '230100', '0451', 'ngq', null, '1');
INSERT INTO `sys_area` VALUES ('230104', '道外区', '3', '230100', '0451', 'dwq', null, '1');
INSERT INTO `sys_area` VALUES ('230108', '平房区', '3', '230100', '0451', 'pfq', null, '1');
INSERT INTO `sys_area` VALUES ('230109', '松北区', '3', '230100', '0451', 'sbq', null, '1');
INSERT INTO `sys_area` VALUES ('230110', '香坊区', '3', '230100', null, 'xfq', null, '1');
INSERT INTO `sys_area` VALUES ('230111', '呼兰区', '3', '230100', '0451', 'hlq', null, '1');
INSERT INTO `sys_area` VALUES ('230112', '阿城区', '3', '230100', null, 'acq', null, '1');
INSERT INTO `sys_area` VALUES ('230123', '依兰县', '3', '230100', '0451', 'ylx', null, '1');
INSERT INTO `sys_area` VALUES ('230124', '方正县', '3', '230100', '0451', 'fzx', null, '1');
INSERT INTO `sys_area` VALUES ('230125', '宾县', '3', '230100', '0451', 'bx', null, '1');
INSERT INTO `sys_area` VALUES ('230126', '巴彦县', '3', '230100', '0451', 'byx', null, '1');
INSERT INTO `sys_area` VALUES ('230127', '木兰县', '3', '230100', '0451', 'mlx', null, '1');
INSERT INTO `sys_area` VALUES ('230128', '通河县', '3', '230100', '0451', 'thx', null, '1');
INSERT INTO `sys_area` VALUES ('230129', '延寿县', '3', '230100', '0451', 'ysx', null, '1');
INSERT INTO `sys_area` VALUES ('230182', '双城市', '3', '230100', '0451', 'scs', null, '1');
INSERT INTO `sys_area` VALUES ('230183', '尚志市', '3', '230100', '0451', 'szs', null, '1');
INSERT INTO `sys_area` VALUES ('230184', '五常市', '3', '230100', '0451', 'wcs', null, '1');
INSERT INTO `sys_area` VALUES ('230200', '齐齐哈尔市', '2', '230000', null, 'qqhes', null, '1');
INSERT INTO `sys_area` VALUES ('230201', '市辖区', '3', '230200', '0452', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('230202', '龙沙区', '3', '230200', '0452', 'lsq', null, '1');
INSERT INTO `sys_area` VALUES ('230203', '建华区', '3', '230200', '0452', 'jhq', null, '1');
INSERT INTO `sys_area` VALUES ('230204', '铁锋区', '3', '230200', '0452', 'tfq', null, '1');
INSERT INTO `sys_area` VALUES ('230205', '昂昂溪区', '3', '230200', '0452', 'aaxq', null, '1');
INSERT INTO `sys_area` VALUES ('230206', '富拉尔基区', '3', '230200', '0452', 'flejq', null, '1');
INSERT INTO `sys_area` VALUES ('230207', '碾子山区', '3', '230200', '0452', 'nzsq', null, '1');
INSERT INTO `sys_area` VALUES ('230208', '梅里斯达斡尔族区', '3', '230200', '0452', 'mlsdwezq', null, '1');
INSERT INTO `sys_area` VALUES ('230221', '龙江县', '3', '230200', '0452', 'ljx', null, '1');
INSERT INTO `sys_area` VALUES ('230223', '依安县', '3', '230200', '0452', 'yax', null, '1');
INSERT INTO `sys_area` VALUES ('230224', '泰来县', '3', '230200', '0452', 'tlx', null, '1');
INSERT INTO `sys_area` VALUES ('230225', '甘南县', '3', '230200', '0452', 'gnx', null, '1');
INSERT INTO `sys_area` VALUES ('230227', '富裕县', '3', '230200', '0452', 'fyx', null, '1');
INSERT INTO `sys_area` VALUES ('230229', '克山县', '3', '230200', '0452', 'ksx', null, '1');
INSERT INTO `sys_area` VALUES ('230230', '克东县', '3', '230200', '0452', 'kdx', null, '1');
INSERT INTO `sys_area` VALUES ('230231', '拜泉县', '3', '230200', '0452', 'bqx', null, '1');
INSERT INTO `sys_area` VALUES ('230281', '讷河市', '3', '230200', '0452', 'nhs', null, '1');
INSERT INTO `sys_area` VALUES ('230300', '鸡西市', '2', '230000', null, 'jxs', null, '1');
INSERT INTO `sys_area` VALUES ('230301', '市辖区', '3', '230300', '0467', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('230302', '鸡冠区', '3', '230300', '0467', 'jgq', null, '1');
INSERT INTO `sys_area` VALUES ('230303', '恒山区', '3', '230300', '0467', 'hsq', null, '1');
INSERT INTO `sys_area` VALUES ('230304', '滴道区', '3', '230300', '0467', 'ddq', null, '1');
INSERT INTO `sys_area` VALUES ('230305', '梨树区', '3', '230300', '0467', 'lsq', null, '1');
INSERT INTO `sys_area` VALUES ('230306', '城子河区', '3', '230300', '0467', 'czhq', null, '1');
INSERT INTO `sys_area` VALUES ('230307', '麻山区', '3', '230300', '0467', 'msq', null, '1');
INSERT INTO `sys_area` VALUES ('230321', '鸡东县', '3', '230300', '0467', 'jdx', null, '1');
INSERT INTO `sys_area` VALUES ('230381', '虎林市', '3', '230300', '0467', 'hls', null, '1');
INSERT INTO `sys_area` VALUES ('230382', '密山市', '3', '230300', '0467', 'mss', null, '1');
INSERT INTO `sys_area` VALUES ('230400', '鹤岗市', '2', '230000', null, 'hgs', null, '1');
INSERT INTO `sys_area` VALUES ('230401', '市辖区', '3', '230400', '0454', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('230402', '向阳区', '3', '230400', '0468', 'xyq', null, '1');
INSERT INTO `sys_area` VALUES ('230403', '工农区', '3', '230400', '0468', 'gnq', null, '1');
INSERT INTO `sys_area` VALUES ('230404', '南山区', '3', '230400', '0468', 'nsq', null, '1');
INSERT INTO `sys_area` VALUES ('230405', '兴安区', '3', '230400', '0468', 'xaq', null, '1');
INSERT INTO `sys_area` VALUES ('230406', '东山区', '3', '230400', '0468', 'dsq', null, '1');
INSERT INTO `sys_area` VALUES ('230407', '兴山区', '3', '230400', '0468', 'xsq', null, '1');
INSERT INTO `sys_area` VALUES ('230421', '萝北县', '3', '230400', '0468', 'lbx', null, '1');
INSERT INTO `sys_area` VALUES ('230422', '绥滨县', '3', '230400', '0468', 'sbx', null, '1');
INSERT INTO `sys_area` VALUES ('230500', '双鸭山市', '2', '230000', null, 'syss', null, '1');
INSERT INTO `sys_area` VALUES ('230501', '市辖区', '3', '230500', '0469', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('230502', '尖山区', '3', '230500', '0469', 'jsq', null, '1');
INSERT INTO `sys_area` VALUES ('230503', '岭东区', '3', '230500', '0469', 'ldq', null, '1');
INSERT INTO `sys_area` VALUES ('230505', '四方台区', '3', '230500', '0469', 'sftq', null, '1');
INSERT INTO `sys_area` VALUES ('230506', '宝山区', '3', '230500', '0469', 'bsq', null, '1');
INSERT INTO `sys_area` VALUES ('230521', '集贤县', '3', '230500', '0469', 'jxx', null, '1');
INSERT INTO `sys_area` VALUES ('230522', '友谊县', '3', '230500', '0469', 'yyx', null, '1');
INSERT INTO `sys_area` VALUES ('230523', '宝清县', '3', '230500', '0469', 'bqx', null, '1');
INSERT INTO `sys_area` VALUES ('230524', '饶河县', '3', '230500', '0469', 'rhx', null, '1');
INSERT INTO `sys_area` VALUES ('230600', '大庆市', '2', '230000', null, 'dqs', null, '1');
INSERT INTO `sys_area` VALUES ('230601', '市辖区', '3', '230600', '0459', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('230602', '萨尔图区', '3', '230600', '0459', 'setq', null, '1');
INSERT INTO `sys_area` VALUES ('230603', '龙凤区', '3', '230600', '0459', 'lfq', null, '1');
INSERT INTO `sys_area` VALUES ('230604', '让胡路区', '3', '230600', '0459', 'rhlq', null, '1');
INSERT INTO `sys_area` VALUES ('230605', '红岗区', '3', '230600', '0459', 'hgq', null, '1');
INSERT INTO `sys_area` VALUES ('230606', '大同区', '3', '230600', '0459', 'dtq', null, '1');
INSERT INTO `sys_area` VALUES ('230621', '肇州县', '3', '230600', '0459', 'zzx', null, '1');
INSERT INTO `sys_area` VALUES ('230622', '肇源县', '3', '230600', '0459', 'zyx', null, '1');
INSERT INTO `sys_area` VALUES ('230623', '林甸县', '3', '230600', '0459', 'ldx', null, '1');
INSERT INTO `sys_area` VALUES ('230624', '杜尔伯特蒙古族自治县', '3', '230600', '0459', 'debtmgzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('230700', '伊春市', '2', '230000', null, 'ycs', null, '1');
INSERT INTO `sys_area` VALUES ('230701', '市辖区', '3', '230700', '0458', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('230702', '伊春区', '3', '230700', '0458', 'ycq', null, '1');
INSERT INTO `sys_area` VALUES ('230703', '南岔区', '3', '230700', '0458', 'ncq', null, '1');
INSERT INTO `sys_area` VALUES ('230704', '友好区', '3', '230700', '0458', 'yhq', null, '1');
INSERT INTO `sys_area` VALUES ('230705', '西林区', '3', '230700', '0458', 'xlq', null, '1');
INSERT INTO `sys_area` VALUES ('230706', '翠峦区', '3', '230700', '0458', 'clq', null, '1');
INSERT INTO `sys_area` VALUES ('230707', '新青区', '3', '230700', '0458', 'xqq', null, '1');
INSERT INTO `sys_area` VALUES ('230708', '美溪区', '3', '230700', '0458', 'mxq', null, '1');
INSERT INTO `sys_area` VALUES ('230709', '金山屯区', '3', '230700', '0458', 'jstq', null, '1');
INSERT INTO `sys_area` VALUES ('230710', '五营区', '3', '230700', '0458', 'wyq', null, '1');
INSERT INTO `sys_area` VALUES ('230711', '乌马河区', '3', '230700', '0458', 'wmhq', null, '1');
INSERT INTO `sys_area` VALUES ('230712', '汤旺河区', '3', '230700', '0458', 'twhq', null, '1');
INSERT INTO `sys_area` VALUES ('230713', '带岭区', '3', '230700', '0458', 'dlq', null, '1');
INSERT INTO `sys_area` VALUES ('230714', '乌伊岭区', '3', '230700', '0458', 'wylq', null, '1');
INSERT INTO `sys_area` VALUES ('230715', '红星区', '3', '230700', '0458', 'hxq', null, '1');
INSERT INTO `sys_area` VALUES ('230716', '上甘岭区', '3', '230700', '0458', 'sglq', null, '1');
INSERT INTO `sys_area` VALUES ('230722', '嘉荫县', '3', '230700', '0458', 'jyx', null, '1');
INSERT INTO `sys_area` VALUES ('230781', '铁力市', '3', '230700', '0458', 'tls', null, '1');
INSERT INTO `sys_area` VALUES ('230800', '佳木斯市', '2', '230000', null, 'jmss', null, '1');
INSERT INTO `sys_area` VALUES ('230801', '市辖区', '3', '230800', '0454', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('230803', '向阳区', '3', '230800', '0454', 'xyq', null, '1');
INSERT INTO `sys_area` VALUES ('230804', '前进区', '3', '230800', '0454', 'qjq', null, '1');
INSERT INTO `sys_area` VALUES ('230805', '东风区', '3', '230800', '0454', 'dfq', null, '1');
INSERT INTO `sys_area` VALUES ('230811', '郊区', '3', '230800', '0454', 'jq', null, '1');
INSERT INTO `sys_area` VALUES ('230822', '桦南县', '3', '230800', '0454', 'hnx', null, '1');
INSERT INTO `sys_area` VALUES ('230826', '桦川县', '3', '230800', '0454', 'hcx', null, '1');
INSERT INTO `sys_area` VALUES ('230828', '汤原县', '3', '230800', '0454', 'tyx', null, '1');
INSERT INTO `sys_area` VALUES ('230833', '抚远县', '3', '230800', '0454', 'fyx', null, '1');
INSERT INTO `sys_area` VALUES ('230881', '同江市', '3', '230800', '0454', 'tjs', null, '1');
INSERT INTO `sys_area` VALUES ('230882', '富锦市', '3', '230800', '0454', 'fjs', null, '1');
INSERT INTO `sys_area` VALUES ('230900', '七台河市', '2', '230000', null, 'qths', null, '1');
INSERT INTO `sys_area` VALUES ('230901', '市辖区', '3', '230900', '0464', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('230902', '新兴区', '3', '230900', '0464', 'xxq', null, '1');
INSERT INTO `sys_area` VALUES ('230903', '桃山区', '3', '230900', '0464', 'tsq', null, '1');
INSERT INTO `sys_area` VALUES ('230904', '茄子河区', '3', '230900', '0464', 'qzhq', null, '1');
INSERT INTO `sys_area` VALUES ('230921', '勃利县', '3', '230900', '0464', 'blx', null, '1');
INSERT INTO `sys_area` VALUES ('231000', '牡丹江市', '2', '230000', null, 'mdjs', null, '1');
INSERT INTO `sys_area` VALUES ('231001', '市辖区', '3', '231000', '0453', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('231002', '东安区', '3', '231000', '0453', 'daq', null, '1');
INSERT INTO `sys_area` VALUES ('231003', '阳明区', '3', '231000', '0453', 'ymq', null, '1');
INSERT INTO `sys_area` VALUES ('231004', '爱民区', '3', '231000', '0453', 'amq', null, '1');
INSERT INTO `sys_area` VALUES ('231005', '西安区', '3', '231000', '0453', 'xaq', null, '1');
INSERT INTO `sys_area` VALUES ('231024', '东宁县', '3', '231000', '0453', 'dnx', null, '1');
INSERT INTO `sys_area` VALUES ('231025', '林口县', '3', '231000', '0453', 'lkx', null, '1');
INSERT INTO `sys_area` VALUES ('231081', '绥芬河市', '3', '231000', '0453', 'sfhs', null, '1');
INSERT INTO `sys_area` VALUES ('231083', '海林市', '3', '231000', '0453', 'hls', null, '1');
INSERT INTO `sys_area` VALUES ('231084', '宁安市', '3', '231000', '0453', 'nas', null, '1');
INSERT INTO `sys_area` VALUES ('231085', '穆棱市', '3', '231000', '0453', 'mls', null, '1');
INSERT INTO `sys_area` VALUES ('231100', '黑河市', '2', '230000', null, 'hhs', null, '1');
INSERT INTO `sys_area` VALUES ('231101', '市辖区', '3', '231100', '0456', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('231102', '爱辉区', '3', '231100', '0456', 'ahq', null, '1');
INSERT INTO `sys_area` VALUES ('231121', '嫩江县', '3', '231100', '0456', 'njx', null, '1');
INSERT INTO `sys_area` VALUES ('231123', '逊克县', '3', '231100', '0456', 'xkx', null, '1');
INSERT INTO `sys_area` VALUES ('231124', '孙吴县', '3', '231100', '0456', 'swx', null, '1');
INSERT INTO `sys_area` VALUES ('231181', '北安市', '3', '231100', '0456', 'bas', null, '1');
INSERT INTO `sys_area` VALUES ('231182', '五大连池市', '3', '231100', '0456', 'wdlcs', null, '1');
INSERT INTO `sys_area` VALUES ('231200', '绥化市', '2', '230000', null, 'shs', null, '1');
INSERT INTO `sys_area` VALUES ('231201', '市辖区', '3', '231200', '0455', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('231202', '北林区', '3', '231200', '0455', 'blq', null, '1');
INSERT INTO `sys_area` VALUES ('231221', '望奎县', '3', '231200', '0455', 'wkx', null, '1');
INSERT INTO `sys_area` VALUES ('231222', '兰西县', '3', '231200', '0455', 'lxx', null, '1');
INSERT INTO `sys_area` VALUES ('231223', '青冈县', '3', '231200', '0455', 'qgx', null, '1');
INSERT INTO `sys_area` VALUES ('231224', '庆安县', '3', '231200', '0455', 'qax', null, '1');
INSERT INTO `sys_area` VALUES ('231225', '明水县', '3', '231200', '0455', 'msx', null, '1');
INSERT INTO `sys_area` VALUES ('231226', '绥棱县', '3', '231200', '0455', 'slx', null, '1');
INSERT INTO `sys_area` VALUES ('231281', '安达市', '3', '231200', '0455', 'ads', null, '1');
INSERT INTO `sys_area` VALUES ('231282', '肇东市', '3', '231200', '0455', 'zds', null, '1');
INSERT INTO `sys_area` VALUES ('231283', '海伦市', '3', '231200', '0455', 'hls', null, '1');
INSERT INTO `sys_area` VALUES ('232700', '大兴安岭地区', '2', '230000', null, 'dxaldq', null, '1');
INSERT INTO `sys_area` VALUES ('232721', '呼玛县', '3', '232700', '0457', 'hmx', null, '1');
INSERT INTO `sys_area` VALUES ('232722', '塔河县', '3', '232700', '0457', 'thx', null, '1');
INSERT INTO `sys_area` VALUES ('232723', '漠河县', '3', '232700', '0457', 'mhx', null, '1');
INSERT INTO `sys_area` VALUES ('310000', '上海市', '1', '0', null, 'shs', null, '1');
INSERT INTO `sys_area` VALUES ('310100', '上海市(辖区)', '2', '310000', null, 'shs(xq)', null, '1');
INSERT INTO `sys_area` VALUES ('310101', '黄浦区', '3', '310100', '021', 'hpq', null, '1');
INSERT INTO `sys_area` VALUES ('310104', '徐汇区', '3', '310100', '021', 'xhq', null, '1');
INSERT INTO `sys_area` VALUES ('310105', '长宁区', '3', '310100', '021', 'znq', null, '1');
INSERT INTO `sys_area` VALUES ('310106', '静安区', '3', '310100', '021', 'jaq', null, '1');
INSERT INTO `sys_area` VALUES ('310107', '普陀区', '3', '310100', '021', 'ptq', null, '1');
INSERT INTO `sys_area` VALUES ('310108', '闸北区', '3', '310100', '021', 'zbq', null, '1');
INSERT INTO `sys_area` VALUES ('310109', '虹口区', '3', '310100', '021', 'hkq', null, '1');
INSERT INTO `sys_area` VALUES ('310110', '杨浦区', '3', '310100', '021', 'ypq', null, '1');
INSERT INTO `sys_area` VALUES ('310112', '闵行区', '3', '310100', '021', 'mxq', null, '1');
INSERT INTO `sys_area` VALUES ('310113', '宝山区', '3', '310100', '021', 'bsq', null, '1');
INSERT INTO `sys_area` VALUES ('310114', '嘉定区', '3', '310100', '021', 'jdq', null, '1');
INSERT INTO `sys_area` VALUES ('310115', '浦东新区', '3', '310100', '021', 'pdxq', null, '1');
INSERT INTO `sys_area` VALUES ('310116', '金山区', '3', '310100', '021', 'jsq', null, '1');
INSERT INTO `sys_area` VALUES ('310117', '松江区', '3', '310100', '021', 'sjq', null, '1');
INSERT INTO `sys_area` VALUES ('310118', '青浦区', '3', '310100', '021', 'qpq', null, '1');
INSERT INTO `sys_area` VALUES ('310120', '奉贤区', '3', '310100', '021', 'fxq', null, '1');
INSERT INTO `sys_area` VALUES ('310200', '县', '2', '310000', null, 'x', null, '1');
INSERT INTO `sys_area` VALUES ('310230', '崇明县', '3', '310200', '021', 'cmx', null, '1');
INSERT INTO `sys_area` VALUES ('320000', '江苏省', '1', '0', null, 'jss', null, '1');
INSERT INTO `sys_area` VALUES ('320100', '南京市', '2', '320000', null, 'njs', null, '1');
INSERT INTO `sys_area` VALUES ('320101', '市辖区', '3', '320100', '025', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('320102', '玄武区', '3', '320100', '025', 'xwq', null, '1');
INSERT INTO `sys_area` VALUES ('320103', '白下区', '3', '320100', '025', 'bxq', null, '1');
INSERT INTO `sys_area` VALUES ('320104', '秦淮区', '3', '320100', '025', 'qhq', null, '1');
INSERT INTO `sys_area` VALUES ('320105', '建邺区', '3', '320100', '025', 'jyq', null, '1');
INSERT INTO `sys_area` VALUES ('320106', '鼓楼区', '3', '320100', '025', 'glq', null, '1');
INSERT INTO `sys_area` VALUES ('320107', '下关区', '3', '320100', '025', 'xgq', null, '1');
INSERT INTO `sys_area` VALUES ('320111', '浦口区', '3', '320100', '025', 'pkq', null, '1');
INSERT INTO `sys_area` VALUES ('320113', '栖霞区', '3', '320100', '025', 'qxq', null, '1');
INSERT INTO `sys_area` VALUES ('320114', '雨花台区', '3', '320100', '025', 'yhtq', null, '1');
INSERT INTO `sys_area` VALUES ('320115', '江宁区', '3', '320100', '025', 'jnq', null, '1');
INSERT INTO `sys_area` VALUES ('320116', '六合区', '3', '320100', '025', 'lhq', null, '1');
INSERT INTO `sys_area` VALUES ('320124', '溧水县', '3', '320100', '025', 'lsx', null, '1');
INSERT INTO `sys_area` VALUES ('320125', '高淳县', '3', '320100', '025', 'gcx', null, '1');
INSERT INTO `sys_area` VALUES ('320200', '无锡市', '2', '320000', null, 'wxs', null, '1');
INSERT INTO `sys_area` VALUES ('320201', '市辖区', '3', '320200', '0510', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('320202', '崇安区', '3', '320200', '0510', 'caq', null, '1');
INSERT INTO `sys_area` VALUES ('320203', '南长区', '3', '320200', '0510', 'nzq', null, '1');
INSERT INTO `sys_area` VALUES ('320204', '北塘区', '3', '320200', '0510', 'btq', null, '1');
INSERT INTO `sys_area` VALUES ('320205', '锡山区', '3', '320200', '0510', 'xsq', null, '1');
INSERT INTO `sys_area` VALUES ('320206', '惠山区', '3', '320200', '0510', 'hsq', null, '1');
INSERT INTO `sys_area` VALUES ('320211', '滨湖区', '3', '320200', '0510', 'bhq', null, '1');
INSERT INTO `sys_area` VALUES ('320281', '江阴市', '3', '320200', '0510', 'jys', null, '1');
INSERT INTO `sys_area` VALUES ('320282', '宜兴市', '3', '320200', '0510', 'yxs', null, '1');
INSERT INTO `sys_area` VALUES ('320300', '徐州市', '2', '320000', null, 'xzs', null, '1');
INSERT INTO `sys_area` VALUES ('320301', '市辖区', '3', '320300', '0516', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('320302', '鼓楼区', '3', '320300', '0516', 'glq', null, '1');
INSERT INTO `sys_area` VALUES ('320303', '云龙区', '3', '320300', '0516', 'ylq', null, '1');
INSERT INTO `sys_area` VALUES ('320305', '贾汪区', '3', '320300', '0516', 'jwq', null, '1');
INSERT INTO `sys_area` VALUES ('320311', '泉山区', '3', '320300', '0516', 'qsq', null, '1');
INSERT INTO `sys_area` VALUES ('320312', '铜山区', '3', '320300', null, 'tsq', null, '1');
INSERT INTO `sys_area` VALUES ('320321', '丰县', '3', '320300', '0516', 'fx', null, '1');
INSERT INTO `sys_area` VALUES ('320322', '沛县', '3', '320300', '0516', 'px', null, '1');
INSERT INTO `sys_area` VALUES ('320324', '睢宁县', '3', '320300', '0516', 'snx', null, '1');
INSERT INTO `sys_area` VALUES ('320381', '新沂市', '3', '320300', '0516', 'xys', null, '1');
INSERT INTO `sys_area` VALUES ('320382', '邳州市', '3', '320300', '0516', 'pzs', null, '1');
INSERT INTO `sys_area` VALUES ('320400', '常州市', '2', '320000', null, 'czs', null, '1');
INSERT INTO `sys_area` VALUES ('320401', '市辖区', '3', '320400', '0519', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('320402', '天宁区', '3', '320400', '0519', 'tnq', null, '1');
INSERT INTO `sys_area` VALUES ('320404', '钟楼区', '3', '320400', '0519', 'zlq', null, '1');
INSERT INTO `sys_area` VALUES ('320405', '戚墅堰区', '3', '320400', '0519', 'qsyq', null, '1');
INSERT INTO `sys_area` VALUES ('320411', '新北区', '3', '320400', '0519', 'xbq', null, '1');
INSERT INTO `sys_area` VALUES ('320412', '武进区', '3', '320400', '0519', 'wjq', null, '1');
INSERT INTO `sys_area` VALUES ('320481', '溧阳市', '3', '320400', '0519', 'lys', null, '1');
INSERT INTO `sys_area` VALUES ('320482', '金坛市', '3', '320400', '0519', 'jts', null, '1');
INSERT INTO `sys_area` VALUES ('320500', '苏州市', '2', '320000', null, 'szs', null, '1');
INSERT INTO `sys_area` VALUES ('320501', '市辖区', '3', '320500', '0512', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('320505', '虎丘区', '3', '320500', '0512', 'hqq', null, '1');
INSERT INTO `sys_area` VALUES ('320506', '吴中区', '3', '320500', '0512', 'wzq', null, '1');
INSERT INTO `sys_area` VALUES ('320507', '相城区', '3', '320500', '0512', 'xcq', null, '1');
INSERT INTO `sys_area` VALUES ('320508', '姑苏区', '3', '320500', null, 'gsq', null, '1');
INSERT INTO `sys_area` VALUES ('320509', '吴江区', '3', '320500', null, 'wjq', null, '1');
INSERT INTO `sys_area` VALUES ('320581', '常熟市', '3', '320500', '0512', 'css', null, '1');
INSERT INTO `sys_area` VALUES ('320582', '张家港市', '3', '320500', '0512', 'zjgs', null, '1');
INSERT INTO `sys_area` VALUES ('320583', '昆山市', '3', '320500', '0512', 'kss', null, '1');
INSERT INTO `sys_area` VALUES ('320585', '太仓市', '3', '320500', '0512', 'tcs', null, '1');
INSERT INTO `sys_area` VALUES ('320600', '南通市', '2', '320000', null, 'nts', null, '1');
INSERT INTO `sys_area` VALUES ('320601', '市辖区', '3', '320600', '0513', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('320602', '崇川区', '3', '320600', '0513', 'ccq', null, '1');
INSERT INTO `sys_area` VALUES ('320611', '港闸区', '3', '320600', '0513', 'gzq', null, '1');
INSERT INTO `sys_area` VALUES ('320612', '通州区', '3', '320600', null, 'tzq', null, '1');
INSERT INTO `sys_area` VALUES ('320621', '海安县', '3', '320600', '0513', 'hax', null, '1');
INSERT INTO `sys_area` VALUES ('320623', '如东县', '3', '320600', '0513', 'rdx', null, '1');
INSERT INTO `sys_area` VALUES ('320681', '启东市', '3', '320600', '0513', 'qds', null, '1');
INSERT INTO `sys_area` VALUES ('320682', '如皋市', '3', '320600', '0513', 'rgs', null, '1');
INSERT INTO `sys_area` VALUES ('320684', '海门市', '3', '320600', '0513', 'hms', null, '1');
INSERT INTO `sys_area` VALUES ('320700', '连云港市', '2', '320000', null, 'lygs', null, '1');
INSERT INTO `sys_area` VALUES ('320701', '市辖区', '3', '320700', '0518', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('320703', '连云区', '3', '320700', '0518', 'lyq', null, '1');
INSERT INTO `sys_area` VALUES ('320705', '新浦区', '3', '320700', '0518', 'xpq', null, '1');
INSERT INTO `sys_area` VALUES ('320706', '海州区', '3', '320700', '0518', 'hzq', null, '1');
INSERT INTO `sys_area` VALUES ('320721', '赣榆县', '3', '320700', '0518', 'gyx', null, '1');
INSERT INTO `sys_area` VALUES ('320722', '东海县', '3', '320700', '0518', 'dhx', null, '1');
INSERT INTO `sys_area` VALUES ('320723', '灌云县', '3', '320700', '0518', 'gyx', null, '1');
INSERT INTO `sys_area` VALUES ('320724', '灌南县', '3', '320700', '0518', 'gnx', null, '1');
INSERT INTO `sys_area` VALUES ('320800', '淮安市', '2', '320000', null, 'has', null, '1');
INSERT INTO `sys_area` VALUES ('320801', '市辖区', '3', '320800', '0517', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('320802', '清河区', '3', '320800', '0517', 'qhq', null, '1');
INSERT INTO `sys_area` VALUES ('320803', '淮安区', '3', '320800', '0517', 'haq', null, '1');
INSERT INTO `sys_area` VALUES ('320804', '淮阴区', '3', '320800', '0517', 'hyq', null, '1');
INSERT INTO `sys_area` VALUES ('320811', '清浦区', '3', '320800', '0517', 'qpq', null, '1');
INSERT INTO `sys_area` VALUES ('320826', '涟水县', '3', '320800', '0517', 'lsx', null, '1');
INSERT INTO `sys_area` VALUES ('320829', '洪泽县', '3', '320800', '0517', 'hzx', null, '1');
INSERT INTO `sys_area` VALUES ('320830', '盱眙县', '3', '320800', '0517', 'xyx', null, '1');
INSERT INTO `sys_area` VALUES ('320831', '金湖县', '3', '320800', '0517', 'jhx', null, '1');
INSERT INTO `sys_area` VALUES ('320900', '盐城市', '2', '320000', null, 'ycs', null, '1');
INSERT INTO `sys_area` VALUES ('320901', '市辖区', '3', '320900', '0515', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('320902', '亭湖区', '3', '320900', '0515', 'thq', null, '1');
INSERT INTO `sys_area` VALUES ('320903', '盐都区', '3', '320900', '0515', 'ydq', null, '1');
INSERT INTO `sys_area` VALUES ('320921', '响水县', '3', '320900', '0515', 'xsx', null, '1');
INSERT INTO `sys_area` VALUES ('320922', '滨海县', '3', '320900', '0515', 'bhx', null, '1');
INSERT INTO `sys_area` VALUES ('320923', '阜宁县', '3', '320900', '0515', 'fnx', null, '1');
INSERT INTO `sys_area` VALUES ('320924', '射阳县', '3', '320900', '0515', 'syx', null, '1');
INSERT INTO `sys_area` VALUES ('320925', '建湖县', '3', '320900', '0515', 'jhx', null, '1');
INSERT INTO `sys_area` VALUES ('320981', '东台市', '3', '320900', '0515', 'dts', null, '1');
INSERT INTO `sys_area` VALUES ('320982', '大丰市', '3', '320900', '0515', 'dfs', null, '1');
INSERT INTO `sys_area` VALUES ('321000', '扬州市', '2', '320000', null, 'yzs', null, '1');
INSERT INTO `sys_area` VALUES ('321001', '市辖区', '3', '321000', '0514', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('321002', '广陵区', '3', '321000', '0514', 'glq', null, '1');
INSERT INTO `sys_area` VALUES ('321003', '邗江区', '3', '321000', '0514', 'hjq', null, '1');
INSERT INTO `sys_area` VALUES ('321012', '江都区', '3', '321000', null, 'jdq', null, '1');
INSERT INTO `sys_area` VALUES ('321023', '宝应县', '3', '321000', '0514', 'byx', null, '1');
INSERT INTO `sys_area` VALUES ('321081', '仪征市', '3', '321000', '0514', 'yzs', null, '1');
INSERT INTO `sys_area` VALUES ('321084', '高邮市', '3', '321000', '0514', 'gys', null, '1');
INSERT INTO `sys_area` VALUES ('321100', '镇江市', '2', '320000', null, 'zjs', null, '1');
INSERT INTO `sys_area` VALUES ('321101', '市辖区', '3', '321100', '0511', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('321102', '京口区', '3', '321100', '0511', 'jkq', null, '1');
INSERT INTO `sys_area` VALUES ('321111', '润州区', '3', '321100', '0511', 'rzq', null, '1');
INSERT INTO `sys_area` VALUES ('321112', '丹徒区', '3', '321100', '0511', 'dtq', null, '1');
INSERT INTO `sys_area` VALUES ('321181', '丹阳市', '3', '321100', '0511', 'dys', null, '1');
INSERT INTO `sys_area` VALUES ('321182', '扬中市', '3', '321100', '0511', 'yzs', null, '1');
INSERT INTO `sys_area` VALUES ('321183', '句容市', '3', '321100', '0511', 'jrs', null, '1');
INSERT INTO `sys_area` VALUES ('321200', '泰州市', '2', '320000', null, 'tzs', null, '1');
INSERT INTO `sys_area` VALUES ('321201', '市辖区', '3', '321200', '0523', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('321202', '海陵区', '3', '321200', '0523', 'hlq', null, '1');
INSERT INTO `sys_area` VALUES ('321203', '高港区', '3', '321200', '0523', 'ggq', null, '1');
INSERT INTO `sys_area` VALUES ('321281', '兴化市', '3', '321200', '0523', 'xhs', null, '1');
INSERT INTO `sys_area` VALUES ('321282', '靖江市', '3', '321200', '0523', 'jjs', null, '1');
INSERT INTO `sys_area` VALUES ('321283', '泰兴市', '3', '321200', '0523', 'txs', null, '1');
INSERT INTO `sys_area` VALUES ('321284', '姜堰市', '3', '321200', '0523', 'jys', null, '1');
INSERT INTO `sys_area` VALUES ('321300', '宿迁市', '2', '320000', null, 'sqs', null, '1');
INSERT INTO `sys_area` VALUES ('321301', '市辖区', '3', '321300', '0527', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('321302', '宿城区', '3', '321300', '0527', 'scq', null, '1');
INSERT INTO `sys_area` VALUES ('321311', '宿豫区', '3', '321300', '0527', 'syq', null, '1');
INSERT INTO `sys_area` VALUES ('321322', '沭阳县', '3', '321300', '0527', 'syx', null, '1');
INSERT INTO `sys_area` VALUES ('321323', '泗阳县', '3', '321300', '0527', 'syx', null, '1');
INSERT INTO `sys_area` VALUES ('321324', '泗洪县', '3', '321300', '0527', 'shx', null, '1');
INSERT INTO `sys_area` VALUES ('330000', '浙江省', '1', '0', null, 'zjs', null, '1');
INSERT INTO `sys_area` VALUES ('330100', '杭州市', '2', '330000', null, 'hzs', null, '1');
INSERT INTO `sys_area` VALUES ('330101', '市辖区', '3', '330100', '0571', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('330102', '上城区', '3', '330100', '0571', 'scq', null, '1');
INSERT INTO `sys_area` VALUES ('330103', '下城区', '3', '330100', '0571', 'xcq', null, '1');
INSERT INTO `sys_area` VALUES ('330104', '江干区', '3', '330100', '0571', 'jgq', null, '1');
INSERT INTO `sys_area` VALUES ('330105', '拱墅区', '3', '330100', '0571', 'gsq', null, '1');
INSERT INTO `sys_area` VALUES ('330106', '西湖区', '3', '330100', '0571', 'xhq', null, '1');
INSERT INTO `sys_area` VALUES ('330108', '滨江区', '3', '330100', '0571', 'bjq', null, '1');
INSERT INTO `sys_area` VALUES ('330109', '萧山区', '3', '330100', '0571', 'xsq', null, '1');
INSERT INTO `sys_area` VALUES ('330110', '余杭区', '3', '330100', '0571', 'yhq', null, '1');
INSERT INTO `sys_area` VALUES ('330122', '桐庐县', '3', '330100', '0571', 'tlx', null, '1');
INSERT INTO `sys_area` VALUES ('330127', '淳安县', '3', '330100', '0571', 'cax', null, '1');
INSERT INTO `sys_area` VALUES ('330182', '建德市', '3', '330100', '0571', 'jds', null, '1');
INSERT INTO `sys_area` VALUES ('330183', '富阳市', '3', '330100', '0571', 'fys', null, '1');
INSERT INTO `sys_area` VALUES ('330185', '临安市', '3', '330100', '0571', 'las', null, '1');
INSERT INTO `sys_area` VALUES ('330200', '宁波市', '2', '330000', null, 'nbs', null, '1');
INSERT INTO `sys_area` VALUES ('330201', '市辖区', '3', '330200', '0574', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('330203', '海曙区', '3', '330200', '0574', 'hsq', null, '1');
INSERT INTO `sys_area` VALUES ('330204', '江东区', '3', '330200', '0574', 'jdq', null, '1');
INSERT INTO `sys_area` VALUES ('330205', '江北区', '3', '330200', '0574', 'jbq', null, '1');
INSERT INTO `sys_area` VALUES ('330206', '北仑区', '3', '330200', '0574', 'blq', null, '1');
INSERT INTO `sys_area` VALUES ('330211', '镇海区', '3', '330200', '0574', 'zhq', null, '1');
INSERT INTO `sys_area` VALUES ('330212', '鄞州区', '3', '330200', '0574', 'yzq', null, '1');
INSERT INTO `sys_area` VALUES ('330225', '象山县', '3', '330200', '0574', 'xsx', null, '1');
INSERT INTO `sys_area` VALUES ('330226', '宁海县', '3', '330200', '0574', 'nhx', null, '1');
INSERT INTO `sys_area` VALUES ('330281', '余姚市', '3', '330200', '0574', 'yys', null, '1');
INSERT INTO `sys_area` VALUES ('330282', '慈溪市', '3', '330200', '0574', 'cxs', null, '1');
INSERT INTO `sys_area` VALUES ('330283', '奉化市', '3', '330200', '0574', 'fhs', null, '1');
INSERT INTO `sys_area` VALUES ('330300', '温州市', '2', '330000', null, 'wzs', null, '1');
INSERT INTO `sys_area` VALUES ('330301', '市辖区', '3', '330300', '0577', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('330302', '鹿城区', '3', '330300', '0577', 'lcq', null, '1');
INSERT INTO `sys_area` VALUES ('330303', '龙湾区', '3', '330300', '0577', 'lwq', null, '1');
INSERT INTO `sys_area` VALUES ('330304', '瓯海区', '3', '330300', '0577', 'ohq', null, '1');
INSERT INTO `sys_area` VALUES ('330322', '洞头县', '3', '330300', '0577', 'dtx', null, '1');
INSERT INTO `sys_area` VALUES ('330324', '永嘉县', '3', '330300', '0577', 'yjx', null, '1');
INSERT INTO `sys_area` VALUES ('330326', '平阳县', '3', '330300', '0577', 'pyx', null, '1');
INSERT INTO `sys_area` VALUES ('330327', '苍南县', '3', '330300', '0577', 'cnx', null, '1');
INSERT INTO `sys_area` VALUES ('330328', '文成县', '3', '330300', '0577', 'wcx', null, '1');
INSERT INTO `sys_area` VALUES ('330329', '泰顺县', '3', '330300', '0577', 'tsx', null, '1');
INSERT INTO `sys_area` VALUES ('330381', '瑞安市', '3', '330300', '0577', 'ras', null, '1');
INSERT INTO `sys_area` VALUES ('330382', '乐清市', '3', '330300', '0577', 'lqs', null, '1');
INSERT INTO `sys_area` VALUES ('330400', '嘉兴市', '2', '330000', null, 'jxs', null, '1');
INSERT INTO `sys_area` VALUES ('330401', '市辖区', '3', '330400', '0573', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('330402', '南湖区', '3', '330400', '0573', 'nhq', null, '1');
INSERT INTO `sys_area` VALUES ('330411', '秀洲区', '3', '330400', '0573', 'xzq', null, '1');
INSERT INTO `sys_area` VALUES ('330421', '嘉善县', '3', '330400', '0573', 'jsx', null, '1');
INSERT INTO `sys_area` VALUES ('330424', '海盐县', '3', '330400', '0573', 'hyx', null, '1');
INSERT INTO `sys_area` VALUES ('330481', '海宁市', '3', '330400', '0573', 'hns', null, '1');
INSERT INTO `sys_area` VALUES ('330482', '平湖市', '3', '330400', '0573', 'phs', null, '1');
INSERT INTO `sys_area` VALUES ('330483', '桐乡市', '3', '330400', '0573', 'txs', null, '1');
INSERT INTO `sys_area` VALUES ('330500', '湖州市', '2', '330000', null, 'hzs', null, '1');
INSERT INTO `sys_area` VALUES ('330501', '市辖区', '3', '330500', '0572', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('330502', '吴兴区', '3', '330500', '0572', 'wxq', null, '1');
INSERT INTO `sys_area` VALUES ('330503', '南浔区', '3', '330500', '0572', 'nxq', null, '1');
INSERT INTO `sys_area` VALUES ('330521', '德清县', '3', '330500', '0572', 'dqx', null, '1');
INSERT INTO `sys_area` VALUES ('330522', '长兴县', '3', '330500', '0572', 'zxx', null, '1');
INSERT INTO `sys_area` VALUES ('330523', '安吉县', '3', '330500', '0572', 'ajx', null, '1');
INSERT INTO `sys_area` VALUES ('330600', '绍兴市', '2', '330000', null, 'sxs', null, '1');
INSERT INTO `sys_area` VALUES ('330601', '市辖区', '3', '330600', '0575', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('330602', '越城区', '3', '330600', '0575', 'ycq', null, '1');
INSERT INTO `sys_area` VALUES ('330621', '绍兴县', '3', '330600', '0575', 'sxx', null, '1');
INSERT INTO `sys_area` VALUES ('330624', '新昌县', '3', '330600', '0575', 'xcx', null, '1');
INSERT INTO `sys_area` VALUES ('330681', '诸暨市', '3', '330600', '0575', 'zjs', null, '1');
INSERT INTO `sys_area` VALUES ('330682', '上虞市', '3', '330600', '0575', 'sys', null, '1');
INSERT INTO `sys_area` VALUES ('330683', '嵊州市', '3', '330600', '0575', 'szs', null, '1');
INSERT INTO `sys_area` VALUES ('330700', '金华市', '2', '330000', null, 'jhs', null, '1');
INSERT INTO `sys_area` VALUES ('330701', '市辖区', '3', '330700', '0579', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('330702', '婺城区', '3', '330700', '0579', 'wcq', null, '1');
INSERT INTO `sys_area` VALUES ('330703', '金东区', '3', '330700', '0579', 'jdq', null, '1');
INSERT INTO `sys_area` VALUES ('330723', '武义县', '3', '330700', '0579', 'wyx', null, '1');
INSERT INTO `sys_area` VALUES ('330726', '浦江县', '3', '330700', '0579', 'pjx', null, '1');
INSERT INTO `sys_area` VALUES ('330727', '磐安县', '3', '330700', '0579', 'pax', null, '1');
INSERT INTO `sys_area` VALUES ('330781', '兰溪市', '3', '330700', '0579', 'lxs', null, '1');
INSERT INTO `sys_area` VALUES ('330782', '义乌市', '3', '330700', '0579', 'yws', null, '1');
INSERT INTO `sys_area` VALUES ('330783', '东阳市', '3', '330700', '0579', 'dys', null, '1');
INSERT INTO `sys_area` VALUES ('330784', '永康市', '3', '330700', '0579', 'yks', null, '1');
INSERT INTO `sys_area` VALUES ('330800', '衢州市', '2', '330000', null, 'qzs', null, '1');
INSERT INTO `sys_area` VALUES ('330801', '市辖区', '3', '330800', '0570', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('330802', '柯城区', '3', '330800', '0570', 'kcq', null, '1');
INSERT INTO `sys_area` VALUES ('330803', '衢江区', '3', '330800', '0570', 'qjq', null, '1');
INSERT INTO `sys_area` VALUES ('330822', '常山县', '3', '330800', '0570', 'csx', null, '1');
INSERT INTO `sys_area` VALUES ('330824', '开化县', '3', '330800', '0570', 'khx', null, '1');
INSERT INTO `sys_area` VALUES ('330825', '龙游县', '3', '330800', '0570', 'lyx', null, '1');
INSERT INTO `sys_area` VALUES ('330881', '江山市', '3', '330800', '0570', 'jss', null, '1');
INSERT INTO `sys_area` VALUES ('330900', '舟山市', '2', '330000', null, 'zss', null, '1');
INSERT INTO `sys_area` VALUES ('330901', '市辖区', '3', '330900', '0580', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('330902', '定海区', '3', '330900', '0580', 'dhq', null, '1');
INSERT INTO `sys_area` VALUES ('330903', '普陀区', '3', '330900', '0580', 'ptq', null, '1');
INSERT INTO `sys_area` VALUES ('330921', '岱山县', '3', '330900', '0580', 'dsx', null, '1');
INSERT INTO `sys_area` VALUES ('330922', '嵊泗县', '3', '330900', '0580', 'ssx', null, '1');
INSERT INTO `sys_area` VALUES ('331000', '台州市', '2', '330000', null, 'tzs', null, '1');
INSERT INTO `sys_area` VALUES ('331001', '市辖区', '3', '331000', '0576', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('331002', '椒江区', '3', '331000', '0576', 'jjq', null, '1');
INSERT INTO `sys_area` VALUES ('331003', '黄岩区', '3', '331000', '0576', 'hyq', null, '1');
INSERT INTO `sys_area` VALUES ('331004', '路桥区', '3', '331000', '0576', 'lqq', null, '1');
INSERT INTO `sys_area` VALUES ('331021', '玉环县', '3', '331000', '0576', 'yhx', null, '1');
INSERT INTO `sys_area` VALUES ('331022', '三门县', '3', '331000', '0576', 'smx', null, '1');
INSERT INTO `sys_area` VALUES ('331023', '天台县', '3', '331000', '0576', 'ttx', null, '1');
INSERT INTO `sys_area` VALUES ('331024', '仙居县', '3', '331000', '0576', 'xjx', null, '1');
INSERT INTO `sys_area` VALUES ('331081', '温岭市', '3', '331000', '0576', 'wls', null, '1');
INSERT INTO `sys_area` VALUES ('331082', '临海市', '3', '331000', '0576', 'lhs', null, '1');
INSERT INTO `sys_area` VALUES ('331100', '丽水市', '2', '330000', null, 'lss', null, '1');
INSERT INTO `sys_area` VALUES ('331101', '市辖区', '3', '331100', '0578', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('331102', '莲都区', '3', '331100', '0578', 'ldq', null, '1');
INSERT INTO `sys_area` VALUES ('331121', '青田县', '3', '331100', '0578', 'qtx', null, '1');
INSERT INTO `sys_area` VALUES ('331122', '缙云县', '3', '331100', '0578', 'jyx', null, '1');
INSERT INTO `sys_area` VALUES ('331123', '遂昌县', '3', '331100', '0578', 'scx', null, '1');
INSERT INTO `sys_area` VALUES ('331124', '松阳县', '3', '331100', '0578', 'syx', null, '1');
INSERT INTO `sys_area` VALUES ('331125', '云和县', '3', '331100', '0578', 'yhx', null, '1');
INSERT INTO `sys_area` VALUES ('331126', '庆元县', '3', '331100', '0578', 'qyx', null, '1');
INSERT INTO `sys_area` VALUES ('331127', '景宁畲族自治县', '3', '331100', '0578', 'jnszzzx', null, '1');
INSERT INTO `sys_area` VALUES ('331181', '龙泉市', '3', '331100', '0578', 'lqs', null, '1');
INSERT INTO `sys_area` VALUES ('340000', '安徽省', '1', '0', null, 'ahs', null, '1');
INSERT INTO `sys_area` VALUES ('340100', '合肥市', '2', '340000', null, 'hfs', null, '1');
INSERT INTO `sys_area` VALUES ('340101', '市辖区', '3', '340100', '0551', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('340102', '瑶海区', '3', '340100', '0551', 'yhq', null, '1');
INSERT INTO `sys_area` VALUES ('340103', '庐阳区', '3', '340100', '0551', 'lyq', null, '1');
INSERT INTO `sys_area` VALUES ('340104', '蜀山区', '3', '340100', '0551', 'ssq', null, '1');
INSERT INTO `sys_area` VALUES ('340111', '包河区', '3', '340100', '0551', 'bhq', null, '1');
INSERT INTO `sys_area` VALUES ('340121', '长丰县', '3', '340100', '0551', 'zfx', null, '1');
INSERT INTO `sys_area` VALUES ('340122', '肥东县', '3', '340100', '0551', 'fdx', null, '1');
INSERT INTO `sys_area` VALUES ('340123', '肥西县', '3', '340100', '0551', 'fxx', null, '1');
INSERT INTO `sys_area` VALUES ('340124', '庐江县', '3', '340100', null, 'ljx', null, '1');
INSERT INTO `sys_area` VALUES ('340181', '巢湖市', '3', '340100', null, 'chs', null, '1');
INSERT INTO `sys_area` VALUES ('340200', '芜湖市', '2', '340000', null, 'whs', null, '1');
INSERT INTO `sys_area` VALUES ('340201', '市辖区', '3', '340200', '0553', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('340202', '镜湖区', '3', '340200', '0553', 'jhq', null, '1');
INSERT INTO `sys_area` VALUES ('340203', '弋江区', '3', '340200', '0553', 'yjq', null, '1');
INSERT INTO `sys_area` VALUES ('340207', '鸠江区', '3', '340200', '0553', 'jjq', null, '1');
INSERT INTO `sys_area` VALUES ('340208', '三山区', '3', '340200', null, 'ssq', null, '1');
INSERT INTO `sys_area` VALUES ('340221', '芜湖县', '3', '340200', '0553', 'whx', null, '1');
INSERT INTO `sys_area` VALUES ('340222', '繁昌县', '3', '340200', '0553', 'fcx', null, '1');
INSERT INTO `sys_area` VALUES ('340223', '南陵县', '3', '340200', '0553', 'nlx', null, '1');
INSERT INTO `sys_area` VALUES ('340225', '无为县', '3', '340200', null, 'wwx', null, '1');
INSERT INTO `sys_area` VALUES ('340300', '蚌埠市', '2', '340000', null, 'bbs', null, '1');
INSERT INTO `sys_area` VALUES ('340301', '市辖区', '3', '340300', '0552', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('340302', '龙子湖区', '3', '340300', '0552', 'lzhq', null, '1');
INSERT INTO `sys_area` VALUES ('340303', '蚌山区', '3', '340300', '0552', 'bsq', null, '1');
INSERT INTO `sys_area` VALUES ('340304', '禹会区', '3', '340300', '0552', 'yhq', null, '1');
INSERT INTO `sys_area` VALUES ('340311', '淮上区', '3', '340300', '0552', 'hsq', null, '1');
INSERT INTO `sys_area` VALUES ('340321', '怀远县', '3', '340300', '0552', 'hyx', null, '1');
INSERT INTO `sys_area` VALUES ('340322', '五河县', '3', '340300', '0552', 'whx', null, '1');
INSERT INTO `sys_area` VALUES ('340323', '固镇县', '3', '340300', '0552', 'gzx', null, '1');
INSERT INTO `sys_area` VALUES ('340400', '淮南市', '2', '340000', null, 'hns', null, '1');
INSERT INTO `sys_area` VALUES ('340401', '市辖区', '3', '340400', '0554', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('340402', '大通区', '3', '340400', '0554', 'dtq', null, '1');
INSERT INTO `sys_area` VALUES ('340403', '田家庵区', '3', '340400', '0554', 'tjaq', null, '1');
INSERT INTO `sys_area` VALUES ('340404', '谢家集区', '3', '340400', '0554', 'xjjq', null, '1');
INSERT INTO `sys_area` VALUES ('340405', '八公山区', '3', '340400', '0554', 'bgsq', null, '1');
INSERT INTO `sys_area` VALUES ('340406', '潘集区', '3', '340400', '0554', 'pjq', null, '1');
INSERT INTO `sys_area` VALUES ('340421', '凤台县', '3', '340400', '0554', 'ftx', null, '1');
INSERT INTO `sys_area` VALUES ('340500', '马鞍山市', '2', '340000', null, 'mass', null, '1');
INSERT INTO `sys_area` VALUES ('340501', '市辖区', '3', '340500', '0555', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('340503', '花山区', '3', '340500', '0555', 'hsq', null, '1');
INSERT INTO `sys_area` VALUES ('340504', '雨山区', '3', '340500', '0555', 'ysq', null, '1');
INSERT INTO `sys_area` VALUES ('340506', '博望区', '3', '340500', null, 'bwq', null, '1');
INSERT INTO `sys_area` VALUES ('340521', '当涂县', '3', '340500', '0555', 'dtx', null, '1');
INSERT INTO `sys_area` VALUES ('340522', '含山县', '3', '340500', null, 'hsx', null, '1');
INSERT INTO `sys_area` VALUES ('340523', '和县', '3', '340500', null, 'hx', null, '1');
INSERT INTO `sys_area` VALUES ('340600', '淮北市', '2', '340000', null, 'hbs', null, '1');
INSERT INTO `sys_area` VALUES ('340601', '市辖区', '3', '340600', '0561', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('340602', '杜集区', '3', '340600', '0561', 'djq', null, '1');
INSERT INTO `sys_area` VALUES ('340603', '相山区', '3', '340600', '0561', 'xsq', null, '1');
INSERT INTO `sys_area` VALUES ('340604', '烈山区', '3', '340600', '0561', 'lsq', null, '1');
INSERT INTO `sys_area` VALUES ('340621', '濉溪县', '3', '340600', '0561', 'sxx', null, '1');
INSERT INTO `sys_area` VALUES ('340700', '铜陵市', '2', '340000', null, 'tls', null, '1');
INSERT INTO `sys_area` VALUES ('340701', '市辖区', '3', '340700', '0562', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('340702', '铜官山区', '3', '340700', '0562', 'tgsq', null, '1');
INSERT INTO `sys_area` VALUES ('340703', '狮子山区', '3', '340700', '0562', 'szsq', null, '1');
INSERT INTO `sys_area` VALUES ('340711', '郊区', '3', '340700', '0562', 'jq', null, '1');
INSERT INTO `sys_area` VALUES ('340721', '铜陵县', '3', '340700', '0562', 'tlx', null, '1');
INSERT INTO `sys_area` VALUES ('340800', '安庆市', '2', '340000', null, 'aqs', null, '1');
INSERT INTO `sys_area` VALUES ('340801', '市辖区', '3', '340800', '0556', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('340802', '迎江区', '3', '340800', '0556', 'yjq', null, '1');
INSERT INTO `sys_area` VALUES ('340803', '大观区', '3', '340800', '0556', 'dgq', null, '1');
INSERT INTO `sys_area` VALUES ('340811', '宜秀区', '3', '340800', '0556', 'yxq', null, '1');
INSERT INTO `sys_area` VALUES ('340822', '怀宁县', '3', '340800', '0556', 'hnx', null, '1');
INSERT INTO `sys_area` VALUES ('340823', '枞阳县', '3', '340800', '0556', 'zyx', null, '1');
INSERT INTO `sys_area` VALUES ('340824', '潜山县', '3', '340800', '0556', 'qsx', null, '1');
INSERT INTO `sys_area` VALUES ('340825', '太湖县', '3', '340800', '0556', 'thx', null, '1');
INSERT INTO `sys_area` VALUES ('340826', '宿松县', '3', '340800', '0556', 'ssx', null, '1');
INSERT INTO `sys_area` VALUES ('340827', '望江县', '3', '340800', '0556', 'wjx', null, '1');
INSERT INTO `sys_area` VALUES ('340828', '岳西县', '3', '340800', '0556', 'yxx', null, '1');
INSERT INTO `sys_area` VALUES ('340881', '桐城市', '3', '340800', '0556', 'tcs', null, '1');
INSERT INTO `sys_area` VALUES ('341000', '黄山市', '2', '340000', null, 'hss', null, '1');
INSERT INTO `sys_area` VALUES ('341001', '市辖区', '3', '341000', '0559', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('341002', '屯溪区', '3', '341000', '0559', 'txq', null, '1');
INSERT INTO `sys_area` VALUES ('341003', '黄山区', '3', '341000', '0559', 'hsq', null, '1');
INSERT INTO `sys_area` VALUES ('341004', '徽州区', '3', '341000', '0559', 'hzq', null, '1');
INSERT INTO `sys_area` VALUES ('341021', '歙县', '3', '341000', '0559', 'sx', null, '1');
INSERT INTO `sys_area` VALUES ('341022', '休宁县', '3', '341000', '0559', 'xnx', null, '1');
INSERT INTO `sys_area` VALUES ('341023', '黟县', '3', '341000', '0559', 'yx', null, '1');
INSERT INTO `sys_area` VALUES ('341024', '祁门县', '3', '341000', '0559', 'qmx', null, '1');
INSERT INTO `sys_area` VALUES ('341100', '滁州市', '2', '340000', null, 'czs', null, '1');
INSERT INTO `sys_area` VALUES ('341101', '市辖区', '3', '341100', '0550', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('341102', '琅琊区', '3', '341100', '0550', 'lyq', null, '1');
INSERT INTO `sys_area` VALUES ('341103', '南谯区', '3', '341100', '0550', 'nqq', null, '1');
INSERT INTO `sys_area` VALUES ('341122', '来安县', '3', '341100', '0550', 'lax', null, '1');
INSERT INTO `sys_area` VALUES ('341124', '全椒县', '3', '341100', '0550', 'qjx', null, '1');
INSERT INTO `sys_area` VALUES ('341125', '定远县', '3', '341100', '0550', 'dyx', null, '1');
INSERT INTO `sys_area` VALUES ('341126', '凤阳县', '3', '341100', '0550', 'fyx', null, '1');
INSERT INTO `sys_area` VALUES ('341181', '天长市', '3', '341100', '0550', 'tzs', null, '1');
INSERT INTO `sys_area` VALUES ('341182', '明光市', '3', '341100', '0550', 'mgs', null, '1');
INSERT INTO `sys_area` VALUES ('341200', '阜阳市', '2', '340000', null, 'fys', null, '1');
INSERT INTO `sys_area` VALUES ('341201', '市辖区', '3', '341200', '0558', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('341202', '颍州区', '3', '341200', '0558', 'yzq', null, '1');
INSERT INTO `sys_area` VALUES ('341203', '颍东区', '3', '341200', '0558', 'ydq', null, '1');
INSERT INTO `sys_area` VALUES ('341204', '颍泉区', '3', '341200', '0558', 'yqq', null, '1');
INSERT INTO `sys_area` VALUES ('341221', '临泉县', '3', '341200', '0558', 'lqx', null, '1');
INSERT INTO `sys_area` VALUES ('341222', '太和县', '3', '341200', '0558', 'thx', null, '1');
INSERT INTO `sys_area` VALUES ('341225', '阜南县', '3', '341200', '0558', 'fnx', null, '1');
INSERT INTO `sys_area` VALUES ('341226', '颍上县', '3', '341200', '0558', 'ysx', null, '1');
INSERT INTO `sys_area` VALUES ('341282', '界首市', '3', '341200', '0558', 'jss', null, '1');
INSERT INTO `sys_area` VALUES ('341300', '宿州市', '2', '340000', null, 'szs', null, '1');
INSERT INTO `sys_area` VALUES ('341301', '市辖区', '3', '341300', '0557', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('341302', '埇桥区', '3', '341300', '0557', 'yqq', null, '1');
INSERT INTO `sys_area` VALUES ('341321', '砀山县', '3', '341300', '0557', 'dsx', null, '1');
INSERT INTO `sys_area` VALUES ('341322', '萧县', '3', '341300', '0557', 'xx', null, '1');
INSERT INTO `sys_area` VALUES ('341323', '灵璧县', '3', '341300', '0557', 'lbx', null, '1');
INSERT INTO `sys_area` VALUES ('341324', '泗县', '3', '341300', '0557', 'sx', null, '1');
INSERT INTO `sys_area` VALUES ('341500', '六安市', '2', '340000', null, 'las', null, '1');
INSERT INTO `sys_area` VALUES ('341501', '市辖区', '3', '341500', '0564', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('341502', '金安区', '3', '341500', '0564', 'jaq', null, '1');
INSERT INTO `sys_area` VALUES ('341503', '裕安区', '3', '341500', '0564', 'yaq', null, '1');
INSERT INTO `sys_area` VALUES ('341521', '寿县', '3', '341500', '0564', 'sx', null, '1');
INSERT INTO `sys_area` VALUES ('341522', '霍邱县', '3', '341500', '0564', 'hqx', null, '1');
INSERT INTO `sys_area` VALUES ('341523', '舒城县', '3', '341500', '0564', 'scx', null, '1');
INSERT INTO `sys_area` VALUES ('341524', '金寨县', '3', '341500', '0564', 'jzx', null, '1');
INSERT INTO `sys_area` VALUES ('341525', '霍山县', '3', '341500', '0564', 'hsx', null, '1');
INSERT INTO `sys_area` VALUES ('341600', '亳州市', '2', '340000', null, 'bzs', null, '1');
INSERT INTO `sys_area` VALUES ('341601', '市辖区', '3', '341600', '0558', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('341602', '谯城区', '3', '341600', '0558', 'qcq', null, '1');
INSERT INTO `sys_area` VALUES ('341621', '涡阳县', '3', '341600', '0558', 'wyx', null, '1');
INSERT INTO `sys_area` VALUES ('341622', '蒙城县', '3', '341600', '0558', 'mcx', null, '1');
INSERT INTO `sys_area` VALUES ('341623', '利辛县', '3', '341600', '0558', 'lxx', null, '1');
INSERT INTO `sys_area` VALUES ('341700', '池州市', '2', '340000', null, 'czs', null, '1');
INSERT INTO `sys_area` VALUES ('341701', '市辖区', '3', '341700', '0566', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('341702', '贵池区', '3', '341700', '0566', 'gcq', null, '1');
INSERT INTO `sys_area` VALUES ('341721', '东至县', '3', '341700', '0566', 'dzx', null, '1');
INSERT INTO `sys_area` VALUES ('341722', '石台县', '3', '341700', '0566', 'stx', null, '1');
INSERT INTO `sys_area` VALUES ('341723', '青阳县', '3', '341700', '0566', 'qyx', null, '1');
INSERT INTO `sys_area` VALUES ('341800', '宣城市', '2', '340000', null, 'xcs', null, '1');
INSERT INTO `sys_area` VALUES ('341801', '市辖区', '3', '341800', '0563', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('341802', '宣州区', '3', '341800', '0563', 'xzq', null, '1');
INSERT INTO `sys_area` VALUES ('341821', '郎溪县', '3', '341800', '0563', 'lxx', null, '1');
INSERT INTO `sys_area` VALUES ('341822', '广德县', '3', '341800', '0563', 'gdx', null, '1');
INSERT INTO `sys_area` VALUES ('341823', '泾县', '3', '341800', '0563', 'jx', null, '1');
INSERT INTO `sys_area` VALUES ('341824', '绩溪县', '3', '341800', '0563', 'jxx', null, '1');
INSERT INTO `sys_area` VALUES ('341825', '旌德县', '3', '341800', '0563', 'jdx', null, '1');
INSERT INTO `sys_area` VALUES ('341881', '宁国市', '3', '341800', '0563', 'ngs', null, '1');
INSERT INTO `sys_area` VALUES ('350000', '福建省', '1', '0', null, 'fjs', null, '1');
INSERT INTO `sys_area` VALUES ('350100', '福州市', '2', '350000', null, 'fzs', null, '1');
INSERT INTO `sys_area` VALUES ('350101', '市辖区', '3', '350100', '0591', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('350102', '鼓楼区', '3', '350100', '0591', 'glq', null, '1');
INSERT INTO `sys_area` VALUES ('350103', '台江区', '3', '350100', '0591', 'tjq', null, '1');
INSERT INTO `sys_area` VALUES ('350104', '仓山区', '3', '350100', '0591', 'csq', null, '1');
INSERT INTO `sys_area` VALUES ('350105', '马尾区', '3', '350100', '0591', 'mwq', null, '1');
INSERT INTO `sys_area` VALUES ('350111', '晋安区', '3', '350100', '0591', 'jaq', null, '1');
INSERT INTO `sys_area` VALUES ('350121', '闽侯县', '3', '350100', '0591', 'mhx', null, '1');
INSERT INTO `sys_area` VALUES ('350122', '连江县', '3', '350100', '0591', 'ljx', null, '1');
INSERT INTO `sys_area` VALUES ('350123', '罗源县', '3', '350100', '0591', 'lyx', null, '1');
INSERT INTO `sys_area` VALUES ('350124', '闽清县', '3', '350100', '0591', 'mqx', null, '1');
INSERT INTO `sys_area` VALUES ('350125', '永泰县', '3', '350100', '0591', 'ytx', null, '1');
INSERT INTO `sys_area` VALUES ('350128', '平潭县', '3', '350100', '0591', 'ptx', null, '1');
INSERT INTO `sys_area` VALUES ('350181', '福清市', '3', '350100', '0591', 'fqs', null, '1');
INSERT INTO `sys_area` VALUES ('350182', '长乐市', '3', '350100', '0591', 'zls', null, '1');
INSERT INTO `sys_area` VALUES ('350200', '厦门市', '2', '350000', null, 'sms', null, '1');
INSERT INTO `sys_area` VALUES ('350201', '市辖区', '3', '350200', '0592', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('350203', '思明区', '3', '350200', '0592', 'smq', null, '1');
INSERT INTO `sys_area` VALUES ('350205', '海沧区', '3', '350200', '0592', 'hcq', null, '1');
INSERT INTO `sys_area` VALUES ('350206', '湖里区', '3', '350200', '0592', 'hlq', null, '1');
INSERT INTO `sys_area` VALUES ('350211', '集美区', '3', '350200', '0592', 'jmq', null, '1');
INSERT INTO `sys_area` VALUES ('350212', '同安区', '3', '350200', '0592', 'taq', null, '1');
INSERT INTO `sys_area` VALUES ('350213', '翔安区', '3', '350200', '0592', 'xaq', null, '1');
INSERT INTO `sys_area` VALUES ('350300', '莆田市', '2', '350000', null, 'pts', null, '1');
INSERT INTO `sys_area` VALUES ('350301', '市辖区', '3', '350300', '0594', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('350302', '城厢区', '3', '350300', '0594', 'cxq', null, '1');
INSERT INTO `sys_area` VALUES ('350303', '涵江区', '3', '350300', '0594', 'hjq', null, '1');
INSERT INTO `sys_area` VALUES ('350304', '荔城区', '3', '350300', '0594', 'lcq', null, '1');
INSERT INTO `sys_area` VALUES ('350305', '秀屿区', '3', '350300', '0594', 'xyq', null, '1');
INSERT INTO `sys_area` VALUES ('350322', '仙游县', '3', '350300', '0594', 'xyx', null, '1');
INSERT INTO `sys_area` VALUES ('350400', '三明市', '2', '350000', null, 'sms', null, '1');
INSERT INTO `sys_area` VALUES ('350401', '市辖区', '3', '350400', '0598', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('350402', '梅列区', '3', '350400', '0598', 'mlq', null, '1');
INSERT INTO `sys_area` VALUES ('350403', '三元区', '3', '350400', '0598', 'syq', null, '1');
INSERT INTO `sys_area` VALUES ('350421', '明溪县', '3', '350400', '0598', 'mxx', null, '1');
INSERT INTO `sys_area` VALUES ('350423', '清流县', '3', '350400', '0598', 'qlx', null, '1');
INSERT INTO `sys_area` VALUES ('350424', '宁化县', '3', '350400', '0598', 'nhx', null, '1');
INSERT INTO `sys_area` VALUES ('350425', '大田县', '3', '350400', '0598', 'dtx', null, '1');
INSERT INTO `sys_area` VALUES ('350426', '尤溪县', '3', '350400', '0598', 'yxx', null, '1');
INSERT INTO `sys_area` VALUES ('350427', '沙县', '3', '350400', '0598', 'sx', null, '1');
INSERT INTO `sys_area` VALUES ('350428', '将乐县', '3', '350400', '0598', 'jlx', null, '1');
INSERT INTO `sys_area` VALUES ('350429', '泰宁县', '3', '350400', '0598', 'tnx', null, '1');
INSERT INTO `sys_area` VALUES ('350430', '建宁县', '3', '350400', '0598', 'jnx', null, '1');
INSERT INTO `sys_area` VALUES ('350481', '永安市', '3', '350400', '0598', 'yas', null, '1');
INSERT INTO `sys_area` VALUES ('350500', '泉州市', '2', '350000', null, 'qzs', null, '1');
INSERT INTO `sys_area` VALUES ('350501', '市辖区', '3', '350500', '0595', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('350502', '鲤城区', '3', '350500', '0595', 'lcq', null, '1');
INSERT INTO `sys_area` VALUES ('350503', '丰泽区', '3', '350500', '0595', 'fzq', null, '1');
INSERT INTO `sys_area` VALUES ('350504', '洛江区', '3', '350500', '0595', 'ljq', null, '1');
INSERT INTO `sys_area` VALUES ('350505', '泉港区', '3', '350500', '0595', 'qgq', null, '1');
INSERT INTO `sys_area` VALUES ('350521', '惠安县', '3', '350500', '0595', 'hax', null, '1');
INSERT INTO `sys_area` VALUES ('350524', '安溪县', '3', '350500', '0595', 'axx', null, '1');
INSERT INTO `sys_area` VALUES ('350525', '永春县', '3', '350500', '0595', 'ycx', null, '1');
INSERT INTO `sys_area` VALUES ('350526', '德化县', '3', '350500', '0595', 'dhx', null, '1');
INSERT INTO `sys_area` VALUES ('350527', '金门县', '3', '350500', '0595', 'jmx', null, '1');
INSERT INTO `sys_area` VALUES ('350581', '石狮市', '3', '350500', '0595', 'sss', null, '1');
INSERT INTO `sys_area` VALUES ('350582', '晋江市', '3', '350500', '0595', 'jjs', null, '1');
INSERT INTO `sys_area` VALUES ('350583', '南安市', '3', '350500', '0595', 'nas', null, '1');
INSERT INTO `sys_area` VALUES ('350600', '漳州市', '2', '350000', null, 'zzs', null, '1');
INSERT INTO `sys_area` VALUES ('350601', '市辖区', '3', '350600', '0596', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('350602', '芗城区', '3', '350600', '0596', 'xcq', null, '1');
INSERT INTO `sys_area` VALUES ('350603', '龙文区', '3', '350600', '0596', 'lwq', null, '1');
INSERT INTO `sys_area` VALUES ('350622', '云霄县', '3', '350600', '0596', 'yxx', null, '1');
INSERT INTO `sys_area` VALUES ('350623', '漳浦县', '3', '350600', '0596', 'zpx', null, '1');
INSERT INTO `sys_area` VALUES ('350624', '诏安县', '3', '350600', '0596', 'zax', null, '1');
INSERT INTO `sys_area` VALUES ('350625', '长泰县', '3', '350600', '0596', 'ztx', null, '1');
INSERT INTO `sys_area` VALUES ('350626', '东山县', '3', '350600', '0596', 'dsx', null, '1');
INSERT INTO `sys_area` VALUES ('350627', '南靖县', '3', '350600', '0596', 'njx', null, '1');
INSERT INTO `sys_area` VALUES ('350628', '平和县', '3', '350600', '0596', 'phx', null, '1');
INSERT INTO `sys_area` VALUES ('350629', '华安县', '3', '350600', '0596', 'hax', null, '1');
INSERT INTO `sys_area` VALUES ('350681', '龙海市', '3', '350600', '0596', 'lhs', null, '1');
INSERT INTO `sys_area` VALUES ('350700', '南平市', '2', '350000', null, 'nps', null, '1');
INSERT INTO `sys_area` VALUES ('350701', '市辖区', '3', '350700', '0599', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('350702', '延平区', '3', '350700', '0599', 'ypq', null, '1');
INSERT INTO `sys_area` VALUES ('350721', '顺昌县', '3', '350700', '0599', 'scx', null, '1');
INSERT INTO `sys_area` VALUES ('350722', '浦城县', '3', '350700', '0599', 'pcx', null, '1');
INSERT INTO `sys_area` VALUES ('350723', '光泽县', '3', '350700', '0599', 'gzx', null, '1');
INSERT INTO `sys_area` VALUES ('350724', '松溪县', '3', '350700', '0599', 'sxx', null, '1');
INSERT INTO `sys_area` VALUES ('350725', '政和县', '3', '350700', '0599', 'zhx', null, '1');
INSERT INTO `sys_area` VALUES ('350781', '邵武市', '3', '350700', '0599', 'sws', null, '1');
INSERT INTO `sys_area` VALUES ('350782', '武夷山市', '3', '350700', '0599', 'wyss', null, '1');
INSERT INTO `sys_area` VALUES ('350783', '建瓯市', '3', '350700', '0599', 'jos', null, '1');
INSERT INTO `sys_area` VALUES ('350784', '建阳市', '3', '350700', '0599', 'jys', null, '1');
INSERT INTO `sys_area` VALUES ('350800', '龙岩市', '2', '350000', null, 'lys', null, '1');
INSERT INTO `sys_area` VALUES ('350801', '市辖区', '3', '350800', '0597', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('350802', '新罗区', '3', '350800', '0597', 'xlq', null, '1');
INSERT INTO `sys_area` VALUES ('350821', '长汀县', '3', '350800', '0597', 'ztx', null, '1');
INSERT INTO `sys_area` VALUES ('350822', '永定县', '3', '350800', '0597', 'ydx', null, '1');
INSERT INTO `sys_area` VALUES ('350823', '上杭县', '3', '350800', '0597', 'shx', null, '1');
INSERT INTO `sys_area` VALUES ('350824', '武平县', '3', '350800', '0597', 'wpx', null, '1');
INSERT INTO `sys_area` VALUES ('350825', '连城县', '3', '350800', '0597', 'lcx', null, '1');
INSERT INTO `sys_area` VALUES ('350881', '漳平市', '3', '350800', '0597', 'zps', null, '1');
INSERT INTO `sys_area` VALUES ('350900', '宁德市', '2', '350000', null, 'nds', null, '1');
INSERT INTO `sys_area` VALUES ('350901', '市辖区', '3', '350900', '0595', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('350902', '蕉城区', '3', '350900', '0593', 'jcq', null, '1');
INSERT INTO `sys_area` VALUES ('350921', '霞浦县', '3', '350900', '0593', 'xpx', null, '1');
INSERT INTO `sys_area` VALUES ('350922', '古田县', '3', '350900', '0593', 'gtx', null, '1');
INSERT INTO `sys_area` VALUES ('350923', '屏南县', '3', '350900', '0593', 'pnx', null, '1');
INSERT INTO `sys_area` VALUES ('350924', '寿宁县', '3', '350900', '0593', 'snx', null, '1');
INSERT INTO `sys_area` VALUES ('350925', '周宁县', '3', '350900', '0593', 'znx', null, '1');
INSERT INTO `sys_area` VALUES ('350926', '柘荣县', '3', '350900', '0593', 'zrx', null, '1');
INSERT INTO `sys_area` VALUES ('350981', '福安市', '3', '350900', '0593', 'fas', null, '1');
INSERT INTO `sys_area` VALUES ('350982', '福鼎市', '3', '350900', '0593', 'fds', null, '1');
INSERT INTO `sys_area` VALUES ('360000', '江西省', '1', '0', null, 'jxs', null, '1');
INSERT INTO `sys_area` VALUES ('360100', '南昌市', '2', '360000', null, 'ncs', null, '1');
INSERT INTO `sys_area` VALUES ('360101', '市辖区', '3', '360100', '0791', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('360102', '东湖区', '3', '360100', '0791', 'dhq', null, '1');
INSERT INTO `sys_area` VALUES ('360103', '西湖区', '3', '360100', '0791', 'xhq', null, '1');
INSERT INTO `sys_area` VALUES ('360104', '青云谱区', '3', '360100', '0791', 'qypq', null, '1');
INSERT INTO `sys_area` VALUES ('360105', '湾里区', '3', '360100', '0791', 'wlq', null, '1');
INSERT INTO `sys_area` VALUES ('360111', '青山湖区', '3', '360100', '0791', 'qshq', null, '1');
INSERT INTO `sys_area` VALUES ('360121', '南昌县', '3', '360100', '0791', 'ncx', null, '1');
INSERT INTO `sys_area` VALUES ('360122', '新建县', '3', '360100', '0791', 'xjx', null, '1');
INSERT INTO `sys_area` VALUES ('360123', '安义县', '3', '360100', '0791', 'ayx', null, '1');
INSERT INTO `sys_area` VALUES ('360124', '进贤县', '3', '360100', '0791', 'jxx', null, '1');
INSERT INTO `sys_area` VALUES ('360200', '景德镇市', '2', '360000', null, 'jdzs', null, '1');
INSERT INTO `sys_area` VALUES ('360201', '市辖区', '3', '360200', '0798', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('360202', '昌江区', '3', '360200', '0798', 'cjq', null, '1');
INSERT INTO `sys_area` VALUES ('360203', '珠山区', '3', '360200', '0798', 'zsq', null, '1');
INSERT INTO `sys_area` VALUES ('360222', '浮梁县', '3', '360200', '0798', 'flx', null, '1');
INSERT INTO `sys_area` VALUES ('360281', '乐平市', '3', '360200', '0798', 'lps', null, '1');
INSERT INTO `sys_area` VALUES ('360300', '萍乡市', '2', '360000', null, 'pxs', null, '1');
INSERT INTO `sys_area` VALUES ('360301', '市辖区', '3', '360300', '0799', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('360302', '安源区', '3', '360300', '0799', 'ayq', null, '1');
INSERT INTO `sys_area` VALUES ('360313', '湘东区', '3', '360300', '0799', 'xdq', null, '1');
INSERT INTO `sys_area` VALUES ('360321', '莲花县', '3', '360300', '0799', 'lhx', null, '1');
INSERT INTO `sys_area` VALUES ('360322', '上栗县', '3', '360300', '0799', 'slx', null, '1');
INSERT INTO `sys_area` VALUES ('360323', '芦溪县', '3', '360300', '0799', 'lxx', null, '1');
INSERT INTO `sys_area` VALUES ('360400', '九江市', '2', '360000', null, 'jjs', null, '1');
INSERT INTO `sys_area` VALUES ('360401', '市辖区', '3', '360400', '0792', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('360402', '庐山区', '3', '360400', '0792', 'lsq', null, '1');
INSERT INTO `sys_area` VALUES ('360403', '浔阳区', '3', '360400', '0792', 'xyq', null, '1');
INSERT INTO `sys_area` VALUES ('360421', '九江县', '3', '360400', '0792', 'jjx', null, '1');
INSERT INTO `sys_area` VALUES ('360423', '武宁县', '3', '360400', '0792', 'wnx', null, '1');
INSERT INTO `sys_area` VALUES ('360424', '修水县', '3', '360400', '0792', 'xsx', null, '1');
INSERT INTO `sys_area` VALUES ('360425', '永修县', '3', '360400', '0792', 'yxx', null, '1');
INSERT INTO `sys_area` VALUES ('360426', '德安县', '3', '360400', '0792', 'dax', null, '1');
INSERT INTO `sys_area` VALUES ('360427', '星子县', '3', '360400', '0792', 'xzx', null, '1');
INSERT INTO `sys_area` VALUES ('360428', '都昌县', '3', '360400', '0792', 'dcx', null, '1');
INSERT INTO `sys_area` VALUES ('360429', '湖口县', '3', '360400', '0792', 'hkx', null, '1');
INSERT INTO `sys_area` VALUES ('360430', '彭泽县', '3', '360400', '0792', 'pzx', null, '1');
INSERT INTO `sys_area` VALUES ('360481', '瑞昌市', '3', '360400', '0792', 'rcs', null, '1');
INSERT INTO `sys_area` VALUES ('360482', '共青城市', '3', '360400', null, 'gqcs', null, '1');
INSERT INTO `sys_area` VALUES ('360500', '新余市', '2', '360000', null, 'xys', null, '1');
INSERT INTO `sys_area` VALUES ('360501', '市辖区', '3', '360500', '0790', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('360502', '渝水区', '3', '360500', '0790', 'ysq', null, '1');
INSERT INTO `sys_area` VALUES ('360521', '分宜县', '3', '360500', '0790', 'fyx', null, '1');
INSERT INTO `sys_area` VALUES ('360600', '鹰潭市', '2', '360000', null, 'yts', null, '1');
INSERT INTO `sys_area` VALUES ('360601', '市辖区', '3', '360600', '0701', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('360602', '月湖区', '3', '360600', '0701', 'yhq', null, '1');
INSERT INTO `sys_area` VALUES ('360622', '余江县', '3', '360600', '0701', 'yjx', null, '1');
INSERT INTO `sys_area` VALUES ('360681', '贵溪市', '3', '360600', '0701', 'gxs', null, '1');
INSERT INTO `sys_area` VALUES ('360700', '赣州市', '2', '360000', null, 'gzs', null, '1');
INSERT INTO `sys_area` VALUES ('360701', '市辖区', '3', '360700', '0797', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('360702', '章贡区', '3', '360700', '0797', 'zgq', null, '1');
INSERT INTO `sys_area` VALUES ('360721', '赣县', '3', '360700', '0797', 'gx', null, '1');
INSERT INTO `sys_area` VALUES ('360722', '信丰县', '3', '360700', '0797', 'xfx', null, '1');
INSERT INTO `sys_area` VALUES ('360723', '大余县', '3', '360700', '0797', 'dyx', null, '1');
INSERT INTO `sys_area` VALUES ('360724', '上犹县', '3', '360700', '0797', 'syx', null, '1');
INSERT INTO `sys_area` VALUES ('360725', '崇义县', '3', '360700', '0797', 'cyx', null, '1');
INSERT INTO `sys_area` VALUES ('360726', '安远县', '3', '360700', '0797', 'ayx', null, '1');
INSERT INTO `sys_area` VALUES ('360727', '龙南县', '3', '360700', '0797', 'lnx', null, '1');
INSERT INTO `sys_area` VALUES ('360728', '定南县', '3', '360700', '0797', 'dnx', null, '1');
INSERT INTO `sys_area` VALUES ('360729', '全南县', '3', '360700', '0797', 'qnx', null, '1');
INSERT INTO `sys_area` VALUES ('360730', '宁都县', '3', '360700', '0797', 'ndx', null, '1');
INSERT INTO `sys_area` VALUES ('360731', '于都县', '3', '360700', '0797', 'ydx', null, '1');
INSERT INTO `sys_area` VALUES ('360732', '兴国县', '3', '360700', '0797', 'xgx', null, '1');
INSERT INTO `sys_area` VALUES ('360733', '会昌县', '3', '360700', '0797', 'hcx', null, '1');
INSERT INTO `sys_area` VALUES ('360734', '寻乌县', '3', '360700', '0797', 'xwx', null, '1');
INSERT INTO `sys_area` VALUES ('360735', '石城县', '3', '360700', '0797', 'scx', null, '1');
INSERT INTO `sys_area` VALUES ('360781', '瑞金市', '3', '360700', '0797', 'rjs', null, '1');
INSERT INTO `sys_area` VALUES ('360782', '南康市', '3', '360700', '0797', 'nks', null, '1');
INSERT INTO `sys_area` VALUES ('360800', '吉安市', '2', '360000', null, 'jas', null, '1');
INSERT INTO `sys_area` VALUES ('360801', '市辖区', '3', '360800', '0796', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('360802', '吉州区', '3', '360800', '0796', 'jzq', null, '1');
INSERT INTO `sys_area` VALUES ('360803', '青原区', '3', '360800', '0796', 'qyq', null, '1');
INSERT INTO `sys_area` VALUES ('360821', '吉安县', '3', '360800', '0796', 'jax', null, '1');
INSERT INTO `sys_area` VALUES ('360822', '吉水县', '3', '360800', '0796', 'jsx', null, '1');
INSERT INTO `sys_area` VALUES ('360823', '峡江县', '3', '360800', '0796', 'xjx', null, '1');
INSERT INTO `sys_area` VALUES ('360824', '新干县', '3', '360800', '0796', 'xgx', null, '1');
INSERT INTO `sys_area` VALUES ('360825', '永丰县', '3', '360800', '0796', 'yfx', null, '1');
INSERT INTO `sys_area` VALUES ('360826', '泰和县', '3', '360800', '0796', 'thx', null, '1');
INSERT INTO `sys_area` VALUES ('360827', '遂川县', '3', '360800', '0796', 'scx', null, '1');
INSERT INTO `sys_area` VALUES ('360828', '万安县', '3', '360800', '0796', 'wax', null, '1');
INSERT INTO `sys_area` VALUES ('360829', '安福县', '3', '360800', '0796', 'afx', null, '1');
INSERT INTO `sys_area` VALUES ('360830', '永新县', '3', '360800', '0796', 'yxx', null, '1');
INSERT INTO `sys_area` VALUES ('360881', '井冈山市', '3', '360800', '0796', 'jgss', null, '1');
INSERT INTO `sys_area` VALUES ('360900', '宜春市', '2', '360000', null, 'ycs', null, '1');
INSERT INTO `sys_area` VALUES ('360901', '市辖区', '3', '360900', '0795', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('360902', '袁州区', '3', '360900', '0795', 'yzq', null, '1');
INSERT INTO `sys_area` VALUES ('360921', '奉新县', '3', '360900', '0795', 'fxx', null, '1');
INSERT INTO `sys_area` VALUES ('360922', '万载县', '3', '360900', '0795', 'wzx', null, '1');
INSERT INTO `sys_area` VALUES ('360923', '上高县', '3', '360900', '0795', 'sgx', null, '1');
INSERT INTO `sys_area` VALUES ('360924', '宜丰县', '3', '360900', '0795', 'yfx', null, '1');
INSERT INTO `sys_area` VALUES ('360925', '靖安县', '3', '360900', '0795', 'jax', null, '1');
INSERT INTO `sys_area` VALUES ('360926', '铜鼓县', '3', '360900', '0795', 'tgx', null, '1');
INSERT INTO `sys_area` VALUES ('360981', '丰城市', '3', '360900', '0795', 'fcs', null, '1');
INSERT INTO `sys_area` VALUES ('360982', '樟树市', '3', '360900', '0795', 'zss', null, '1');
INSERT INTO `sys_area` VALUES ('360983', '高安市', '3', '360900', '0795', 'gas', null, '1');
INSERT INTO `sys_area` VALUES ('361000', '抚州市', '2', '360000', null, 'fzs', null, '1');
INSERT INTO `sys_area` VALUES ('361001', '市辖区', '3', '361000', '0794', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('361002', '临川区', '3', '361000', '0794', 'lcq', null, '1');
INSERT INTO `sys_area` VALUES ('361021', '南城县', '3', '361000', '0794', 'ncx', null, '1');
INSERT INTO `sys_area` VALUES ('361022', '黎川县', '3', '361000', '0794', 'lcx', null, '1');
INSERT INTO `sys_area` VALUES ('361023', '南丰县', '3', '361000', '0794', 'nfx', null, '1');
INSERT INTO `sys_area` VALUES ('361024', '崇仁县', '3', '361000', '0794', 'crx', null, '1');
INSERT INTO `sys_area` VALUES ('361025', '乐安县', '3', '361000', '0794', 'lax', null, '1');
INSERT INTO `sys_area` VALUES ('361026', '宜黄县', '3', '361000', '0794', 'yhx', null, '1');
INSERT INTO `sys_area` VALUES ('361027', '金溪县', '3', '361000', '0794', 'jxx', null, '1');
INSERT INTO `sys_area` VALUES ('361028', '资溪县', '3', '361000', '0794', 'zxx', null, '1');
INSERT INTO `sys_area` VALUES ('361029', '东乡县', '3', '361000', '0794', 'dxx', null, '1');
INSERT INTO `sys_area` VALUES ('361030', '广昌县', '3', '361000', '0794', 'gcx', null, '1');
INSERT INTO `sys_area` VALUES ('361100', '上饶市', '2', '360000', null, 'srs', null, '1');
INSERT INTO `sys_area` VALUES ('361101', '市辖区', '3', '361100', '0793', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('361102', '信州区', '3', '361100', '0793', 'xzq', null, '1');
INSERT INTO `sys_area` VALUES ('361121', '上饶县', '3', '361100', '0793', 'srx', null, '1');
INSERT INTO `sys_area` VALUES ('361122', '广丰县', '3', '361100', '0793', 'gfx', null, '1');
INSERT INTO `sys_area` VALUES ('361123', '玉山县', '3', '361100', '0793', 'ysx', null, '1');
INSERT INTO `sys_area` VALUES ('361124', '铅山县', '3', '361100', '0793', 'qsx', null, '1');
INSERT INTO `sys_area` VALUES ('361125', '横峰县', '3', '361100', '0793', 'hfx', null, '1');
INSERT INTO `sys_area` VALUES ('361126', '弋阳县', '3', '361100', '0793', 'yyx', null, '1');
INSERT INTO `sys_area` VALUES ('361127', '余干县', '3', '361100', '0793', 'ygx', null, '1');
INSERT INTO `sys_area` VALUES ('361128', '鄱阳县', '3', '361100', '0793', 'pyx', null, '1');
INSERT INTO `sys_area` VALUES ('361129', '万年县', '3', '361100', '0793', 'wnx', null, '1');
INSERT INTO `sys_area` VALUES ('361130', '婺源县', '3', '361100', '0793', 'wyx', null, '1');
INSERT INTO `sys_area` VALUES ('361181', '德兴市', '3', '361100', '0793', 'dxs', null, '1');
INSERT INTO `sys_area` VALUES ('370000', '山东省', '1', '0', null, 'sds', null, '1');
INSERT INTO `sys_area` VALUES ('370100', '济南市', '2', '370000', null, 'jns', null, '1');
INSERT INTO `sys_area` VALUES ('370101', '市辖区', '3', '370100', '0531', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('370102', '历下区', '3', '370100', '0531', 'lxq', null, '1');
INSERT INTO `sys_area` VALUES ('370103', '市中区', '3', '370100', '0531', 'szq', null, '1');
INSERT INTO `sys_area` VALUES ('370104', '槐荫区', '3', '370100', '0531', 'hyq', null, '1');
INSERT INTO `sys_area` VALUES ('370105', '天桥区', '3', '370100', '0531', 'tqq', null, '1');
INSERT INTO `sys_area` VALUES ('370112', '历城区', '3', '370100', '0531', 'lcq', null, '1');
INSERT INTO `sys_area` VALUES ('370113', '长清区', '3', '370100', '0531', 'zqq', null, '1');
INSERT INTO `sys_area` VALUES ('370124', '平阴县', '3', '370100', '0531', 'pyx', null, '1');
INSERT INTO `sys_area` VALUES ('370125', '济阳县', '3', '370100', '0531', 'jyx', null, '1');
INSERT INTO `sys_area` VALUES ('370126', '商河县', '3', '370100', '0531', 'shx', null, '1');
INSERT INTO `sys_area` VALUES ('370181', '章丘市', '3', '370100', '0531', 'zqs', null, '1');
INSERT INTO `sys_area` VALUES ('370200', '青岛市', '2', '370000', null, 'qds', null, '1');
INSERT INTO `sys_area` VALUES ('370201', '市辖区', '3', '370200', '0532', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('370202', '市南区', '3', '370200', '0532', 'snq', null, '1');
INSERT INTO `sys_area` VALUES ('370203', '市北区', '3', '370200', '0532', 'sbq', null, '1');
INSERT INTO `sys_area` VALUES ('370205', '四方区', '3', '370200', '0532', 'sfq', null, '1');
INSERT INTO `sys_area` VALUES ('370211', '黄岛区', '3', '370200', '0532', 'hdq', null, '1');
INSERT INTO `sys_area` VALUES ('370212', '崂山区', '3', '370200', '0532', 'lsq', null, '1');
INSERT INTO `sys_area` VALUES ('370213', '李沧区', '3', '370200', '0532', 'lcq', null, '1');
INSERT INTO `sys_area` VALUES ('370214', '城阳区', '3', '370200', '0532', 'cyq', null, '1');
INSERT INTO `sys_area` VALUES ('370281', '胶州市', '3', '370200', '0532', 'jzs', null, '1');
INSERT INTO `sys_area` VALUES ('370282', '即墨市', '3', '370200', '0532', 'jms', null, '1');
INSERT INTO `sys_area` VALUES ('370283', '平度市', '3', '370200', '0532', 'pds', null, '1');
INSERT INTO `sys_area` VALUES ('370284', '胶南市', '3', '370200', '0532', 'jns', null, '1');
INSERT INTO `sys_area` VALUES ('370285', '莱西市', '3', '370200', '0532', 'lxs', null, '1');
INSERT INTO `sys_area` VALUES ('370300', '淄博市', '2', '370000', null, 'zbs', null, '1');
INSERT INTO `sys_area` VALUES ('370301', '市辖区', '3', '370300', '0533', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('370302', '淄川区', '3', '370300', '0533', 'zcq', null, '1');
INSERT INTO `sys_area` VALUES ('370303', '张店区', '3', '370300', '0533', 'zdq', null, '1');
INSERT INTO `sys_area` VALUES ('370304', '博山区', '3', '370300', '0533', 'bsq', null, '1');
INSERT INTO `sys_area` VALUES ('370305', '临淄区', '3', '370300', '0533', 'lzq', null, '1');
INSERT INTO `sys_area` VALUES ('370306', '周村区', '3', '370300', '0533', 'zcq', null, '1');
INSERT INTO `sys_area` VALUES ('370321', '桓台县', '3', '370300', '0533', 'htx', null, '1');
INSERT INTO `sys_area` VALUES ('370322', '高青县', '3', '370300', '0533', 'gqx', null, '1');
INSERT INTO `sys_area` VALUES ('370323', '沂源县', '3', '370300', '0533', 'yyx', null, '1');
INSERT INTO `sys_area` VALUES ('370400', '枣庄市', '2', '370000', null, 'zzs', null, '1');
INSERT INTO `sys_area` VALUES ('370401', '市辖区', '3', '370400', '0632', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('370402', '市中区', '3', '370400', '0632', 'szq', null, '1');
INSERT INTO `sys_area` VALUES ('370403', '薛城区', '3', '370400', '0632', 'xcq', null, '1');
INSERT INTO `sys_area` VALUES ('370404', '峄城区', '3', '370400', '0632', 'ycq', null, '1');
INSERT INTO `sys_area` VALUES ('370405', '台儿庄区', '3', '370400', '0632', 'tezq', null, '1');
INSERT INTO `sys_area` VALUES ('370406', '山亭区', '3', '370400', '0632', 'stq', null, '1');
INSERT INTO `sys_area` VALUES ('370481', '滕州市', '3', '370400', '0632', 'tzs', null, '1');
INSERT INTO `sys_area` VALUES ('370500', '东营市', '2', '370000', null, 'dys', null, '1');
INSERT INTO `sys_area` VALUES ('370501', '市辖区', '3', '370500', '0546', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('370502', '东营区', '3', '370500', '0546', 'dyq', null, '1');
INSERT INTO `sys_area` VALUES ('370503', '河口区', '3', '370500', '0546', 'hkq', null, '1');
INSERT INTO `sys_area` VALUES ('370521', '垦利县', '3', '370500', '0546', 'klx', null, '1');
INSERT INTO `sys_area` VALUES ('370522', '利津县', '3', '370500', '0546', 'ljx', null, '1');
INSERT INTO `sys_area` VALUES ('370523', '广饶县', '3', '370500', '0546', 'grx', null, '1');
INSERT INTO `sys_area` VALUES ('370600', '烟台市', '2', '370000', null, 'yts', null, '1');
INSERT INTO `sys_area` VALUES ('370601', '市辖区', '3', '370600', '0535', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('370602', '芝罘区', '3', '370600', '0535', 'zfq', null, '1');
INSERT INTO `sys_area` VALUES ('370611', '福山区', '3', '370600', '0535', 'fsq', null, '1');
INSERT INTO `sys_area` VALUES ('370612', '牟平区', '3', '370600', '0535', 'mpq', null, '1');
INSERT INTO `sys_area` VALUES ('370613', '莱山区', '3', '370600', '0535', 'lsq', null, '1');
INSERT INTO `sys_area` VALUES ('370634', '长岛县', '3', '370600', '0535', 'zdx', null, '1');
INSERT INTO `sys_area` VALUES ('370681', '龙口市', '3', '370600', '0535', 'lks', null, '1');
INSERT INTO `sys_area` VALUES ('370682', '莱阳市', '3', '370600', '0535', 'lys', null, '1');
INSERT INTO `sys_area` VALUES ('370683', '莱州市', '3', '370600', '0535', 'lzs', null, '1');
INSERT INTO `sys_area` VALUES ('370684', '蓬莱市', '3', '370600', '0535', 'pls', null, '1');
INSERT INTO `sys_area` VALUES ('370685', '招远市', '3', '370600', '0535', 'zys', null, '1');
INSERT INTO `sys_area` VALUES ('370686', '栖霞市', '3', '370600', '0535', 'qxs', null, '1');
INSERT INTO `sys_area` VALUES ('370687', '海阳市', '3', '370600', '0535', 'hys', null, '1');
INSERT INTO `sys_area` VALUES ('370700', '潍坊市', '2', '370000', null, 'wfs', null, '1');
INSERT INTO `sys_area` VALUES ('370701', '市辖区', '3', '370700', '0536', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('370702', '潍城区', '3', '370700', '0536', 'wcq', null, '1');
INSERT INTO `sys_area` VALUES ('370703', '寒亭区', '3', '370700', '0536', 'htq', null, '1');
INSERT INTO `sys_area` VALUES ('370704', '坊子区', '3', '370700', '0536', 'fzq', null, '1');
INSERT INTO `sys_area` VALUES ('370705', '奎文区', '3', '370700', '0536', 'kwq', null, '1');
INSERT INTO `sys_area` VALUES ('370724', '临朐县', '3', '370700', '0536', 'lqx', null, '1');
INSERT INTO `sys_area` VALUES ('370725', '昌乐县', '3', '370700', '0536', 'clx', null, '1');
INSERT INTO `sys_area` VALUES ('370781', '青州市', '3', '370700', '0536', 'qzs', null, '1');
INSERT INTO `sys_area` VALUES ('370782', '诸城市', '3', '370700', '0536', 'zcs', null, '1');
INSERT INTO `sys_area` VALUES ('370783', '寿光市', '3', '370700', '0536', 'sgs', null, '1');
INSERT INTO `sys_area` VALUES ('370784', '安丘市', '3', '370700', '0536', 'aqs', null, '1');
INSERT INTO `sys_area` VALUES ('370785', '高密市', '3', '370700', '0536', 'gms', null, '1');
INSERT INTO `sys_area` VALUES ('370786', '昌邑市', '3', '370700', '0536', 'cys', null, '1');
INSERT INTO `sys_area` VALUES ('370800', '济宁市', '2', '370000', null, 'jns', null, '1');
INSERT INTO `sys_area` VALUES ('370801', '市辖区', '3', '370800', '0537', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('370802', '市中区', '3', '370800', '0537', 'szq', null, '1');
INSERT INTO `sys_area` VALUES ('370811', '任城区', '3', '370800', '0537', 'rcq', null, '1');
INSERT INTO `sys_area` VALUES ('370826', '微山县', '3', '370800', '0537', 'wsx', null, '1');
INSERT INTO `sys_area` VALUES ('370827', '鱼台县', '3', '370800', '0537', 'ytx', null, '1');
INSERT INTO `sys_area` VALUES ('370828', '金乡县', '3', '370800', '0537', 'jxx', null, '1');
INSERT INTO `sys_area` VALUES ('370829', '嘉祥县', '3', '370800', '0537', 'jxx', null, '1');
INSERT INTO `sys_area` VALUES ('370830', '汶上县', '3', '370800', '0537', 'wsx', null, '1');
INSERT INTO `sys_area` VALUES ('370831', '泗水县', '3', '370800', '0537', 'ssx', null, '1');
INSERT INTO `sys_area` VALUES ('370832', '梁山县', '3', '370800', '0537', 'lsx', null, '1');
INSERT INTO `sys_area` VALUES ('370881', '曲阜市', '3', '370800', '0537', 'qfs', null, '1');
INSERT INTO `sys_area` VALUES ('370882', '兖州市', '3', '370800', '0537', 'yzs', null, '1');
INSERT INTO `sys_area` VALUES ('370883', '邹城市', '3', '370800', '0537', 'zcs', null, '1');
INSERT INTO `sys_area` VALUES ('370900', '泰安市', '2', '370000', null, 'tas', null, '1');
INSERT INTO `sys_area` VALUES ('370901', '市辖区', '3', '370900', '0538', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('370902', '泰山区', '3', '370900', '0538', 'tsq', null, '1');
INSERT INTO `sys_area` VALUES ('370911', '岱岳区', '3', '370900', null, 'dyq', null, '1');
INSERT INTO `sys_area` VALUES ('370921', '宁阳县', '3', '370900', '0538', 'nyx', null, '1');
INSERT INTO `sys_area` VALUES ('370923', '东平县', '3', '370900', '0538', 'dpx', null, '1');
INSERT INTO `sys_area` VALUES ('370982', '新泰市', '3', '370900', '0538', 'xts', null, '1');
INSERT INTO `sys_area` VALUES ('370983', '肥城市', '3', '370900', '0538', 'fcs', null, '1');
INSERT INTO `sys_area` VALUES ('371000', '威海市', '2', '370000', null, 'whs', null, '1');
INSERT INTO `sys_area` VALUES ('371001', '市辖区', '3', '371000', '0631', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('371002', '环翠区', '3', '371000', '0631', 'hcq', null, '1');
INSERT INTO `sys_area` VALUES ('371081', '文登市', '3', '371000', '0631', 'wds', null, '1');
INSERT INTO `sys_area` VALUES ('371082', '荣成市', '3', '371000', '0631', 'rcs', null, '1');
INSERT INTO `sys_area` VALUES ('371083', '乳山市', '3', '371000', '0631', 'rss', null, '1');
INSERT INTO `sys_area` VALUES ('371100', '日照市', '2', '370000', null, 'rzs', null, '1');
INSERT INTO `sys_area` VALUES ('371101', '市辖区', '3', '371100', '0633', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('371102', '东港区', '3', '371100', '0633', 'dgq', null, '1');
INSERT INTO `sys_area` VALUES ('371103', '岚山区', '3', '371100', '0633', 'lsq', null, '1');
INSERT INTO `sys_area` VALUES ('371121', '五莲县', '3', '371100', '0633', 'wlx', null, '1');
INSERT INTO `sys_area` VALUES ('371122', '莒县', '3', '371100', '0633', 'jx', null, '1');
INSERT INTO `sys_area` VALUES ('371200', '莱芜市', '2', '370000', null, 'lws', null, '1');
INSERT INTO `sys_area` VALUES ('371201', '市辖区', '3', '371200', '0634', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('371202', '莱城区', '3', '371200', '0634', 'lcq', null, '1');
INSERT INTO `sys_area` VALUES ('371203', '钢城区', '3', '371200', '0634', 'gcq', null, '1');
INSERT INTO `sys_area` VALUES ('371300', '临沂市', '2', '370000', null, 'lys', null, '1');
INSERT INTO `sys_area` VALUES ('371301', '市辖区', '3', '371300', '0539', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('371302', '兰山区', '3', '371300', '0539', 'lsq', null, '1');
INSERT INTO `sys_area` VALUES ('371311', '罗庄区', '3', '371300', '0539', 'lzq', null, '1');
INSERT INTO `sys_area` VALUES ('371312', '河东区', '3', '371300', '0539', 'hdq', null, '1');
INSERT INTO `sys_area` VALUES ('371321', '沂南县', '3', '371300', '0539', 'ynx', null, '1');
INSERT INTO `sys_area` VALUES ('371322', '郯城县', '3', '371300', '0539', 'tcx', null, '1');
INSERT INTO `sys_area` VALUES ('371323', '沂水县', '3', '371300', '0539', 'ysx', null, '1');
INSERT INTO `sys_area` VALUES ('371324', '苍山县', '3', '371300', '0539', 'csx', null, '1');
INSERT INTO `sys_area` VALUES ('371325', '费县', '3', '371300', '0539', 'fx', null, '1');
INSERT INTO `sys_area` VALUES ('371326', '平邑县', '3', '371300', '0539', 'pyx', null, '1');
INSERT INTO `sys_area` VALUES ('371327', '莒南县', '3', '371300', '0539', 'jnx', null, '1');
INSERT INTO `sys_area` VALUES ('371328', '蒙阴县', '3', '371300', '0539', 'myx', null, '1');
INSERT INTO `sys_area` VALUES ('371329', '临沭县', '3', '371300', '0539', 'lsx', null, '1');
INSERT INTO `sys_area` VALUES ('371400', '德州市', '2', '370000', null, 'dzs', null, '1');
INSERT INTO `sys_area` VALUES ('371401', '市辖区', '3', '371400', '0534', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('371402', '德城区', '3', '371400', '0534', 'dcq', null, '1');
INSERT INTO `sys_area` VALUES ('371421', '陵县', '3', '371400', '0534', 'lx', null, '1');
INSERT INTO `sys_area` VALUES ('371422', '宁津县', '3', '371400', '0534', 'njx', null, '1');
INSERT INTO `sys_area` VALUES ('371423', '庆云县', '3', '371400', '0534', 'qyx', null, '1');
INSERT INTO `sys_area` VALUES ('371424', '临邑县', '3', '371400', '0534', 'lyx', null, '1');
INSERT INTO `sys_area` VALUES ('371425', '齐河县', '3', '371400', '0534', 'qhx', null, '1');
INSERT INTO `sys_area` VALUES ('371426', '平原县', '3', '371400', '0534', 'pyx', null, '1');
INSERT INTO `sys_area` VALUES ('371427', '夏津县', '3', '371400', '0534', 'xjx', null, '1');
INSERT INTO `sys_area` VALUES ('371428', '武城县', '3', '371400', '0534', 'wcx', null, '1');
INSERT INTO `sys_area` VALUES ('371481', '乐陵市', '3', '371400', '0534', 'lls', null, '1');
INSERT INTO `sys_area` VALUES ('371482', '禹城市', '3', '371400', '0534', 'ycs', null, '1');
INSERT INTO `sys_area` VALUES ('371500', '聊城市', '2', '370000', null, 'lcs', null, '1');
INSERT INTO `sys_area` VALUES ('371501', '市辖区', '3', '371500', '0635', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('371502', '东昌府区', '3', '371500', '0635', 'dcfq', null, '1');
INSERT INTO `sys_area` VALUES ('371521', '阳谷县', '3', '371500', '0635', 'ygx', null, '1');
INSERT INTO `sys_area` VALUES ('371522', '莘县', '3', '371500', '0635', 'xx', null, '1');
INSERT INTO `sys_area` VALUES ('371523', '茌平县', '3', '371500', '0635', 'cpx', null, '1');
INSERT INTO `sys_area` VALUES ('371524', '东阿县', '3', '371500', '0635', 'dax', null, '1');
INSERT INTO `sys_area` VALUES ('371525', '冠县', '3', '371500', '0635', 'gx', null, '1');
INSERT INTO `sys_area` VALUES ('371526', '高唐县', '3', '371500', '0635', 'gtx', null, '1');
INSERT INTO `sys_area` VALUES ('371581', '临清市', '3', '371500', '0635', 'lqs', null, '1');
INSERT INTO `sys_area` VALUES ('371600', '滨州市', '2', '370000', null, 'bzs', null, '1');
INSERT INTO `sys_area` VALUES ('371601', '市辖区', '3', '371600', '0543', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('371602', '滨城区', '3', '371600', '0543', 'bcq', null, '1');
INSERT INTO `sys_area` VALUES ('371621', '惠民县', '3', '371600', '0543', 'hmx', null, '1');
INSERT INTO `sys_area` VALUES ('371622', '阳信县', '3', '371600', '0543', 'yxx', null, '1');
INSERT INTO `sys_area` VALUES ('371623', '无棣县', '3', '371600', '0543', 'wdx', null, '1');
INSERT INTO `sys_area` VALUES ('371624', '沾化县', '3', '371600', '0543', 'zhx', null, '1');
INSERT INTO `sys_area` VALUES ('371625', '博兴县', '3', '371600', '0543', 'bxx', null, '1');
INSERT INTO `sys_area` VALUES ('371626', '邹平县', '3', '371600', '0543', 'zpx', null, '1');
INSERT INTO `sys_area` VALUES ('371700', '菏泽市', '2', '370000', null, 'hzs', null, '1');
INSERT INTO `sys_area` VALUES ('371701', '市辖区', '3', '371700', '0530', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('371702', '牡丹区', '3', '371700', '0530', 'mdq', null, '1');
INSERT INTO `sys_area` VALUES ('371721', '曹县', '3', '371700', '0530', 'cx', null, '1');
INSERT INTO `sys_area` VALUES ('371722', '单县', '3', '371700', '0530', 'dx', null, '1');
INSERT INTO `sys_area` VALUES ('371723', '成武县', '3', '371700', '0530', 'cwx', null, '1');
INSERT INTO `sys_area` VALUES ('371724', '巨野县', '3', '371700', '0530', 'jyx', null, '1');
INSERT INTO `sys_area` VALUES ('371725', '郓城县', '3', '371700', '0530', 'ycx', null, '1');
INSERT INTO `sys_area` VALUES ('371726', '鄄城县', '3', '371700', '0530', 'jcx', null, '1');
INSERT INTO `sys_area` VALUES ('371727', '定陶县', '3', '371700', '0530', 'dtx', null, '1');
INSERT INTO `sys_area` VALUES ('371728', '东明县', '3', '371700', '0530', 'dmx', null, '1');
INSERT INTO `sys_area` VALUES ('410000', '河南省', '1', '0', null, 'hns', null, '1');
INSERT INTO `sys_area` VALUES ('410100', '郑州市', '2', '410000', null, 'zzs', null, '1');
INSERT INTO `sys_area` VALUES ('410101', '市辖区', '3', '410100', '0371', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('410102', '中原区', '3', '410100', '0371', 'zyq', null, '1');
INSERT INTO `sys_area` VALUES ('410103', '二七区', '3', '410100', '0371', 'eqq', null, '1');
INSERT INTO `sys_area` VALUES ('410104', '管城回族区', '3', '410100', '0371', 'gchzq', null, '1');
INSERT INTO `sys_area` VALUES ('410105', '金水区', '3', '410100', '0371', 'jsq', null, '1');
INSERT INTO `sys_area` VALUES ('410106', '上街区', '3', '410100', '0371', 'sjq', null, '1');
INSERT INTO `sys_area` VALUES ('410108', '惠济区', '3', '410100', '0371', 'hjq', null, '1');
INSERT INTO `sys_area` VALUES ('410122', '中牟县', '3', '410100', '0371', 'zmx', null, '1');
INSERT INTO `sys_area` VALUES ('410181', '巩义市', '3', '410100', '0371', 'gys', null, '1');
INSERT INTO `sys_area` VALUES ('410182', '荥阳市', '3', '410100', '0371', 'yys', null, '1');
INSERT INTO `sys_area` VALUES ('410183', '新密市', '3', '410100', '0371', 'xms', null, '1');
INSERT INTO `sys_area` VALUES ('410184', '新郑市', '3', '410100', '0371', 'xzs', null, '1');
INSERT INTO `sys_area` VALUES ('410185', '登封市', '3', '410100', '0371', 'dfs', null, '1');
INSERT INTO `sys_area` VALUES ('410200', '开封市', '2', '410000', null, 'kfs', null, '1');
INSERT INTO `sys_area` VALUES ('410201', '市辖区', '3', '410200', '0378', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('410202', '龙亭区', '3', '410200', '0378', 'ltq', null, '1');
INSERT INTO `sys_area` VALUES ('410203', '顺河回族区', '3', '410200', '0378', 'shhzq', null, '1');
INSERT INTO `sys_area` VALUES ('410204', '鼓楼区', '3', '410200', '0378', 'glq', null, '1');
INSERT INTO `sys_area` VALUES ('410205', '禹王台区', '3', '410200', '0378', 'ywtq', null, '1');
INSERT INTO `sys_area` VALUES ('410211', '金明区', '3', '410200', '0378', 'jmq', null, '1');
INSERT INTO `sys_area` VALUES ('410221', '杞县', '3', '410200', '0378', 'qx', null, '1');
INSERT INTO `sys_area` VALUES ('410222', '通许县', '3', '410200', '0378', 'txx', null, '1');
INSERT INTO `sys_area` VALUES ('410223', '尉氏县', '3', '410200', '0378', 'wsx', null, '1');
INSERT INTO `sys_area` VALUES ('410224', '开封县', '3', '410200', '0378', 'kfx', null, '1');
INSERT INTO `sys_area` VALUES ('410225', '兰考县', '3', '410200', '0378', 'lkx', null, '1');
INSERT INTO `sys_area` VALUES ('410300', '洛阳市', '2', '410000', null, 'lys', null, '1');
INSERT INTO `sys_area` VALUES ('410301', '市辖区', '3', '410300', '0379', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('410302', '老城区', '3', '410300', '0379', 'lcq', null, '1');
INSERT INTO `sys_area` VALUES ('410303', '西工区', '3', '410300', '0379', 'xgq', null, '1');
INSERT INTO `sys_area` VALUES ('410304', '瀍河回族区', '3', '410300', '0379', 'chhzq', null, '1');
INSERT INTO `sys_area` VALUES ('410305', '涧西区', '3', '410300', '0379', 'jxq', null, '1');
INSERT INTO `sys_area` VALUES ('410306', '吉利区', '3', '410300', '0379', 'jlq', null, '1');
INSERT INTO `sys_area` VALUES ('410311', '洛龙区', '3', '410300', null, 'llq', null, '1');
INSERT INTO `sys_area` VALUES ('410322', '孟津县', '3', '410300', '0379', 'mjx', null, '1');
INSERT INTO `sys_area` VALUES ('410323', '新安县', '3', '410300', '0379', 'xax', null, '1');
INSERT INTO `sys_area` VALUES ('410324', '栾川县', '3', '410300', '0379', 'lcx', null, '1');
INSERT INTO `sys_area` VALUES ('410325', '嵩县', '3', '410300', '0379', 'sx', null, '1');
INSERT INTO `sys_area` VALUES ('410326', '汝阳县', '3', '410300', '0379', 'ryx', null, '1');
INSERT INTO `sys_area` VALUES ('410327', '宜阳县', '3', '410300', '0379', 'yyx', null, '1');
INSERT INTO `sys_area` VALUES ('410328', '洛宁县', '3', '410300', '0379', 'lnx', null, '1');
INSERT INTO `sys_area` VALUES ('410329', '伊川县', '3', '410300', '0379', 'ycx', null, '1');
INSERT INTO `sys_area` VALUES ('410381', '偃师市', '3', '410300', '0379', 'yss', null, '1');
INSERT INTO `sys_area` VALUES ('410400', '平顶山市', '2', '410000', null, 'pdss', null, '1');
INSERT INTO `sys_area` VALUES ('410401', '市辖区', '3', '410400', '0375', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('410402', '新华区', '3', '410400', '0375', 'xhq', null, '1');
INSERT INTO `sys_area` VALUES ('410403', '卫东区', '3', '410400', '0375', 'wdq', null, '1');
INSERT INTO `sys_area` VALUES ('410404', '石龙区', '3', '410400', '0375', 'slq', null, '1');
INSERT INTO `sys_area` VALUES ('410411', '湛河区', '3', '410400', '0375', 'zhq', null, '1');
INSERT INTO `sys_area` VALUES ('410421', '宝丰县', '3', '410400', '0375', 'bfx', null, '1');
INSERT INTO `sys_area` VALUES ('410422', '叶县', '3', '410400', '0375', 'yx', null, '1');
INSERT INTO `sys_area` VALUES ('410423', '鲁山县', '3', '410400', '0375', 'lsx', null, '1');
INSERT INTO `sys_area` VALUES ('410425', '郏县', '3', '410400', '0375', 'jx', null, '1');
INSERT INTO `sys_area` VALUES ('410481', '舞钢市', '3', '410400', '0375', 'wgs', null, '1');
INSERT INTO `sys_area` VALUES ('410482', '汝州市', '3', '410400', '0375', 'rzs', null, '1');
INSERT INTO `sys_area` VALUES ('410500', '安阳市', '2', '410000', null, 'ays', null, '1');
INSERT INTO `sys_area` VALUES ('410501', '市辖区', '3', '410500', '0372', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('410502', '文峰区', '3', '410500', '0372', 'wfq', null, '1');
INSERT INTO `sys_area` VALUES ('410503', '北关区', '3', '410500', '0372', 'bgq', null, '1');
INSERT INTO `sys_area` VALUES ('410505', '殷都区', '3', '410500', '0372', 'ydq', null, '1');
INSERT INTO `sys_area` VALUES ('410506', '龙安区', '3', '410500', '0372', 'laq', null, '1');
INSERT INTO `sys_area` VALUES ('410522', '安阳县', '3', '410500', '0372', 'ayx', null, '1');
INSERT INTO `sys_area` VALUES ('410523', '汤阴县', '3', '410500', '0372', 'tyx', null, '1');
INSERT INTO `sys_area` VALUES ('410526', '滑县', '3', '410500', '0372', 'hx', null, '1');
INSERT INTO `sys_area` VALUES ('410527', '内黄县', '3', '410500', '0372', 'nhx', null, '1');
INSERT INTO `sys_area` VALUES ('410581', '林州市', '3', '410500', '0372', 'lzs', null, '1');
INSERT INTO `sys_area` VALUES ('410600', '鹤壁市', '2', '410000', null, 'hbs', null, '1');
INSERT INTO `sys_area` VALUES ('410601', '市辖区', '3', '410600', '0392', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('410602', '鹤山区', '3', '410600', '0392', 'hsq', null, '1');
INSERT INTO `sys_area` VALUES ('410603', '山城区', '3', '410600', '0392', 'scq', null, '1');
INSERT INTO `sys_area` VALUES ('410611', '淇滨区', '3', '410600', '0392', 'qbq', null, '1');
INSERT INTO `sys_area` VALUES ('410621', '浚县', '3', '410600', '0392', 'jx', null, '1');
INSERT INTO `sys_area` VALUES ('410622', '淇县', '3', '410600', '0392', 'qx', null, '1');
INSERT INTO `sys_area` VALUES ('410700', '新乡市', '2', '410000', null, 'xxs', null, '1');
INSERT INTO `sys_area` VALUES ('410701', '市辖区', '3', '410700', '0373', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('410702', '红旗区', '3', '410700', '0373', 'hqq', null, '1');
INSERT INTO `sys_area` VALUES ('410703', '卫滨区', '3', '410700', '0373', 'wbq', null, '1');
INSERT INTO `sys_area` VALUES ('410704', '凤泉区', '3', '410700', '0373', 'fqq', null, '1');
INSERT INTO `sys_area` VALUES ('410711', '牧野区', '3', '410700', '0373', 'myq', null, '1');
INSERT INTO `sys_area` VALUES ('410721', '新乡县', '3', '410700', '0373', 'xxx', null, '1');
INSERT INTO `sys_area` VALUES ('410724', '获嘉县', '3', '410700', '0373', 'hjx', null, '1');
INSERT INTO `sys_area` VALUES ('410725', '原阳县', '3', '410700', '0373', 'yyx', null, '1');
INSERT INTO `sys_area` VALUES ('410726', '延津县', '3', '410700', '0373', 'yjx', null, '1');
INSERT INTO `sys_area` VALUES ('410727', '封丘县', '3', '410700', '0373', 'fqx', null, '1');
INSERT INTO `sys_area` VALUES ('410728', '长垣县', '3', '410700', '0373', 'zyx', null, '1');
INSERT INTO `sys_area` VALUES ('410781', '卫辉市', '3', '410700', '0373', 'whs', null, '1');
INSERT INTO `sys_area` VALUES ('410782', '辉县市', '3', '410700', '0373', 'hxs', null, '1');
INSERT INTO `sys_area` VALUES ('410800', '焦作市', '2', '410000', null, 'jzs', null, '1');
INSERT INTO `sys_area` VALUES ('410801', '市辖区', '3', '410800', '0391', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('410802', '解放区', '3', '410800', '0391', 'jfq', null, '1');
INSERT INTO `sys_area` VALUES ('410803', '中站区', '3', '410800', '0391', 'zzq', null, '1');
INSERT INTO `sys_area` VALUES ('410804', '马村区', '3', '410800', '0391', 'mcq', null, '1');
INSERT INTO `sys_area` VALUES ('410811', '山阳区', '3', '410800', '0391', 'syq', null, '1');
INSERT INTO `sys_area` VALUES ('410821', '修武县', '3', '410800', '0391', 'xwx', null, '1');
INSERT INTO `sys_area` VALUES ('410822', '博爱县', '3', '410800', '0391', 'bax', null, '1');
INSERT INTO `sys_area` VALUES ('410823', '武陟县', '3', '410800', '0391', 'wzx', null, '1');
INSERT INTO `sys_area` VALUES ('410825', '温县', '3', '410800', '0391', 'wx', null, '1');
INSERT INTO `sys_area` VALUES ('410882', '沁阳市', '3', '410800', '0391', 'qys', null, '1');
INSERT INTO `sys_area` VALUES ('410883', '孟州市', '3', '410800', '0391', 'mzs', null, '1');
INSERT INTO `sys_area` VALUES ('410900', '濮阳市', '2', '410000', null, 'pys', null, '1');
INSERT INTO `sys_area` VALUES ('410901', '市辖区', '3', '410900', '0393', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('410902', '华龙区', '3', '410900', '0393', 'hlq', null, '1');
INSERT INTO `sys_area` VALUES ('410922', '清丰县', '3', '410900', '0393', 'qfx', null, '1');
INSERT INTO `sys_area` VALUES ('410923', '南乐县', '3', '410900', '0393', 'nlx', null, '1');
INSERT INTO `sys_area` VALUES ('410926', '范县', '3', '410900', '0393', 'fx', null, '1');
INSERT INTO `sys_area` VALUES ('410927', '台前县', '3', '410900', '0393', 'tqx', null, '1');
INSERT INTO `sys_area` VALUES ('410928', '濮阳县', '3', '410900', '0393', 'pyx', null, '1');
INSERT INTO `sys_area` VALUES ('411000', '许昌市', '2', '410000', null, 'xcs', null, '1');
INSERT INTO `sys_area` VALUES ('411001', '市辖区', '3', '411000', '0373', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('411002', '魏都区', '3', '411000', '0374', 'wdq', null, '1');
INSERT INTO `sys_area` VALUES ('411023', '许昌县', '3', '411000', '0374', 'xcx', null, '1');
INSERT INTO `sys_area` VALUES ('411024', '鄢陵县', '3', '411000', '0374', 'ylx', null, '1');
INSERT INTO `sys_area` VALUES ('411025', '襄城县', '3', '411000', '0374', 'xcx', null, '1');
INSERT INTO `sys_area` VALUES ('411081', '禹州市', '3', '411000', '0374', 'yzs', null, '1');
INSERT INTO `sys_area` VALUES ('411082', '长葛市', '3', '411000', '0374', 'zgs', null, '1');
INSERT INTO `sys_area` VALUES ('411100', '漯河市', '2', '410000', null, 'lhs', null, '1');
INSERT INTO `sys_area` VALUES ('411101', '市辖区', '3', '411100', '0395', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('411102', '源汇区', '3', '411100', '0395', 'yhq', null, '1');
INSERT INTO `sys_area` VALUES ('411103', '郾城区', '3', '411100', '0395', 'ycq', null, '1');
INSERT INTO `sys_area` VALUES ('411104', '召陵区', '3', '411100', '0395', 'zlq', null, '1');
INSERT INTO `sys_area` VALUES ('411121', '舞阳县', '3', '411100', '0395', 'wyx', null, '1');
INSERT INTO `sys_area` VALUES ('411122', '临颍县', '3', '411100', '0395', 'lyx', null, '1');
INSERT INTO `sys_area` VALUES ('411200', '三门峡市', '2', '410000', null, 'smxs', null, '1');
INSERT INTO `sys_area` VALUES ('411201', '市辖区', '3', '411200', '0398', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('411202', '湖滨区', '3', '411200', '0398', 'hbq', null, '1');
INSERT INTO `sys_area` VALUES ('411221', '渑池县', '3', '411200', '0398', 'mcx', null, '1');
INSERT INTO `sys_area` VALUES ('411222', '陕县', '3', '411200', '0398', 'sx', null, '1');
INSERT INTO `sys_area` VALUES ('411224', '卢氏县', '3', '411200', '0398', 'lsx', null, '1');
INSERT INTO `sys_area` VALUES ('411281', '义马市', '3', '411200', '0398', 'yms', null, '1');
INSERT INTO `sys_area` VALUES ('411282', '灵宝市', '3', '411200', '0398', 'lbs', null, '1');
INSERT INTO `sys_area` VALUES ('411300', '南阳市', '2', '410000', null, 'nys', null, '1');
INSERT INTO `sys_area` VALUES ('411301', '市辖区', '3', '411300', '0377', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('411302', '宛城区', '3', '411300', '0377', 'wcq', null, '1');
INSERT INTO `sys_area` VALUES ('411303', '卧龙区', '3', '411300', '0377', 'wlq', null, '1');
INSERT INTO `sys_area` VALUES ('411321', '南召县', '3', '411300', '0377', 'nzx', null, '1');
INSERT INTO `sys_area` VALUES ('411322', '方城县', '3', '411300', '0377', 'fcx', null, '1');
INSERT INTO `sys_area` VALUES ('411323', '西峡县', '3', '411300', '0377', 'xxx', null, '1');
INSERT INTO `sys_area` VALUES ('411324', '镇平县', '3', '411300', '0377', 'zpx', null, '1');
INSERT INTO `sys_area` VALUES ('411325', '内乡县', '3', '411300', '0377', 'nxx', null, '1');
INSERT INTO `sys_area` VALUES ('411326', '淅川县', '3', '411300', '0377', 'xcx', null, '1');
INSERT INTO `sys_area` VALUES ('411327', '社旗县', '3', '411300', '0377', 'sqx', null, '1');
INSERT INTO `sys_area` VALUES ('411328', '唐河县', '3', '411300', '0377', 'thx', null, '1');
INSERT INTO `sys_area` VALUES ('411329', '新野县', '3', '411300', '0377', 'xyx', null, '1');
INSERT INTO `sys_area` VALUES ('411330', '桐柏县', '3', '411300', '0377', 'tbx', null, '1');
INSERT INTO `sys_area` VALUES ('411381', '邓州市', '3', '411300', '0377', 'dzs', null, '1');
INSERT INTO `sys_area` VALUES ('411400', '商丘市', '2', '410000', null, 'sqs', null, '1');
INSERT INTO `sys_area` VALUES ('411401', '市辖区', '3', '411400', '0370', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('411402', '梁园区', '3', '411400', '0370', 'lyq', null, '1');
INSERT INTO `sys_area` VALUES ('411403', '睢阳区', '3', '411400', '0370', 'syq', null, '1');
INSERT INTO `sys_area` VALUES ('411421', '民权县', '3', '411400', '0370', 'mqx', null, '1');
INSERT INTO `sys_area` VALUES ('411422', '睢县', '3', '411400', '0370', 'sx', null, '1');
INSERT INTO `sys_area` VALUES ('411423', '宁陵县', '3', '411400', '0370', 'nlx', null, '1');
INSERT INTO `sys_area` VALUES ('411424', '柘城县', '3', '411400', '0370', 'zcx', null, '1');
INSERT INTO `sys_area` VALUES ('411425', '虞城县', '3', '411400', '0370', 'ycx', null, '1');
INSERT INTO `sys_area` VALUES ('411426', '夏邑县', '3', '411400', '0370', 'xyx', null, '1');
INSERT INTO `sys_area` VALUES ('411481', '永城市', '3', '411400', '0370', 'ycs', null, '1');
INSERT INTO `sys_area` VALUES ('411500', '信阳市', '2', '410000', null, 'xys', null, '1');
INSERT INTO `sys_area` VALUES ('411501', '市辖区', '3', '411500', '0376', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('411502', '浉河区', '3', '411500', '0376', 'shq', null, '1');
INSERT INTO `sys_area` VALUES ('411503', '平桥区', '3', '411500', '0376', 'pqq', null, '1');
INSERT INTO `sys_area` VALUES ('411521', '罗山县', '3', '411500', '0376', 'lsx', null, '1');
INSERT INTO `sys_area` VALUES ('411522', '光山县', '3', '411500', '0397', 'gsx', null, '1');
INSERT INTO `sys_area` VALUES ('411523', '新县', '3', '411500', '0397', 'xx', null, '1');
INSERT INTO `sys_area` VALUES ('411524', '商城县', '3', '411500', '0397', 'scx', null, '1');
INSERT INTO `sys_area` VALUES ('411525', '固始县', '3', '411500', '0397', 'gsx', null, '1');
INSERT INTO `sys_area` VALUES ('411526', '潢川县', '3', '411500', '0397', 'hcx', null, '1');
INSERT INTO `sys_area` VALUES ('411527', '淮滨县', '3', '411500', '0397', 'hbx', null, '1');
INSERT INTO `sys_area` VALUES ('411528', '息县', '3', '411500', '0397', 'xx', null, '1');
INSERT INTO `sys_area` VALUES ('411600', '周口市', '2', '410000', null, 'zks', null, '1');
INSERT INTO `sys_area` VALUES ('411601', '市辖区', '3', '411600', '0394', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('411602', '川汇区', '3', '411600', '0394', 'chq', null, '1');
INSERT INTO `sys_area` VALUES ('411621', '扶沟县', '3', '411600', '0394', 'fgx', null, '1');
INSERT INTO `sys_area` VALUES ('411622', '西华县', '3', '411600', '0394', 'xhx', null, '1');
INSERT INTO `sys_area` VALUES ('411623', '商水县', '3', '411600', '0394', 'ssx', null, '1');
INSERT INTO `sys_area` VALUES ('411624', '沈丘县', '3', '411600', '0394', 'sqx', null, '1');
INSERT INTO `sys_area` VALUES ('411625', '郸城县', '3', '411600', '0394', 'dcx', null, '1');
INSERT INTO `sys_area` VALUES ('411626', '淮阳县', '3', '411600', '0394', 'hyx', null, '1');
INSERT INTO `sys_area` VALUES ('411627', '太康县', '3', '411600', '0394', 'tkx', null, '1');
INSERT INTO `sys_area` VALUES ('411628', '鹿邑县', '3', '411600', '0394', 'lyx', null, '1');
INSERT INTO `sys_area` VALUES ('411681', '项城市', '3', '411600', '0394', 'xcs', null, '1');
INSERT INTO `sys_area` VALUES ('411700', '驻马店市', '2', '410000', null, 'zmds', null, '1');
INSERT INTO `sys_area` VALUES ('411701', '市辖区', '3', '411700', '0396', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('411702', '驿城区', '3', '411700', '0396', 'ycq', null, '1');
INSERT INTO `sys_area` VALUES ('411721', '西平县', '3', '411700', '0396', 'xpx', null, '1');
INSERT INTO `sys_area` VALUES ('411722', '上蔡县', '3', '411700', '0396', 'scx', null, '1');
INSERT INTO `sys_area` VALUES ('411723', '平舆县', '3', '411700', '0396', 'pyx', null, '1');
INSERT INTO `sys_area` VALUES ('411724', '正阳县', '3', '411700', '0396', 'zyx', null, '1');
INSERT INTO `sys_area` VALUES ('411725', '确山县', '3', '411700', '0396', 'qsx', null, '1');
INSERT INTO `sys_area` VALUES ('411726', '泌阳县', '3', '411700', '0396', 'myx', null, '1');
INSERT INTO `sys_area` VALUES ('411727', '汝南县', '3', '411700', '0396', 'rnx', null, '1');
INSERT INTO `sys_area` VALUES ('411728', '遂平县', '3', '411700', '0396', 'spx', null, '1');
INSERT INTO `sys_area` VALUES ('411729', '新蔡县', '3', '411700', '0396', 'xcx', null, '1');
INSERT INTO `sys_area` VALUES ('419000', '省直辖县级行政区划', '2', '410000', null, 'szxxjxzqh', null, '1');
INSERT INTO `sys_area` VALUES ('419001', '济源市', '3', '419000', null, 'jys', null, '1');
INSERT INTO `sys_area` VALUES ('420000', '湖北省', '1', '0', null, 'hbs', '1', '1');
INSERT INTO `sys_area` VALUES ('420100', '武汉市', '2', '420000', null, 'whs', null, '1');
INSERT INTO `sys_area` VALUES ('420101', '市辖区', '3', '420100', '027', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('420102', '江岸区', '3', '420100', '027', 'jaq', null, '1');
INSERT INTO `sys_area` VALUES ('420103', '江汉区', '3', '420100', '027', 'jhq', null, '1');
INSERT INTO `sys_area` VALUES ('420104', '硚口区', '3', '420100', '027', 'qkq', null, '1');
INSERT INTO `sys_area` VALUES ('420105', '汉阳区', '3', '420100', '027', 'hyq', null, '1');
INSERT INTO `sys_area` VALUES ('420106', '武昌区', '3', '420100', '027', 'wcq', null, '1');
INSERT INTO `sys_area` VALUES ('420107', '青山区', '3', '420100', '027', 'qsq', null, '1');
INSERT INTO `sys_area` VALUES ('420111', '洪山区', '3', '420100', '027', 'hsq', null, '1');
INSERT INTO `sys_area` VALUES ('420112', '东西湖区', '3', '420100', '027', 'dxhq', null, '1');
INSERT INTO `sys_area` VALUES ('420113', '汉南区', '3', '420100', '027', 'hnq', null, '1');
INSERT INTO `sys_area` VALUES ('420114', '蔡甸区', '3', '420100', '027', 'cdq', null, '1');
INSERT INTO `sys_area` VALUES ('420115', '江夏区', '3', '420100', '027', 'jxq', null, '1');
INSERT INTO `sys_area` VALUES ('420116', '黄陂区', '3', '420100', '027', 'hpq', null, '1');
INSERT INTO `sys_area` VALUES ('420117', '新洲区', '3', '420100', '027', 'xzq', null, '1');
INSERT INTO `sys_area` VALUES ('420200', '黄石市', '2', '420000', null, 'hss', null, '1');
INSERT INTO `sys_area` VALUES ('420201', '市辖区', '3', '420200', '0714', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('420202', '黄石港区', '3', '420200', '0714', 'hsgq', null, '1');
INSERT INTO `sys_area` VALUES ('420203', '西塞山区', '3', '420200', '0714', 'xssq', null, '1');
INSERT INTO `sys_area` VALUES ('420204', '下陆区', '3', '420200', '0714', 'xlq', null, '1');
INSERT INTO `sys_area` VALUES ('420205', '铁山区', '3', '420200', '0714', 'tsq', null, '1');
INSERT INTO `sys_area` VALUES ('420222', '阳新县', '3', '420200', '0714', 'yxx', null, '1');
INSERT INTO `sys_area` VALUES ('420281', '大冶市', '3', '420200', '0714', 'dys', null, '1');
INSERT INTO `sys_area` VALUES ('420300', '十堰市', '2', '420000', null, 'sys', null, '1');
INSERT INTO `sys_area` VALUES ('420301', '市辖区', '3', '420300', '0719', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('420302', '茅箭区', '3', '420300', '0719', 'mjq', null, '1');
INSERT INTO `sys_area` VALUES ('420303', '张湾区', '3', '420300', '0719', 'zwq', null, '1');
INSERT INTO `sys_area` VALUES ('420321', '郧县', '3', '420300', '0719', 'yx', null, '1');
INSERT INTO `sys_area` VALUES ('420322', '郧西县', '3', '420300', '0719', 'yxx', null, '1');
INSERT INTO `sys_area` VALUES ('420323', '竹山县', '3', '420300', '0719', 'zsx', null, '1');
INSERT INTO `sys_area` VALUES ('420324', '竹溪县', '3', '420300', '0719', 'zxx', null, '1');
INSERT INTO `sys_area` VALUES ('420325', '房县', '3', '420300', '0719', 'fx', null, '1');
INSERT INTO `sys_area` VALUES ('420381', '丹江口市', '3', '420300', '0719', 'djks', null, '1');
INSERT INTO `sys_area` VALUES ('420500', '宜昌市', '2', '420000', null, 'ycs', null, '1');
INSERT INTO `sys_area` VALUES ('420501', '市辖区', '3', '420500', '0717', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('420502', '西陵区', '3', '420500', '0717', 'xlq', null, '1');
INSERT INTO `sys_area` VALUES ('420503', '伍家岗区', '3', '420500', '0717', 'wjgq', null, '1');
INSERT INTO `sys_area` VALUES ('420504', '点军区', '3', '420500', '0717', 'djq', null, '1');
INSERT INTO `sys_area` VALUES ('420505', '猇亭区', '3', '420500', '0717', 'ytq', null, '1');
INSERT INTO `sys_area` VALUES ('420506', '夷陵区', '3', '420500', '0717', 'ylq', null, '1');
INSERT INTO `sys_area` VALUES ('420525', '远安县', '3', '420500', '0717', 'yax', null, '1');
INSERT INTO `sys_area` VALUES ('420526', '兴山县', '3', '420500', '0717', 'xsx', null, '1');
INSERT INTO `sys_area` VALUES ('420527', '秭归县', '3', '420500', '0717', 'zgx', null, '1');
INSERT INTO `sys_area` VALUES ('420528', '长阳土家族自治县', '3', '420500', '0717', 'zytjzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('420529', '五峰土家族自治县', '3', '420500', '0717', 'wftjzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('420581', '宜都市', '3', '420500', '0717', 'yds', null, '1');
INSERT INTO `sys_area` VALUES ('420582', '当阳市', '3', '420500', '0717', 'dys', null, '1');
INSERT INTO `sys_area` VALUES ('420583', '枝江市', '3', '420500', '0717', 'zjs', null, '1');
INSERT INTO `sys_area` VALUES ('420600', '襄阳市', '2', '420000', null, 'xys', null, '1');
INSERT INTO `sys_area` VALUES ('420601', '市辖区', '3', '420600', '0710', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('420602', '襄城区', '3', '420600', '0710', 'xcq', null, '1');
INSERT INTO `sys_area` VALUES ('420606', '樊城区', '3', '420600', '0710', 'fcq', null, '1');
INSERT INTO `sys_area` VALUES ('420607', '襄州区', '3', '420600', '0710', 'xzq', null, '1');
INSERT INTO `sys_area` VALUES ('420624', '南漳县', '3', '420600', '0710', 'nzx', null, '1');
INSERT INTO `sys_area` VALUES ('420625', '谷城县', '3', '420600', '0710', 'gcx', null, '1');
INSERT INTO `sys_area` VALUES ('420626', '保康县', '3', '420600', '0710', 'bkx', null, '1');
INSERT INTO `sys_area` VALUES ('420682', '老河口市', '3', '420600', '0710', 'lhks', null, '1');
INSERT INTO `sys_area` VALUES ('420683', '枣阳市', '3', '420600', '0710', 'zys', null, '1');
INSERT INTO `sys_area` VALUES ('420684', '宜城市', '3', '420600', '0710', 'ycs', null, '1');
INSERT INTO `sys_area` VALUES ('420700', '鄂州市', '2', '420000', null, 'ezs', null, '1');
INSERT INTO `sys_area` VALUES ('420701', '市辖区', '3', '420700', '0711', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('420702', '梁子湖区', '3', '420700', '0711', 'lzhq', null, '1');
INSERT INTO `sys_area` VALUES ('420703', '华容区', '3', '420700', '0711', 'hrq', null, '1');
INSERT INTO `sys_area` VALUES ('420704', '鄂城区', '3', '420700', '0711', 'ecq', null, '1');
INSERT INTO `sys_area` VALUES ('420800', '荆门市', '2', '420000', null, 'jms', null, '1');
INSERT INTO `sys_area` VALUES ('420801', '市辖区', '3', '420800', '0724', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('420802', '东宝区', '3', '420800', '0724', 'dbq', null, '1');
INSERT INTO `sys_area` VALUES ('420804', '掇刀区', '3', '420800', '0724', 'ddq', null, '1');
INSERT INTO `sys_area` VALUES ('420821', '京山县', '3', '420800', '0724', 'jsx', null, '1');
INSERT INTO `sys_area` VALUES ('420822', '沙洋县', '3', '420800', '0724', 'syx', null, '1');
INSERT INTO `sys_area` VALUES ('420881', '钟祥市', '3', '420800', '0724', 'zxs', null, '1');
INSERT INTO `sys_area` VALUES ('420900', '孝感市', '2', '420000', null, 'xgs', null, '1');
INSERT INTO `sys_area` VALUES ('420901', '市辖区', '3', '420900', '0712', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('420902', '孝南区', '3', '420900', '0712', 'xnq', null, '1');
INSERT INTO `sys_area` VALUES ('420921', '孝昌县', '3', '420900', '0712', 'xcx', null, '1');
INSERT INTO `sys_area` VALUES ('420922', '大悟县', '3', '420900', '0712', 'dwx', null, '1');
INSERT INTO `sys_area` VALUES ('420923', '云梦县', '3', '420900', '0712', 'ymx', null, '1');
INSERT INTO `sys_area` VALUES ('420981', '应城市', '3', '420900', '0712', 'ycs', null, '1');
INSERT INTO `sys_area` VALUES ('420982', '安陆市', '3', '420900', '0712', 'als', null, '1');
INSERT INTO `sys_area` VALUES ('420984', '汉川市', '3', '420900', '0712', 'hcs', null, '1');
INSERT INTO `sys_area` VALUES ('421000', '荆州市', '2', '420000', null, 'jzs', null, '1');
INSERT INTO `sys_area` VALUES ('421001', '市辖区', '3', '421000', '0716', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('421002', '沙市区', '3', '421000', '0716', 'ssq', null, '1');
INSERT INTO `sys_area` VALUES ('421003', '荆州区', '3', '421000', '0716', 'jzq', null, '1');
INSERT INTO `sys_area` VALUES ('421022', '公安县', '3', '421000', '0716', 'gax', null, '1');
INSERT INTO `sys_area` VALUES ('421023', '监利县', '3', '421000', '0716', 'jlx', null, '1');
INSERT INTO `sys_area` VALUES ('421024', '江陵县', '3', '421000', '0716', 'jlx', null, '1');
INSERT INTO `sys_area` VALUES ('421081', '石首市', '3', '421000', '0716', 'sss', null, '1');
INSERT INTO `sys_area` VALUES ('421083', '洪湖市', '3', '421000', '0716', 'hhs', null, '1');
INSERT INTO `sys_area` VALUES ('421087', '松滋市', '3', '421000', '0716', 'szs', null, '1');
INSERT INTO `sys_area` VALUES ('421100', '黄冈市', '2', '420000', null, 'hgs', null, '1');
INSERT INTO `sys_area` VALUES ('421101', '市辖区', '3', '421100', '0713', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('421102', '黄州区', '3', '421100', '0713', 'hzq', null, '1');
INSERT INTO `sys_area` VALUES ('421121', '团风县', '3', '421100', '0713', 'tfx', null, '1');
INSERT INTO `sys_area` VALUES ('421122', '红安县', '3', '421100', '0713', 'hax', null, '1');
INSERT INTO `sys_area` VALUES ('421123', '罗田县', '3', '421100', '0713', 'ltx', null, '1');
INSERT INTO `sys_area` VALUES ('421124', '英山县', '3', '421100', '0713', 'ysx', null, '1');
INSERT INTO `sys_area` VALUES ('421125', '浠水县', '3', '421100', '0713', 'xsx', null, '1');
INSERT INTO `sys_area` VALUES ('421126', '蕲春县', '3', '421100', '0713', 'qcx', null, '1');
INSERT INTO `sys_area` VALUES ('421127', '黄梅县', '3', '421100', '0713', 'hmx', null, '1');
INSERT INTO `sys_area` VALUES ('421181', '麻城市', '3', '421100', '0713', 'mcs', null, '1');
INSERT INTO `sys_area` VALUES ('421182', '武穴市', '3', '421100', '0713', 'wxs', null, '1');
INSERT INTO `sys_area` VALUES ('421200', '咸宁市', '2', '420000', null, 'xns', null, '1');
INSERT INTO `sys_area` VALUES ('421201', '市辖区', '3', '421200', '0715', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('421202', '咸安区', '3', '421200', '0715', 'xaq', null, '1');
INSERT INTO `sys_area` VALUES ('421221', '嘉鱼县', '3', '421200', '0715', 'jyx', null, '1');
INSERT INTO `sys_area` VALUES ('421222', '通城县', '3', '421200', '0715', 'tcx', null, '1');
INSERT INTO `sys_area` VALUES ('421223', '崇阳县', '3', '421200', '0715', 'cyx', null, '1');
INSERT INTO `sys_area` VALUES ('421224', '通山县', '3', '421200', '0715', 'tsx', null, '1');
INSERT INTO `sys_area` VALUES ('421281', '赤壁市', '3', '421200', '0715', 'cbs', null, '1');
INSERT INTO `sys_area` VALUES ('421300', '随州市', '2', '420000', null, 'szs', null, '1');
INSERT INTO `sys_area` VALUES ('421301', '市辖区', '3', '421300', '0722', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('421303', '曾都区', '3', '421300', null, 'cdq', null, '1');
INSERT INTO `sys_area` VALUES ('421321', '随县', '3', '421300', null, 'sx', null, '1');
INSERT INTO `sys_area` VALUES ('421381', '广水市', '3', '421300', '0722', 'gss', null, '1');
INSERT INTO `sys_area` VALUES ('422800', '恩施土家族苗族自治州', '2', '420000', null, 'estjzmzzzz', null, '1');
INSERT INTO `sys_area` VALUES ('422801', '恩施市', '3', '422800', '0718', 'ess', null, '1');
INSERT INTO `sys_area` VALUES ('422802', '利川市', '3', '422800', '0718', 'lcs', null, '1');
INSERT INTO `sys_area` VALUES ('422822', '建始县', '3', '422800', '0718', 'jsx', null, '1');
INSERT INTO `sys_area` VALUES ('422823', '巴东县', '3', '422800', '0718', 'bdx', null, '1');
INSERT INTO `sys_area` VALUES ('422825', '宣恩县', '3', '422800', '0718', 'xex', null, '1');
INSERT INTO `sys_area` VALUES ('422826', '咸丰县', '3', '422800', '0718', 'xfx', null, '1');
INSERT INTO `sys_area` VALUES ('422827', '来凤县', '3', '422800', '0718', 'lfx', null, '1');
INSERT INTO `sys_area` VALUES ('422828', '鹤峰县', '3', '422800', '0718', 'hfx', null, '1');
INSERT INTO `sys_area` VALUES ('429000', '省直辖县级行政区划', '2', '420000', null, 'szxxjxzqh', null, '1');
INSERT INTO `sys_area` VALUES ('429004', '仙桃市', '3', '429000', '0728', 'xts', null, '1');
INSERT INTO `sys_area` VALUES ('429005', '潜江市', '3', '429000', '0728', 'qjs', null, '1');
INSERT INTO `sys_area` VALUES ('429006', '天门市', '3', '429000', '0728', 'tms', null, '1');
INSERT INTO `sys_area` VALUES ('429021', '神农架林区', '3', '429000', '0719', 'snjlq', null, '1');
INSERT INTO `sys_area` VALUES ('430000', '湖南省', '1', '0', null, 'hns', '1', '1');
INSERT INTO `sys_area` VALUES ('430100', '长沙市', '2', '430000', null, 'css', '1', '1');
INSERT INTO `sys_area` VALUES ('430101', '市辖区', '3', '430100', '0731', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('430102', '芙蓉区', '3', '430100', '0731', 'frq', null, '1');
INSERT INTO `sys_area` VALUES ('430103', '天心区', '3', '430100', '0731', 'txq', null, '1');
INSERT INTO `sys_area` VALUES ('430104', '岳麓区', '3', '430100', '0731', 'ylq', null, '1');
INSERT INTO `sys_area` VALUES ('430105', '开福区', '3', '430100', '0731', 'kfq', null, '1');
INSERT INTO `sys_area` VALUES ('430111', '雨花区', '3', '430100', '0731', 'yhq', null, '1');
INSERT INTO `sys_area` VALUES ('430112', '望城区', '3', '430100', null, 'wcq', null, '1');
INSERT INTO `sys_area` VALUES ('430121', '长沙县', '3', '430100', '0731', 'zsx', null, '1');
INSERT INTO `sys_area` VALUES ('430124', '宁乡县', '3', '430100', '0731', 'nxx', null, '1');
INSERT INTO `sys_area` VALUES ('430181', '浏阳市', '3', '430100', '0731', 'lys', null, '1');
INSERT INTO `sys_area` VALUES ('430200', '株洲市', '2', '430000', null, 'zzs', '1', '1');
INSERT INTO `sys_area` VALUES ('430201', '市辖区', '3', '430200', '0733', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('430202', '荷塘区', '3', '430200', '0733', 'htq', null, '1');
INSERT INTO `sys_area` VALUES ('430203', '芦淞区', '3', '430200', '0733', 'lsq', null, '1');
INSERT INTO `sys_area` VALUES ('430204', '石峰区', '3', '430200', '0733', 'sfq', null, '1');
INSERT INTO `sys_area` VALUES ('430211', '天元区', '3', '430200', '0733', 'tyq', null, '1');
INSERT INTO `sys_area` VALUES ('430221', '株洲县', '3', '430200', '0733', 'zzx', null, '1');
INSERT INTO `sys_area` VALUES ('430223', '攸县', '3', '430200', '0733', 'yx', null, '1');
INSERT INTO `sys_area` VALUES ('430224', '茶陵县', '3', '430200', '0733', 'clx', null, '1');
INSERT INTO `sys_area` VALUES ('430225', '炎陵县', '3', '430200', '0733', 'ylx', null, '1');
INSERT INTO `sys_area` VALUES ('430281', '醴陵市', '3', '430200', '0733', 'lls', null, '1');
INSERT INTO `sys_area` VALUES ('430300', '湘潭市', '2', '430000', null, 'xts', '1', '1');
INSERT INTO `sys_area` VALUES ('430301', '市辖区', '3', '430300', '0732', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('430302', '雨湖区', '3', '430300', '0732', 'yhq', null, '1');
INSERT INTO `sys_area` VALUES ('430304', '岳塘区', '3', '430300', '0732', 'ytq', null, '1');
INSERT INTO `sys_area` VALUES ('430321', '湘潭县', '3', '430300', '0732', 'xtx', null, '1');
INSERT INTO `sys_area` VALUES ('430381', '湘乡市', '3', '430300', '0732', 'xxs', null, '1');
INSERT INTO `sys_area` VALUES ('430382', '韶山市', '3', '430300', '0732', 'sss', null, '1');
INSERT INTO `sys_area` VALUES ('430400', '衡阳市', '2', '430000', null, 'hys', '1', '1');
INSERT INTO `sys_area` VALUES ('430401', '市辖区', '3', '430400', '0734', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('430405', '珠晖区', '3', '430400', '0734', 'zhq', null, '1');
INSERT INTO `sys_area` VALUES ('430406', '雁峰区', '3', '430400', '0734', 'yfq', null, '1');
INSERT INTO `sys_area` VALUES ('430407', '石鼓区', '3', '430400', '0734', 'sgq', null, '1');
INSERT INTO `sys_area` VALUES ('430408', '蒸湘区', '3', '430400', '0734', 'zxq', null, '1');
INSERT INTO `sys_area` VALUES ('430412', '南岳区', '3', '430400', '0734', 'nyq', null, '1');
INSERT INTO `sys_area` VALUES ('430421', '衡阳县', '3', '430400', '0734', 'hyx', null, '1');
INSERT INTO `sys_area` VALUES ('430422', '衡南县', '3', '430400', '0734', 'hnx', null, '1');
INSERT INTO `sys_area` VALUES ('430423', '衡山县', '3', '430400', '0734', 'hsx', null, '1');
INSERT INTO `sys_area` VALUES ('430424', '衡东县', '3', '430400', '0734', 'hdx', null, '1');
INSERT INTO `sys_area` VALUES ('430426', '祁东县', '3', '430400', '0734', 'qdx', null, '1');
INSERT INTO `sys_area` VALUES ('430481', '耒阳市', '3', '430400', '0734', 'lys', null, '1');
INSERT INTO `sys_area` VALUES ('430482', '常宁市', '3', '430400', '0734', 'cns', null, '1');
INSERT INTO `sys_area` VALUES ('430500', '邵阳市', '2', '430000', null, 'sys', '1', '1');
INSERT INTO `sys_area` VALUES ('430501', '市辖区', '3', '430500', '0739', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('430502', '双清区', '3', '430500', '0739', 'sqq', null, '1');
INSERT INTO `sys_area` VALUES ('430503', '大祥区', '3', '430500', '0739', 'dxq', null, '1');
INSERT INTO `sys_area` VALUES ('430511', '北塔区', '3', '430500', '0739', 'btq', null, '1');
INSERT INTO `sys_area` VALUES ('430521', '邵东县', '3', '430500', '0739', 'sdx', null, '1');
INSERT INTO `sys_area` VALUES ('430522', '新邵县', '3', '430500', '0739', 'xsx', null, '1');
INSERT INTO `sys_area` VALUES ('430523', '邵阳县', '3', '430500', '0739', 'syx', null, '1');
INSERT INTO `sys_area` VALUES ('430524', '隆回县', '3', '430500', '0739', 'lhx', null, '1');
INSERT INTO `sys_area` VALUES ('430525', '洞口县', '3', '430500', '0739', 'dkx', null, '1');
INSERT INTO `sys_area` VALUES ('430527', '绥宁县', '3', '430500', '0739', 'snx', null, '1');
INSERT INTO `sys_area` VALUES ('430528', '新宁县', '3', '430500', '0739', 'xnx', null, '1');
INSERT INTO `sys_area` VALUES ('430529', '城步苗族自治县', '3', '430500', '0739', 'cbmzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('430581', '武冈市', '3', '430500', '0739', 'wgs', null, '1');
INSERT INTO `sys_area` VALUES ('430600', '岳阳市', '2', '430000', null, 'yys', '1', '1');
INSERT INTO `sys_area` VALUES ('430601', '市辖区', '3', '430600', '0730', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('430602', '岳阳楼区', '3', '430600', '0730', 'yylq', null, '1');
INSERT INTO `sys_area` VALUES ('430603', '云溪区', '3', '430600', '0730', 'yxq', null, '1');
INSERT INTO `sys_area` VALUES ('430611', '君山区', '3', '430600', '0730', 'jsq', null, '1');
INSERT INTO `sys_area` VALUES ('430621', '岳阳县', '3', '430600', '0730', 'yyx', null, '1');
INSERT INTO `sys_area` VALUES ('430623', '华容县', '3', '430600', '0730', 'hrx', null, '1');
INSERT INTO `sys_area` VALUES ('430624', '湘阴县', '3', '430600', '0730', 'xyx', null, '1');
INSERT INTO `sys_area` VALUES ('430626', '平江县', '3', '430600', '0730', 'pjx', null, '1');
INSERT INTO `sys_area` VALUES ('430681', '汨罗市', '3', '430600', '0730', 'mls', null, '1');
INSERT INTO `sys_area` VALUES ('430682', '临湘市', '3', '430600', '0730', 'lxs', null, '1');
INSERT INTO `sys_area` VALUES ('430700', '常德市', '2', '430000', null, 'cds', '1', '1');
INSERT INTO `sys_area` VALUES ('430701', '市辖区', '3', '430700', '0736', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('430702', '武陵区', '3', '430700', '0736', 'wlq', null, '1');
INSERT INTO `sys_area` VALUES ('430703', '鼎城区', '3', '430700', '0736', 'dcq', null, '1');
INSERT INTO `sys_area` VALUES ('430721', '安乡县', '3', '430700', '0736', 'axx', null, '1');
INSERT INTO `sys_area` VALUES ('430722', '汉寿县', '3', '430700', '0736', 'hsx', null, '1');
INSERT INTO `sys_area` VALUES ('430723', '澧县', '3', '430700', '0736', 'lx', null, '1');
INSERT INTO `sys_area` VALUES ('430724', '临澧县', '3', '430700', '0736', 'llx', null, '1');
INSERT INTO `sys_area` VALUES ('430725', '桃源县', '3', '430700', '0736', 'tyx', null, '1');
INSERT INTO `sys_area` VALUES ('430726', '石门县', '3', '430700', '0736', 'smx', null, '1');
INSERT INTO `sys_area` VALUES ('430781', '津市市', '3', '430700', '0736', 'jss', null, '1');
INSERT INTO `sys_area` VALUES ('430800', '张家界市', '2', '430000', null, 'zjjs', '1', '1');
INSERT INTO `sys_area` VALUES ('430801', '市辖区', '3', '430800', '0744', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('430802', '永定区', '3', '430800', '0744', 'ydq', null, '1');
INSERT INTO `sys_area` VALUES ('430811', '武陵源区', '3', '430800', '0744', 'wlyq', null, '1');
INSERT INTO `sys_area` VALUES ('430821', '慈利县', '3', '430800', '0744', 'clx', null, '1');
INSERT INTO `sys_area` VALUES ('430822', '桑植县', '3', '430800', '0744', 'szx', null, '1');
INSERT INTO `sys_area` VALUES ('430900', '益阳市', '2', '430000', null, 'yys', '1', '1');
INSERT INTO `sys_area` VALUES ('430901', '市辖区', '3', '430900', '0737', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('430902', '资阳区', '3', '430900', '0737', 'zyq', null, '1');
INSERT INTO `sys_area` VALUES ('430903', '赫山区', '3', '430900', '0737', 'hsq', null, '1');
INSERT INTO `sys_area` VALUES ('430921', '南县', '3', '430900', '0737', 'nx', null, '1');
INSERT INTO `sys_area` VALUES ('430922', '桃江县', '3', '430900', '0737', 'tjx', null, '1');
INSERT INTO `sys_area` VALUES ('430923', '安化县', '3', '430900', '0737', 'ahx', null, '1');
INSERT INTO `sys_area` VALUES ('430981', '沅江市', '3', '430900', '0737', 'yjs', null, '1');
INSERT INTO `sys_area` VALUES ('431000', '郴州市', '2', '430000', null, 'czs', '1', '1');
INSERT INTO `sys_area` VALUES ('431001', '市辖区', '3', '431000', '0735', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('431002', '北湖区', '3', '431000', '0735', 'bhq', null, '1');
INSERT INTO `sys_area` VALUES ('431003', '苏仙区', '3', '431000', '0735', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('431021', '桂阳县', '3', '431000', '0735', 'gyx', null, '1');
INSERT INTO `sys_area` VALUES ('431022', '宜章县', '3', '431000', '0735', 'yzx', null, '1');
INSERT INTO `sys_area` VALUES ('431023', '永兴县', '3', '431000', '0735', 'yxx', null, '1');
INSERT INTO `sys_area` VALUES ('431024', '嘉禾县', '3', '431000', '0735', 'jhx', null, '1');
INSERT INTO `sys_area` VALUES ('431025', '临武县', '3', '431000', '0735', 'lwx', null, '1');
INSERT INTO `sys_area` VALUES ('431026', '汝城县', '3', '431000', '0735', 'rcx', null, '1');
INSERT INTO `sys_area` VALUES ('431027', '桂东县', '3', '431000', '0735', 'gdx', null, '1');
INSERT INTO `sys_area` VALUES ('431028', '安仁县', '3', '431000', '0735', 'arx', null, '1');
INSERT INTO `sys_area` VALUES ('431081', '资兴市', '3', '431000', '0735', 'zxs', null, '1');
INSERT INTO `sys_area` VALUES ('431100', '永州市', '2', '430000', null, 'yzs', '1', '1');
INSERT INTO `sys_area` VALUES ('431101', '市辖区', '3', '431100', '0746', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('431102', '零陵区', '3', '431100', '0746', 'llq', null, '1');
INSERT INTO `sys_area` VALUES ('431103', '冷水滩区', '3', '431100', '0746', 'lstq', null, '1');
INSERT INTO `sys_area` VALUES ('431121', '祁阳县', '3', '431100', '0746', 'qyx', null, '1');
INSERT INTO `sys_area` VALUES ('431122', '东安县', '3', '431100', '0746', 'dax', null, '1');
INSERT INTO `sys_area` VALUES ('431123', '双牌县', '3', '431100', '0746', 'spx', null, '1');
INSERT INTO `sys_area` VALUES ('431124', '道县', '3', '431100', '0746', 'dx', null, '1');
INSERT INTO `sys_area` VALUES ('431125', '江永县', '3', '431100', '0746', 'jyx', null, '1');
INSERT INTO `sys_area` VALUES ('431126', '宁远县', '3', '431100', '0746', 'nyx', null, '1');
INSERT INTO `sys_area` VALUES ('431127', '蓝山县', '3', '431100', '0746', 'lsx', null, '1');
INSERT INTO `sys_area` VALUES ('431128', '新田县', '3', '431100', '0746', 'xtx', null, '1');
INSERT INTO `sys_area` VALUES ('431129', '江华瑶族自治县', '3', '431100', '0746', 'jhyzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('431200', '怀化市', '2', '430000', null, 'hhs', '1', '1');
INSERT INTO `sys_area` VALUES ('431201', '市辖区', '3', '431200', '0745', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('431202', '鹤城区', '3', '431200', '0745', 'hcq', null, '1');
INSERT INTO `sys_area` VALUES ('431221', '中方县', '3', '431200', '0745', 'zfx', null, '1');
INSERT INTO `sys_area` VALUES ('431222', '沅陵县', '3', '431200', '0745', 'ylx', null, '1');
INSERT INTO `sys_area` VALUES ('431223', '辰溪县', '3', '431200', '0745', 'cxx', null, '1');
INSERT INTO `sys_area` VALUES ('431224', '溆浦县', '3', '431200', '0745', 'xpx', null, '1');
INSERT INTO `sys_area` VALUES ('431225', '会同县', '3', '431200', '0745', 'htx', null, '1');
INSERT INTO `sys_area` VALUES ('431226', '麻阳苗族自治县', '3', '431200', '0745', 'mymzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('431227', '新晃侗族自治县', '3', '431200', '0745', 'xhdzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('431228', '芷江侗族自治县', '3', '431200', '0745', 'zjdzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('431229', '靖州苗族侗族自治县', '3', '431200', '0745', 'jzmzdzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('431230', '通道侗族自治县', '3', '431200', '0745', 'tddzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('431281', '洪江市', '3', '431200', '0745', 'hjs', null, '1');
INSERT INTO `sys_area` VALUES ('431300', '娄底市', '2', '430000', null, 'lds', '1', '1');
INSERT INTO `sys_area` VALUES ('431301', '市辖区', '3', '431300', '0738', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('431302', '娄星区', '3', '431300', '0738', 'lxq', null, '1');
INSERT INTO `sys_area` VALUES ('431321', '双峰县', '3', '431300', '0738', 'sfx', null, '1');
INSERT INTO `sys_area` VALUES ('431322', '新化县', '3', '431300', '0738', 'xhx', null, '1');
INSERT INTO `sys_area` VALUES ('431381', '冷水江市', '3', '431300', '0738', 'lsjs', null, '1');
INSERT INTO `sys_area` VALUES ('431382', '涟源市', '3', '431300', '0738', 'lys', null, '1');
INSERT INTO `sys_area` VALUES ('433100', '湘西土家族苗族自治州', '2', '430000', null, 'xxtjzmzzzz', '1', '1');
INSERT INTO `sys_area` VALUES ('433101', '吉首市', '3', '433100', '0743', 'jss', null, '1');
INSERT INTO `sys_area` VALUES ('433122', '泸溪县', '3', '433100', '0743', 'lxx', null, '1');
INSERT INTO `sys_area` VALUES ('433123', '凤凰县', '3', '433100', '0743', 'fhx', null, '1');
INSERT INTO `sys_area` VALUES ('433124', '花垣县', '3', '433100', '0743', 'hyx', null, '1');
INSERT INTO `sys_area` VALUES ('433125', '保靖县', '3', '433100', '0743', 'bjx', null, '1');
INSERT INTO `sys_area` VALUES ('433126', '古丈县', '3', '433100', '0743', 'gzx', null, '1');
INSERT INTO `sys_area` VALUES ('433127', '永顺县', '3', '433100', '0743', 'ysx', null, '1');
INSERT INTO `sys_area` VALUES ('433130', '龙山县', '3', '433100', '0743', 'lsx', null, '1');
INSERT INTO `sys_area` VALUES ('440000', '广东省', '1', '0', null, 'gds', null, '1');
INSERT INTO `sys_area` VALUES ('440100', '广州市', '2', '440000', null, 'gzs', null, '1');
INSERT INTO `sys_area` VALUES ('440101', '市辖区', '3', '440100', '020', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('440103', '荔湾区', '3', '440100', '020', 'lwq', null, '1');
INSERT INTO `sys_area` VALUES ('440104', '越秀区', '3', '440100', '020', 'yxq', null, '1');
INSERT INTO `sys_area` VALUES ('440105', '海珠区', '3', '440100', '020', 'hzq', null, '1');
INSERT INTO `sys_area` VALUES ('440106', '天河区', '3', '440100', '020', 'thq', null, '1');
INSERT INTO `sys_area` VALUES ('440111', '白云区', '3', '440100', '020', 'byq', null, '1');
INSERT INTO `sys_area` VALUES ('440112', '黄埔区', '3', '440100', '020', 'hpq', null, '1');
INSERT INTO `sys_area` VALUES ('440113', '番禺区', '3', '440100', '020', 'fyq', null, '1');
INSERT INTO `sys_area` VALUES ('440114', '花都区', '3', '440100', '020', 'hdq', null, '1');
INSERT INTO `sys_area` VALUES ('440115', '南沙区', '3', '440100', null, 'nsq', null, '1');
INSERT INTO `sys_area` VALUES ('440116', '萝岗区', '3', '440100', null, 'lgq', null, '1');
INSERT INTO `sys_area` VALUES ('440183', '增城市', '3', '440100', '020', 'zcs', null, '1');
INSERT INTO `sys_area` VALUES ('440184', '从化市', '3', '440100', '020', 'chs', null, '1');
INSERT INTO `sys_area` VALUES ('440200', '韶关市', '2', '440000', null, 'sgs', null, '1');
INSERT INTO `sys_area` VALUES ('440201', '市辖区', '3', '440200', '0751', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('440203', '武江区', '3', '440200', '0751', 'wjq', null, '1');
INSERT INTO `sys_area` VALUES ('440204', '浈江区', '3', '440200', '0751', 'zjq', null, '1');
INSERT INTO `sys_area` VALUES ('440205', '曲江区', '3', '440200', '0751', 'qjq', null, '1');
INSERT INTO `sys_area` VALUES ('440222', '始兴县', '3', '440200', '0751', 'sxx', null, '1');
INSERT INTO `sys_area` VALUES ('440224', '仁化县', '3', '440200', '0751', 'rhx', null, '1');
INSERT INTO `sys_area` VALUES ('440229', '翁源县', '3', '440200', '0751', 'wyx', null, '1');
INSERT INTO `sys_area` VALUES ('440232', '乳源瑶族自治县', '3', '440200', '0751', 'ryyzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('440233', '新丰县', '3', '440200', '0751', 'xfx', null, '1');
INSERT INTO `sys_area` VALUES ('440281', '乐昌市', '3', '440200', '0751', 'lcs', null, '1');
INSERT INTO `sys_area` VALUES ('440282', '南雄市', '3', '440200', '0751', 'nxs', null, '1');
INSERT INTO `sys_area` VALUES ('440300', '深圳市', '2', '440000', null, 'szs', null, '1');
INSERT INTO `sys_area` VALUES ('440301', '市辖区', '3', '440300', '0755', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('440303', '罗湖区', '3', '440300', '0755', 'lhq', null, '1');
INSERT INTO `sys_area` VALUES ('440304', '福田区', '3', '440300', '0755', 'ftq', null, '1');
INSERT INTO `sys_area` VALUES ('440305', '南山区', '3', '440300', '0755', 'nsq', null, '1');
INSERT INTO `sys_area` VALUES ('440306', '宝安区', '3', '440300', '0755', 'baq', null, '1');
INSERT INTO `sys_area` VALUES ('440307', '龙岗区', '3', '440300', '0755', 'lgq', null, '1');
INSERT INTO `sys_area` VALUES ('440308', '盐田区', '3', '440300', '0755', 'ytq', null, '1');
INSERT INTO `sys_area` VALUES ('440400', '珠海市', '2', '440000', null, 'zhs', null, '1');
INSERT INTO `sys_area` VALUES ('440401', '市辖区', '3', '440400', '0756', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('440402', '香洲区', '3', '440400', '0756', 'xzq', null, '1');
INSERT INTO `sys_area` VALUES ('440403', '斗门区', '3', '440400', '0756', 'dmq', null, '1');
INSERT INTO `sys_area` VALUES ('440404', '金湾区', '3', '440400', '0756', 'jwq', null, '1');
INSERT INTO `sys_area` VALUES ('440500', '汕头市', '2', '440000', null, 'sts', null, '1');
INSERT INTO `sys_area` VALUES ('440501', '市辖区', '3', '440500', '0754', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('440507', '龙湖区', '3', '440500', '0754', 'lhq', null, '1');
INSERT INTO `sys_area` VALUES ('440511', '金平区', '3', '440500', '0754', 'jpq', null, '1');
INSERT INTO `sys_area` VALUES ('440512', '濠江区', '3', '440500', '0754', 'hjq', null, '1');
INSERT INTO `sys_area` VALUES ('440513', '潮阳区', '3', '440500', '0661', 'cyq', null, '1');
INSERT INTO `sys_area` VALUES ('440514', '潮南区', '3', '440500', '0661', 'cnq', null, '1');
INSERT INTO `sys_area` VALUES ('440515', '澄海区', '3', '440500', '0754', 'chq', null, '1');
INSERT INTO `sys_area` VALUES ('440523', '南澳县', '3', '440500', '0754', 'nax', null, '1');
INSERT INTO `sys_area` VALUES ('440600', '佛山市', '2', '440000', null, 'fss', null, '1');
INSERT INTO `sys_area` VALUES ('440601', '市辖区', '3', '440600', '0757', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('440604', '禅城区', '3', '440600', '0757', 'scq', null, '1');
INSERT INTO `sys_area` VALUES ('440605', '南海区', '3', '440600', '0757', 'nhq', null, '1');
INSERT INTO `sys_area` VALUES ('440606', '顺德区', '3', '440600', '0757', 'sdq', null, '1');
INSERT INTO `sys_area` VALUES ('440607', '三水区', '3', '440600', '0757', 'ssq', null, '1');
INSERT INTO `sys_area` VALUES ('440608', '高明区', '3', '440600', '0757', 'gmq', null, '1');
INSERT INTO `sys_area` VALUES ('440700', '江门市', '2', '440000', null, 'jms', null, '1');
INSERT INTO `sys_area` VALUES ('440701', '市辖区', '3', '440700', '0750', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('440703', '蓬江区', '3', '440700', '0750', 'pjq', null, '1');
INSERT INTO `sys_area` VALUES ('440704', '江海区', '3', '440700', '0750', 'jhq', null, '1');
INSERT INTO `sys_area` VALUES ('440705', '新会区', '3', '440700', '0750', 'xhq', null, '1');
INSERT INTO `sys_area` VALUES ('440781', '台山市', '3', '440700', '0750', 'tss', null, '1');
INSERT INTO `sys_area` VALUES ('440783', '开平市', '3', '440700', '0750', 'kps', null, '1');
INSERT INTO `sys_area` VALUES ('440784', '鹤山市', '3', '440700', '0750', 'hss', null, '1');
INSERT INTO `sys_area` VALUES ('440785', '恩平市', '3', '440700', '0750', 'eps', null, '1');
INSERT INTO `sys_area` VALUES ('440800', '湛江市', '2', '440000', null, 'zjs', null, '1');
INSERT INTO `sys_area` VALUES ('440801', '市辖区', '3', '440800', '0759', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('440802', '赤坎区', '3', '440800', '0759', 'ckq', null, '1');
INSERT INTO `sys_area` VALUES ('440803', '霞山区', '3', '440800', '0759', 'xsq', null, '1');
INSERT INTO `sys_area` VALUES ('440804', '坡头区', '3', '440800', '0759', 'ptq', null, '1');
INSERT INTO `sys_area` VALUES ('440811', '麻章区', '3', '440800', '0759', 'mzq', null, '1');
INSERT INTO `sys_area` VALUES ('440823', '遂溪县', '3', '440800', '0759', 'sxx', null, '1');
INSERT INTO `sys_area` VALUES ('440825', '徐闻县', '3', '440800', '0759', 'xwx', null, '1');
INSERT INTO `sys_area` VALUES ('440881', '廉江市', '3', '440800', '0759', 'ljs', null, '1');
INSERT INTO `sys_area` VALUES ('440882', '雷州市', '3', '440800', '0759', 'lzs', null, '1');
INSERT INTO `sys_area` VALUES ('440883', '吴川市', '3', '440800', '0759', 'wcs', null, '1');
INSERT INTO `sys_area` VALUES ('440900', '茂名市', '2', '440000', null, 'mms', null, '1');
INSERT INTO `sys_area` VALUES ('440901', '市辖区', '3', '440900', '0688', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('440902', '茂南区', '3', '440900', '0668', 'mnq', null, '1');
INSERT INTO `sys_area` VALUES ('440903', '茂港区', '3', '440900', '0668', 'mgq', null, '1');
INSERT INTO `sys_area` VALUES ('440923', '电白县', '3', '440900', '0668', 'dbx', null, '1');
INSERT INTO `sys_area` VALUES ('440981', '高州市', '3', '440900', '0668', 'gzs', null, '1');
INSERT INTO `sys_area` VALUES ('440982', '化州市', '3', '440900', '0668', 'hzs', null, '1');
INSERT INTO `sys_area` VALUES ('440983', '信宜市', '3', '440900', '0668', 'xys', null, '1');
INSERT INTO `sys_area` VALUES ('441200', '肇庆市', '2', '440000', null, 'zqs', null, '1');
INSERT INTO `sys_area` VALUES ('441201', '市辖区', '3', '441200', '0758', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('441202', '端州区', '3', '441200', '0758', 'dzq', null, '1');
INSERT INTO `sys_area` VALUES ('441203', '鼎湖区', '3', '441200', '0758', 'dhq', null, '1');
INSERT INTO `sys_area` VALUES ('441223', '广宁县', '3', '441200', '0758', 'gnx', null, '1');
INSERT INTO `sys_area` VALUES ('441224', '怀集县', '3', '441200', '0758', 'hjx', null, '1');
INSERT INTO `sys_area` VALUES ('441225', '封开县', '3', '441200', '0758', 'fkx', null, '1');
INSERT INTO `sys_area` VALUES ('441226', '德庆县', '3', '441200', '0758', 'dqx', null, '1');
INSERT INTO `sys_area` VALUES ('441283', '高要市', '3', '441200', '0758', 'gys', null, '1');
INSERT INTO `sys_area` VALUES ('441284', '四会市', '3', '441200', '0758', 'shs', null, '1');
INSERT INTO `sys_area` VALUES ('441300', '惠州市', '2', '440000', null, 'hzs', null, '1');
INSERT INTO `sys_area` VALUES ('441301', '市辖区', '3', '441300', '0752', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('441302', '惠城区', '3', '441300', '0752', 'hcq', null, '1');
INSERT INTO `sys_area` VALUES ('441303', '惠阳区', '3', '441300', '0752', 'hyq', null, '1');
INSERT INTO `sys_area` VALUES ('441322', '博罗县', '3', '441300', '0752', 'blx', null, '1');
INSERT INTO `sys_area` VALUES ('441323', '惠东县', '3', '441300', '0752', 'hdx', null, '1');
INSERT INTO `sys_area` VALUES ('441324', '龙门县', '3', '441300', '0752', 'lmx', null, '1');
INSERT INTO `sys_area` VALUES ('441400', '梅州市', '2', '440000', null, 'mzs', null, '1');
INSERT INTO `sys_area` VALUES ('441401', '市辖区', '3', '441400', '0753', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('441402', '梅江区', '3', '441400', '0753', 'mjq', null, '1');
INSERT INTO `sys_area` VALUES ('441421', '梅县', '3', '441400', '0753', 'mx', null, '1');
INSERT INTO `sys_area` VALUES ('441422', '大埔县', '3', '441400', '0753', 'dpx', null, '1');
INSERT INTO `sys_area` VALUES ('441423', '丰顺县', '3', '441400', '0753', 'fsx', null, '1');
INSERT INTO `sys_area` VALUES ('441424', '五华县', '3', '441400', '0753', 'whx', null, '1');
INSERT INTO `sys_area` VALUES ('441426', '平远县', '3', '441400', '0753', 'pyx', null, '1');
INSERT INTO `sys_area` VALUES ('441427', '蕉岭县', '3', '441400', '0753', 'jlx', null, '1');
INSERT INTO `sys_area` VALUES ('441481', '兴宁市', '3', '441400', '0753', 'xns', null, '1');
INSERT INTO `sys_area` VALUES ('441500', '汕尾市', '2', '440000', null, 'sws', null, '1');
INSERT INTO `sys_area` VALUES ('441501', '市辖区', '3', '441500', '0660', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('441502', '城区', '3', '441500', '0660', 'cq', null, '1');
INSERT INTO `sys_area` VALUES ('441521', '海丰县', '3', '441500', '0660', 'hfx', null, '1');
INSERT INTO `sys_area` VALUES ('441523', '陆河县', '3', '441500', '0660', 'lhx', null, '1');
INSERT INTO `sys_area` VALUES ('441581', '陆丰市', '3', '441500', '0660', 'lfs', null, '1');
INSERT INTO `sys_area` VALUES ('441600', '河源市', '2', '440000', null, 'hys', null, '1');
INSERT INTO `sys_area` VALUES ('441601', '市辖区', '3', '441600', '0762', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('441602', '源城区', '3', '441600', '0762', 'ycq', null, '1');
INSERT INTO `sys_area` VALUES ('441621', '紫金县', '3', '441600', '0762', 'zjx', null, '1');
INSERT INTO `sys_area` VALUES ('441622', '龙川县', '3', '441600', '0762', 'lcx', null, '1');
INSERT INTO `sys_area` VALUES ('441623', '连平县', '3', '441600', '0762', 'lpx', null, '1');
INSERT INTO `sys_area` VALUES ('441624', '和平县', '3', '441600', '0762', 'hpx', null, '1');
INSERT INTO `sys_area` VALUES ('441625', '东源县', '3', '441600', '0762', 'dyx', null, '1');
INSERT INTO `sys_area` VALUES ('441700', '阳江市', '2', '440000', null, 'yjs', null, '1');
INSERT INTO `sys_area` VALUES ('441701', '市辖区', '3', '441700', '0662', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('441702', '江城区', '3', '441700', '0662', 'jcq', null, '1');
INSERT INTO `sys_area` VALUES ('441721', '阳西县', '3', '441700', '0662', 'yxx', null, '1');
INSERT INTO `sys_area` VALUES ('441723', '阳东县', '3', '441700', '0662', 'ydx', null, '1');
INSERT INTO `sys_area` VALUES ('441781', '阳春市', '3', '441700', '0662', 'ycs', null, '1');
INSERT INTO `sys_area` VALUES ('441800', '清远市', '2', '440000', null, 'qys', null, '1');
INSERT INTO `sys_area` VALUES ('441801', '市辖区', '3', '441800', '0763', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('441802', '清城区', '3', '441800', '0763', 'qcq', null, '1');
INSERT INTO `sys_area` VALUES ('441821', '佛冈县', '3', '441800', '0763', 'fgx', null, '1');
INSERT INTO `sys_area` VALUES ('441823', '阳山县', '3', '441800', '0763', 'ysx', null, '1');
INSERT INTO `sys_area` VALUES ('441825', '连山壮族瑶族自治县', '3', '441800', '0763', 'lszzyzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('441826', '连南瑶族自治县', '3', '441800', '0763', 'lnyzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('441827', '清新县', '3', '441800', '0763', 'qxx', null, '1');
INSERT INTO `sys_area` VALUES ('441881', '英德市', '3', '441800', '0763', 'yds', null, '1');
INSERT INTO `sys_area` VALUES ('441882', '连州市', '3', '441800', '0763', 'lzs', null, '1');
INSERT INTO `sys_area` VALUES ('441900', '东莞市', '2', '440000', null, 'dgs', null, '1');
INSERT INTO `sys_area` VALUES ('442000', '中山市', '2', '440000', null, 'zss', null, '1');
INSERT INTO `sys_area` VALUES ('445100', '潮州市', '2', '440000', null, 'czs', null, '1');
INSERT INTO `sys_area` VALUES ('445101', '市辖区', '3', '445100', '0768', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('445102', '湘桥区', '3', '445100', '0768', 'xqq', null, '1');
INSERT INTO `sys_area` VALUES ('445121', '潮安县', '3', '445100', '0768', 'cax', null, '1');
INSERT INTO `sys_area` VALUES ('445122', '饶平县', '3', '445100', '0768', 'rpx', null, '1');
INSERT INTO `sys_area` VALUES ('445200', '揭阳市', '2', '440000', null, 'jys', null, '1');
INSERT INTO `sys_area` VALUES ('445201', '市辖区', '3', '445200', '0663', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('445202', '榕城区', '3', '445200', '0663', 'rcq', null, '1');
INSERT INTO `sys_area` VALUES ('445221', '揭东县', '3', '445200', '0663', 'jdx', null, '1');
INSERT INTO `sys_area` VALUES ('445222', '揭西县', '3', '445200', '0663', 'jxx', null, '1');
INSERT INTO `sys_area` VALUES ('445224', '惠来县', '3', '445200', '0663', 'hlx', null, '1');
INSERT INTO `sys_area` VALUES ('445281', '普宁市', '3', '445200', '0663', 'pns', null, '1');
INSERT INTO `sys_area` VALUES ('445300', '云浮市', '2', '440000', null, 'yfs', null, '1');
INSERT INTO `sys_area` VALUES ('445301', '市辖区', '3', '445300', '0766', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('445302', '云城区', '3', '445300', '0766', 'ycq', null, '1');
INSERT INTO `sys_area` VALUES ('445321', '新兴县', '3', '445300', '0766', 'xxx', null, '1');
INSERT INTO `sys_area` VALUES ('445322', '郁南县', '3', '445300', '0766', 'ynx', null, '1');
INSERT INTO `sys_area` VALUES ('445323', '云安县', '3', '445300', '0766', 'yax', null, '1');
INSERT INTO `sys_area` VALUES ('445381', '罗定市', '3', '445300', '0766', 'lds', null, '1');
INSERT INTO `sys_area` VALUES ('450000', '广西壮族自治区', '1', '0', null, 'gxzzzzq', null, '1');
INSERT INTO `sys_area` VALUES ('450100', '南宁市', '2', '450000', null, 'nns', null, '1');
INSERT INTO `sys_area` VALUES ('450101', '市辖区', '3', '450100', '0771', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('450102', '兴宁区', '3', '450100', '0771', 'xnq', null, '1');
INSERT INTO `sys_area` VALUES ('450103', '青秀区', '3', '450100', '0771', 'qxq', null, '1');
INSERT INTO `sys_area` VALUES ('450105', '江南区', '3', '450100', '0771', 'jnq', null, '1');
INSERT INTO `sys_area` VALUES ('450107', '西乡塘区', '3', '450100', '0771', 'xxtq', null, '1');
INSERT INTO `sys_area` VALUES ('450108', '良庆区', '3', '450100', '0771', 'lqq', null, '1');
INSERT INTO `sys_area` VALUES ('450109', '邕宁区', '3', '450100', '0771', 'ynq', null, '1');
INSERT INTO `sys_area` VALUES ('450122', '武鸣县', '3', '450100', '0771', 'wmx', null, '1');
INSERT INTO `sys_area` VALUES ('450123', '隆安县', '3', '450100', '0771', 'lax', null, '1');
INSERT INTO `sys_area` VALUES ('450124', '马山县', '3', '450100', '0771', 'msx', null, '1');
INSERT INTO `sys_area` VALUES ('450125', '上林县', '3', '450100', '0771', 'slx', null, '1');
INSERT INTO `sys_area` VALUES ('450126', '宾阳县', '3', '450100', '0771', 'byx', null, '1');
INSERT INTO `sys_area` VALUES ('450127', '横县', '3', '450100', '0771', 'hx', null, '1');
INSERT INTO `sys_area` VALUES ('450200', '柳州市', '2', '450000', null, 'lzs', null, '1');
INSERT INTO `sys_area` VALUES ('450201', '市辖区', '3', '450200', '0772', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('450202', '城中区', '3', '450200', '0772', 'czq', null, '1');
INSERT INTO `sys_area` VALUES ('450203', '鱼峰区', '3', '450200', '0772', 'yfq', null, '1');
INSERT INTO `sys_area` VALUES ('450204', '柳南区', '3', '450200', '0772', 'lnq', null, '1');
INSERT INTO `sys_area` VALUES ('450205', '柳北区', '3', '450200', '0772', 'lbq', null, '1');
INSERT INTO `sys_area` VALUES ('450221', '柳江县', '3', '450200', '0772', 'ljx', null, '1');
INSERT INTO `sys_area` VALUES ('450222', '柳城县', '3', '450200', '0772', 'lcx', null, '1');
INSERT INTO `sys_area` VALUES ('450223', '鹿寨县', '3', '450200', '0772', 'lzx', null, '1');
INSERT INTO `sys_area` VALUES ('450224', '融安县', '3', '450200', '0772', 'rax', null, '1');
INSERT INTO `sys_area` VALUES ('450225', '融水苗族自治县', '3', '450200', '0772', 'rsmzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('450226', '三江侗族自治县', '3', '450200', '0772', 'sjdzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('450300', '桂林市', '2', '450000', null, 'gls', null, '1');
INSERT INTO `sys_area` VALUES ('450301', '市辖区', '3', '450300', '0773', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('450302', '秀峰区', '3', '450300', '0773', 'xfq', null, '1');
INSERT INTO `sys_area` VALUES ('450303', '叠彩区', '3', '450300', '0773', 'dcq', null, '1');
INSERT INTO `sys_area` VALUES ('450304', '象山区', '3', '450300', '0773', 'xsq', null, '1');
INSERT INTO `sys_area` VALUES ('450305', '七星区', '3', '450300', '0773', 'qxq', null, '1');
INSERT INTO `sys_area` VALUES ('450311', '雁山区', '3', '450300', '0773', 'ysq', null, '1');
INSERT INTO `sys_area` VALUES ('450321', '阳朔县', '3', '450300', '0773', 'ysx', null, '1');
INSERT INTO `sys_area` VALUES ('450322', '临桂县', '3', '450300', '0773', 'lgx', null, '1');
INSERT INTO `sys_area` VALUES ('450323', '灵川县', '3', '450300', '0773', 'lcx', null, '1');
INSERT INTO `sys_area` VALUES ('450324', '全州县', '3', '450300', '0773', 'qzx', null, '1');
INSERT INTO `sys_area` VALUES ('450325', '兴安县', '3', '450300', '0773', 'xax', null, '1');
INSERT INTO `sys_area` VALUES ('450326', '永福县', '3', '450300', '0773', 'yfx', null, '1');
INSERT INTO `sys_area` VALUES ('450327', '灌阳县', '3', '450300', '0773', 'gyx', null, '1');
INSERT INTO `sys_area` VALUES ('450328', '龙胜各族自治县', '3', '450300', '0773', 'lsgzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('450329', '资源县', '3', '450300', '0773', 'zyx', null, '1');
INSERT INTO `sys_area` VALUES ('450330', '平乐县', '3', '450300', '0773', 'plx', null, '1');
INSERT INTO `sys_area` VALUES ('450331', '荔浦县', '3', '450300', '0773', 'lpx', null, '1');
INSERT INTO `sys_area` VALUES ('450332', '恭城瑶族自治县', '3', '450300', '0773', 'gcyzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('450400', '梧州市', '2', '450000', null, 'wzs', null, '1');
INSERT INTO `sys_area` VALUES ('450401', '市辖区', '3', '450400', '0774', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('450403', '万秀区', '3', '450400', '0774', 'wxq', null, '1');
INSERT INTO `sys_area` VALUES ('450404', '蝶山区', '3', '450400', '0774', 'dsq', null, '1');
INSERT INTO `sys_area` VALUES ('450405', '长洲区', '3', '450400', '0774', 'zzq', null, '1');
INSERT INTO `sys_area` VALUES ('450421', '苍梧县', '3', '450400', '0774', 'cwx', null, '1');
INSERT INTO `sys_area` VALUES ('450422', '藤县', '3', '450400', '0774', 'tx', null, '1');
INSERT INTO `sys_area` VALUES ('450423', '蒙山县', '3', '450400', '0774', 'msx', null, '1');
INSERT INTO `sys_area` VALUES ('450481', '岑溪市', '3', '450400', '0774', 'cxs', null, '1');
INSERT INTO `sys_area` VALUES ('450500', '北海市', '2', '450000', null, 'bhs', null, '1');
INSERT INTO `sys_area` VALUES ('450501', '市辖区', '3', '450500', '0779', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('450502', '海城区', '3', '450500', '0779', 'hcq', null, '1');
INSERT INTO `sys_area` VALUES ('450503', '银海区', '3', '450500', '0779', 'yhq', null, '1');
INSERT INTO `sys_area` VALUES ('450512', '铁山港区', '3', '450500', '0779', 'tsgq', null, '1');
INSERT INTO `sys_area` VALUES ('450521', '合浦县', '3', '450500', '0779', 'hpx', null, '1');
INSERT INTO `sys_area` VALUES ('450600', '防城港市', '2', '450000', null, 'fcgs', null, '1');
INSERT INTO `sys_area` VALUES ('450601', '市辖区', '3', '450600', '0770', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('450602', '港口区', '3', '450600', '0770', 'gkq', null, '1');
INSERT INTO `sys_area` VALUES ('450603', '防城区', '3', '450600', '0770', 'fcq', null, '1');
INSERT INTO `sys_area` VALUES ('450621', '上思县', '3', '450600', '0770', 'ssx', null, '1');
INSERT INTO `sys_area` VALUES ('450681', '东兴市', '3', '450600', '0770', 'dxs', null, '1');
INSERT INTO `sys_area` VALUES ('450700', '钦州市', '2', '450000', null, 'qzs', null, '1');
INSERT INTO `sys_area` VALUES ('450701', '市辖区', '3', '450700', '0777', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('450702', '钦南区', '3', '450700', '0777', 'qnq', null, '1');
INSERT INTO `sys_area` VALUES ('450703', '钦北区', '3', '450700', '0777', 'qbq', null, '1');
INSERT INTO `sys_area` VALUES ('450721', '灵山县', '3', '450700', '0777', 'lsx', null, '1');
INSERT INTO `sys_area` VALUES ('450722', '浦北县', '3', '450700', '0777', 'pbx', null, '1');
INSERT INTO `sys_area` VALUES ('450800', '贵港市', '2', '450000', null, 'ggs', null, '1');
INSERT INTO `sys_area` VALUES ('450801', '市辖区', '3', '450800', '0775', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('450802', '港北区', '3', '450800', '0775', 'gbq', null, '1');
INSERT INTO `sys_area` VALUES ('450803', '港南区', '3', '450800', '0775', 'gnq', null, '1');
INSERT INTO `sys_area` VALUES ('450804', '覃塘区', '3', '450800', '0775', 'ttq', null, '1');
INSERT INTO `sys_area` VALUES ('450821', '平南县', '3', '450800', '0775', 'pnx', null, '1');
INSERT INTO `sys_area` VALUES ('450881', '桂平市', '3', '450800', '0775', 'gps', null, '1');
INSERT INTO `sys_area` VALUES ('450900', '玉林市', '2', '450000', null, 'yls', null, '1');
INSERT INTO `sys_area` VALUES ('450901', '市辖区', '3', '450900', '0775', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('450902', '玉州区', '3', '450900', '0775', 'yzq', null, '1');
INSERT INTO `sys_area` VALUES ('450921', '容县', '3', '450900', '0775', 'rx', null, '1');
INSERT INTO `sys_area` VALUES ('450922', '陆川县', '3', '450900', '0775', 'lcx', null, '1');
INSERT INTO `sys_area` VALUES ('450923', '博白县', '3', '450900', '0775', 'bbx', null, '1');
INSERT INTO `sys_area` VALUES ('450924', '兴业县', '3', '450900', '0775', 'xyx', null, '1');
INSERT INTO `sys_area` VALUES ('450981', '北流市', '3', '450900', '0775', 'bls', null, '1');
INSERT INTO `sys_area` VALUES ('451000', '百色市', '2', '450000', null, 'bss', null, '1');
INSERT INTO `sys_area` VALUES ('451001', '市辖区', '3', '451000', '0776', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('451002', '右江区', '3', '451000', '0776', 'yjq', null, '1');
INSERT INTO `sys_area` VALUES ('451021', '田阳县', '3', '451000', '0776', 'tyx', null, '1');
INSERT INTO `sys_area` VALUES ('451022', '田东县', '3', '451000', '0776', 'tdx', null, '1');
INSERT INTO `sys_area` VALUES ('451023', '平果县', '3', '451000', '0776', 'pgx', null, '1');
INSERT INTO `sys_area` VALUES ('451024', '德保县', '3', '451000', '0776', 'dbx', null, '1');
INSERT INTO `sys_area` VALUES ('451025', '靖西县', '3', '451000', '0776', 'jxx', null, '1');
INSERT INTO `sys_area` VALUES ('451026', '那坡县', '3', '451000', '0776', 'npx', null, '1');
INSERT INTO `sys_area` VALUES ('451027', '凌云县', '3', '451000', '0776', 'lyx', null, '1');
INSERT INTO `sys_area` VALUES ('451028', '乐业县', '3', '451000', '0776', 'lyx', null, '1');
INSERT INTO `sys_area` VALUES ('451029', '田林县', '3', '451000', '0776', 'tlx', null, '1');
INSERT INTO `sys_area` VALUES ('451030', '西林县', '3', '451000', '0776', 'xlx', null, '1');
INSERT INTO `sys_area` VALUES ('451031', '隆林各族自治县', '3', '451000', '0776', 'llgzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('451100', '贺州市', '2', '450000', null, 'hzs', null, '1');
INSERT INTO `sys_area` VALUES ('451101', '市辖区', '3', '451100', '0774', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('451102', '八步区', '3', '451100', '0774', 'bbq', null, '1');
INSERT INTO `sys_area` VALUES ('451121', '昭平县', '3', '451100', '0774', 'zpx', null, '1');
INSERT INTO `sys_area` VALUES ('451122', '钟山县', '3', '451100', '0774', 'zsx', null, '1');
INSERT INTO `sys_area` VALUES ('451123', '富川瑶族自治县', '3', '451100', '0774', 'fcyzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('451200', '河池市', '2', '450000', null, 'hcs', null, '1');
INSERT INTO `sys_area` VALUES ('451201', '市辖区', '3', '451200', '0778', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('451202', '金城江区', '3', '451200', '0778', 'jcjq', null, '1');
INSERT INTO `sys_area` VALUES ('451221', '南丹县', '3', '451200', '0778', 'ndx', null, '1');
INSERT INTO `sys_area` VALUES ('451222', '天峨县', '3', '451200', '0778', 'tex', null, '1');
INSERT INTO `sys_area` VALUES ('451223', '凤山县', '3', '451200', '0778', 'fsx', null, '1');
INSERT INTO `sys_area` VALUES ('451224', '东兰县', '3', '451200', '0778', 'dlx', null, '1');
INSERT INTO `sys_area` VALUES ('451225', '罗城仫佬族自治县', '3', '451200', '0778', 'lcmlzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('451226', '环江毛南族自治县', '3', '451200', '0778', 'hjmnzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('451227', '巴马瑶族自治县', '3', '451200', '0778', 'bmyzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('451228', '都安瑶族自治县', '3', '451200', '0778', 'dayzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('451229', '大化瑶族自治县', '3', '451200', '0778', 'dhyzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('451281', '宜州市', '3', '451200', '0778', 'yzs', null, '1');
INSERT INTO `sys_area` VALUES ('451300', '来宾市', '2', '450000', null, 'lbs', null, '1');
INSERT INTO `sys_area` VALUES ('451301', '市辖区', '3', '451300', '0772', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('451302', '兴宾区', '3', '451300', '0772', 'xbq', null, '1');
INSERT INTO `sys_area` VALUES ('451321', '忻城县', '3', '451300', '0772', 'xcx', null, '1');
INSERT INTO `sys_area` VALUES ('451322', '象州县', '3', '451300', '0772', 'xzx', null, '1');
INSERT INTO `sys_area` VALUES ('451323', '武宣县', '3', '451300', '0772', 'wxx', null, '1');
INSERT INTO `sys_area` VALUES ('451324', '金秀瑶族自治县', '3', '451300', '0772', 'jxyzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('451381', '合山市', '3', '451300', '0772', 'hss', null, '1');
INSERT INTO `sys_area` VALUES ('451400', '崇左市', '2', '450000', null, 'czs', null, '1');
INSERT INTO `sys_area` VALUES ('451401', '市辖区', '3', '451400', '0771', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('451402', '江洲区', '3', '451400', '0771', 'jzq', null, '1');
INSERT INTO `sys_area` VALUES ('451421', '扶绥县', '3', '451400', '0771', 'fsx', null, '1');
INSERT INTO `sys_area` VALUES ('451422', '宁明县', '3', '451400', '0771', 'nmx', null, '1');
INSERT INTO `sys_area` VALUES ('451423', '龙州县', '3', '451400', '0771', 'lzx', null, '1');
INSERT INTO `sys_area` VALUES ('451424', '大新县', '3', '451400', '0771', 'dxx', null, '1');
INSERT INTO `sys_area` VALUES ('451425', '天等县', '3', '451400', '0771', 'tdx', null, '1');
INSERT INTO `sys_area` VALUES ('451481', '凭祥市', '3', '451400', '0771', 'pxs', null, '1');
INSERT INTO `sys_area` VALUES ('460000', '海南省', '1', '0', null, 'hns', null, '1');
INSERT INTO `sys_area` VALUES ('460100', '海口市', '2', '460000', null, 'hks', null, '1');
INSERT INTO `sys_area` VALUES ('460101', '市辖区', '3', '460100', '0898', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('460105', '秀英区', '3', '460100', '0898', 'xyq', null, '1');
INSERT INTO `sys_area` VALUES ('460106', '龙华区', '3', '460100', '0898', 'lhq', null, '1');
INSERT INTO `sys_area` VALUES ('460107', '琼山区', '3', '460100', '0898', 'qsq', null, '1');
INSERT INTO `sys_area` VALUES ('460108', '美兰区', '3', '460100', '0898', 'mlq', null, '1');
INSERT INTO `sys_area` VALUES ('460200', '三亚市', '2', '460000', null, 'sys', null, '1');
INSERT INTO `sys_area` VALUES ('460201', '市辖区', '3', '460200', '0898', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('460300', '三沙市', '2', '460000', null, 'sss', null, '1');
INSERT INTO `sys_area` VALUES ('460321', '西沙群岛', '3', '460300', null, 'xsqd', null, '1');
INSERT INTO `sys_area` VALUES ('460322', '南沙群岛', '3', '460300', null, 'nsqd', null, '1');
INSERT INTO `sys_area` VALUES ('460323', '中沙群岛的岛礁及其海域', '3', '460300', null, 'zsqdddjjqhy', null, '1');
INSERT INTO `sys_area` VALUES ('469000', '省直辖县级行政区划', '2', '460000', null, 'szxxjxzqh', null, '1');
INSERT INTO `sys_area` VALUES ('469001', '五指山市', '3', '469000', '0898', 'wzss', null, '1');
INSERT INTO `sys_area` VALUES ('469002', '琼海市', '3', '469000', '0898', 'qhs', null, '1');
INSERT INTO `sys_area` VALUES ('469003', '儋州市', '3', '469000', '0898', 'dzs', null, '1');
INSERT INTO `sys_area` VALUES ('469005', '文昌市', '3', '469000', '0898', 'wcs', null, '1');
INSERT INTO `sys_area` VALUES ('469006', '万宁市', '3', '469000', '0898', 'wns', null, '1');
INSERT INTO `sys_area` VALUES ('469007', '东方市', '3', '469000', '0898', 'dfs', null, '1');
INSERT INTO `sys_area` VALUES ('469021', '定安县', '3', '469000', null, 'dax', null, '1');
INSERT INTO `sys_area` VALUES ('469022', '屯昌县', '3', '469000', null, 'tcx', null, '1');
INSERT INTO `sys_area` VALUES ('469023', '澄迈县', '3', '469000', null, 'cmx', null, '1');
INSERT INTO `sys_area` VALUES ('469024', '临高县', '3', '469000', null, 'lgx', null, '1');
INSERT INTO `sys_area` VALUES ('469025', '白沙黎族自治县', '3', '469000', '0898', 'bslzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('469026', '昌江黎族自治县', '3', '469000', '0898', 'cjlzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('469027', '乐东黎族自治县', '3', '469000', '0898', 'ldlzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('469028', '陵水黎族自治县', '3', '469000', '0898', 'lslzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('469029', '保亭黎族苗族自治县', '3', '469000', null, 'btlzmzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('469030', '琼中黎族苗族自治县', '3', '469000', '0898', 'qzlzmzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('500000', '重庆市', '1', '0', null, 'zqs', null, '1');
INSERT INTO `sys_area` VALUES ('500100', '重庆市(辖区)', '2', '500000', null, 'zqs(xq)', null, '1');
INSERT INTO `sys_area` VALUES ('500101', '万州区', '3', '500100', '023', 'wzq', null, '1');
INSERT INTO `sys_area` VALUES ('500102', '涪陵区', '3', '500100', '023', 'flq', null, '1');
INSERT INTO `sys_area` VALUES ('500103', '渝中区', '3', '500100', '023', 'yzq', null, '1');
INSERT INTO `sys_area` VALUES ('500104', '大渡口区', '3', '500100', '023', 'ddkq', null, '1');
INSERT INTO `sys_area` VALUES ('500105', '江北区', '3', '500100', '023', 'jbq', null, '1');
INSERT INTO `sys_area` VALUES ('500106', '沙坪坝区', '3', '500100', '023', 'spbq', null, '1');
INSERT INTO `sys_area` VALUES ('500107', '九龙坡区', '3', '500100', '023', 'jlpq', null, '1');
INSERT INTO `sys_area` VALUES ('500108', '南岸区', '3', '500100', '023', 'naq', null, '1');
INSERT INTO `sys_area` VALUES ('500109', '北碚区', '3', '500100', '023', 'bbq', null, '1');
INSERT INTO `sys_area` VALUES ('500110', '綦江区', '3', '500100', '023', 'qjq', null, '1');
INSERT INTO `sys_area` VALUES ('500111', '大足区', '3', '500100', '023', 'dzq', null, '1');
INSERT INTO `sys_area` VALUES ('500112', '渝北区', '3', '500100', '023', 'ybq', null, '1');
INSERT INTO `sys_area` VALUES ('500113', '巴南区', '3', '500100', '023', 'bnq', null, '1');
INSERT INTO `sys_area` VALUES ('500114', '黔江区', '3', '500100', '023', 'qjq', null, '1');
INSERT INTO `sys_area` VALUES ('500115', '长寿区', '3', '500100', '023', 'zsq', null, '1');
INSERT INTO `sys_area` VALUES ('500116', '江津区', '3', '500100', null, 'jjq', null, '1');
INSERT INTO `sys_area` VALUES ('500117', '合川区', '3', '500100', null, 'hcq', null, '1');
INSERT INTO `sys_area` VALUES ('500118', '永川区', '3', '500100', null, 'ycq', null, '1');
INSERT INTO `sys_area` VALUES ('500119', '南川区', '3', '500100', null, 'ncq', null, '1');
INSERT INTO `sys_area` VALUES ('500200', '县', '2', '500000', null, 'x', null, '1');
INSERT INTO `sys_area` VALUES ('500223', '潼南县', '3', '500200', '023', 'tnx', null, '1');
INSERT INTO `sys_area` VALUES ('500224', '铜梁县', '3', '500200', '023', 'tlx', null, '1');
INSERT INTO `sys_area` VALUES ('500226', '荣昌县', '3', '500200', '023', 'rcx', null, '1');
INSERT INTO `sys_area` VALUES ('500227', '璧山县', '3', '500200', '023', 'bsx', null, '1');
INSERT INTO `sys_area` VALUES ('500228', '梁平县', '3', '500200', '023', 'lpx', null, '1');
INSERT INTO `sys_area` VALUES ('500229', '城口县', '3', '500200', '023', 'ckx', null, '1');
INSERT INTO `sys_area` VALUES ('500230', '丰都县', '3', '500200', '023', 'fdx', null, '1');
INSERT INTO `sys_area` VALUES ('500231', '垫江县', '3', '500200', '023', 'djx', null, '1');
INSERT INTO `sys_area` VALUES ('500232', '武隆县', '3', '500200', '023', 'wlx', null, '1');
INSERT INTO `sys_area` VALUES ('500233', '忠县', '3', '500200', '023', 'zx', null, '1');
INSERT INTO `sys_area` VALUES ('500234', '开县', '3', '500200', '023', 'kx', null, '1');
INSERT INTO `sys_area` VALUES ('500235', '云阳县', '3', '500200', '023', 'yyx', null, '1');
INSERT INTO `sys_area` VALUES ('500236', '奉节县', '3', '500200', '023', 'fjx', null, '1');
INSERT INTO `sys_area` VALUES ('500237', '巫山县', '3', '500200', '023', 'wsx', null, '1');
INSERT INTO `sys_area` VALUES ('500238', '巫溪县', '3', '500200', '023', 'wxx', null, '1');
INSERT INTO `sys_area` VALUES ('500240', '石柱土家族自治县', '3', '500200', '023', 'sztjzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('500241', '秀山土家族苗族自治县', '3', '500200', '023', 'xstjzmzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('500242', '酉阳土家族苗族自治县', '3', '500200', '023', 'yytjzmzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('500243', '彭水苗族土家族自治县', '3', '500200', '023', 'psmztjzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('510000', '四川省', '1', '0', null, 'scs', null, '1');
INSERT INTO `sys_area` VALUES ('510100', '成都市', '2', '510000', null, 'cds', null, '1');
INSERT INTO `sys_area` VALUES ('510101', '市辖区', '3', '510100', '028', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('510104', '锦江区', '3', '510100', '028', 'jjq', null, '1');
INSERT INTO `sys_area` VALUES ('510105', '青羊区', '3', '510100', '028', 'qyq', null, '1');
INSERT INTO `sys_area` VALUES ('510106', '金牛区', '3', '510100', '028', 'jnq', null, '1');
INSERT INTO `sys_area` VALUES ('510107', '武侯区', '3', '510100', '028', 'whq', null, '1');
INSERT INTO `sys_area` VALUES ('510108', '成华区', '3', '510100', '028', 'chq', null, '1');
INSERT INTO `sys_area` VALUES ('510112', '龙泉驿区', '3', '510100', '028', 'lqyq', null, '1');
INSERT INTO `sys_area` VALUES ('510113', '青白江区', '3', '510100', '028', 'qbjq', null, '1');
INSERT INTO `sys_area` VALUES ('510114', '新都区', '3', '510100', '028', 'xdq', null, '1');
INSERT INTO `sys_area` VALUES ('510115', '温江区', '3', '510100', '028', 'wjq', null, '1');
INSERT INTO `sys_area` VALUES ('510121', '金堂县', '3', '510100', '028', 'jtx', null, '1');
INSERT INTO `sys_area` VALUES ('510122', '双流县', '3', '510100', '028', 'slx', null, '1');
INSERT INTO `sys_area` VALUES ('510124', '郫县', '3', '510100', '028', 'px', null, '1');
INSERT INTO `sys_area` VALUES ('510129', '大邑县', '3', '510100', '028', 'dyx', null, '1');
INSERT INTO `sys_area` VALUES ('510131', '蒲江县', '3', '510100', '028', 'pjx', null, '1');
INSERT INTO `sys_area` VALUES ('510132', '新津县', '3', '510100', '028', 'xjx', null, '1');
INSERT INTO `sys_area` VALUES ('510181', '都江堰市', '3', '510100', '028', 'djys', null, '1');
INSERT INTO `sys_area` VALUES ('510182', '彭州市', '3', '510100', '028', 'pzs', null, '1');
INSERT INTO `sys_area` VALUES ('510183', '邛崃市', '3', '510100', '028', 'qls', null, '1');
INSERT INTO `sys_area` VALUES ('510184', '崇州市', '3', '510100', '028', 'czs', null, '1');
INSERT INTO `sys_area` VALUES ('510300', '自贡市', '2', '510000', null, 'zgs', null, '1');
INSERT INTO `sys_area` VALUES ('510301', '市辖区', '3', '510300', '0813', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('510302', '自流井区', '3', '510300', '0813', 'zljq', null, '1');
INSERT INTO `sys_area` VALUES ('510303', '贡井区', '3', '510300', '0813', 'gjq', null, '1');
INSERT INTO `sys_area` VALUES ('510304', '大安区', '3', '510300', '0813', 'daq', null, '1');
INSERT INTO `sys_area` VALUES ('510311', '沿滩区', '3', '510300', '0813', 'ytq', null, '1');
INSERT INTO `sys_area` VALUES ('510321', '荣县', '3', '510300', '0813', 'rx', null, '1');
INSERT INTO `sys_area` VALUES ('510322', '富顺县', '3', '510300', '0813', 'fsx', null, '1');
INSERT INTO `sys_area` VALUES ('510400', '攀枝花市', '2', '510000', null, 'pzhs', null, '1');
INSERT INTO `sys_area` VALUES ('510401', '市辖区', '3', '510400', '0812', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('510402', '东区', '3', '510400', '0812', 'dq', null, '1');
INSERT INTO `sys_area` VALUES ('510403', '西区', '3', '510400', '0812', 'xq', null, '1');
INSERT INTO `sys_area` VALUES ('510411', '仁和区', '3', '510400', '0812', 'rhq', null, '1');
INSERT INTO `sys_area` VALUES ('510421', '米易县', '3', '510400', '0812', 'myx', null, '1');
INSERT INTO `sys_area` VALUES ('510422', '盐边县', '3', '510400', '0812', 'ybx', null, '1');
INSERT INTO `sys_area` VALUES ('510500', '泸州市', '2', '510000', null, 'lzs', null, '1');
INSERT INTO `sys_area` VALUES ('510501', '市辖区', '3', '510500', '0830', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('510502', '江阳区', '3', '510500', '0830', 'jyq', null, '1');
INSERT INTO `sys_area` VALUES ('510503', '纳溪区', '3', '510500', '0830', 'nxq', null, '1');
INSERT INTO `sys_area` VALUES ('510504', '龙马潭区', '3', '510500', '0830', 'lmtq', null, '1');
INSERT INTO `sys_area` VALUES ('510521', '泸县', '3', '510500', '0830', 'lx', null, '1');
INSERT INTO `sys_area` VALUES ('510522', '合江县', '3', '510500', '0830', 'hjx', null, '1');
INSERT INTO `sys_area` VALUES ('510524', '叙永县', '3', '510500', '0830', 'xyx', null, '1');
INSERT INTO `sys_area` VALUES ('510525', '古蔺县', '3', '510500', '0830', 'glx', null, '1');
INSERT INTO `sys_area` VALUES ('510600', '德阳市', '2', '510000', null, 'dys', null, '1');
INSERT INTO `sys_area` VALUES ('510601', '市辖区', '3', '510600', '0838', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('510603', '旌阳区', '3', '510600', '0838', 'jyq', null, '1');
INSERT INTO `sys_area` VALUES ('510623', '中江县', '3', '510600', '0838', 'zjx', null, '1');
INSERT INTO `sys_area` VALUES ('510626', '罗江县', '3', '510600', '0838', 'ljx', null, '1');
INSERT INTO `sys_area` VALUES ('510681', '广汉市', '3', '510600', '0838', 'ghs', null, '1');
INSERT INTO `sys_area` VALUES ('510682', '什邡市', '3', '510600', '0838', 'sfs', null, '1');
INSERT INTO `sys_area` VALUES ('510683', '绵竹市', '3', '510600', '0838', 'mzs', null, '1');
INSERT INTO `sys_area` VALUES ('510700', '绵阳市', '2', '510000', null, 'mys', null, '1');
INSERT INTO `sys_area` VALUES ('510701', '市辖区', '3', '510700', '0816', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('510703', '涪城区', '3', '510700', '0816', 'fcq', null, '1');
INSERT INTO `sys_area` VALUES ('510704', '游仙区', '3', '510700', '0816', 'yxq', null, '1');
INSERT INTO `sys_area` VALUES ('510722', '三台县', '3', '510700', '0816', 'stx', null, '1');
INSERT INTO `sys_area` VALUES ('510723', '盐亭县', '3', '510700', '0816', 'ytx', null, '1');
INSERT INTO `sys_area` VALUES ('510724', '安县', '3', '510700', '0816', 'ax', null, '1');
INSERT INTO `sys_area` VALUES ('510725', '梓潼县', '3', '510700', '0816', 'ztx', null, '1');
INSERT INTO `sys_area` VALUES ('510726', '北川羌族自治县', '3', '510700', '0816', 'bcqzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('510727', '平武县', '3', '510700', '0816', 'pwx', null, '1');
INSERT INTO `sys_area` VALUES ('510781', '江油市', '3', '510700', '0816', 'jys', null, '1');
INSERT INTO `sys_area` VALUES ('510800', '广元市', '2', '510000', null, 'gys', null, '1');
INSERT INTO `sys_area` VALUES ('510801', '市辖区', '3', '510800', '0839', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('510802', '利州区', '3', '510800', '0839', 'lzq', null, '1');
INSERT INTO `sys_area` VALUES ('510811', '元坝区', '3', '510800', '0839', 'ybq', null, '1');
INSERT INTO `sys_area` VALUES ('510812', '朝天区', '3', '510800', '0839', 'ctq', null, '1');
INSERT INTO `sys_area` VALUES ('510821', '旺苍县', '3', '510800', '0839', 'wcx', null, '1');
INSERT INTO `sys_area` VALUES ('510822', '青川县', '3', '510800', '0839', 'qcx', null, '1');
INSERT INTO `sys_area` VALUES ('510823', '剑阁县', '3', '510800', '0839', 'jgx', null, '1');
INSERT INTO `sys_area` VALUES ('510824', '苍溪县', '3', '510800', '0839', 'cxx', null, '1');
INSERT INTO `sys_area` VALUES ('510900', '遂宁市', '2', '510000', null, 'sns', null, '1');
INSERT INTO `sys_area` VALUES ('510901', '市辖区', '3', '510900', '0825', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('510903', '船山区', '3', '510900', '0825', 'csq', null, '1');
INSERT INTO `sys_area` VALUES ('510904', '安居区', '3', '510900', '0825', 'ajq', null, '1');
INSERT INTO `sys_area` VALUES ('510921', '蓬溪县', '3', '510900', '0825', 'pxx', null, '1');
INSERT INTO `sys_area` VALUES ('510922', '射洪县', '3', '510900', '0825', 'shx', null, '1');
INSERT INTO `sys_area` VALUES ('510923', '大英县', '3', '510900', '0825', 'dyx', null, '1');
INSERT INTO `sys_area` VALUES ('511000', '内江市', '2', '510000', null, 'njs', null, '1');
INSERT INTO `sys_area` VALUES ('511001', '市辖区', '3', '511000', '0839', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('511002', '市中区', '3', '511000', '0832', 'szq', null, '1');
INSERT INTO `sys_area` VALUES ('511011', '东兴区', '3', '511000', '0832', 'dxq', null, '1');
INSERT INTO `sys_area` VALUES ('511024', '威远县', '3', '511000', '0832', 'wyx', null, '1');
INSERT INTO `sys_area` VALUES ('511025', '资中县', '3', '511000', '0832', 'zzx', null, '1');
INSERT INTO `sys_area` VALUES ('511028', '隆昌县', '3', '511000', '0832', 'lcx', null, '1');
INSERT INTO `sys_area` VALUES ('511100', '乐山市', '2', '510000', null, 'lss', null, '1');
INSERT INTO `sys_area` VALUES ('511101', '市辖区', '3', '511100', '0833', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('511102', '市中区', '3', '511100', '0833', 'szq', null, '1');
INSERT INTO `sys_area` VALUES ('511111', '沙湾区', '3', '511100', '0833', 'swq', null, '1');
INSERT INTO `sys_area` VALUES ('511112', '五通桥区', '3', '511100', '0833', 'wtqq', null, '1');
INSERT INTO `sys_area` VALUES ('511113', '金口河区', '3', '511100', '0833', 'jkhq', null, '1');
INSERT INTO `sys_area` VALUES ('511123', '犍为县', '3', '511100', '0833', 'jwx', null, '1');
INSERT INTO `sys_area` VALUES ('511124', '井研县', '3', '511100', '0833', 'jyx', null, '1');
INSERT INTO `sys_area` VALUES ('511126', '夹江县', '3', '511100', '0833', 'jjx', null, '1');
INSERT INTO `sys_area` VALUES ('511129', '沐川县', '3', '511100', '0833', 'mcx', null, '1');
INSERT INTO `sys_area` VALUES ('511132', '峨边彝族自治县', '3', '511100', '0833', 'ebyzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('511133', '马边彝族自治县', '3', '511100', '0833', 'mbyzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('511181', '峨眉山市', '3', '511100', '0833', 'emss', null, '1');
INSERT INTO `sys_area` VALUES ('511300', '南充市', '2', '510000', null, 'ncs', null, '1');
INSERT INTO `sys_area` VALUES ('511301', '市辖区', '3', '511300', '0817', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('511302', '顺庆区', '3', '511300', '0817', 'sqq', null, '1');
INSERT INTO `sys_area` VALUES ('511303', '高坪区', '3', '511300', '0817', 'gpq', null, '1');
INSERT INTO `sys_area` VALUES ('511304', '嘉陵区', '3', '511300', '0817', 'jlq', null, '1');
INSERT INTO `sys_area` VALUES ('511321', '南部县', '3', '511300', '0817', 'nbx', null, '1');
INSERT INTO `sys_area` VALUES ('511322', '营山县', '3', '511300', '0817', 'ysx', null, '1');
INSERT INTO `sys_area` VALUES ('511323', '蓬安县', '3', '511300', '0817', 'pax', null, '1');
INSERT INTO `sys_area` VALUES ('511324', '仪陇县', '3', '511300', '0817', 'ylx', null, '1');
INSERT INTO `sys_area` VALUES ('511325', '西充县', '3', '511300', '0817', 'xcx', null, '1');
INSERT INTO `sys_area` VALUES ('511381', '阆中市', '3', '511300', '0817', 'lzs', null, '1');
INSERT INTO `sys_area` VALUES ('511400', '眉山市', '2', '510000', null, 'mss', null, '1');
INSERT INTO `sys_area` VALUES ('511401', '市辖区', '3', '511400', '0833', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('511402', '东坡区', '3', '511400', '0833', 'dpq', null, '1');
INSERT INTO `sys_area` VALUES ('511421', '仁寿县', '3', '511400', '0833', 'rsx', null, '1');
INSERT INTO `sys_area` VALUES ('511422', '彭山县', '3', '511400', '0833', 'psx', null, '1');
INSERT INTO `sys_area` VALUES ('511423', '洪雅县', '3', '511400', '0833', 'hyx', null, '1');
INSERT INTO `sys_area` VALUES ('511424', '丹棱县', '3', '511400', '0833', 'dlx', null, '1');
INSERT INTO `sys_area` VALUES ('511425', '青神县', '3', '511400', '0833', 'qsx', null, '1');
INSERT INTO `sys_area` VALUES ('511500', '宜宾市', '2', '510000', null, 'ybs', null, '1');
INSERT INTO `sys_area` VALUES ('511501', '市辖区', '3', '511500', '0831', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('511502', '翠屏区', '3', '511500', '0831', 'cpq', null, '1');
INSERT INTO `sys_area` VALUES ('511503', '南溪区', '3', '511500', null, 'nxq', null, '1');
INSERT INTO `sys_area` VALUES ('511521', '宜宾县', '3', '511500', '0831', 'ybx', null, '1');
INSERT INTO `sys_area` VALUES ('511523', '江安县', '3', '511500', '0831', 'jax', null, '1');
INSERT INTO `sys_area` VALUES ('511524', '长宁县', '3', '511500', '0831', 'znx', null, '1');
INSERT INTO `sys_area` VALUES ('511525', '高县', '3', '511500', '0831', 'gx', null, '1');
INSERT INTO `sys_area` VALUES ('511526', '珙县', '3', '511500', '0831', 'gx', null, '1');
INSERT INTO `sys_area` VALUES ('511527', '筠连县', '3', '511500', '0831', 'ylx', null, '1');
INSERT INTO `sys_area` VALUES ('511528', '兴文县', '3', '511500', '0831', 'xwx', null, '1');
INSERT INTO `sys_area` VALUES ('511529', '屏山县', '3', '511500', '0831', 'psx', null, '1');
INSERT INTO `sys_area` VALUES ('511600', '广安市', '2', '510000', null, 'gas', null, '1');
INSERT INTO `sys_area` VALUES ('511601', '市辖区', '3', '511600', '0826', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('511602', '广安区', '3', '511600', '0826', 'gaq', null, '1');
INSERT INTO `sys_area` VALUES ('511621', '岳池县', '3', '511600', '0826', 'ycx', null, '1');
INSERT INTO `sys_area` VALUES ('511622', '武胜县', '3', '511600', '0826', 'wsx', null, '1');
INSERT INTO `sys_area` VALUES ('511623', '邻水县', '3', '511600', '0826', 'lsx', null, '1');
INSERT INTO `sys_area` VALUES ('511681', '华蓥市', '3', '511600', '0826', 'hys', null, '1');
INSERT INTO `sys_area` VALUES ('511700', '达州市', '2', '510000', null, 'dzs', null, '1');
INSERT INTO `sys_area` VALUES ('511701', '市辖区', '3', '511700', '0818', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('511702', '通川区', '3', '511700', '0818', 'tcq', null, '1');
INSERT INTO `sys_area` VALUES ('511721', '达县', '3', '511700', '0818', 'dx', null, '1');
INSERT INTO `sys_area` VALUES ('511722', '宣汉县', '3', '511700', '0818', 'xhx', null, '1');
INSERT INTO `sys_area` VALUES ('511723', '开江县', '3', '511700', '0818', 'kjx', null, '1');
INSERT INTO `sys_area` VALUES ('511724', '大竹县', '3', '511700', '0818', 'dzx', null, '1');
INSERT INTO `sys_area` VALUES ('511725', '渠县', '3', '511700', '0818', 'qx', null, '1');
INSERT INTO `sys_area` VALUES ('511781', '万源市', '3', '511700', '0818', 'wys', null, '1');
INSERT INTO `sys_area` VALUES ('511800', '雅安市', '2', '510000', null, 'yas', null, '1');
INSERT INTO `sys_area` VALUES ('511801', '市辖区', '3', '511800', '0835', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('511802', '雨城区', '3', '511800', '0835', 'ycq', null, '1');
INSERT INTO `sys_area` VALUES ('511803', '名山区', '3', '511800', null, 'msq', null, '1');
INSERT INTO `sys_area` VALUES ('511822', '荥经县', '3', '511800', '0835', 'yjx', null, '1');
INSERT INTO `sys_area` VALUES ('511823', '汉源县', '3', '511800', '0835', 'hyx', null, '1');
INSERT INTO `sys_area` VALUES ('511824', '石棉县', '3', '511800', '0835', 'smx', null, '1');
INSERT INTO `sys_area` VALUES ('511825', '天全县', '3', '511800', '0835', 'tqx', null, '1');
INSERT INTO `sys_area` VALUES ('511826', '芦山县', '3', '511800', '0835', 'lsx', null, '1');
INSERT INTO `sys_area` VALUES ('511827', '宝兴县', '3', '511800', '0835', 'bxx', null, '1');
INSERT INTO `sys_area` VALUES ('511900', '巴中市', '2', '510000', null, 'bzs', null, '1');
INSERT INTO `sys_area` VALUES ('511901', '市辖区', '3', '511900', '0827', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('511902', '巴州区', '3', '511900', '0827', 'bzq', null, '1');
INSERT INTO `sys_area` VALUES ('511921', '通江县', '3', '511900', '0827', 'tjx', null, '1');
INSERT INTO `sys_area` VALUES ('511922', '南江县', '3', '511900', '0827', 'njx', null, '1');
INSERT INTO `sys_area` VALUES ('511923', '平昌县', '3', '511900', '0827', 'pcx', null, '1');
INSERT INTO `sys_area` VALUES ('512000', '资阳市', '2', '510000', null, 'zys', null, '1');
INSERT INTO `sys_area` VALUES ('512001', '市辖区', '3', '512000', '0832', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('512002', '雁江区', '3', '512000', '0832', 'yjq', null, '1');
INSERT INTO `sys_area` VALUES ('512021', '安岳县', '3', '512000', '0832', 'ayx', null, '1');
INSERT INTO `sys_area` VALUES ('512022', '乐至县', '3', '512000', '0832', 'lzx', null, '1');
INSERT INTO `sys_area` VALUES ('512081', '简阳市', '3', '512000', '0832', 'jys', null, '1');
INSERT INTO `sys_area` VALUES ('513200', '阿坝藏族羌族自治州', '2', '510000', null, 'abzzqzzzz', null, '1');
INSERT INTO `sys_area` VALUES ('513221', '汶川县', '3', '513200', '0837', 'wcx', null, '1');
INSERT INTO `sys_area` VALUES ('513222', '理县', '3', '513200', '0837', 'lx', null, '1');
INSERT INTO `sys_area` VALUES ('513223', '茂县', '3', '513200', '0837', 'mx', null, '1');
INSERT INTO `sys_area` VALUES ('513224', '松潘县', '3', '513200', '0837', 'spx', null, '1');
INSERT INTO `sys_area` VALUES ('513225', '九寨沟县', '3', '513200', '0837', 'jzgx', null, '1');
INSERT INTO `sys_area` VALUES ('513226', '金川县', '3', '513200', '0837', 'jcx', null, '1');
INSERT INTO `sys_area` VALUES ('513227', '小金县', '3', '513200', '0837', 'xjx', null, '1');
INSERT INTO `sys_area` VALUES ('513228', '黑水县', '3', '513200', '0837', 'hsx', null, '1');
INSERT INTO `sys_area` VALUES ('513229', '马尔康县', '3', '513200', '0837', 'mekx', null, '1');
INSERT INTO `sys_area` VALUES ('513230', '壤塘县', '3', '513200', '0837', 'rtx', null, '1');
INSERT INTO `sys_area` VALUES ('513231', '阿坝县', '3', '513200', '0837', 'abx', null, '1');
INSERT INTO `sys_area` VALUES ('513232', '若尔盖县', '3', '513200', '0837', 'regx', null, '1');
INSERT INTO `sys_area` VALUES ('513233', '红原县', '3', '513200', '0837', 'hyx', null, '1');
INSERT INTO `sys_area` VALUES ('513300', '甘孜藏族自治州', '2', '510000', null, 'gzzzzzz', null, '1');
INSERT INTO `sys_area` VALUES ('513321', '康定县', '3', '513300', '0836', 'kdx', null, '1');
INSERT INTO `sys_area` VALUES ('513322', '泸定县', '3', '513300', '0836', 'ldx', null, '1');
INSERT INTO `sys_area` VALUES ('513323', '丹巴县', '3', '513300', '0836', 'dbx', null, '1');
INSERT INTO `sys_area` VALUES ('513324', '九龙县', '3', '513300', '0836', 'jlx', null, '1');
INSERT INTO `sys_area` VALUES ('513325', '雅江县', '3', '513300', '0836', 'yjx', null, '1');
INSERT INTO `sys_area` VALUES ('513326', '道孚县', '3', '513300', '0836', 'dfx', null, '1');
INSERT INTO `sys_area` VALUES ('513327', '炉霍县', '3', '513300', '0836', 'lhx', null, '1');
INSERT INTO `sys_area` VALUES ('513328', '甘孜县', '3', '513300', '0836', 'gzx', null, '1');
INSERT INTO `sys_area` VALUES ('513329', '新龙县', '3', '513300', '0836', 'xlx', null, '1');
INSERT INTO `sys_area` VALUES ('513330', '德格县', '3', '513300', '0836', 'dgx', null, '1');
INSERT INTO `sys_area` VALUES ('513331', '白玉县', '3', '513300', '0836', 'byx', null, '1');
INSERT INTO `sys_area` VALUES ('513332', '石渠县', '3', '513300', '0836', 'sqx', null, '1');
INSERT INTO `sys_area` VALUES ('513333', '色达县', '3', '513300', '0836', 'sdx', null, '1');
INSERT INTO `sys_area` VALUES ('513334', '理塘县', '3', '513300', '0836', 'ltx', null, '1');
INSERT INTO `sys_area` VALUES ('513335', '巴塘县', '3', '513300', '0836', 'btx', null, '1');
INSERT INTO `sys_area` VALUES ('513336', '乡城县', '3', '513300', '0836', 'xcx', null, '1');
INSERT INTO `sys_area` VALUES ('513337', '稻城县', '3', '513300', '0836', 'dcx', null, '1');
INSERT INTO `sys_area` VALUES ('513338', '得荣县', '3', '513300', '0836', 'drx', null, '1');
INSERT INTO `sys_area` VALUES ('513400', '凉山彝族自治州', '2', '510000', null, 'lsyzzzz', null, '1');
INSERT INTO `sys_area` VALUES ('513401', '西昌市', '3', '513400', '0834', 'xcs', null, '1');
INSERT INTO `sys_area` VALUES ('513422', '木里藏族自治县', '3', '513400', '0834', 'mlzzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('513423', '盐源县', '3', '513400', '0834', 'yyx', null, '1');
INSERT INTO `sys_area` VALUES ('513424', '德昌县', '3', '513400', '0834', 'dcx', null, '1');
INSERT INTO `sys_area` VALUES ('513425', '会理县', '3', '513400', '0834', 'hlx', null, '1');
INSERT INTO `sys_area` VALUES ('513426', '会东县', '3', '513400', '0834', 'hdx', null, '1');
INSERT INTO `sys_area` VALUES ('513427', '宁南县', '3', '513400', '0834', 'nnx', null, '1');
INSERT INTO `sys_area` VALUES ('513428', '普格县', '3', '513400', '0834', 'pgx', null, '1');
INSERT INTO `sys_area` VALUES ('513429', '布拖县', '3', '513400', '0834', 'btx', null, '1');
INSERT INTO `sys_area` VALUES ('513430', '金阳县', '3', '513400', '0834', 'jyx', null, '1');
INSERT INTO `sys_area` VALUES ('513431', '昭觉县', '3', '513400', '0834', 'zjx', null, '1');
INSERT INTO `sys_area` VALUES ('513432', '喜德县', '3', '513400', '0834', 'xdx', null, '1');
INSERT INTO `sys_area` VALUES ('513433', '冕宁县', '3', '513400', '0834', 'mnx', null, '1');
INSERT INTO `sys_area` VALUES ('513434', '越西县', '3', '513400', '0834', 'yxx', null, '1');
INSERT INTO `sys_area` VALUES ('513435', '甘洛县', '3', '513400', '0834', 'glx', null, '1');
INSERT INTO `sys_area` VALUES ('513436', '美姑县', '3', '513400', '0834', 'mgx', null, '1');
INSERT INTO `sys_area` VALUES ('513437', '雷波县', '3', '513400', '0834', 'lbx', null, '1');
INSERT INTO `sys_area` VALUES ('520000', '贵州省', '1', '0', null, 'gzs', null, '1');
INSERT INTO `sys_area` VALUES ('520100', '贵阳市', '2', '520000', null, 'gys', null, '1');
INSERT INTO `sys_area` VALUES ('520101', '市辖区', '3', '520100', '0851', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('520102', '南明区', '3', '520100', '0851', 'nmq', null, '1');
INSERT INTO `sys_area` VALUES ('520103', '云岩区', '3', '520100', '0851', 'yyq', null, '1');
INSERT INTO `sys_area` VALUES ('520111', '花溪区', '3', '520100', '0851', 'hxq', null, '1');
INSERT INTO `sys_area` VALUES ('520112', '乌当区', '3', '520100', '0851', 'wdq', null, '1');
INSERT INTO `sys_area` VALUES ('520113', '白云区', '3', '520100', '0851', 'byq', null, '1');
INSERT INTO `sys_area` VALUES ('520114', '小河区', '3', '520100', '0851', 'xhq', null, '1');
INSERT INTO `sys_area` VALUES ('520121', '开阳县', '3', '520100', '0851', 'kyx', null, '1');
INSERT INTO `sys_area` VALUES ('520122', '息烽县', '3', '520100', '0851', 'xfx', null, '1');
INSERT INTO `sys_area` VALUES ('520123', '修文县', '3', '520100', '0851', 'xwx', null, '1');
INSERT INTO `sys_area` VALUES ('520181', '清镇市', '3', '520100', '0851', 'qzs', null, '1');
INSERT INTO `sys_area` VALUES ('520200', '六盘水市', '2', '520000', null, 'lpss', null, '1');
INSERT INTO `sys_area` VALUES ('520201', '钟山区', '3', '520200', '0858', 'zsq', null, '1');
INSERT INTO `sys_area` VALUES ('520203', '六枝特区', '3', '520200', '0858', 'lztq', null, '1');
INSERT INTO `sys_area` VALUES ('520221', '水城县', '3', '520200', '0858', 'scx', null, '1');
INSERT INTO `sys_area` VALUES ('520222', '盘县', '3', '520200', '0858', 'px', null, '1');
INSERT INTO `sys_area` VALUES ('520300', '遵义市', '2', '520000', null, 'zys', null, '1');
INSERT INTO `sys_area` VALUES ('520301', '市辖区', '3', '520300', '0852', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('520302', '红花岗区', '3', '520300', '0852', 'hhgq', null, '1');
INSERT INTO `sys_area` VALUES ('520303', '汇川区', '3', '520300', '0852', 'hcq', null, '1');
INSERT INTO `sys_area` VALUES ('520321', '遵义县', '3', '520300', '0852', 'zyx', null, '1');
INSERT INTO `sys_area` VALUES ('520322', '桐梓县', '3', '520300', '0852', 'tzx', null, '1');
INSERT INTO `sys_area` VALUES ('520323', '绥阳县', '3', '520300', '0852', 'syx', null, '1');
INSERT INTO `sys_area` VALUES ('520324', '正安县', '3', '520300', '0852', 'zax', null, '1');
INSERT INTO `sys_area` VALUES ('520325', '道真仡佬族苗族自治县', '3', '520300', '0852', 'dzglzmzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('520326', '务川仡佬族苗族自治县', '3', '520300', '0852', 'wcglzmzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('520327', '凤冈县', '3', '520300', '0852', 'fgx', null, '1');
INSERT INTO `sys_area` VALUES ('520328', '湄潭县', '3', '520300', '0852', 'mtx', null, '1');
INSERT INTO `sys_area` VALUES ('520329', '余庆县', '3', '520300', '0852', 'yqx', null, '1');
INSERT INTO `sys_area` VALUES ('520330', '习水县', '3', '520300', '0852', 'xsx', null, '1');
INSERT INTO `sys_area` VALUES ('520381', '赤水市', '3', '520300', '0852', 'css', null, '1');
INSERT INTO `sys_area` VALUES ('520382', '仁怀市', '3', '520300', '0852', 'rhs', null, '1');
INSERT INTO `sys_area` VALUES ('520400', '安顺市', '2', '520000', null, 'ass', null, '1');
INSERT INTO `sys_area` VALUES ('520401', '市辖区', '3', '520400', '0853', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('520402', '西秀区', '3', '520400', '0853', 'xxq', null, '1');
INSERT INTO `sys_area` VALUES ('520421', '平坝县', '3', '520400', '0853', 'pbx', null, '1');
INSERT INTO `sys_area` VALUES ('520422', '普定县', '3', '520400', '0853', 'pdx', null, '1');
INSERT INTO `sys_area` VALUES ('520423', '镇宁布依族苗族自治县', '3', '520400', '0853', 'znbyzmzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('520424', '关岭布依族苗族自治县', '3', '520400', '0853', 'glbyzmzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('520425', '紫云苗族布依族自治县', '3', '520400', '0853', 'zymzbyzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('520500', '毕节市', '2', '520000', null, 'bjs', null, '1');
INSERT INTO `sys_area` VALUES ('520502', '七星关区', '3', '520500', null, 'qxgq', null, '1');
INSERT INTO `sys_area` VALUES ('520521', '大方县', '3', '520500', null, 'dfx', null, '1');
INSERT INTO `sys_area` VALUES ('520522', '黔西县', '3', '520500', null, 'qxx', null, '1');
INSERT INTO `sys_area` VALUES ('520523', '金沙县', '3', '520500', null, 'jsx', null, '1');
INSERT INTO `sys_area` VALUES ('520524', '织金县', '3', '520500', null, 'zjx', null, '1');
INSERT INTO `sys_area` VALUES ('520525', '纳雍县', '3', '520500', null, 'nyx', null, '1');
INSERT INTO `sys_area` VALUES ('520526', '威宁彝族回族苗族自治县', '3', '520500', null, 'wnyzhzmzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('520527', '赫章县', '3', '520500', null, 'hzx', null, '1');
INSERT INTO `sys_area` VALUES ('520600', '铜仁市', '2', '520000', null, 'trs', null, '1');
INSERT INTO `sys_area` VALUES ('520602', '碧江区', '3', '520600', null, 'bjq', null, '1');
INSERT INTO `sys_area` VALUES ('520603', '万山区', '3', '520600', null, 'wsq', null, '1');
INSERT INTO `sys_area` VALUES ('520621', '江口县', '3', '520600', null, 'jkx', null, '1');
INSERT INTO `sys_area` VALUES ('520622', '玉屏侗族自治县', '3', '520600', null, 'ypdzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('520623', '石阡县', '3', '520600', null, 'sqx', null, '1');
INSERT INTO `sys_area` VALUES ('520624', '思南县', '3', '520600', null, 'snx', null, '1');
INSERT INTO `sys_area` VALUES ('520625', '印江土家族苗族自治县', '3', '520600', null, 'yjtjzmzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('520626', '德江县', '3', '520600', null, 'djx', null, '1');
INSERT INTO `sys_area` VALUES ('520627', '沿河土家族自治县', '3', '520600', null, 'yhtjzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('520628', '松桃苗族自治县', '3', '520600', null, 'stmzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('522300', '黔西南布依族苗族自治州', '2', '520000', null, 'qxnbyzmzzzz', null, '1');
INSERT INTO `sys_area` VALUES ('522301', '兴义市', '3', '522300', '0859', 'xys', null, '1');
INSERT INTO `sys_area` VALUES ('522322', '兴仁县', '3', '522300', '0859', 'xrx', null, '1');
INSERT INTO `sys_area` VALUES ('522323', '普安县', '3', '522300', '0859', 'pax', null, '1');
INSERT INTO `sys_area` VALUES ('522324', '晴隆县', '3', '522300', '0859', 'qlx', null, '1');
INSERT INTO `sys_area` VALUES ('522325', '贞丰县', '3', '522300', '0859', 'zfx', null, '1');
INSERT INTO `sys_area` VALUES ('522326', '望谟县', '3', '522300', '0859', 'wmx', null, '1');
INSERT INTO `sys_area` VALUES ('522327', '册亨县', '3', '522300', '0859', 'chx', null, '1');
INSERT INTO `sys_area` VALUES ('522328', '安龙县', '3', '522300', '0859', 'alx', null, '1');
INSERT INTO `sys_area` VALUES ('522600', '黔东南苗族侗族自治州', '2', '520000', null, 'qdnmzdzzzz', null, '1');
INSERT INTO `sys_area` VALUES ('522601', '凯里市', '3', '522600', '0855', 'kls', null, '1');
INSERT INTO `sys_area` VALUES ('522622', '黄平县', '3', '522600', '0855', 'hpx', null, '1');
INSERT INTO `sys_area` VALUES ('522623', '施秉县', '3', '522600', '0855', 'sbx', null, '1');
INSERT INTO `sys_area` VALUES ('522624', '三穗县', '3', '522600', '0855', 'ssx', null, '1');
INSERT INTO `sys_area` VALUES ('522625', '镇远县', '3', '522600', '0855', 'zyx', null, '1');
INSERT INTO `sys_area` VALUES ('522626', '岑巩县', '3', '522600', '0855', 'cgx', null, '1');
INSERT INTO `sys_area` VALUES ('522627', '天柱县', '3', '522600', '0855', 'tzx', null, '1');
INSERT INTO `sys_area` VALUES ('522628', '锦屏县', '3', '522600', '0855', 'jpx', null, '1');
INSERT INTO `sys_area` VALUES ('522629', '剑河县', '3', '522600', '0855', 'jhx', null, '1');
INSERT INTO `sys_area` VALUES ('522630', '台江县', '3', '522600', '0855', 'tjx', null, '1');
INSERT INTO `sys_area` VALUES ('522631', '黎平县', '3', '522600', '0855', 'lpx', null, '1');
INSERT INTO `sys_area` VALUES ('522632', '榕江县', '3', '522600', '0855', 'rjx', null, '1');
INSERT INTO `sys_area` VALUES ('522633', '从江县', '3', '522600', '0855', 'cjx', null, '1');
INSERT INTO `sys_area` VALUES ('522634', '雷山县', '3', '522600', '0855', 'lsx', null, '1');
INSERT INTO `sys_area` VALUES ('522635', '麻江县', '3', '522600', '0855', 'mjx', null, '1');
INSERT INTO `sys_area` VALUES ('522636', '丹寨县', '3', '522600', '0855', 'dzx', null, '1');
INSERT INTO `sys_area` VALUES ('522700', '黔南布依族苗族自治州', '2', '520000', null, 'qnbyzmzzzz', null, '1');
INSERT INTO `sys_area` VALUES ('522701', '都匀市', '3', '522700', '0854', 'dys', null, '1');
INSERT INTO `sys_area` VALUES ('522702', '福泉市', '3', '522700', '0854', 'fqs', null, '1');
INSERT INTO `sys_area` VALUES ('522722', '荔波县', '3', '522700', '0854', 'lbx', null, '1');
INSERT INTO `sys_area` VALUES ('522723', '贵定县', '3', '522700', '0854', 'gdx', null, '1');
INSERT INTO `sys_area` VALUES ('522725', '瓮安县', '3', '522700', '0854', 'wax', null, '1');
INSERT INTO `sys_area` VALUES ('522726', '独山县', '3', '522700', '0854', 'dsx', null, '1');
INSERT INTO `sys_area` VALUES ('522727', '平塘县', '3', '522700', '0854', 'ptx', null, '1');
INSERT INTO `sys_area` VALUES ('522728', '罗甸县', '3', '522700', '0854', 'ldx', null, '1');
INSERT INTO `sys_area` VALUES ('522729', '长顺县', '3', '522700', '0854', 'zsx', null, '1');
INSERT INTO `sys_area` VALUES ('522730', '龙里县', '3', '522700', '0854', 'llx', null, '1');
INSERT INTO `sys_area` VALUES ('522731', '惠水县', '3', '522700', '0854', 'hsx', null, '1');
INSERT INTO `sys_area` VALUES ('522732', '三都水族自治县', '3', '522700', '0854', 'sdszzzx', null, '1');
INSERT INTO `sys_area` VALUES ('530000', '云南省', '1', '0', null, 'yns', null, '1');
INSERT INTO `sys_area` VALUES ('530100', '昆明市', '2', '530000', null, 'kms', null, '1');
INSERT INTO `sys_area` VALUES ('530101', '市辖区', '3', '530100', '0871', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('530102', '五华区', '3', '530100', '0871', 'whq', null, '1');
INSERT INTO `sys_area` VALUES ('530103', '盘龙区', '3', '530100', '0871', 'plq', null, '1');
INSERT INTO `sys_area` VALUES ('530111', '官渡区', '3', '530100', '0871', 'gdq', null, '1');
INSERT INTO `sys_area` VALUES ('530112', '西山区', '3', '530100', '0871', 'xsq', null, '1');
INSERT INTO `sys_area` VALUES ('530113', '东川区', '3', '530100', '0871', 'dcq', null, '1');
INSERT INTO `sys_area` VALUES ('530114', '呈贡区', '3', '530100', null, 'cgq', null, '1');
INSERT INTO `sys_area` VALUES ('530122', '晋宁县', '3', '530100', '0871', 'jnx', null, '1');
INSERT INTO `sys_area` VALUES ('530124', '富民县', '3', '530100', '0871', 'fmx', null, '1');
INSERT INTO `sys_area` VALUES ('530125', '宜良县', '3', '530100', '0871', 'ylx', null, '1');
INSERT INTO `sys_area` VALUES ('530126', '石林彝族自治县', '3', '530100', '0871', 'slyzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('530127', '嵩明县', '3', '530100', '0871', 'smx', null, '1');
INSERT INTO `sys_area` VALUES ('530128', '禄劝彝族苗族自治县', '3', '530100', '0871', 'lqyzmzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('530129', '寻甸回族彝族自治县', '3', '530100', '0871', 'xdhzyzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('530181', '安宁市', '3', '530100', '0871', 'ans', null, '1');
INSERT INTO `sys_area` VALUES ('530300', '曲靖市', '2', '530000', null, 'qjs', null, '1');
INSERT INTO `sys_area` VALUES ('530301', '市辖区', '3', '530300', '0874', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('530302', '麒麟区', '3', '530300', '0874', 'qlq', null, '1');
INSERT INTO `sys_area` VALUES ('530321', '马龙县', '3', '530300', '0874', 'mlx', null, '1');
INSERT INTO `sys_area` VALUES ('530322', '陆良县', '3', '530300', '0874', 'llx', null, '1');
INSERT INTO `sys_area` VALUES ('530323', '师宗县', '3', '530300', '0874', 'szx', null, '1');
INSERT INTO `sys_area` VALUES ('530324', '罗平县', '3', '530300', '0874', 'lpx', null, '1');
INSERT INTO `sys_area` VALUES ('530325', '富源县', '3', '530300', '0874', 'fyx', null, '1');
INSERT INTO `sys_area` VALUES ('530326', '会泽县', '3', '530300', '0874', 'hzx', null, '1');
INSERT INTO `sys_area` VALUES ('530328', '沾益县', '3', '530300', '0874', 'zyx', null, '1');
INSERT INTO `sys_area` VALUES ('530381', '宣威市', '3', '530300', '0874', 'xws', null, '1');
INSERT INTO `sys_area` VALUES ('530400', '玉溪市', '2', '530000', null, 'yxs', null, '1');
INSERT INTO `sys_area` VALUES ('530402', '红塔区', '3', '530400', '0877', 'htq', null, '1');
INSERT INTO `sys_area` VALUES ('530421', '江川县', '3', '530400', '0877', 'jcx', null, '1');
INSERT INTO `sys_area` VALUES ('530422', '澄江县', '3', '530400', '0877', 'cjx', null, '1');
INSERT INTO `sys_area` VALUES ('530423', '通海县', '3', '530400', '0877', 'thx', null, '1');
INSERT INTO `sys_area` VALUES ('530424', '华宁县', '3', '530400', '0877', 'hnx', null, '1');
INSERT INTO `sys_area` VALUES ('530425', '易门县', '3', '530400', '0877', 'ymx', null, '1');
INSERT INTO `sys_area` VALUES ('530426', '峨山彝族自治县', '3', '530400', '0877', 'esyzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('530427', '新平彝族傣族自治县', '3', '530400', '0877', 'xpyzdzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('530428', '元江哈尼族彝族傣族自治县', '3', '530400', '0877', 'yjhnzyzdzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('530500', '保山市', '2', '530000', null, 'bss', null, '1');
INSERT INTO `sys_area` VALUES ('530501', '市辖区', '3', '530500', '0875', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('530502', '隆阳区', '3', '530500', '0875', 'lyq', null, '1');
INSERT INTO `sys_area` VALUES ('530521', '施甸县', '3', '530500', '0875', 'sdx', null, '1');
INSERT INTO `sys_area` VALUES ('530522', '腾冲县', '3', '530500', '0875', 'tcx', null, '1');
INSERT INTO `sys_area` VALUES ('530523', '龙陵县', '3', '530500', '0875', 'llx', null, '1');
INSERT INTO `sys_area` VALUES ('530524', '昌宁县', '3', '530500', '0875', 'cnx', null, '1');
INSERT INTO `sys_area` VALUES ('530600', '昭通市', '2', '530000', null, 'zts', null, '1');
INSERT INTO `sys_area` VALUES ('530601', '市辖区', '3', '530600', '0870', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('530602', '昭阳区', '3', '530600', '0870', 'zyq', null, '1');
INSERT INTO `sys_area` VALUES ('530621', '鲁甸县', '3', '530600', '0870', 'ldx', null, '1');
INSERT INTO `sys_area` VALUES ('530622', '巧家县', '3', '530600', '0870', 'qjx', null, '1');
INSERT INTO `sys_area` VALUES ('530623', '盐津县', '3', '530600', '0870', 'yjx', null, '1');
INSERT INTO `sys_area` VALUES ('530624', '大关县', '3', '530600', '0870', 'dgx', null, '1');
INSERT INTO `sys_area` VALUES ('530625', '永善县', '3', '530600', '0870', 'ysx', null, '1');
INSERT INTO `sys_area` VALUES ('530626', '绥江县', '3', '530600', '0870', 'sjx', null, '1');
INSERT INTO `sys_area` VALUES ('530627', '镇雄县', '3', '530600', '0870', 'zxx', null, '1');
INSERT INTO `sys_area` VALUES ('530628', '彝良县', '3', '530600', '0870', 'ylx', null, '1');
INSERT INTO `sys_area` VALUES ('530629', '威信县', '3', '530600', '0870', 'wxx', null, '1');
INSERT INTO `sys_area` VALUES ('530630', '水富县', '3', '530600', '0870', 'sfx', null, '1');
INSERT INTO `sys_area` VALUES ('530700', '丽江市', '2', '530000', null, 'ljs', null, '1');
INSERT INTO `sys_area` VALUES ('530701', '市辖区', '3', '530700', '0888', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('530702', '古城区', '3', '530700', '0888', 'gcq', null, '1');
INSERT INTO `sys_area` VALUES ('530721', '玉龙纳西族自治县', '3', '530700', '0888', 'ylnxzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('530722', '永胜县', '3', '530700', '0888', 'ysx', null, '1');
INSERT INTO `sys_area` VALUES ('530723', '华坪县', '3', '530700', '0888', 'hpx', null, '1');
INSERT INTO `sys_area` VALUES ('530724', '宁蒗彝族自治县', '3', '530700', '0888', 'nlyzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('530800', '普洱市', '2', '530000', null, 'pes', null, '1');
INSERT INTO `sys_area` VALUES ('530801', '市辖区', '3', '530800', '0879', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('530802', '思茅区', '3', '530800', '0879', 'smq', null, '1');
INSERT INTO `sys_area` VALUES ('530821', '宁洱哈尼族彝族自治县', '3', '530800', '0879', 'nehnzyzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('530822', '墨江哈尼族自治县', '3', '530800', '0879', 'mjhnzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('530823', '景东彝族自治县', '3', '530800', '0879', 'jdyzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('530824', '景谷傣族彝族自治县', '3', '530800', '0879', 'jgdzyzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('530825', '镇沅彝族哈尼族拉祜族自治县', '3', '530800', '0879', 'zyyzhnzlhzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('530826', '江城哈尼族彝族自治县', '3', '530800', '0879', 'jchnzyzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('530827', '孟连傣族拉祜族佤族自治县', '3', '530800', '0879', 'mldzlhzwzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('530828', '澜沧拉祜族自治县', '3', '530800', '0879', 'lclhzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('530829', '西盟佤族自治县', '3', '530800', '0879', 'xmwzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('530900', '临沧市', '2', '530000', null, 'lcs', null, '1');
INSERT INTO `sys_area` VALUES ('530901', '市辖区', '3', '530900', '0883', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('530902', '临翔区', '3', '530900', '0883', 'lxq', null, '1');
INSERT INTO `sys_area` VALUES ('530921', '凤庆县', '3', '530900', '0883', 'fqx', null, '1');
INSERT INTO `sys_area` VALUES ('530922', '云县', '3', '530900', '0883', 'yx', null, '1');
INSERT INTO `sys_area` VALUES ('530923', '永德县', '3', '530900', '0883', 'ydx', null, '1');
INSERT INTO `sys_area` VALUES ('530924', '镇康县', '3', '530900', '0883', 'zkx', null, '1');
INSERT INTO `sys_area` VALUES ('530925', '双江拉祜族佤族布朗族傣族自治县', '3', '530900', '0883', 'sjlhzwzblzdzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('530926', '耿马傣族佤族自治县', '3', '530900', '0883', 'gmdzwzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('530927', '沧源佤族自治县', '3', '530900', '0883', 'cywzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('532300', '楚雄彝族自治州', '2', '530000', null, 'cxyzzzz', null, '1');
INSERT INTO `sys_area` VALUES ('532301', '楚雄市', '3', '532300', '0878', 'cxs', null, '1');
INSERT INTO `sys_area` VALUES ('532322', '双柏县', '3', '532300', '0878', 'sbx', null, '1');
INSERT INTO `sys_area` VALUES ('532323', '牟定县', '3', '532300', '0878', 'mdx', null, '1');
INSERT INTO `sys_area` VALUES ('532324', '南华县', '3', '532300', '0878', 'nhx', null, '1');
INSERT INTO `sys_area` VALUES ('532325', '姚安县', '3', '532300', '0878', 'yax', null, '1');
INSERT INTO `sys_area` VALUES ('532326', '大姚县', '3', '532300', '0878', 'dyx', null, '1');
INSERT INTO `sys_area` VALUES ('532327', '永仁县', '3', '532300', '0878', 'yrx', null, '1');
INSERT INTO `sys_area` VALUES ('532328', '元谋县', '3', '532300', '0878', 'ymx', null, '1');
INSERT INTO `sys_area` VALUES ('532329', '武定县', '3', '532300', '0878', 'wdx', null, '1');
INSERT INTO `sys_area` VALUES ('532331', '禄丰县', '3', '532300', '0878', 'lfx', null, '1');
INSERT INTO `sys_area` VALUES ('532500', '红河哈尼族彝族自治州', '2', '530000', null, 'hhhnzyzzzz', null, '1');
INSERT INTO `sys_area` VALUES ('532501', '个旧市', '3', '532500', '0873', 'gjs', null, '1');
INSERT INTO `sys_area` VALUES ('532502', '开远市', '3', '532500', '0873', 'kys', null, '1');
INSERT INTO `sys_area` VALUES ('532503', '蒙自市', '3', '532500', null, 'mzs', null, '1');
INSERT INTO `sys_area` VALUES ('532523', '屏边苗族自治县', '3', '532500', '0873', 'pbmzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('532524', '建水县', '3', '532500', '0873', 'jsx', null, '1');
INSERT INTO `sys_area` VALUES ('532525', '石屏县', '3', '532500', '0873', 'spx', null, '1');
INSERT INTO `sys_area` VALUES ('532526', '弥勒县', '3', '532500', '0873', 'mlx', null, '1');
INSERT INTO `sys_area` VALUES ('532527', '泸西县', '3', '532500', '0873', 'lxx', null, '1');
INSERT INTO `sys_area` VALUES ('532528', '元阳县', '3', '532500', '0873', 'yyx', null, '1');
INSERT INTO `sys_area` VALUES ('532529', '红河县', '3', '532500', '0873', 'hhx', null, '1');
INSERT INTO `sys_area` VALUES ('532530', '金平苗族瑶族傣族自治县', '3', '532500', '0873', 'jpmzyzdzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('532531', '绿春县', '3', '532500', '0873', 'lcx', null, '1');
INSERT INTO `sys_area` VALUES ('532532', '河口瑶族自治县', '3', '532500', '0873', 'hkyzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('532600', '文山壮族苗族自治州', '2', '530000', null, 'wszzmzzzz', null, '1');
INSERT INTO `sys_area` VALUES ('532601', '文山市', '3', '532600', null, 'wss', null, '1');
INSERT INTO `sys_area` VALUES ('532622', '砚山县', '3', '532600', '0876', 'ysx', null, '1');
INSERT INTO `sys_area` VALUES ('532623', '西畴县', '3', '532600', '0876', 'xcx', null, '1');
INSERT INTO `sys_area` VALUES ('532624', '麻栗坡县', '3', '532600', '0876', 'mlpx', null, '1');
INSERT INTO `sys_area` VALUES ('532625', '马关县', '3', '532600', '0876', 'mgx', null, '1');
INSERT INTO `sys_area` VALUES ('532626', '丘北县', '3', '532600', '0876', 'qbx', null, '1');
INSERT INTO `sys_area` VALUES ('532627', '广南县', '3', '532600', '0876', 'gnx', null, '1');
INSERT INTO `sys_area` VALUES ('532628', '富宁县', '3', '532600', '0876', 'fnx', null, '1');
INSERT INTO `sys_area` VALUES ('532800', '西双版纳傣族自治州', '2', '530000', null, 'xsbndzzzz', null, '1');
INSERT INTO `sys_area` VALUES ('532801', '景洪市', '3', '532800', '0691', 'jhs', null, '1');
INSERT INTO `sys_area` VALUES ('532822', '勐海县', '3', '532800', '0691', 'mhx', null, '1');
INSERT INTO `sys_area` VALUES ('532823', '勐腊县', '3', '532800', '0691', 'mlx', null, '1');
INSERT INTO `sys_area` VALUES ('532900', '大理白族自治州', '2', '530000', null, 'dlbzzzz', null, '1');
INSERT INTO `sys_area` VALUES ('532901', '大理市', '3', '532900', '0872', 'dls', null, '1');
INSERT INTO `sys_area` VALUES ('532922', '漾濞彝族自治县', '3', '532900', '0872', 'ybyzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('532923', '祥云县', '3', '532900', '0872', 'xyx', null, '1');
INSERT INTO `sys_area` VALUES ('532924', '宾川县', '3', '532900', '0872', 'bcx', null, '1');
INSERT INTO `sys_area` VALUES ('532925', '弥渡县', '3', '532900', '0872', 'mdx', null, '1');
INSERT INTO `sys_area` VALUES ('532926', '南涧彝族自治县', '3', '532900', '0872', 'njyzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('532927', '巍山彝族回族自治县', '3', '532900', '0872', 'wsyzhzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('532928', '永平县', '3', '532900', '0872', 'ypx', null, '1');
INSERT INTO `sys_area` VALUES ('532929', '云龙县', '3', '532900', '0872', 'ylx', null, '1');
INSERT INTO `sys_area` VALUES ('532930', '洱源县', '3', '532900', '0872', 'eyx', null, '1');
INSERT INTO `sys_area` VALUES ('532931', '剑川县', '3', '532900', '0872', 'jcx', null, '1');
INSERT INTO `sys_area` VALUES ('532932', '鹤庆县', '3', '532900', '0872', 'hqx', null, '1');
INSERT INTO `sys_area` VALUES ('533100', '德宏傣族景颇族自治州', '2', '530000', null, 'dhdzjpzzzz', null, '1');
INSERT INTO `sys_area` VALUES ('533102', '瑞丽市', '3', '533100', '0692', 'rls', null, '1');
INSERT INTO `sys_area` VALUES ('533103', '芒市', '3', '533100', '0692', 'ms', null, '1');
INSERT INTO `sys_area` VALUES ('533122', '梁河县', '3', '533100', '0692', 'lhx', null, '1');
INSERT INTO `sys_area` VALUES ('533123', '盈江县', '3', '533100', '0692', 'yjx', null, '1');
INSERT INTO `sys_area` VALUES ('533124', '陇川县', '3', '533100', '0692', 'lcx', null, '1');
INSERT INTO `sys_area` VALUES ('533300', '怒江傈僳族自治州', '2', '530000', null, 'njlszzzz', null, '1');
INSERT INTO `sys_area` VALUES ('533321', '泸水县', '3', '533300', '0886', 'lsx', null, '1');
INSERT INTO `sys_area` VALUES ('533323', '福贡县', '3', '533300', '0886', 'fgx', null, '1');
INSERT INTO `sys_area` VALUES ('533324', '贡山独龙族怒族自治县', '3', '533300', '0886', 'gsdlznzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('533325', '兰坪白族普米族自治县', '3', '533300', '0886', 'lpbzpmzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('533400', '迪庆藏族自治州', '2', '530000', null, 'dqzzzzz', null, '1');
INSERT INTO `sys_area` VALUES ('533421', '香格里拉县', '3', '533400', '0887', 'xgllx', null, '1');
INSERT INTO `sys_area` VALUES ('533422', '德钦县', '3', '533400', '0887', 'dqx', null, '1');
INSERT INTO `sys_area` VALUES ('533423', '维西傈僳族自治县', '3', '533400', '0887', 'wxlszzzx', null, '1');
INSERT INTO `sys_area` VALUES ('540000', '西藏自治区', '1', '0', null, 'xzzzq', null, '1');
INSERT INTO `sys_area` VALUES ('540100', '拉萨市', '2', '540000', null, 'lss', null, '1');
INSERT INTO `sys_area` VALUES ('540101', '市辖区', '3', '540100', '0891', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('540102', '城关区', '3', '540100', '0891', 'cgq', null, '1');
INSERT INTO `sys_area` VALUES ('540121', '林周县', '3', '540100', '0891', 'lzx', null, '1');
INSERT INTO `sys_area` VALUES ('540122', '当雄县', '3', '540100', '0891', 'dxx', null, '1');
INSERT INTO `sys_area` VALUES ('540123', '尼木县', '3', '540100', '0891', 'nmx', null, '1');
INSERT INTO `sys_area` VALUES ('540124', '曲水县', '3', '540100', '0891', 'qsx', null, '1');
INSERT INTO `sys_area` VALUES ('540125', '堆龙德庆县', '3', '540100', '0891', 'dldqx', null, '1');
INSERT INTO `sys_area` VALUES ('540126', '达孜县', '3', '540100', '0891', 'dzx', null, '1');
INSERT INTO `sys_area` VALUES ('540127', '墨竹工卡县', '3', '540100', '0891', 'mzgkx', null, '1');
INSERT INTO `sys_area` VALUES ('542100', '昌都地区', '2', '540000', null, 'cddq', null, '1');
INSERT INTO `sys_area` VALUES ('542121', '昌都县', '3', '542100', '0895', 'cdx', null, '1');
INSERT INTO `sys_area` VALUES ('542122', '江达县', '3', '542100', '0895', 'jdx', null, '1');
INSERT INTO `sys_area` VALUES ('542123', '贡觉县', '3', '542100', '0805', 'gjx', null, '1');
INSERT INTO `sys_area` VALUES ('542124', '类乌齐县', '3', '542100', '0850', 'lwqx', null, '1');
INSERT INTO `sys_area` VALUES ('542125', '丁青县', '3', '542100', '0805', 'dqx', null, '1');
INSERT INTO `sys_area` VALUES ('542126', '察雅县', '3', '542100', '0895', 'cyx', null, '1');
INSERT INTO `sys_area` VALUES ('542127', '八宿县', '3', '542100', '0805', 'bsx', null, '1');
INSERT INTO `sys_area` VALUES ('542128', '左贡县', '3', '542100', '0805', 'zgx', null, '1');
INSERT INTO `sys_area` VALUES ('542129', '芒康县', '3', '542100', '0805', 'mkx', null, '1');
INSERT INTO `sys_area` VALUES ('542132', '洛隆县', '3', '542100', '0805', 'llx', null, '1');
INSERT INTO `sys_area` VALUES ('542133', '边坝县', '3', '542100', '0805', 'bbx', null, '1');
INSERT INTO `sys_area` VALUES ('542200', '山南地区', '2', '540000', null, 'sndq', null, '1');
INSERT INTO `sys_area` VALUES ('542221', '乃东县', '3', '542200', '0893', 'ndx', null, '1');
INSERT INTO `sys_area` VALUES ('542222', '扎囊县', '3', '542200', '0804', 'znx', null, '1');
INSERT INTO `sys_area` VALUES ('542223', '贡嘎县', '3', '542200', '0893', 'ggx', null, '1');
INSERT INTO `sys_area` VALUES ('542224', '桑日县', '3', '542200', '0893', 'srx', null, '1');
INSERT INTO `sys_area` VALUES ('542225', '琼结县', '3', '542200', '0803', 'qjx', null, '1');
INSERT INTO `sys_area` VALUES ('542226', '曲松县', '3', '542200', '0803', 'qsx', null, '1');
INSERT INTO `sys_area` VALUES ('542227', '措美县', '3', '542200', '0807', 'cmx', null, '1');
INSERT INTO `sys_area` VALUES ('542228', '洛扎县', '3', '542200', '0804', 'lzx', null, '1');
INSERT INTO `sys_area` VALUES ('542229', '加查县', '3', '542200', '0803', 'jcx', null, '1');
INSERT INTO `sys_area` VALUES ('542231', '隆子县', '3', '542200', '0803', 'lzx', null, '1');
INSERT INTO `sys_area` VALUES ('542232', '错那县', '3', '542200', '0803', 'cnx', null, '1');
INSERT INTO `sys_area` VALUES ('542233', '浪卡子县', '3', '542200', '0804', 'lkzx', null, '1');
INSERT INTO `sys_area` VALUES ('542300', '日喀则地区', '2', '540000', null, 'rkzdq', null, '1');
INSERT INTO `sys_area` VALUES ('542301', '日喀则市', '3', '542300', '0892', 'rkzs', null, '1');
INSERT INTO `sys_area` VALUES ('542322', '南木林县', '3', '542300', '0803', 'nmlx', null, '1');
INSERT INTO `sys_area` VALUES ('542323', '江孜县', '3', '542300', '0892', 'jzx', null, '1');
INSERT INTO `sys_area` VALUES ('542324', '定日县', '3', '542300', '0802', 'drx', null, '1');
INSERT INTO `sys_area` VALUES ('542325', '萨迦县', '3', '542300', '0802', 'sjx', null, '1');
INSERT INTO `sys_area` VALUES ('542326', '拉孜县', '3', '542300', '0803', 'lzx', null, '1');
INSERT INTO `sys_area` VALUES ('542327', '昂仁县', '3', '542300', '0803', 'arx', null, '1');
INSERT INTO `sys_area` VALUES ('542328', '谢通门县', '3', '542300', '0803', 'xtmx', null, '1');
INSERT INTO `sys_area` VALUES ('542329', '白朗县', '3', '542300', '0892', 'blx', null, '1');
INSERT INTO `sys_area` VALUES ('542330', '仁布县', '3', '542300', '0892', 'rbx', null, '1');
INSERT INTO `sys_area` VALUES ('542331', '康马县', '3', '542300', '0802', 'kmx', null, '1');
INSERT INTO `sys_area` VALUES ('542332', '定结县', '3', '542300', '0802', 'djx', null, '1');
INSERT INTO `sys_area` VALUES ('542333', '仲巴县', '3', '542300', '0802', 'zbx', null, '1');
INSERT INTO `sys_area` VALUES ('542334', '亚东县', '3', '542300', '0802', 'ydx', null, '1');
INSERT INTO `sys_area` VALUES ('542335', '吉隆县', '3', '542300', '0802', 'jlx', null, '1');
INSERT INTO `sys_area` VALUES ('542336', '聂拉木县', '3', '542300', '0802', 'nlmx', null, '1');
INSERT INTO `sys_area` VALUES ('542337', '萨嘎县', '3', '542300', '0802', 'sgx', null, '1');
INSERT INTO `sys_area` VALUES ('542338', '岗巴县', '3', '542300', '0802', 'gbx', null, '1');
INSERT INTO `sys_area` VALUES ('542400', '那曲地区', '2', '540000', null, 'nqdq', null, '1');
INSERT INTO `sys_area` VALUES ('542421', '那曲县', '3', '542400', '0896', 'nqx', null, '1');
INSERT INTO `sys_area` VALUES ('542422', '嘉黎县', '3', '542400', '0806', 'jlx', null, '1');
INSERT INTO `sys_area` VALUES ('542423', '比如县', '3', '542400', '0806', 'brx', null, '1');
INSERT INTO `sys_area` VALUES ('542424', '聂荣县', '3', '542400', '0806', 'nrx', null, '1');
INSERT INTO `sys_area` VALUES ('542425', '安多县', '3', '542400', '0896', 'adx', null, '1');
INSERT INTO `sys_area` VALUES ('542426', '申扎县', '3', '542400', '0806', 'szx', null, '1');
INSERT INTO `sys_area` VALUES ('542427', '索县', '3', '542400', '0807', 'sx', null, '1');
INSERT INTO `sys_area` VALUES ('542428', '班戈县', '3', '542400', '0806', 'bgx', null, '1');
INSERT INTO `sys_area` VALUES ('542429', '巴青县', '3', '542400', '0806', 'bqx', null, '1');
INSERT INTO `sys_area` VALUES ('542430', '尼玛县', '3', '542400', '0808', 'nmx', null, '1');
INSERT INTO `sys_area` VALUES ('542500', '阿里地区', '2', '540000', null, 'aldq', null, '1');
INSERT INTO `sys_area` VALUES ('542521', '普兰县', '3', '542500', '0806', 'plx', null, '1');
INSERT INTO `sys_area` VALUES ('542522', '札达县', '3', '542500', '0807', 'zdx', null, '1');
INSERT INTO `sys_area` VALUES ('542523', '噶尔县', '3', '542500', '0897', 'gex', null, '1');
INSERT INTO `sys_area` VALUES ('542524', '日土县', '3', '542500', '0807', 'rtx', null, '1');
INSERT INTO `sys_area` VALUES ('542525', '革吉县', '3', '542500', '0807', 'gjx', null, '1');
INSERT INTO `sys_area` VALUES ('542526', '改则县', '3', '542500', '0807', 'gzx', null, '1');
INSERT INTO `sys_area` VALUES ('542527', '措勤县', '3', '542500', '0806', 'cqx', null, '1');
INSERT INTO `sys_area` VALUES ('542600', '林芝地区', '2', '540000', null, 'lzdq', null, '1');
INSERT INTO `sys_area` VALUES ('542621', '林芝县', '3', '542600', '0894', 'lzx', null, '1');
INSERT INTO `sys_area` VALUES ('542622', '工布江达县', '3', '542600', '0894', 'gbjdx', null, '1');
INSERT INTO `sys_area` VALUES ('542623', '米林县', '3', '542600', '0894', 'mlx', null, '1');
INSERT INTO `sys_area` VALUES ('542624', '墨脱县', '3', '542600', '0894', 'mtx', null, '1');
INSERT INTO `sys_area` VALUES ('542625', '波密县', '3', '542600', '0894', 'bmx', null, '1');
INSERT INTO `sys_area` VALUES ('542626', '察隅县', '3', '542600', '0894', 'cyx', null, '1');
INSERT INTO `sys_area` VALUES ('542627', '朗县', '3', '542600', '0894', 'lx', null, '1');
INSERT INTO `sys_area` VALUES ('610000', '陕西省', '1', '0', null, 'sxs', null, '1');
INSERT INTO `sys_area` VALUES ('610100', '西安市', '2', '610000', null, 'xas', null, '1');
INSERT INTO `sys_area` VALUES ('610101', '市辖区', '3', '610100', '029', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('610102', '新城区', '3', '610100', '029', 'xcq', null, '1');
INSERT INTO `sys_area` VALUES ('610103', '碑林区', '3', '610100', '029', 'blq', null, '1');
INSERT INTO `sys_area` VALUES ('610104', '莲湖区', '3', '610100', '029', 'lhq', null, '1');
INSERT INTO `sys_area` VALUES ('610111', '灞桥区', '3', '610100', '029', 'bqq', null, '1');
INSERT INTO `sys_area` VALUES ('610112', '未央区', '3', '610100', '029', 'wyq', null, '1');
INSERT INTO `sys_area` VALUES ('610113', '雁塔区', '3', '610100', '029', 'ytq', null, '1');
INSERT INTO `sys_area` VALUES ('610114', '阎良区', '3', '610100', '029', 'ylq', null, '1');
INSERT INTO `sys_area` VALUES ('610115', '临潼区', '3', '610100', '029', 'ltq', null, '1');
INSERT INTO `sys_area` VALUES ('610116', '长安区', '3', '610100', '029', 'zaq', null, '1');
INSERT INTO `sys_area` VALUES ('610122', '蓝田县', '3', '610100', '029', 'ltx', null, '1');
INSERT INTO `sys_area` VALUES ('610124', '周至县', '3', '610100', '029', 'zzx', null, '1');
INSERT INTO `sys_area` VALUES ('610125', '户县', '3', '610100', '029', 'hx', null, '1');
INSERT INTO `sys_area` VALUES ('610126', '高陵县', '3', '610100', '029', 'glx', null, '1');
INSERT INTO `sys_area` VALUES ('610200', '铜川市', '2', '610000', null, 'tcs', null, '1');
INSERT INTO `sys_area` VALUES ('610201', '市辖区', '3', '610200', '0919', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('610202', '王益区', '3', '610200', '0919', 'wyq', null, '1');
INSERT INTO `sys_area` VALUES ('610203', '印台区', '3', '610200', '0919', 'ytq', null, '1');
INSERT INTO `sys_area` VALUES ('610204', '耀州区', '3', '610200', '0919', 'yzq', null, '1');
INSERT INTO `sys_area` VALUES ('610222', '宜君县', '3', '610200', '0919', 'yjx', null, '1');
INSERT INTO `sys_area` VALUES ('610300', '宝鸡市', '2', '610000', null, 'bjs', null, '1');
INSERT INTO `sys_area` VALUES ('610301', '市辖区', '3', '610300', '0917', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('610302', '渭滨区', '3', '610300', '0917', 'wbq', null, '1');
INSERT INTO `sys_area` VALUES ('610303', '金台区', '3', '610300', '0917', 'jtq', null, '1');
INSERT INTO `sys_area` VALUES ('610304', '陈仓区', '3', '610300', '0917', 'ccq', null, '1');
INSERT INTO `sys_area` VALUES ('610322', '凤翔县', '3', '610300', '0917', 'fxx', null, '1');
INSERT INTO `sys_area` VALUES ('610323', '岐山县', '3', '610300', '0917', 'qsx', null, '1');
INSERT INTO `sys_area` VALUES ('610324', '扶风县', '3', '610300', '0917', 'ffx', null, '1');
INSERT INTO `sys_area` VALUES ('610326', '眉县', '3', '610300', '0917', 'mx', null, '1');
INSERT INTO `sys_area` VALUES ('610327', '陇县', '3', '610300', '0917', 'lx', null, '1');
INSERT INTO `sys_area` VALUES ('610328', '千阳县', '3', '610300', '0917', 'qyx', null, '1');
INSERT INTO `sys_area` VALUES ('610329', '麟游县', '3', '610300', '0917', 'lyx', null, '1');
INSERT INTO `sys_area` VALUES ('610330', '凤县', '3', '610300', '0917', 'fx', null, '1');
INSERT INTO `sys_area` VALUES ('610331', '太白县', '3', '610300', '0917', 'tbx', null, '1');
INSERT INTO `sys_area` VALUES ('610400', '咸阳市', '2', '610000', null, 'xys', null, '1');
INSERT INTO `sys_area` VALUES ('610401', '市辖区', '3', '610400', '0910', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('610402', '秦都区', '3', '610400', '0910', 'qdq', null, '1');
INSERT INTO `sys_area` VALUES ('610403', '杨陵区', '3', '610400', '0910', 'ylq', null, '1');
INSERT INTO `sys_area` VALUES ('610404', '渭城区', '3', '610400', '0910', 'wcq', null, '1');
INSERT INTO `sys_area` VALUES ('610422', '三原县', '3', '610400', '0910', 'syx', null, '1');
INSERT INTO `sys_area` VALUES ('610423', '泾阳县', '3', '610400', '0910', 'jyx', null, '1');
INSERT INTO `sys_area` VALUES ('610424', '乾县', '3', '610400', '0910', 'qx', null, '1');
INSERT INTO `sys_area` VALUES ('610425', '礼泉县', '3', '610400', '0910', 'lqx', null, '1');
INSERT INTO `sys_area` VALUES ('610426', '永寿县', '3', '610400', '0910', 'ysx', null, '1');
INSERT INTO `sys_area` VALUES ('610427', '彬县', '3', '610400', '0910', 'bx', null, '1');
INSERT INTO `sys_area` VALUES ('610428', '长武县', '3', '610400', '0910', 'zwx', null, '1');
INSERT INTO `sys_area` VALUES ('610429', '旬邑县', '3', '610400', '0910', 'xyx', null, '1');
INSERT INTO `sys_area` VALUES ('610430', '淳化县', '3', '610400', '0910', 'chx', null, '1');
INSERT INTO `sys_area` VALUES ('610431', '武功县', '3', '610400', '0910', 'wgx', null, '1');
INSERT INTO `sys_area` VALUES ('610481', '兴平市', '3', '610400', '0910', 'xps', null, '1');
INSERT INTO `sys_area` VALUES ('610500', '渭南市', '2', '610000', null, 'wns', null, '1');
INSERT INTO `sys_area` VALUES ('610501', '市辖区', '3', '610500', '0913', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('610502', '临渭区', '3', '610500', '0913', 'lwq', null, '1');
INSERT INTO `sys_area` VALUES ('610521', '华县', '3', '610500', '0913', 'hx', null, '1');
INSERT INTO `sys_area` VALUES ('610522', '潼关县', '3', '610500', '0913', 'tgx', null, '1');
INSERT INTO `sys_area` VALUES ('610523', '大荔县', '3', '610500', '0913', 'dlx', null, '1');
INSERT INTO `sys_area` VALUES ('610524', '合阳县', '3', '610500', '0913', 'hyx', null, '1');
INSERT INTO `sys_area` VALUES ('610525', '澄城县', '3', '610500', '0913', 'ccx', null, '1');
INSERT INTO `sys_area` VALUES ('610526', '蒲城县', '3', '610500', '0913', 'pcx', null, '1');
INSERT INTO `sys_area` VALUES ('610527', '白水县', '3', '610500', '0913', 'bsx', null, '1');
INSERT INTO `sys_area` VALUES ('610528', '富平县', '3', '610500', '0913', 'fpx', null, '1');
INSERT INTO `sys_area` VALUES ('610581', '韩城市', '3', '610500', '0913', 'hcs', null, '1');
INSERT INTO `sys_area` VALUES ('610582', '华阴市', '3', '610500', '0913', 'hys', null, '1');
INSERT INTO `sys_area` VALUES ('610600', '延安市', '2', '610000', null, 'yas', null, '1');
INSERT INTO `sys_area` VALUES ('610601', '市辖区', '3', '610600', '0911', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('610602', '宝塔区', '3', '610600', '0911', 'btq', null, '1');
INSERT INTO `sys_area` VALUES ('610621', '延长县', '3', '610600', '0911', 'yzx', null, '1');
INSERT INTO `sys_area` VALUES ('610622', '延川县', '3', '610600', '0911', 'ycx', null, '1');
INSERT INTO `sys_area` VALUES ('610623', '子长县', '3', '610600', '0911', 'zzx', null, '1');
INSERT INTO `sys_area` VALUES ('610624', '安塞县', '3', '610600', '0911', 'asx', null, '1');
INSERT INTO `sys_area` VALUES ('610625', '志丹县', '3', '610600', '0911', 'zdx', null, '1');
INSERT INTO `sys_area` VALUES ('610626', '吴起县', '3', '610600', '0911', 'wqx', null, '1');
INSERT INTO `sys_area` VALUES ('610627', '甘泉县', '3', '610600', '0911', 'gqx', null, '1');
INSERT INTO `sys_area` VALUES ('610628', '富县', '3', '610600', '0911', 'fx', null, '1');
INSERT INTO `sys_area` VALUES ('610629', '洛川县', '3', '610600', '0911', 'lcx', null, '1');
INSERT INTO `sys_area` VALUES ('610630', '宜川县', '3', '610600', '0911', 'ycx', null, '1');
INSERT INTO `sys_area` VALUES ('610631', '黄龙县', '3', '610600', '0911', 'hlx', null, '1');
INSERT INTO `sys_area` VALUES ('610632', '黄陵县', '3', '610600', '0911', 'hlx', null, '1');
INSERT INTO `sys_area` VALUES ('610700', '汉中市', '2', '610000', null, 'hzs', null, '1');
INSERT INTO `sys_area` VALUES ('610701', '市辖区', '3', '610700', '0916', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('610702', '汉台区', '3', '610700', '0916', 'htq', null, '1');
INSERT INTO `sys_area` VALUES ('610721', '南郑县', '3', '610700', '0916', 'nzx', null, '1');
INSERT INTO `sys_area` VALUES ('610722', '城固县', '3', '610700', '0916', 'cgx', null, '1');
INSERT INTO `sys_area` VALUES ('610723', '洋县', '3', '610700', '0916', 'yx', null, '1');
INSERT INTO `sys_area` VALUES ('610724', '西乡县', '3', '610700', '0916', 'xxx', null, '1');
INSERT INTO `sys_area` VALUES ('610725', '勉县', '3', '610700', '0916', 'mx', null, '1');
INSERT INTO `sys_area` VALUES ('610726', '宁强县', '3', '610700', '0916', 'nqx', null, '1');
INSERT INTO `sys_area` VALUES ('610727', '略阳县', '3', '610700', '0916', 'lyx', null, '1');
INSERT INTO `sys_area` VALUES ('610728', '镇巴县', '3', '610700', '0916', 'zbx', null, '1');
INSERT INTO `sys_area` VALUES ('610729', '留坝县', '3', '610700', '0916', 'lbx', null, '1');
INSERT INTO `sys_area` VALUES ('610730', '佛坪县', '3', '610700', '0916', 'fpx', null, '1');
INSERT INTO `sys_area` VALUES ('610800', '榆林市', '2', '610000', null, 'yls', null, '1');
INSERT INTO `sys_area` VALUES ('610801', '市辖区', '3', '610800', '0912', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('610802', '榆阳区', '3', '610800', '0912', 'yyq', null, '1');
INSERT INTO `sys_area` VALUES ('610821', '神木县', '3', '610800', '0912', 'smx', null, '1');
INSERT INTO `sys_area` VALUES ('610822', '府谷县', '3', '610800', '0912', 'fgx', null, '1');
INSERT INTO `sys_area` VALUES ('610823', '横山县', '3', '610800', '0912', 'hsx', null, '1');
INSERT INTO `sys_area` VALUES ('610824', '靖边县', '3', '610800', '0912', 'jbx', null, '1');
INSERT INTO `sys_area` VALUES ('610825', '定边县', '3', '610800', '0912', 'dbx', null, '1');
INSERT INTO `sys_area` VALUES ('610826', '绥德县', '3', '610800', '0912', 'sdx', null, '1');
INSERT INTO `sys_area` VALUES ('610827', '米脂县', '3', '610800', '0912', 'mzx', null, '1');
INSERT INTO `sys_area` VALUES ('610828', '佳县', '3', '610800', '0912', 'jx', null, '1');
INSERT INTO `sys_area` VALUES ('610829', '吴堡县', '3', '610800', '0912', 'wbx', null, '1');
INSERT INTO `sys_area` VALUES ('610830', '清涧县', '3', '610800', '0912', 'qjx', null, '1');
INSERT INTO `sys_area` VALUES ('610831', '子洲县', '3', '610800', '0912', 'zzx', null, '1');
INSERT INTO `sys_area` VALUES ('610900', '安康市', '2', '610000', null, 'aks', null, '1');
INSERT INTO `sys_area` VALUES ('610901', '市辖区', '3', '610900', '0915', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('610902', '汉滨区', '3', '610900', '0915', 'hbq', null, '1');
INSERT INTO `sys_area` VALUES ('610921', '汉阴县', '3', '610900', '0915', 'hyx', null, '1');
INSERT INTO `sys_area` VALUES ('610922', '石泉县', '3', '610900', '0915', 'sqx', null, '1');
INSERT INTO `sys_area` VALUES ('610923', '宁陕县', '3', '610900', '0915', 'nsx', null, '1');
INSERT INTO `sys_area` VALUES ('610924', '紫阳县', '3', '610900', '0915', 'zyx', null, '1');
INSERT INTO `sys_area` VALUES ('610925', '岚皋县', '3', '610900', '0915', 'lgx', null, '1');
INSERT INTO `sys_area` VALUES ('610926', '平利县', '3', '610900', '0915', 'plx', null, '1');
INSERT INTO `sys_area` VALUES ('610927', '镇坪县', '3', '610900', '0915', 'zpx', null, '1');
INSERT INTO `sys_area` VALUES ('610928', '旬阳县', '3', '610900', '0915', 'xyx', null, '1');
INSERT INTO `sys_area` VALUES ('610929', '白河县', '3', '610900', '0915', 'bhx', null, '1');
INSERT INTO `sys_area` VALUES ('611000', '商洛市', '2', '610000', null, 'sls', null, '1');
INSERT INTO `sys_area` VALUES ('611001', '市辖区', '3', '611000', '0914', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('611002', '商州区', '3', '611000', '0914', 'szq', null, '1');
INSERT INTO `sys_area` VALUES ('611021', '洛南县', '3', '611000', '0914', 'lnx', null, '1');
INSERT INTO `sys_area` VALUES ('611022', '丹凤县', '3', '611000', '0914', 'dfx', null, '1');
INSERT INTO `sys_area` VALUES ('611023', '商南县', '3', '611000', '0914', 'snx', null, '1');
INSERT INTO `sys_area` VALUES ('611024', '山阳县', '3', '611000', '0914', 'syx', null, '1');
INSERT INTO `sys_area` VALUES ('611025', '镇安县', '3', '611000', '0914', 'zax', null, '1');
INSERT INTO `sys_area` VALUES ('611026', '柞水县', '3', '611000', '0914', 'zsx', null, '1');
INSERT INTO `sys_area` VALUES ('620000', '甘肃省', '1', '0', null, 'gss', null, '1');
INSERT INTO `sys_area` VALUES ('620100', '兰州市', '2', '620000', null, 'lzs', null, '1');
INSERT INTO `sys_area` VALUES ('620101', '市辖区', '3', '620100', '0931', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('620102', '城关区', '3', '620100', '0931', 'cgq', null, '1');
INSERT INTO `sys_area` VALUES ('620103', '七里河区', '3', '620100', '0931', 'qlhq', null, '1');
INSERT INTO `sys_area` VALUES ('620104', '西固区', '3', '620100', '0931', 'xgq', null, '1');
INSERT INTO `sys_area` VALUES ('620105', '安宁区', '3', '620100', '0931', 'anq', null, '1');
INSERT INTO `sys_area` VALUES ('620111', '红古区', '3', '620100', '0931', 'hgq', null, '1');
INSERT INTO `sys_area` VALUES ('620121', '永登县', '3', '620100', '0931', 'ydx', null, '1');
INSERT INTO `sys_area` VALUES ('620122', '皋兰县', '3', '620100', '0931', 'glx', null, '1');
INSERT INTO `sys_area` VALUES ('620123', '榆中县', '3', '620100', '0931', 'yzx', null, '1');
INSERT INTO `sys_area` VALUES ('620200', '嘉峪关市', '2', '620000', null, 'jygs', null, '1');
INSERT INTO `sys_area` VALUES ('620201', '市辖区', '3', '620200', '0935', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('620300', '金昌市', '2', '620000', null, 'jcs', null, '1');
INSERT INTO `sys_area` VALUES ('620301', '市辖区', '3', '620300', '0935', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('620302', '金川区', '3', '620300', '0935', 'jcq', null, '1');
INSERT INTO `sys_area` VALUES ('620321', '永昌县', '3', '620300', '0935', 'ycx', null, '1');
INSERT INTO `sys_area` VALUES ('620400', '白银市', '2', '620000', null, 'bys', null, '1');
INSERT INTO `sys_area` VALUES ('620401', '市辖区', '3', '620400', '0943', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('620402', '白银区', '3', '620400', '0943', 'byq', null, '1');
INSERT INTO `sys_area` VALUES ('620403', '平川区', '3', '620400', '0943', 'pcq', null, '1');
INSERT INTO `sys_area` VALUES ('620421', '靖远县', '3', '620400', '0943', 'jyx', null, '1');
INSERT INTO `sys_area` VALUES ('620422', '会宁县', '3', '620400', '0943', 'hnx', null, '1');
INSERT INTO `sys_area` VALUES ('620423', '景泰县', '3', '620400', '0943', 'jtx', null, '1');
INSERT INTO `sys_area` VALUES ('620500', '天水市', '2', '620000', null, 'tss', null, '1');
INSERT INTO `sys_area` VALUES ('620501', '市辖区', '3', '620500', '0938', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('620502', '秦州区', '3', '620500', '0938', 'qzq', null, '1');
INSERT INTO `sys_area` VALUES ('620503', '麦积区', '3', '620500', '0938', 'mjq', null, '1');
INSERT INTO `sys_area` VALUES ('620521', '清水县', '3', '620500', '0938', 'qsx', null, '1');
INSERT INTO `sys_area` VALUES ('620522', '秦安县', '3', '620500', '0938', 'qax', null, '1');
INSERT INTO `sys_area` VALUES ('620523', '甘谷县', '3', '620500', '0938', 'ggx', null, '1');
INSERT INTO `sys_area` VALUES ('620524', '武山县', '3', '620500', '0938', 'wsx', null, '1');
INSERT INTO `sys_area` VALUES ('620525', '张家川回族自治县', '3', '620500', '0935', 'zjchzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('620600', '武威市', '2', '620000', null, 'wws', null, '1');
INSERT INTO `sys_area` VALUES ('620601', '市辖区', '3', '620600', '0935', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('620602', '凉州区', '3', '620600', '0935', 'lzq', null, '1');
INSERT INTO `sys_area` VALUES ('620621', '民勤县', '3', '620600', '0935', 'mqx', null, '1');
INSERT INTO `sys_area` VALUES ('620622', '古浪县', '3', '620600', '0935', 'glx', null, '1');
INSERT INTO `sys_area` VALUES ('620623', '天祝藏族自治县', '3', '620600', '0935', 'tzzzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('620700', '张掖市', '2', '620000', null, 'zys', null, '1');
INSERT INTO `sys_area` VALUES ('620701', '市辖区', '3', '620700', '0936', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('620702', '甘州区', '3', '620700', '0936', 'gzq', null, '1');
INSERT INTO `sys_area` VALUES ('620721', '肃南裕固族自治县', '3', '620700', '0936', 'snygzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('620722', '民乐县', '3', '620700', '0936', 'mlx', null, '1');
INSERT INTO `sys_area` VALUES ('620723', '临泽县', '3', '620700', '0936', 'lzx', null, '1');
INSERT INTO `sys_area` VALUES ('620724', '高台县', '3', '620700', '0936', 'gtx', null, '1');
INSERT INTO `sys_area` VALUES ('620725', '山丹县', '3', '620700', '0936', 'sdx', null, '1');
INSERT INTO `sys_area` VALUES ('620800', '平凉市', '2', '620000', null, 'pls', null, '1');
INSERT INTO `sys_area` VALUES ('620801', '市辖区', '3', '620800', '0933', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('620802', '崆峒区', '3', '620800', '0933', 'ktq', null, '1');
INSERT INTO `sys_area` VALUES ('620821', '泾川县', '3', '620800', '0933', 'jcx', null, '1');
INSERT INTO `sys_area` VALUES ('620822', '灵台县', '3', '620800', '0933', 'ltx', null, '1');
INSERT INTO `sys_area` VALUES ('620823', '崇信县', '3', '620800', '0933', 'cxx', null, '1');
INSERT INTO `sys_area` VALUES ('620824', '华亭县', '3', '620800', '0933', 'htx', null, '1');
INSERT INTO `sys_area` VALUES ('620825', '庄浪县', '3', '620800', '0933', 'zlx', null, '1');
INSERT INTO `sys_area` VALUES ('620826', '静宁县', '3', '620800', '0933', 'jnx', null, '1');
INSERT INTO `sys_area` VALUES ('620900', '酒泉市', '2', '620000', null, 'jqs', null, '1');
INSERT INTO `sys_area` VALUES ('620901', '市辖区', '3', '620900', '0937', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('620902', '肃州区', '3', '620900', '0937', 'szq', null, '1');
INSERT INTO `sys_area` VALUES ('620921', '金塔县', '3', '620900', '0937', 'jtx', null, '1');
INSERT INTO `sys_area` VALUES ('620922', '瓜州县', '3', '620900', '0937', 'gzx', null, '1');
INSERT INTO `sys_area` VALUES ('620923', '肃北蒙古族自治县', '3', '620900', '0937', 'sbmgzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('620924', '阿克塞哈萨克族自治县', '3', '620900', '0937', 'akshskzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('620981', '玉门市', '3', '620900', '0937', 'yms', null, '1');
INSERT INTO `sys_area` VALUES ('620982', '敦煌市', '3', '620900', '0937', 'dhs', null, '1');
INSERT INTO `sys_area` VALUES ('621000', '庆阳市', '2', '620000', null, 'qys', null, '1');
INSERT INTO `sys_area` VALUES ('621001', '市辖区', '3', '621000', '0934', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('621002', '西峰区', '3', '621000', '0934', 'xfq', null, '1');
INSERT INTO `sys_area` VALUES ('621021', '庆城县', '3', '621000', '0934', 'qcx', null, '1');
INSERT INTO `sys_area` VALUES ('621022', '环县', '3', '621000', '0934', 'hx', null, '1');
INSERT INTO `sys_area` VALUES ('621023', '华池县', '3', '621000', '0934', 'hcx', null, '1');
INSERT INTO `sys_area` VALUES ('621024', '合水县', '3', '621000', '0934', 'hsx', null, '1');
INSERT INTO `sys_area` VALUES ('621025', '正宁县', '3', '621000', '0934', 'znx', null, '1');
INSERT INTO `sys_area` VALUES ('621026', '宁县', '3', '621000', '0934', 'nx', null, '1');
INSERT INTO `sys_area` VALUES ('621027', '镇原县', '3', '621000', '0934', 'zyx', null, '1');
INSERT INTO `sys_area` VALUES ('621100', '定西市', '2', '620000', null, 'dxs', null, '1');
INSERT INTO `sys_area` VALUES ('621101', '市辖区', '3', '621100', '0932', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('621102', '安定区', '3', '621100', '0932', 'adq', null, '1');
INSERT INTO `sys_area` VALUES ('621121', '通渭县', '3', '621100', '0932', 'twx', null, '1');
INSERT INTO `sys_area` VALUES ('621122', '陇西县', '3', '621100', '0932', 'lxx', null, '1');
INSERT INTO `sys_area` VALUES ('621123', '渭源县', '3', '621100', '0932', 'wyx', null, '1');
INSERT INTO `sys_area` VALUES ('621124', '临洮县', '3', '621100', '0932', 'ltx', null, '1');
INSERT INTO `sys_area` VALUES ('621125', '漳县', '3', '621100', '0932', 'zx', null, '1');
INSERT INTO `sys_area` VALUES ('621126', '岷县', '3', '621100', '0932', 'mx', null, '1');
INSERT INTO `sys_area` VALUES ('621200', '陇南市', '2', '620000', null, 'lns', null, '1');
INSERT INTO `sys_area` VALUES ('621201', '市辖区', '3', '621200', '0935', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('621202', '武都区', '3', '621200', '0935', 'wdq', null, '1');
INSERT INTO `sys_area` VALUES ('621221', '成县', '3', '621200', '0935', 'cx', null, '1');
INSERT INTO `sys_area` VALUES ('621222', '文县', '3', '621200', '0935', 'wx', null, '1');
INSERT INTO `sys_area` VALUES ('621223', '宕昌县', '3', '621200', '0935', 'dcx', null, '1');
INSERT INTO `sys_area` VALUES ('621224', '康县', '3', '621200', '0935', 'kx', null, '1');
INSERT INTO `sys_area` VALUES ('621225', '西和县', '3', '621200', '0935', 'xhx', null, '1');
INSERT INTO `sys_area` VALUES ('621226', '礼县', '3', '621200', '0935', 'lx', null, '1');
INSERT INTO `sys_area` VALUES ('621227', '徽县', '3', '621200', '0935', 'hx', null, '1');
INSERT INTO `sys_area` VALUES ('621228', '两当县', '3', '621200', '0935', 'ldx', null, '1');
INSERT INTO `sys_area` VALUES ('622900', '临夏回族自治州', '2', '620000', null, 'lxhzzzz', null, '1');
INSERT INTO `sys_area` VALUES ('622901', '临夏市', '3', '622900', '0930', 'lxs', null, '1');
INSERT INTO `sys_area` VALUES ('622921', '临夏县', '3', '622900', '0930', 'lxx', null, '1');
INSERT INTO `sys_area` VALUES ('622922', '康乐县', '3', '622900', '0930', 'klx', null, '1');
INSERT INTO `sys_area` VALUES ('622923', '永靖县', '3', '622900', '0930', 'yjx', null, '1');
INSERT INTO `sys_area` VALUES ('622924', '广河县', '3', '622900', '0930', 'ghx', null, '1');
INSERT INTO `sys_area` VALUES ('622925', '和政县', '3', '622900', '0930', 'hzx', null, '1');
INSERT INTO `sys_area` VALUES ('622926', '东乡族自治县', '3', '622900', '0930', 'dxzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('622927', '积石山保安族东乡族撒拉族自治县', '3', '622900', '0930', 'jssbazdxzslzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('623000', '甘南藏族自治州', '2', '620000', null, 'gnzzzzz', null, '1');
INSERT INTO `sys_area` VALUES ('623001', '合作市', '3', '623000', '0941', 'hzs', null, '1');
INSERT INTO `sys_area` VALUES ('623021', '临潭县', '3', '623000', '0941', 'ltx', null, '1');
INSERT INTO `sys_area` VALUES ('623022', '卓尼县', '3', '623000', '0941', 'znx', null, '1');
INSERT INTO `sys_area` VALUES ('623023', '舟曲县', '3', '623000', '0941', 'zqx', null, '1');
INSERT INTO `sys_area` VALUES ('623024', '迭部县', '3', '623000', '0941', 'dbx', null, '1');
INSERT INTO `sys_area` VALUES ('623025', '玛曲县', '3', '623000', '0941', 'mqx', null, '1');
INSERT INTO `sys_area` VALUES ('623026', '碌曲县', '3', '623000', '0941', 'lqx', null, '1');
INSERT INTO `sys_area` VALUES ('623027', '夏河县', '3', '623000', '0941', 'xhx', null, '1');
INSERT INTO `sys_area` VALUES ('630000', '青海省', '1', '0', null, 'qhs', null, '1');
INSERT INTO `sys_area` VALUES ('630100', '西宁市', '2', '630000', null, 'xns', null, '1');
INSERT INTO `sys_area` VALUES ('630101', '市辖区', '3', '630100', '0971', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('630102', '城东区', '3', '630100', '0971', 'cdq', null, '1');
INSERT INTO `sys_area` VALUES ('630103', '城中区', '3', '630100', '0971', 'czq', null, '1');
INSERT INTO `sys_area` VALUES ('630104', '城西区', '3', '630100', '0971', 'cxq', null, '1');
INSERT INTO `sys_area` VALUES ('630105', '城北区', '3', '630100', '0971', 'cbq', null, '1');
INSERT INTO `sys_area` VALUES ('630121', '大通回族土族自治县', '3', '630100', '0971', 'dthztzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('630122', '湟中县', '3', '630100', '0972', 'hzx', null, '1');
INSERT INTO `sys_area` VALUES ('630123', '湟源县', '3', '630100', '0972', 'hyx', null, '1');
INSERT INTO `sys_area` VALUES ('632100', '海东地区', '2', '630000', null, 'hddq', null, '1');
INSERT INTO `sys_area` VALUES ('632121', '平安县', '3', '632100', '0972', 'pax', null, '1');
INSERT INTO `sys_area` VALUES ('632122', '民和回族土族自治县', '3', '632100', '0972', 'mhhztzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('632123', '乐都县', '3', '632100', '0972', 'ldx', null, '1');
INSERT INTO `sys_area` VALUES ('632126', '互助土族自治县', '3', '632100', '0972', 'hztzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('632127', '化隆回族自治县', '3', '632100', '0972', 'hlhzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('632128', '循化撒拉族自治县', '3', '632100', '0972', 'xhslzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('632200', '海北藏族自治州', '2', '630000', null, 'hbzzzzz', null, '1');
INSERT INTO `sys_area` VALUES ('632221', '门源回族自治县', '3', '632200', '0978', 'myhzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('632222', '祁连县', '3', '632200', '0970', 'qlx', null, '1');
INSERT INTO `sys_area` VALUES ('632223', '海晏县', '3', '632200', '0970', 'hyx', null, '1');
INSERT INTO `sys_area` VALUES ('632224', '刚察县', '3', '632200', '0970', 'gcx', null, '1');
INSERT INTO `sys_area` VALUES ('632300', '黄南藏族自治州', '2', '630000', null, 'hnzzzzz', null, '1');
INSERT INTO `sys_area` VALUES ('632321', '同仁县', '3', '632300', '0973', 'trx', null, '1');
INSERT INTO `sys_area` VALUES ('632322', '尖扎县', '3', '632300', '0973', 'jzx', null, '1');
INSERT INTO `sys_area` VALUES ('632323', '泽库县', '3', '632300', '0973', 'zkx', null, '1');
INSERT INTO `sys_area` VALUES ('632324', '河南蒙古族自治县', '3', '632300', '0973', 'hnmgzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('632500', '海南藏族自治州', '2', '630000', null, 'hnzzzzz', null, '1');
INSERT INTO `sys_area` VALUES ('632521', '共和县', '3', '632500', '0974', 'ghx', null, '1');
INSERT INTO `sys_area` VALUES ('632522', '同德县', '3', '632500', '0974', 'tdx', null, '1');
INSERT INTO `sys_area` VALUES ('632523', '贵德县', '3', '632500', '0974', 'gdx', null, '1');
INSERT INTO `sys_area` VALUES ('632524', '兴海县', '3', '632500', '0974', 'xhx', null, '1');
INSERT INTO `sys_area` VALUES ('632525', '贵南县', '3', '632500', '0974', 'gnx', null, '1');
INSERT INTO `sys_area` VALUES ('632600', '果洛藏族自治州', '2', '630000', null, 'glzzzzz', null, '1');
INSERT INTO `sys_area` VALUES ('632621', '玛沁县', '3', '632600', '0975', 'mqx', null, '1');
INSERT INTO `sys_area` VALUES ('632622', '班玛县', '3', '632600', '0975', 'bmx', null, '1');
INSERT INTO `sys_area` VALUES ('632623', '甘德县', '3', '632600', '0975', 'gdx', null, '1');
INSERT INTO `sys_area` VALUES ('632624', '达日县', '3', '632600', '0975', 'drx', null, '1');
INSERT INTO `sys_area` VALUES ('632625', '久治县', '3', '632600', '0975', 'jzx', null, '1');
INSERT INTO `sys_area` VALUES ('632626', '玛多县', '3', '632600', '0975', 'mdx', null, '1');
INSERT INTO `sys_area` VALUES ('632700', '玉树藏族自治州', '2', '630000', null, 'yszzzzz', null, '1');
INSERT INTO `sys_area` VALUES ('632721', '玉树县', '3', '632700', '0976', 'ysx', null, '1');
INSERT INTO `sys_area` VALUES ('632722', '杂多县', '3', '632700', '0976', 'zdx', null, '1');
INSERT INTO `sys_area` VALUES ('632723', '称多县', '3', '632700', '0976', 'cdx', null, '1');
INSERT INTO `sys_area` VALUES ('632724', '治多县', '3', '632700', '0976', 'zdx', null, '1');
INSERT INTO `sys_area` VALUES ('632725', '囊谦县', '3', '632700', '0976', 'nqx', null, '1');
INSERT INTO `sys_area` VALUES ('632726', '曲麻莱县', '3', '632700', '0976', 'qmlx', null, '1');
INSERT INTO `sys_area` VALUES ('632800', '海西蒙古族藏族自治州', '2', '630000', null, 'hxmgzzzzzz', null, '1');
INSERT INTO `sys_area` VALUES ('632801', '格尔木市', '3', '632800', '0977', 'gems', null, '1');
INSERT INTO `sys_area` VALUES ('632802', '德令哈市', '3', '632800', '0977', 'dlhs', null, '1');
INSERT INTO `sys_area` VALUES ('632821', '乌兰县', '3', '632800', '0977', 'wlx', null, '1');
INSERT INTO `sys_area` VALUES ('632822', '都兰县', '3', '632800', '0977', 'dlx', null, '1');
INSERT INTO `sys_area` VALUES ('632823', '天峻县', '3', '632800', '0977', 'tjx', null, '1');
INSERT INTO `sys_area` VALUES ('640000', '宁夏回族自治区', '1', '0', null, 'nxhzzzq', null, '1');
INSERT INTO `sys_area` VALUES ('640100', '银川市', '2', '640000', null, 'ycs', null, '1');
INSERT INTO `sys_area` VALUES ('640101', '市辖区', '3', '640100', '0951', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('640104', '兴庆区', '3', '640100', '0951', 'xqq', null, '1');
INSERT INTO `sys_area` VALUES ('640105', '西夏区', '3', '640100', '0951', 'xxq', null, '1');
INSERT INTO `sys_area` VALUES ('640106', '金凤区', '3', '640100', '0951', 'jfq', null, '1');
INSERT INTO `sys_area` VALUES ('640121', '永宁县', '3', '640100', '0951', 'ynx', null, '1');
INSERT INTO `sys_area` VALUES ('640122', '贺兰县', '3', '640100', '0951', 'hlx', null, '1');
INSERT INTO `sys_area` VALUES ('640181', '灵武市', '3', '640100', '0951', 'lws', null, '1');
INSERT INTO `sys_area` VALUES ('640200', '石嘴山市', '2', '640000', null, 'szss', null, '1');
INSERT INTO `sys_area` VALUES ('640201', '市辖区', '3', '640200', '0952', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('640202', '大武口区', '3', '640200', '0952', 'dwkq', null, '1');
INSERT INTO `sys_area` VALUES ('640205', '惠农区', '3', '640200', '0952', 'hnq', null, '1');
INSERT INTO `sys_area` VALUES ('640221', '平罗县', '3', '640200', '0952', 'plx', null, '1');
INSERT INTO `sys_area` VALUES ('640300', '吴忠市', '2', '640000', null, 'wzs', null, '1');
INSERT INTO `sys_area` VALUES ('640301', '市辖区', '3', '640300', '0953', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('640302', '利通区', '3', '640300', '0953', 'ltq', null, '1');
INSERT INTO `sys_area` VALUES ('640303', '红寺堡区', '3', '640300', null, 'hsbq', null, '1');
INSERT INTO `sys_area` VALUES ('640323', '盐池县', '3', '640300', '0953', 'ycx', null, '1');
INSERT INTO `sys_area` VALUES ('640324', '同心县', '3', '640300', '0953', 'txx', null, '1');
INSERT INTO `sys_area` VALUES ('640381', '青铜峡市', '3', '640300', '0953', 'qtxs', null, '1');
INSERT INTO `sys_area` VALUES ('640400', '固原市', '2', '640000', null, 'gys', null, '1');
INSERT INTO `sys_area` VALUES ('640401', '市辖区', '3', '640400', '0954', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('640402', '原州区', '3', '640400', '0954', 'yzq', null, '1');
INSERT INTO `sys_area` VALUES ('640422', '西吉县', '3', '640400', '0954', 'xjx', null, '1');
INSERT INTO `sys_area` VALUES ('640423', '隆德县', '3', '640400', '0954', 'ldx', null, '1');
INSERT INTO `sys_area` VALUES ('640424', '泾源县', '3', '640400', '0954', 'jyx', null, '1');
INSERT INTO `sys_area` VALUES ('640425', '彭阳县', '3', '640400', '0954', 'pyx', null, '1');
INSERT INTO `sys_area` VALUES ('640500', '中卫市', '2', '640000', null, 'zws', null, '1');
INSERT INTO `sys_area` VALUES ('640501', '市辖区', '3', '640500', '0953', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('640502', '沙坡头区', '3', '640500', '0953', 'sptq', null, '1');
INSERT INTO `sys_area` VALUES ('640521', '中宁县', '3', '640500', '0953', 'znx', null, '1');
INSERT INTO `sys_area` VALUES ('640522', '海原县', '3', '640500', '0954', 'hyx', null, '1');
INSERT INTO `sys_area` VALUES ('650000', '新疆维吾尔自治区', '1', '0', null, 'xjwwezzq', null, '1');
INSERT INTO `sys_area` VALUES ('650100', '乌鲁木齐市', '2', '650000', null, 'wlmqs', null, '1');
INSERT INTO `sys_area` VALUES ('650101', '市辖区', '3', '650100', '0991', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('650102', '天山区', '3', '650100', '0991', 'tsq', null, '1');
INSERT INTO `sys_area` VALUES ('650103', '沙依巴克区', '3', '650100', '0991', 'sybkq', null, '1');
INSERT INTO `sys_area` VALUES ('650104', '新市区', '3', '650100', '0991', 'xsq', null, '1');
INSERT INTO `sys_area` VALUES ('650105', '水磨沟区', '3', '650100', '0991', 'smgq', null, '1');
INSERT INTO `sys_area` VALUES ('650106', '头屯河区', '3', '650100', '0991', 'tthq', null, '1');
INSERT INTO `sys_area` VALUES ('650107', '达坂城区', '3', '650100', '0992', 'dbcq', null, '1');
INSERT INTO `sys_area` VALUES ('650109', '米东区', '3', '650100', null, 'mdq', null, '1');
INSERT INTO `sys_area` VALUES ('650121', '乌鲁木齐县', '3', '650100', '0991', 'wlmqx', null, '1');
INSERT INTO `sys_area` VALUES ('650200', '克拉玛依市', '2', '650000', null, 'klmys', null, '1');
INSERT INTO `sys_area` VALUES ('650201', '市辖区', '3', '650200', '0990', 'sxq', null, '1');
INSERT INTO `sys_area` VALUES ('650202', '独山子区', '3', '650200', '0990', 'dszq', null, '1');
INSERT INTO `sys_area` VALUES ('650203', '克拉玛依区', '3', '650200', '0990', 'klmyq', null, '1');
INSERT INTO `sys_area` VALUES ('650204', '白碱滩区', '3', '650200', '0990', 'bjtq', null, '1');
INSERT INTO `sys_area` VALUES ('650205', '乌尔禾区', '3', '650200', '0990', 'wehq', null, '1');
INSERT INTO `sys_area` VALUES ('652100', '吐鲁番地区', '2', '650000', null, 'tlfdq', null, '1');
INSERT INTO `sys_area` VALUES ('652101', '吐鲁番市', '3', '652100', '0995', 'tlfs', null, '1');
INSERT INTO `sys_area` VALUES ('652122', '鄯善县', '3', '652100', '0995', 'ssx', null, '1');
INSERT INTO `sys_area` VALUES ('652123', '托克逊县', '3', '652100', '0995', 'tkxx', null, '1');
INSERT INTO `sys_area` VALUES ('652200', '哈密地区', '2', '650000', null, 'hmdq', null, '1');
INSERT INTO `sys_area` VALUES ('652201', '哈密市', '3', '652200', '0902', 'hms', null, '1');
INSERT INTO `sys_area` VALUES ('652222', '巴里坤哈萨克自治县', '3', '652200', '0902', 'blkhskzzx', null, '1');
INSERT INTO `sys_area` VALUES ('652223', '伊吾县', '3', '652200', '0902', 'ywx', null, '1');
INSERT INTO `sys_area` VALUES ('652300', '昌吉回族自治州', '2', '650000', null, 'cjhzzzz', null, '1');
INSERT INTO `sys_area` VALUES ('652301', '昌吉市', '3', '652300', '0994', 'cjs', null, '1');
INSERT INTO `sys_area` VALUES ('652302', '阜康市', '3', '652300', '0994', 'fks', null, '1');
INSERT INTO `sys_area` VALUES ('652323', '呼图壁县', '3', '652300', '0994', 'htbx', null, '1');
INSERT INTO `sys_area` VALUES ('652324', '玛纳斯县', '3', '652300', '0994', 'mnsx', null, '1');
INSERT INTO `sys_area` VALUES ('652325', '奇台县', '3', '652300', '0994', 'qtx', null, '1');
INSERT INTO `sys_area` VALUES ('652327', '吉木萨尔县', '3', '652300', '0994', 'jmsex', null, '1');
INSERT INTO `sys_area` VALUES ('652328', '木垒哈萨克自治县', '3', '652300', '0994', 'mlhskzzx', null, '1');
INSERT INTO `sys_area` VALUES ('652700', '博尔塔拉蒙古自治州', '2', '650000', null, 'betlmgzzz', null, '1');
INSERT INTO `sys_area` VALUES ('652701', '博乐市', '3', '652700', '0909', 'bls', null, '1');
INSERT INTO `sys_area` VALUES ('652722', '精河县', '3', '652700', '0909', 'jhx', null, '1');
INSERT INTO `sys_area` VALUES ('652723', '温泉县', '3', '652700', '0909', 'wqx', null, '1');
INSERT INTO `sys_area` VALUES ('652800', '巴音郭楞蒙古自治州', '2', '650000', null, 'byglmgzzz', null, '1');
INSERT INTO `sys_area` VALUES ('652801', '库尔勒市', '3', '652800', '0996', 'kels', null, '1');
INSERT INTO `sys_area` VALUES ('652822', '轮台县', '3', '652800', '0996', 'ltx', null, '1');
INSERT INTO `sys_area` VALUES ('652823', '尉犁县', '3', '652800', '0996', 'wlx', null, '1');
INSERT INTO `sys_area` VALUES ('652824', '若羌县', '3', '652800', '0996', 'rqx', null, '1');
INSERT INTO `sys_area` VALUES ('652825', '且末县', '3', '652800', '0996', 'qmx', null, '1');
INSERT INTO `sys_area` VALUES ('652826', '焉耆回族自治县', '3', '652800', '0996', 'yqhzzzx', null, '1');
INSERT INTO `sys_area` VALUES ('652827', '和静县', '3', '652800', '0996', 'hjx', null, '1');
INSERT INTO `sys_area` VALUES ('652828', '和硕县', '3', '652800', '0996', 'hsx', null, '1');
INSERT INTO `sys_area` VALUES ('652829', '博湖县', '3', '652800', '0996', 'bhx', null, '1');
INSERT INTO `sys_area` VALUES ('652900', '阿克苏地区', '2', '650000', null, 'aksdq', null, '1');
INSERT INTO `sys_area` VALUES ('652901', '阿克苏市', '3', '652900', '0997', 'akss', null, '1');
INSERT INTO `sys_area` VALUES ('652922', '温宿县', '3', '652900', '0997', 'wsx', null, '1');
INSERT INTO `sys_area` VALUES ('652923', '库车县', '3', '652900', '0997', 'kcx', null, '1');
INSERT INTO `sys_area` VALUES ('652924', '沙雅县', '3', '652900', '0997', 'syx', null, '1');
INSERT INTO `sys_area` VALUES ('652925', '新和县', '3', '652900', '0997', 'xhx', null, '1');
INSERT INTO `sys_area` VALUES ('652926', '拜城县', '3', '652900', '0997', 'bcx', null, '1');
INSERT INTO `sys_area` VALUES ('652927', '乌什县', '3', '652900', '0997', 'wsx', null, '1');
INSERT INTO `sys_area` VALUES ('652928', '阿瓦提县', '3', '652900', '0997', 'awtx', null, '1');
INSERT INTO `sys_area` VALUES ('652929', '柯坪县', '3', '652900', '0908', 'kpx', null, '1');
INSERT INTO `sys_area` VALUES ('653000', '克孜勒苏柯尔克孜自治州', '2', '650000', null, 'kzlskekzzzz', null, '1');
INSERT INTO `sys_area` VALUES ('653001', '阿图什市', '3', '653000', '0908', 'atss', null, '1');
INSERT INTO `sys_area` VALUES ('653022', '阿克陶县', '3', '653000', '0908', 'aktx', null, '1');
INSERT INTO `sys_area` VALUES ('653023', '阿合奇县', '3', '653000', '0997', 'ahqx', null, '1');
INSERT INTO `sys_area` VALUES ('653024', '乌恰县', '3', '653000', '0908', 'wqx', null, '1');
INSERT INTO `sys_area` VALUES ('653100', '喀什地区', '2', '650000', null, 'ksdq', null, '1');
INSERT INTO `sys_area` VALUES ('653101', '喀什市', '3', '653100', '0998', 'kss', null, '1');
INSERT INTO `sys_area` VALUES ('653121', '疏附县', '3', '653100', '0998', 'sfx', null, '1');
INSERT INTO `sys_area` VALUES ('653122', '疏勒县', '3', '653100', '0998', 'slx', null, '1');
INSERT INTO `sys_area` VALUES ('653123', '英吉沙县', '3', '653100', '0998', 'yjsx', null, '1');
INSERT INTO `sys_area` VALUES ('653124', '泽普县', '3', '653100', '0998', 'zpx', null, '1');
INSERT INTO `sys_area` VALUES ('653125', '莎车县', '3', '653100', '0998', 'scx', null, '1');
INSERT INTO `sys_area` VALUES ('653126', '叶城县', '3', '653100', '0998', 'ycx', null, '1');
INSERT INTO `sys_area` VALUES ('653127', '麦盖提县', '3', '653100', '0998', 'mgtx', null, '1');
INSERT INTO `sys_area` VALUES ('653128', '岳普湖县', '3', '653100', '0998', 'yphx', null, '1');
INSERT INTO `sys_area` VALUES ('653129', '伽师县', '3', '653100', '0998', 'jsx', null, '1');
INSERT INTO `sys_area` VALUES ('653130', '巴楚县', '3', '653100', '0998', 'bcx', null, '1');
INSERT INTO `sys_area` VALUES ('653131', '塔什库尔干塔吉克自治县', '3', '653100', '0998', 'tskegtjkzzx', null, '1');
INSERT INTO `sys_area` VALUES ('653200', '和田地区', '2', '650000', null, 'htdq', null, '1');
INSERT INTO `sys_area` VALUES ('653201', '和田市', '3', '653200', '0903', 'hts', null, '1');
INSERT INTO `sys_area` VALUES ('653221', '和田县', '3', '653200', '0903', 'htx', null, '1');
INSERT INTO `sys_area` VALUES ('653222', '墨玉县', '3', '653200', '0903', 'myx', null, '1');
INSERT INTO `sys_area` VALUES ('653223', '皮山县', '3', '653200', '0903', 'psx', null, '1');
INSERT INTO `sys_area` VALUES ('653224', '洛浦县', '3', '653200', '0903', 'lpx', null, '1');
INSERT INTO `sys_area` VALUES ('653225', '策勒县', '3', '653200', '0903', 'clx', null, '1');
INSERT INTO `sys_area` VALUES ('653226', '于田县', '3', '653200', '0903', 'ytx', null, '1');
INSERT INTO `sys_area` VALUES ('653227', '民丰县', '3', '653200', '0903', 'mfx', null, '1');
INSERT INTO `sys_area` VALUES ('654000', '伊犁哈萨克自治州', '2', '650000', null, 'ylhskzzz', null, '1');
INSERT INTO `sys_area` VALUES ('654002', '伊宁市', '3', '654000', '0999', 'yns', null, '1');
INSERT INTO `sys_area` VALUES ('654003', '奎屯市', '3', '654000', '0992', 'kts', null, '1');
INSERT INTO `sys_area` VALUES ('654021', '伊宁县', '3', '654000', '0999', 'ynx', null, '1');
INSERT INTO `sys_area` VALUES ('654022', '察布查尔锡伯自治县', '3', '654000', '0999', 'cbcexbzzx', null, '1');
INSERT INTO `sys_area` VALUES ('654023', '霍城县', '3', '654000', '0999', 'hcx', null, '1');
INSERT INTO `sys_area` VALUES ('654024', '巩留县', '3', '654000', '0999', 'glx', null, '1');
INSERT INTO `sys_area` VALUES ('654025', '新源县', '3', '654000', '0999', 'xyx', null, '1');
INSERT INTO `sys_area` VALUES ('654026', '昭苏县', '3', '654000', '0999', 'zsx', null, '1');
INSERT INTO `sys_area` VALUES ('654027', '特克斯县', '3', '654000', '0999', 'tksx', null, '1');
INSERT INTO `sys_area` VALUES ('654028', '尼勒克县', '3', '654000', '0999', 'nlkx', null, '1');
INSERT INTO `sys_area` VALUES ('654200', '塔城地区', '2', '650000', null, 'tcdq', null, '1');
INSERT INTO `sys_area` VALUES ('654201', '塔城市', '3', '654200', '0901', 'tcs', null, '1');
INSERT INTO `sys_area` VALUES ('654202', '乌苏市', '3', '654200', '0992', 'wss', null, '1');
INSERT INTO `sys_area` VALUES ('654221', '额敏县', '3', '654200', '0901', 'emx', null, '1');
INSERT INTO `sys_area` VALUES ('654223', '沙湾县', '3', '654200', '0993', 'swx', null, '1');
INSERT INTO `sys_area` VALUES ('654224', '托里县', '3', '654200', '0901', 'tlx', null, '1');
INSERT INTO `sys_area` VALUES ('654225', '裕民县', '3', '654200', '0901', 'ymx', null, '1');
INSERT INTO `sys_area` VALUES ('654226', '和布克赛尔蒙古自治县', '3', '654200', '0990', 'hbksemgzzx', null, '1');
INSERT INTO `sys_area` VALUES ('654300', '阿勒泰地区', '2', '650000', null, 'altdq', null, '1');
INSERT INTO `sys_area` VALUES ('654301', '阿勒泰市', '3', '654300', '0906', 'alts', null, '1');
INSERT INTO `sys_area` VALUES ('654321', '布尔津县', '3', '654300', '0906', 'bejx', null, '1');
INSERT INTO `sys_area` VALUES ('654322', '富蕴县', '3', '654300', '0906', 'fyx', null, '1');
INSERT INTO `sys_area` VALUES ('654323', '福海县', '3', '654300', '0906', 'fhx', null, '1');
INSERT INTO `sys_area` VALUES ('654324', '哈巴河县', '3', '654300', '0906', 'hbhx', null, '1');
INSERT INTO `sys_area` VALUES ('654325', '青河县', '3', '654300', '0906', 'qhx', null, '1');
INSERT INTO `sys_area` VALUES ('654326', '吉木乃县', '3', '654300', '0906', 'jmnx', null, '1');
INSERT INTO `sys_area` VALUES ('659000', '自治区直辖县级行政区划', '2', '650000', null, 'zzqzxxjxzqh', null, '1');
INSERT INTO `sys_area` VALUES ('659001', '石河子市', '3', '659000', '0993', 'shzs', null, '1');
INSERT INTO `sys_area` VALUES ('659002', '阿拉尔市', '3', '659000', '0997', 'ales', null, '1');
INSERT INTO `sys_area` VALUES ('659003', '图木舒克市', '3', '659000', '0998', 'tmsks', null, '1');
INSERT INTO `sys_area` VALUES ('659004', '五家渠市', '3', '659000', '0994', 'wjqs', null, '1');
INSERT INTO `sys_area` VALUES ('710000', '台湾省', '1', '0', null, 'tws', null, '1');
INSERT INTO `sys_area` VALUES ('810000', '香港特别行政区', '1', '0', null, 'xgtbxzq', null, '1');
INSERT INTO `sys_area` VALUES ('820000', '澳门特别行政区', '1', '0', null, 'amtbxzq', null, '1');

-- ----------------------------
-- Table structure for sys_bulletins
-- ----------------------------
DROP TABLE IF EXISTS `sys_bulletins`;
CREATE TABLE `sys_bulletins` (
  `bulletin_id` int(12) NOT NULL,
  `bulletin_title` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `bulletin_content` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `file_batch` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `sender_staff` int(6) DEFAULT NULL,
  `sender_man` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `send_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `valid_flag` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`bulletin_id`),
  KEY `idx_sys_bulletins_send` (`send_date`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='公告表';

-- ----------------------------
-- Records of sys_bulletins
-- ----------------------------
INSERT INTO `sys_bulletins` VALUES ('1', 'test', 'test', null, null, null, '2017-06-30 10:35:23', '1');
INSERT INTO `sys_bulletins` VALUES ('2', '1111', '<p>你好啊1</p>', null, null, null, '2017-06-30 10:35:33', '1');
INSERT INTO `sys_bulletins` VALUES ('3', 'ds', '<p>ssff</p>', null, null, 'admin ', '2018-03-13 10:06:19', '1');
INSERT INTO `sys_bulletins` VALUES ('4', 'ss', '<p>ss</p>', null, null, 'admin ', '2018-03-13 10:06:36', '0');

-- ----------------------------
-- Table structure for sys_code_table
-- ----------------------------
DROP TABLE IF EXISTS `sys_code_table`;
CREATE TABLE `sys_code_table` (
  `code_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `code_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `code_sql` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `valid_flag` int(11) DEFAULT '1',
  PRIMARY KEY (`code_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='码表主表';

-- ----------------------------
-- Records of sys_code_table
-- ----------------------------
INSERT INTO `sys_code_table` VALUES ('bank_name', '银行名称', null, '1');
INSERT INTO `sys_code_table` VALUES ('cert_type', '证件类型', null, '1');
INSERT INTO `sys_code_table` VALUES ('diploma_type', '学历', null, '1');
INSERT INTO `sys_code_table` VALUES ('mem_status', '会员状态', null, '1');
INSERT INTO `sys_code_table` VALUES ('menu_type', '菜单类型', null, '1');
INSERT INTO `sys_code_table` VALUES ('menu_visiable', '菜单可见标志', null, '1');
INSERT INTO `sys_code_table` VALUES ('month', '月份', null, '1');
INSERT INTO `sys_code_table` VALUES ('nation', '民族', null, '1');
INSERT INTO `sys_code_table` VALUES ('role_range', '角色授权', null, '1');
INSERT INTO `sys_code_table` VALUES ('sex', '性别', null, '1');
INSERT INTO `sys_code_table` VALUES ('staff_sta', '用户状态', null, '1');
INSERT INTO `sys_code_table` VALUES ('status', '状态', null, '1');
INSERT INTO `sys_code_table` VALUES ('user_kind', '用户类型', 'select user_kind as data_value ,kind_name as display_value from sys_user_kind', '1');
INSERT INTO `sys_code_table` VALUES ('valid_flag', '有效标志', null, '1');
INSERT INTO `sys_code_table` VALUES ('years', '年份', null, '1');

-- ----------------------------
-- Table structure for sys_code_table_detail
-- ----------------------------
DROP TABLE IF EXISTS `sys_code_table_detail`;
CREATE TABLE `sys_code_table_detail` (
  `code_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `data_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `display_value` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `dis_order` int(4) NOT NULL DEFAULT '0',
  `valid_flag` int(11) DEFAULT '1',
  PRIMARY KEY (`code_type`,`data_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='码表详细表';

-- ----------------------------
-- Records of sys_code_table_detail
-- ----------------------------
INSERT INTO `sys_code_table_detail` VALUES ('bank_name', '1', '中国建设银行', '0', '1');
INSERT INTO `sys_code_table_detail` VALUES ('bank_name', '2', '中国工商银行', '0', '1');
INSERT INTO `sys_code_table_detail` VALUES ('bank_name', '3', '中国农业银行', '0', '1');
INSERT INTO `sys_code_table_detail` VALUES ('bank_name', '4', '中国银行', '0', '1');
INSERT INTO `sys_code_table_detail` VALUES ('bank_name', '5', '中国招商银行', '0', '1');
INSERT INTO `sys_code_table_detail` VALUES ('bank_name', '6', '中国交通银行', '0', '1');
INSERT INTO `sys_code_table_detail` VALUES ('cert_type', '1', '身份证', '1', '1');
INSERT INTO `sys_code_table_detail` VALUES ('cert_type', '2', '军官证', '2', '1');
INSERT INTO `sys_code_table_detail` VALUES ('cert_type', '3', '驾驶证', '3', '1');
INSERT INTO `sys_code_table_detail` VALUES ('mem_status', '0', '禁用', '0', '1');
INSERT INTO `sys_code_table_detail` VALUES ('mem_status', '1', '正常', '0', '1');
INSERT INTO `sys_code_table_detail` VALUES ('mem_status', '2', '锁定', '0', '1');
INSERT INTO `sys_code_table_detail` VALUES ('menu_type', '1', '菜单', '0', '1');
INSERT INTO `sys_code_table_detail` VALUES ('menu_type', '2', '权限', '1', '1');
INSERT INTO `sys_code_table_detail` VALUES ('menu_visiable', '1', '默认可见', '0', '1');
INSERT INTO `sys_code_table_detail` VALUES ('menu_visiable', '2', '按角色可见', '0', '1');
INSERT INTO `sys_code_table_detail` VALUES ('month', '0', '一月', '0', '1');
INSERT INTO `sys_code_table_detail` VALUES ('month', '1', '二月', '0', '1');
INSERT INTO `sys_code_table_detail` VALUES ('month', '10', '十一月', '0', '1');
INSERT INTO `sys_code_table_detail` VALUES ('month', '11', '十二月', '0', '1');
INSERT INTO `sys_code_table_detail` VALUES ('month', '2', '三月', '0', '1');
INSERT INTO `sys_code_table_detail` VALUES ('month', '3', '四月', '0', '1');
INSERT INTO `sys_code_table_detail` VALUES ('month', '4', '五月', '0', '1');
INSERT INTO `sys_code_table_detail` VALUES ('month', '5', '六月', '0', '1');
INSERT INTO `sys_code_table_detail` VALUES ('month', '6', '七月', '0', '1');
INSERT INTO `sys_code_table_detail` VALUES ('month', '7', '八月', '0', '1');
INSERT INTO `sys_code_table_detail` VALUES ('month', '8', '九月', '0', '1');
INSERT INTO `sys_code_table_detail` VALUES ('month', '9', '十月', '0', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '1', '汉族', '0', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '11', '满族', '11', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '12', '侗族', '12', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '13', '瑶族', '13', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '14', '白族', '14', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '15', '土家族', '15', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '16', '哈尼族', '16', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '17', '哈萨克族', '17', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '18', '傣族', '18', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '19', '黎族', '19', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '2', '蒙古族', '2', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '20', '傈傈族', '20', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '21', '佤族', '21', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '22', '畲族', '22', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '23', '高山族', '23', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '24', '拉祜族', '24', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '25', '水族', '25', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '26', '东乡族', '26', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '27', '纳西族', '27', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '28', '景颇族', '28', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '29', '柯尔克孜族', '29', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '3', '回族', '3', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '30', '土族', '30', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '31', '达翰尔族', '31', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '32', '仫佬族', '32', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '33', '羌族', '33', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '34', '布朗族', '34', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '35', '撒拉族', '35', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '36', '毛南族', '36', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '37', '仡佬族', '37', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '38', '锡伯族', '38', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '39', '阿昌族', '39', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '4', '藏族', '4', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '40', '普米族', '40', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '41', '塔吉克族', '41', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '42', '怒族', '42', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '43', '乌孜别克族', '43', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '44', '俄罗斯族', '44', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '45', '鄂温克族', '45', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '46', '德昂族', '46', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '47', '保安族', '47', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '48', '裕固族', '48', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '49', '京族', '49', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '5', '维吾尔族', '5', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '50', '塔塔尔族', '50', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '51', '独龙族', '51', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '52', '鄂伦春族', '52', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '53', '赫哲族', '53', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '54', '门巴族', '54', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '55', '珞巴族', '55', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '56', '基诺族', '56', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '6', '苗族', '6', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '7', '彝族', '7', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '8', '壮族', '8', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '9', '布依族', '9', '1');
INSERT INTO `sys_code_table_detail` VALUES ('nation', '99', '其他', '57', '1');
INSERT INTO `sys_code_table_detail` VALUES ('role_range', '0', '不可授权', '0', '1');
INSERT INTO `sys_code_table_detail` VALUES ('role_range', '1', '可授权', '0', '1');
INSERT INTO `sys_code_table_detail` VALUES ('sex', '0', '未知', '3', '1');
INSERT INTO `sys_code_table_detail` VALUES ('sex', '1', '男', '1', '1');
INSERT INTO `sys_code_table_detail` VALUES ('sex', '2', '女', '2', '1');
INSERT INTO `sys_code_table_detail` VALUES ('staff_sta', '0', '禁用', '0', '1');
INSERT INTO `sys_code_table_detail` VALUES ('staff_sta', '1', '正常', '0', '1');
INSERT INTO `sys_code_table_detail` VALUES ('status', '0', '禁用', '0', '1');
INSERT INTO `sys_code_table_detail` VALUES ('status', '1', '启用', '0', '1');
INSERT INTO `sys_code_table_detail` VALUES ('valid_flag', '0', '无效', '1', '1');
INSERT INTO `sys_code_table_detail` VALUES ('valid_flag', '1', '有效', '2', '1');
INSERT INTO `sys_code_table_detail` VALUES ('years', '2015', '2015', '0', '1');
INSERT INTO `sys_code_table_detail` VALUES ('years', '2016', '2016', '0', '1');
INSERT INTO `sys_code_table_detail` VALUES ('years', '2017', '2017', '0', '1');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `dept_id` int(11) NOT NULL COMMENT '部门ID',
  `dept_code` varchar(50) DEFAULT NULL,
  `dept_up_id` int(11) DEFAULT NULL,
  `dept_name` varchar(20) DEFAULT NULL COMMENT '部门名称',
  `tel` varchar(20) DEFAULT NULL,
  `remark` varchar(500) DEFAULT NULL,
  `creat_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `valid_flag` int(11) DEFAULT '1' COMMENT '0 否1是',
  PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='部门';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES ('1', null, '0', '网购部', '', '', '2017-07-05 13:49:04', '1');
INSERT INTO `sys_dept` VALUES ('2', null, '0', '制造工厂', '', '', '2017-07-05 13:49:26', '1');
INSERT INTO `sys_dept` VALUES ('3', null, '2', '流水线1', '', '', '2017-07-05 13:49:39', '1');
INSERT INTO `sys_dept` VALUES ('4', null, '1', 'ss', 'sss', 'ss', '2018-03-13 14:46:14', '0');
INSERT INTO `sys_dept` VALUES ('5', null, '2', '流水线2', '', '', '2017-07-05 13:49:50', '1');
INSERT INTO `sys_dept` VALUES ('6', null, '0', '财务部', '', null, '2015-08-17 22:13:26', '1');
INSERT INTO `sys_dept` VALUES ('7', null, '1', 'ss', 'ss', 'ss', '2018-03-13 14:46:18', '1');
INSERT INTO `sys_dept` VALUES ('8', null, '1', 'ss', 'sss', 'ss', '2018-03-13 14:46:19', '0');
INSERT INTO `sys_dept` VALUES ('9', null, '1', 'ss', 'ss', 'ss', '2018-03-13 14:46:20', '1');
INSERT INTO `sys_dept` VALUES ('10', null, '1', 'ss', 'ss', 'ss', '2018-03-13 14:46:31', '1');
INSERT INTO `sys_dept` VALUES ('17', null, '0', '行政部', '', null, '2015-09-02 18:12:41', '1');
INSERT INTO `sys_dept` VALUES ('19', null, '0', '销售部', '', '', '2015-09-13 20:04:33', '1');
INSERT INTO `sys_dept` VALUES ('22', null, '0', '软件开发部', '', '', '2015-09-13 21:32:41', '1');

-- ----------------------------
-- Table structure for sys_file
-- ----------------------------
DROP TABLE IF EXISTS `sys_file`;
CREATE TABLE `sys_file` (
  `id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `content_type` varchar(100) DEFAULT NULL,
  `filepath` varchar(500) DEFAULT NULL,
  `valid_flag` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文件表';

-- ----------------------------
-- Records of sys_file
-- ----------------------------
INSERT INTO `sys_file` VALUES ('3', '86751499137025742.png', '1043993', 'image/png', null, '1');
INSERT INTO `sys_file` VALUES ('4', '72161499137259704.png', '1043993', 'image/png', null, '1');
INSERT INTO `sys_file` VALUES ('5', '64171499137372686.png', '1043993', 'image/png', null, '1');
INSERT INTO `sys_file` VALUES ('6', '3601499137515176.png', '1043993', 'image/png', 'd:\\lxcloud\\upload\\6-3601499137515176.png', '1');
INSERT INTO `sys_file` VALUES ('7', '63341499137539777.png', '1043993', 'image/png', 'd:\\lxcloud\\upload\\7-63341499137539777.png', '1');
INSERT INTO `sys_file` VALUES ('8', '79231499147282285.jpg', '133151', 'image/jpeg', 'd:\\lxcloud\\upload\\8-79231499147282285.jpg', '1');
INSERT INTO `sys_file` VALUES ('9', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '133151', 'image/jpeg', 'd:\\lxcloud\\upload\\9-u=1264996776,4079453714&fm=214&gp=0.jpg', '1');
INSERT INTO `sys_file` VALUES ('10', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '133151', 'image/jpeg', 'd:\\lxcloud\\upload\\10-u=1264996776,4079453714&fm=214&gp=0.jpg', '1');
INSERT INTO `sys_file` VALUES ('11', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\11-u=1264996776,4079453714&fm=214&gp=0.jpg', '1');
INSERT INTO `sys_file` VALUES ('12', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\12-u=1264996776,4079453714&fm=214&gp=0.jpg', '1');
INSERT INTO `sys_file` VALUES ('13', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\13-u=1264996776,4079453714&fm=214&gp=0.jpg', '1');
INSERT INTO `sys_file` VALUES ('14', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\14-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('15', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\15-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('16', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\16-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('17', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\17-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('18', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\18-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('19', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\19-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('20', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\20-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('21', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\21-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('22', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\22-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('23', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\23-u=1264996776,4079453714&fm=214&gp=0.jpg', '1');
INSERT INTO `sys_file` VALUES ('24', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\24-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('25', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\25-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('26', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\26-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('27', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\27-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('28', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\28-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('29', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\29-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('30', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\30-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('31', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\31-u=1264996776,4079453714&fm=214&gp=0.jpg', '1');
INSERT INTO `sys_file` VALUES ('32', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\32-u=1264996776,4079453714&fm=214&gp=0.jpg', '1');
INSERT INTO `sys_file` VALUES ('33', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\33-u=1264996776,4079453714&fm=214&gp=0.jpg', '1');
INSERT INTO `sys_file` VALUES ('34', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\34-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('35', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\35-u=1264996776,4079453714&fm=214&gp=0.jpg', '0');
INSERT INTO `sys_file` VALUES ('36', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\36-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('37', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\37-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('38', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\38-u=1264996776,4079453714&fm=214&gp=0.jpg', '1');
INSERT INTO `sys_file` VALUES ('39', 'logo_icon.png', '7268', 'image/png', 'd:\\lxcloud\\upload\\39-logo_icon.png', '1');
INSERT INTO `sys_file` VALUES ('40', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\40-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('41', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\41-u=1264996776,4079453714&fm=214&gp=0.jpg', '1');
INSERT INTO `sys_file` VALUES ('42', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\42-u=1264996776,4079453714&fm=214&gp=0.jpg', '1');
INSERT INTO `sys_file` VALUES ('43', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\43-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('44', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\44-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('45', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\45-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('46', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\46-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('47', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\47-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('48', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\48-u=1264996776,4079453714&fm=214&gp=0.jpg', '1');
INSERT INTO `sys_file` VALUES ('49', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\49-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('50', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\50-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('51', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\51-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('52', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\52-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('53', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\53-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('54', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\54-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('55', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\55-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('56', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\56-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('57', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\57-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('58', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\58-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('59', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\59-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('60', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\60-u=1264996776,4079453714&fm=214&gp=0.jpg', '1');
INSERT INTO `sys_file` VALUES ('61', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\61-u=1264996776,4079453714&fm=214&gp=0.jpg', '1');
INSERT INTO `sys_file` VALUES ('62', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\62-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('63', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\63-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('64', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\64-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('65', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\65-u=1264996776,4079453714&fm=214&gp=0.jpg', '1');
INSERT INTO `sys_file` VALUES ('66', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\66-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('67', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\67-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('68', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\68-u=1264996776,4079453714&fm=214&gp=0.jpg', '1');
INSERT INTO `sys_file` VALUES ('69', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\69-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('70', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\70-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('71', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\71-u=1264996776,4079453714&fm=214&gp=0.jpg', '1');
INSERT INTO `sys_file` VALUES ('72', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\72-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('73', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\73-u=1264996776,4079453714&fm=214&gp=0.jpg', '1');
INSERT INTO `sys_file` VALUES ('74', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\74-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('75', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\75-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('76', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\76-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('77', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\77-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('78', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\78-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('79', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\79-u=1264996776,4079453714&fm=214&gp=0.jpg', '1');
INSERT INTO `sys_file` VALUES ('80', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\80-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('81', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\81-u=1264996776,4079453714&fm=214&gp=0.jpg', '1');
INSERT INTO `sys_file` VALUES ('82', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\82-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('83', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\83-u=1264996776,4079453714&fm=214&gp=0.jpg', '1');
INSERT INTO `sys_file` VALUES ('84', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\84-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '0');
INSERT INTO `sys_file` VALUES ('85', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\85-u=1264996776,4079453714&fm=214&gp=0.jpg', '0');
INSERT INTO `sys_file` VALUES ('86', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\86-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '0');
INSERT INTO `sys_file` VALUES ('87', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\87-u=1264996776,4079453714&fm=214&gp=0.jpg', '0');
INSERT INTO `sys_file` VALUES ('88', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\88-u=1264996776,4079453714&fm=214&gp=0.jpg', '0');
INSERT INTO `sys_file` VALUES ('89', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\89-u=1264996776,4079453714&fm=214&gp=0.jpg', '0');
INSERT INTO `sys_file` VALUES ('90', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\90-u=1264996776,4079453714&fm=214&gp=0.jpg', '0');
INSERT INTO `sys_file` VALUES ('91', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\91-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '0');
INSERT INTO `sys_file` VALUES ('92', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\92-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('93', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\93-u=1264996776,4079453714&fm=214&gp=0.jpg', '1');
INSERT INTO `sys_file` VALUES ('94', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\94-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '0');
INSERT INTO `sys_file` VALUES ('95', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\95-u=1264996776,4079453714&fm=214&gp=0.jpg', '0');
INSERT INTO `sys_file` VALUES ('96', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\96-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('97', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\97-u=1264996776,4079453714&fm=214&gp=0.jpg', '1');
INSERT INTO `sys_file` VALUES ('98', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\98-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('99', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\99-u=1264996776,4079453714&fm=214&gp=0.jpg', '1');
INSERT INTO `sys_file` VALUES ('100', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\100-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('101', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\101-u=1264996776,4079453714&fm=214&gp=0.jpg', '0');
INSERT INTO `sys_file` VALUES ('102', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\102-u=1264996776,4079453714&fm=214&gp=0.jpg', '0');
INSERT INTO `sys_file` VALUES ('103', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\103-u=1264996776,4079453714&fm=214&gp=0.jpg', '0');
INSERT INTO `sys_file` VALUES ('104', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\104-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('105', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\105-u=1264996776,4079453714&fm=214&gp=0.jpg', '1');
INSERT INTO `sys_file` VALUES ('106', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\106-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('107', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\107-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('108', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\108-u=1264996776,4079453714&fm=214&gp=0.jpg', '1');
INSERT INTO `sys_file` VALUES ('109', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\109-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('110', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\110-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('111', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\111-u=1264996776,4079453714&fm=214&gp=0.jpg', '1');
INSERT INTO `sys_file` VALUES ('112', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\112-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('113', 'u=1264996776,4079453714&fm=214&gp=0.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\113-u=1264996776,4079453714&fm=214&gp=0.jpg', '1');
INSERT INTO `sys_file` VALUES ('114', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\114-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('115', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\115-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '1');
INSERT INTO `sys_file` VALUES ('116', 'u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '188216', 'image/jpeg', 'd:\\lxcloud\\upload\\116-u=1264996776,4079453714&fm=214&gp=0 - 副本.jpg', '0');
INSERT INTO `sys_file` VALUES ('117', '六星云智能控制管理系统.png', '5641', 'image/png', 'd:\\lxcloud\\upload\\117-六星云智能控制管理系统.png', '1');
INSERT INTO `sys_file` VALUES ('118', 'bj.jpg', '149183', 'image/jpeg', 'd:\\lxcloud\\upload\\118-bj.jpg', '1');
INSERT INTO `sys_file` VALUES ('119', 'bj.jpg', '149183', 'image/jpeg', 'd:\\lxcloud\\upload\\119-bj.jpg', '1');
INSERT INTO `sys_file` VALUES ('120', 'bj.jpg', '149183', 'image/jpeg', 'd:\\lxcloud\\upload\\120-bj.jpg', '1');
INSERT INTO `sys_file` VALUES ('121', '六星云智能控制管理系统.png', '5641', 'image/png', 'd:\\lxcloud\\upload\\121-六星云智能控制管理系统.png', '1');
INSERT INTO `sys_file` VALUES ('122', 'bj.jpg', '149183', 'image/jpeg', 'd:\\lxcloud\\upload\\122-bj.jpg', '1');
INSERT INTO `sys_file` VALUES ('123', 'bj.jpg', '149183', 'image/jpeg', 'd:\\lxcloud\\upload\\123-bj.jpg', '0');
INSERT INTO `sys_file` VALUES ('124', 'leave-formkey.png', '21702', 'image/png', 'd:\\lxcloud\\upload\\124-leave-formkey.png', '1');
INSERT INTO `sys_file` VALUES ('125', 'leave-formkey.png', '21702', 'image/png', 'd:\\lxcloud\\upload\\125-leave-formkey.png', '1');
INSERT INTO `sys_file` VALUES ('126', 'dept-leader-audit.form', '2524', 'application/octet-stream', 'd:\\lxcloud\\upload\\126-dept-leader-audit.form', '1');
INSERT INTO `sys_file` VALUES ('127', 'hr-audit.form', '930', 'application/octet-stream', 'd:\\lxcloud\\upload\\127-hr-audit.form', '1');
INSERT INTO `sys_file` VALUES ('128', 'start.form', '2391', 'application/octet-stream', 'd:\\lxcloud\\upload\\128-start.form', '1');
INSERT INTO `sys_file` VALUES ('129', 'leave-formkey - 11.png', '21702', 'image/png', 'd:\\lxcloud\\upload\\129-leave-formkey - 11.png', '1');
INSERT INTO `sys_file` VALUES ('130', 'report-back.form', '809', 'application/octet-stream', 'd:\\lxcloud\\upload\\130-report-back.form', '1');
INSERT INTO `sys_file` VALUES ('131', '微信图片_20180130141255.png', '92248', 'image/png', 'd:\\lxcloud\\upload\\131-微信图片_20180130141255.png', '1');
INSERT INTO `sys_file` VALUES ('132', '机动车牌证申请表(1).xls', '33792', 'application/vnd.ms-excel', 'd:\\lxcloud\\upload\\132-机动车牌证申请表(1).xls', '1');
INSERT INTO `sys_file` VALUES ('133', '截图 2018-04-23 11.49.53 (1).png', '103569', 'image/png', 'd:\\lxcloud\\upload\\133-截图 2018-04-23 11.49.53 (1).png', '1');
INSERT INTO `sys_file` VALUES ('134', '截图 2018-04-23 11.49.53 (1).png', '103569', 'image/png', 'd:\\lxcloud\\upload\\134-截图 2018-04-23 11.49.53 (1).png', '1');
INSERT INTO `sys_file` VALUES ('135', '截图 2018-04-23 11.49.53.png', '103569', 'image/png', 'd:\\lxcloud\\upload\\135-截图 2018-04-23 11.49.53.png', '1');

-- ----------------------------
-- Table structure for sys_login_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_login_log`;
CREATE TABLE `sys_login_log` (
  `login_id` int(12) NOT NULL,
  `login_user` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `user_id` int(20) DEFAULT NULL,
  `user_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `login_address` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `login_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `logout_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `login_flag` int(22) DEFAULT NULL,
  `logout_flag` int(22) DEFAULT NULL,
  `remark` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `valid_flag` int(11) DEFAULT '1',
  PRIMARY KEY (`login_id`),
  KEY `idx_sys_login_log_date` (`login_date`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='登录历史表';

-- ----------------------------
-- Records of sys_login_log
-- ----------------------------
INSERT INTO `sys_login_log` VALUES ('406', 'admin ', '0', '系统管理员', '0:0:0:0:0:0:0:1', '2018-03-15 23:26:08', '0000-00-00 00:00:00', '1', null, null, '1');
INSERT INTO `sys_login_log` VALUES ('407', 'admin ', '0', '系统管理员', '0:0:0:0:0:0:0:1', '2018-03-15 23:34:29', '0000-00-00 00:00:00', '1', null, null, '1');
INSERT INTO `sys_login_log` VALUES ('408', 'admin ', '0', '系统管理员', '0:0:0:0:0:0:0:1', '2018-03-16 00:16:48', '0000-00-00 00:00:00', '1', null, null, '1');
INSERT INTO `sys_login_log` VALUES ('409', 'admin ', '0', '系统管理员', '0:0:0:0:0:0:0:1', '2018-03-16 00:16:53', '0000-00-00 00:00:00', '1', null, null, '1');
INSERT INTO `sys_login_log` VALUES ('410', 'admin ', '0', '系统管理员', '0:0:0:0:0:0:0:1', '2018-03-16 00:17:40', '0000-00-00 00:00:00', '1', null, null, '1');
INSERT INTO `sys_login_log` VALUES ('411', 'admin ', '0', '系统管理员', '0:0:0:0:0:0:0:1', '2018-03-16 00:17:55', '0000-00-00 00:00:00', '1', null, null, '1');
INSERT INTO `sys_login_log` VALUES ('412', 'admin ', '0', '系统管理员', '0:0:0:0:0:0:0:1', '2018-03-16 00:18:28', '0000-00-00 00:00:00', '1', null, null, '1');
INSERT INTO `sys_login_log` VALUES ('413', 'admin ', '0', '系统管理员', '0:0:0:0:0:0:0:1', '2018-03-16 00:18:31', '0000-00-00 00:00:00', '1', null, null, '1');
INSERT INTO `sys_login_log` VALUES ('414', 'admin ', '0', '系统管理员', '0:0:0:0:0:0:0:1', '2018-03-16 00:19:27', '0000-00-00 00:00:00', '1', null, null, '1');
INSERT INTO `sys_login_log` VALUES ('415', 'admin ', '0', '系统管理员', '0:0:0:0:0:0:0:1', '2018-03-16 00:22:07', '0000-00-00 00:00:00', '1', null, null, '1');
INSERT INTO `sys_login_log` VALUES ('416', 'admin ', '0', '系统管理员', '0:0:0:0:0:0:0:1', '2018-03-16 00:22:49', '0000-00-00 00:00:00', '1', null, null, '1');
INSERT INTO `sys_login_log` VALUES ('417', 'admin ', '0', '系统管理员', '0:0:0:0:0:0:0:1', '2018-03-16 00:22:57', '0000-00-00 00:00:00', '1', null, null, '1');
INSERT INTO `sys_login_log` VALUES ('418', 'admin ', '0', '系统管理员', '0:0:0:0:0:0:0:1', '2018-03-16 09:23:52', '0000-00-00 00:00:00', '1', null, null, '1');
INSERT INTO `sys_login_log` VALUES ('419', 'admin ', '0', '系统管理员', '0:0:0:0:0:0:0:1', '2018-03-16 14:11:19', '0000-00-00 00:00:00', '1', null, null, '1');
INSERT INTO `sys_login_log` VALUES ('420', 'admin ', '0', '系统管理员', '0:0:0:0:0:0:0:1', '2018-03-16 14:22:39', '0000-00-00 00:00:00', '1', null, null, '1');
INSERT INTO `sys_login_log` VALUES ('421', 'admin ', '0', '系统管理员', '0:0:0:0:0:0:0:1', '2018-03-16 15:17:19', '0000-00-00 00:00:00', '1', null, null, '1');
INSERT INTO `sys_login_log` VALUES ('422', 'admin ', '0', '系统管理员', '0:0:0:0:0:0:0:1', '2018-03-16 15:18:38', '0000-00-00 00:00:00', '1', null, null, '1');
INSERT INTO `sys_login_log` VALUES ('423', 'admin ', '0', '系统管理员', '0:0:0:0:0:0:0:1', '2018-03-16 15:21:26', '0000-00-00 00:00:00', '1', null, null, '1');
INSERT INTO `sys_login_log` VALUES ('424', 'admin ', '0', '系统管理员', '0:0:0:0:0:0:0:1', '2018-04-03 16:32:42', '0000-00-00 00:00:00', '1', null, null, '1');
INSERT INTO `sys_login_log` VALUES ('425', 'admin ', '0', '系统管理员', '0:0:0:0:0:0:0:1', '2018-04-03 16:33:54', '0000-00-00 00:00:00', '1', null, null, '1');
INSERT INTO `sys_login_log` VALUES ('426', 'admin ', '0', '系统管理员', '0:0:0:0:0:0:0:1', '2018-04-04 10:49:09', '0000-00-00 00:00:00', '1', null, null, '1');
INSERT INTO `sys_login_log` VALUES ('427', 'admin ', '0', '系统管理员', '0:0:0:0:0:0:0:1', '2018-04-04 11:23:45', '0000-00-00 00:00:00', '1', null, null, '1');
INSERT INTO `sys_login_log` VALUES ('428', 'admin ', '0', '系统管理员', '0:0:0:0:0:0:0:1', '2018-05-11 16:47:28', '0000-00-00 00:00:00', '1', null, null, '1');
INSERT INTO `sys_login_log` VALUES ('429', 'admin ', '0', '系统管理员', '0:0:0:0:0:0:0:1', '2018-05-16 09:13:21', '0000-00-00 00:00:00', '1', null, null, '1');
INSERT INTO `sys_login_log` VALUES ('430', 'admin ', '0', '系统管理员', '0:0:0:0:0:0:0:1', '2018-05-16 09:17:51', '0000-00-00 00:00:00', '1', null, null, '1');
INSERT INTO `sys_login_log` VALUES ('431', 'admin ', '0', '系统管理员', '0:0:0:0:0:0:0:1', '2018-05-16 09:21:49', '0000-00-00 00:00:00', '1', null, null, '1');
INSERT INTO `sys_login_log` VALUES ('432', 'admin ', '0', '系统管理员', '0:0:0:0:0:0:0:1', '2018-05-16 09:21:50', '0000-00-00 00:00:00', '1', null, null, '1');
INSERT INTO `sys_login_log` VALUES ('433', 'admin ', '0', '系统管理员', '0:0:0:0:0:0:0:1', '2018-05-16 09:41:25', '0000-00-00 00:00:00', '1', null, null, '1');
INSERT INTO `sys_login_log` VALUES ('434', 'admin ', '0', '系统管理员', '0:0:0:0:0:0:0:1', '2018-05-16 09:43:40', '0000-00-00 00:00:00', '1', null, null, '1');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` int(12) NOT NULL,
  `menu_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `menu_desc` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `menu_up_id` int(10) NOT NULL,
  `menu_url` varchar(200) DEFAULT NULL,
  `menu_order` int(6) NOT NULL DEFAULT '0',
  `permission_code` varchar(50) DEFAULT NULL,
  `pic_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `menu_type` int(1) NOT NULL DEFAULT '1',
  `menu_visiable` int(1) DEFAULT NULL COMMENT '菜单可见性 0默认都显示 1仅管理可见 2 仅员工可见',
  `log_flag` int(1) DEFAULT '0',
  `valid_flag` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`menu_id`),
  KEY `idx_sys_menu_up` (`menu_up_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '编辑', null, '2458', '', '0', 'MemBaseinfo:update', '', '2', '0', '1', '1');
INSERT INTO `sys_menu` VALUES ('2', 'Ueditor', null, '90', '/views/sample/ueditor-um', '0', '', '', '1', '1', '1', '1');
INSERT INTO `sys_menu` VALUES ('3', 'excel操作', null, '90', '', '1', '', '', '1', '0', '1', '0');
INSERT INTO `sys_menu` VALUES ('6', '部门管理', null, '99', '/views/jsp/system/deptManager', '15', '', '', '1', '1', '1', '1');
INSERT INTO `sys_menu` VALUES ('7', '日志管理', null, '99', null, '0', null, null, '1', null, '0', '0');
INSERT INTO `sys_menu` VALUES ('24', 'layui-列表组件', null, '26', '/views/sample/datagrid-layui', '1', '', '', '1', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('25', 'layui-表单组件', null, '26', '/views/sample/form-layui', '1', '', '', '1', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('26', 'layui', null, '0', '', '1', '', '', '1', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('27', 'layui-按钮', null, '26', '/views/sample/button-layui', '1', '', '', '1', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('28', 'layui-时间日期', null, '26', '/views/sample/date-layui', '1', '', '', '1', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('29', 'layui-示例', null, '26', '/views/sample/sample-layui', '1', '', '', '1', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('30', '列表示例', null, '90', '/views/sample/si_list', '0', '', '', '1', null, '1', '1');
INSERT INTO `sys_menu` VALUES ('31', '表单示例', null, '90', '/views/sample/si_formcomp', '0', '', '', '1', null, '1', '1');
INSERT INTO `sys_menu` VALUES ('32', '方法', null, '90', 'sss', '1', 's', '', '1', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('90', '开发示例', null, '0', '', '100', '', '&#xe6a6;', '1', '1', '0', '1');
INSERT INTO `sys_menu` VALUES ('99', '系统管理', null, '0', '', '99', '', '&#xe6f5;', '1', '1', '0', '1');
INSERT INTO `sys_menu` VALUES ('221', '重置密码', null, '99', '/views/jsp/system/pwdManager', '14', '', '', '1', '1', '0', '1');
INSERT INTO `sys_menu` VALUES ('230', '菜单管理', null, '99', '/views/jsp/system/menuManager', '11', '', '', '1', '1', '0', '1');
INSERT INTO `sys_menu` VALUES ('237', '角色管理', null, '99', '/views/jsp/system/roleManager', '12', '', '', '1', '1', '0', '1');
INSERT INTO `sys_menu` VALUES ('245', '登录日志管理', null, '99', '/views/jsp/system/logLogin', '98', '', '', '1', '1', '0', '1');
INSERT INTO `sys_menu` VALUES ('247', '用户管理', null, '99', '/views/jsp/system/staffManager', '13', '', '', '1', '1', '0', '1');
INSERT INTO `sys_menu` VALUES ('1222', '公告管理', null, '99', '/views/jsp/system/bulletinManager', '5', '', '', '1', '0', '0', '1');
INSERT INTO `sys_menu` VALUES ('2016', '表单组件', null, '90', '/views/sample/formcomponent', '0', null, '', '1', '1', '0', '0');
INSERT INTO `sys_menu` VALUES ('2017', '列表组件', null, '90', '/views/sample/datagrid', '0', null, '', '1', '1', '0', '0');
INSERT INTO `sys_menu` VALUES ('2018', '弹框组件', null, '90', '/views/sample/openwindow', '0', null, '', '1', '1', '0', '1');
INSERT INTO `sys_menu` VALUES ('2020', '百度地图', null, '90', '/views/sample/baidumap', '0', null, '', '1', '1', '0', '0');
INSERT INTO `sys_menu` VALUES ('2024', 'tab组件', null, '90', '/views/sample/tabs', '0', null, '', '1', '1', '0', '0');
INSERT INTO `sys_menu` VALUES ('2025', '图表组件', null, '90', '/views/sample/highcharts', '0', null, '', '1', '1', '0', '1');
INSERT INTO `sys_menu` VALUES ('2109', '系统参数管理', null, '99', '/views/jsp/system/paramManager', '0', '', '', '1', '1', '1', '1');
INSERT INTO `sys_menu` VALUES ('2110', '字典信息管理', null, '99', '/views/jsp/system/codeManager', '1', '', '', '1', '1', '1', '1');
INSERT INTO `sys_menu` VALUES ('2134', '缓存管理', null, '99', '/views/jsp/system/cacheManager', '2', '', '', '1', '1', '1', '1');
INSERT INTO `sys_menu` VALUES ('2409', '操作日志查询', null, '99', '/views/jsp/system/logOperate', '99', '', '', '1', '1', '1', '1');
INSERT INTO `sys_menu` VALUES ('2458', '会员列表(开发实例)', null, '90', '/views/sample/member/MemBaseinfoManage.', '0', '', '', '1', '0', '1', '1');
INSERT INTO `sys_menu` VALUES ('2461', '图片上传', null, '90', '/views/sample/webuploader', '0', '', '', '1', '1', '1', '0');
INSERT INTO `sys_menu` VALUES ('2470', '接口测试', null, '90', '/views/sample/datainte', '0', null, '', '1', '1', '1', '0');
INSERT INTO `sys_menu` VALUES ('2471', '示例1', null, '2474', '/views/sample/ppp/changepwd', '0', null, '', '1', '1', '1', '0');
INSERT INTO `sys_menu` VALUES ('2472', '示例2', null, '2474', '/views/sample/ppp/myloginfo', '0', null, '', '1', '1', '1', '0');
INSERT INTO `sys_menu` VALUES ('2473', '示例3', null, '2474', '/views/sample/ppp/personInfo', '0', null, '', '1', '1', '1', '0');
INSERT INTO `sys_menu` VALUES ('2474', '示例界面', null, '90', '/views/sample/ppp/myloginfo', '0', null, '', '1', '1', '1', '0');

-- ----------------------------
-- Table structure for sys_operate_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_operate_log`;
CREATE TABLE `sys_operate_log` (
  `operate_id` int(12) NOT NULL,
  `login_id` int(12) DEFAULT NULL,
  `login_user` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `user_id` int(20) DEFAULT NULL,
  `user_kind` int(1) DEFAULT NULL,
  `user_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `login_address` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `operate_url` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `operate_name` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `operate_class` varchar(300) DEFAULT NULL,
  `operate_method` varchar(300) DEFAULT NULL,
  `operate_param` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `remark` varchar(5000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `valid_flag` int(11) DEFAULT '1',
  PRIMARY KEY (`operate_id`),
  KEY `idx_sys_operate_log_date` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='操作记录表';

-- ----------------------------
-- Records of sys_operate_log
-- ----------------------------
INSERT INTO `sys_operate_log` VALUES ('531', null, 'admin ', '0', null, '系统管理员', null, null, '', null, null, null, '2018-03-16 15:00:44', 'Software caused connection abort: socket write error', '1');
INSERT INTO `sys_operate_log` VALUES ('532', null, 'admin ', '0', null, '系统管理员', null, null, '', null, null, null, '2018-03-16 15:00:44', 'Software caused connection abort: socket write error', '1');
INSERT INTO `sys_operate_log` VALUES ('533', null, 'admin ', '0', null, '系统管理员', null, null, '', null, null, null, '2018-03-16 15:02:18', 'Software caused connection abort: socket write error', '1');
INSERT INTO `sys_operate_log` VALUES ('534', null, 'admin ', '0', null, '系统管理员', null, null, '', null, null, null, '2018-03-16 15:02:18', 'Connection reset by peer: socket write error', '1');
INSERT INTO `sys_operate_log` VALUES ('535', null, 'admin ', '0', null, '系统管理员', null, null, '', null, null, null, '2018-03-16 15:02:18', 'Software caused connection abort: socket write error', '1');
INSERT INTO `sys_operate_log` VALUES ('536', null, 'admin ', '0', null, '系统管理员', null, null, '', null, null, null, '2018-04-04 11:23:42', '密码不正确', '1');

-- ----------------------------
-- Table structure for sys_parameter
-- ----------------------------
DROP TABLE IF EXISTS `sys_parameter`;
CREATE TABLE `sys_parameter` (
  `code` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `value` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `remark` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `valid_flag` int(11) DEFAULT '1',
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='参数表';

-- ----------------------------
-- Records of sys_parameter
-- ----------------------------
INSERT INTO `sys_parameter` VALUES ('application_name', 'TD开发平台', '', '1');
INSERT INTO `sys_parameter` VALUES ('application_version', 'V1.0.0.01', '', '1');
INSERT INTO `sys_parameter` VALUES ('checkcode_flag', '0', '', '1');
INSERT INTO `sys_parameter` VALUES ('company_name', 'TD有限公司', null, '1');
INSERT INTO `sys_parameter` VALUES ('default_exit_noin_time', '60', null, '1');
INSERT INTO `sys_parameter` VALUES ('default_password', 'aaaa0000', null, '1');
INSERT INTO `sys_parameter` VALUES ('file_upload_path', 'd:/lxcloud/upload', null, '1');
INSERT INTO `sys_parameter` VALUES ('mail_address', null, null, '1');
INSERT INTO `sys_parameter` VALUES ('mail_debug', null, null, '1');
INSERT INTO `sys_parameter` VALUES ('mail_username', null, null, '1');
INSERT INTO `sys_parameter` VALUES ('mailsupport', '0', null, '1');
INSERT INTO `sys_parameter` VALUES ('sms_accountSid', '59cba09df3c8d07429c08f0799442e93', null, '1');
INSERT INTO `sys_parameter` VALUES ('sms_appId', 'caa89d9bf1654f02829f10524e2db7b0', null, '1');
INSERT INTO `sys_parameter` VALUES ('sms_authToken', '544d396af7190a27786d16e5e054c520', null, '1');
INSERT INTO `sys_parameter` VALUES ('sms_checkcode_templateId', '27748', '验证码模板id', '1');
INSERT INTO `sys_parameter` VALUES ('sms_checkcode_timeout', '60', '短信验证码失效时间(分钟)', '1');
INSERT INTO `sys_parameter` VALUES ('sms_http_ssl_ip', '0', null, '1');
INSERT INTO `sys_parameter` VALUES ('sms_http_ssl_port', '443', null, '1');
INSERT INTO `sys_parameter` VALUES ('sms_send_interval', '60', '发送短信验证码间隔', '1');
INSERT INTO `sys_parameter` VALUES ('sms_server', 'api.ucpaas.com', null, '1');
INSERT INTO `sys_parameter` VALUES ('sms_version', '2014-06-30', null, '1');
INSERT INTO `sys_parameter` VALUES ('ss', 'ss', 'aa', '0');
INSERT INTO `sys_parameter` VALUES ('wx_api_secretkey', '6cFPsrZriuTVK2sNWDqUyogJtkML6y1H', null, '1');
INSERT INTO `sys_parameter` VALUES ('wx_app_secretkey', '6cFPsrZriuTVK2sNWDqUyogJtkML6y1H', null, '1');
INSERT INTO `sys_parameter` VALUES ('wx_appid', 'wxf6a19f140f3e9144', null, '1');
INSERT INTO `sys_parameter` VALUES ('wx_mch_id', '1304976901', null, '1');
INSERT INTO `sys_parameter` VALUES ('wxurl_closeorder', 'https://api.mch.weixin.qq.com/pay/closeorder', null, '1');
INSERT INTO `sys_parameter` VALUES ('wxurl_orderquery', 'https://api.mch.weixin.qq.com/pay/orderquery', null, '1');
INSERT INTO `sys_parameter` VALUES ('wxurl_refund', 'https://api.mch.weixin.qq.com/secapi/pay/refund', null, '1');
INSERT INTO `sys_parameter` VALUES ('wxurl_refundquery', 'https://api.mch.weixin.qq.com/pay/refundquery', null, '1');
INSERT INTO `sys_parameter` VALUES ('wxurl_unifiedorder', 'https://api.mch.weixin.qq.com/pay/unifiedorder', null, '1');

-- ----------------------------
-- Table structure for sys_roles
-- ----------------------------
DROP TABLE IF EXISTS `sys_roles`;
CREATE TABLE `sys_roles` (
  `role_id` int(10) NOT NULL,
  `role_code` varchar(50) DEFAULT NULL,
  `role_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `role_desc` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `dis_order` int(6) NOT NULL DEFAULT '0',
  `user_kind` int(11) DEFAULT NULL,
  `is_delete` int(11) DEFAULT '0',
  `valid_flag` int(11) DEFAULT '1',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of sys_roles
-- ----------------------------
INSERT INTO `sys_roles` VALUES ('1', 'super', '平台超级管理员', '', '0', '9', '1', '1', null);
INSERT INTO `sys_roles` VALUES ('4', 'test', 'test', '', '0', '8', '0', '0', '2018-03-13 15:41:52');
INSERT INTO `sys_roles` VALUES ('5', 'ff', 'fff', '', '0', '9', '0', '0', '2018-03-13 15:42:47');
INSERT INTO `sys_roles` VALUES ('6', 'ff', 'ff', '', '0', '7', '0', '0', '2018-03-13 15:50:42');
INSERT INTO `sys_roles` VALUES ('7', 'admin', '普通管理员', '', '0', '9', '0', '1', '2018-03-14 16:38:48');
INSERT INTO `sys_roles` VALUES ('8', '', '中心主管', '', '0', '8', '0', '1', '2018-03-14 16:58:08');
INSERT INTO `sys_roles` VALUES ('9', '', '中心职工', '', '0', '8', '0', '1', '2018-03-14 16:58:19');
INSERT INTO `sys_roles` VALUES ('10', '', '机构主管', '', '0', '7', '0', '1', '2018-03-14 16:58:40');
INSERT INTO `sys_roles` VALUES ('11', '', '机构职工', '', '0', '7', '0', '1', '2018-03-14 16:58:47');

-- ----------------------------
-- Table structure for sys_role_menus
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menus`;
CREATE TABLE `sys_role_menus` (
  `role_id` int(10) NOT NULL,
  `menu_id` int(10) NOT NULL,
  `grant_option` int(1) NOT NULL DEFAULT '0',
  `valid_flag` int(11) DEFAULT '1',
  `jsid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色菜单关系表';

-- ----------------------------
-- Records of sys_role_menus
-- ----------------------------
INSERT INTO `sys_role_menus` VALUES ('3', '0', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('3', '90', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('3', '2458', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('3', '1', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('2', '90', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('2', '2458', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('6', '0', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('6', '90', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('6', '2', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('6', '2016', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('6', '2017', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('6', '2018', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('6', '2020', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('6', '2024', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('6', '2025', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('6', '2458', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('6', '1', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('6', '2461', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('6', '2470', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('6', '2474', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('6', '2471', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('6', '2472', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('6', '2473', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('6', '3', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('6', '30', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('6', '31', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('4', '0', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('4', '99', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('4', '2109', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('4', '2110', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('4', '2134', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('4', '1222', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('4', '230', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('4', '237', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('4', '247', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('4', '221', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('4', '6', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('4', '245', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('4', '2409', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('7', '0', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('7', '99', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('7', '2109', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('7', '2110', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('7', '2134', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('7', '1222', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('7', '230', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('7', '237', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('7', '247', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('7', '221', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('7', '6', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('7', '245', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('7', '2409', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('1', '99', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('1', '2109', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('1', '2110', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('1', '2134', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('1', '1222', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('1', '230', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('1', '237', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('1', '247', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('1', '221', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('1', '6', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('1', '245', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('1', '2409', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('1', '90', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('1', '2018', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('1', '2020', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('1', '2025', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('1', '2458', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('1', '1', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('1', '2461', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('1', '2470', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('1', '3', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('1', '30', '0', '1', null);
INSERT INTO `sys_role_menus` VALUES ('1', '31', '0', '1', null);

-- ----------------------------
-- Table structure for sys_serial
-- ----------------------------
DROP TABLE IF EXISTS `sys_serial`;
CREATE TABLE `sys_serial` (
  `serial_type` varchar(30) NOT NULL,
  `serial_desc` varchar(50) DEFAULT NULL,
  `init_str_no` varchar(10) DEFAULT NULL,
  `init_num_no` int(12) DEFAULT NULL,
  `prefix_str` varchar(10) DEFAULT NULL,
  `prefix_date` varchar(10) DEFAULT NULL,
  `cur_str_no` varchar(10) DEFAULT NULL,
  `cur_num_no` int(12) DEFAULT NULL,
  `valid_flag` int(11) DEFAULT '1',
  PRIMARY KEY (`serial_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='序列号表';

-- ----------------------------
-- Records of sys_serial
-- ----------------------------
INSERT INTO `sys_serial` VALUES ('bulletin_id', null, null, null, null, null, null, '4', '1');
INSERT INTO `sys_serial` VALUES ('dept_id', null, null, null, null, null, null, '10', '1');
INSERT INTO `sys_serial` VALUES ('emp_dept_id', null, null, null, null, null, null, '5', '1');
INSERT INTO `sys_serial` VALUES ('file_file_id', null, null, null, null, null, null, '135', '1');
INSERT INTO `sys_serial` VALUES ('login_id', null, null, null, null, null, null, '434', '1');
INSERT INTO `sys_serial` VALUES ('matyp_id', null, null, null, null, null, null, '1', '1');
INSERT INTO `sys_serial` VALUES ('memid', null, null, null, null, null, null, '103', '1');
INSERT INTO `sys_serial` VALUES ('menu_id', null, null, null, null, null, null, '32', '1');
INSERT INTO `sys_serial` VALUES ('mserial_id', null, null, null, null, null, null, '29', '1');
INSERT INTO `sys_serial` VALUES ('mtype_id', null, null, null, null, null, null, '32', '1');
INSERT INTO `sys_serial` VALUES ('operate_id', null, null, null, null, null, null, '536', '1');
INSERT INTO `sys_serial` VALUES ('role_id', null, null, null, null, null, null, '11', '1');
INSERT INTO `sys_serial` VALUES ('staff_id', null, null, null, null, null, null, '3', '1');
INSERT INTO `sys_serial` VALUES ('stuff_catagory_id', null, null, null, null, null, null, '10', '1');
INSERT INTO `sys_serial` VALUES ('stuff_id', null, null, null, null, null, null, '76', '1');
INSERT INTO `sys_serial` VALUES ('user_id', null, null, null, null, null, null, '3', '1');

-- ----------------------------
-- Table structure for sys_sqls
-- ----------------------------
DROP TABLE IF EXISTS `sys_sqls`;
CREATE TABLE `sys_sqls` (
  `sql_id` int(10) NOT NULL DEFAULT '0',
  `sql_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `sql_text` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `sql_desc` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `remark` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `last_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `valid_flag` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`sql_id`,`sql_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='sql配置表';

-- ----------------------------
-- Records of sys_sqls
-- ----------------------------

-- ----------------------------
-- Table structure for sys_staff
-- ----------------------------
DROP TABLE IF EXISTS `sys_staff`;
CREATE TABLE `sys_staff` (
  `staff_id` int(6) NOT NULL,
  `login_user` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `staff_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `user_kind` int(11) DEFAULT NULL,
  `staff_sta` int(1) NOT NULL DEFAULT '1',
  `dept_id` int(6) DEFAULT NULL,
  `dis_order` int(6) NOT NULL DEFAULT '0',
  `sex` int(1) DEFAULT NULL,
  `idcardno` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `birthdate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `nationality` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `position` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `address` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `postal_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `mobile_phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `home_phone` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `office_phone` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `qq` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `remark` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `valid_flag` int(11) DEFAULT '1',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`staff_id`),
  UNIQUE KEY `idx_login_user` (`login_user`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='员工表';

-- ----------------------------
-- Records of sys_staff
-- ----------------------------
INSERT INTO `sys_staff` VALUES ('0', 'admin', '系统管理员', '9', '1', '0', '0', null, null, '2015-09-14 00:08:23', null, null, null, null, null, null, null, '', null, null, '1', null);
INSERT INTO `sys_staff` VALUES ('1', 'zhangsan', 'zhangsan', '9', '1', '6', '0', null, null, '0000-00-00 00:00:00', null, null, null, null, null, null, null, null, null, null, '1', '2017-06-27 15:44:46');
INSERT INTO `sys_staff` VALUES ('2', 'lisi', 'lisi', '9', '1', '1', '0', null, null, '0000-00-00 00:00:00', null, null, null, null, null, null, null, null, null, null, '1', '2017-06-27 15:44:59');
INSERT INTO `sys_staff` VALUES ('3', '', '', '8', '1', null, '0', null, null, '0000-00-00 00:00:00', null, null, null, null, null, null, null, null, null, null, '0', '2018-03-14 16:35:14');

-- ----------------------------
-- Table structure for sys_system_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_system_log`;
CREATE TABLE `sys_system_log` (
  `log_sn` int(50) NOT NULL,
  `server_name` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `log_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `log_level` varchar(20) CHARACTER SET utf8 NOT NULL,
  `log_name` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `log_message` longtext CHARACTER SET utf8,
  `log_exception` longtext CHARACTER SET utf8,
  `login_user` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `user_address` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `operate_action` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `operate_name` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `biz_id` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `valid_flag` int(11) DEFAULT '1',
  `jsid` int(11) DEFAULT NULL,
  PRIMARY KEY (`log_sn`),
  KEY `idx_sys_system_log_date` (`log_date`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sys_system_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` int(20) NOT NULL,
  `user_kind` int(1) NOT NULL,
  `user_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `login_user` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_logintime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_logouttime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lock_state` varchar(1) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '1',
  `lock_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lock_reason` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `password_errorcount` int(6) NOT NULL DEFAULT '0',
  `valid_flag` int(11) DEFAULT '1',
  PRIMARY KEY (`user_id`,`user_kind`),
  KEY `idx_sys_user_loginuser` (`login_user`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户登录信息表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('0', '9', '系统管理员', 'e10adc3949ba59abbe56e057f20f883e', 'admin ', '2015-08-19 12:34:03', '2015-08-26 10:43:02', '2015-08-26 10:43:02', '1', '2015-08-26 10:43:02', null, '0', '1');
INSERT INTO `sys_user` VALUES ('1', '9', 'zhangsan', 'A07B21DD84272ADB9652A7978BAC6706', 'zhangsan', '2017-06-27 15:44:46', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1', '0000-00-00 00:00:00', null, '0', '1');
INSERT INTO `sys_user` VALUES ('2', '9', 'lisi', 'A07B21DD84272ADB9652A7978BAC6706', 'lisi', '2017-06-27 15:44:59', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1', '0000-00-00 00:00:00', null, '0', '1');
INSERT INTO `sys_user` VALUES ('3', '8', '', 'A07B21DD84272ADB9652A7978BAC6706', '', '2018-03-14 16:35:14', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1', '0000-00-00 00:00:00', null, '0', '0');

-- ----------------------------
-- Table structure for sys_user_kind
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_kind`;
CREATE TABLE `sys_user_kind` (
  `user_kind` int(1) NOT NULL,
  `kind_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `kind_desc` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `dis_order` int(4) NOT NULL DEFAULT '0',
  `login_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `password_tip` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `login_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `login_sql` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `find_sql` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `query_sql` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `param` varchar(100) DEFAULT NULL,
  `role_id` int(10) DEFAULT NULL,
  `grade_id` int(1) NOT NULL DEFAULT '1',
  `valid_flag` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_kind`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户类型表';

-- ----------------------------
-- Records of sys_user_kind
-- ----------------------------
INSERT INTO `sys_user_kind` VALUES ('7', '机构用户', '中心用户', '7', '登录名', '123456', 'login_user', 'select   * from sys_staff where  user_kind=\'7\' and  login_user = ?  and valid_flag=\'1\'', null, null, 'login_user', '0', '1', '1');
INSERT INTO `sys_user_kind` VALUES ('8', '中心用户', '中心用户', '8', '登录名', '123456', 'login_user', 'select   * from sys_staff where  user_kind=\'8\' and  login_user = ?  and valid_flag=\'1\'', null, null, 'login_user', '0', '1', '1');
INSERT INTO `sys_user_kind` VALUES ('9', '系统用户', '系统用户', '9', '登录名', '123456', 'login_user', 'select   * from sys_staff where  user_kind=\'9\' and  login_user = ?  and valid_flag=\'1\'', '', '', 'login_user', '0', '3', '1');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `user_id` int(20) NOT NULL,
  `role_id` int(10) NOT NULL DEFAULT '0',
  `valid_flag` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户角色关系表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('0', '1', '1');
INSERT INTO `sys_user_role` VALUES ('2', '1', '1');
INSERT INTO `sys_user_role` VALUES ('2', '4', '1');
INSERT INTO `sys_user_role` VALUES ('1', '4', '1');

-- ----------------------------
-- Function structure for get_address_detail
-- ----------------------------
DROP FUNCTION IF EXISTS `get_address_detail`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `get_address_detail`(v_mdid integer) RETURNS text CHARSET utf8
BEGIN
	#Routine body goes here...

  DECLARE detail text default '';
  DECLARE t_province varchar(10);
	DECLARE t_city varchar(10);
	DECLARE t_country varchar(10);

	DECLARE t_detail text;
	DECLARE t_phone text;
	DECLARE t_getor text;

  select  province,country,md_getor,md_phone,md_detail,city  

  from zhengshi_zmdhdb.hy_mem_address where mdid=v_mdid

  into t_province,t_country,t_getor,t_phone,t_detail,t_city;

  SELECT  area_name from sys_area  where area_code= t_province into t_province;
  SELECT  area_name from sys_area  where area_code= t_city into t_city;
  SELECT  area_name from sys_area  where area_code= t_country into t_country;

  SELECT concat(t_getor,',',t_phone,',',t_province,t_city,t_country,t_detail) into detail;

	RETURN detail;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for get_code_display
-- ----------------------------
DROP FUNCTION IF EXISTS `get_code_display`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `get_code_display`(v_code varchar(100), v_value varchar(100)) RETURNS varchar(200) CHARSET utf8
BEGIN
	 
  DECLARE v_display varchar(200) default '';
  
  select t.display_value from sys_code_table_detail t where t.code_type=v_code and t.data_value=v_value into v_display;

	RETURN v_display;
 
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for get_staff_name
-- ----------------------------
DROP FUNCTION IF EXISTS `get_staff_name`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `get_staff_name`(v_staffid int) RETURNS varchar(200) CHARSET utf8
begin 

declare v_staff_name varchar(200) default '';

select  staff_name from sys_staff  where staff_id=v_staffid  into  v_staff_name; 

return v_staff_name; 

end
;;
DELIMITER ;
