package com.tdcy.rest.sys;

/*import io.swagger.annotations.Api;
 import io.swagger.annotations.ApiOperation;*/

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tdcy.framework.exception.QuickuException;
import com.tdcy.framework.util.RestUtils;
import com.tdcy.framework.util.StringUtils;
import com.tdcy.sys.dao.IAreaDAO;
import com.tdcy.sys.dao.eo.AreaEO;
import com.tdcy.sys.service.IAreaSvc;
import com.tdcy.sys.util.AreaUtils;

@Controller
public class AreaRest {

	@Resource
	IAreaSvc areaSvc;

	@Resource
	IAreaDAO areaDAO;

	@ResponseBody
	@RequestMapping(value = "/getOpenCityList", method = RequestMethod.POST)
	public Map getOpenCityList(HttpServletRequest request) throws IOException {
		List<AreaEO> elist = areaDAO.findTableRecords(AreaEO.class);
		List<AreaEO> alist = getAreaList(elist, "0");

		List<AreaEO> nlist = new ArrayList<AreaEO>();
		List<AreaEO> plist = null;
		for (AreaEO ao : alist) {
			plist = getAreaList(elist, ao.getAreaCode() + "");

			for (AreaEO eo : plist) {
				if (eo.getOpenflag() != null && eo.getOpenflag().compareTo(1) == 0) {
					nlist.add(eo);
				}
			}
		}

		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

		for (AreaEO aeo : nlist) {
			String firstspell = aeo.getPinyin().substring(0, 1);
			Map<String, Object> m = findTagMap(list, firstspell);
			if (m == null) {
				m = new HashMap<String, Object>();
				m.put("tag", firstspell);
				m.put("cities", new ArrayList<Map<String, String>>());
				list.add(m);
			}

			List<Map<String, String>> vlist = (List<Map<String, String>>) m.get("cities");

			Map<String, String> mm = new HashMap<String, String>();
			mm.put("code", aeo.getAreaCode() + "");
			mm.put("name", aeo.getAreaName());
			mm.put("pinyin", aeo.getPinyin());
			vlist.add(mm);
		}
		
		for(Map<String,Object> m :list){
			ArrayList<Map<String, String>> cities = (ArrayList<Map<String, String>>) m.get("cities");
			
			Collections.sort(cities, new Comparator<Map<String, String>>() {
				@Override
				public int compare(Map<String, String> o1, Map<String, String> o2) {
					String pinyin1 = (String) o1.get("pinyin");
					String pinyin2 = (String) o2.get("pinyin");
					return pinyin1.compareTo(pinyin2);
				}
			});

		}

		Collections.sort(list, new Comparator<Map<String, Object>>() {
			@Override
			public int compare(Map<String, Object> o1, Map<String, Object> o2) {
				String tag1 = (String) o1.get("tag");
				String tag2 = (String) o2.get("tag");
				return tag1.compareTo(tag2);
			}
		});

		Map<String, List<Map<String, Object>>> retmap = new HashMap<String, List<Map<String, Object>>>();
		retmap.put("lists", list);
		return RestUtils.createJSONSuccess("添加成功", retmap);
	}

	public static List<AreaEO> getAreaList(List<AreaEO> areaList, String cityId) {
		List<AreaEO> retList = new ArrayList<AreaEO>();
		Integer ciryid = Integer.parseInt(cityId);
		for (AreaEO ae : areaList) {
			if (ae.getAreaUpCode().compareTo(ciryid) == 0) {
				retList.add(ae);
			}
		}
		return retList;
	}

	private Map<String, Object> findTagMap(List<Map<String, Object>> list, String firstspell) {
		for (Map<String, Object> m : list) {
			String tag = (String) m.get("tag");
			if (tag.equals(firstspell)) {
				return m;
			}
		}

		return null;
	}

	@ResponseBody
	@RequestMapping(value = "/getCityAreaList", method = RequestMethod.POST)
	public Map getCityAreaList(HttpServletRequest request) throws IOException {
		String level = request.getParameter("level");
		if (StringUtils.isEmpty(level)) {
			throw new QuickuException("-1", "请输入level参数");
		}

		String code = request.getParameter("parentcode");

		List<AreaEO> alist = null;
		if ("1".equals(level)) {
			alist = AreaUtils.getAreaListByCityId("0");
		} else if ("2".equals(level)) {
			alist = AreaUtils.getAreaListByCityId(code);
		} else if ("3".equals(level)) {
			alist = AreaUtils.getAreaListByCityId(code);
		} else {
			throw new QuickuException("-2", "level参数只能是[1,2,3]");
		}

		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		for (AreaEO aeo : alist) {
			Map<String, String> m = new HashMap<String, String>();
			m.put("code", aeo.getAreaCode() + "");
			m.put("name", aeo.getAreaName());
			list.add(m);
		}

		return RestUtils.createJSONSuccess("添加成功", list);
	}

}
