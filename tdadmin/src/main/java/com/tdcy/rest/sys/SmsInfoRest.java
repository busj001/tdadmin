package com.tdcy.rest.sys;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tdcy.framework.BaseController;
import com.tdcy.framework.util.RestUtils;
import com.tdcy.framework.util.StringUtils;
import com.tdcy.framework.util.WebUtils;
import com.tdcy.sys.constant.ParamConstants;
import com.tdcy.sys.service.ISmsSvc;
import com.tdcy.sys.service.bean.SmsSendBean;
import com.tdcy.sys.util.ParamUtils;

@Controller
public class SmsInfoRest extends BaseController {
	@Resource
	ISmsSvc smsSvc;

	/**
	 * 获取模板列息
	 * 
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/getSmsConfig")
	public Object getSmsConfig(HttpServletRequest request, HttpServletResponse response) {
		String timeout = ParamUtils.getParamValue(ParamConstants.SMS_CHECKCODE_TIMEOUT);
		String templateId = ParamUtils.getParamValue(ParamConstants.SMS_CHECKCODE_TEMPLATEID);

		Map<String, Object> retMap = new HashMap<String, Object>();
		retMap.put("timeout", timeout);
		retMap.put("checkCodeTmpId", templateId);
		return WebUtils.createJSONSuccess("数据加载成功", retMap);
	}

	/**
	 * 获取模板列息
	 * 
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/getTemplateList")
	public Object getTemplateList(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> retMap = new HashMap<String, Object>();
		retMap.put("templateList", smsSvc.getTemplateList());
		return WebUtils.createJSONSuccess("短信发送成功", retMap);
	}

	/**
	 * 获取操作员信息
	 * 
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/sendSms")
	public Object sendSms(HttpServletRequest request, HttpServletResponse response) {
		Map<String, String> param = RestUtils.getRestParam(request);

		String templateId = StringUtils.getString(param, "templateId");
		String to = StringUtils.getString(param, "to");
		SmsSendBean sbean = new SmsSendBean();
		sbean.setParam(StringUtils.getString(param, "param"));
		sbean.setTo(to);
		sbean.setTemplateId(templateId);
		smsSvc.sendSms(sbean);

		Map<String, Object> retMap = new HashMap<String, Object>();
		return WebUtils.createJSONSuccess("短信已发送", retMap);
	}

	/**
	 * 获取操作员信息
	 * 
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/sendCheckSms")
	public Object sendCheckSms(HttpServletRequest request, HttpServletResponse response) {
		Map<String, String> param = RestUtils.getRestParam(request);

		String to = StringUtils.getString(param, "to");
		String jsid = StringUtils.getString(param, "jsid");

		SmsSendBean sbean = new SmsSendBean();
		sbean.setTo(to);
		smsSvc.sendCheckSms(sbean);
		
		
		Map<String,Object> checkinfo= new HashMap<String,Object>();
		checkinfo.put("ccCodevalue", sbean.getCheckCode());
		checkinfo.put("ccId", sbean.getCcId());
		checkinfo.put("createTime", sbean.getCreateTime());
		checkinfo.put("sendTime", sbean.getSendTime());
		checkinfo.put("validFlag", 1);
		
		Map<String, Object> retMap = new HashMap<String, Object>();
		retMap.put("checkinfo", checkinfo);
		return WebUtils.createJSONSuccess("短信已发送", retMap);
	}

	/**
	 * 获取操作员信息
	 * 
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/checkSmsCode")
	public Object checkSmsCode(HttpServletRequest request, HttpServletResponse response) {
		Map<String, String> param = RestUtils.getRestParam(request);
		String chkid = StringUtils.getString(param, "chkid");
		String chkcode = StringUtils.getString(param, "chkcode");
		smsSvc.checkSmsCode(chkid, chkcode);

		Map<String, Object> retMap = new HashMap<String, Object>();
		return WebUtils.createJSONSuccess("验证成功", retMap);
	}

}
