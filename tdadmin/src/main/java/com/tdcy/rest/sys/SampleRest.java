package com.tdcy.rest.sys;

/*import io.swagger.annotations.Api;
 import io.swagger.annotations.ApiOperation;*/

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tdcy.framework.exception.QuickuException;
import com.tdcy.framework.util.RestUtils;
import com.tdcy.framework.util.WebUtils;

@Controller
@RequestMapping(value = "/rest")
public class SampleRest {

	@ResponseBody
	@RequestMapping(value = "/test", method = RequestMethod.POST)
	public Map getall(HttpServletRequest request) throws IOException {
		Map<String, Object> a = new HashMap<String, Object>();
		a.put("a", "b");

		return RestUtils.createJSONSuccess("添加成功", a);
	}

	@ResponseBody
	@RequestMapping(value = "/testexp", method = RequestMethod.POST)
	public Object getloginrolemenus(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		int i = 1;
		if (i == 1) {
			throw new QuickuException("测试异常");
		}
		return WebUtils.createJSONSuccess("测试成功");
	}

}
