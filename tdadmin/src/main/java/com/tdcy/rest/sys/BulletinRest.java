package com.tdcy.rest.sys;

/*import io.swagger.annotations.Api;
 import io.swagger.annotations.ApiOperation;*/

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tdcy.framework.BaseController;
import com.tdcy.framework.bean.PageInfo;
import com.tdcy.framework.exception.QuickuException;
import com.tdcy.framework.util.DateUtils;
import com.tdcy.framework.util.RestUtils;
import com.tdcy.framework.util.StringUtils;
import com.tdcy.sys.dao.eo.BulletinsEO;
import com.tdcy.sys.service.IBulletinsSvc;
import com.tdcy.sys.service.bean.BulletinQueryCondition;

@Controller
public class BulletinRest extends BaseController {
	@Resource
	IBulletinsSvc bsvc;

	/**
	 * 根据消费编码获取消息详细信息
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/getNewsById", method = RequestMethod.POST)
	public Map getNewsById(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String newsid = request.getParameter("newsid");
		if (StringUtils.isEmpty(newsid)) {
			throw new QuickuException("-1", "请输入newsid参数");
		}
		BulletinsEO news  = bsvc.getBulletinsInfo(Integer.parseInt(newsid));
		return RestUtils.createJSONSuccess("加载成功", news);
	}

	
	
	@ResponseBody
	@RequestMapping(value = "/getRecentNews", method = RequestMethod.POST)
	public Map getRecentNews(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String top = request.getParameter("top");
		if (StringUtils.isEmpty(top)) {
			throw new QuickuException("-1", "请输入top参数");
		}
		Integer row = Integer.parseInt(top);
		BulletinQueryCondition qc = new BulletinQueryCondition();
		qc.setPage(1);
		qc.setRows(row);
		PageInfo<BulletinsEO> apage = bsvc.getBulletinPageInfo(qc);

		List<Map<String, String>> alist = new ArrayList<Map<String, String>>();
		for (BulletinsEO beo : apage.getResultsList()) {
			Map<String, String> m = new HashMap<String, String>();
			m.put("title", beo.getBulletinTitle());
			alist.add(m);
		}

		Map<String, Object> mm = new HashMap<String, Object>();
		mm.put("news", alist);
		return RestUtils.createJSONSuccess("成功", mm);
	}

	@ResponseBody
	@RequestMapping(value = "/getPaginationNews", method = RequestMethod.POST)
	public Map getPaginationNews(HttpServletRequest request, HttpServletResponse response, BulletinQueryCondition qc) throws IOException {
		String page = RestUtils.getRestParam(request, "page");
		String pageSize = RestUtils.getRestParam(request, "pageSize");
		
		qc.setPage(Integer.parseInt(page));
		qc.setRows(Integer.parseInt(pageSize));
		PageInfo<BulletinsEO> apage = bsvc.getBulletinPageInfo(qc);

		List<Map<String, String>> alist = new ArrayList<Map<String, String>>();
		for (BulletinsEO beo : apage.getResultsList()) {
			Map<String, String> m = new HashMap<String, String>();
			m.put("title", beo.getBulletinTitle());
			m.put("content", beo.getBulletinContent());
			m.put("releasetime", DateUtils.datetimeToString(beo.getSendDate()));
			alist.add(m);
		}

		Map<String, Object> mm = new HashMap<String, Object>();
		mm.put("news", alist);
		mm.put("totalRecords", apage.getTotalRecord());
		mm.put("totalPages", apage.getTotalPage());
		return RestUtils.createJSONSuccess("成功", mm);
	}
}
