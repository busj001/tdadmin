package com.tdcy.sys.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;

/**
 * @Description 行政区划EO类
 */
@Table(name = "sys_area")
public class AreaEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 行政区划代码
	 */
	@Id
	@Column(name = "area_code")
	private Integer areaCode;
	/**
	 * 行政区划名称
	 */
	@Column(name = "area_name")
	private String areaName;
	
	/**
	 * 行政区划名称
	 */
	@Column(name = "pinyin")
	private String pinyin;
	/**
	 * 行政区划等级
	 */
	@Column(name = "area_level")
	private Integer areaLevel;
	/**
	 * 上级行政区划
	 */
	@Column(name = "area_up_code")
	private Integer areaUpCode;

	@Column(name = "zipcode")
	private String zipcode;
	
	@Column(name = "openflag")
	private Integer openflag;
	

	public String getPinyin() {
		return pinyin;
	}

	public void setPinyin(String pinyin) {
		this.pinyin = pinyin;
	}

	public Integer getOpenflag() {
		return openflag;
	}

	public void setOpenflag(Integer openflag) {
		this.openflag = openflag;
	}

	public String toString() {
		return areaCode + "|" + areaName;
	}

	/**
	 * 
	* 
	*/
	@Column(name = "valid_flag")
	private Integer validFlag;

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	/**
	 * 获取 行政区划代码
	 */
	public Integer getAreaCode() {
		return areaCode;
	}

	/**
	 * 设置行政区划代码
	 */
	public void setAreaCode(Integer areaCode) {
		this.areaCode = areaCode;
	}

	/**
	 * 获取 行政区划名称
	 */
	public String getAreaName() {
		return areaName;
	}

	/**
	 * 设置行政区划名称
	 */
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	/**
	 * 获取 行政区划等级
	 */
	public Integer getAreaLevel() {
		return areaLevel;
	}

	/**
	 * 设置行政区划等级
	 */
	public void setAreaLevel(Integer areaLevel) {
		this.areaLevel = areaLevel;
	}

	/**
	 * 获取 上级行政区划
	 */
	public Integer getAreaUpCode() {
		return areaUpCode;
	}

	/**
	 * 设置上级行政区划
	 */
	public void setAreaUpCode(Integer areaUpCode) {
		this.areaUpCode = areaUpCode;
	}

	/**
	 * 获取
	 */
	public Integer getValidFlag() {
		return validFlag;
	}

	/**
	 * 设置
	 */
	public void setValidFlag(Integer validFlag) {
		this.validFlag = validFlag;
	}
}
