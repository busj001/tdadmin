package com.tdcy.sys.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;
/**
* @Description 预付订单表EO类
*/
@Table(name = "cmt_wx_pay")
public class WxPayEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "preorder_id")
	private Integer preorderId;
	/**
	* 
	*/
	@Column(name = "jsid")
	private Integer jsid;
	/**
	* 微信订单号
	*/
	@Column(name = "wx_order_no")
	private String wxOrderNo;
	/**
	* 户商订单编号
	*/
	@Column(name = "pay_bizno")
	private String payBizno;
	/**
	* 
	*/
	@Column(name = "pay_money")
	private Float payMoney;
	/**
	* 订单描述
	*/
	@Column(name = "biz_desc")
	private String bizDesc;
	/**
	* 预支付交易会话标识
	*/
	@Column(name = "prepay_id")
	private String prepayId;
	/**
	* 二维码链接
	*/
	@Column(name = "code_url")
	private String codeUrl;
	/**
	* 
	*/
	@Column(name = "pay_status")
	private String payStatus;
	/**
	* 
	*/
	@Column(name = "create_time")
	private java.util.Date createTime;
	/**
	* 
	*/
	@Column(name = "valid_flag")
	private Integer validFlag;
	
	private String tradeType;
	
	private String attach;
	
	
	
	public String getAttach() {
		return attach;
	}
	public void setAttach(String attach) {
		this.attach = attach;
	}
	public String getTradeType() {
		return tradeType;
	}
	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}
	/**
	* 获取 
	*/ 
	public Integer getPreorderId() {
		return preorderId;
	}
	/**
	* 设置
	*/
	public void setPreorderId(Integer preorderId) {
		this.preorderId = preorderId;
	}
	/**
	* 获取 
	*/ 
	public Integer getJsid() {
		return jsid;
	}
	/**
	* 设置
	*/
	public void setJsid(Integer jsid) {
		this.jsid = jsid;
	}
	/**
	* 获取 微信订单号
	*/ 
	public String getWxOrderNo() {
		return wxOrderNo;
	}
	/**
	* 设置微信订单号
	*/
	public void setWxOrderNo(String wxOrderNo) {
		this.wxOrderNo = wxOrderNo;
	}
	/**
	* 获取 户商订单编号
	*/ 
	public String getPayBizno() {
		return payBizno;
	}
	/**
	* 设置户商订单编号
	*/
	public void setPayBizno(String payBizno) {
		this.payBizno = payBizno;
	}
	/**
	* 获取 
	*/ 
	public Float getPayMoney() {
		return payMoney;
	}
	/**
	* 设置
	*/
	public void setPayMoney(Float payMoney) {
		this.payMoney = payMoney;
	}
	/**
	* 获取 订单描述
	*/ 
	public String getBizDesc() {
		return bizDesc;
	}
	/**
	* 设置订单描述
	*/
	public void setBizDesc(String bizDesc) {
		this.bizDesc = bizDesc;
	}
	/**
	* 获取 预支付交易会话标识
	*/ 
	public String getPrepayId() {
		return prepayId;
	}
	/**
	* 设置预支付交易会话标识
	*/
	public void setPrepayId(String prepayId) {
		this.prepayId = prepayId;
	}
	/**
	* 获取 二维码链接
	*/ 
	public String getCodeUrl() {
		return codeUrl;
	}
	/**
	* 设置二维码链接
	*/
	public void setCodeUrl(String codeUrl) {
		this.codeUrl = codeUrl;
	}
	/**
	* 获取 
	*/ 
	public String getPayStatus() {
		return payStatus;
	}
	/**
	* 设置
	*/
	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getCreateTime() {
		return createTime;
	}
	/**
	* 设置
	*/
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	/**
	* 获取 
	*/ 
	public Integer getValidFlag() {
		return validFlag;
	}
	/**
	* 设置
	*/
	public void setValidFlag(Integer validFlag) {
		this.validFlag = validFlag;
	}
}

