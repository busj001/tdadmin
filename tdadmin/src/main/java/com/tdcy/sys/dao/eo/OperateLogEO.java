package com.tdcy.sys.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;
/**
* @Description EO类
*/
@Table(name = "sys_operate_log")
public class OperateLogEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "operate_id")
	private Integer operateId;
	/**
	* 
	*/
	@Column(name = "login_id")
	private Integer loginId;
	/**
	* 
	*/
	@Column(name = "login_user")
	private String loginUser;
	/**
	* 
	*/
	@Column(name = "user_id")
	private Integer userId;
	/**
	* 
	*/
	@Column(name = "user_kind")
	private Integer userKind;
	/**
	* 
	*/
	@Column(name = "user_name")
	private String userName;
	/**
	* 
	*/
	@Column(name = "login_address")
	private String loginAddress;
	/**
	* 
	*/
	@Column(name = "operate_url")
	private String operateUrl;
	/**
	* 
	*/
	@Column(name = "operate_name")
	private String operateName;
	/**
	* 
	*/
	@Column(name = "operate_class")
	private String operateClass;
	/**
	* 
	*/
	@Column(name = "operate_method")
	private String operateMethod;
	/**
	* 
	*/
	@Column(name = "operate_param")
	private String operateParam;
	/**
	* 
	*/
	@Column(name = "create_time")
	private java.util.Date createTime;
	/**
	* 
	*/
	@Column(name = "remark")
	private String remark;
	/**
	* 
	*/
	@Column(name = "valid_flag")
	private Integer validFlag;
	/**
	* 获取 
	*/ 
	public Integer getOperateId() {
		return operateId;
	}
	/**
	* 设置
	*/
	public void setOperateId(Integer operateId) {
		this.operateId = operateId;
	}
	/**
	* 获取 
	*/ 
	public Integer getLoginId() {
		return loginId;
	}
	/**
	* 设置
	*/
	public void setLoginId(Integer loginId) {
		this.loginId = loginId;
	}
	/**
	* 获取 
	*/ 
	public String getLoginUser() {
		return loginUser;
	}
	/**
	* 设置
	*/
	public void setLoginUser(String loginUser) {
		this.loginUser = loginUser;
	}
	/**
	* 获取 
	*/ 
	public Integer getUserId() {
		return userId;
	}
	/**
	* 设置
	*/
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	/**
	* 获取 
	*/ 
	public Integer getUserKind() {
		return userKind;
	}
	/**
	* 设置
	*/
	public void setUserKind(Integer userKind) {
		this.userKind = userKind;
	}
	/**
	* 获取 
	*/ 
	public String getUserName() {
		return userName;
	}
	/**
	* 设置
	*/
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	* 获取 
	*/ 
	public String getLoginAddress() {
		return loginAddress;
	}
	/**
	* 设置
	*/
	public void setLoginAddress(String loginAddress) {
		this.loginAddress = loginAddress;
	}
	/**
	* 获取 
	*/ 
	public String getOperateUrl() {
		return operateUrl;
	}
	/**
	* 设置
	*/
	public void setOperateUrl(String operateUrl) {
		this.operateUrl = operateUrl;
	}
	/**
	* 获取 
	*/ 
	public String getOperateName() {
		return operateName;
	}
	/**
	* 设置
	*/
	public void setOperateName(String operateName) {
		this.operateName = operateName;
	}
	/**
	* 获取 
	*/ 
	public String getOperateClass() {
		return operateClass;
	}
	/**
	* 设置
	*/
	public void setOperateClass(String operateClass) {
		this.operateClass = operateClass;
	}
	/**
	* 获取 
	*/ 
	public String getOperateMethod() {
		return operateMethod;
	}
	/**
	* 设置
	*/
	public void setOperateMethod(String operateMethod) {
		this.operateMethod = operateMethod;
	}
	/**
	* 获取 
	*/ 
	public String getOperateParam() {
		return operateParam;
	}
	/**
	* 设置
	*/
	public void setOperateParam(String operateParam) {
		this.operateParam = operateParam;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getCreateTime() {
		return createTime;
	}
	/**
	* 设置
	*/
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	/**
	* 获取 
	*/ 
	public String getRemark() {
		return remark;
	}
	/**
	* 设置
	*/
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	* 获取 
	*/ 
	public Integer getValidFlag() {
		return validFlag;
	}
	/**
	* 设置
	*/
	public void setValidFlag(Integer validFlag) {
		this.validFlag = validFlag;
	}
}

