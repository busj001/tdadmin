package com.tdcy.sys.dao.impl;

import org.springframework.stereotype.Service;

import com.tdcy.framework.bean.PageInfo;
import com.tdcy.framework.dao.DaoImpl;
import com.tdcy.sys.dao.IDeptDAO;
import com.tdcy.sys.dao.eo.DeptEO;
import com.tdcy.sys.service.bean.QueryCondition;

/**
 * @Description 部门DAO实现类
 */
@Service
public class EmpDeptDAOImpl extends DaoImpl implements IDeptDAO {

	@Override
	public PageInfo<DeptEO> getPageInfo(QueryCondition qc) {
		String sql = "select * from er_emp_dept";

		return this.queryRecordByClassForPageInfo(sql, DeptEO.class,
				qc.getPageInfo());
	}

}
