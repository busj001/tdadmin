package com.tdcy.sys.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;

/**
 * @Description EO类
 */
@Table(name = "sys_access")
public class AccessEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 接入编号
	 */
	@Id
	@Column(name = "access_id")
	private Integer accessId;
	/**
	 * 接入应用名称
	 */
	@Column(name = "app_name")
	private String appName;
	/**
	 * 接入用户名
	 */
	@Column(name = "verfiy_name")
	private String verfiyName;
	/**
	 * 接入密码
	 */
	@Column(name = "verfiy_password")
	private String verfiyPassword;
	/**
	 * 入接状态
	 */
	@Column(name = "status")
	private Integer status;
	/**
	 * 有效标志
	 */
	@Column(name = "valid_flag")
	private Integer validFlag;
	/**
	 * 创建时间
	 */
	@Column(name = "create_time")
	private java.util.Date createTime;

	/**
	 * 获取 接入编号
	 */
	public Integer getAccessId() {
		return accessId;
	}

	/**
	 * 设置接入编号
	 */
	public void setAccessId(Integer accessId) {
		this.accessId = accessId;
	}

	/**
	 * 获取 接入应用名称
	 */
	public String getAppName() {
		return appName;
	}

	/**
	 * 设置接入应用名称
	 */
	public void setAppName(String appName) {
		this.appName = appName;
	}

	/**
	 * 获取 接入用户名
	 */
	public String getVerfiyName() {
		return verfiyName;
	}

	/**
	 * 设置接入用户名
	 */
	public void setVerfiyName(String verfiyName) {
		this.verfiyName = verfiyName;
	}

	/**
	 * 获取 接入密码
	 */
	public String getVerfiyPassword() {
		return verfiyPassword;
	}

	/**
	 * 设置接入密码
	 */
	public void setVerfiyPassword(String verfiyPassword) {
		this.verfiyPassword = verfiyPassword;
	}

	/**
	 * 获取 入接状态
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * 设置入接状态
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * 获取 有效标志
	 */
	public Integer getValidFlag() {
		return validFlag;
	}

	/**
	 * 设置有效标志
	 */
	public void setValidFlag(Integer validFlag) {
		this.validFlag = validFlag;
	}

	/**
	 * 获取 创建时间
	 */
	public java.util.Date getCreateTime() {
		return createTime;
	}

	/**
	 * 设置创建时间
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
}
