package com.tdcy.sys.dao;

import com.tdcy.framework.dao.IDao;
import com.tdcy.sys.dao.eo.AccessEO;

/**
 * @Description DAO类
 */
public interface IAccessDAO extends IDao {
	public AccessEO findByAppId(String appid);
}
