package com.tdcy.sys.dao;

import java.util.List;

import com.tdcy.framework.dao.IDao;
import com.tdcy.sys.dao.eo.StaffEO;
import com.tdcy.sys.service.bean.StaffQueryCondition;

/**
 * @Description DAO类
 */
public interface IStaffDAO extends IDao {
	public List<StaffEO> queryStaff(StaffQueryCondition staffCo);
	
	public StaffEO findByLoginUser(String loginUser);
}
