package com.tdcy.sys.dao;

import java.util.List;

import com.tdcy.framework.dao.IDao;
import com.tdcy.sys.dao.eo.ParamEO;
import com.tdcy.sys.service.bean.ParamQueryCondition;

public interface IParamDAO extends IDao {
	public ParamEO getParam(String key);
	
	public List<ParamEO> getParamList(ParamQueryCondition qc);
}
