package com.tdcy.sys.dao;

import com.tdcy.framework.bean.PageInfo;
import com.tdcy.framework.dao.IDao;
import com.tdcy.sys.dao.eo.WxPayEO;
import com.tdcy.sys.service.bean.WxPayCondition;

/**
 * @Description 预付订单表DAO类
 */
public interface IWxPayDAO extends IDao {
	public void updateByPayBizNo(String bizNo, String wxBizNo, String payStatus);
	
	public WxPayEO queryByBizNo(String bizNo);
	
	public void deleteByPyBizNo(String bizNo);

	public PageInfo<WxPayEO> getPageInfo(WxPayCondition queryco);
	
	public boolean hasPayRecord(String bizNo) ;
}
