package com.tdcy.sys.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;
/**
* @Description 预付订单表EO类
*/
@Table(name = "cmt_wx_inf_log")
public class WxInfLogEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "wx_inf_id")
	private Integer wxInfId;
	/**
	* 发送xml
	*/
	@Column(name = "request_xml")
	private String requestXml;
	/**
	*  接收xml
	*/
	@Column(name = "result_xml")
	private String resultXml;
	/**
	* 接口名称
	*/
	@Column(name = "inf_name")
	private String infName;
	/**
	* 通信标识
	*/
	@Column(name = "return_code")
	private String returnCode;
	/**
	* 返回信息
	*/
	@Column(name = "return_msg")
	private String returnMsg;
	/**
	* 业务结果
	*/
	@Column(name = "result_code")
	private String resultCode;
	/**
	* 错误代码
	*/
	@Column(name = "err_code")
	private String errCode;
	/**
	* 错误代码描述
	*/
	@Column(name = "err_code_des")
	private String errCodeDes;
	/**
	* 支付订单id
	*/
	@Column(name = "pay_biz_no")
	private String payBizNo;
	/**
	* 支付订单id
	*/
	@Column(name = "wx_biz_no")
	private String wxBizNo;
	/**
	* 
	*/
	@Column(name = "create_time")
	private java.util.Date createTime;
	/**
	* 
	*/
	@Column(name = "valid_flag")
	private Integer validFlag;
	/**
	* 获取 
	*/ 
	public Integer getWxInfId() {
		return wxInfId;
	}

	/**
	* 设置
	*/
	public void setWxInfId(Integer wxInfId) {
		this.wxInfId = wxInfId;
	}
	/**
	* 获取 发送xml
	*/ 
	public String getRequestXml() {
		return requestXml;
	}
	/**
	* 设置发送xml
	*/
	public void setRequestXml(String requestXml) {
		this.requestXml = requestXml;
	}
	/**
	* 获取  接收xml
	*/ 
	public String getResultXml() {
		return resultXml;
	}
	/**
	* 设置 接收xml
	*/
	public void setResultXml(String resultXml) {
		this.resultXml = resultXml;
	}
	/**
	* 获取 接口名称
	*/ 
	public String getInfName() {
		return infName;
	}
	/**
	* 设置接口名称
	*/
	public void setInfName(String infName) {
		this.infName = infName;
	}
	/**
	* 获取 通信标识
	*/ 
	public String getReturnCode() {
		return returnCode;
	}
	/**
	* 设置通信标识
	*/
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}
	/**
	* 获取 返回信息
	*/ 
	public String getReturnMsg() {
		return returnMsg;
	}
	/**
	* 设置返回信息
	*/
	public void setReturnMsg(String returnMsg) {
		this.returnMsg = returnMsg;
	}
	/**
	* 获取 业务结果
	*/ 
	public String getResultCode() {
		return resultCode;
	}
	/**
	* 设置业务结果
	*/
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	/**
	* 获取 错误代码
	*/ 
	public String getErrCode() {
		return errCode;
	}
	/**
	* 设置错误代码
	*/
	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}
	/**
	* 获取 错误代码描述
	*/ 
	public String getErrCodeDes() {
		return errCodeDes;
	}
	/**
	* 设置错误代码描述
	*/
	public void setErrCodeDes(String errCodeDes) {
		this.errCodeDes = errCodeDes;
	}

	public String getPayBizNo() {
		return payBizNo;
	}

	public void setPayBizNo(String payBizNo) {
		this.payBizNo = payBizNo;
	}

	public String getWxBizNo() {
		return wxBizNo;
	}

	public void setWxBizNo(String wxBizNo) {
		this.wxBizNo = wxBizNo;
	}

	/**
	* 获取 
	*/ 
	public java.util.Date getCreateTime() {
		return createTime;
	}
	/**
	* 设置
	*/
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	/**
	* 获取 
	*/ 
	public Integer getValidFlag() {
		return validFlag;
	}
	/**
	* 设置
	*/
	public void setValidFlag(Integer validFlag) {
		this.validFlag = validFlag;
	}
}

