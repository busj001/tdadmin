package com.tdcy.sys.dao;

import java.util.Map;

import com.tdcy.framework.bean.PageInfo;
import com.tdcy.framework.dao.IDao;
import com.tdcy.sys.dao.eo.SystemLogEO;

/**
 * @Description DAO类
 */
public interface ISystemLogDAO extends IDao {
	public PageInfo<SystemLogEO> getPageInfo(Map<String, String> queryParam,
			PageInfo pageinfo);
}
