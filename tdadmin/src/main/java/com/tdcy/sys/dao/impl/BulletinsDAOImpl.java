package com.tdcy.sys.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.tdcy.framework.bean.PageInfo;
import com.tdcy.framework.dao.DaoImpl;
import com.tdcy.framework.util.StringUtils;
import com.tdcy.sys.dao.IBulletinsDAO;
import com.tdcy.sys.dao.eo.BulletinsEO;
import com.tdcy.sys.service.bean.BulletinQueryCondition;

/**
* @Description DAO实现类
*/
@Service
public class BulletinsDAOImpl extends DaoImpl implements IBulletinsDAO {

	@Override
	public PageInfo<BulletinsEO> getPageInfo(BulletinQueryCondition queryco) {
		String sql ="select * from sys_bulletins where valid_flag='1'";
		List<Object> plist = new ArrayList<Object>();
		if(queryco.getStartTime()!=null){
			sql += " and send_date>=?";
			plist.add(queryco.getStartTime());
		}
		if(queryco.getEndTime()!=null){
			sql += " and send_date<=?";
			plist.add(queryco.getEndTime());
		}
		
		sql += " order by send_date desc ";
		return this.queryRecordByClassForPageInfo(sql, BulletinsEO.class, queryco.getPageInfo(),StringUtils.toArray(plist));
	}

}
