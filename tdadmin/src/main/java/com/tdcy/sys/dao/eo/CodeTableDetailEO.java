package com.tdcy.sys.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;

@Table(name = "sys_code_table_detail")
public class CodeTableDetailEO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "code_type")
	private String codeType;

	@Id
	@Column(name = "data_value")
	private String dataValue;

	@Column(name = "display_value")
	private String displayValue;

	@Column(name = "dis_order")
	private int disOrder;


	public String getCodeType() {
		return codeType;
	}

	public void setCodeType(String codeType) {
		this.codeType = codeType;
	}

	public String getDataValue() {
		return dataValue;
	}

	public void setDataValue(String dataValue) {
		this.dataValue = dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}

	public void setDisplayValue(String displayValue) {
		this.displayValue = displayValue;
	}

	public int getDisOrder() {
		return disOrder;
	}

	public void setDisOrder(int disOrder) {
		this.disOrder = disOrder;
	}

}
