package com.tdcy.sys.dao.impl;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.tdcy.framework.bean.PageInfo;
import com.tdcy.framework.dao.DaoImpl;
import com.tdcy.sys.dao.ISystemLogDAO;
import com.tdcy.sys.dao.eo.SystemLogEO;

/**
 * @Description DAO实现类
 */
@Service
public class SystemLogDAOImpl extends DaoImpl implements ISystemLogDAO {
	@Override
	public PageInfo<SystemLogEO> getPageInfo(Map<String, String> queryParam,
			PageInfo pageinfo) {
		String sql = "select * from sys_system_log";

		return this.queryRecordByClassForPageInfo(sql, SystemLogEO.class,
				pageinfo);
	}
}
