package com.tdcy.sys.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;
/**
* @Description EO类
*/
@Table(name = "sys_menu")
public class MenuEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "menu_id")
	private Integer menuId;
	/**
	* 
	*/
	@Column(name = "menu_name")
	private String menuName;
	/**
	* 
	*/
	@Column(name = "menu_desc")
	private String menuDesc;
	@Column(name = "permission_code")
	private String permissionCode;
	
	/**
	* 
	*/
	@Column(name = "menu_up_id")
	private Integer menuUpId;
	/**
	* 
	*/
	@Column(name = "menu_url")
	private String menuUrl;
	@Column(name = "menu_order")
	private Integer menuOrder;
	/**
	* 
	*/
	@Column(name = "pic_name")
	private String picName;
	/**
	* 
	*/
	@Column(name = "menu_type")
	private Integer menuType;
	
	/**
	* 
	*/
	@Column(name = "menu_visiable")
	private Integer menuVisiable;
	
	
	/**
	* 
	*/
	@Column(name = "log_flag")
	private Integer logFlag;
	/**
	* 
	*/
	@Column(name = "valid_flag")
	private Integer validFlag;
	
	
	public String getPermissionCode() {
		return permissionCode;
	}
	public void setPermissionCode(String permissionCode) {
		this.permissionCode = permissionCode;
	}
	public Integer getMenuVisiable() {
		return menuVisiable;
	}
	public void setMenuVisiable(Integer menuVisiable) {
		this.menuVisiable = menuVisiable;
	}
	/**
	* 获取 
	*/ 
	public Integer getMenuId() {
		return menuId;
	}
	/**
	* 设置
	*/
	public void setMenuId(Integer menuId) {
		this.menuId = menuId;
	}
	/**
	* 获取 
	*/ 
	public String getMenuName() {
		return menuName;
	}
	/**
	* 设置
	*/
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	/**
	* 获取 
	*/ 
	public String getMenuDesc() {
		return menuDesc;
	}
	/**
	* 设置
	*/
	public void setMenuDesc(String menuDesc) {
		this.menuDesc = menuDesc;
	}
	/**
	* 获取 
	*/ 
	public Integer getMenuUpId() {
		return menuUpId;
	}
	/**
	* 设置
	*/
	public void setMenuUpId(Integer menuUpId) {
		this.menuUpId = menuUpId;
	}
	/**
	* 获取 
	*/ 
	public String getMenuUrl() {
		return menuUrl;
	}
	/**
	* 设置
	*/
	public void setMenuUrl(String menuUrl) {
		this.menuUrl = menuUrl;
	}
	/**
	* 获取 
	*/ 
	public Integer getMenuOrder() {
		return menuOrder;
	}
	/**
	* 设置
	*/
	public void setMenuOrder(Integer menuOrder) {
		this.menuOrder = menuOrder;
	}
	/**
	* 获取 
	*/ 
	public String getPicName() {
		return picName;
	}
	/**
	* 设置
	*/
	public void setPicName(String picName) {
		this.picName = picName;
	}
	/**
	* 获取 
	*/ 
	public Integer getMenuType() {
		return menuType;
	}
	/**
	* 设置
	*/
	public void setMenuType(Integer menuType) {
		this.menuType = menuType;
	}
	/**
	* 获取 
	*/ 
	public Integer getLogFlag() {
		return logFlag;
	}
	/**
	* 设置
	*/
	public void setLogFlag(Integer logFlag) {
		this.logFlag = logFlag;
	}
	/**
	* 获取 
	*/ 
	public Integer getValidFlag() {
		return validFlag;
	}
	/**
	* 设置
	*/
	public void setValidFlag(Integer validFlag) {
		this.validFlag = validFlag;
	}
}

