package com.tdcy.sys.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.tdcy.framework.dao.DaoImpl;
import com.tdcy.framework.util.StringUtils;
import com.tdcy.sys.dao.IStaffDAO;
import com.tdcy.sys.dao.eo.StaffEO;
import com.tdcy.sys.service.bean.StaffQueryCondition;

/**
 * @Description DAO实现类
 */
@Service
public class StaffDAOImpl extends DaoImpl implements IStaffDAO {
	@Override
	public StaffEO findByLoginUser(String loginUser) {
		String sql = "select * from sys_staff where valid_flag = '1' and login_user = ?";
		return this.execSqlQueryOne(StaffEO.class, sql, new Object[] { loginUser });
	}

	@Override
	public List<StaffEO> queryStaff(StaffQueryCondition staffCo) {
		String sql = "select * from sys_staff where 1=1 and valid_flag = '1' ";
		ArrayList<Object> p = new ArrayList<Object>();
		if (!StringUtils.isEmpty(staffCo.getLoginUser())) {
			sql += " and login_user like ?";
			p.add("%"+staffCo.getLoginUser()+"%");
		}

		if (!StringUtils.isEmpty(staffCo.getStaffName())) {
			sql += " and staff_name like ?";
			p.add("%"+staffCo.getStaffName()+"%");
		}

		return this.execSqlQuery(StaffEO.class, sql, p.toArray());
	}

}
