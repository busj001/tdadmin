package com.tdcy.sys.dao;

import com.tdcy.framework.dao.IDao;
import com.tdcy.sys.dao.eo.AreaEO;

/**
 * @Description 行政区划DAO类
 */
public interface IAreaDAO extends IDao {
	public AreaEO findByCode(String code);
}
