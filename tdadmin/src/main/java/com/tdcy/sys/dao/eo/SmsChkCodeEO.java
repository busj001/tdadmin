package com.tdcy.sys.dao.eo;

import java.io.Serializable;
import java.util.Date;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;

/**
 * @Description EO类
 */
@Table(name = "cmt_sms_checkcode")
public class SmsChkCodeEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 短信id
	 */
	@Id
	@Column(name = "cc_id")
	private Integer ccId;
	/**
	* 
	*/
	@Column(name = "cc_codevalue")
	private String ccCodevalue;
	/**
	* 
	*/
	@Column(name = "valid_flag")
	private String validFlag;
	/**
	* 
	*/
	@Column(name = "create_time")
	private Date createTime;

	/**
	* 
	*/
	@Column(name = "send_time")
	private Date sendTime;
	
	
	@Column(name = "sms_id")
	private Integer smsId;


	public Integer getSmsId() {
		return smsId;
	}

	public void setSmsId(Integer smsId) {
		this.smsId = smsId;
	}

	public Date getSendTime() {
		return sendTime;
	}

	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}

	public Integer getCcId() {
		return ccId;
	}

	public void setCcId(Integer ccId) {
		this.ccId = ccId;
	}

	public String getCcCodevalue() {
		return ccCodevalue;
	}

	public void setCcCodevalue(String ccCodevalue) {
		this.ccCodevalue = ccCodevalue;
	}

	public String getValidFlag() {
		return validFlag;
	}

	public void setValidFlag(String validFlag) {
		this.validFlag = validFlag;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
