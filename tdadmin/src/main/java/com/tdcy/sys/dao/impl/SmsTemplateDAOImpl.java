package com.tdcy.sys.dao.impl;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.tdcy.framework.bean.PageInfo;
import com.tdcy.framework.dao.DaoImpl;
import com.tdcy.framework.util.StringUtils;
import com.tdcy.sys.dao.ISmsTemplateDAO;
import com.tdcy.sys.dao.eo.SmsTemplateEO;
import com.tdcy.sys.service.bean.SmsTemplateCo;

/**
 * @Description DAO实现类
 */
@Service
public class SmsTemplateDAOImpl extends DaoImpl implements ISmsTemplateDAO {

	@SuppressWarnings("unchecked")
	@Override
	public PageInfo<SmsTemplateEO> findAllSmsTemplate(SmsTemplateCo co) {
		String sql = " select tpid,platTmpId,platTmpName,tmpContent from cmt_sms_template where  valid_flag =1 ";
		ArrayList<Object> p = new ArrayList<Object>();
		StringBuffer sb = new StringBuffer(sql);
		if (co != null) {
			if (co.getPlatTmpId() != null && !co.getPlatTmpId().trim().equals("")) {
				sb.append(" and platTmpId like ?");
				p.add("%" + co.getPlatTmpId() + "%");
			}
			if (co.getPlatTmpName() != null && !co.getPlatTmpName().trim().equals("")) {
				sb.append(" and platTmpName like ?");
				p.add("%" + co.getPlatTmpName() + "%");
			}
			if (co.getTmpContent() != null && !co.getTmpContent().trim().equals("")) {
				sb.append(" and tmpContent like ?");
				p.add("%" + co.getTmpContent() + "%");
			}
			if (co.getKey() != null && !co.getKey().trim().equals("")) {
				sb.append(" and (");
				sb.append(" platTmpId like ?");
				p.add("%" + co.getKey() + "%");
				sb.append(" or platTmpName like ?");
				p.add("%" + co.getKey() + "%");
				sb.append(" or tmpContent like ?");
				p.add("%" + co.getKey() + "%");
				sb.append(" )");
			}
		}
		return this.queryRecordByClassForPageInfo(sb.toString(), SmsTemplateEO.class, co.getPageInfo(), StringUtils.toArray(p));
	}

	@Override
	public SmsTemplateEO findSmsTemplateByTmpId(String tmpId) {
		String sql = "select * from cmt_sms_template where valid_flag =1 and platTmpId = ? ";
		return this.execSqlQueryOne(SmsTemplateEO.class, sql, new Object[] { tmpId });
	}

	@Override
	public int deletesmstemp(String smstempids) {
		String sql = "delete from cmt_sms_template where tpid in (" + smstempids + ")";
		return this.execSqlUpdate(sql);
	}
}
