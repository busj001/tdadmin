package com.tdcy.sys.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;

@Table(name = "sys_serial")
public class SerialEO implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "serial_type")
	private String serialType;

	@Column(name = "init_str_no")
	private String initStrNo;

	@Column(name = "init_num_no")
	private int initNumNo;

	@Column(name = "prefix_str")
	private String prefixStr;

	@Column(name = "prefix_date")
	private String prefixDate;

	@Column(name = "cur_str_no")
	private String curStrNo;

	@Column(name = "cur_num_no")
	private int curNumNo;

	public String getSerialType() {
		return serialType;
	}

	public void setSerialType(String serialType) {
		this.serialType = serialType;
	}

	public String getInitStrNo() {
		return initStrNo;
	}

	public void setInitStrNo(String initStrNo) {
		this.initStrNo = initStrNo;
	}

	public String getPrefixStr() {
		return prefixStr;
	}

	public void setPrefixStr(String prefixStr) {
		this.prefixStr = prefixStr;
	}

	public String getPrefixDate() {
		return prefixDate;
	}

	public void setPrefixDate(String prefixDate) {
		this.prefixDate = prefixDate;
	}

	public String getCurStrNo() {
		return curStrNo;
	}

	public void setCurStrNo(String curStrNo) {
		this.curStrNo = curStrNo;
	}

	public int getInitNumNo() {
		return initNumNo;
	}

	public void setInitNumNo(int initNumNo) {
		this.initNumNo = initNumNo;
	}

	public int getCurNumNo() {
		return curNumNo;
	}

	public void setCurNumNo(int curNumNo) {
		this.curNumNo = curNumNo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
