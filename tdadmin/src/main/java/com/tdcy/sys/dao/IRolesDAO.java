package com.tdcy.sys.dao;

import java.util.List;

import com.tdcy.framework.dao.IDao;
import com.tdcy.sys.dao.eo.RolesEO;

/**
 * @Description DAO类
 */
public interface IRolesDAO extends IDao {
	public List<RolesEO> getRolesByLoginUser(String loginUser);
	
	public RolesEO findByCode(String code);
}
