package com.tdcy.sys.dao;

import com.tdcy.framework.dao.IDao;
import com.tdcy.sys.dao.eo.CodeTableEO;

public interface ICodeTableDAO extends IDao {
	public CodeTableEO getCodeTable(String codeType);
}
