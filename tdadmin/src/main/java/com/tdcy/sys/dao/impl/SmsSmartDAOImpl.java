package com.tdcy.sys.dao.impl;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.tdcy.framework.bean.PageInfo;
import com.tdcy.framework.dao.DaoImpl;
import com.tdcy.framework.util.StringUtils;
import com.tdcy.sys.dao.ISmsSmartDAO;
import com.tdcy.sys.dao.eo.SmsSmartEO;
import com.tdcy.sys.service.bean.SmsSmartCo;

/**
 * @Description DAO实现类
 */
@Service
public class SmsSmartDAOImpl extends DaoImpl implements ISmsSmartDAO {
	@SuppressWarnings("unchecked")
	@Override
	public PageInfo<SmsSmartEO> findAllSmsSmart(SmsSmartCo co) {
		String sql = "select smart_id,sms_name,sms_content,send_time,smart_flag,sms_type from cmt_sms_smart where  valid_flag =1";
		ArrayList<Object> p = new ArrayList<Object>();
		StringBuffer sb = new StringBuffer(sql);
		if (co != null) {
			if (co.getSmsName() != null && !co.getSmsName().trim().equals("")) {
				sb.append(" and sms_name like ?");
				p.add("%" + co.getSmsName() + "%");
			}
			if (co.getSmsContent() != null && !co.getSmsContent().trim().equals("")) {
				sb.append(" and sms_content like ?");
				p.add("%" + co.getSmsContent() + "%");
			}
			if (co.getSmartFlag() != null && !co.getSmstype().equals("")) {
				sb.append(" and smart_flag=?");
				p.add(co.getSmartFlag());
			}
			if (co.getSmstype() != null && !co.getSmstype().equals("")) {
				sb.append(" and sms_type=?");
				p.add(co.getSmstype());
			}
			if (co.getKey() != null && !co.getKey().trim().equals("")) {
				sb.append(" and (");
				sb.append(" sms_name like ?");
				p.add("%" + co.getKey() + "%");
				sb.append(" or sms_content like ?");
				p.add("%" + co.getKey() + "%");
				sb.append(" )");
			}
		}
		return this.queryRecordByClassForPageInfo(sb.toString(), SmsSmartEO.class, co.getPageInfo(),
				StringUtils.toArray(p));
	}

	@Override
	public int deletesmart(String smartids) {
		String sql = "delete from cmt_sms_smart where smart_id in (" + smartids + ")";
		return this.execSqlUpdate(sql);
	}
}
