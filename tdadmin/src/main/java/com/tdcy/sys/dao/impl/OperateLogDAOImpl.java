package com.tdcy.sys.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.tdcy.framework.bean.PageInfo;
import com.tdcy.framework.dao.DaoImpl;
import com.tdcy.framework.util.StringUtils;
import com.tdcy.sys.dao.IOperateLogDAO;
import com.tdcy.sys.dao.eo.OperateLogEO;
import com.tdcy.sys.service.bean.OperLogQueryCondition;

/**
 * @Description DAO实现类
 */
@Service
public class OperateLogDAOImpl extends DaoImpl implements IOperateLogDAO {
	@Override
	public PageInfo<OperateLogEO> getPageInfo(OperLogQueryCondition queryco) {
		String sql = "select * from sys_operate_log where 1=1 ";
		ArrayList<Object> params = new ArrayList<Object>();
		sql = this.handlerQueryCondition(queryco, sql, params);
		return this.queryRecordByClassForPageInfo(sql, OperateLogEO.class, queryco.getPageInfo(), StringUtils.toArray(params));
	}

	public List<OperateLogEO> getOperateList(OperLogQueryCondition queryco) {
		String sql = "select * from sys_operate_log where 1=1 ";
		ArrayList<Object> params = new ArrayList<Object>();
		sql = this.handlerQueryCondition(queryco, sql, params);
		return this.execSqlQuery(OperateLogEO.class, sql, StringUtils.toArray(params));
	}

	public String handlerQueryCondition(OperLogQueryCondition queryco, String sql, ArrayList<Object> params) {
		if (queryco != null) {

			if (queryco.getLoginUser() != null && !queryco.getLoginUser().equals("")) {
				sql += " and login_user=?";
				params.add(queryco.getLoginUser());
			}

			if (queryco.getUserName() != null && !queryco.getUserName().equals("")) {
				sql += " and user_name=?";
				params.add(queryco.getUserName());
			}

			if (queryco.getStartTime() != null && !queryco.getStartTime().equals("")) {
				sql += " and create_time>=?";
				params.add(queryco.getStartTime());
			}
			if (queryco.getEndTime() != null && !queryco.getEndTime().equals("")) {
				sql += " and create_time<=?";
				params.add(queryco.getEndTime());
			}
		}
		sql += " order by create_time desc";
		return sql;
	}
}
