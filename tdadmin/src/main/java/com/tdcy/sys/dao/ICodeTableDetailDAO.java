package com.tdcy.sys.dao;

import java.util.List;

import com.tdcy.framework.dao.IDao;
import com.tdcy.sys.dao.eo.CodeTableDetailEO;

public interface ICodeTableDetailDAO extends IDao {
	public List<CodeTableDetailEO> getCodeDetailList(String codeType);
}
