package com.tdcy.sys.dao;

import com.tdcy.framework.bean.PageInfo;
import com.tdcy.framework.dao.IDao;
import com.tdcy.sys.dao.eo.SmsTemplateEO;
import com.tdcy.sys.service.bean.SmsTemplateCo;

/**
 * @Description DAO类
 */
public interface ISmsTemplateDAO extends IDao {
	public PageInfo<SmsTemplateEO> findAllSmsTemplate(SmsTemplateCo smsTemplateCo);

	public SmsTemplateEO findSmsTemplateByTmpId(String tmpId);

	public int deletesmstemp(String smstempids);

}
