package com.tdcy.sys.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;
/**
* @Description EO类
*/
@Table(name = "sys_user")
public class UserEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "user_id")
	private Integer userId;
	/**
	* 
	*/
	@Column(name = "user_kind")
	private Integer userKind;
	/**
	* 
	*/
	@Column(name = "user_name")
	private String userName;
	/**
	* 
	*/
	@Column(name = "password")
	private String password;
	/**
	* 
	*/
	@Column(name = "login_user")
	private String loginUser;
	/**
	* 
	*/
	@Column(name = "create_time")
	private java.util.Date createTime;
	/**
	* 
	*/
	@Column(name = "last_logintime")
	private java.util.Date lastLogintime;
	/**
	* 
	*/
	@Column(name = "last_logouttime")
	private java.util.Date lastLogouttime;
	/**
	* 
	*/
	@Column(name = "lock_state")
	private String lockState;
	/**
	* 
	*/
	@Column(name = "lock_time")
	private java.util.Date lockTime;
	/**
	* 
	*/
	@Column(name = "lock_reason")
	private String lockReason;
	/**
	* 
	*/
	@Column(name = "password_errorcount")
	private Integer passwordErrorcount;
	
	/**
	* 获取 
	*/ 
	public Integer getUserId() {
		return userId;
	}
	/**
	* 设置
	*/
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	/**
	* 获取 
	*/ 
	public Integer getUserKind() {
		return userKind;
	}
	/**
	* 设置
	*/
	public void setUserKind(Integer userKind) {
		this.userKind = userKind;
	}
	/**
	* 获取 
	*/ 
	public String getUserName() {
		return userName;
	}
	/**
	* 设置
	*/
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	* 获取 
	*/ 
	public String getPassword() {
		return password;
	}
	/**
	* 设置
	*/
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	* 获取 
	*/ 
	public String getLoginUser() {
		return loginUser;
	}
	/**
	* 设置
	*/
	public void setLoginUser(String loginUser) {
		this.loginUser = loginUser;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getCreateTime() {
		return createTime;
	}
	/**
	* 设置
	*/
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getLastLogintime() {
		return lastLogintime;
	}
	/**
	* 设置
	*/
	public void setLastLogintime(java.util.Date lastLogintime) {
		this.lastLogintime = lastLogintime;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getLastLogouttime() {
		return lastLogouttime;
	}
	/**
	* 设置
	*/
	public void setLastLogouttime(java.util.Date lastLogouttime) {
		this.lastLogouttime = lastLogouttime;
	}
	/**
	* 获取 
	*/ 
	public String getLockState() {
		return lockState;
	}
	/**
	* 设置
	*/
	public void setLockState(String lockState) {
		this.lockState = lockState;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getLockTime() {
		return lockTime;
	}
	/**
	* 设置
	*/
	public void setLockTime(java.util.Date lockTime) {
		this.lockTime = lockTime;
	}
	/**
	* 获取 
	*/ 
	public String getLockReason() {
		return lockReason;
	}
	/**
	* 设置
	*/
	public void setLockReason(String lockReason) {
		this.lockReason = lockReason;
	}
	/**
	* 获取 
	*/ 
	public Integer getPasswordErrorcount() {
		return passwordErrorcount;
	}
	/**
	* 设置
	*/
	public void setPasswordErrorcount(Integer passwordErrorcount) {
		this.passwordErrorcount = passwordErrorcount;
	}
}

