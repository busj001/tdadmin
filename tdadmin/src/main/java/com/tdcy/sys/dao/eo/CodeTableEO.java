package com.tdcy.sys.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;

@Table(name = "sys_code_table")
public class CodeTableEO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "code_type")
	private String codeType;

	@Column(name = "code_name")
	private String codeName;

	@Column(name = "code_sql")
	private String codeSql;

	public String getCodeType() {
		return codeType;
	}

	public void setCodeType(String codeType) {
		this.codeType = codeType;
	}

	public String getCodeName() {
		return codeName;
	}

	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}

	public String getCodeSql() {
		return codeSql;
	}

	public void setCodeSql(String codeSql) {
		this.codeSql = codeSql;
	}

}
