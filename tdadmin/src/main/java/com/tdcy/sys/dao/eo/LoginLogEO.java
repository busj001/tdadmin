package com.tdcy.sys.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Table;
/**
* @Description EO类
*/
@Table(name = "sys_login_log")
public class LoginLogEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Column(name = "login_id")
	private Integer loginId;
	/**
	* 
	*/
	@Column(name = "login_user")
	private String loginUser;
	/**
	* 
	*/
	@Column(name = "user_id")
	private Integer userId;
	/**
	* 
	*/
	@Column(name = "user_name")
	private String userName;
	/**
	* 
	*/
	@Column(name = "login_address")
	private String loginAddress;
	/**
	* 
	*/
	@Column(name = "login_date")
	private java.util.Date loginDate;
	/**
	* 
	*/
	@Column(name = "logout_date")
	private java.util.Date logoutDate;
	/**
	* 
	*/
	@Column(name = "login_flag")
	private Integer loginFlag;
	/**
	* 
	*/
	@Column(name = "logout_flag")
	private Integer logoutFlag;
	/**
	* 
	*/
	@Column(name = "remark")
	private String remark;
	/**
	* 
	*/
	@Column(name = "valid_flag")
	private Integer validFlag;
	/**
	* 获取 
	*/ 
	public Integer getLoginId() {
		return loginId;
	}
	/**
	* 设置
	*/
	public void setLoginId(Integer loginId) {
		this.loginId = loginId;
	}
	/**
	* 获取 
	*/ 
	public String getLoginUser() {
		return loginUser;
	}
	/**
	* 设置
	*/
	public void setLoginUser(String loginUser) {
		this.loginUser = loginUser;
	}
	/**
	* 获取 
	*/ 
	public Integer getUserId() {
		return userId;
	}
	/**
	* 设置
	*/
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	/**
	* 获取 
	*/ 
	public String getUserName() {
		return userName;
	}
	/**
	* 设置
	*/
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	* 获取 
	*/ 
	public String getLoginAddress() {
		return loginAddress;
	}
	/**
	* 设置
	*/
	public void setLoginAddress(String loginAddress) {
		this.loginAddress = loginAddress;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getLoginDate() {
		return loginDate;
	}
	/**
	* 设置
	*/
	public void setLoginDate(java.util.Date loginDate) {
		this.loginDate = loginDate;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getLogoutDate() {
		return logoutDate;
	}
	/**
	* 设置
	*/
	public void setLogoutDate(java.util.Date logoutDate) {
		this.logoutDate = logoutDate;
	}
	/**
	* 获取 
	*/ 
	public Integer getLoginFlag() {
		return loginFlag;
	}
	/**
	* 设置
	*/
	public void setLoginFlag(Integer loginFlag) {
		this.loginFlag = loginFlag;
	}
	/**
	* 获取 
	*/ 
	public Integer getLogoutFlag() {
		return logoutFlag;
	}
	/**
	* 设置
	*/
	public void setLogoutFlag(Integer logoutFlag) {
		this.logoutFlag = logoutFlag;
	}
	/**
	* 获取 
	*/ 
	public String getRemark() {
		return remark;
	}
	/**
	* 设置
	*/
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	* 获取 
	*/ 
	public Integer getValidFlag() {
		return validFlag;
	}
	/**
	* 设置
	*/
	public void setValidFlag(Integer validFlag) {
		this.validFlag = validFlag;
	}
}

