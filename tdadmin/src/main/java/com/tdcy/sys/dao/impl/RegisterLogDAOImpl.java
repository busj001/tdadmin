package com.tdcy.sys.dao.impl;

import org.springframework.stereotype.Service;

import com.tdcy.framework.dao.DaoImpl;
import com.tdcy.sys.dao.IRegisterLogDAO;

/**
* @Description DAO实现类
*/
@Service
public class RegisterLogDAOImpl extends DaoImpl implements IRegisterLogDAO {

}
