package com.tdcy.sys.dao.impl;

import org.springframework.stereotype.Service;

import com.tdcy.framework.dao.DaoImpl;
import com.tdcy.sys.dao.IUserDAO;
import com.tdcy.sys.dao.eo.UserEO;

/**
 * @Description DAO实现类
 */
@Service
public class UserDAOImpl extends DaoImpl implements IUserDAO {
	@Override
	public UserEO findByLoginUser(String loginUser) {
		String sql = "select * from sys_user where  valid_flag = '1' and login_user = ?";
		return this.execSqlQueryOne(UserEO.class, sql,
				new Object[] { loginUser });
	}

	@Override
	public void updatePassByLoginUsers(String pass,String loginUsers) {
		String sql = "update sys_user set password = ? where login_user in ( "+loginUsers+")";
		this.execSqlUpdate(sql, new Object[]{pass});
	}

}
