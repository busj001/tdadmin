package com.tdcy.sys.dao;

import com.tdcy.framework.bean.PageInfo;
import com.tdcy.framework.dao.IDao;
import com.tdcy.sys.dao.eo.SmsSmartEO;
import com.tdcy.sys.service.bean.SmsSmartCo;

public interface ISmsSmartDAO extends IDao {
	public PageInfo<SmsSmartEO> findAllSmsSmart(SmsSmartCo smsSmartCo);

	public int deletesmart(String smartids);

}
