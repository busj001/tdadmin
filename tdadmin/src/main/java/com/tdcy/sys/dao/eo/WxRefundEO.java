package com.tdcy.sys.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;
/**
* @Description 预付订单表EO类
*/
@Table(name = "cmt_wx_refund")
public class WxRefundEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "wx_refund_id")
	private Integer wxRefundId;
	/**
	* 
	*/
	@Column(name = "jsid")
	private Integer jsid;
	/**
	* 微信支付订单号
	*/
	@Column(name = "wx_order_no")
	private String wxOrderNo;
	/**
	* 微信退款单号
	*/
	@Column(name = "wx_refund_no")
	private String wxRefundNo;
	/**
	* 订单编号
	*/
	@Column(name = "pay_bizno")
	private String payBizno;
	/**
	* 
	*/
	@Column(name = "pay_refund_no")
	private String payRefundNo;
	/**
	* 
	*/
	@Column(name = "total_fee")
	private Float totalFee;
	/**
	* 
	*/
	@Column(name = "refund_fee")
	private Float refundFee;
	/**
	* 订单描述
	*/
	@Column(name = "biz_desc")
	private String bizDesc;
	/**
	* 
	*/
	@Column(name = "refund_status")
	private String refundStatus;
	/**
	* 
	*/
	@Column(name = "create_time")
	private java.util.Date createTime;
	/**
	* 
	*/
	@Column(name = "valid_flag")
	private Integer validFlag;
	
	public Integer getWxRefundId() {
		return wxRefundId;
	}
	public void setWxRefundId(Integer wxRefundId) {
		this.wxRefundId = wxRefundId;
	}
	/**
	* 获取 
	*/ 
	public Integer getJsid() {
		return jsid;
	}
	/**
	* 设置
	*/
	public void setJsid(Integer jsid) {
		this.jsid = jsid;
	}
	/**
	* 获取 微信支付订单号
	*/ 
	public String getWxOrderNo() {
		return wxOrderNo;
	}
	/**
	* 设置微信支付订单号
	*/
	public void setWxOrderNo(String wxOrderNo) {
		this.wxOrderNo = wxOrderNo;
	}
	/**
	* 获取 微信退款单号
	*/ 
	public String getWxRefundNo() {
		return wxRefundNo;
	}
	/**
	* 设置微信退款单号
	*/
	public void setWxRefundNo(String wxRefundNo) {
		this.wxRefundNo = wxRefundNo;
	}
	/**
	* 获取 订单编号
	*/ 
	public String getPayBizno() {
		return payBizno;
	}
	/**
	* 设置订单编号
	*/
	public void setPayBizno(String payBizno) {
		this.payBizno = payBizno;
	}
	/**
	* 获取 
	*/ 
	public String getPayRefundNo() {
		return payRefundNo;
	}
	/**
	* 设置
	*/
	public void setPayRefundNo(String payRefundNo) {
		this.payRefundNo = payRefundNo;
	}
	/**
	* 获取 
	*/ 
	public Float getTotalFee() {
		return totalFee;
	}
	/**
	* 设置
	*/
	public void setTotalFee(Float totalFee) {
		this.totalFee = totalFee;
	}
	/**
	* 获取 
	*/ 
	public Float getRefundFee() {
		return refundFee;
	}
	/**
	* 设置
	*/
	public void setRefundFee(Float refundFee) {
		this.refundFee = refundFee;
	}
	/**
	* 获取 订单描述
	*/ 
	public String getBizDesc() {
		return bizDesc;
	}
	/**
	* 设置订单描述
	*/
	public void setBizDesc(String bizDesc) {
		this.bizDesc = bizDesc;
	}
	/**
	* 获取 
	*/ 
	public String getRefundStatus() {
		return refundStatus;
	}
	/**
	* 设置
	*/
	public void setRefundStatus(String refundStatus) {
		this.refundStatus = refundStatus;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getCreateTime() {
		return createTime;
	}
	/**
	* 设置
	*/
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	/**
	* 获取 
	*/ 
	public Integer getValidFlag() {
		return validFlag;
	}
	/**
	* 设置
	*/
	public void setValidFlag(Integer validFlag) {
		this.validFlag = validFlag;
	}
}

