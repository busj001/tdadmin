package com.tdcy.sys.dao.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.tdcy.framework.bean.PageInfo;
import com.tdcy.framework.dao.DaoImpl;
import com.tdcy.framework.util.StringUtils;
import com.tdcy.sys.dao.ISmsRecordDAO;
import com.tdcy.sys.dao.eo.SmsRecordEO;
import com.tdcy.sys.service.bean.SmsRecordCo;

/**
 * @Description DAO实现类
 */
@Service
public class SmsRecordDAOImpl extends DaoImpl implements ISmsRecordDAO {

	@SuppressWarnings("unchecked")
	@Override
	public PageInfo<SmsRecordEO> findAllSms(SmsRecordCo co) {
		String sql = " select sms_id,sms_phone,sms_content,send_time,create_time,status,remark,smstype from cmt_sms_record where  valid_flag =1 ";
		ArrayList<Object> p = new ArrayList<Object>();
		StringBuffer sb = new StringBuffer(sql);
		if (co != null) {
			if (co.getSmsPhone() != null && !co.getSmsPhone().trim().equals("")) {
				sb.append(" and sms_phone like ?");
				p.add("%" + co.getSmsPhone() + "%");
			}
			if (co.getSmsContent() != null && !co.getSmsContent().trim().equals("")) {
				sb.append(" and sms_content=?");
				p.add(co.getSmsContent() + "");
			}
			if (co.getStatus() != null && !co.getStatus().equals("")) {
				sb.append(" and status=?");
				p.add(co.getStatus() + "");
			}
			if (co.getSmstype() != null && !co.getSmstype().equals("")) {
				sb.append(" and smstype=?");
				p.add(co.getSmstype() + "");
			}
			if (co.getStartTime() != null) {
				sb.append(" and send_time>=?");
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String startTime = sdf.format(co.getStartTime());
				p.add(startTime + "");
			}
			if (co.getEndTime() != null) {
				sb.append(" and send_time<=?");
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String endTime = sdf.format(co.getEndTime());
				p.add(endTime + "");
			}
			if (co.getKey() != null && !co.getKey().trim().equals("")) {
				sb.append(" and (");
				sb.append(" sms_phone like ?");
				p.add("%" + co.getKey() + "%");
				sb.append(" or sms_content like ?");
				p.add("%" + co.getKey() + "%");
				sb.append(" )");
			}
		}
		sb.append(" order by create_time desc");
		return this.queryRecordByClassForPageInfo(sb.toString(), SmsRecordEO.class, co.getPageInfo(),
				StringUtils.toArray(p));

	}

	@Override
	public void updateSmsRecordStatus(String backMsgId, String status) {
		String sql = "update cmt_sms_record set status = ? where backsmsId = ?";
		this.execSqlUpdate(sql, new Object[] { status, backMsgId });
	}

}
