package com.tdcy.sys.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;
/**
* @Description EO类
*/
@Table(name = "sys_bulletins")
public class BulletinsEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "bulletin_id")
	private Integer bulletinId;
	@Column(name = "bulletin_title")
	private String bulletinTitle;
	/**
	* 
	*/
	@Column(name = "bulletin_content")
	private String bulletinContent;
	/**
	* 
	*/
	@Column(name = "file_batch")
	private String fileBatch;
	/**
	* 
	*/
	@Column(name = "sender_staff")
	private Integer senderStaff;
	/**
	* 
	*/
	@Column(name = "sender_man")
	private String senderMan;
	/**
	* 
	*/
	@Column(name = "send_date")
	private java.util.Date sendDate;
	/**
	* 
	*/
	@Column(name = "valid_flag")
	private Integer validFlag;
	/**
	* 获取 
	*/ 
	public Integer getBulletinId() {
		return bulletinId;
	}
	/**
	* 设置
	*/
	public void setBulletinId(Integer bulletinId) {
		this.bulletinId = bulletinId;
	}
	/**
	* 获取 
	*/ 
	public String getBulletinTitle() {
		return bulletinTitle;
	}
	/**
	* 设置
	*/
	public void setBulletinTitle(String bulletinTitle) {
		this.bulletinTitle = bulletinTitle;
	}
	/**
	* 获取 
	*/ 
	public String getBulletinContent() {
		return bulletinContent;
	}
	/**
	* 设置
	*/
	public void setBulletinContent(String bulletinContent) {
		this.bulletinContent = bulletinContent;
	}
	/**
	* 获取 
	*/ 
	public String getFileBatch() {
		return fileBatch;
	}
	/**
	* 设置
	*/
	public void setFileBatch(String fileBatch) {
		this.fileBatch = fileBatch;
	}
	/**
	* 获取 
	*/ 
	public Integer getSenderStaff() {
		return senderStaff;
	}
	/**
	* 设置
	*/
	public void setSenderStaff(Integer senderStaff) {
		this.senderStaff = senderStaff;
	}
	/**
	* 获取 
	*/ 
	public String getSenderMan() {
		return senderMan;
	}
	/**
	* 设置
	*/
	public void setSenderMan(String senderMan) {
		this.senderMan = senderMan;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getSendDate() {
		return sendDate;
	}
	/**
	* 设置
	*/
	public void setSendDate(java.util.Date sendDate) {
		this.sendDate = sendDate;
	}
	/**
	* 获取 
	*/ 
	public Integer getValidFlag() {
		return validFlag;
	}
	/**
	* 设置
	*/
	public void setValidFlag(Integer validFlag) {
		this.validFlag = validFlag;
	}
}

