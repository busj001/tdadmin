package com.tdcy.sys.dao;

import com.tdcy.framework.bean.PageInfo;
import com.tdcy.framework.dao.IDao;
import com.tdcy.sys.dao.eo.BulletinsEO;
import com.tdcy.sys.service.bean.BulletinQueryCondition;
/**
* @Description DAO类
*/
public interface IBulletinsDAO extends IDao {
	public PageInfo<BulletinsEO> getPageInfo(BulletinQueryCondition queryco);
}
