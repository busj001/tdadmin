package com.tdcy.sys.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;
/**
* @Description EO类
*/
@Table(name = "sys_sqls")
public class SqlsEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "sql_id")
	private Integer sqlId;
	/**
	* 
	*/
	@Column(name = "sql_name")
	private String sqlName;
	/**
	* 
	*/
	@Column(name = "sql_text")
	private String sqlText;
	/**
	* 
	*/
	@Column(name = "sql_desc")
	private String sqlDesc;
	/**
	* 
	*/
	@Column(name = "remark")
	private String remark;
	/**
	* 
	*/
	@Column(name = "last_date")
	private java.util.Date lastDate;
	/**
	* 
	*/
	@Column(name = "valid_flag")
	private Integer validFlag;
	/**
	* 获取 
	*/ 
	public Integer getSqlId() {
		return sqlId;
	}
	/**
	* 设置
	*/
	public void setSqlId(Integer sqlId) {
		this.sqlId = sqlId;
	}
	/**
	* 获取 
	*/ 
	public String getSqlName() {
		return sqlName;
	}
	/**
	* 设置
	*/
	public void setSqlName(String sqlName) {
		this.sqlName = sqlName;
	}
	/**
	* 获取 
	*/ 
	public String getSqlText() {
		return sqlText;
	}
	/**
	* 设置
	*/
	public void setSqlText(String sqlText) {
		this.sqlText = sqlText;
	}
	/**
	* 获取 
	*/ 
	public String getSqlDesc() {
		return sqlDesc;
	}
	/**
	* 设置
	*/
	public void setSqlDesc(String sqlDesc) {
		this.sqlDesc = sqlDesc;
	}
	/**
	* 获取 
	*/ 
	public String getRemark() {
		return remark;
	}
	/**
	* 设置
	*/
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getLastDate() {
		return lastDate;
	}
	/**
	* 设置
	*/
	public void setLastDate(java.util.Date lastDate) {
		this.lastDate = lastDate;
	}
	/**
	* 获取 
	*/ 
	public Integer getValidFlag() {
		return validFlag;
	}
	/**
	* 设置
	*/
	public void setValidFlag(Integer validFlag) {
		this.validFlag = validFlag;
	}
}

