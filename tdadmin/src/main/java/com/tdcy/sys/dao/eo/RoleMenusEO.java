package com.tdcy.sys.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;
/**
* @Description EO类
*/
@Table(name = "sys_role_menus")
public class RoleMenusEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "role_id")
	private Integer roleId;
	/**
	* 
	*/
	@Column(name = "menu_id")
	private Integer menuId;
	/**
	* 
	*/
	@Column(name = "grant_option")
	private Integer grantOption;
	
	@Column(name = "role_name",isdbcol=false)
	private String roleName;

	@Column(name = "menu_name",isdbcol=false)
	private String menuName;
	
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	/**
	* 获取 
	*/ 
	public Integer getRoleId() {
		return roleId;
	}
	/**
	* 设置
	*/
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	/**
	* 获取 
	*/ 
	public Integer getMenuId() {
		return menuId;
	}
	/**
	* 设置
	*/
	public void setMenuId(Integer menuId) {
		this.menuId = menuId;
	}
	/**
	* 获取 
	*/ 
	public Integer getGrantOption() {
		return grantOption;
	}
	/**
	* 设置
	*/
	public void setGrantOption(Integer grantOption) {
		this.grantOption = grantOption;
	}
}

