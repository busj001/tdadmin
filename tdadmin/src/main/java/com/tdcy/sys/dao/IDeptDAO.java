package com.tdcy.sys.dao;

import com.tdcy.framework.bean.PageInfo;
import com.tdcy.framework.dao.IDao;
import com.tdcy.sys.dao.eo.DeptEO;
import com.tdcy.sys.service.bean.QueryCondition;

/**
 * @Description 部门DAO类
 */
public interface IDeptDAO extends IDao {
	public PageInfo<DeptEO> getPageInfo(QueryCondition qc);
}
