package com.tdcy.sys.dao;

import com.tdcy.framework.bean.PageInfo;
import com.tdcy.framework.dao.IDao;
import com.tdcy.sys.dao.eo.LoginLogEO;
import com.tdcy.sys.service.bean.LoginLogQueryCondition;

public interface ILoginLogDAO extends IDao {
	public PageInfo<LoginLogEO> getPageInfo(LoginLogQueryCondition queryco);
}
