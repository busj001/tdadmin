package com.tdcy.sys.dao.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.tdcy.framework.dao.DaoImpl;
import com.tdcy.sys.dao.IRoleMenusDAO;
import com.tdcy.sys.dao.eo.MenuEO;

/**
 * @Description DAO实现类
 */
@Service
public class RoleMenusDAOImpl extends DaoImpl implements IRoleMenusDAO {

	@Override
	public void deleteByRoleId(Integer roleid) {
		String sql = "delete from sys_role_menus where valid_flag = '1' and role_id = ?";
		this.execSqlUpdate(sql, new Object[] { roleid });
	}

	@Override
	public void batchSave(List<Object[]> savep) {
		String sql = "insert into sys_role_menus (role_id,menu_id) values(?,?)";
		this.execSqlUpdateBatch(sql, savep.size(), savep);

	}

	@Override
	public List<MenuEO> getAllMenusByRoleId(Integer roleid) {
		String sql = "select c.* from sys_role_menus a	,sys_roles b, sys_menu c " + "  where a.menu_id = c.menu_id and a.role_id = b.role_id "
				+ " and a.valid_flag = '1' and b.valid_flag = '1' and c.valid_flag = '1' and c.menu_type = '1' " //
				+ "  and  a.role_id = ?";
		return this.execSqlQuery(MenuEO.class, sql, new Object[] { roleid });
	}

	@Override
	public List<MenuEO> getAllRightsByRoleId(Integer roleid) {
		String sql = "select c.* from sys_role_menus a	,sys_roles b, sys_menu c " + "  where a.menu_id = c.menu_id and a.role_id = b.role_id "
				+ " and a.valid_flag = '1' and b.valid_flag = '1' and c.valid_flag = '1'  and c.menu_type = '2'" //
				+ "  and  a.role_id = ?";
		return this.execSqlQuery(MenuEO.class, sql, new Object[] { roleid });
	}

}
