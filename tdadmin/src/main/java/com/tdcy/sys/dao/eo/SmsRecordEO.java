package com.tdcy.sys.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;

/**
 * @Description EO类
 */
@Table(name = "cmt_sms_record")
public class SmsRecordEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 短信id
	 */
	@Id
	@Column(name = "sms_id")
	private Integer smsId;
	/**
	* 
	*/
	// @Column(name = "sms_name")
	private String smsName;
	/**
	* 
	*/
	@Column(name = "sms_phone")
	private String smsPhone;
	/**
	* 
	*/
	@Column(name = "sms_content")
	private String smsContent;
	/**
	* 
	*/
	@Column(name = "send_time")
	private java.util.Date sendTime;
	/**
	* 
	*/
	@Column(name = "create_time")
	private java.util.Date createTime;
	/**
	* 
	*/
	@Column(name = "status")
	private Integer status;
	/**
	* 
	*/
	@Column(name = "remark")
	private String remark;

	/**
	 * 1:验证类短信 2 通知类短信
	 */
	 @Column(name = "smstype")
	private Integer smsType;
	/**
	 * 如果为验证类，此字段为验证码
	 */
	// @Column(name = "checkcode")
	private String checkcode;
	/**
	 * 短信模板ID
	 */
	@Column(name = "tmp_id")
	private String tmpId;
	/**
	 * 短信平台短信id
	 */
	@Column(name = "backsmsid")
	private String backsmsid;
	/**
	 * 业务id
	 */
	// @Column(name = "biz_id")
	private String bizId;
	/**
	 * 业务id
	 */
	@Column(name = "jsid")
	private Integer jsid;
	/**
	* 
	*/
	@Column(name = "valid_flag")
	private Integer validFlag;

	/**
	 * 业务id
	 */
	@Column(name = "respcode")
	private String respcode;

	/**
	 * 业务id
	 */
	@Column(name = "respmsg")
	private String respmsg;

	/**
	 * 类型
	 */
	@Column(name = "smstype")
	private Integer smstype;

	public Integer getJsid() {
		return jsid;
	}

	public void setJsid(Integer jsid) {
		this.jsid = jsid;
	}

	public String getRespcode() {
		return respcode;
	}

	public void setRespcode(String respcode) {
		this.respcode = respcode;
	}

	public String getRespmsg() {
		return respmsg;
	}

	public void setRespmsg(String respmsg) {
		this.respmsg = respmsg;
	}

	/**
	 * 获取 短信id
	 */
	public Integer getSmsId() {
		return smsId;
	}

	/**
	 * 设置短信id
	 */
	public void setSmsId(Integer smsId) {
		this.smsId = smsId;
	}

	/**
	 * 获取
	 */
	public String getSmsName() {
		return smsName;
	}

	/**
	 * 设置
	 */
	public void setSmsName(String smsName) {
		this.smsName = smsName;
	}

	/**
	 * 获取
	 */
	public String getSmsPhone() {
		return smsPhone;
	}

	/**
	 * 设置
	 */
	public void setSmsPhone(String smsPhone) {
		this.smsPhone = smsPhone;
	}

	/**
	 * 获取
	 */
	public String getSmsContent() {
		return smsContent;
	}

	/**
	 * 设置
	 */
	public void setSmsContent(String smsContent) {
		this.smsContent = smsContent;
	}

	/**
	 * 获取
	 */
	public java.util.Date getSendTime() {
		return sendTime;
	}

	/**
	 * 设置
	 */
	public void setSendTime(java.util.Date sendTime) {
		this.sendTime = sendTime;
	}

	/**
	 * 获取
	 */
	public java.util.Date getCreateTime() {
		return createTime;
	}

	/**
	 * 设置
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * 获取
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * 设置
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * 获取
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * 设置
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * 获取 1:验证类短信 2 通知类短信
	 */
	public Integer getSmsType() {
		return smsType;
	}

	/**
	 * 设置1:验证类短信 2 通知类短信
	 */
	public void setSmsType(Integer smsType) {
		this.smsType = smsType;
	}

	/**
	 * 获取 如果为验证类，此字段为验证码
	 */
	public String getCheckcode() {
		return checkcode;
	}

	/**
	 * 设置如果为验证类，此字段为验证码
	 */
	public void setCheckcode(String checkcode) {
		this.checkcode = checkcode;
	}

	/**
	 * 获取 短信模板ID
	 */
	public String getTmpId() {
		return tmpId;
	}

	/**
	 * 设置短信模板ID
	 */
	public void setTmpId(String tmpId) {
		this.tmpId = tmpId;
	}

	/**
	 * 获取 短信平台短信id
	 */
	public String getBacksmsid() {
		return backsmsid;
	}

	/**
	 * 设置短信平台短信id
	 */
	public void setBacksmsid(String backsmsid) {
		this.backsmsid = backsmsid;
	}

	/**
	 * 获取 业务id
	 */
	public String getBizId() {
		return bizId;
	}

	/**
	 * 设置业务id
	 */
	public void setBizId(String bizId) {
		this.bizId = bizId;
	}

	public Integer getSmstype() {
		return smstype;
	}

	public void setSmstype(Integer smstype) {
		this.smstype = smstype;
	}

	/**
	 * 获取
	 */
	public Integer getValidFlag() {
		return validFlag;
	}

	/**
	 * 设置
	 */
	public void setValidFlag(Integer validFlag) {
		this.validFlag = validFlag;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
