package com.tdcy.sys.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;

/**
 * @Description EO类
 */
@Table(name = "cmt_sms_template")
public class SmsTemplateEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 短信id
	 */
	@Id
	@Column(name = "tpid")
	private Integer tpid;
	/**
	* 
	*/
	@Column(name = "platTmpId")
	private String platTmpId;
	/**
	* 
	*/
	@Column(name = "platTmpName")
	private String platTmpName;
	/**
	* 
	*/
	@Column(name = "tmpContent")
	private String tmpContent;

	@Column(name = "valid_flag")
	private String validFlag;

	public Integer getTpid() {
		return tpid;
	}

	public void setTpid(Integer tpid) {
		this.tpid = tpid;
	}

	public String getPlatTmpId() {
		return platTmpId;
	}

	public void setPlatTmpId(String platTmpId) {
		this.platTmpId = platTmpId;
	}

	public String getPlatTmpName() {
		return platTmpName;
	}

	public void setPlatTmpName(String platTmpName) {
		this.platTmpName = platTmpName;
	}

	public String getTmpContent() {
		return tmpContent;
	}

	public void setTmpContent(String tmpContent) {
		this.tmpContent = tmpContent;
	}

	public String getValidFlag() {
		return validFlag;
	}

	public void setValidFlag(String validFlag) {
		this.validFlag = validFlag;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
