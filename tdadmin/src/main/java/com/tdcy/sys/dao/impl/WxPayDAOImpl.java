package com.tdcy.sys.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.tdcy.framework.bean.PageInfo;
import com.tdcy.framework.dao.DaoImpl;
import com.tdcy.framework.util.StringUtils;
import com.tdcy.sys.dao.IWxPayDAO;
import com.tdcy.sys.dao.eo.WxPayEO;
import com.tdcy.sys.service.bean.WxPayCondition;

/**
 * @Description 预付订单表DAO实现类
 */
@Service
public class WxPayDAOImpl extends DaoImpl implements IWxPayDAO {

	@Override
	public void updateByPayBizNo(String paybizNo, String wxBizNo, String payStatus) {
		String sql = "update cmt_wx_pay set wx_order_no = ? , pay_status = ? where pay_bizno = ? and valid_flag='1'";
		this.execSqlUpdate(sql, new Object[] { wxBizNo, payStatus, paybizNo });
	}

	public PageInfo<WxPayEO> getPageInfo(WxPayCondition queryco) {
		String sql = "select * from cmt_wx_pay where 1=1 and valid_flag = '1' ";
		List<Object> list = new ArrayList<Object>();

		if (queryco.getPayStatus() != null) {
			sql += " and pay_status = ?";
			list.add(queryco.getPayStatus());
		}

		return this.queryRecordByClassForPageInfo(sql, WxPayEO.class, queryco.getPageInfo(), StringUtils.toArray(list));
	}

	@Override
	public WxPayEO queryByBizNo(String bizNo) {
		String sql = "select * from  cmt_wx_pay where   valid_flag = '1' and  pay_bizno = ?";
		return this.execSqlQueryOne(WxPayEO.class, sql, new Object[] { bizNo });
	}
	
	@Override
	public boolean hasPayRecord(String bizNo) {
		String sql = "select * from  cmt_wx_pay where   valid_flag = '1' and  pay_bizno = ? and pay_status = 'SUCCESS'";
		WxPayEO e= this.execSqlQueryOne(WxPayEO.class, sql, new Object[] { bizNo });
		if(e==null){
			return false;
		}else{
			return true;
		}
	}

	@Override
	public void deleteByPyBizNo(String bizNo) {
		String sql = "delete from  cmt_wx_pay where  valid_flag = '1' and pay_bizno = ?";
		this.execSqlUpdate(sql, new Object[] { bizNo });
	}
}
