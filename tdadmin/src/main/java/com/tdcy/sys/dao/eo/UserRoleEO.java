package com.tdcy.sys.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;

/**
 * @Description EO类
 */
@Table(name = "sys_user_role")
public class UserRoleEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "user_id")
	private Integer userId;
	/**
	* 
	*/
	@Column(name = "role_id")
	private Integer roleId;

	/**
	* 
	*/
	@Column(name = "valid_flag")
	private Integer validFlag;
	
	@Column(name = "user_name",isdbcol=false)
	private String userName;
	@Column(name = "user_kind",isdbcol=false)
	private String userKind;
	@Column(name = "login_user",isdbcol=false)
	private String loginUser;
	@Column(name = "role_name",isdbcol=false)
	private String roleName;


	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserKind() {
		return userKind;
	}

	public void setUserKind(String userKind) {
		this.userKind = userKind;
	}

	public String getLoginUser() {
		return loginUser;
	}

	public void setLoginUser(String loginUser) {
		this.loginUser = loginUser;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Integer getValidFlag() {
		return validFlag;
	}

	public void setValidFlag(Integer validFlag) {
		this.validFlag = validFlag;
	}

}
