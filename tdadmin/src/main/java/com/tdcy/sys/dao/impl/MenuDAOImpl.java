package com.tdcy.sys.dao.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.tdcy.framework.dao.DaoImpl;
import com.tdcy.sys.dao.IMenuDAO;
import com.tdcy.sys.dao.eo.MenuEO;

/**
 * @Description DAO实现类
 */
@Service
public class MenuDAOImpl extends DaoImpl implements IMenuDAO {

	@Override
	public List<MenuEO> findMenuByRoleId(Integer roleId) {
		String sql = "select a.* from sys_menu a ,sys_role_menus b where a.valid_flag = '1' and b.valid_flag = '1' and a.menu_id = b.menu_id and b.role_id = ?";
		return execSqlQuery(MenuEO.class, sql, new Object[] { roleId });
	}
}
