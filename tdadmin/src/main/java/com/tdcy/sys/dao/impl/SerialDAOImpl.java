package com.tdcy.sys.dao.impl;

import org.springframework.stereotype.Service;

import com.tdcy.framework.dao.DaoImpl;
import com.tdcy.sys.dao.ISerialDAO;
import com.tdcy.sys.dao.eo.SerialEO;

@Service
public class SerialDAOImpl extends DaoImpl implements ISerialDAO {

	@Override
	public SerialEO querySerialEO(String MaxNo_Name) {
		String getsql = "select * from sys_serial" + " where serial_type='" + MaxNo_Name+"' for update";
		return this.execSqlQueryOne(SerialEO.class, getsql);
	}

}
