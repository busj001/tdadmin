package com.tdcy.sys.dao;

import com.tdcy.framework.dao.IDao;
import com.tdcy.sys.dao.eo.SerialEO;

public interface ISerialDAO extends IDao {
	public SerialEO querySerialEO(String MaxNo_Name);
}
