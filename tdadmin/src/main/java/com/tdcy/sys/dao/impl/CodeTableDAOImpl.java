package com.tdcy.sys.dao.impl;

import org.springframework.stereotype.Service;

import com.tdcy.framework.dao.DaoImpl;
import com.tdcy.sys.dao.ICodeTableDAO;
import com.tdcy.sys.dao.eo.CodeTableEO;

@Service
public class CodeTableDAOImpl extends DaoImpl implements ICodeTableDAO {
	@Override
	public CodeTableEO getCodeTable(String codeType) {
		String sql = "select * from sys_code_table where code_type = ?";
		return this.execSqlQueryOne(CodeTableEO.class, sql,new Object[]{codeType});
	}

}
