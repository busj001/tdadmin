package com.tdcy.sys.dao.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.tdcy.framework.dao.DaoImpl;
import com.tdcy.sys.dao.IUserRoleDAO;
import com.tdcy.sys.dao.eo.RolesEO;
import com.tdcy.sys.dao.eo.UserEO;
import com.tdcy.sys.dao.eo.UserRoleEO;

/**
 * @Description DAO实现类
 */
@Service
public class UserRoleDAOImpl extends DaoImpl implements IUserRoleDAO {
	@Override
	public List<UserEO> getUserByRoleId(Integer roleId) {
		String sql = " select b.* from sys_user_role a,sys_user b,sys_roles c " + " where a.user_id = b.user_id and a.role_id = c.role_id "
				+ " and a.valid_flag = '1' and b.valid_flag = '1' and c.valid_flag = '1'" + " and a.role_id = ?";
		return execSqlQuery(UserRoleEO.class, sql, new Object[] { roleId });
	}

	@Override
	public void batchSave(List<Object[]> savep) {
		String sql = "insert into sys_user_role (user_id,role_id) values(?,?)";
		this.execSqlUpdateBatch(sql, savep.size(), savep);
	}

	@Override
	public void deleteByUserId(Integer userid) {
		String sql = "delete from sys_user_role where user_id = ?";
		this.execSqlUpdate(sql, new Object[] { userid });
	}

	@Override
	public List<RolesEO> getRolesByUserId(Integer userid) {
		String sql = "select a.* from sys_user_role b,sys_roles a where a.valid_flag = '1' and b.valid_flag = '1' and a.role_id = b.role_id and b.user_id = ?";
		return this.execSqlQuery(RolesEO.class, sql, new Object[] { userid });
	}
}
