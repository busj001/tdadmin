package com.tdcy.sys.dao;

import java.util.List;

import com.tdcy.framework.dao.IDao;
import com.tdcy.sys.dao.eo.RolesEO;
import com.tdcy.sys.dao.eo.UserEO;

/**
 * @Description DAO类
 */
public interface IUserRoleDAO extends IDao {
	public List<UserEO> getUserByRoleId(Integer roleId);

	public List<RolesEO> getRolesByUserId(Integer userid);

	public void batchSave(List<Object[]> savep);

	public void deleteByUserId(Integer userid);
}
