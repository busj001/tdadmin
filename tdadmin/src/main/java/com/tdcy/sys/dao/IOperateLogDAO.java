package com.tdcy.sys.dao;

import java.util.List;

import com.tdcy.framework.bean.PageInfo;
import com.tdcy.framework.dao.IDao;
import com.tdcy.sys.dao.eo.OperateLogEO;
import com.tdcy.sys.service.bean.OperLogQueryCondition;

/**
 * @Description DAO类
 */
public interface IOperateLogDAO extends IDao {
	public PageInfo<OperateLogEO> getPageInfo(OperLogQueryCondition queryco);

	public List<OperateLogEO> getOperateList(OperLogQueryCondition queryco);
}
