package com.tdcy.sys.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;

/**
 * @Description 部门EO类
 */
@Table(name = "sys_dept")
public class DeptEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 部门ID
	 */
	@Id
	@Column(name = "dept_id")
	private Integer deptId;
	/**
	* 
	*/
	@Column(name = "dept_code")
	private String deptCode;
	/**
	* 
	*/
	@Column(name = "dept_up_id")
	private Integer deptUpId;
	/**
	 * 部门名称
	 */
	@Column(name = "dept_name")
	private String deptName;
	/**
	* 
	*/
	@Column(name = "tel")
	private String tel;
	/**
	* 
	*/
	@Column(name = "remark")
	private String remark;
	/**
	* 
	*/
	@Column(name = "creat_time")
	private java.util.Date creatTime;
	/**
	 * 0 否1是
	 */
	@Column(name = "valid_flag")
	private Integer validFlag;

	/**
	 * 获取 部门ID
	 */
	public Integer getDeptId() {
		return deptId;
	}

	/**
	 * 设置部门ID
	 */
	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}

	/**
	 * 获取
	 */
	public String getDeptCode() {
		return deptCode;
	}

	/**
	 * 设置
	 */
	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}

	/**
	 * 获取
	 */
	public Integer getDeptUpId() {
		return deptUpId;
	}

	/**
	 * 设置
	 */
	public void setDeptUpId(Integer deptUpId) {
		this.deptUpId = deptUpId;
	}

	/**
	 * 获取 部门名称
	 */
	public String getDeptName() {
		return deptName;
	}

	/**
	 * 设置部门名称
	 */
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	/**
	 * 获取
	 */
	public String getTel() {
		return tel;
	}

	/**
	 * 设置
	 */
	public void setTel(String tel) {
		this.tel = tel;
	}

	/**
	 * 获取
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * 设置
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * 获取
	 */
	public java.util.Date getCreatTime() {
		return creatTime;
	}

	/**
	 * 设置
	 */
	public void setCreatTime(java.util.Date creatTime) {
		this.creatTime = creatTime;
	}

	/**
	 * 获取 0 否1是
	 */
	public Integer getValidFlag() {
		return validFlag;
	}

	/**
	 * 设置0 否1是
	 */
	public void setValidFlag(Integer validFlag) {
		this.validFlag = validFlag;
	}
}
