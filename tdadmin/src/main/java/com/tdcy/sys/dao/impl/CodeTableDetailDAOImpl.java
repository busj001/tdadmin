package com.tdcy.sys.dao.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.tdcy.framework.dao.DaoImpl;
import com.tdcy.sys.dao.ICodeTableDetailDAO;
import com.tdcy.sys.dao.eo.CodeTableDetailEO;

@Service
public class CodeTableDetailDAOImpl extends DaoImpl implements
		ICodeTableDetailDAO {

	@Override
	public List<CodeTableDetailEO> getCodeDetailList(String codeType) {
		String sql = "select * from sys_code_table_detail where code_type = ?";
		return this.execSqlQuery(CodeTableDetailEO.class, sql,
				new Object[] { codeType });
	}

}
