package com.tdcy.sys.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;
/**
* @Description EO类
*/
@Table(name = "sys_system_log")
public class SystemLogEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "log_sn")
	private Integer logSn;
	/**
	* 
	*/
	@Column(name = "server_name")
	private String serverName;
	/**
	* 
	*/
	@Column(name = "log_date")
	private java.util.Date logDate;
	/**
	* 
	*/
	@Column(name = "log_level")
	private String logLevel;
	/**
	* 
	*/
	@Column(name = "log_name")
	private String logName;
	/**
	* 
	*/
	@Column(name = "log_message")
	private String logMessage;
	/**
	* 
	*/
	@Column(name = "log_exception")
	private String logException;
	/**
	* 
	*/
	@Column(name = "login_user")
	private String loginUser;
	/**
	* 
	*/
	@Column(name = "user_address")
	private String userAddress;
	/**
	* 
	*/
	@Column(name = "operate_action")
	private String operateAction;
	/**
	* 
	*/
	@Column(name = "operate_name")
	private String operateName;
	/**
	* 
	*/
	@Column(name = "biz_id")
	private String bizId;
	/**
	* 获取 
	*/ 
	public Integer getLogSn() {
		return logSn;
	}
	/**
	* 设置
	*/
	public void setLogSn(Integer logSn) {
		this.logSn = logSn;
	}
	/**
	* 获取 
	*/ 
	public String getServerName() {
		return serverName;
	}
	/**
	* 设置
	*/
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getLogDate() {
		return logDate;
	}
	/**
	* 设置
	*/
	public void setLogDate(java.util.Date logDate) {
		this.logDate = logDate;
	}
	/**
	* 获取 
	*/ 
	public String getLogLevel() {
		return logLevel;
	}
	/**
	* 设置
	*/
	public void setLogLevel(String logLevel) {
		this.logLevel = logLevel;
	}
	/**
	* 获取 
	*/ 
	public String getLogName() {
		return logName;
	}
	/**
	* 设置
	*/
	public void setLogName(String logName) {
		this.logName = logName;
	}
	/**
	* 获取 
	*/ 
	public String getLogMessage() {
		return logMessage;
	}
	/**
	* 设置
	*/
	public void setLogMessage(String logMessage) {
		this.logMessage = logMessage;
	}
	/**
	* 获取 
	*/ 
	public String getLogException() {
		return logException;
	}
	/**
	* 设置
	*/
	public void setLogException(String logException) {
		this.logException = logException;
	}
	/**
	* 获取 
	*/ 
	public String getLoginUser() {
		return loginUser;
	}
	/**
	* 设置
	*/
	public void setLoginUser(String loginUser) {
		this.loginUser = loginUser;
	}
	/**
	* 获取 
	*/ 
	public String getUserAddress() {
		return userAddress;
	}
	/**
	* 设置
	*/
	public void setUserAddress(String userAddress) {
		this.userAddress = userAddress;
	}
	/**
	* 获取 
	*/ 
	public String getOperateAction() {
		return operateAction;
	}
	/**
	* 设置
	*/
	public void setOperateAction(String operateAction) {
		this.operateAction = operateAction;
	}
	/**
	* 获取 
	*/ 
	public String getOperateName() {
		return operateName;
	}
	/**
	* 设置
	*/
	public void setOperateName(String operateName) {
		this.operateName = operateName;
	}
	/**
	* 获取 
	*/ 
	public String getBizId() {
		return bizId;
	}
	/**
	* 设置
	*/
	public void setBizId(String bizId) {
		this.bizId = bizId;
	}
}

