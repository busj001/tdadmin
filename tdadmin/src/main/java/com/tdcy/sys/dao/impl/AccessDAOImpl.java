package com.tdcy.sys.dao.impl;

import org.springframework.stereotype.Service;

import com.tdcy.framework.dao.DaoImpl;
import com.tdcy.sys.dao.IAccessDAO;
import com.tdcy.sys.dao.eo.AccessEO;

/**
 * @Description DAO实现类
 */
@Service
public class AccessDAOImpl extends DaoImpl implements IAccessDAO {
	public AccessEO findByAppId(String app_id) {
		String sql = "select * from sys_access where app_id = ?";
		return this.execSqlQueryOne(AccessEO.class, sql, new Object[] { app_id });
	}
}
