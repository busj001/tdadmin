package com.tdcy.sys.dao.impl;

import org.springframework.stereotype.Service;

import com.tdcy.framework.dao.DaoImpl;
import com.tdcy.sys.dao.IAreaDAO;
import com.tdcy.sys.dao.eo.AreaEO;

/**
 * @Description 行政区划DAO实现类
 */
@Service
public class AreaDAOImpl extends DaoImpl implements IAreaDAO {

	@Override
	public AreaEO findByCode(String code) {
		String sql = "select * from sys_area where area_code = ?";
		return this.execSqlQueryOne(AreaEO.class, sql, new Object[] { code });
	}


}
