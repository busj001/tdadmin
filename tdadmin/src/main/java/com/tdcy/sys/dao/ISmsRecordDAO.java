package com.tdcy.sys.dao;

import com.tdcy.framework.bean.PageInfo;
import com.tdcy.framework.dao.IDao;
import com.tdcy.sys.dao.eo.SmsRecordEO;
import com.tdcy.sys.service.bean.SmsRecordCo;

public interface ISmsRecordDAO extends IDao {

	public PageInfo<SmsRecordEO> findAllSms(SmsRecordCo smsRecordCo);

	public void updateSmsRecordStatus(String backMsgId, String status);
}
