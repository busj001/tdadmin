package com.tdcy.sys.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.tdcy.framework.bean.PageInfo;
import com.tdcy.framework.dao.DaoImpl;
import com.tdcy.framework.util.StringUtils;
import com.tdcy.sys.dao.ILoginLogDAO;
import com.tdcy.sys.dao.eo.LoginLogEO;
import com.tdcy.sys.service.bean.LoginLogQueryCondition;

@Service
public class LoginLogDAOImpl extends DaoImpl implements ILoginLogDAO {

	@Override
	public PageInfo<LoginLogEO> getPageInfo(LoginLogQueryCondition queryco) {
		String sql = "select * from sys_login_log where 1=1 ";
		List<Object> plist = new ArrayList<Object>();
		if(!StringUtils.isEmpty(queryco.getLoginUser())){
			sql += " and login_user>=?";
			plist.add(queryco.getLoginUser());
		}
		
		if(queryco.getStartTime()!=null){
			sql += " and login_date>=?";
			plist.add(queryco.getStartTime());
		}
		if(queryco.getEndTime()!=null){
			sql += " and login_date<=?";
			plist.add(queryco.getEndTime());
		}
		sql += " order by login_date desc";
		return this.queryRecordByClassForPageInfo(sql, LoginLogEO.class, queryco.getPageInfo(),StringUtils.toArray(plist));
	}

}
