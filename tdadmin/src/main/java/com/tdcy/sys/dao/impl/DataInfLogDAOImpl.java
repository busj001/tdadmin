package com.tdcy.sys.dao.impl;

import org.springframework.stereotype.Service;

import com.tdcy.framework.dao.DaoImpl;
import com.tdcy.sys.dao.IDataInfLogDAO;

/**
 * @Description 预付订单表DAO实现类
 */
@Service
public class DataInfLogDAOImpl extends DaoImpl implements IDataInfLogDAO {

}
