package com.tdcy.sys.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;

/**
 * @Description EO类
 */
@Table(name = "sys_file")
public class FileFileEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "id")
	private Integer id;
	/**
	* 
	*/
	@Column(name = "name")
	private String name;
	/**
	* 
	*/
	@Column(name = "size")
	private Integer size;
	/**
	* 
	*/
	@Column(name = "content_type")
	private String contentType;
	/**
	* 
	*/
	@Column(name = "filepath")
	private String filepath;

	@Column(name = "content", isdbcol = false)
	private byte[] content;

	/**
	 * 获取
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 设置
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 获取
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 获取
	 */
	public Integer getSize() {
		return size;
	}

	/**
	 * 设置
	 */
	public void setSize(Integer size) {
		this.size = size;
	}

	/**
	 * 获取
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * 设置
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

}
