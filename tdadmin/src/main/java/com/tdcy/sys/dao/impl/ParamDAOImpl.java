package com.tdcy.sys.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.tdcy.framework.dao.DaoImpl;
import com.tdcy.framework.util.StringUtils;
import com.tdcy.sys.dao.IParamDAO;
import com.tdcy.sys.dao.eo.ParamEO;
import com.tdcy.sys.service.bean.ParamQueryCondition;

@Service
public class ParamDAOImpl extends DaoImpl implements IParamDAO {
	@Override
	public ParamEO getParam(String key) {
		String sql = "select * from sys_parameter where code   = ?";
		return this.execSqlQueryOne(ParamEO.class, sql, new Object[] { key });
	}

	@Override
	public List<ParamEO> getParamList(ParamQueryCondition qc) {
		String sql = "select * from sys_parameter where valid_flag=1 ";
		List<Object> params = new ArrayList<Object>();
		if(!StringUtils.isEmpty(qc.getCode())){
			sql+=" and code  like '%"+qc.getCode()+"%'";
		}
		return this.execSqlQuery(ParamEO.class, sql, StringUtils.toArray(params));
	}
}
