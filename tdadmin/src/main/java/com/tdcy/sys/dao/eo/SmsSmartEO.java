package com.tdcy.sys.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;

/**
 * @Description EO类
 */
@Table(name = "cmt_sms_smart")
public class SmsSmartEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 智能短信编码
	 */
	@Id
	@Column(name = "smart_id")
	private Integer smartid;

	/**
	 * 短信类型名称
	 */
	@Column(name = "sms_name")
	private String smsName;
	/**
	 * 短信内容
	 */
	@Column(name = "sms_content")
	private String smsContent;
	/**
	 * 发送时间
	 */
	@Column(name = "send_time")
	private java.util.Date sendTime;
	/**
	 * 创建时间
	 */
	@Column(name = "create_time")
	private java.util.Date createTime;
	/**
	 * 是否开启
	 */
	@Column(name = "smart_flag")
	private Integer smartFlag;
	/**
	 * 类型
	 */
	@Column(name = "sms_type")
	private Integer smstype;

	@Column(name = "valid_flag")
	private Integer validFlag;

	public Integer getSmartid() {
		return smartid;
	}

	public void setSmartid(Integer smartid) {
		this.smartid = smartid;
	}

	public String getSmsName() {
		return smsName;
	}

	public void setSmsName(String smsName) {
		this.smsName = smsName;
	}

	public String getSmsContent() {
		return smsContent;
	}

	public void setSmsContent(String smsContent) {
		this.smsContent = smsContent;
	}

	public java.util.Date getSendTime() {
		return sendTime;
	}

	public void setSendTime(java.util.Date sendTime) {
		this.sendTime = sendTime;
	}

	public java.util.Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	public Integer getSmartFlag() {
		return smartFlag;
	}

	public void setSmartFlag(Integer smartFlag) {
		this.smartFlag = smartFlag;
	}

	public Integer getSmstype() {
		return smstype;
	}

	public void setSmstype(Integer smstype) {
		this.smstype = smstype;
	}

	public Integer getValidFlag() {
		return validFlag;
	}

	public void setValidFlag(Integer validFlag) {
		this.validFlag = validFlag;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
