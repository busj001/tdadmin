package com.tdcy.sys.dao;

import java.util.List;

import com.tdcy.framework.dao.IDao;
import com.tdcy.sys.dao.eo.MenuEO;

/**
 * @Description DAO类
 */
public interface IRoleMenusDAO extends IDao {
	public void deleteByRoleId(Integer roleid);

	public void batchSave(List<Object[]> savep);
	
	/**
	 * 根据角色id获取所有的菜单
	 * @param roleid
	 * @return
	 */
	public List<MenuEO> getAllMenusByRoleId(Integer roleid);
	
	public List<MenuEO> getAllRightsByRoleId(Integer roleid);

}
