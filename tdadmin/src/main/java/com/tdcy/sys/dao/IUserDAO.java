package com.tdcy.sys.dao;

import com.tdcy.framework.dao.IDao;
import com.tdcy.sys.dao.eo.UserEO;

/**
 * @Description DAO类
 */
public interface IUserDAO extends IDao {
	public UserEO findByLoginUser(String loginUser);

	public void updatePassByLoginUsers(String pass, String loginUsers);
}
