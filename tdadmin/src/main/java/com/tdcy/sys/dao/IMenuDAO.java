package com.tdcy.sys.dao;

import java.util.List;

import com.tdcy.framework.dao.IDao;
import com.tdcy.sys.dao.eo.MenuEO;

/**
 * @Description DAO类
 */
public interface IMenuDAO extends IDao {
	public List<MenuEO> findMenuByRoleId(Integer roleId);
}
