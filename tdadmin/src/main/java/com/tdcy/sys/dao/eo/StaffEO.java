package com.tdcy.sys.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;

/**
 * @Description EO类
 */
@Table(name = "sys_staff")
public class StaffEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "staff_id")
	private Integer staffId;
	/**
	* 
	*/
	@Column(name = "login_user")
	private String loginUser;
	/**
	* 
	*/
	@Column(name = "staff_name")
	private String staffName;
	/**
	* 
	*/
	@Column(name = "staff_sta")
	private Integer staffSta;

	/**
	* 
	*/
	@Column(name = "user_kind")
	private Integer userKind;
	/**
	* 
	*/
	@Column(name = "dept_id")
	private Integer deptId;
	/**
	* 
	*/
	@Column(name = "dis_order")
	private Integer disOrder;
	/**
	* 
	*/
	@Column(name = "sex")
	private Integer sex;
	/**
	* 
	*/
	@Column(name = "idcardno")
	private String idcardno;
	/**
	* 
	*/
	@Column(name = "birthdate")
	private java.util.Date birthdate;

	/**
	* 
	*/
	@Column(name = "create_time")
	private java.util.Date createTime;

	/**
	* 
	*/
	@Column(name = "nationality")
	private String nationality;
	/**
	* 
	*/
	@Column(name = "position")
	private String position;
	/**
	* 
	*/
	@Column(name = "address")
	private String address;
	/**
	* 
	*/
	@Column(name = "mobile_phone")
	private String mobilePhone;
	/**
	* 
	*/
	@Column(name = "home_phone")
	private String homePhone;
	/**
	* 
	*/
	@Column(name = "office_phone")
	private String officePhone;
	/**
	* 
	*/
	@Column(name = "email")
	private String email;
	/**
	* 
	*/
	@Column(name = "qq")
	private String qq;
	/**
	* 
	*/
	@Column(name = "remark")
	private String remark;
	/**
	* 
	*/
	@Column(name = "rolestr", isdbcol = false)
	private String rolestr;

	public String getRolestr() {
		return rolestr;
	}

	public void setRolestr(String rolestr) {
		this.rolestr = rolestr;
	}

	public Integer getUserKind() {
		return userKind;
	}

	public void setUserKind(Integer userKind) {
		this.userKind = userKind;
	}

	/**
	 * 获取
	 */
	public Integer getStaffId() {
		return staffId;
	}

	/**
	 * 设置
	 */
	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}

	/**
	 * 获取
	 */
	public String getLoginUser() {
		return loginUser;
	}

	/**
	 * 设置
	 */
	public void setLoginUser(String loginUser) {
		this.loginUser = loginUser;
	}

	/**
	 * 获取
	 */
	public String getStaffName() {
		return staffName;
	}

	/**
	 * 设置
	 */
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	/**
	 * 获取
	 */
	public Integer getStaffSta() {
		return staffSta;
	}

	/**
	 * 设置
	 */
	public void setStaffSta(Integer staffSta) {
		this.staffSta = staffSta;
	}

	/**
	 * 获取
	 */
	public Integer getDeptId() {
		return deptId;
	}

	/**
	 * 设置
	 */
	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}

	/**
	 * 获取
	 */
	public Integer getDisOrder() {
		return disOrder;
	}

	/**
	 * 设置
	 */
	public void setDisOrder(Integer disOrder) {
		this.disOrder = disOrder;
	}

	/**
	 * 获取
	 */
	public Integer getSex() {
		return sex;
	}

	/**
	 * 设置
	 */
	public void setSex(Integer sex) {
		this.sex = sex;
	}

	/**
	 * 获取
	 */
	public String getIdcardno() {
		return idcardno;
	}

	/**
	 * 设置
	 */
	public void setIdcardno(String idcardno) {
		this.idcardno = idcardno;
	}

	/**
	 * 获取
	 */
	public java.util.Date getBirthdate() {
		return birthdate;
	}

	/**
	 * 设置
	 */
	public void setBirthdate(java.util.Date birthdate) {
		this.birthdate = birthdate;
	}

	/**
	 * 获取
	 */
	public String getNationality() {
		return nationality;
	}

	/**
	 * 设置
	 */
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	/**
	 * 获取
	 */
	public String getPosition() {
		return position;
	}

	/**
	 * 设置
	 */
	public void setPosition(String position) {
		this.position = position;
	}

	/**
	 * 获取
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * 设置
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * 获取
	 */
	public String getMobilePhone() {
		return mobilePhone;
	}

	/**
	 * 设置
	 */
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	/**
	 * 获取
	 */
	public String getHomePhone() {
		return homePhone;
	}

	/**
	 * 设置
	 */
	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	/**
	 * 获取
	 */
	public String getOfficePhone() {
		return officePhone;
	}

	/**
	 * 设置
	 */
	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}

	/**
	 * 获取
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * 设置
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * 获取
	 */
	public String getQq() {
		return qq;
	}

	/**
	 * 设置
	 */
	public void setQq(String qq) {
		this.qq = qq;
	}

	/**
	 * 获取
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * 设置
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	public java.util.Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

}
