package com.tdcy.sys.dao.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.tdcy.framework.dao.DaoImpl;
import com.tdcy.sys.dao.IRolesDAO;
import com.tdcy.sys.dao.eo.RolesEO;

/**
 * @Description DAO实现类
 */
@Service
public class RolesDAOImpl extends DaoImpl implements IRolesDAO {

	@Override
	public List<RolesEO> getRolesByLoginUser(String loginUser) {
		String sql = "select a.* from sys_user_role b,sys_roles a,sys_user c where a.role_id = b.role_id and b.user_id = c.user_id"
				+ " and a.valid_flag = '1'   and b.valid_flag = '1'  and c.valid_flag = '1' and c.login_user = ?";
		return this.execSqlQuery(RolesEO.class, sql, new Object[] { loginUser });
	}

	@Override
	public RolesEO findByCode(String code) {
		String sql = "select  * from sys_roles where role_code = ? and valid_flag = 1";
		return this.execSqlQueryOne(RolesEO.class, sql, new Object[] { code });
	}

}
