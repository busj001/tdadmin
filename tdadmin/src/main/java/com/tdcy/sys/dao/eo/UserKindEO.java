	package com.tdcy.sys.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;
/**
* @Description EO类
*/
@Table(name = "sys_user_kind")
public class UserKindEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "user_kind")
	private Integer userKind;
	/**
	* 
	*/
	@Column(name = "kind_name")
	private String kindName;
	/**
	* 
	*/
	@Column(name = "kind_desc")
	private String kindDesc;
	/**
	* 
	*/
	@Column(name = "dis_order")
	private Integer disOrder;
	/**
	* 
	*/
	@Column(name = "login_name")
	private String loginName;
	/**
	* 
	*/
	@Column(name = "password_tip")
	private String passwordTip;
	/**
	* 
	*/
	@Column(name = "login_type")
	private String loginType;
	/**
	* 
	*/
	@Column(name = "login_sql")
	private String loginSql;
	/**
	* 
	*/
	@Column(name = "find_sql")
	private String findSql;
	/**
	* 
	*/
	@Column(name = "query_sql")
	private String querySql;
	/**
	* 
	*/
	@Column(name = "role_id")
	private Integer roleId;
	@Column(name = "param")
	private String params;
	/**
	* 
	*/
	@Column(name = "grade_id")
	private Integer gradeId;
	/**
	* 
	*/
	@Column(name = "valid_flag")
	private Integer validFlag;
	
	public String getParams() {
		return params;
	}
	public void setParams(String params) {
		this.params = params;
	}
	/**
	* 获取 
	*/ 
	public Integer getUserKind() {
		return userKind;
	}
	/**
	* 设置
	*/
	public void setUserKind(Integer userKind) {
		this.userKind = userKind;
	}
	/**
	* 获取 
	*/ 
	public String getKindName() {
		return kindName;
	}
	/**
	* 设置
	*/
	public void setKindName(String kindName) {
		this.kindName = kindName;
	}
	/**
	* 获取 
	*/ 
	public String getKindDesc() {
		return kindDesc;
	}
	/**
	* 设置
	*/
	public void setKindDesc(String kindDesc) {
		this.kindDesc = kindDesc;
	}
	/**
	* 获取 
	*/ 
	public Integer getDisOrder() {
		return disOrder;
	}
	/**
	* 设置
	*/
	public void setDisOrder(Integer disOrder) {
		this.disOrder = disOrder;
	}
	/**
	* 获取 
	*/ 
	public String getLoginName() {
		return loginName;
	}
	/**
	* 设置
	*/
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	/**
	* 获取 
	*/ 
	public String getPasswordTip() {
		return passwordTip;
	}
	/**
	* 设置
	*/
	public void setPasswordTip(String passwordTip) {
		this.passwordTip = passwordTip;
	}
	/**
	* 获取 
	*/ 
	public String getLoginType() {
		return loginType;
	}
	/**
	* 设置
	*/
	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}
	/**
	* 获取 
	*/ 
	public String getLoginSql() {
		return loginSql;
	}
	/**
	* 设置
	*/
	public void setLoginSql(String loginSql) {
		this.loginSql = loginSql;
	}
	/**
	* 获取 
	*/ 
	public String getFindSql() {
		return findSql;
	}
	/**
	* 设置
	*/
	public void setFindSql(String findSql) {
		this.findSql = findSql;
	}
	/**
	* 获取 
	*/ 
	public String getQuerySql() {
		return querySql;
	}
	/**
	* 设置
	*/
	public void setQuerySql(String querySql) {
		this.querySql = querySql;
	}
	/**
	* 获取 
	*/ 
	public Integer getRoleId() {
		return roleId;
	}
	/**
	* 设置
	*/
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	/**
	* 获取 
	*/ 
	public Integer getGradeId() {
		return gradeId;
	}
	/**
	* 设置
	*/
	public void setGradeId(Integer gradeId) {
		this.gradeId = gradeId;
	}
	/**
	* 获取 
	*/ 
	public Integer getValidFlag() {
		return validFlag;
	}
	/**
	* 设置
	*/
	public void setValidFlag(Integer validFlag) {
		this.validFlag = validFlag;
	}
}

