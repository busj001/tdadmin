package com.tdcy.sys.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tdcy.framework.BaseController;
import com.tdcy.framework.exception.BaseException;
import com.tdcy.framework.util.RequestUtils;
import com.tdcy.framework.util.WebUtils;
import com.tdcy.sys.dao.eo.ParamEO;
import com.tdcy.sys.service.IParamSvc;
import com.tdcy.sys.service.bean.ParamQueryCondition;

@Controller
@RequestMapping(value = "/common")
public class ParamController extends BaseController {
	@Resource
	IParamSvc svc;

	@ResponseBody
	@RequestMapping(value = "/getallparam", method = RequestMethod.POST)
	public Object getall(ParamQueryCondition qc,HttpServletRequest request, HttpServletResponse response) throws IOException {
		List<ParamEO> eolist = svc.getAllParam(qc);
		return WebUtils.createJSONSuccess("添加成功", eolist);
	}

	@ResponseBody
	@RequestMapping(value = "/addparam", method = RequestMethod.POST)
	public Object add(HttpServletRequest request, HttpServletResponse response, ParamEO param) throws IOException {
		ParamEO pe = svc.getParam(param.getCode());
		if (pe != null) {
			throw new BaseException("该参数已经存在");
		}
		ParamEO eo = svc.addParam(param);
		return WebUtils.createJSONSuccess("添加成功", eo);
	}


	@ResponseBody
	@RequestMapping(value = "/getparam", method = RequestMethod.POST)
	public Object getparam(HttpServletRequest request, HttpServletResponse response, ParamQueryCondition qc) throws IOException {
		ParamEO pe = svc.getParam(qc.getCode());
		return WebUtils.createJSONSuccess("添加成功", pe);
	}
	
	@ResponseBody
	@RequestMapping(value = "/updateparam", method = RequestMethod.POST)
	public Object update(HttpServletRequest request, HttpServletResponse response, ParamEO param) throws IOException {
		ParamEO eo = svc.updateParam(param);
		return WebUtils.createJSONSuccess("更新成功", eo);
	}

	@ResponseBody
	@RequestMapping(value = "/delparam", method = RequestMethod.POST)
	public Object del(HttpServletRequest request, HttpServletResponse response, @RequestParam("paramKey") String paramKey) throws IOException {
		boolean flag = svc.delParam(paramKey);
		return WebUtils.createJSONSuccess("删除成功", flag);
	}

	@ResponseBody
	@RequestMapping(value = "/saveparams", method = RequestMethod.POST)
	public Object saveparams(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Map<String, String> paramMap = RequestUtils.getAllParameters(request);
		List<ParamEO> params = new ArrayList<ParamEO>();
		Set<String> ss = paramMap.keySet();
		for (String k : ss) {
			ParamEO p = new ParamEO();
			p.setCode(k);
			p.setValue(paramMap.get(k));
			params.add(p);
		}
		svc.saveParam(params);
		return WebUtils.createJSONSuccess("保存成功");
	}
}
