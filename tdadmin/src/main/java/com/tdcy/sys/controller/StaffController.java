package com.tdcy.sys.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tdcy.framework.BaseController;
import com.tdcy.framework.util.CryptoHUtils;
import com.tdcy.framework.util.RequestUtils;
import com.tdcy.framework.util.WebUtils;
import com.tdcy.sys.dao.eo.StaffEO;
import com.tdcy.sys.service.IRolesSvc;
import com.tdcy.sys.service.IStaffSvc;
import com.tdcy.sys.service.IUserSvc;
import com.tdcy.sys.service.bean.StaffInfoBean;
import com.tdcy.sys.service.bean.StaffQueryCondition;
import com.tdcy.sys.service.bean.UpdatePwdCo;
import com.tdcy.sys.util.ParamUtils;

@Controller
@RequestMapping(value = "/sys")
public class StaffController extends BaseController {
	@Resource
	IStaffSvc staffSvc;
	@Resource
	IRolesSvc roleSvc;
	@Resource
	IUserSvc userSvc;

	@ResponseBody
	@RequestMapping(value = "/getallstaff", method = RequestMethod.POST)
	public Object getall(HttpServletRequest request, HttpServletResponse response) throws IOException {
		List<StaffEO> eolist = staffSvc.getAllStaff();
		return WebUtils.createJSONSuccess("添加成功", eolist);
	}

	@ResponseBody
	@RequestMapping(value = "/querystaff", method = RequestMethod.POST)
	public Object queryStaff(HttpServletRequest request, HttpServletResponse response, StaffQueryCondition sqc) throws IOException {
		List<StaffEO> eolist = staffSvc.queryStaff(sqc);
		return WebUtils.createJSONSuccess("添加成功", eolist);
	}

	@ResponseBody
	@RequestMapping(value = "/addstaff", method = RequestMethod.POST)
	public Object add(HttpServletRequest request, HttpServletResponse response, StaffEO staff) throws IOException {
		String roleinfo = RequestUtils.getParameter(request, "roleinfo");
		String password = RequestUtils.getParameter(request, "password");
		StaffEO eo = staffSvc.addStaff(staff, roleinfo, password);
		return WebUtils.createJSONSuccess("添加成功", eo);
	}

	@ResponseBody
	@RequestMapping(value = "/updatestaff", method = RequestMethod.POST)
	public Object update(HttpServletRequest request, HttpServletResponse response, StaffEO staff) throws IOException {
		String roleinfo = RequestUtils.getParameter(request, "roleinfo");
		String password = RequestUtils.getParameter(request, "password");
		StaffEO eo = staffSvc.updateStaff(staff, roleinfo, password);
		return WebUtils.createJSONSuccess("更新成功", eo);
	}

	@ResponseBody
	@RequestMapping(value = "/delstaff", method = RequestMethod.POST)
	public Object del(HttpServletRequest request, HttpServletResponse response, @RequestParam("staffId") Integer staffId) throws IOException {
		boolean flag = staffSvc.delStaff(staffId);
		return WebUtils.createJSONSuccess("删除成功", flag);
	}

	@ResponseBody
	@RequestMapping(value = "/getstaffinfo", method = RequestMethod.POST)
	public Object getinfo(HttpServletRequest request, HttpServletResponse response, @RequestParam("staffId") Integer staffId) throws IOException {
		StaffInfoBean eo = staffSvc.getStaffInfo(staffId);
		return WebUtils.createJSONSuccess("获取成功", eo);
	}

	@ResponseBody
	@RequestMapping(value = "/changepass", method = RequestMethod.POST)
	public Object changepass(HttpServletRequest request, HttpServletResponse response, UpdatePwdCo pwdCo) throws IOException {
		userSvc.updatePass(pwdCo);
		return WebUtils.createJSONSuccess("修改成功");
	}

	@ResponseBody
	@RequestMapping(value = "/resetpass", method = RequestMethod.POST)
	public Object resetpass(HttpServletRequest request, HttpServletResponse response, @RequestParam("loginUsers") String loginUsers) throws IOException {
		UpdatePwdCo pwdCo = new UpdatePwdCo();
		pwdCo.setLoginuser(loginUsers);
		
		String password = CryptoHUtils.md5(ParamUtils.getParamValue("default_password"));
		pwdCo.setFirstpass(password);
		pwdCo.setSecondpass(password);
		userSvc.restPass(pwdCo);
		return WebUtils.createJSONSuccess("修改成功");
	}
}

