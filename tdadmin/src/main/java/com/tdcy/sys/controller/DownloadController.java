package com.tdcy.sys.controller;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.tdcy.framework.BaseController;
import com.tdcy.sys.dao.eo.FileFileEO;
import com.tdcy.sys.service.IFileSvc;

@Controller
@RequestMapping(value = "/file")
public class DownloadController extends BaseController {

	@Resource
	IFileSvc filesvc;

	/**
	 * upload
	 * 
	 * @param response
	 * @param request
	 * @param file
	 * @throws IOException
	 */
	@RequestMapping(value = "/downloadFile", method = RequestMethod.GET)
	public Object downloadFile(HttpServletResponse response, HttpServletRequest request, int fileid) throws IOException {
		FileFileEO feo = filesvc.getFileFileInfo(fileid);
		OutputStream toClient = new BufferedOutputStream(response.getOutputStream());
		response.setContentType(feo.getContentType());
		toClient.write(feo.getContent());
		toClient.flush();
		toClient.close();
		return null;
	}

	/**
	 * upload
	 * 
	 * @param response
	 * @param request
	 * @param file
	 * @throws IOException
	 */
	@RequestMapping(value = "/downloadImage", method = RequestMethod.GET)
	public Object downloadImage(HttpServletResponse response, HttpServletRequest request, int fileid) throws IOException {
		FileFileEO feo = filesvc.getFileFileInfo(fileid);
		OutputStream toClient = new BufferedOutputStream(response.getOutputStream());
		response.setContentType(feo.getContentType());
		toClient.write(feo.getContent());
		toClient.flush();
		toClient.close();
		return null;
	}

}
