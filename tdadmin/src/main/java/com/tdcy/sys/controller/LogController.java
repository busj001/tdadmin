package com.tdcy.sys.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tdcy.framework.BaseController;
import com.tdcy.framework.bean.PageInfo;
import com.tdcy.framework.util.WebUtils;
import com.tdcy.sys.dao.eo.OperateLogEO;
import com.tdcy.sys.service.ILogManagerSvc;
import com.tdcy.sys.service.bean.LoginLogQueryCondition;
import com.tdcy.sys.service.bean.OperLogQueryCondition;
import com.tdcy.sys.util.ExpImpUtils;
import com.tdcy.sys.util.SessionUtilExt;

@Controller
@RequestMapping(value = "/sys")
public class LogController extends BaseController {
	@Resource
	ILogManagerSvc logsvc;

	@ResponseBody
	@RequestMapping(value = "/getloginlog")
	public Object getLoginLog(HttpServletRequest request, HttpServletResponse response, LoginLogQueryCondition queryco) {
		PageInfo eolist = logsvc.getloginPageInfo(queryco);
		return WebUtils.createJSONSuccess("获取成功", eolist);
	}

	@ResponseBody
	@RequestMapping(value = "/getoperlog")
	public Object getoperlog(HttpServletRequest request, HttpServletResponse response, OperLogQueryCondition queryco) {
		PageInfo eolist = logsvc.getOperatePageInfo(queryco);
		return WebUtils.createJSONSuccess("获取成功", eolist);
	}

	@RequestMapping(value = "/exportopreatelog", method = RequestMethod.POST)
	public Object exportmembaselist(HttpServletRequest request, HttpServletResponse response, OperLogQueryCondition queryco) throws IOException {
		List<OperateLogEO> eolist = logsvc.getOperateList(queryco);

		SessionUtilExt.setSession(request, ExpImpUtils.TEMPLATE_NAME, "opreatelog.xls");
		SessionUtilExt.setSession(request, ExpImpUtils.TEMPLATE_LIST_DATA, eolist);
		SessionUtilExt.setSession(request, ExpImpUtils.EXPORTFILE_NAME, "会员操作日志信息.xls");

		this.sendRedirect(response, "../expimp/exp");
		return null;
	}
}
