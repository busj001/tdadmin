package com.tdcy.sys.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tdcy.framework.BaseController;
import com.tdcy.framework.util.RequestUtils;
import com.tdcy.framework.util.WebUtils;
import com.tdcy.sys.dao.eo.MenuEO;
import com.tdcy.sys.dao.eo.RolesEO;
import com.tdcy.sys.dao.eo.UserEO;
import com.tdcy.sys.service.IMenuSvc;
import com.tdcy.sys.service.IRolesSvc;
import com.tdcy.sys.service.bean.LoginInfoBean;
import com.tdcy.sys.service.bean.RoleQueryCondition;
import com.tdcy.sys.util.BusiContextExt;

@Controller
@RequestMapping(value = "/sys")
public class RoleController extends BaseController {
	@Resource
	IRolesSvc roleSvc;
	@Resource
	IMenuSvc menuSvc;

	@ResponseBody
	@RequestMapping(value = "/getallrole", method = RequestMethod.POST)
	public Object getall(HttpServletRequest request, HttpServletResponse response, RoleQueryCondition rq) throws IOException {
		List<RolesEO> eolist = roleSvc.getAllRoles();

		if (rq.getUserKind() != null) {
			List<RolesEO> teo = new ArrayList<RolesEO>();
			for (RolesEO reo : eolist) {
				if (reo.getUserKind().compareTo(rq.getUserKind()) == 0) {
					teo.add(reo);
				}
			}
			eolist = teo;
		}

		return WebUtils.createJSONSuccess("添加成功", eolist);
	}

	@ResponseBody
	@RequestMapping(value = "/getloginrolemenus", method = RequestMethod.POST)
	public Object getloginrolemenus(HttpServletRequest request, HttpServletResponse response) throws IOException {
		LoginInfoBean loginbean = BusiContextExt.getLoginInfo();
		return WebUtils.createJSONSuccess("添加成功", loginbean.getMenuInfoEOList());
	}

	@ResponseBody
	@RequestMapping(value = "/addrole", method = RequestMethod.POST)
	public Object add(HttpServletRequest request, HttpServletResponse response, RolesEO eo) throws IOException {
		String rights = RequestUtils.getParameter(request, "rights");
		eo = roleSvc.addRoles(eo, rights);
		return WebUtils.createJSONSuccess("添加成功", eo);
	}

	@ResponseBody
	@RequestMapping(value = "/updaterole", method = RequestMethod.POST)
	public Object update(HttpServletRequest request, HttpServletResponse response, RolesEO eo) throws IOException {
		String rights = RequestUtils.getParameter(request, "rights");
		eo = roleSvc.updateRoles(eo, rights);
		return WebUtils.createJSONSuccess("更新成功", eo);
	}

	@ResponseBody
	@RequestMapping(value = "/delrole", method = RequestMethod.POST)
	public Object del(HttpServletRequest request, HttpServletResponse response, @RequestParam("roleId") int roleId) throws IOException {
		boolean flag = roleSvc.delRoles(roleId);
		return WebUtils.createJSONSuccess("删除成功", flag);
	}

	@ResponseBody
	@RequestMapping(value = "/getroleinfo", method = RequestMethod.POST)
	public Object getinfo(HttpServletRequest request, HttpServletResponse response, @RequestParam("roleId") int roleId) throws IOException {
		RolesEO eo = roleSvc.getRolesInfo(roleId);
		return WebUtils.createJSONSuccess("获取成功", eo);
	}

	@ResponseBody
	@RequestMapping(value = "/getrolemenuAndright", method = RequestMethod.POST)
	public Object getrolemenuAndright(HttpServletRequest request, HttpServletResponse response, @RequestParam("roleId") int roleId) throws IOException {
		List<MenuEO> eo = menuSvc.getAllMenusByRoleId(roleId);
		List<MenuEO> eo2 = menuSvc.getAllRightsByRoleId(roleId);
		eo.addAll(eo2);
		return WebUtils.createJSONSuccess("获取成功", eo);
	}

	@ResponseBody
	@RequestMapping(value = "/getrolemenu", method = RequestMethod.POST)
	public Object getrolemenu(HttpServletRequest request, HttpServletResponse response, @RequestParam("roleId") int roleId) throws IOException {
		List<MenuEO> eo = menuSvc.getAllMenusByRoleId(roleId);
		return WebUtils.createJSONSuccess("获取成功", eo);
	}

	@ResponseBody
	@RequestMapping(value = "/getroleuser", method = RequestMethod.POST)
	public Object getroleuser(HttpServletRequest request, HttpServletResponse response, @RequestParam("roleId") int roleId) throws IOException {
		List<UserEO> eo = roleSvc.getRoleUser(roleId);
		return WebUtils.createJSONSuccess("获取成功", eo);
	}

}
