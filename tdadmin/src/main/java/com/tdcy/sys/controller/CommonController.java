package com.tdcy.sys.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.tdcy.framework.BaseController;
import com.tdcy.framework.util.RequestUtils;

@Controller
@RequestMapping(value = "/views")
public class CommonController extends BaseController {
	public static final String page_prefix = "views/";
	/**
	 * 用来打开某个节目
	 * 
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/{pageindex}")
	public Object viewspage(HttpServletRequest request, HttpServletResponse response,@PathVariable("pageindex") String pageindex) {
		Map<String, String> params = RequestUtils.getAllParameters(request);
		ModelAndView mv = new ModelAndView();
		mv.addAllObjects(params);
		String page =  pageindex;
		mv.setViewName(page);
		return mv;
	}
	
	/**
	 * 
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/{pageindex1}/{pageindex2}")
	public Object viewspage2(HttpServletRequest request, HttpServletResponse response,@PathVariable("pageindex1") String pageindex,@PathVariable("pageindex2") String pageindex2) {
		Map<String, String> params = RequestUtils.getAllParameters(request);
		ModelAndView mv = new ModelAndView();
		mv.addAllObjects(params);
		String page =  pageindex+"/"+pageindex2;
		mv.setViewName(page);
		return mv;
	}

	/**
	 * 
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/{pageindex1}/{pageindex2}/{pageindex3}")
	public Object viewspage3(HttpServletRequest request, HttpServletResponse response,@PathVariable("pageindex1") String pageindex,@PathVariable("pageindex2") String pageindex2,@PathVariable("pageindex3") String pageindex3) {
		Map<String, String> params = RequestUtils.getAllParameters(request);
		ModelAndView mv = new ModelAndView();
		mv.addAllObjects(params);
		String page =  "/"+pageindex+"/"+pageindex2+"/"+pageindex3;
		mv.setViewName(page);
		return mv;
	}
	
	/**
	 * 
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/{pageindex1}/{pageindex2}/{pageindex3}/{pageindex4}")
	public Object viewspage3(HttpServletRequest request, HttpServletResponse response,@PathVariable("pageindex1") String pageindex,@PathVariable("pageindex2") String pageindex2,@PathVariable("pageindex3") String pageindex3,@PathVariable("pageindex4") String pageindex4) {
		Map<String, String> params = RequestUtils.getAllParameters(request);
		ModelAndView mv = new ModelAndView();
		mv.addAllObjects(params);
		String page =  pageindex+"/"+pageindex2+"/"+pageindex3+"/"+pageindex4;
		mv.setViewName(page);
		return mv;
	}
}
