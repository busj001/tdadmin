package com.tdcy.sys.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.tdcy.framework.BaseController;
import com.tdcy.framework.util.RequestUtils;
import com.tdcy.framework.util.WebUtils;
import com.tdcy.sys.dao.eo.FileFileEO;
import com.tdcy.sys.service.IFileSvc;

@Controller
@RequestMapping(value = "/file")
public class UploadController extends BaseController {

	@Resource
	IFileSvc filesvc;

	/**
	 * upload
	 * 
	 * @param response
	 * @param request
	 * @param file
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
	public Object uploadFile(HttpServletResponse response, HttpServletRequest request, @RequestParam(value = "file", required = false) MultipartFile file) throws IOException {
		FileFileEO eo = filesvc.addFileFile(file);

		Map<String, Object> m = new HashMap<String, Object>();
		m.put("fileid", eo.getId());

		return WebUtils.createJSONSuccess("上传成功", m);

	}

	/**
	 * upload
	 * 
	 * @param response
	 * @param request
	 * @param file
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/uploadImage", method = RequestMethod.POST)
	public Object uploadImage(HttpServletResponse response, HttpServletRequest request, @RequestParam(value = "file", required = false) MultipartFile file) throws IOException {
		FileFileEO eo = filesvc.addFileFile(file);
		Map<String, Object> m = new HashMap<String, Object>();
		m.put("fileid", eo.getId());

		return WebUtils.createJSONSuccess("上传成功", m);
	}

	/**
	 * upload
	 * 
	 * @param response
	 * @param request
	 * @param file
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/delFile", method = RequestMethod.POST)
	public Object delFile(HttpServletResponse response, HttpServletRequest request, @RequestParam(value = "fileid") Integer fileid) throws IOException {
		String deleteFile = RequestUtils.getParameter(request, "deleteFile");
		boolean flag = false;
		if ("true".equals(deleteFile) || "1".equals(deleteFile)) {
			flag = true;
		}
		filesvc.delFileFile(fileid, flag);
		Map<String, Object> m = new HashMap<String, Object>();
		m.put("fileid", fileid);
		return WebUtils.createJSONSuccess("上传成功", m);
	}

}
