package com.tdcy.sys.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tdcy.framework.BaseController;
import com.tdcy.framework.util.WebUtils;
import com.tdcy.sys.service.ISmsSvc;
import com.tdcy.sys.service.bean.SmsSendBean;

@Controller
@RequestMapping(value = "/sys")
public class SmsController extends BaseController {

	@Resource
	ISmsSvc smsSvc;

	@ResponseBody
	@RequestMapping(value = "/sendCheckSms")
	public Object sendCheckSms(HttpServletRequest request, HttpServletResponse response, @RequestParam("to") String to) {
		SmsSendBean sbean = new SmsSendBean();
		sbean.setTo(to);
		smsSvc.sendCheckSms(sbean);

		Map<String, Object> checkinfo = new HashMap<String, Object>();
		checkinfo.put("ccCodevalue", sbean.getCheckCode());
		checkinfo.put("ccId", sbean.getCcId());
		checkinfo.put("createTime", sbean.getCreateTime());
		checkinfo.put("sendTime", sbean.getSendTime());
		checkinfo.put("validFlag", 1);

		Map<String, Object> retMap = new HashMap<String, Object>();
		retMap.put("checkinfo", checkinfo);

		return WebUtils.createJSONSuccess("短信已发送", retMap);
	}

	/**
	 * 获取操作员信息
	 * 
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/checkSmsCode")
	public Object checkSmsCode(HttpServletRequest request, HttpServletResponse response, @RequestParam("chkid") String chkid, @RequestParam("chkcode") String chkcode) {
		smsSvc.checkSmsCode(chkid, chkcode);

		Map<String, Object> retMap = new HashMap<String, Object>();
		return WebUtils.createJSONSuccess("验证成功", retMap);
	}

}
