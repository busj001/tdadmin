package com.tdcy.sys.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tdcy.framework.BaseController;
import com.tdcy.framework.util.WebUtils;
import com.tdcy.sys.dao.IAreaDAO;
import com.tdcy.sys.dao.eo.AreaEO;
import com.tdcy.sys.service.IAreaSvc;
import com.tdcy.sys.util.AreaUtils;

/**
 * @Description 行政区划Controller
 */
@Controller
@RequestMapping(value = "/common")
public class AreaController extends BaseController {
	@Resource
	IAreaSvc areaSvc;
	@Resource
	IAreaDAO areaDao;

	@ResponseBody
	@RequestMapping(value = "/getallarea", method = RequestMethod.POST)
	public Object getall(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		List<AreaEO> eolist = areaSvc.getAllArea();
		return WebUtils.createJSONSuccess("数据加载成功", eolist);
	}

	@ResponseBody
	@RequestMapping(value = "/getallopenarea", method = RequestMethod.POST)
	public Object getallopenarea(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		List<AreaEO> eolist = areaSvc.getAllArea();
		List<AreaEO> retlsit = new ArrayList<AreaEO>();
		for (AreaEO aeo : eolist) {
			if ((aeo.getOpenflag() != null && aeo.getOpenflag().compareTo(1) == 0)
					|| aeo.getAreaLevel() == 3) {
				retlsit.add(aeo);
			}
		}
		return WebUtils.createJSONSuccess("数据加载成功", retlsit);
	}

	@ResponseBody
	@RequestMapping(value = "/getprovlist", method = RequestMethod.POST)
	public Object getprov(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		List<AreaEO> elist = areaSvc.getAllArea();
		List<AreaEO> eolist = getAreaList(elist, "0");
		return WebUtils.createJSONSuccess("数据加载成功", eolist);
	}

	@ResponseBody
	@RequestMapping(value = "/getcitylist", method = RequestMethod.POST)
	public Object getprov(HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam("provcode") String provcode) throws IOException {
		List<AreaEO> elist = areaSvc.getAllArea();
		List<AreaEO> eolist = getAreaList(elist, provcode);
		return WebUtils.createJSONSuccess("数据加载成功", eolist);
	}

	private List<AreaEO> getAreaList(List<AreaEO> areaList, String cityId) {
		List<AreaEO> retList = new ArrayList<AreaEO>();
		Integer ciryid = Integer.parseInt(cityId);
		for (AreaEO ae : areaList) {
			if (ae.getAreaUpCode().compareTo(ciryid) == 0) {
				retList.add(ae);
			}
		}
		return retList;
	}

	@ResponseBody
	@RequestMapping(value = "/getareaname", method = RequestMethod.POST)
	public Object getareaname(HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam("areaCode") String areaCode) throws IOException {
		String areaName = AreaUtils.getAreaAllName(areaCode);
		return WebUtils.createJSONSuccess("获取成功", areaName);
	}
}
