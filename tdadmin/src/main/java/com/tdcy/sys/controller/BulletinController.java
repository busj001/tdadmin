package com.tdcy.sys.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tdcy.framework.BaseController;
import com.tdcy.framework.bean.PageInfo;
import com.tdcy.framework.util.WebUtils;
import com.tdcy.sys.dao.eo.BulletinsEO;
import com.tdcy.sys.service.IBulletinsSvc;
import com.tdcy.sys.service.bean.BulletinQueryCondition;
import com.tdcy.sys.util.BusiContextExt;

@Controller
@RequestMapping(value = "/sys")
public class BulletinController extends BaseController {
	@Resource
	IBulletinsSvc bulletinSvc;

	@ResponseBody
	@RequestMapping(value = "/getallbulletin", method = RequestMethod.POST)
	public Object getallbulletin(HttpServletRequest request, HttpServletResponse response) throws IOException {
		List<BulletinsEO> eolist = bulletinSvc.getAllBulletins();
		return WebUtils.createJSONSuccess("添加成功", eolist);
	}

	@ResponseBody
	@RequestMapping(value = "/getpagebulletin", method = RequestMethod.POST)
	public Object getpagebulletin(HttpServletRequest request, HttpServletResponse response,BulletinQueryCondition queryco) throws IOException {
		PageInfo<BulletinsEO> eolist = bulletinSvc.getBulletinPageInfo(queryco);
		return WebUtils.createJSONSuccess("添加成功", eolist);
	}

	
	@ResponseBody
	@RequestMapping(value = "/addbulletin", method = RequestMethod.POST)
	public Object addbulletin(HttpServletRequest request, HttpServletResponse response, BulletinsEO bulletin) throws IOException {
		bulletin.setSenderMan(BusiContextExt.getLoginUser());
		BulletinsEO eo = bulletinSvc.addBulletins(bulletin);
		return WebUtils.createJSONSuccess("添加成功", eo);
	}

	@ResponseBody
	@RequestMapping(value = "/updatebulletin", method = RequestMethod.POST)
	public Object updatebulletin(HttpServletRequest request, HttpServletResponse response, BulletinsEO bulletin) throws IOException {
		BulletinsEO eo = bulletinSvc.updateBulletins(bulletin);
		return WebUtils.createJSONSuccess("更新成功", eo);
	}

	@ResponseBody
	@RequestMapping(value = "/delbulletin", method = RequestMethod.POST)
	public Object delbulletin(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "bulletinId") Integer bulletinId) throws IOException {
		boolean flag = bulletinSvc.delBulletins(bulletinId);
		return WebUtils.createJSONSuccess("删除成功", flag);
	}

	@ResponseBody
	@RequestMapping(value = "/getbulletininfo", method = RequestMethod.POST)
	public Object getbulletininfo(HttpServletRequest request, HttpServletResponse response, @RequestParam("bulletinId") Integer bulletinId) throws IOException {

		BulletinsEO eo = bulletinSvc.getBulletinsInfo(bulletinId);
		return WebUtils.createJSONSuccess("获取成功", eo);
	}

}
