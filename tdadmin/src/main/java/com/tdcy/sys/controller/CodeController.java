package com.tdcy.sys.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tdcy.framework.BaseController;
import com.tdcy.framework.util.StringUtils;
import com.tdcy.framework.util.WebUtils;
import com.tdcy.sys.service.ICodeSvc;
import com.tdcy.sys.service.bean.CodeBean;
import com.tdcy.sys.service.bean.CodeQueryCondition;

@Controller
@RequestMapping(value = "/common")
public class CodeController extends BaseController {
	@Resource
	ICodeSvc codeSvc;

	@ResponseBody
	@RequestMapping(value = "/getallcode", method = RequestMethod.POST)
	public Object getall(CodeQueryCondition qc,HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		List<CodeBean> eolist = codeSvc.getAllCodeList();
		
		List<CodeBean> templist = new ArrayList<CodeBean>();
		if(!StringUtils.isEmpty(qc.getCode())){
			for(CodeBean cb:eolist){
				if(cb.getCodeType().contains(qc.getCode())){
					templist.add(cb);
				}
			}
		}else{
			templist.addAll(eolist);
		}
		return WebUtils.createJSONSuccess("添加成功", templist);
	}

}
