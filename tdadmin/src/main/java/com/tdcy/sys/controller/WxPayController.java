package com.tdcy.sys.controller;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Map;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tdcy.framework.BaseController;
import com.tdcy.framework.BusiContext;
import com.tdcy.framework.bean.PageInfo;
import com.tdcy.framework.exception.QuickuException;
import com.tdcy.framework.util.RequestUtils;
import com.tdcy.framework.util.WebUtils;
import com.tdcy.sys.dao.eo.WxPayEO;
import com.tdcy.sys.service.IWxPaySvc;
import com.tdcy.sys.service.bean.LoginInfoBean;
import com.tdcy.sys.service.bean.WxPayCondition;

/**
 * @Description 微信支付Contsroller
 */
@Controller
@RequestMapping(value = "/common")
public class WxPayController extends BaseController {
	@Resource
	IWxPaySvc wxSvc;

	@ResponseBody
	@RequestMapping(value = "/wxprapay", method = RequestMethod.POST)
	public Object wxprapay(HttpServletRequest request, HttpServletResponse response, WxPayEO payeo) throws IOException {
		payeo = wxSvc.prepay(payeo);
		return WebUtils.createJSONSuccess("上传成功", payeo);
	}

	@ResponseBody
	@RequestMapping(value = "/queryprepaylist", method = RequestMethod.POST)
	public Object queryprepaylist(HttpServletRequest request, HttpServletResponse response, WxPayCondition wpcondition) throws IOException {
		PageInfo<WxPayEO> list = wxSvc.queryPrePayList(wpcondition);
		return WebUtils.createJSONSuccess("查询成功", list);
	}

	@RequestMapping(value = "/wxqrcode", method = RequestMethod.GET)
	public Object wxqrcode(HttpServletRequest request, HttpServletResponse response, @RequestParam("payBizNo") String payBizNo) throws IOException {
		BufferedImage bi = wxSvc.getQrCode(payBizNo);
		try {
			ImageIO.write(bi, "png", response.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@ResponseBody
	@RequestMapping(value = "/queryorder", method = RequestMethod.POST)
	public Object queryorder(HttpServletRequest request, HttpServletResponse response, @RequestParam("payBizNo") String payBizNo) throws IOException {
		String status = wxSvc.queryOrderStatus(payBizNo);
		return WebUtils.createJSONSuccess("查询成功", status);
	}

	@ResponseBody
	@RequestMapping(value = "/getwxpackage", method = RequestMethod.POST)
	public Object getwxpackage(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Map<String, String> paramap = RequestUtils.getAllParameters(request);
		Map<String, String> statmap = wxSvc.getWxPackage(paramap);
		return WebUtils.createJSONSuccess("查询成功", statmap);
	}

	public LoginInfoBean getLoginInfo() {
		Object obj = BusiContext.getContext("logininfo");
		if (obj != null) {
			return (LoginInfoBean) obj;
		} else {
			throw new QuickuException("获取登录信息不成功");
		}
	}
}
