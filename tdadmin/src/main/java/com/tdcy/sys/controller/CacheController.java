package com.tdcy.sys.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tdcy.framework.BaseController;
import com.tdcy.framework.util.RequestUtils;
import com.tdcy.framework.util.StringUtils;
import com.tdcy.framework.util.WebUtils;
import com.tdcy.sys.util.EhcacheUtils;

/**
 * @Description 行政区划Controller
 */
@Controller
@RequestMapping(value = "/common")
public class CacheController extends BaseController {
	@ResponseBody
	@RequestMapping(value = "/getcache", method = {RequestMethod.POST,RequestMethod.GET})
	public Object getall(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Map<String, String> paraMap = RequestUtils.getAllParameters(request);
		String cname = StringUtils.getString(paraMap, "cname");
		List<Map<String, String>> cList = EhcacheUtils.getCacheList(cname);
		return WebUtils.createJSONSuccess("数据加载成功", cList);
	}

	@ResponseBody
	@RequestMapping(value = "/clearcache", method = {RequestMethod.POST,RequestMethod.GET})
	public Object add(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Map<String, String> paraMap = RequestUtils.getAllParameters(request);
		String cname = StringUtils.getString(paraMap, "cname");
		String ckey = StringUtils.getString(paraMap, "ckey");

		if("all".equals(ckey)){
			List<Map<String, String>> cList = EhcacheUtils.getCacheList(cname);
			for(Map<String,String> m:cList){
				String key = StringUtils.getString(m, "ckey");
				EhcacheUtils.removeCache(cname, key);
			}
		}else{
			EhcacheUtils.removeCache(cname, ckey);	
		}
		
		return WebUtils.createJSONSuccess("清除成功");
	}
}
