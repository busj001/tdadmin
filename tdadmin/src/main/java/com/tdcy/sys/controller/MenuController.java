package com.tdcy.sys.controller;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tdcy.framework.BaseController;
import com.tdcy.framework.util.WebUtils;
import com.tdcy.sys.dao.eo.MenuEO;
import com.tdcy.sys.service.IMenuSvc;

@Controller
@RequestMapping(value = "/sys")
public class MenuController extends BaseController {
	@Resource
	IMenuSvc menuSvc;

	@ResponseBody
	@RequestMapping(value = "/getallmenuAndright", method = RequestMethod.POST)
	public Object getallmenuAndright(HttpServletRequest request, HttpServletResponse response) throws IOException {
		List<MenuEO> eolist = menuSvc.getAllMenu();
		List<MenuEO> eolist2 = menuSvc.getAllRight();
		eolist.addAll(eolist2);
		Collections.sort(eolist, new Comparator<MenuEO>() {
			@Override
			public int compare(MenuEO o1, MenuEO o2) {
				if (o1.getMenuOrder() > o2.getMenuOrder()) {
					return 1;
				} else if (o1.getMenuOrder() < o2.getMenuOrder()) {
					return -1;
				} else {
					return 0;
				}
			}
		});
		return WebUtils.createJSONSuccess("添加成功", eolist);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getallmenu", method = RequestMethod.POST)
	public Object getall(HttpServletRequest request, HttpServletResponse response) throws IOException {
		List<MenuEO> eolist = menuSvc.getAllMenu();
		Collections.sort(eolist, new Comparator<MenuEO>() {
			@Override
			public int compare(MenuEO o1, MenuEO o2) {
				if (o1.getMenuOrder() > o2.getMenuOrder()) {
					return 1;
				} else if (o1.getMenuOrder() < o2.getMenuOrder()) {
					return -1;
				} else {
					return 0;
				}
			}
		});
		return WebUtils.createJSONSuccess("添加成功", eolist);
	}
	
	@ResponseBody
	@RequestMapping(value = "/addmenu", method = RequestMethod.POST)
	public Object add(HttpServletRequest request, HttpServletResponse response, MenuEO menu) throws IOException {
		MenuEO eo = menuSvc.addMenu(menu);
		return WebUtils.createJSONSuccess("添加成功", eo);
	}

	@ResponseBody
	@RequestMapping(value = "/updatemenu", method = RequestMethod.POST)
	public Object update(HttpServletRequest request, HttpServletResponse response, MenuEO menu) throws IOException {
		MenuEO eo = menuSvc.updateMenu(menu);
		return WebUtils.createJSONSuccess("更新成功", eo);
	}

	@ResponseBody
	@RequestMapping(value = "/delmenu", method = RequestMethod.POST)
	public Object del(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "menuId") Integer menuId) throws IOException {
		boolean flag = menuSvc.delMenu(menuId);
		return WebUtils.createJSONSuccess("删除成功", flag);
	}

	@ResponseBody
	@RequestMapping(value = "/getmenuinfo", method = RequestMethod.POST)
	public Object getinfo(HttpServletRequest request, HttpServletResponse response, @RequestParam("menuId") Integer menuId) throws IOException {

		MenuEO eo = menuSvc.getMenuInfo(menuId);
		return WebUtils.createJSONSuccess("获取成功", eo);
	}

}
