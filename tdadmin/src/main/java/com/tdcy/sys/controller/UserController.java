package com.tdcy.sys.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tdcy.framework.BaseController;
import com.tdcy.framework.util.BeanUtil;
import com.tdcy.framework.util.JsonUtils;
import com.tdcy.framework.util.SessionUtils;
import com.tdcy.framework.util.WebUtils;
import com.tdcy.sys.constant.AppConstants;
import com.tdcy.sys.dao.IUserKindDAO;
import com.tdcy.sys.dao.IUserRoleDAO;
import com.tdcy.sys.dao.eo.MenuEO;
import com.tdcy.sys.dao.eo.RolesEO;
import com.tdcy.sys.dao.eo.StaffEO;
import com.tdcy.sys.service.ILogManagerSvc;
import com.tdcy.sys.service.ILoginSvc;
import com.tdcy.sys.service.IUserSvc;
import com.tdcy.sys.service.bean.LoginInfoBean;
import com.tdcy.sys.service.bean.UpdatePwdCo;
import com.tdcy.sys.util.BusiContextExt;
import com.tdcy.sys.util.MenuNode;
import com.tdcy.sys.util.SessionUtilExt;

@Controller
@RequestMapping(value = "sys")
public class UserController extends BaseController {
	@Resource
	IUserKindDAO userKindDAO;

	@Resource
	ILoginSvc loginSvc;

	@Resource
	IUserSvc userSvc;

	@Resource
	ILogManagerSvc logSvc;

	@Resource
	IUserRoleDAO userrolesDAO;

	@ResponseBody
	@RequestMapping(value = "/resetPassword")
	public Object resetPassword(HttpServletRequest request, HttpServletResponse response, UpdatePwdCo pwdCo) {
		userSvc.restPass(pwdCo);
		SessionUtilExt.removeLoginInfo(request);
		return WebUtils.createJSONSuccess("密码修改成功,请重新登录！", "1");
	}

	/**
	 * 进入主页
	 * 
	 * @param request
	 * @param isroot
	 * @return
	 */
	@RequestMapping(value = "/main")
	public Object main(HttpServletRequest request) {
		LoginInfoBean loginInfo = BusiContextExt.getLoginInfo();

		List<RolesEO> rolesList = loginInfo.getRoleEOList();
		SessionUtils.setSession(request, AppConstants.SESSION_ROLES, rolesList);

		Map<String, Object> loginMap = BusiContextExt.getLoginDetail();

		StaffEO seo = new StaffEO();
		BeanUtil.copyProperties(loginMap, seo);
		SessionUtils.setSesion(request.getSession(), AppConstants.SESSION_LOGIN_EO, seo);

		List<MenuEO> list = loginSvc.getMenuList(loginInfo);
		loginInfo.setMenuInfoEOList(list);

		SessionUtils.setSession(request, AppConstants.SESSION_MENUS, JsonUtils.toJson(list));
		
		//构造前端可以识别的菜单
		List<MenuNode> titles = MenuNode.buildTitle(list);
		SessionUtils.setSession(request, "menus", titles);

		return "jsp/index/hmain";

	}

	/**
	 * 进入欢迎页
	 * 
	 * @param request
	 * @param isroot
	 * @return
	 */
	@RequestMapping(value = "/welcome")
	public Object welcome(HttpServletRequest request) {
		return "jsp/index/welcome";
	}

	@ResponseBody
	@RequestMapping(value = "/updatePassword")
	public Object updatePassword(HttpServletRequest request, HttpServletResponse response, UpdatePwdCo pwdCo) {
		pwdCo.setLoginuser(BusiContextExt.getLoginUser());
		userSvc.updatePass(pwdCo);
		SessionUtilExt.removeLoginInfo(request);
		return WebUtils.createJSONSuccess("密码修改成功,请重新登录！", "1");
	}
}
