package com.tdcy.sys.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tdcy.framework.BaseController;
import com.tdcy.framework.util.WebUtils;
import com.tdcy.sys.dao.eo.DeptEO;
import com.tdcy.sys.service.IDeptSvc;

/**
 * @Description 职位Controller
 */
@Controller
@RequestMapping(value = "/sys")
public class DeptController extends BaseController {
	@Resource
	IDeptSvc baseSvc;

	@ResponseBody
	@RequestMapping(value = "/getalldept", method = RequestMethod.POST)
	public Object getalldept(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		List<DeptEO> eolist = baseSvc.getAllDept();
		return WebUtils.createJSONSuccess("数据加载成功", eolist);
	}

	@ResponseBody
	@RequestMapping(value = "/adddept", method = RequestMethod.POST)
	public Object adddept(HttpServletRequest request,
			HttpServletResponse response, DeptEO bean) throws IOException {
		DeptEO eo = baseSvc.addDept(bean);
		return WebUtils.createJSONSuccess("添加成功", eo);
	}

	@ResponseBody
	@RequestMapping(value = "/updatedept", method = RequestMethod.POST)
	public Object updatedept(HttpServletRequest request,
			HttpServletResponse response, DeptEO bean) throws IOException {
		DeptEO eo = baseSvc.updateDept(bean);
		return WebUtils.createJSONSuccess("更新成功", eo);
	}

	@ResponseBody
	@RequestMapping(value = "/deldept", method = RequestMethod.POST)
	public Object deldept(HttpServletRequest request,
			HttpServletResponse response, @RequestParam("deptId") Integer deptId)
			throws IOException {
		baseSvc.delDept(deptId);
		return WebUtils.createJSONSuccess("删除成功", true);
	}

	@ResponseBody
	@RequestMapping(value = "/getinfodept", method = RequestMethod.POST)
	public Object getinfodept(HttpServletRequest request,
			HttpServletResponse response, @RequestParam("deptId") Integer deptId)
			throws IOException {
		DeptEO eo = baseSvc.getDeptInfo(deptId);
		return WebUtils.createJSONSuccess("获取成功", eo);
	}
}
