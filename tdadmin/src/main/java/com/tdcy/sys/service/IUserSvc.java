package com.tdcy.sys.service;

import java.util.List;

import com.tdcy.sys.dao.eo.UserEO;
import com.tdcy.sys.service.bean.LoginCo;
import com.tdcy.sys.service.bean.UpdatePwdCo;

/**
 * @Description Service类
 */
public interface IUserSvc {
	public List<UserEO> getAllUser();

	public UserEO getUserInfo(Integer user_id);

	public UserEO updateUser(UserEO eo);

	public UserEO addUser(UserEO eo);

	public boolean delUser(Integer user_id);

	public UserEO getUserInfoByLoginUser(String loginUser);

	public UserEO addUser(UserEO eo, String roleids);

	public UserEO updateUser(UserEO eo, String rolestr);

	public void updatePass(UpdatePwdCo pwdCo);

	public UserEO login(LoginCo co);

	public void restPass(UpdatePwdCo pwdCo);

}
