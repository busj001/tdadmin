package com.tdcy.sys.service.bean;

import java.io.Serializable;
import java.util.Date;

public class SmsSendBean implements Serializable {
	private String templateId;
	private String to;
	private String param;
	
	private Boolean checkSms;
	private String checkCode;
	
	private Integer ccId;
	private Date createTime;
	private Date sendTime;

	public Boolean getCheckSms() {
		return checkSms;
	}

	public String getCheckCode() {
		return checkCode;
	}

	public void setCheckCode(String checkCode) {
		this.checkCode = checkCode;
	}

	public boolean isCheckSms() {
		return checkSms;
	}

	public void setCheckSms(Boolean checkSms) {
		this.checkSms = checkSms;
	}
	public Integer getCcId() {
		return ccId;
	}

	public void setCcId(Integer ccId) {
		this.ccId = ccId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getSendTime() {
		return sendTime;
	}

	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}

	public String getTemplateId() {
		return templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

}
