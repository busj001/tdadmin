package com.tdcy.sys.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.tdcy.sys.dao.IDeptDAO;
import com.tdcy.sys.dao.eo.DeptEO;
import com.tdcy.sys.service.IDeptSvc;
import com.tdcy.sys.service.ISeqNoSvc;

/**
 * @Description 岗位Service实现类
 */
@Service
public class DeptSvcImpl implements IDeptSvc {

	@Resource
	IDeptDAO deptDAO;

	@Resource
	ISeqNoSvc seqSvc;

	@Cacheable(value = "appCache", key = "'depts'")
	public List<DeptEO> getAllDept() {
		return deptDAO.findTableRecords(DeptEO.class);
	}

	@Override
	public DeptEO getDeptInfo(int deptId) {
		return deptDAO.findByPrimaryKey(DeptEO.class, deptId);
	}

	@Override
	@CacheEvict(value = "appCache", key = "'depts'")
	public DeptEO updateDept(DeptEO bean) {
		return deptDAO.update(bean);
	}

	@Override
	@CacheEvict(value = "appCache", key = "'depts'")
	public DeptEO addDept(DeptEO bean) {
		bean.setDeptId(seqSvc.getMaxNo("dept_id"));
		return deptDAO.save(bean);
	}

	@Override
	@CacheEvict(value = "appCache", key = "'depts'")
	public boolean delDept(int deptid) {
		return deptDAO.deleteByPrimaryKey(DeptEO.class, deptid);
	}
}
