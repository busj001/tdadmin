package com.tdcy.sys.service;

import java.awt.image.BufferedImage;
import java.util.Map;

import com.tdcy.framework.bean.PageInfo;
import com.tdcy.sys.dao.eo.WxPayEO;
import com.tdcy.sys.service.bean.WxPayCondition;

/**
 * @Description 预付订单表Service类
 */
public interface IWxPaySvc {

	public WxPayEO prepay(WxPayEO wxpay);

	public PageInfo<WxPayEO> queryPrePayList(WxPayCondition wpcondition);

	public BufferedImage getQrCode(String payBizNo);

	public void wxcallBack(WxPayEO eo);

	public String queryOrderStatus(String payBizNo);

	public Map<String, String> refund(WxPayEO eo);

	public Map<String, String> getWxPackage(Map<String, String> paramMap);
}
