package com.tdcy.sys.service.bean;


public class RoleQueryCondition extends QueryCondition {
	private Integer userKind;

	public Integer getUserKind() {
		return userKind;
	}

	public void setUserKind(Integer userKind) {
		this.userKind = userKind;
	}
	
	
}
