package com.tdcy.sys.service;

import java.util.List;

import com.tdcy.framework.bean.PageInfo;
import com.tdcy.sys.dao.eo.SmsRecordEO;
import com.tdcy.sys.dao.eo.SmsSmartEO;
import com.tdcy.sys.dao.eo.SmsTemplateEO;
import com.tdcy.sys.service.bean.SmsRecordCo;
import com.tdcy.sys.service.bean.SmsSendBean;
import com.tdcy.sys.service.bean.SmsSmartCo;
import com.tdcy.sys.service.bean.SmsTemplateCo;

public interface ISmsSvc {
	public List<SmsTemplateEO> getTemplateList();

	public void sendSms(SmsSendBean sbean);

	public void sendCheckSms(SmsSendBean sbean);

	public void checkSmsCode(String chkid, String chkcode);

	public void updateSmsRecordStatus(String smsBackId, String status);

	// 短信发送查询
	public PageInfo<SmsRecordEO> getAllSms(SmsRecordCo smsRecordCo);

	public SmsRecordEO getSmsInfo(Integer smsId);

	public PageInfo<SmsTemplateEO> getAllSmsTemplate(SmsTemplateCo smsTemplateCo);

	public SmsTemplateEO getSmsTemplateInfo(Integer tpid);

	public SmsTemplateEO addsmstp(SmsTemplateCo smsTemplateCo);

	public SmsTemplateEO updatesmstemp(SmsTemplateEO smsTemplateEO);

	public int deletesmstemp(String smstempids);

	public PageInfo<SmsSmartEO> getAllSmsSmart(SmsSmartCo smsSmartCo);

	public SmsSmartEO getInfoSmsSmart(Integer smartid);

	public SmsSmartEO addSmsSmart(SmsSmartCo smsSmartCo);

	public SmsSmartEO updateSmsSmart(SmsSmartEO smsSmartEO);

	public int deletesmart(String smartids);

}