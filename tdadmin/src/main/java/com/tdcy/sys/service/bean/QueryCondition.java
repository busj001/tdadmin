package com.tdcy.sys.service.bean;

import java.io.Serializable;

import com.tdcy.framework.bean.PageInfo;

public class QueryCondition implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer rows;
	private Integer page;
	private Integer limit;
	private String sortBy;
	private String sortType;

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public String getSortType() {
		return sortType;
	}

	public void setSortType(String sortType) {
		this.sortType = sortType;
	}

	public Integer getRows() {
		return rows;
	}

	public void setRows(Integer rows) {
		this.rows = rows;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public PageInfo getPageInfo() {
		PageInfo pi = new PageInfo();
		pi.setPageSize(rows);
		pi.setCurPageNO(page);
		return pi;
	}
}
