package com.tdcy.sys.service.bean;


public class StaffQueryCondition extends QueryCondition {
	String staffName;
	String loginUser;

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}


	public String getLoginUser() {
		return loginUser;
	}

	public void setLoginUser(String loginUser) {
		this.loginUser = loginUser;
	}
}
