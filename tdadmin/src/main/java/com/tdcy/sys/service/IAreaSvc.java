package com.tdcy.sys.service;

import java.util.List;

import com.tdcy.sys.dao.eo.AreaEO;

/**
 * @Description 行政区划Service类
 */
public interface IAreaSvc {
	public List<AreaEO> getAllArea();

	public AreaEO getAreaByCode(String code);
}
