package com.tdcy.sys.service.bean;

import java.io.Serializable;

public class CodeBean implements Serializable {
	private String codeType;
	private String dataValue;
	private String displayValue;
	private int disOrder;
	private String codeSql;

	public String toString() {
		return "{'" + codeType + "'|'" + dataValue + "'|'" + displayValue
				+ "'}";
	}

	public String getCodeSql() {
		return codeSql;
	}

	public void setCodeSql(String codeSql) {
		this.codeSql = codeSql;
	}

	public int getDisOrder() {
		return disOrder;
	}

	public void setDisOrder(int disOrder) {
		this.disOrder = disOrder;
	}

	public String getCodeType() {
		return codeType;
	}

	public void setCodeType(String codeType) {
		this.codeType = codeType;
	}

	public String getDataValue() {
		return dataValue;
	}

	public void setDataValue(String dataValue) {
		this.dataValue = dataValue;
	}

	public String getDisplayValue() {
		return displayValue;
	}

	public void setDisplayValue(String displayValue) {
		this.displayValue = displayValue;
	}

}
