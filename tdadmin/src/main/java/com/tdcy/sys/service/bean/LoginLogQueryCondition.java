package com.tdcy.sys.service.bean;

import java.util.Date;


public class LoginLogQueryCondition extends QueryCondition {
	String loginUser;
	Date startTime;
	Date endTime;
	public String getLoginUser() {
		return loginUser;
	}
	public void setLoginUser(String loginUser) {
		this.loginUser = loginUser;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	

}
