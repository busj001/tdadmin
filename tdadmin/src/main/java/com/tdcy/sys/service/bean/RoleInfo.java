package com.tdcy.sys.service.bean;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;

public class RoleInfo {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "role_id")
	private Integer roleId;
	/**
	* 
	*/
	@Column(name = "role_name")
	private String roleName;
	/**
	* 
	*/
	@Column(name = "role_desc")
	private String roleDesc;
	/**
	* 
	*/
	@Column(name = "role_creator")
	private String roleCreator;
	/**
	* 
	*/
	@Column(name = "role_range")
	private Integer roleRange;
	/**
	* 
	*/
	@Column(name = "dept_id")
	private String deptId;
	/**
	* 
	*/
	@Column(name = "dis_order")
	private Integer disOrder;
	

	/**
	 * 获取
	 */
	public Integer getRoleId() {
		return roleId;
	}

	/**
	 * 设置
	 */
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	/**
	 * 获取
	 */
	public String getRoleName() {
		return roleName;
	}

	/**
	 * 设置
	 */
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	/**
	 * 获取
	 */
	public String getRoleDesc() {
		return roleDesc;
	}

	/**
	 * 设置
	 */
	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc;
	}

	/**
	 * 获取
	 */
	public String getRoleCreator() {
		return roleCreator;
	}

	/**
	 * 设置
	 */
	public void setRoleCreator(String roleCreator) {
		this.roleCreator = roleCreator;
	}

	/**
	 * 获取
	 */
	public Integer getRoleRange() {
		return roleRange;
	}

	/**
	 * 设置
	 */
	public void setRoleRange(Integer roleRange) {
		this.roleRange = roleRange;
	}

	/**
	 * 获取
	 */
	public String getDeptId() {
		return deptId;
	}

	/**
	 * 设置
	 */
	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}

	/**
	 * 获取
	 */
	public Integer getDisOrder() {
		return disOrder;
	}

	/**
	 * 设置
	 */
	public void setDisOrder(Integer disOrder) {
		this.disOrder = disOrder;
	}
}
