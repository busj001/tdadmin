package com.tdcy.sys.service.bean;

import java.io.Serializable;

public class SmsConfigBean implements Serializable{
	String accountSid;
	String authToken;
	String appId;
	String server;
	String sslip;
	int sslport;
	String version;

	public String getAccountSid() {
		return accountSid;
	}

	public void setAccountSid(String accountSid) {
		this.accountSid = accountSid;
	}

	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public String getSslip() {
		return sslip;
	}

	public void setSslip(String sslip) {
		this.sslip = sslip;
	}

	public int getSslport() {
		return sslport;
	}

	public void setSslport(int sslport) {
		this.sslport = sslport;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

}
