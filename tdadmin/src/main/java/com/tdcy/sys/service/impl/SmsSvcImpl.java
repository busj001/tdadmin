package com.tdcy.sys.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.tdcy.framework.bean.PageInfo;
import com.tdcy.framework.exception.QuickuException;
import com.tdcy.framework.util.StringUtils;
import com.tdcy.sys.constant.ParamConstants;
import com.tdcy.sys.dao.ISmsChkCodeDAO;
import com.tdcy.sys.dao.ISmsRecordDAO;
import com.tdcy.sys.dao.ISmsSmartDAO;
import com.tdcy.sys.dao.ISmsTemplateDAO;
import com.tdcy.sys.dao.eo.SmsChkCodeEO;
import com.tdcy.sys.dao.eo.SmsRecordEO;
import com.tdcy.sys.dao.eo.SmsSmartEO;
import com.tdcy.sys.dao.eo.SmsTemplateEO;
import com.tdcy.sys.service.ISeqNoSvc;
import com.tdcy.sys.service.ISmsSvc;
import com.tdcy.sys.service.bean.SmsRecordCo;
import com.tdcy.sys.service.bean.SmsSendBean;
import com.tdcy.sys.service.bean.SmsSmartCo;
import com.tdcy.sys.service.bean.SmsTemplateCo;
import com.tdcy.sys.util.BizNoUtils;
import com.tdcy.sys.util.ParamUtils;
import com.tdcy.sys.util.SeqUtils;
import com.tdcy.sys.util.sms.SmsUtils;

@Service
public class SmsSvcImpl implements ISmsSvc {
	@Resource
	ISmsRecordDAO smsrdao;
	@Resource
	ISmsTemplateDAO tmpdao;
	@Resource
	ISmsChkCodeDAO ccdao;
	@Resource
	ISmsSmartDAO ssdao;

	@Resource
	ISeqNoSvc seqSvc;

	/**
	 * 发送验证码类短信
	 * 
	 * @param sbean
	 */
	public void sendCheckSms(SmsSendBean sbean) {
		if (StringUtils.isEmpty(sbean.getTo())) {
			throw new QuickuException("-21", "发送号码不能为空");
		}
		String checkCode = BizNoUtils.getSmsCheckCode();
		String timeout = ParamUtils.getParamValue(ParamConstants.SMS_CHECKCODE_TIMEOUT);
		String templateId = ParamUtils.getParamValue(ParamConstants.SMS_CHECKCODE_TEMPLATEID);

		sbean.setParam(checkCode + "," + timeout);
		sbean.setTemplateId(templateId);
		sbean.setCheckSms(true);
		sbean.setCheckCode(checkCode);
		this.sendSms(sbean);
	}

	/**
	 * 发送普通短信
	 */
	public void sendSms(SmsSendBean sbean) {
		if (StringUtils.isEmpty(sbean.getTemplateId())) {
			throw new QuickuException("-12", "模板ID不能为空");
		}

		if (StringUtils.isEmpty(sbean.getTo())) {
			throw new QuickuException("-13", "发送号码不能为空");
		}
		
		if(sbean.getCheckSms()==null){
			sbean.setCheckSms(false);
		}
		if(sbean.getCheckSms()){
			int ccId =SeqUtils.getMaxNo("sms_checkcode_id");
			sbean.setCcId(ccId);
			Date date = new Date();
			sbean.setSendTime(date);
			sbean.setCreateTime(date);
		}

		SmsUtils utils = new SmsUtils();
		utils.setSbean(sbean);
		new Thread(utils).start();
	}

	@Override
	public List<SmsTemplateEO> getTemplateList() {
		return tmpdao.findTableRecords(SmsTemplateEO.class);
	}

	@Override
	public void checkSmsCode(String chkid, String chkcode) {
		if (StringUtils.isEmpty(chkid)) {
			throw new QuickuException("-22", "验证码(ID)不能为空");
		}

		if (StringUtils.isEmpty(chkcode)) {
			throw new QuickuException("-23", "验证码不能为空");
		}

		SmsChkCodeEO cceo = ccdao.findByPrimaryKey(SmsChkCodeEO.class, chkid);
		if (cceo == null) {
			throw new QuickuException("-31", "验证码不存在");
		}
		if (!chkcode.equals(cceo.getCcCodevalue())) {
			throw new QuickuException("-32", "验证码不正确");
		}

		String timeout = ParamUtils.getParamValue(ParamConstants.SMS_CHECKCODE_TIMEOUT);

		
		
		Date now = new Date();
		Date stime = cceo.getSendTime();

		long varl = now.getTime() - stime.getTime();
		long ms = varl / (1000);
		
		if (ms > Long.parseLong(timeout)) {
			throw new QuickuException("-33", "验证码已失效");
		}
	}

	public static void main(String[] args) {
		long i = 230057;
		System.out.println(i / 1000);
	}

	// 短信发送查询
	@Override
	public PageInfo<SmsRecordEO> getAllSms(SmsRecordCo smsRecordCo) {// TODO
		return smsrdao.findAllSms(smsRecordCo);
	}

	// 短信详情
	@Override
	public SmsRecordEO getSmsInfo(Integer smsId) {
		SmsRecordEO smsreo = smsrdao.findByPrimaryKey(SmsRecordEO.class, smsId);
		return smsreo;
	}

	// 短信模板
	@Override
	public PageInfo<SmsTemplateEO> getAllSmsTemplate(SmsTemplateCo smsTemplateCo) {// TODO
		return tmpdao.findAllSmsTemplate(smsTemplateCo);
	}

	// 短信模板详情
	@Override
	public SmsTemplateEO getSmsTemplateInfo(Integer tpid) {
		SmsTemplateEO smteo = tmpdao.findByPrimaryKey(SmsTemplateEO.class, tpid);
		return smteo;
	}

	@Override
	public SmsTemplateEO addsmstp(SmsTemplateCo smsTemplateCo) {
		SmsTemplateEO st = new SmsTemplateEO();
		st.setTpid(seqSvc.getMaxNo("tpid"));
		st.setPlatTmpId(smsTemplateCo.getPlatTmpId());
		st.setPlatTmpName(smsTemplateCo.getPlatTmpName());
		st.setTmpContent(smsTemplateCo.getTmpContent());
		SmsTemplateEO steo = tmpdao.save(st);
		return steo;
	}

	@Override
	public SmsTemplateEO updatesmstemp(SmsTemplateEO smsTemplateEO) {
		SmsTemplateEO steo = tmpdao.update(smsTemplateEO);
		return steo;
	}

	@Override
	public int deletesmstemp(String smstempids) {
		return tmpdao.deletesmstemp(smstempids);
	}

	@Override
	public PageInfo<SmsSmartEO> getAllSmsSmart(SmsSmartCo smsSmartCo) {
		return ssdao.findAllSmsSmart(smsSmartCo);

	}

	@Override
	public SmsSmartEO getInfoSmsSmart(Integer smartid) {
		SmsSmartEO sseo = ssdao.findByPrimaryKey(SmsSmartEO.class, smartid);
		return sseo;
	}

	@Override
	public SmsSmartEO addSmsSmart(SmsSmartCo smsSmartCo) {
		SmsSmartEO ss = new SmsSmartEO();
		ss.setSmartid(seqSvc.getMaxNo("smartid"));
		ss.setSmsName(smsSmartCo.getSmsName());
		ss.setSmsContent(smsSmartCo.getSmsContent());
		ss.setSmartFlag(smsSmartCo.getSmartFlag());
		ss.setSmstype(smsSmartCo.getSmstype());
		ss.setSendTime(smsSmartCo.getSendTime());
		ss.setCreateTime(new Date());
		SmsSmartEO sseo = ssdao.save(ss);
		return sseo;
	}

	@Override
	public SmsSmartEO updateSmsSmart(SmsSmartEO smsSmartEO) {
		SmsSmartEO sseo = ssdao.update(smsSmartEO);
		return sseo;
	}

	@Override
	public void updateSmsRecordStatus(String smsBackId, String status) {
		smsrdao.updateSmsRecordStatus(smsBackId, status);
	}

	@Override
	public int deletesmart(String smartids) {
		return ssdao.deletesmart(smartids);
	}
}