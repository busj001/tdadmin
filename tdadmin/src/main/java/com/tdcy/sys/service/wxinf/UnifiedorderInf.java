package com.tdcy.sys.service.wxinf;

import java.util.HashMap;
import java.util.Map;

import com.tdcy.sys.dao.eo.WxPayEO;
import com.tdcy.sys.util.ParamUtils;
import com.tdcy.sys.util.wx.WxUtils;

/**
 * 统一下单api
 * 
 * @author Administrator
 * 
 */
public class UnifiedorderInf extends WfInf {
	String wxUrl = ParamUtils.getParamValue("wxurl_unifiedorder");
	String apiKey = ParamUtils.getWxApiSecretkey();

	@Override
	public Map<String, String> execute(Object paramobj) {
		WxPayEO wxpay = (WxPayEO) paramobj;
		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("trade_type", wxpay.getTradeType()); // 交易类型
		paramMap.put("spbill_create_ip", WxUtils.getlocalIp()); // 本机的Ip
		paramMap.put("product_id", wxpay.getPayBizno() + ""); // 商户根据自己业务传递的参数必填
		paramMap.put("body", wxpay.getBizDesc()); // 描述
		paramMap.put("out_trade_no", wxpay.getPayBizno()); // 商户后台的贸易单号
		paramMap.put("total_fee", "" + (int) (wxpay.getPayMoney() * 100)); // 金额必须为整数(单位为分)
		paramMap.put("notify_url", wxpay.getCodeUrl()); // 支付成功后，回调地址
		paramMap.put("attach", wxpay.getAttach()); // 支付成功后，回调地址
		paramMap.put("appid", ParamUtils.getWxAppId()); // appid
		paramMap.put("mch_id", ParamUtils.getWxMchId()); // 商户号
		paramMap.put("nonce_str", WxUtils.getNonceStr()); // 随机数
		paramMap.put("sign", WxUtils.getSign(paramMap, apiKey));

		Map<String, String> resultMap = WxUtils.post("unifiedorder", wxUrl,
				paramMap, false);
		return resultMap;
	}

}
