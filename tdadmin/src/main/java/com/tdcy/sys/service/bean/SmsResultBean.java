package com.tdcy.sys.service.bean;

import java.io.Serializable;
import java.util.Map;

import com.tdcy.framework.util.StringUtils;

public class SmsResultBean implements Serializable{
	Map<String, Object> resultParam;

	public Map<String, Object> getResultParam() {
		return resultParam;
	}

	public void setResultParam(Map<String, Object> resultParam) {
		this.resultParam = resultParam;
	}

	public String getRespCode() {
		return StringUtils.getString(resultParam, "respCode");
	}

	public String getFailure() {
		return StringUtils.getString(resultParam, "failure");
	}

	public Object getParam(String key) {
		return StringUtils.getString(resultParam, key);
	}

}
