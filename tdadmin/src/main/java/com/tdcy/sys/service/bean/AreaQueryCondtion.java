package com.tdcy.sys.service.bean;

public class AreaQueryCondtion extends QueryCondition {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String cityId;
	private String provId;
	private String countryId;

	public String getProvId() {
		return provId;
	}

	public void setProvId(String provId) {
		this.provId = provId;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

}
