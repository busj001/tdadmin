package com.tdcy.sys.service;

import java.util.List;

import com.tdcy.framework.bean.PageInfo;
import com.tdcy.sys.dao.eo.BulletinsEO;
import com.tdcy.sys.service.bean.BulletinQueryCondition;

/**
* @Description Service类
*/
public interface IBulletinsSvc{
	public List<BulletinsEO> getAllBulletins();
	

	public PageInfo<BulletinsEO> getBulletinPageInfo(BulletinQueryCondition queryco);

	public BulletinsEO getBulletinsInfo(Integer bulletin_id);

	public BulletinsEO updateBulletins( BulletinsEO bulletinsInfo);

	public BulletinsEO addBulletins(BulletinsEO bulletinsInfo);

	public boolean delBulletins(Integer bulletin_id);
}
