package com.tdcy.sys.service.bean;

public class SmsTemplateCo extends QueryCondition {

	/**
	 * 关键词
	 */
	private String key;

	private String platTmpId;
	private String platTmpName;
	private String tmpContent;
	private String validFlag;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValidFlag() {
		return validFlag;
	}

	public void setValidFlag(String validFlag) {
		this.validFlag = validFlag;
	}

	public String getPlatTmpId() {
		return platTmpId;
	}

	public void setPlatTmpId(String platTmpId) {
		this.platTmpId = platTmpId;
	}

	public String getPlatTmpName() {
		return platTmpName;
	}

	public void setPlatTmpName(String platTmpName) {
		this.platTmpName = platTmpName;
	}

	public String getTmpContent() {
		return tmpContent;
	}

	public void setTmpContent(String tmpContent) {
		this.tmpContent = tmpContent;
	}
}
