package com.tdcy.sys.service.wxinf;

import java.util.HashMap;
import java.util.Map;

import com.tdcy.sys.dao.eo.WxPayEO;
import com.tdcy.sys.util.ParamUtils;
import com.tdcy.sys.util.wx.WxUtils;

/**
 * 统一下单api
 * 
 * @author Administrator
 * 
 */
public class OrderCloseInf extends WfInf {
	String wxUrl = ParamUtils.getParamValue("wxurl_closeorder");
	String apiKey = ParamUtils.getWxApiSecretkey();

	@Override
	public Map<String, String> execute(Object paramobj) {

		WxPayEO wxpay = (WxPayEO) paramobj;
		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("appid", ParamUtils.getWxAppId()); // appid
		paramMap.put("mch_id", ParamUtils.getWxMchId()); // 商户号
		paramMap.put("nonce_str", WxUtils.getNonceStr()); // 随机数
		paramMap.put("out_trade_no", wxpay.getPayBizno());
		paramMap.put("sign", WxUtils.getSign(paramMap, apiKey));

		Map<String, String> resultMap = WxUtils.post("closeorder", wxUrl, paramMap,false);
		return resultMap;
	}

}
