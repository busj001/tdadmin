package com.tdcy.sys.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.tdcy.sys.dao.ILoginLogDAO;
import com.tdcy.sys.dao.IRolesDAO;
import com.tdcy.sys.dao.IStaffDAO;
import com.tdcy.sys.dao.IUserDAO;
import com.tdcy.sys.dao.IUserKindDAO;
import com.tdcy.sys.dao.IUserRoleDAO;
import com.tdcy.sys.dao.eo.MenuEO;
import com.tdcy.sys.dao.eo.RolesEO;
import com.tdcy.sys.service.ILoginSvc;
import com.tdcy.sys.service.IMenuSvc;
import com.tdcy.sys.service.ISeqNoSvc;
import com.tdcy.sys.service.IUserSvc;
import com.tdcy.sys.service.bean.LoginCo;
import com.tdcy.sys.service.bean.LoginInfoBean;
import com.tdcy.sys.util.MenuUtils;

@Service
public class LoginSvcImpl implements ILoginSvc {
	@Resource
	IUserSvc userSvc;

	@Resource
	IStaffDAO staffDAO;

	@Resource
	IUserKindDAO userkindDAO;

	@Resource
	IUserDAO userDao;

	@Resource
	IRolesDAO rolesDAO;

	@Resource
	IUserRoleDAO userrolesDAO;

	@Resource
	ILoginLogDAO loginLogDAO;

	@Resource
	IMenuSvc menuSvc;

	@Resource
	ISeqNoSvc seqNoSvc;

	@Override
	public LoginInfoBean checkLogin(LoginCo co) {

		LoginInfoBean loginInfo = new LoginInfoBean();

		return loginInfo;
	}

	/**
	 * 获取系统管理权限
	 */
	public List<MenuEO> getMenuList(LoginInfoBean loginInfo) {
		List<MenuEO> ret = null;
		List<MenuEO> menulist = new ArrayList<MenuEO>();

		List<RolesEO> roleList = loginInfo.getRoleEOList();
//		if (UserUtils.isAdminRole(roleList)) {
//			List<MenuEO> menus = menuSvc.getAllMenu();
//			for (MenuEO me : menus) {
//				menulist.add(me);
//			}
//		} else {
			for (RolesEO re : roleList) {
				List<MenuEO> menu = menuSvc.getAllMenusByRoleId(re.getRoleId());
				menulist.addAll(menu);
			}
//		}

		ret = MenuUtils.removeDupMenu(menulist);

		Collections.sort(ret, new Comparator<MenuEO>() {
			@Override
			public int compare(MenuEO o1, MenuEO o2) {
				if (o1.getMenuOrder() > o2.getMenuOrder()) {
					return 1;
				} else if (o1.getMenuOrder() < o2.getMenuOrder()) {
					return -1;
				} else {
					return 0;
				}
			}
		});
		return ret;
	}

}
