package com.tdcy.sys.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.tdcy.framework.bean.PageInfo;
import com.tdcy.sys.dao.ILoginLogDAO;
import com.tdcy.sys.dao.IOperateLogDAO;
import com.tdcy.sys.dao.ISystemLogDAO;
import com.tdcy.sys.dao.eo.LoginLogEO;
import com.tdcy.sys.dao.eo.OperateLogEO;
import com.tdcy.sys.dao.eo.SystemLogEO;
import com.tdcy.sys.dao.eo.UserEO;
import com.tdcy.sys.service.ILogManagerSvc;
import com.tdcy.sys.service.ISeqNoSvc;
import com.tdcy.sys.service.bean.LoginInfoBean;
import com.tdcy.sys.service.bean.LoginLogQueryCondition;
import com.tdcy.sys.service.bean.OperLogQueryCondition;

/**
 * @Description Service实现类
 */
@Service
public class LogManagerSvcImpl implements ILogManagerSvc {

	@Resource
	ILoginLogDAO loginlogDAO;

	@Resource
	ISeqNoSvc seqSvc;

	@Resource
	IOperateLogDAO operatelogDAO;

	@Resource
	ISystemLogDAO systemlogDAO;

	public List<SystemLogEO> getAllSystemLog() {
		return systemlogDAO.findTableRecords(SystemLogEO.class);
	}

	@Override
	public SystemLogEO getSystemLogInfo(Integer systemlogId) {
		return systemlogDAO.findByPrimaryKey(SystemLogEO.class, systemlogId);
	}

	@Override
	public SystemLogEO updateSystemLog(SystemLogEO eo) {
		return systemlogDAO.update(eo);
	}

	@Override
	public SystemLogEO addSystemLog(SystemLogEO eo) {
		eo.setLogSn(seqSvc.getMaxNo("log_sn"));
		return systemlogDAO.save(eo);
	}

	@Override
	public boolean delSystemLog(int systemlogid) {
		return systemlogDAO.deleteByPrimaryKey(SystemLogEO.class, systemlogid);
	}

	public List<OperateLogEO> getAllOperateLog() {
		return operatelogDAO.findTableRecords(OperateLogEO.class);
	}

	@Override
	public OperateLogEO getOperateLogInfo(Integer operatelogId) {
		return operatelogDAO.findByPrimaryKey(OperateLogEO.class, operatelogId);
	}

	@Override
	public OperateLogEO updateOperateLog(OperateLogEO eo) {
		return operatelogDAO.update(eo);
	}

	@Override
	public OperateLogEO addOperateLog(OperateLogEO eo) {
		eo.setOperateId(seqSvc.getMaxNo("operate_id"));
		return operatelogDAO.save(eo);
	}

	@Override
	public boolean delOperateLog(Integer operatelogid) {
		return operatelogDAO.deleteByPrimaryKey(OperateLogEO.class, operatelogid);
	}

	public List<LoginLogEO> getAllLoginLog() {
		return loginlogDAO.findTableRecords(LoginLogEO.class);
	}

	@Override
	public LoginLogEO getLoginLogInfo(Integer loginlogId) {
		return loginlogDAO.findByPrimaryKey(LoginLogEO.class, loginlogId);
	}

	@Override
	public LoginLogEO updateLoginLog(Integer loginlogId, LoginLogEO eo) {
		return loginlogDAO.update(eo);
	}

	@Override
	public LoginLogEO addLoginLog(LoginInfoBean login) {
		UserEO ue = login.getUserEO();
		LoginLogEO eo = new LoginLogEO();
		eo.setLoginId(seqSvc.getMaxNo("login_id"));
		eo.setLoginUser(ue.getLoginUser());
		eo.setUserId(ue.getUserId());
		eo.setUserName(ue.getUserName());
		eo.setLoginAddress(login.getAddress());
		eo.setLoginDate(new Date());
		eo.setLoginFlag(1);

		return loginlogDAO.save(eo);
	}

	@Override
	public boolean delLoginLog(Integer loginlogid) {
		return loginlogDAO.deleteByPrimaryKey(LoginLogEO.class, loginlogid);
	}

	@Override
	public LoginLogEO addLoginLog(LoginLogEO eo) {
		eo.setLoginId(seqSvc.getMaxNo("login_id"));
		return loginlogDAO.save(eo);
	}

	@Override
	public PageInfo<LoginLogEO> getloginPageInfo(LoginLogQueryCondition queryco) {
		return loginlogDAO.getPageInfo(queryco);
	}

	@Override
	public PageInfo<OperateLogEO> getOperatePageInfo(OperLogQueryCondition queryco) {
		return operatelogDAO.getPageInfo(queryco);
	}

	@Override
	public PageInfo<SystemLogEO> getSystemPageInfo(Map<String, String> queryParam, PageInfo pageinfo) {
		return null;
	}
	
	public List<OperateLogEO> getOperateList(OperLogQueryCondition queryco){
		return operatelogDAO.getOperateList(queryco);
	}
}
