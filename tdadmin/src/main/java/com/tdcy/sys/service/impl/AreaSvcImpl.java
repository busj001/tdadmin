package com.tdcy.sys.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.tdcy.sys.dao.IAreaDAO;
import com.tdcy.sys.dao.eo.AreaEO;
import com.tdcy.sys.service.IAreaSvc;
import com.tdcy.sys.service.ISeqNoSvc;

/**
 * @Description 行政区划Service实现类
 */
@Service
public class AreaSvcImpl implements IAreaSvc {

	@Resource
	IAreaDAO areaDAO;

	@Resource
	ISeqNoSvc seqSvc;

	@Cacheable(value = "appCache", key = "'areas'")
	public List<AreaEO> getAllArea() {
		return areaDAO.findTableRecords(AreaEO.class);
	}

	public AreaEO getAreaByCode(String code) {
		return areaDAO.findByCode(code);
	}
}
