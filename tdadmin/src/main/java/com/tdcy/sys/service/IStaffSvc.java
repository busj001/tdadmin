package com.tdcy.sys.service;

import java.util.List;

import com.tdcy.sys.dao.eo.StaffEO;
import com.tdcy.sys.service.bean.StaffInfoBean;
import com.tdcy.sys.service.bean.StaffQueryCondition;

/**
 * @Description Service类
 */
public interface IStaffSvc {
	public List<StaffEO> getAllStaff();

	public StaffInfoBean getStaffInfo(Integer staff_id);

	public StaffEO updateStaff(StaffEO staff, String rolestr, String password);

	public StaffEO addStaff(StaffEO staff, String rolestr, String password);

	public boolean delStaff(Integer staff_id);

	public StaffEO getStaffInfoByLoginUser(String loginUser);

	public List<StaffEO> queryStaff(StaffQueryCondition staffCo);
}
