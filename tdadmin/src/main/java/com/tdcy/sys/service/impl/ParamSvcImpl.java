package com.tdcy.sys.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tdcy.sys.dao.IParamDAO;
import com.tdcy.sys.dao.eo.ParamEO;
import com.tdcy.sys.service.IParamSvc;
import com.tdcy.sys.service.bean.ParamQueryCondition;
import com.tdcy.sys.util.EhcacheUtils;

@Service
public class ParamSvcImpl implements IParamSvc {
	@Resource
	IParamDAO paramDAO;

	public List<ParamEO> getAllParam(ParamQueryCondition qc) {
		List<ParamEO> list = paramDAO.getParamList(qc);
		return list;
	}

	@Cacheable(value = "appCache", key = "'param'+#key")
	public ParamEO getParam(String key) {
		ParamEO eo = paramDAO.getParam(key);
		return eo;
	}

	@Override
	@CacheEvict(value = "appCache", key = "'param'+#param.code")
	public ParamEO updateParam(ParamEO param) {
		ParamEO meo = paramDAO.update(param);
		return meo;
	}

	@Override
	@Transactional
	@CacheEvict(value = "appCache", key = "'param'+#param.code")
	public ParamEO addParam(ParamEO param) {
		ParamEO meo = paramDAO.save(param);
		return meo;
	}

	@Override
	@CacheEvict(value = "appCache", key = "'param'+#param.code")
	public boolean delParam(String paramKey) {
		String sql = "update sys_parameter t set t.valid_flag = '0' where 1=1  and t.code='" + paramKey + "'";
		paramDAO.execSqlUpdate(sql);
		return true;
	}

	@Override
	public void saveParam(List<ParamEO> param) {
		for (ParamEO p : param) {
			ParamEO eo = paramDAO.getParam(p.getCode());
			if (eo != null) {
				paramDAO.update(p);
			} else {
				paramDAO.save(p);
			}
			EhcacheUtils.removeCache("appCache", "param"+p.getCode());
		}
	}

}
