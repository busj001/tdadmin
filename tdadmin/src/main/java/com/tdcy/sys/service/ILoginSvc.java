package com.tdcy.sys.service;

import java.util.List;

import com.tdcy.sys.dao.eo.MenuEO;
import com.tdcy.sys.service.bean.LoginCo;
import com.tdcy.sys.service.bean.LoginInfoBean;

public interface ILoginSvc {
	public LoginInfoBean checkLogin(LoginCo loginCo);

	public List<MenuEO> getMenuList(LoginInfoBean loginInfo);
}
