package com.tdcy.sys.service.bean;


public class WxPayCondition extends QueryCondition {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	private Integer payStatus;
	

	public Integer getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(Integer payStatus) {
		this.payStatus = payStatus;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
