package com.tdcy.sys.service.wxinf;

import java.util.HashMap;
import java.util.Map;

import com.tdcy.sys.dao.eo.WxPayEO;
import com.tdcy.sys.util.ParamUtils;
import com.tdcy.sys.util.wx.WxUtils;

/**
 * 统一下单api
 * 
 * @author Administrator
 * 
 */
public class RefundInf extends WfInf {
	String wxUrl = ParamUtils.getParamValue("wxurl_closeorder");
	String apiKey = ParamUtils.getWxApiSecretkey();

	@Override
	public Map<String, String> execute(Object paramobj) {

		WxPayEO wxpay = (WxPayEO) paramobj;
		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("appid", ParamUtils.getWxAppId()); // appid
		paramMap.put("mch_id", ParamUtils.getWxMchId()); // 商户号
		paramMap.put("nonce_str", WxUtils.getNonceStr()); // 随机数
		paramMap.put("out_trade_no", wxpay.getPayBizno());

		// 退款单号
		paramMap.put("out_refund_no", wxpay.getPayBizno());

		// paramMap.put("total_fee", wxpay.get);
		// paramMap.put("refund_fee", wxpay.getPayBizno());
		// paramMap.put("op_user_id", wxpay.getWxMchId());

		paramMap.put("sign", WxUtils.getSign(paramMap, apiKey));

		Map<String, String> resultMap = WxUtils.post("orderquery", wxUrl,
				paramMap, true);
		return resultMap;
	}

}
