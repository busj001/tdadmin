package com.tdcy.sys.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.shiro.authc.AuthenticationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.tdcy.framework.exception.BaseException;
import com.tdcy.framework.util.StringUtils;
import com.tdcy.sys.dao.IUserDAO;
import com.tdcy.sys.dao.IUserRoleDAO;
import com.tdcy.sys.dao.eo.UserEO;
import com.tdcy.sys.service.ISeqNoSvc;
import com.tdcy.sys.service.IUserSvc;
import com.tdcy.sys.service.bean.LoginCo;
import com.tdcy.sys.service.bean.UpdatePwdCo;

/**
 * @Description Service实现类
 */
@Service
public class UserSvcImpl implements IUserSvc {
	@Resource
	IUserRoleDAO userroleDAO;

	@Resource
	IUserDAO userDAO;

	@Resource
	ISeqNoSvc seqSvc;

	public List<UserEO> getAllUser() {
		return userDAO.findTableRecords(UserEO.class);
	}

	@Override
	public UserEO getUserInfo(Integer userId) {
		return userDAO.findByPrimaryKey(UserEO.class, userId);
	}

	@Override
	public UserEO updateUser(UserEO eo) {
		return userDAO.update(eo);
	}

	@Override
	public UserEO addUser(UserEO eo) {
		eo.setUserId(seqSvc.getMaxNo("user_id"));
		eo.setCreateTime(new Date());
		return userDAO.save(eo);
	}

	@Override
	public boolean delUser(Integer userid) {
		return userDAO.deleteByPrimaryKey(UserEO.class, userid);
	}

	@Override
	@Transactional(propagation = Propagation.NESTED)
	public UserEO addUser(UserEO eo, String rolestr) {
		int userid = seqSvc.getMaxNo("user_id");
		eo.setUserId(userid);
		eo.setCreateTime(new Date());
		String password = eo.getPassword();
		eo.setPassword(password);
		userDAO.save(eo);

		if (!StringUtils.isEmpty(rolestr)) {
			String[] roleids = rolestr.split(",");
			List<Object[]> menp = new ArrayList<Object[]>();
			for (int i = 0; i < roleids.length; i++) {
				Object[] b = new Object[] { eo.getUserId(), roleids[i] };
				menp.add(b);
			}
			userroleDAO.batchSave(menp);
		}

		return eo;
	}

	@Override
	@Transactional(propagation = Propagation.NESTED)
	public UserEO updateUser(UserEO eo, String rolestr) {
		String password = eo.getPassword();
		// if (!StringUtils.isEmpty(password)) {
		// password = CryptoHUtils.md5(password);
		// } else {
		// password = null;
		// }
		eo.setPassword(password);
		userDAO.update(eo);
		if (!StringUtils.isEmpty(rolestr)) {
			userroleDAO.deleteByUserId(eo.getUserId());
			String[] roleids = rolestr.split(",");
			List<Object[]> menp = new ArrayList<Object[]>();
			for (int i = 0; i < roleids.length; i++) {
				Object[] b = new Object[] { eo.getUserId(), roleids[i] };
				menp.add(b);
			}
			userroleDAO.batchSave(menp);
		}

		return eo;
	}

	@Override
	public UserEO getUserInfoByLoginUser(String loginUser) {
		return userDAO.findByLoginUser(loginUser);
	}

	@Override
	public UserEO login(LoginCo co) {
		UserEO userEO = userDAO.findByLoginUser(co.getLoginUser());
		if (userEO == null) {
			throw new AuthenticationException( "【" + co.getLoginUser() + "】不存在");
		}

		String inputpassmd5 = co.getPassword();
		if (!inputpassmd5.equalsIgnoreCase(userEO.getPassword())) {
			throw new AuthenticationException( "密码不正确");
		}
		
		String lockSta = userEO.getLockState();

		if ("0".equals(lockSta)) {
			throw new AuthenticationException( "用户已锁定，锁定原因:" + userEO.getLockReason());
		}
		return userEO;
	}

	@Override
	public void updatePass(UpdatePwdCo pwdCo) {
		if (StringUtils.isEmpty(pwdCo.getFirstpass()) || StringUtils.isEmpty(pwdCo.getOldpass()) || StringUtils.isEmpty(pwdCo.getSecondpass())) {
			throw new BaseException("-11", "原始密码，新密码，重复新密码都不能为空");
		}

		if (!pwdCo.getFirstpass().equals(pwdCo.getSecondpass())) {
			throw new BaseException("-12", "新密码，重复新密码必须相同");
		}

		UserEO userEO = userDAO.findByLoginUser(pwdCo.getLoginuser());
		if (userEO == null) {
			throw new BaseException("-13", "找不到用户信息");
		}

		String oldpass = pwdCo.getOldpass();
		if (!oldpass.equalsIgnoreCase(userEO.getPassword())) {
			throw new BaseException("-13", "原始密码不正确");
		}

		String newpass = pwdCo.getFirstpass();

		userEO.setPassword(newpass);
		userDAO.update(userEO);
	}
	

	@Override
	public void restPass(UpdatePwdCo pwdCo) {
		if (!pwdCo.getFirstpass().equals(pwdCo.getSecondpass())) {
			throw new BaseException("-12", "新密码，重复新密码必须相同");
		}
		userDAO.updatePassByLoginUsers(pwdCo.getFirstpass(), pwdCo.getLoginuser());
	}
}
