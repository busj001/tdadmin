package com.tdcy.sys.service;

import java.util.List;

import com.tdcy.sys.service.bean.CodeBean;

public interface ICodeSvc {
	public List<CodeBean> getCodeList(String codeType);

	public List<CodeBean> getAllCodeList();
}
