package com.tdcy.sys.service;

import com.tdcy.framework.exception.BaseException;

public interface ISeqNoSvc {
	public int getMaxNo(String MaxNo_Name) throws BaseException;

	public int getMaxNo(String MaxNo_Name, int MaxNo_Size) throws BaseException;
	
	public int getMaxNoTest(String MaxNo_Name, int MaxNo_Size) throws BaseException;
}
