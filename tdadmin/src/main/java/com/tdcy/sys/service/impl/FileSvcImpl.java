package com.tdcy.sys.service.impl;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import javax.annotation.Resource;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.tdcy.sys.dao.IFileFileDAO;
import com.tdcy.sys.dao.eo.FileFileEO;
import com.tdcy.sys.service.IFileSvc;
import com.tdcy.sys.service.ISeqNoSvc;
import com.tdcy.sys.util.ParamUtils;

/**
 * @Description Service实现类
 */
@Service
public class FileSvcImpl implements IFileSvc {
	@Resource
	IFileFileDAO filefileDAO;
	@Resource
	ISeqNoSvc seqSvc;
	

	@Override
	@Transactional
	public FileFileEO getFileFileInfo(int filefileId) throws IOException {
		FileFileEO ffo = filefileDAO.findByPrimaryKey(FileFileEO.class,
				filefileId);

		String path = ffo.getFilepath();
		byte[] bs = FileUtils.readFileToByteArray(new File(path));
		ffo.setContent(bs);
		return ffo;
	}

	@Override
	@Transactional
	public boolean delFileFile(int filefileid,boolean deleteFile) {
		FileFileEO ffo = filefileDAO.findByPrimaryKey(FileFileEO.class,
				filefileid);
		if(deleteFile){
			String path = ffo.getFilepath();
			FileUtils.deleteQuietly(new File(path));
		}
		
		return filefileDAO.deleteByPrimaryKey(FileFileEO.class, filefileid);
	}

	@Override
	@Transactional
	public FileFileEO addFileFile(MultipartFile file) throws IOException {
		byte[] b = file.getBytes();
		int size = (int) b.length;
		int id = seqSvc.getMaxNo("file_file_id");

		FileFileEO eo = new FileFileEO();
		eo.setId(id);
		eo.setSize(size);
		eo.setName(file.getOriginalFilename());
		eo.setContentType(file.getContentType());
		String savePath = ParamUtils .getParamValue("file_upload_path", "upload");
		File f = new File(savePath + File.separator + id + "-"
				+ file.getOriginalFilename());
		ByteArrayInputStream bais = new ByteArrayInputStream(file.getBytes());

		FileUtils.copyInputStreamToFile(bais, f);
		eo.setFilepath(f.getAbsolutePath());
		filefileDAO.save(eo);
		return eo;
	}

	@Override
	@Transactional
	public FileFileEO addFileFile(String filename, String contentType,
			byte[] data) throws IOException {
		byte[] b = data;
		int size = (int) b.length;
		int id = seqSvc.getMaxNo("file_file_id");

		FileFileEO eo = new FileFileEO();
		eo.setId(id);
		eo.setSize(size);
		eo.setName(filename);
		eo.setContentType(contentType);
		String savePath = ParamUtils .getParamValue("file_upload_path", "upload");
		File f = new File(savePath + File.separator + id + "-" + filename);
		ByteArrayInputStream bais = new ByteArrayInputStream(data);

		FileUtils.copyInputStreamToFile(bais, f);
		eo.setFilepath(f.getAbsolutePath());
		filefileDAO.save(eo);
		return eo;
	}
}
