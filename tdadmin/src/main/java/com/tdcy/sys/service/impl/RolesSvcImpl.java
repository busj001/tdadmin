package com.tdcy.sys.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tdcy.framework.exception.BaseException;
import com.tdcy.framework.util.StringUtils;
import com.tdcy.sys.dao.IMenuDAO;
import com.tdcy.sys.dao.IRoleMenusDAO;
import com.tdcy.sys.dao.IRolesDAO;
import com.tdcy.sys.dao.IUserRoleDAO;
import com.tdcy.sys.dao.eo.RolesEO;
import com.tdcy.sys.dao.eo.UserEO;
import com.tdcy.sys.service.IRolesSvc;
import com.tdcy.sys.service.ISeqNoSvc;
import com.tdcy.sys.util.UserUtils;

/**
 * @Description Service实现类
 */
@Service
public class RolesSvcImpl implements IRolesSvc {

	@Resource
	IRolesDAO rolesDAO;

	@Resource
	IUserRoleDAO userrolesDAO;

	@Resource
	IRoleMenusDAO rolesmenuDAO;

	@Resource
	IMenuDAO menuDAO;

	@Resource
	ISeqNoSvc seqSvc;

	@Cacheable(value = "appCache", key = "'roles'")
	public List<RolesEO> getAllRoles() {
		return rolesDAO.findTableRecords(RolesEO.class);
	}

	@Override
	public RolesEO getRolesInfo(Integer rolesId) {
		return rolesDAO.findByPrimaryKey(RolesEO.class, rolesId);
	}

	@Override
	@Transactional
	@CacheEvict(value = "appCache", key = "'roles'")
	public RolesEO updateRoles(RolesEO eo, String rightstr) {
		Integer roleid = eo.getRoleId();
		RolesEO re = rolesDAO.update(eo);
		if (!StringUtils.isEmpty(rightstr)) {
			rolesmenuDAO.deleteByRoleId(roleid);

			String[] menuids = rightstr.split(",");
			List<Object[]> menp = new ArrayList<Object[]>();
			for (int i = 0; i < menuids.length; i++) {
				Object[] b = new Object[] { roleid, menuids[i] };
				menp.add(b);
			}
			rolesmenuDAO.batchSave(menp);
		}
		return re;
	}

	@Override
	@Transactional
	@CacheEvict(value = "appCache", key = "'roles'")
	public RolesEO addRoles(RolesEO eo, String rightstr) {
		Integer roleid = seqSvc.getMaxNo("role_id");
		eo.setRoleId(roleid);
		eo = rolesDAO.save(eo);

		if (!StringUtils.isEmpty(rightstr)) {
			String[] menuids = rightstr.split(",");
			List<Object[]> menp = new ArrayList<Object[]>();
			for (int i = 0; i < menuids.length; i++) {
				Object[] b = new Object[] { roleid, menuids[i] };
				menp.add(b);
			}
			rolesmenuDAO.batchSave(menp);
		}

		return eo;
	}

	@Override
	@CacheEvict(value = "appCache", key = "'roles'")
	public boolean delRoles(Integer rolesid) {
		RolesEO reo = rolesDAO.findByPrimaryKey(RolesEO.class, rolesid);
		if (UserUtils.isAdminRole(reo)) {
			throw new BaseException("管理员角色不能删除");
		}

		if (reo.getIsDelete().compareTo(1) == 0) {
			throw new BaseException("该角色的为系统角色，不能删除");
		}
		boolean flag = rolesDAO.deleteByPrimaryKey(RolesEO.class, rolesid);
		return flag;
	}

	@Override
	public List<UserEO> getRoleUser(Integer roleid) {
		return userrolesDAO.getUserByRoleId(roleid);
	}
}
