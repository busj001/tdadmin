package com.tdcy.sys.service.bean;

import java.util.List;

import com.tdcy.sys.dao.eo.RolesEO;
import com.tdcy.sys.dao.eo.StaffEO;

public class StaffInfoBean {
	public StaffEO staffEO;
	public List<RolesEO> roleList;
	public StaffEO getStaffEO() {
		return staffEO;
	}
	public void setStaffEO(StaffEO staffEO) {
		this.staffEO = staffEO;
	}
	public List<RolesEO> getRoleList() {
		return roleList;
	}
	public void setRoleList(List<RolesEO> roleList) {
		this.roleList = roleList;
	}
}
