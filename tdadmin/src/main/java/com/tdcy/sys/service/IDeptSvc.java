package com.tdcy.sys.service;

import java.util.List;

import com.tdcy.sys.dao.eo.DeptEO;

/**
 * @Description 岗位Service类
 */
public interface IDeptSvc {
	public List<DeptEO> getAllDept();

	public DeptEO getDeptInfo(int dept_id);

	public DeptEO updateDept(DeptEO bean);

	public DeptEO addDept(DeptEO bean);

	public boolean delDept(int dept_id);
}
