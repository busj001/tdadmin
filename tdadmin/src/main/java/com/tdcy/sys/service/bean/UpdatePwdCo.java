package com.tdcy.sys.service.bean;

public class UpdatePwdCo {
	private String loginuser;
	private Integer chgType;
	private String oldpass;
	private String firstpass;
	private String secondpass;
	private String loginpass;
	private String phone;
	private String certno;
	private String oldcertno;

	public Integer getChgType() {
		return chgType;
	}

	public void setChgType(Integer chgType) {
		this.chgType = chgType;
	}

	public String getLoginuser() {
		return loginuser;
	}

	public void setLoginuser(String loginuser) {
		this.loginuser = loginuser;
	}

	public String getOldpass() {
		return oldpass;
	}

	public void setOldpass(String oldpass) {
		this.oldpass = oldpass;
	}

	public String getFirstpass() {
		return firstpass;
	}

	public void setFirstpass(String firstpass) {
		this.firstpass = firstpass;
	}

	public String getSecondpass() {
		return secondpass;
	}

	public void setSecondpass(String secondpass) {
		this.secondpass = secondpass;
	}

	public String getLoginpass() {
		return loginpass;
	}

	public void setLoginpass(String loginpass) {
		this.loginpass = loginpass;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCertno() {
		return certno;
	}

	public void setCertno(String certno) {
		this.certno = certno;
	}

	public String getOldcertno() {
		return oldcertno;
	}

	public void setOldcertno(String oldcertno) {
		this.oldcertno = oldcertno;
	}

}
