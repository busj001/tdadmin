package com.tdcy.sys.service.bean;

import java.util.Date;

public class SmsRecordCo extends QueryCondition {

	/**
	 * 关键词
	 */
	private String key;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	private Integer smsId;
	private String smsPhone;
	private String smsContent;
	private java.util.Date sendTime;
	private java.util.Date createTime;
	private Integer status;
	private String remark;
	private String tmpId;
	private String backsmsid;
	private String jsid;
	private Integer validFlag;
	private Integer smstype;
	private Date startTime;
	private Date endTime;

	private String respcode;

	private String respmsg;

	public String getJsid() {
		return jsid;
	}

	public void setJsid(String jsid) {
		this.jsid = jsid;
	}

	public String getRespcode() {
		return respcode;
	}

	public void setRespcode(String respcode) {
		this.respcode = respcode;
	}

	public String getRespmsg() {
		return respmsg;
	}

	public void setRespmsg(String respmsg) {
		this.respmsg = respmsg;
	}

	/**
	 * 获取 短信id
	 */
	public Integer getSmsId() {
		return smsId;
	}

	/**
	 * 设置短信id
	 */
	public void setSmsId(Integer smsId) {
		this.smsId = smsId;
	}

	/**
	 * 获取
	 */
	public String getSmsPhone() {
		return smsPhone;
	}

	/**
	 * 设置
	 */
	public void setSmsPhone(String smsPhone) {
		this.smsPhone = smsPhone;
	}

	/**
	 * 获取
	 */
	public String getSmsContent() {
		return smsContent;
	}

	/**
	 * 设置
	 */
	public void setSmsContent(String smsContent) {
		this.smsContent = smsContent;
	}

	/**
	 * 获取
	 */
	public java.util.Date getSendTime() {
		return sendTime;
	}

	/**
	 * 设置
	 */
	public void setSendTime(java.util.Date sendTime) {
		this.sendTime = sendTime;
	}

	/**
	 * 获取
	 */
	public java.util.Date getCreateTime() {
		return createTime;
	}

	/**
	 * 设置
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * 获取
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * 设置
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * 获取
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * 设置
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * 获取 短信模板ID
	 */
	public String getTmpId() {
		return tmpId;
	}

	/**
	 * 设置短信模板ID
	 */
	public void setTmpId(String tmpId) {
		this.tmpId = tmpId;
	}

	/**
	 * 获取 短信平台短信id
	 */
	public String getBacksmsid() {
		return backsmsid;
	}

	/**
	 * 设置短信平台短信id
	 */
	public void setBacksmsid(String backsmsid) {
		this.backsmsid = backsmsid;
	}

	/**
	 * 获取
	 */
	public Integer getValidFlag() {
		return validFlag;
	}

	/**
	 * 设置
	 */
	public void setValidFlag(Integer validFlag) {
		this.validFlag = validFlag;
	}

	public Integer getSmstype() {
		return smstype;
	}

	public void setSmstype(Integer smstype) {
		this.smstype = smstype;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
}
