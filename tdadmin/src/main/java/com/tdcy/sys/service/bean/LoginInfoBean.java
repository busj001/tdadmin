package com.tdcy.sys.service.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.tdcy.framework.util.StringUtils;
import com.tdcy.sys.dao.eo.MenuEO;
import com.tdcy.sys.dao.eo.RolesEO;
import com.tdcy.sys.dao.eo.UserEO;
import com.tdcy.sys.dao.eo.UserKindEO;

public class LoginInfoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private UserKindEO userKindEO;
	private String address;

	private UserEO userEO;
	
	/**
	 * 该字段用户保存用户详情 操作员用户时为staffEO Erp用户时为ErpStaffEO
	 */
	private Object loginDetail;

	private List<RolesEO> roleEOList;

	private List<MenuEO> menuInfoEOList;
	
	private List<MenuEO> rightInfoEOList;

	public String getRoleName() {
		List<String> rolelsit = new ArrayList<String>();
		for (RolesEO re : roleEOList) {
			rolelsit.add(re.getRoleName());
		}
		return StringUtils.join(rolelsit, ",");
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<RolesEO> getRoleEOList() {
		return roleEOList;
	}

	public void setRoleEOList(List<RolesEO> roleEOList) {
		this.roleEOList = roleEOList;
	}

	public Object getLoginDetail() {
		return loginDetail;
	}

	public void setLoginDetail(Object loginDetail) {
		this.loginDetail = loginDetail;
	}

	public UserEO getUserEO() {
		return userEO;
	}

	public void setUserEO(UserEO userEO) {
		this.userEO = userEO;
	}

	public void setMenuInfoEOList(List<MenuEO> menuInfoEOList) {
		this.menuInfoEOList = menuInfoEOList;
	}

	public List<MenuEO> getMenuInfoEOList() {
		return menuInfoEOList;
	}

	public UserKindEO getUserKindEO() {
		return userKindEO;
	}

	public void setUserKindEO(UserKindEO userKindEO) {
		this.userKindEO = userKindEO;
	}

	public List<MenuEO> getRightInfoEOList() {
		return rightInfoEOList;
	}

	public void setRightInfoEOList(List<MenuEO> rightInfoEOList) {
		this.rightInfoEOList = rightInfoEOList;
	}


}
