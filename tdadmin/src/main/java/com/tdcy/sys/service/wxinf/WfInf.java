package com.tdcy.sys.service.wxinf;

import java.util.Map;

/**
 * 微信接口
 * @author Administrator
 *
 */
public abstract class WfInf {
	public abstract Map<String, String> execute(Object paramobj);
}
