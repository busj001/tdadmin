package com.tdcy.sys.service.bean;

import com.tdcy.sys.dao.eo.UserKindEO;


public class LoginCo {
	private String userKind;
	private String loginUser;
	private String password;
	private String checkCode;
	private String isRememberMe;
	
	private UserKindEO userKindEO;

	public UserKindEO getUserKindEO() {
		return userKindEO;
	}

	public void setUserKindEO(UserKindEO userKindEO) {
		this.userKindEO = userKindEO;
	}

	public String getIsRememberMe() {
		return isRememberMe;
	}

	public void setIsRememberMe(String isRememberMe) {
		this.isRememberMe = isRememberMe;
	}


	public String getUserKind() {
		return userKind;
	}

	public void setUserKind(String userKind) {
		this.userKind = userKind;
	}

	public String getLoginUser() {
		return loginUser;
	}

	public void setLoginUser(String loginUser) {
		this.loginUser = loginUser;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCheckCode() {
		return checkCode;
	}

	public void setCheckCode(String checkCode) {
		this.checkCode = checkCode;
	}

}
