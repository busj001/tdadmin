package com.tdcy.sys.service.validator;

import com.tdcy.framework.exception.BaseException;
import com.tdcy.framework.util.StringUtils;
import com.tdcy.sys.service.bean.LoginCo;

public class LoginValidator {
	public void validateLogin(LoginCo co) {
		if (StringUtils.isEmpty(co.getLoginUser())) {
			throw new BaseException("用户名不能为空");
		}

		if (StringUtils.isEmpty(co.getPassword())) {
			throw new BaseException("密码不能为空");
		}

	}
}
