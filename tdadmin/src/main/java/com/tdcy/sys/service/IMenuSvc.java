package com.tdcy.sys.service;

import java.util.List;

import com.tdcy.sys.dao.eo.MenuEO;

public interface IMenuSvc {
	public List<MenuEO> getAllMenu();
	public List<MenuEO> getAllRight();
	
	public MenuEO getMenuInfo(Integer menuid);

	public MenuEO updateMenu(MenuEO menuEO);

	public MenuEO addMenu(MenuEO menuEO);

	public boolean delMenu(Integer menuid);

	public List<MenuEO> getAllMenusByRoleId(Integer roleid);
	public List<MenuEO> getAllRightsByRoleId(Integer roleid) ;
}
