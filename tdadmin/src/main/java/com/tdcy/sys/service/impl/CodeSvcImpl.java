package com.tdcy.sys.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.tdcy.framework.util.StringUtils;
import com.tdcy.sys.dao.ICodeTableDAO;
import com.tdcy.sys.dao.ICodeTableDetailDAO;
import com.tdcy.sys.dao.eo.CodeTableDetailEO;
import com.tdcy.sys.dao.eo.CodeTableEO;
import com.tdcy.sys.service.ICodeSvc;
import com.tdcy.sys.service.bean.CodeBean;

@Service
public class CodeSvcImpl implements ICodeSvc {
	@Resource
	ICodeTableDAO codeTableDAO;

	@Resource
	ICodeTableDetailDAO codeTableDetailDAO;

	@Cacheable(value = "appCache", key = "'code_'+#codeType")
	public List<CodeBean> getCodeList(String codeType) {
		CodeTableEO cbeo = codeTableDAO.getCodeTable(codeType);
		List<CodeTableDetailEO> codeTableDetailList = codeTableDetailDAO
				.getCodeDetailList(codeType);

		List<CodeTableEO> codeTableList = new ArrayList<CodeTableEO>();
		if(cbeo!=null){
			codeTableList.add(cbeo);
			
		}
		List<CodeBean> list = transferCodebeans(codeTableList,
				codeTableDetailList, null);
		return list;
	}

	public List<CodeBean> getAllCodeList() {
		List<CodeBean> list = null;
		List<CodeTableEO> codeTableList = getAllCodeTable();
		List<CodeTableDetailEO> codeTableDetailList = getAllCodeTableDetail();

		list = transferCodebeans(codeTableList, codeTableDetailList, null);

		return list;
	}

	protected List<CodeBean> transferCodebeans(List<CodeTableEO> codeTableList,
			List<CodeTableDetailEO> codeTableDetailList, Integer jsid) {
		List<CodeBean> codeBeanList = new ArrayList<CodeBean>();
		if(codeTableList==null){
			return codeBeanList;
		}
		for (CodeTableEO cte : codeTableList) {
			List<CodeTableDetailEO> ctdeoList = null;
			if (!StringUtils.isEmpty(cte.getCodeSql())) {
				if (jsid != null) {
					ctdeoList = codeTableDAO.execSqlQuery(
							CodeTableDetailEO.class, cte.getCodeSql(),
							new Object[] { jsid });
				} else {
					if (!cte.getCodeSql().contains("?")) {
						ctdeoList = codeTableDAO.execSqlQuery(
								CodeTableDetailEO.class, cte.getCodeSql());
					}
				}

			} else {
				ctdeoList = getCodeTableDetailList(codeTableDetailList,
						cte.getCodeType());
			}

			if (ctdeoList != null) {
				for (CodeTableDetailEO eo : ctdeoList) {
					CodeBean cbean = new CodeBean();
					cbean.setCodeSql(cte.getCodeSql());
					cbean.setCodeType(cte.getCodeType());
					cbean.setDataValue(eo.getDataValue());
					cbean.setDisOrder(eo.getDisOrder());
					cbean.setDisplayValue(eo.getDisplayValue());
					codeBeanList.add(cbean);
				}
			}

		}

		return codeBeanList;
	}

	protected List<CodeTableDetailEO> getCodeTableDetailList(
			List<CodeTableDetailEO> codeTableDetailList, String codeType) {
		List<CodeTableDetailEO> ctdeoList = new ArrayList<CodeTableDetailEO>();
		for (CodeTableDetailEO eo : codeTableDetailList) {
			if (eo.getCodeType().equals(codeType)) {
				ctdeoList.add(eo);
			}
		}
		return ctdeoList;
	}

	protected List<CodeTableEO> getAllCodeTable() {
		return codeTableDAO.findTableRecords(CodeTableEO.class);
	}

	protected List<CodeTableDetailEO> getAllCodeTableDetail() {
		return codeTableDetailDAO.findTableRecords(CodeTableDetailEO.class);
	}
}
