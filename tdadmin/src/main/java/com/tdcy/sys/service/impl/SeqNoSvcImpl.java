package com.tdcy.sys.service.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tdcy.framework.exception.BaseException;
import com.tdcy.framework.util.ContextUtils;
import com.tdcy.sys.dao.ISerialDAO;
import com.tdcy.sys.dao.eo.SerialEO;
import com.tdcy.sys.service.ISeqNoSvc;

@Service
public class SeqNoSvcImpl implements ISeqNoSvc {
	@Resource
	ISerialDAO serialDAO;

	/**
	 * 采用独立的连接,事务也是独立的,从而使获取序列号的事务不影响主线的事务处理，仅申请1个序列号.
	 * 
	 * @param MaxNo_Name
	 *            String 序列号名
	 * @return long
	 * @throws BaseException
	 *             the hygeia exception
	 */
	public int getMaxNo(String MaxNo_Name) throws BaseException {
		return getMaxNo(MaxNo_Name, 1);
	}

	/**
	 * 获取序列号,传入使用的数据库联接，指定申请序列号个数，可指定是否提交数据库联接.
	 * 
	 * @param name
	 *            String 序列号名
	 * @param MaxNo_Size
	 *            int 申请的序列号的个数,当插入多条明细数据时，不需要每次都申请，可一次申请指定数量的连续的序列号。
	 * @param use_connection
	 *            Connection 使用的数据库联接变量
	 * @param isCommit
	 *            boolean 是否提交数据库联接
	 * @return long
	 * @throws BaseException
	 *             the hygeia exception
	 */
	@Transactional
	public int getMaxNo(String MaxNo_Name, int MaxNo_Size) throws BaseException {
		JdbcTemplate jdbcTemplate = (JdbcTemplate) ContextUtils.getApplicationContext().getBean("jdbcTemplate");

		Connection conn = null;
		Statement stat = null;
		ResultSet rest = null;
		try {
			conn = jdbcTemplate.getDataSource().getConnection();
			conn.setAutoCommit(false);
			int cur_num = 0;

			String getsql = "select cur_num_no from sys_serial" + " where serial_type='" + MaxNo_Name + "' for update";
			stat = conn.createStatement();
			rest = stat.executeQuery(getsql);
			if (rest.next()) {
				cur_num = rest.getInt(1);
			} else {
				String insertsql = "insert sys_serial(serial_type,cur_num_no) values('" + MaxNo_Name + "','0')";
				stat.executeUpdate(insertsql);
			}

			String updatesql = "update sys_serial set cur_num_no=" + (cur_num + MaxNo_Size) + " where serial_type = '" + MaxNo_Name + "'";
			stat.executeUpdate(updatesql);
			conn.commit();
			return ++cur_num;
		} catch (SQLException e1) {
			e1.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			throw new BaseException("获取序列号失败");
		} finally {
			try {
				if (rest != null) {
					rest.close();
				}
				if (stat != null) {
					stat.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				throw new BaseException("获取序列号失败");
			}
		}
	}

	/**
	 * 获取序列号,传入使用的数据库联接，指定申请序列号个数，可指定是否提交数据库联接.
	 * 
	 * @param name
	 *            String 序列号名
	 * @param MaxNo_Size
	 *            int 申请的序列号的个数,当插入多条明细数据时，不需要每次都申请，可一次申请指定数量的连续的序列号。
	 * @param use_connection
	 *            Connection 使用的数据库联接变量
	 * @param isCommit
	 *            boolean 是否提交数据库联接
	 * @return long
	 * @throws BaseException
	 *             the hygeia exception
	 */
	@Transactional
	public int getMaxNoTest(String MaxNo_Name, int MaxNo_Size) throws BaseException {
		int cur_num = 0;

		SerialEO seo = serialDAO.querySerialEO(MaxNo_Name);

		if (seo == null) {
			seo = new SerialEO();
			seo.setSerialType(MaxNo_Name);
			seo.setCurNumNo(1);
			serialDAO.save(seo);
		} else {
			cur_num = seo.getCurNumNo();
		}

		System.out.println(cur_num);
		
		seo = new SerialEO();
		seo.setSerialType(MaxNo_Name);
		seo.setCurNumNo(cur_num + MaxNo_Size);
		serialDAO.update(seo);
		return ++cur_num;
	}

}
