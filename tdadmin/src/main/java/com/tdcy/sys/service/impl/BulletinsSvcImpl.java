package com.tdcy.sys.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.tdcy.framework.bean.PageInfo;
import com.tdcy.sys.dao.IBulletinsDAO;
import com.tdcy.sys.dao.eo.BulletinsEO;
import com.tdcy.sys.service.IBulletinsSvc;
import com.tdcy.sys.service.ISeqNoSvc;
import com.tdcy.sys.service.bean.BulletinQueryCondition;

/**
* @Description Service实现类
*/
@Service
public class BulletinsSvcImpl implements IBulletinsSvc {

	@Resource
	IBulletinsDAO bulletinsDAO;

	@Resource
	ISeqNoSvc seqSvc;

	public List<BulletinsEO> getAllBulletins() {
		return bulletinsDAO.findTableRecords(BulletinsEO.class);
	}

	@Override
	public BulletinsEO getBulletinsInfo(Integer bulletinsId) {
		return bulletinsDAO.findByPrimaryKey(BulletinsEO.class, bulletinsId);
	}

	@Override
	public BulletinsEO updateBulletins(BulletinsEO eo) {
		return bulletinsDAO.update(eo);
	}

	@Override
	public BulletinsEO addBulletins(BulletinsEO eo) {
		eo.setBulletinId(seqSvc.getMaxNo("bulletin_id"));
		return bulletinsDAO.save(eo);
	}

	@Override
	public boolean delBulletins(Integer bulletinsid) {
		return bulletinsDAO.deleteByPrimaryKey(BulletinsEO.class, bulletinsid);
	}

	@Override
	public PageInfo<BulletinsEO> getBulletinPageInfo(BulletinQueryCondition queryco) {
		return bulletinsDAO.getPageInfo(queryco);
	}
}
