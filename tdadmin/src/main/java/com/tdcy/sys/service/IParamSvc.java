package com.tdcy.sys.service;

import java.util.List;

import com.tdcy.sys.dao.eo.ParamEO;
import com.tdcy.sys.service.bean.ParamQueryCondition;

public interface IParamSvc {
	public List<ParamEO> getAllParam(ParamQueryCondition qc);

	public ParamEO getParam(String key);

	public ParamEO updateParam(ParamEO param);

	public ParamEO addParam(ParamEO param);

	public void saveParam(List<ParamEO> param);

	public boolean delParam(String paramKey);
}
