package com.tdcy.sys.service;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import com.tdcy.sys.dao.eo.FileFileEO;

/**
 * @Description Service类
 */
public interface IFileSvc {
	public FileFileEO getFileFileInfo(int id) throws IOException;

	public boolean delFileFile(int id,boolean deleteFile) throws IOException;

	public FileFileEO addFileFile(MultipartFile file) throws IOException;

	public FileFileEO addFileFile(String filename, String contentType,
			byte[] data) throws IOException;
}
