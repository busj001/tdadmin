package com.tdcy.sys.service;

import java.util.List;

import com.tdcy.sys.dao.eo.RolesEO;
import com.tdcy.sys.dao.eo.UserEO;

/**
 * @Description Service类
 */
public interface IRolesSvc {
	public List<RolesEO> getAllRoles();
	
	public RolesEO getRolesInfo(Integer role_id);

	public RolesEO updateRoles(RolesEO eo, String rightstr);

	public RolesEO addRoles(RolesEO eo, String rightstr);

	public boolean delRoles(Integer role_id);

	public List<UserEO> getRoleUser(Integer roleid);
}
