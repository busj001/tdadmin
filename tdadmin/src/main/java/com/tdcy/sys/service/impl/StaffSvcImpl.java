package com.tdcy.sys.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tdcy.framework.exception.BaseException;
import com.tdcy.framework.util.CryptoHUtils;
import com.tdcy.framework.util.StringUtils;
import com.tdcy.sys.dao.IRolesDAO;
import com.tdcy.sys.dao.IStaffDAO;
import com.tdcy.sys.dao.eo.RolesEO;
import com.tdcy.sys.dao.eo.StaffEO;
import com.tdcy.sys.dao.eo.UserEO;
import com.tdcy.sys.service.ISeqNoSvc;
import com.tdcy.sys.service.IStaffSvc;
import com.tdcy.sys.service.IUserSvc;
import com.tdcy.sys.service.bean.StaffInfoBean;
import com.tdcy.sys.service.bean.StaffQueryCondition;
import com.tdcy.sys.util.ParamUtils;
import com.tdcy.sys.util.UserUtils;

/**
 * @Description Service实现类
 */
@Service
public class StaffSvcImpl implements IStaffSvc {

	@Resource
	IStaffDAO staffDAO;

	@Resource
	IUserSvc userSvc;

	@Resource
	ISeqNoSvc seqSvc;

	@Resource
	IRolesDAO roleDAO;

	public List<StaffEO> getAllStaff() {
		return staffDAO.findTableRecords(StaffEO.class);
	}

	@Override
	public StaffInfoBean getStaffInfo(Integer staffId) {
		StaffInfoBean bean = new StaffInfoBean();

		StaffEO eo = staffDAO.findByPrimaryKey(StaffEO.class, staffId);
		bean.setStaffEO(eo);

		List<RolesEO> roleList = roleDAO.getRolesByLoginUser(eo.getLoginUser());
		bean.setRoleList(roleList);

		return bean;
	}

	@Override
	@Transactional
	public StaffEO updateStaff(StaffEO staff, String rolestr, String password) {
		staff = staffDAO.update(staff);

		UserEO e = userSvc.getUserInfoByLoginUser(staff.getLoginUser());
		if (!StringUtils.isEmpty(password)) {
			e.setPassword(password);
		}
		e.setUserName(staff.getStaffName());

		userSvc.updateUser(e, rolestr);

		return staff;
	}

	@Override
	@Transactional
	public StaffEO addStaff(StaffEO staff, String rolestr, String password) {
		StaffEO e = staffDAO.findByLoginUser(staff.getLoginUser());
		if (e != null) {
			throw new BaseException("用户[" + staff.getLoginUser() + "]已经存在");
		}

		staff.setStaffId(seqSvc.getMaxNo("staff_id"));
		staff.setUserKind(UserUtils.USER_KIND_STAFF);
		staff = staffDAO.save(staff);

		UserEO user = new UserEO();
		user.setUserName(staff.getStaffName());
		user.setLoginUser(staff.getLoginUser());
		if (StringUtils.isEmpty(password)) {
			password = CryptoHUtils.md5(ParamUtils.getParamValue("default_password"));
		}
		user.setPassword(password);
		user.setUserKind(staff.getUserKind());
		userSvc.addUser(user, rolestr);

		return staff;
	}

	@Override
	public boolean delStaff(Integer staffid) {
		StaffEO se = staffDAO.findByPrimaryKey(StaffEO.class, staffid);
		UserEO ue = userSvc.getUserInfoByLoginUser(se.getLoginUser());
		if (ue != null) {
			userSvc.delUser(ue.getUserId());
		}
		return staffDAO.deleteByPrimaryKey(StaffEO.class, staffid);
	}

	@Override
	public List<StaffEO> queryStaff(StaffQueryCondition staffCo) {
		return staffDAO.queryStaff(staffCo);
	}

	@Override
	public StaffEO getStaffInfoByLoginUser(String loginUser) {
		return staffDAO.findByLoginUser(loginUser);
	}
}
