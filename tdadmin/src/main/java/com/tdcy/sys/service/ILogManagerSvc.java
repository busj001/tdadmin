package com.tdcy.sys.service;

import java.util.List;
import java.util.Map;

import com.tdcy.framework.bean.PageInfo;
import com.tdcy.sys.dao.eo.LoginLogEO;
import com.tdcy.sys.dao.eo.OperateLogEO;
import com.tdcy.sys.dao.eo.SystemLogEO;
import com.tdcy.sys.service.bean.LoginInfoBean;
import com.tdcy.sys.service.bean.LoginLogQueryCondition;
import com.tdcy.sys.service.bean.OperLogQueryCondition;

/**
 * @Description Service类
 */
public interface ILogManagerSvc {
	public LoginLogEO addLoginLog(LoginInfoBean login);

	public List<LoginLogEO> getAllLoginLog();

	public PageInfo<LoginLogEO> getloginPageInfo(LoginLogQueryCondition queryco);

	public LoginLogEO getLoginLogInfo(Integer login_id);

	public LoginLogEO updateLoginLog(Integer login_id, LoginLogEO eo);

	public LoginLogEO addLoginLog(LoginLogEO eo);

	public boolean delLoginLog(Integer login_id);

	public List<OperateLogEO> getAllOperateLog();

	public PageInfo<OperateLogEO> getOperatePageInfo(OperLogQueryCondition queryco);
	
	public List<OperateLogEO> getOperateList(OperLogQueryCondition queryco);

	public OperateLogEO getOperateLogInfo(Integer operate_id);

	public OperateLogEO updateOperateLog(OperateLogEO eo);

	public OperateLogEO addOperateLog(OperateLogEO eo);

	public boolean delOperateLog(Integer operate_id);

	public List<SystemLogEO> getAllSystemLog();

	public PageInfo<SystemLogEO> getSystemPageInfo(Map<String, String> queryParam, PageInfo pageinfo);

	public SystemLogEO getSystemLogInfo(Integer log_sn);

	public SystemLogEO updateSystemLog(SystemLogEO ep);

	public SystemLogEO addSystemLog(SystemLogEO ep);

	public boolean delSystemLog(int log_sn);
}
