package com.tdcy.sys.util;

public class ExpImpUtils {
	public static final String TEMPLATE_CLASS = "template_class";
	public static final String TEMPLATE_NAME = "template_name";
	public static final String TEMPLATE_LIST_DATA = "template_list_data";
	public static final String TEMPLATE_MAP_DATA = "template_map_data";
	public static final String EXPORTFILE_NAME = "exportfile_name";
}
