package com.tdcy.sys.util.shiro;

import com.tdcy.sys.service.bean.LoginCo;
import com.tdcy.sys.service.bean.LoginInfoBean;

/**
 * 用户名密码令牌
 * 
 */
public class UsernamePasswordToken extends
		org.apache.shiro.authc.UsernamePasswordToken {
	private LoginCo loginCo;//用户类型
	private LoginInfoBean loginInfo; //用來初始化参数
	
	public LoginInfoBean getLoginInfo() {
		return loginInfo;
	}

	public void setLoginInfo(LoginInfoBean loginInfo) {
		this.loginInfo = loginInfo;
	}

	

	public LoginCo getLoginCo() {
		return loginCo;
	}

	public void setLoginCo(LoginCo loginCo) {
		this.loginCo = loginCo;
	}

	public UsernamePasswordToken() {
		super();
	}

	/**
	 * 构造方法
	 * 
	 */
	public UsernamePasswordToken(LoginCo loginCo) {
		super(loginCo.getLoginUser(), loginCo.getPassword().toCharArray());
	}
}