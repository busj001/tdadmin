package com.tdcy.sys.util;

import java.util.List;

import com.tdcy.framework.util.ContextUtils;
import com.tdcy.framework.util.JsonUtils;
import com.tdcy.sys.service.ICodeSvc;
import com.tdcy.sys.service.bean.CodeBean;

public class CodeTableUtils {
	public static List<CodeBean> geteCodeList(String codeType) {
		ICodeSvc svc = ContextUtils.getApplicationContext().getBean(
				ICodeSvc.class);
		List<CodeBean> list =  svc.getCodeList(codeType);
		return list;
	}

	public static CodeBean getCodeBean(String codeType, String dataValue) {
		List<CodeBean> list = geteCodeList(codeType);
		for (CodeBean cb : list) {
			if (cb.getCodeType().equals(codeType)
					&& cb.getDataValue().equals(dataValue)) {
				return cb;
			}
		}

		return null;
	}

	public static String getCodeBeanJson(String codeType) {
		ICodeSvc svc = ContextUtils.getApplicationContext().getBean(
				ICodeSvc.class);
		List<CodeBean> cblist = svc.getCodeList(codeType);
		if (cblist != null) {
			return JsonUtils.toJson(cblist);
		} else {
			return null;
		}
	}

	public static String getDisplayValue(String codeType, String dataValue) {
		CodeBean cb = getCodeBean(codeType, dataValue);
		if (cb != null) {
			return cb.getDisplayValue();
		} else {
			return null;
		}
	}

}
