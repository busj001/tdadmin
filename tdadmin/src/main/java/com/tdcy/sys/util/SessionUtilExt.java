package com.tdcy.sys.util;

import javax.servlet.http.HttpServletRequest;

import com.tdcy.framework.util.SessionUtils;
import com.tdcy.sys.constant.AppConstants;
import com.tdcy.sys.service.bean.LoginInfoBean;

public class SessionUtilExt extends SessionUtils {
	public static LoginInfoBean getLoginInfo(HttpServletRequest request) {
		Object obj = getSesion(request.getSession(), AppConstants.SESSION_LOGIN_INFO);
		if (obj == null) {
			return null;
		} else {
			return (LoginInfoBean) obj;
		}
	}

	public static void setLoginInfo(HttpServletRequest request, LoginInfoBean loginInfo) {
		setSesion(request.getSession(), AppConstants.SESSION_LOGIN_INFO, loginInfo);
	}

	public static void removeLoginInfo(HttpServletRequest request) {
		setSesion(request.getSession(), AppConstants.SESSION_LOGIN_INFO, null);
	}
}
