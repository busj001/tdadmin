package com.tdcy.sys.util;

import java.util.ArrayList;
import java.util.List;

import com.tdcy.framework.exception.BaseException;
import com.tdcy.framework.util.ContextUtils;
import com.tdcy.sys.dao.IRolesDAO;
import com.tdcy.sys.dao.eo.RolesEO;
import com.tdcy.sys.dao.eo.UserKindEO;

public class UserUtils {
	public final static int USER_KIND_ADMIN = 9; // 管理员用户
	public final static int USER_KIND_STAFF = 8;// 员工
	public final static int USER_KIND_CUSTOMER = 7;// 集团用户

	public static boolean isAdminKind(UserKindEO eo) {
		if (eo.getUserKind().compareTo(USER_KIND_ADMIN)==0) {
			return true;
		}

		return false;
	}
	
	public static boolean isStaffKind(UserKindEO eo) {
		if (eo.getUserKind().compareTo(USER_KIND_STAFF)==0) {
			return true;
		}

		return false;
	}

	public static boolean isCustomerKind(UserKindEO eo) {
		if (eo.getUserKind().compareTo(USER_KIND_CUSTOMER)==0) {
			return true;
		}

		return false;
	}

	
	public static List<UserKindEO> removeAdminKind(List<UserKindEO> eos) {
		List<UserKindEO> teo = new ArrayList<UserKindEO>();
		for (UserKindEO reo : eos) {
			if (reo.getUserKind().compareTo(USER_KIND_ADMIN) != 0) {
				teo.add(reo);
			}
		}

		return teo;
	}

	public static final String ADMIN_ROLE = "role:admin";
	public static final String CUSTOMER_ROLE = "role:customer";

	public static boolean isAdminRole(List<RolesEO> roleList) {
		boolean flag = false;
		for (RolesEO eo : roleList) {
			if (eo.getRoleCode().equals(ADMIN_ROLE)) {
				flag = true;
				break;
			}
		}
		return flag;
	}

	public static boolean isAdminRole(RolesEO eo) {
		boolean flag = false;
		if (eo.getRoleCode().equals(ADMIN_ROLE)) {
			flag = true;
		}
		return flag;
	}

	public static Integer getAdminRoleID() {
		IRolesDAO dao = ContextUtils.getApplicationContext().getBean(IRolesDAO.class);
		RolesEO eos = dao.findByCode(ADMIN_ROLE);

		if (eos != null) {
			return eos.getRoleId();
		} else {
			throw new BaseException("找不到管理员的角色	");
		}
	}

	public static boolean isCustomerRole(List<RolesEO> roleList) {
		boolean flag = false;
		for (RolesEO eo : roleList) {
			if (eo.getRoleCode().equals(CUSTOMER_ROLE)) {
				flag = true;
				break;
			}
		}
		return flag;
	}

	public static boolean isCustomerRole(RolesEO eo) {
		boolean flag = false;
		if (eo.getRoleCode().equals(CUSTOMER_ROLE)) {
			flag = true;
		}
		return flag;
	}

	public static Integer getCustomerRoleID() {
		IRolesDAO dao = ContextUtils.getApplicationContext().getBean(IRolesDAO.class);
		RolesEO eos = dao.findByCode(CUSTOMER_ROLE);

		if (eos != null) {
			return eos.getRoleId();
		} else {
			throw new BaseException("找不到集团用户的角色	");
		}
	}

	public static List<RolesEO> removeAdminRoles(List<RolesEO> eos) {
		Integer adminId = getAdminRoleID();
		List<RolesEO> teo = new ArrayList<RolesEO>();
		for (RolesEO reo : eos) {
			if (reo.getRoleId().compareTo(adminId) != 0) {
				teo.add(reo);
			}
		}

		return teo;
	}
}
