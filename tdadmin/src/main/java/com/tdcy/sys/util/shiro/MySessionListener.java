package com.tdcy.sys.util.shiro;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.SessionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MySessionListener implements SessionListener {
	public Logger logger = LoggerFactory.getLogger(MySessionListener.class);

	@Override
	public void onStart(Session session) {
		// 不执行任何操作
	}

	@Override
	public void onStop(Session session) {

		underline(session.getId().toString());
	}

	@Override
	public void onExpiration(Session session) {

		underline(session.getId().toString());
	}

	protected void underline(String sessionkey) {
//		UserSessionExample example = new UserSessionExample();
//		example.createCriteria().andSessionkeyEqualTo(sessionkey);
//
//		UserSession record = new UserSession();
//		record.setState(0);// 下线
//		record.setLogouttime(new Timestamp(System.currentTimeMillis()));
//
//		userSessionService.updateByExampleSelective(record, example);
		logger.debug("session user："+sessionkey +" down");
	}

}
