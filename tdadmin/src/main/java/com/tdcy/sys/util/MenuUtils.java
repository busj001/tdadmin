package com.tdcy.sys.util;

import java.util.ArrayList;
import java.util.List;

import com.tdcy.sys.dao.eo.MenuEO;

public class MenuUtils {
	public static List<MenuEO> removeDupMenu(List<MenuEO> menulist) {
		List<MenuEO> tlsit = new ArrayList<MenuEO>();

		for (MenuEO eo : menulist) {
			boolean flag = false;
			for (MenuEO t : tlsit) {
				if (eo.getMenuId().compareTo(t.getMenuId())==0) {
					flag = true;
					break;
				}
			}
			if (flag) {
				continue;
			} else {
				tlsit.add(eo);
			}
		}
		return tlsit;
	}


}
