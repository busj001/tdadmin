package com.tdcy.sys.util.wx;

import com.tdcy.framework.util.ContextUtils;
import com.tdcy.sys.dao.IWxInfLogDAO;
import com.tdcy.sys.dao.eo.WxInfLogEO;

public class AddWxInfLogThread implements Runnable {
	private WxInfLogEO eo;

	public WxInfLogEO getEo() {
		return eo;
	}

	public void setEo(WxInfLogEO eo) {
		this.eo = eo;
	}

	public void run() {
		IWxInfLogDAO dao = ContextUtils.getApplicationContext().getBean(IWxInfLogDAO.class);
		dao.save(eo);
	}
}
