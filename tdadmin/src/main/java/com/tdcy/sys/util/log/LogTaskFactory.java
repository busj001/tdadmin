package com.tdcy.sys.util.log;

import java.util.Date;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tdcy.framework.util.ContextUtils;
import com.tdcy.framework.util.ExceptionUtils;
import com.tdcy.sys.dao.eo.LoginLogEO;
import com.tdcy.sys.dao.eo.OperateLogEO;
import com.tdcy.sys.dao.eo.UserEO;
import com.tdcy.sys.service.ILogManagerSvc;

/**
 * 日志操作任务创建工厂
 *
 * @author chenyong
 * @date 2016年12月6日 下午9:18:27
 */
public class LogTaskFactory {
    private static Logger logger = LoggerFactory.getLogger(LogTaskFactory.class);	
    private static ILogManagerSvc logmanger = ContextUtils.getBean(ILogManagerSvc.class);

    public static TimerTask loginLog(final UserEO user, final String ip) {
        return new TimerTask() {
            @Override
            public void run() {
                try {
                    LoginLogEO loginLog = createLoginLog( user, null, ip);
                    logmanger.addLoginLog(loginLog);
                } catch (Exception e) {
                    logger.error("创建登录日志异常!", e);
                }
            }
        };
    }

    public static TimerTask loginLog(final String username, final String msg, final String ip) {
        return new TimerTask() {
            @Override
            public void run() {
            	UserEO user = new UserEO();
            	user.setLoginUser(username);
                LoginLogEO loginLog = createLoginLog( user, "账号:" + username + "," + msg, ip);
                try {
                	logmanger.addLoginLog(loginLog);
                } catch (Exception e) {
                    logger.error("创建登录失败异常!", e);
                }
            }
        };
    }

    public static TimerTask exitLog(final UserEO userId, final String ip) {
        return new TimerTask() {
            @Override
            public void run() {
                LoginLogEO loginLog = createLoginLog( userId, null,ip);
                try {
                	logmanger.addLoginLog(loginLog);
                } catch (Exception e) {
                    logger.error("创建退出日志异常!", e);
                }
            }
        };
    }

    public static TimerTask bussinessLog(final UserEO usereo,final String bussinessName,final String bussinessUrl, final String clazzName,final String methodName, final String msg) {
        return new TimerTask() {
            @Override
            public void run() {
                OperateLogEO operationLog = createOperationLog(usereo, bussinessName, bussinessUrl, clazzName,methodName, msg);
                try {
                	logmanger.addOperateLog(operationLog);
                } catch (Exception e) {
                    logger.error("创建业务日志异常!", e);
                }
            }
        };
    }

    public static TimerTask exceptionLog(final UserEO user, final Exception exception) {
        return new TimerTask() {
            @Override
            public void run() {
                String msg = ExceptionUtils.getErrorStr(exception);
                OperateLogEO operationLog = createOperationLog( user, "", null, null,null, msg);
                try {
                	logmanger.addOperateLog(operationLog);
                } catch (Exception e) {
                    logger.error("创建异常日志异常!", e);
                }
            }
        };
    }
    
    /**
     * 创建操作日志
     *
     * @author chenyong
     * @Date 2017/3/30 18:45
     */
    public static OperateLogEO createOperationLog(UserEO usereo,String bussinessName,String bussinessUrl, String clazzName, String methodName, String msg) {
    	OperateLogEO operationLog = new OperateLogEO();
        operationLog.setOperateUrl(bussinessUrl);;
        operationLog.setOperateClass(clazzName);
        operationLog.setOperateMethod(methodName);
        operationLog.setOperateName(bussinessName);
        if(usereo!=null){
        	 operationLog.setUserId(usereo.getUserId());
             operationLog.setLoginUser(usereo.getLoginUser());
             operationLog.setUserName(usereo.getUserName());
        }
       
        operationLog.setRemark(msg);
        return operationLog;
    }

    /**
     * 创建登录日志
     *
     * @Date 2017/3/30 18:46
     */
    public static LoginLogEO createLoginLog(UserEO usereo, String msg,String ip) {
    	LoginLogEO loginLog = new LoginLogEO();
    	if(usereo!=null){
    		loginLog.setUserId(usereo.getUserId());
            loginLog.setLoginUser(usereo.getLoginUser()	);
            loginLog.setUserName(usereo.getUserName());
    	}
        
        loginLog.setLoginDate(new Date());
        loginLog.setLoginFlag(1);
        loginLog.setLoginAddress(ip);
        loginLog.setRemark(msg);
        return loginLog;
    }
}
