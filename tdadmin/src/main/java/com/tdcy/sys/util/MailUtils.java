package com.tdcy.sys.util;

import com.tdcy.framework.exception.BaseException;
import com.tdcy.framework.mail.SimpleMailSender;
import com.tdcy.framework.mail.entity.MailSenderInfo;
import com.tdcy.framework.util.StringUtils;
import com.tdcy.framework.util.TaskController;

public class MailUtils {
	public static void sendMail(MailSenderInfo mailInfo) {
		String mailsupport = ParamUtils.getParamValue("mailsupport");
		if ("1".equals(mailsupport)) {
			if (mailInfo == null) {
				throw new BaseException("邮件信息不能为空！");
			}
			if (StringUtils.isEmpty(mailInfo.getToAddress())) {
				throw new BaseException("邮箱地址不能为空！");
			}
			if (StringUtils.isEmpty(mailInfo.getSubject())) {
				throw new BaseException("邮件标题不能为空！");
			}
			if (StringUtils.isEmpty(mailInfo.getContent())) {
				throw new BaseException("邮件内容不能为空！");
			}
			SendMialTask saveTask = new SendMialTask(mailInfo);
			TaskController.execute(saveTask);
		}

	}

}

class SendMialTask implements Runnable {
	private MailSenderInfo mailInfo;// 保存邮件信息

	public SendMialTask(MailSenderInfo mailInfo) {
		this.mailInfo = mailInfo;
	}

	public void run() {
		mailInfo.setUserName(ParamUtils.getParamValue("mail_username"));// 设置用户名
		mailInfo.setPassword(ParamUtils
				.getParamValue("mail_password"));// 您的邮箱密码
		mailInfo.setFromAddress(ParamUtils.getParamValue("mail_address"));// 您的邮箱地址
		// 发送对象
		SimpleMailSender sms = new SimpleMailSender(true);
		sms.setSmtpType("smtps");// 设置邮件类型，用公司邮箱发送邮件需要此设置，其它邮箱发送，不要进行该操作，直接默认为smtp
		sms.setDebug(new Boolean(ParamUtils.getParamValue("mail_debug")));// 设置为true，打印发送邮件日志信息
		sms.sendMail(mailInfo);

		// 发送邮件的代码

	}
}