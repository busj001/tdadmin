package com.tdcy.sys.util;

import com.tdcy.framework.util.ContextUtils;
import com.tdcy.sys.dao.IParamDAO;
import com.tdcy.sys.dao.eo.ParamEO;
import com.tdcy.sys.service.IParamSvc;

public class ParamUtils {
	public static ParamEO getParam(String key) {
		IParamSvc svc = ContextUtils.getApplicationContext().getBean(
				IParamSvc.class);
		return svc.getParam(key);
	}

	public static ParamEO getParamByDAO(String key) {
		IParamDAO svc = ContextUtils.getApplicationContext().getBean(
				IParamDAO.class);
		return svc.getParam(key);
	}

	public static String getParamValueMoneyFormatter(String key) {
		Double param = Double.parseDouble(getParamValue(key));
		java.text.DecimalFormat df = new java.text.DecimalFormat("#.00");
		return df.format(param);
	}

	public static String getParamValueMoneyFormatter(String key,
			String defaultVale) {
		String paramStr = getParamValue(key);
		if (paramStr == null || paramStr.equals("")) {
			return defaultVale;
		}
		Double param = Double.parseDouble(getParamValue(key));
		java.text.DecimalFormat df = new java.text.DecimalFormat("#.00");
		return df.format(param);
	}

	public static String getParamValue(String key) {
		ParamEO eo = getParam(key);
		if (eo == null) {
			return "";
		}
		return eo.getValue();
	}

	public static String getParamValue(String key, String defaultValue) {
		ParamEO eo = getParam(key);
		if (eo == null) {
			return defaultValue;
		}
		return eo.getValue();
	}

	public static Double getParamValue(String key, Double defaultValue) {
		ParamEO eo = getParam(key);
		if (eo == null) {
			return defaultValue;
		}
		String _value = eo.getValue();
		try {
			return Double.parseDouble(_value);
		} catch (Exception e) {
			return defaultValue;
		}
	}

	/**
	 * 实时获取
	 * 
	 * @param key
	 * @param defaultValue
	 * @return
	 */
	public static String getSSParamValue(String key, String defaultValue) {
		ParamEO eo = getParamByDAO(key);
		if (eo == null) {
			return defaultValue;
		}
		return eo.getValue();
	}

	/**
	 * 实时获取
	 * 
	 * @param key
	 * @param defaultValue
	 * @return
	 */
	public static Double getSSParamValue(String key, Double defaultValue) {
		ParamEO eo = getParamByDAO(key);
		if (eo == null) {
			return defaultValue;
		}
		String _value = eo.getValue();
		try {
			return Double.parseDouble(_value);
		} catch (Exception e) {
			return defaultValue;
		}
	}

	public static String getWxAppId() {
		ParamEO eo = getParam("wx_appid");
		return eo.getValue();
	}

	public static String getWxMchId() {
		ParamEO eo = getParam("wx_mch_id");
		return eo.getValue();
	}

	public static String getWxApiSecretkey() {
		ParamEO eo = getParam("wx_api_secretkey");
		return eo.getValue();
	}

	public static String getWxAppSecretkey() {
		ParamEO eo = getParam("wx_app_secretkey");
		return eo.getValue();
	}

}
