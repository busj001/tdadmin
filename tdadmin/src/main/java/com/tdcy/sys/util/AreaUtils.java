package com.tdcy.sys.util;

import java.util.ArrayList;
import java.util.List;

import com.tdcy.framework.util.ContextUtils;
import com.tdcy.sys.dao.eo.AreaEO;
import com.tdcy.sys.service.IAreaSvc;

public class AreaUtils {
	public static List<AreaEO> getAreaListByCityId(String cityId) {
		IAreaSvc svc = ContextUtils.getApplicationContext().getBean(IAreaSvc.class);
		List<AreaEO> areaList = svc.getAllArea();
		List<AreaEO> retList = new ArrayList<AreaEO>();
		Integer ciryid = Integer.parseInt(cityId);
		for (AreaEO ae : areaList) {
			if (ae.getAreaUpCode().compareTo(ciryid) == 0) {
				retList.add(ae);
			}
		}
		return retList;
	}

	public static String getAreaName(String code) {
		IAreaSvc svc = ContextUtils.getApplicationContext().getBean(IAreaSvc.class);
		AreaEO ae = svc.getAreaByCode(code);
		if (ae != null) {
			return ae.getAreaName();
		}
		return null;
	}
	
	public static String getZipCode(String code) {
		IAreaSvc svc = ContextUtils.getApplicationContext().getBean(IAreaSvc.class);
		AreaEO ae = svc.getAreaByCode(code);
		if (ae != null) {
			return ae.getZipcode();
		}
		return null;
	}

	public static String getAreaAllName(String code) {
		String prov = code.substring(0, 2);
		String city = code.substring(2, 4);
		String country = code.substring(4, 6);

		String areaname = "";
		String provname = getAreaName(prov + "0000");

		if ("00".equals(city)) {
			areaname = provname;
			return areaname;
		} else {
			String cityname = getAreaName(prov + city + "00");

			if ("00".equals(country)) {
				areaname = provname + cityname;
				return areaname;
			} else {
				String coutryname = getAreaName(prov + city + country);
				areaname = provname + cityname + coutryname;
				return areaname;
			}
		}
	}

	public static void main(String[] args) {
		String s = "431234";
		System.out.println(s.substring(0, 2));
		System.out.println(s.substring(2, 4));
		System.out.println(s.substring(4, 6));
	}
}
