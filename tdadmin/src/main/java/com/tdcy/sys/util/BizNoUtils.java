package com.tdcy.sys.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import com.tdcy.framework.util.DateUtils;
import com.tdcy.framework.util.StringUtils;

/**
 * 业务编号生成类
 * 
 * @author Administrator
 * 
 */
public class BizNoUtils {
	public static final String PREFIEX_ADMIN = "AM_";
	public static final String PREFIEX_TERMIN = "JT_";
	public static final String PREFIEX_JSSTAFF = "ST_";
	public static final String PREFIEX_ORDER = "OD_";
	public static final String PRODUCT_ORDER_FLAG = "C";
	public static final String ROOM_ORDER_FLAG = "H";

	public static final Map<String, String> digtialMap = new HashMap<String, String>();

	static {
		digtialMap.put("10", "A");
		digtialMap.put("11", "B");
		digtialMap.put("12", "C");
		digtialMap.put("13", "D");
		digtialMap.put("14", "E");
		digtialMap.put("15", "F");
		digtialMap.put("16", "G");
		digtialMap.put("17", "H");
		digtialMap.put("18", "I");
		digtialMap.put("19", "J");
		digtialMap.put("20", "K");
		digtialMap.put("21", "L");
		digtialMap.put("22", "M");
		digtialMap.put("23", "N");
		digtialMap.put("24", "O");
		digtialMap.put("25", "P");
		digtialMap.put("26", "Q");
		digtialMap.put("27", "R");
		digtialMap.put("28", "S");
		digtialMap.put("29", "T");
		digtialMap.put("30", "U");
		digtialMap.put("31", "V");
		digtialMap.put("32", "W");
		digtialMap.put("33", "X");
		digtialMap.put("34", "Y");
		digtialMap.put("35", "Z");
	}

	public static void main(String[] args) {
		SimpleDateFormat sdf = new SimpleDateFormat("mmss:SSS");
		System.out.println(sdf.format(new Date()));
		String minutesSecondMillSeconds = sdf.format(new Date());
		minutesSecondMillSeconds = minutesSecondMillSeconds.substring(0, minutesSecondMillSeconds.length() - 1);
		System.out.println(minutesSecondMillSeconds);
		System.out.println(Integer.toHexString(9).toUpperCase());
		System.out.println(new SimpleDateFormat("yy").format(new Date()));
		System.out.println(digtialMap.get("22"));

	}

	/**
	 * 返回统计会员消费代码
	 * 
	 * @return
	 */
	public static String getStatMemConsumNo() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		return sdf.format(date);
	}

	/**
	 * 获取 yyyyMMddHHmmss+商户ID（四位，不足左补齐0）
	 * 
	 * @param jsid
	 * @return
	 */
	public static String getPersonCheckInNo(Integer jsid, Integer checkid) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		String timeString = sdf.format(new Date());
		return "P" + StringUtils.leftPad(jsid + "", 4, "0") + timeString + StringUtils.leftPad(checkid + "", 8, "0");
	}

	/**
	 * 获取 yyyyMMddHHmmss+商户ID（四位，不足左补齐0）
	 * 
	 * @param jsid
	 * @return
	 */
	public static String getGroupCheckInNo(Integer jsid, Integer checkid) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		String timeString = sdf.format(new Date());
		return "G" + StringUtils.leftPad(jsid + "", 4, "0") + timeString + StringUtils.leftPad(checkid + "", 8, "0");
	}

	/**
	 * 获取订单编号 规则：会员编码5位，不足左边用0补齐+类型+年（以16进制式26个字母表示）+
	 * 月（以16进制式26个字母表示）+日（以16进制式26个字母表示）+时（以16进制式26个字母表示）+分秒微秒
	 * 
	 * @param memid
	 * @param type
	 *            C:代表产品 H ：代表客房   K:客房预定订单  I：表示充值 O：表示提现  M：代表消费卡
	 * @param date
	 * @return
	 */
	public static String getOrderNo(String memid, String type, Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("mmssSSS");
		String yearString = new SimpleDateFormat("yy").format(date);
		Integer year = Integer.parseInt(yearString);
		Integer month = (date.getMonth()) + 1;
		Integer day = (date.getDay());
		Integer hour = (date.getHours());

		String vYear = null;
		String vMonth = null;
		String vDay = null;
		String vHour = null;

		if (year < 10) {
			vYear = Integer.toHexString(year).toUpperCase();
		} else {
			vYear = digtialMap.get(String.valueOf(year));
		}

		if (month < 10) {
			vMonth = Integer.toHexString(month).toUpperCase();
		} else {
			vMonth = digtialMap.get(String.valueOf(month));
		}

		if (day < 10) {
			vDay = Integer.toHexString(day).toUpperCase();
		} else {
			vDay = digtialMap.get(String.valueOf(day));
		}

		if (hour < 10) {
			vHour = Integer.toHexString(hour).toUpperCase();
		} else {
			vHour = digtialMap.get(String.valueOf(hour));
		}
		String minutesSecondMillSeconds = sdf.format(date);
		minutesSecondMillSeconds = minutesSecondMillSeconds.substring(0, minutesSecondMillSeconds.length() - 1);

		String result = new StringBuffer().append(StringUtils.leftPad(memid + "", 5, "0")).append(type).append(vYear).append(vMonth).append(vDay).append(vHour).append(minutesSecondMillSeconds)
				.toString();

		return result;
	}

	/**
	 * 获取店铺编号 商户编号："JT_"+商户ID（四位，不足左补齐0）
	 * 
	 * @param jsid
	 * @return
	 */
	public static String getJSNo(String jsid) {
		return PREFIEX_TERMIN + StringUtils.leftPad(jsid + "", 4, "0");
	}

	/**
	 * 获取管理员编号 管理员编号 "AM_"+商户ID（四位，不足左补齐0）+员工ID（四位，不足左补齐0）
	 * 
	 * @param jsid
	 * @return
	 */
	public static String getAdminNo(String jsid) {
		int adminno = SeqUtils.getMaxNo("admin_login_user_id");
		return PREFIEX_ADMIN + StringUtils.leftPad(jsid + "", 4, "0") + StringUtils.leftPad(adminno + "", 4, "0");
	}

	/**
	 * 获取加盟蒂娜 员工编号 "ST_"+商户ID（四位，不足左补齐0）+员工ID（四位，不足左补齐0）
	 * 
	 * @param jsid
	 * @return
	 */
	public static String getJSStaffNo(String jsid) {
		int adminno = SeqUtils.getMaxNo("staff_no_id");
		return PREFIEX_JSSTAFF + StringUtils.leftPad(jsid + "", 4, "0") + StringUtils.leftPad(adminno + "", 4, "0");
	}

	/**
	 * 生成短信验证码
	 * 
	 * @return
	 */
	public static String getSmsCheckCode() {
		String charValue = "";
		for (int i = 0; i < 5; i++) {
			Random r = new Random();
			int a = r.nextInt(10);

			char c = (char) (a + '0');
			charValue += String.valueOf(c);
		}
		return charValue;
	}

	/**
	 * 会员编号
	 * 
	 * @param src
	 *            会员来源
	 * @return
	 */
	public static String getMemNo(String countCode, String jscode) {
		int memno = SeqUtils.getMaxNo("mem_no_" + jscode);
		return countCode + jscode + StringUtils.leftPad(memno + "", 4, "0");
	}

	/**
	 * 获取预订编号
	 * 
	 * @param src
	 *            会员来源
	 * @return
	 */
	public static String getReserveNo(String jsid) {
		String datestr = DateUtils.dateToString(new Date(), "yyyyMMddHHmmss");
		int seqno = SeqUtils.getMaxNo("ydrecord_no");
		return StringUtils.leftPad(jsid + "", 4, "0") + datestr + StringUtils.leftPad(seqno + "", 4, "0");
	}

	/**
	 * 获取预订编号
	 * 
	 * @param src
	 *            会员来源
	 * @return
	 */
	public static String getInCashOrderNo(Integer memid) {
		String datestr = DateUtils.dateToString(new Date(), "yyyyMMddHHmmss");
		int seqno = SeqUtils.getMaxNo("incash_order_no");
		return StringUtils.leftPad(memid + "", 4, "0") + datestr + StringUtils.leftPad(seqno + "", 4, "0");
	}

	/**
	 * 获取微信订单编号 规则
	 * 
	 * @param type
	 *            C:代表产品 H ：代表客房 I：表示充值 O：表示提现
	 * @param date
	 * @return
	 */
	public static String getWxOrderNo(String type) {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("mmssSSS");
		String yearString = new SimpleDateFormat("yy").format(date);
		Integer year = Integer.parseInt(yearString);
		Integer month = (date.getMonth()) + 1;
		Integer day = (date.getDay());
		Integer hour = (date.getHours());

		String vYear = null;
		String vMonth = null;
		String vDay = null;
		String vHour = null;

		if (year < 10) {
			vYear = Integer.toHexString(year).toUpperCase();
		} else {
			vYear = digtialMap.get(String.valueOf(year));
		}

		if (month < 10) {
			vMonth = Integer.toHexString(month).toUpperCase();
		} else {
			vMonth = digtialMap.get(String.valueOf(month));
		}

		if (day < 10) {
			vDay = Integer.toHexString(day).toUpperCase();
		} else {
			vDay = digtialMap.get(String.valueOf(day));
		}

		if (hour < 10) {
			vHour = Integer.toHexString(hour).toUpperCase();
		} else {
			vHour = digtialMap.get(String.valueOf(hour));
		}
		String minutesSecondMillSeconds = sdf.format(date);
		minutesSecondMillSeconds = minutesSecondMillSeconds.substring(0, minutesSecondMillSeconds.length() - 1);

		String result = new StringBuffer().append("I"+type).append(vYear).append(vMonth).append(vDay).append(vHour).append(minutesSecondMillSeconds).toString();

		return result;
	}
	
	/**
	 * 获取微信订单编号 规则
	 * 
	 * @param type
	 *            C:代表产品 H ：代表客房 I：表示充值 O：表示提现
	 * @param date
	 * @return
	 */
	public static String getWxRefundNo(String type) {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("mmssSSS");
		String yearString = new SimpleDateFormat("yy").format(date);
		Integer year = Integer.parseInt(yearString);
		Integer month = (date.getMonth()) + 1;
		Integer day = (date.getDay());
		Integer hour = (date.getHours());

		String vYear = null;
		String vMonth = null;
		String vDay = null;
		String vHour = null;

		if (year < 10) {
			vYear = Integer.toHexString(year).toUpperCase();
		} else {
			vYear = digtialMap.get(String.valueOf(year));
		}

		if (month < 10) {
			vMonth = Integer.toHexString(month).toUpperCase();
		} else {
			vMonth = digtialMap.get(String.valueOf(month));
		}

		if (day < 10) {
			vDay = Integer.toHexString(day).toUpperCase();
		} else {
			vDay = digtialMap.get(String.valueOf(day));
		}

		if (hour < 10) {
			vHour = Integer.toHexString(hour).toUpperCase();
		} else {
			vHour = digtialMap.get(String.valueOf(hour));
		}
		String minutesSecondMillSeconds = sdf.format(date);
		minutesSecondMillSeconds = minutesSecondMillSeconds.substring(0, minutesSecondMillSeconds.length() - 1);

		String result = new StringBuffer().append("O"+type).append(vYear).append(vMonth).append(vDay).append(vHour).append(minutesSecondMillSeconds).toString();

		return result;
	}
	
	/**
	 * 直销会员编号
	 * 
	 * @param src
	 *            会员来源
	 * @return
	 */
	public static String getMemNo() {
		int memno = SeqUtils.getMaxNo("mem_no");
		return "zm"+StringUtils.leftPad(memno + "", 8, "0");
	}

}
