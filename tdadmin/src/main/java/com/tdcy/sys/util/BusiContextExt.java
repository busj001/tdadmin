package com.tdcy.sys.util;

import java.util.Map;

import com.tdcy.framework.BusiContext;
import com.tdcy.sys.constant.AppConstants;
import com.tdcy.sys.dao.eo.StaffEO;
import com.tdcy.sys.dao.eo.UserEO;
import com.tdcy.sys.service.bean.LoginInfoBean;

public class BusiContextExt extends BusiContext {
	public static LoginInfoBean getLoginInfo() {
		Object obj = getContext(AppConstants.BUSI_LOGIN_INFO);
		if (obj != null) {
			return (LoginInfoBean) obj;
		} else {
			return null;
		}
	}

	public static String getLoginUser() {
		return getLoginInfo().getUserEO().getLoginUser();
	}

	public static UserEO getUserEO() {
		return getLoginInfo().getUserEO();
	}

	public static Map<String, Object> getLoginDetail() {
		return (Map<String, Object>) getLoginInfo().getLoginDetail();
	}

	public static StaffEO getStaffInfo() {
		return (StaffEO) getContext(AppConstants.BUSI_LOGIN_EO);
	}

}
