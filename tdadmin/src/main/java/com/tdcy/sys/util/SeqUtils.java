package com.tdcy.sys.util;

import com.tdcy.framework.util.ContextUtils;
import com.tdcy.sys.service.ISeqNoSvc;

public class SeqUtils {
	public static int getMaxNo(String MaxNo_Name) {
		ISeqNoSvc svc = ContextUtils.getApplicationContext().getBean(
				ISeqNoSvc.class);
		return svc.getMaxNo(MaxNo_Name);
	}

	public static int getMaxNo(String MaxNo_Name, int MaxNo_Size) {
		ISeqNoSvc svc = ContextUtils.getApplicationContext().getBean(
				ISeqNoSvc.class);
		return svc.getMaxNo(MaxNo_Name, MaxNo_Size);
	}
}
