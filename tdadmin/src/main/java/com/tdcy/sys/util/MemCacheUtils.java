package com.tdcy.sys.util;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.rubyeye.xmemcached.KeyIterator;
import net.rubyeye.xmemcached.MemcachedClient;

import org.springframework.cache.support.SimpleCacheManager;

import com.google.code.ssm.Cache;
import com.tdcy.framework.exception.BaseException;
import com.tdcy.framework.util.ContextUtils;
import com.tdcy.framework.util.StringUtils;

/**
 * 
 * @ClassName: MemcachedUtils
 * @Description: Memcached工具类
 * @author yinjw
 * @date 2014-6-18 下午5:28:08
 * 
 */
public class MemCacheUtils {
	public static SimpleCacheManager cacheManager = (SimpleCacheManager) ContextUtils.getApplicationContext().getBean("cacheManager");

	public static MemcachedClient getClient(String ckey) {
		Cache cf = (Cache) ContextUtils.getApplicationContext().getBean(ckey);
		MemcachedClient mc = (MemcachedClient) cf.getNativeClient();
		return mc;
	}

	public static List<Map<String, String>> getCacheList(String ckey) {
		List<Map<String, String>> retList = new ArrayList<Map<String, String>>();
		Collection<String> cs = cacheManager.getCacheNames();
		for (Iterator<String> it = cs.iterator(); it.hasNext();) {
			String n = it.next();
			MemcachedClient mc = getClient(n);
			List<Map<String, String>> t = getCacheList(mc, n, ckey);
			if (t.size() > 0) {
				retList.addAll(t);
			}

		}
		return retList;
	}

	public static List<Map<String, String>> getCacheList(MemcachedClient mc, String cacheName, String ckey) {
		List<Map<String, String>> retList = new ArrayList<Map<String, String>>();
		try {
			Collection<InetSocketAddress> addrss = mc.getAvailableServers();
			for (InetSocketAddress isa : addrss) {
				KeyIterator it;
				it = mc.getKeyIterator(isa);

				while (it.hasNext()) {
					Map<String, String> km = new HashMap<String, String>();
					String key = it.next();
					if (StringUtils.isEmpty(ckey)) {
						km.put("cname", cacheName);
						km.put("ckey", key);
						retList.add(km);
					} else {
						if (key.equals(ckey)) {
							km.put("cname", cacheName);
							km.put("ckey", key);
							retList.add(km);
						}
					}

				}
			}
		} catch (Exception  e) {
			e.printStackTrace();
			throw new BaseException("获取失败");
		}
		return retList;
	}

	public static List<Map<String, String>> getCache(String cacheKey) {
		List<Map<String, String>> retList = new ArrayList<Map<String, String>>();
		try {
			Collection<String> cs = cacheManager.getCacheNames();
			for (Iterator<String> it = cs.iterator(); it.hasNext();) {
				String n = it.next();
				MemcachedClient mc = getClient(n);
				mc.get(cacheKey);
			}
		} catch (Exception  e) {
			e.printStackTrace();
			throw new BaseException("获取失败");
		}
		return retList;
	}

	public static void removeCache(String cacheName, String cacheKey) {
		MemcachedClient mc = getClient(cacheName);
		try {
			mc.delete(cacheKey);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BaseException("清除失败");
		}
	}
}