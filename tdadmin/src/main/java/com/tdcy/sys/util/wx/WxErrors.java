package com.tdcy.sys.util.wx;

import java.util.HashMap;
import java.util.Map;

public class WxErrors {
	public static Map<String, String> emap = new HashMap<String, String>();
	static {
		emap.put("NOAUTH", "商户无此接口权限");
		emap.put("NOTENOUGH", "余额不足");
		emap.put("ORDERPAID	", "商户订单已支付");
		emap.put("ORDERCLOSED", "订单已关闭");
		emap.put("SYSTEMERROR", "系统错误");
		emap.put("APPID_NOT_EXIST", "APPID不存在");
		emap.put("MCHID_NOT_EXIST", "MCHID不存在");
		emap.put("APPID_MCHID_NOT_MATCH", "appid和mch_id不匹配");
		emap.put("LACK_PARAMS", "缺少参数");
		emap.put("OUT_TRADE_NO_USED", "商户订单号重复");
		emap.put("SIGNERROR", "签名错误");
		emap.put("XML_FORMAT_ERROR", "XML格式错误");
		emap.put("REQUIRE_POST_METHOD", "请使用post方法");
		emap.put("POST_DATA_EMPTY", "post数据为空");
		emap.put("NOT_UTF8", "编码格式错误");
	}

	public static String getErrorMsg(String ecode) {
		return emap.get(ecode);
	}
}
