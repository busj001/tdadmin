package com.tdcy.sys.util.shiro;

import java.io.Serializable;

import com.tdcy.sys.service.bean.LoginInfoBean;

/**
 * 授权用户信息
 */
public class ShiroUser implements Serializable {
	private LoginInfoBean loginInfo;
	private Integer id; // 编号
	private String username; // 登录名
	private String realname; // 姓名

	public ShiroUser(LoginInfoBean user) {
		this.id = user.getUserEO().getUserId();
		this.username = user.getUserEO().getLoginUser();
		this.realname = user.getUserEO().getUserName();
		this.loginInfo = user;
	}

	public LoginInfoBean getLoginInfo() {
		return loginInfo;
	}

	public void setLoginInfo(LoginInfoBean loginInfo) {
		this.loginInfo = loginInfo;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setRealname(String realname) {
		this.realname = realname;
	}

	public String getUsername() {
		return username;
	}

	public String getRealname() {
		return realname;
	}

	/**
	 * 获取SESSIONID
	 */
	public String getSessionid() {
		try {
			return (String) ShiroUtils.getSession().getId();
		} catch (Exception e) {
			return "";
		}
	}
}