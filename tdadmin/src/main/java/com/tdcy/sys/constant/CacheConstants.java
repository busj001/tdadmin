package com.tdcy.sys.constant;

public class CacheConstants {

	public final static String CACHENAME_APP = "appCache";
	public final static String CACHEKEY_CODETABLE = "cachekey_codetable";
	public final static String CACHEKEY_PARAM = "cachekey_param";
	public final static String CACHEKEY_ALLMENU = "cachekey_allmenu";
	public final static String CACHEKEY_ROLEMENU = "cachekey_rolemenu";
	
	public final static String MEM_FILE_PREFIX ="filefile_";
	public final static String MEM_IMAGE_PREFIX ="fileimage_";
}
