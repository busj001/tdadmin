package com.tdcy.sys.constant;

public class CodeTypeConstants {
	public static final String CERT_TYPE = "cert_type";
	public static final String MENU_TYPE = "menu_type";
	public static final String NATION = "nation";
	public static final String SEX = "sex";
	public static final String STAFF_STA = "staff_sta";
	public static final String VALID_FLAG = "valid_flag";
}
