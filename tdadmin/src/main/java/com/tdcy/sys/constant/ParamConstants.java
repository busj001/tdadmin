package com.tdcy.sys.constant;

public class ParamConstants {
	public static final String APPLICATION_NAME = "application_name";
	public static final String APPLICATION_VERSION = "application_version";
	public static final String CHECKCODE_FLAG = "checkcode_flag";
	public static final String COMPANY_NAME = "company_name";
	public static final String MAIL_ADDRESS = "mail_address";
	public static final String MAIL_DEBUG = "mail_debug";
	public static final String MAIL_USERNAME = "mail_username";
	public static final String MAILSUPPORT = "mailsupport";
	public static final String SMS_ACCOUNTSID = "sms_accountSid";
	public static final String SMS_APPID = "sms_appId";
	public static final String SMS_AUTHTOKEN = "sms_authToken";
	public static final String SMS_CHECKCODE_TEMPLATEID = "sms_checkcode_templateId";
	public static final String SMS_CHECKCODE_TIMEOUT = "sms_checkcode_timeout";
	public static final String SMS_HTTP_SSL_IP = "sms_http_ssl_ip";
	public static final String SMS_HTTP_SSL_PORT = "sms_http_ssl_port";
	public static final String SMS_SERVER = "sms_server";
	public static final String SMS_VERSION = "sms_version";
	public static final String WX_API_SECRETKEY = "wx_api_secretkey";
	public static final String WX_APPID = "wx_appid";
	public static final String WX_MCH_ID = "wx_mch_id";

}
