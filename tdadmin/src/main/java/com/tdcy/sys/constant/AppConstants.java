package com.tdcy.sys.constant;

public class AppConstants {
	public final static String USER_KIND = "USER_KIND";
	
	/**保存在session中的常量*/
	public final static String SESSION_LOGIN_INFO = "LOGIN_INFO_SESSION";
	public final static String SESSION_LOGIN_EO = "LOGIN_EO_SESSION";
	public final static String SESSION_MENUS = "SESSION_MENUS";
	public final static String SESSION_ROLES = "SESSION_ROLES";

	/** 保存在当前访问线程中的常量*/
	public static final String BUSI_LOGIN_INFO = "BUSI_LOGININFO_BEAN";
	public static final String BUSI_LOGIN_EO = "BUSI_LOGINEO";
	public static final String BUSI_REQUESTURI = "BUSI_REQUESTURI";
	public static final String BUSI_REQUESTURL = "BUSI_REQUESTURL";
	public static final String BUSI_PATH = "BUSI_PATH";
	public static final String BUSI_BASEPATH = "BUSI_BASEPATH";
	
	public final static String SESSION_PRINT_JASPERPRINT = "SESSION_PRINT_JASPERPRINT";
}
