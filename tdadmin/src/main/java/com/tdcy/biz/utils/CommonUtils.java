package com.tdcy.biz.utils;

import java.util.List;

import com.tdcy.framework.util.ContextUtils;
import com.tdcy.framework.util.JsonUtils;
import com.tdcy.sys.dao.IUserKindDAO;
import com.tdcy.sys.dao.eo.DeptEO;
import com.tdcy.sys.dao.eo.RolesEO;
import com.tdcy.sys.dao.eo.UserKindEO;
import com.tdcy.sys.service.IDeptSvc;
import com.tdcy.sys.service.IRolesSvc;

public class CommonUtils {
	public static String getUserKindJson() {
		IUserKindDAO svc = ContextUtils.getApplicationContext().getBean(IUserKindDAO.class);
		List<UserKindEO> eos = svc.findTableRecords(UserKindEO.class);
		return JsonUtils.toJson(eos);
	}

	public static String getDeptsJson() {
		IDeptSvc svc = ContextUtils.getApplicationContext().getBean(IDeptSvc.class);
		List<DeptEO> eos = svc.getAllDept();
		return JsonUtils.toJson(eos);
	}

	public static String getRolesJson() {
		IRolesSvc svc = ContextUtils.getApplicationContext().getBean(IRolesSvc.class);
		List<RolesEO> eos = svc.getAllRoles();
		return JsonUtils.toJson(eos);
	}
}
