package com.tdcy.biz.utils;

import java.util.ArrayList;
import java.util.List;

import com.tdcy.framework.util.StringUtils;

public class ImpErrorInfoObj {
	private String code;
	private List<String> messageList = new ArrayList<String>();
	private String result;

	public void setResult(String result) {
		this.result = result;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void addMessage(String message) {
		this.messageList.add(message);
	}

	public void setMessageList(List<String> messageList) {
		this.messageList = messageList;
	}

	public String getCode() {
		return code;
	}

	public List<String> getMessageList() {
		return messageList;
	}

	public String getMsgStr() {
		return StringUtils.join(messageList, "||");
	}

	public String getResult() {
		return result;
	}

}
