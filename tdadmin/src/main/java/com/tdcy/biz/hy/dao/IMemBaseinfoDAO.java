package com.tdcy.biz.hy.dao;

import java.util.List;

import com.tdcy.biz.hy.dao.eo.MemBaseinfoEO;
import com.tdcy.biz.hy.service.bean.MemBaseInfoQueryCondition;
import com.tdcy.framework.bean.PageInfo;
import com.tdcy.framework.dao.IDao;
/**
* @Description 会员基本信息DAO类
*/
public interface IMemBaseinfoDAO extends IDao {
	public PageInfo<MemBaseinfoEO> getPageMemBaseinfo(MemBaseInfoQueryCondition qc);
	
	public List<MemBaseinfoEO> getListMemBaseinfo(MemBaseInfoQueryCondition qc);
}
