package com.tdcy.biz.hy.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.tdcy.biz.hy.dao.IMemBaseinfoDAO;
import com.tdcy.biz.hy.dao.eo.MemBaseinfoEO;
import com.tdcy.biz.hy.service.IMemBaseinfoSvc;
import com.tdcy.sys.service.ISeqNoSvc;

/**
* @Description 会员基本信息Service实现类
*/
@Service
public class MemBaseinfoSvcImpl implements IMemBaseinfoSvc {

	@Resource
	IMemBaseinfoDAO membaseinfoDAO;

	@Resource
	ISeqNoSvc seqSvc;

	public List<MemBaseinfoEO> getAllMemBaseinfo() {
		return membaseinfoDAO.findTableRecords(MemBaseinfoEO.class);
	}
	
	@Override
	public MemBaseinfoEO getMemBaseinfoInfo(Integer membaseinfoId) {
		return membaseinfoDAO.findByPrimaryKey(MemBaseinfoEO.class, membaseinfoId);
	}

	@Override
	public MemBaseinfoEO updateMemBaseinfo(MemBaseinfoEO membaseinfo) {
		MemBaseinfoEO membaseinfoeo=membaseinfoDAO.update(membaseinfo);
		return membaseinfoeo;
	}

	@Override
	public MemBaseinfoEO addMemBaseinfo(MemBaseinfoEO membaseinfo) {
		membaseinfo.setMemid(seqSvc.getMaxNo("memid"));
		MemBaseinfoEO membaseinfoeo=membaseinfoDAO.save(membaseinfo);
		return membaseinfoeo;
	}

	@Override
	public boolean delMemBaseinfo(Integer membaseinfoid) {
		return membaseinfoDAO.deleteByPrimaryKey(MemBaseinfoEO.class, membaseinfoid);
	}
}
