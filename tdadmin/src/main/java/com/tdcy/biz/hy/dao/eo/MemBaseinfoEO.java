package com.tdcy.biz.hy.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;
/**
* @Description 会员基本信息EO类
*/
@Table(name = "hy_mem_baseinfo")
public class MemBaseinfoEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 会员编码 
	*/
	@Id
	@Column(name = "memid")
	private Integer memid;
	/**
	* 会员唯一码
	*/
	@Column(name = "mem_no")
	private String memNo;
	/**
	* 会员姓名
	*/
	@Column(name = "mem_name")
	private String memName;
	/**
	* 会员拼音码
	*/
	@Column(name = "mem_pinyin")
	private String memPinyin;
	/**
	* 会员级别
	*/
	@Column(name = "mem_level")
	private Integer memLevel;
	/**
	* 
	*/
	@Column(name = "mem_quarity")
	private Integer memQuarity;
	/**
	* 会员性别  1: 男  2： 女  0：未知
	*/
	@Column(name = "sex")
	private String sex;
	/**
	* 
	*/
	@Column(name = "active_way")
	private Integer activeWay;
	/**
	* 证件类型
	*/
	@Column(name = "mem_ver_type")
	private Integer memVerType;
	/**
	* 证件号
	*/
	@Column(name = "mem_ver_no")
	private String memVerNo;
	/**
	* 会员所在省
	*/
	@Column(name = "mem_prov")
	private String memProv;
	/**
	* 会员所在市
	*/
	@Column(name = "mem_city")
	private String memCity;
	/**
	* 所在行政区划
	*/
	@Column(name = "mem_area")
	private String memArea;
	/**
	* 地址
	*/
	@Column(name = "mem_address")
	private String memAddress;
	/**
	* 会员邮箱
	*/
	@Column(name = "mem_email")
	private String memEmail;
	/**
	* 电话
	*/
	@Column(name = "mem_phone")
	private String memPhone;
	/**
	* 会员卡状态
	*/
	@Column(name = "mem_card_status")
	private Integer memCardStatus;
	/**
	* 会员状态
	*/
	@Column(name = "mem_status")
	private Integer memStatus;
	/**
	* 是否通过购买卡方式注册
	*/
	@Column(name = "mem_addtype")
	private Integer memAddtype;
	/**
	* 备注
	*/
	@Column(name = "remark")
	private String remark;
	/**
	* 创建时间
	*/
	@Column(name = "create_time")
	private java.util.Date createTime;
	/**
	* 最新更新时间
	*/
	@Column(name = "udpate_time")
	private java.util.Date udpateTime;
	/**
	* 有效标志（1：有效  0：无效）
	*/
	@Column(name = "valid_flag")
	private Integer validFlag;
	/**
	* 
	*/
	@Column(name = "bank_code")
	private String bankCode;
	/**
	* 记录创建人
	*/
	@Column(name = "createor")
	private Integer createor;
	/**
	* 
	*/
	@Column(name = "bank_num")
	private String bankNum;
	/**
	* 
	*/
	@Column(name = "bank_openuser_name")
	private String bankOpenuserName;
	/**
	* 
	*/
	@Column(name = "bank_open_address")
	private String bankOpenAddress;
	/**
	* 
	*/
	@Column(name = "nickname")
	private String nickname;
	/**
	* 
	*/
	@Column(name = "tags")
	private String tags;
	/**
	* 
	*/
	@Column(name = "avatarpic_id")
	private Integer avatarpicId;
	/**
	* 详细地址
	*/
	@Column(name = "md_detail")
	private String mdDetail;
	/**
	* 预申请id
	*/
	@Column(name = "memapplyid")
	private Integer memapplyid;
	/**
	* 
	*/
	@Column(name = "init_flag")
	private Integer initFlag;
	/**
	* 
	*/
	@Column(name = "idcard_photoid")
	private String idcardPhotoid;
	/**
	* 获取 会员编码 
	*/ 
	public Integer getMemid() {
		return memid;
	}
	/**
	* 设置会员编码 
	*/
	public void setMemid(Integer memid) {
		this.memid = memid;
	}
	/**
	* 获取 会员唯一码
	*/ 
	public String getMemNo() {
		return memNo;
	}
	/**
	* 设置会员唯一码
	*/
	public void setMemNo(String memNo) {
		this.memNo = memNo;
	}
	/**
	* 获取 会员姓名
	*/ 
	public String getMemName() {
		return memName;
	}
	/**
	* 设置会员姓名
	*/
	public void setMemName(String memName) {
		this.memName = memName;
	}
	/**
	* 获取 会员拼音码
	*/ 
	public String getMemPinyin() {
		return memPinyin;
	}
	/**
	* 设置会员拼音码
	*/
	public void setMemPinyin(String memPinyin) {
		this.memPinyin = memPinyin;
	}
	/**
	* 获取 会员级别
	*/ 
	public Integer getMemLevel() {
		return memLevel;
	}
	/**
	* 设置会员级别
	*/
	public void setMemLevel(Integer memLevel) {
		this.memLevel = memLevel;
	}
	/**
	* 获取 
	*/ 
	public Integer getMemQuarity() {
		return memQuarity;
	}
	/**
	* 设置
	*/
	public void setMemQuarity(Integer memQuarity) {
		this.memQuarity = memQuarity;
	}
	/**
	* 获取 会员性别  1: 男  2： 女  0：未知
	*/ 
	public String getSex() {
		return sex;
	}
	/**
	* 设置会员性别  1: 男  2： 女  0：未知
	*/
	public void setSex(String sex) {
		this.sex = sex;
	}
	/**
	* 获取 
	*/ 
	public Integer getActiveWay() {
		return activeWay;
	}
	/**
	* 设置
	*/
	public void setActiveWay(Integer activeWay) {
		this.activeWay = activeWay;
	}
	/**
	* 获取 证件类型
	*/ 
	public Integer getMemVerType() {
		return memVerType;
	}
	/**
	* 设置证件类型
	*/
	public void setMemVerType(Integer memVerType) {
		this.memVerType = memVerType;
	}
	/**
	* 获取 证件号
	*/ 
	public String getMemVerNo() {
		return memVerNo;
	}
	/**
	* 设置证件号
	*/
	public void setMemVerNo(String memVerNo) {
		this.memVerNo = memVerNo;
	}
	/**
	* 获取 会员所在省
	*/ 
	public String getMemProv() {
		return memProv;
	}
	/**
	* 设置会员所在省
	*/
	public void setMemProv(String memProv) {
		this.memProv = memProv;
	}
	/**
	* 获取 会员所在市
	*/ 
	public String getMemCity() {
		return memCity;
	}
	/**
	* 设置会员所在市
	*/
	public void setMemCity(String memCity) {
		this.memCity = memCity;
	}
	/**
	* 获取 所在行政区划
	*/ 
	public String getMemArea() {
		return memArea;
	}
	/**
	* 设置所在行政区划
	*/
	public void setMemArea(String memArea) {
		this.memArea = memArea;
	}
	/**
	* 获取 地址
	*/ 
	public String getMemAddress() {
		return memAddress;
	}
	/**
	* 设置地址
	*/
	public void setMemAddress(String memAddress) {
		this.memAddress = memAddress;
	}
	/**
	* 获取 会员邮箱
	*/ 
	public String getMemEmail() {
		return memEmail;
	}
	/**
	* 设置会员邮箱
	*/
	public void setMemEmail(String memEmail) {
		this.memEmail = memEmail;
	}
	/**
	* 获取 电话
	*/ 
	public String getMemPhone() {
		return memPhone;
	}
	/**
	* 设置电话
	*/
	public void setMemPhone(String memPhone) {
		this.memPhone = memPhone;
	}
	/**
	* 获取 会员卡状态
	*/ 
	public Integer getMemCardStatus() {
		return memCardStatus;
	}
	/**
	* 设置会员卡状态
	*/
	public void setMemCardStatus(Integer memCardStatus) {
		this.memCardStatus = memCardStatus;
	}
	/**
	* 获取 会员状态
	*/ 
	public Integer getMemStatus() {
		return memStatus;
	}
	/**
	* 设置会员状态
	*/
	public void setMemStatus(Integer memStatus) {
		this.memStatus = memStatus;
	}
	/**
	* 获取 是否通过购买卡方式注册
	*/ 
	public Integer getMemAddtype() {
		return memAddtype;
	}
	/**
	* 设置是否通过购买卡方式注册
	*/
	public void setMemAddtype(Integer memAddtype) {
		this.memAddtype = memAddtype;
	}
	/**
	* 获取 备注
	*/ 
	public String getRemark() {
		return remark;
	}
	/**
	* 设置备注
	*/
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	* 获取 创建时间
	*/ 
	public java.util.Date getCreateTime() {
		return createTime;
	}
	/**
	* 设置创建时间
	*/
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	/**
	* 获取 最新更新时间
	*/ 
	public java.util.Date getUdpateTime() {
		return udpateTime;
	}
	/**
	* 设置最新更新时间
	*/
	public void setUdpateTime(java.util.Date udpateTime) {
		this.udpateTime = udpateTime;
	}
	/**
	* 获取 有效标志（1：有效  0：无效）
	*/ 
	public Integer getValidFlag() {
		return validFlag;
	}
	/**
	* 设置有效标志（1：有效  0：无效）
	*/
	public void setValidFlag(Integer validFlag) {
		this.validFlag = validFlag;
	}
	/**
	* 获取 
	*/ 
	public String getBankCode() {
		return bankCode;
	}
	/**
	* 设置
	*/
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	/**
	* 获取 记录创建人
	*/ 
	public Integer getCreateor() {
		return createor;
	}
	/**
	* 设置记录创建人
	*/
	public void setCreateor(Integer createor) {
		this.createor = createor;
	}
	/**
	* 获取 
	*/ 
	public String getBankNum() {
		return bankNum;
	}
	/**
	* 设置
	*/
	public void setBankNum(String bankNum) {
		this.bankNum = bankNum;
	}
	/**
	* 获取 
	*/ 
	public String getBankOpenuserName() {
		return bankOpenuserName;
	}
	/**
	* 设置
	*/
	public void setBankOpenuserName(String bankOpenuserName) {
		this.bankOpenuserName = bankOpenuserName;
	}
	/**
	* 获取 
	*/ 
	public String getBankOpenAddress() {
		return bankOpenAddress;
	}
	/**
	* 设置
	*/
	public void setBankOpenAddress(String bankOpenAddress) {
		this.bankOpenAddress = bankOpenAddress;
	}
	/**
	* 获取 
	*/ 
	public String getNickname() {
		return nickname;
	}
	/**
	* 设置
	*/
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	/**
	* 获取 
	*/ 
	public String getTags() {
		return tags;
	}
	/**
	* 设置
	*/
	public void setTags(String tags) {
		this.tags = tags;
	}
	/**
	* 获取 
	*/ 
	public Integer getAvatarpicId() {
		return avatarpicId;
	}
	/**
	* 设置
	*/
	public void setAvatarpicId(Integer avatarpicId) {
		this.avatarpicId = avatarpicId;
	}
	/**
	* 获取 详细地址
	*/ 
	public String getMdDetail() {
		return mdDetail;
	}
	/**
	* 设置详细地址
	*/
	public void setMdDetail(String mdDetail) {
		this.mdDetail = mdDetail;
	}
	/**
	* 获取 预申请id
	*/ 
	public Integer getMemapplyid() {
		return memapplyid;
	}
	/**
	* 设置预申请id
	*/
	public void setMemapplyid(Integer memapplyid) {
		this.memapplyid = memapplyid;
	}
	/**
	* 获取 
	*/ 
	public Integer getInitFlag() {
		return initFlag;
	}
	/**
	* 设置
	*/
	public void setInitFlag(Integer initFlag) {
		this.initFlag = initFlag;
	}
	/**
	* 获取 
	*/ 
	public String getIdcardPhotoid() {
		return idcardPhotoid;
	}
	/**
	* 设置
	*/
	public void setIdcardPhotoid(String idcardPhotoid) {
		this.idcardPhotoid = idcardPhotoid;
	}
}

