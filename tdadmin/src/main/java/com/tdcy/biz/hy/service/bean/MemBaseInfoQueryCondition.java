package com.tdcy.biz.hy.service.bean;

import com.tdcy.sys.service.bean.QueryCondition;

public class MemBaseInfoQueryCondition extends QueryCondition {
	private String memStatus;
	private String memNo;
	private String memPhone;
	public String getMemStatus() {
		return memStatus;
	}
	public void setMemStatus(String memStatus) {
		this.memStatus = memStatus;
	}
	public String getMemNo() {
		return memNo;
	}
	public void setMemNo(String memNo) {
		this.memNo = memNo;
	}
	public String getMemPhone() {
		return memPhone;
	}
	public void setMemPhone(String memPhone) {
		this.memPhone = memPhone;
	}

}
