package com.tdcy.biz.hy.service.validator;

import com.tdcy.biz.hy.dao.eo.MemBaseinfoEO;
import com.tdcy.framework.exception.QuickuException;
import com.tdcy.framework.util.StringUtils;

public class MemInfoValidator {
	public void validate(MemBaseinfoEO infoco) {

		if (StringUtils.isEmpty(infoco.getMemPhone())) {
			throw new QuickuException("电话号码不能为空");
		}
	}

}
