package com.tdcy.biz.hy.service;

import java.util.List;

import com.tdcy.biz.hy.dao.eo.MemBaseinfoEO;

/**
* @Description 会员基本信息Service类
*/
public interface IMemBaseinfoSvc{
	public List<MemBaseinfoEO> getAllMemBaseinfo();

	public MemBaseinfoEO getMemBaseinfoInfo(Integer memid);

	public MemBaseinfoEO updateMemBaseinfo(MemBaseinfoEO membaseinfo);

	public MemBaseinfoEO addMemBaseinfo(MemBaseinfoEO membaseinfo);

	public boolean delMemBaseinfo(Integer memid);
}
