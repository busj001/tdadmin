package com.tdcy.biz.hy.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.tdcy.biz.hy.dao.IMemBaseinfoDAO;
import com.tdcy.biz.hy.dao.eo.MemBaseinfoEO;
import com.tdcy.biz.hy.service.bean.MemBaseInfoQueryCondition;
import com.tdcy.framework.bean.PageInfo;
import com.tdcy.framework.dao.DaoImpl;
/**
* @Description 会员基本信息DAO实现类
*/
@Service
public class MemBaseinfoDAOImpl extends DaoImpl implements IMemBaseinfoDAO {

	public PageInfo<MemBaseinfoEO> getPageMemBaseinfo(MemBaseInfoQueryCondition qc){
		StringBuffer sql = new StringBuffer();
		ArrayList<Object> p = new ArrayList<Object>();
		sql.append("select  t.*\n");
		sql.append("from hy_mem_baseinfo t where t.valid_flag =1 ");
		
		
		return this.queryRecordByClassForPageInfo(sql.toString(), MemBaseinfoEO.class, qc.getPageInfo(), p.toArray());
	}
	
	public List<MemBaseinfoEO> getListMemBaseinfo(MemBaseInfoQueryCondition qc){
		StringBuffer sql = new StringBuffer();
		ArrayList<Object> p = new ArrayList<Object>();
		sql.append("select  t.*\n");
		sql.append("from hy_mem_baseinfo t where t.valid_flag =1 ");
		
		
		return this.execSqlQuery(MemBaseinfoEO.class, sql.toString(), p.toArray());
	}

}
