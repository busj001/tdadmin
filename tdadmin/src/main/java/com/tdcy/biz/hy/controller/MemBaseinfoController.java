package com.tdcy.biz.hy.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tdcy.biz.hy.dao.IMemBaseinfoDAO;
import com.tdcy.biz.hy.dao.eo.MemBaseinfoEO;
import com.tdcy.biz.hy.service.IMemBaseinfoSvc;
import com.tdcy.biz.hy.service.bean.MemBaseInfoQueryCondition;
import com.tdcy.framework.BaseController;
import com.tdcy.framework.bean.PageInfo;
import com.tdcy.framework.util.WebUtils;
/**
* @Description 会员基本信息Controller
*/
@Controller
@RequestMapping(value = "/hy")
public class MemBaseinfoController extends BaseController {
	@Resource
	IMemBaseinfoSvc membaseinfoSvc;
	
	@Resource
	IMemBaseinfoDAO membaseinfoDAO;
	
	@ResponseBody
	@RequestMapping(value = "/getAllMemBaseinfo", method =RequestMethod.POST)
	public Object getallMemBaseinfo() throws IOException {
		List<MemBaseinfoEO> eolist = membaseinfoSvc.getAllMemBaseinfo();
		return WebUtils.createJSONSuccess("数据加载成功", eolist);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getMemBaseinfoPage", method =RequestMethod.POST)
	public Object getpageMemBaseinfo(MemBaseInfoQueryCondition qc) throws IOException {
		PageInfo<MemBaseinfoEO> page = membaseinfoDAO.getPageMemBaseinfo(qc);
		return WebUtils.createJSONSuccess("数据加载成功", page);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getMemBaseinfoList", method =RequestMethod.POST)
	public Object getlistMemBaseinfo(MemBaseInfoQueryCondition qc) throws IOException {
		List<MemBaseinfoEO> eolist = membaseinfoDAO.getListMemBaseinfo(qc);
		return WebUtils.createJSONSuccess("数据加载成功", eolist);
	}
	

	@ResponseBody
	@RequestMapping(value = "/addMemBaseinfo", method =RequestMethod.POST)
	public Object addMemBaseinfo(MemBaseinfoEO membaseinfo) throws IOException {
		MemBaseinfoEO eo = membaseinfoSvc.addMemBaseinfo(membaseinfo);
		return WebUtils.createJSONSuccess("添加成功", eo);
	}

	@ResponseBody
	@RequestMapping(value = "/updateMemBaseinfo", method =RequestMethod.POST)
	public Object updateMemBaseinfo(MemBaseinfoEO membaseinfo) throws IOException {
		MemBaseinfoEO eo = membaseinfoSvc.updateMemBaseinfo(membaseinfo);
		return WebUtils.createJSONSuccess("更新成功", eo);
	}

	@ResponseBody
	@RequestMapping(value = "/delMemBaseinfo", method =RequestMethod.POST)
	public Object delMemBaseinfo(@RequestParam("memid") Integer memid) throws IOException {
		boolean flag = membaseinfoSvc.delMemBaseinfo(memid);
		return WebUtils.createJSONSuccess("删除成功", flag);
	}

	@ResponseBody
	@RequestMapping(value = "/getMemBaseinfoInfo", method =RequestMethod.POST)
	public Object getmembaseinfoinfo(@RequestParam("memid")Integer memid) throws IOException {
		MemBaseinfoEO eo = membaseinfoSvc.getMemBaseinfoInfo(memid);
		return WebUtils.createJSONSuccess("获取成功", eo);
	}

}
