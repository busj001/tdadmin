package com.tdcy.framework.mail;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;

import com.tdcy.framework.mail.test.SendMailTask;

public class TestSendManyMail {
	public static void main(String[] args) throws MalformedURLException, FileNotFoundException {
		for (int i = 0; i < 15; i++) {
			SendMailTask s = new SendMailTask();
			Thread t = new Thread(s);
			t.start();
		}
	}
}
