package com.tdcy.framework.mail.entity;

public class MailAttachFile {
	// 上传的文件名
	private String name;
	
	private String contentType;
	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public MailAttachFile(String name) {
		this.name = name;
	}
}
