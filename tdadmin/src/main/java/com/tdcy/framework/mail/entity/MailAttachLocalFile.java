package com.tdcy.framework.mail.entity;

public class MailAttachLocalFile extends MailAttachFile {
	private String filePath;

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public MailAttachLocalFile(String filePath, String name) {
		super(name);
		this.filePath = filePath;
	}
}
