package com.tdcy.framework.mail;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URL;

import com.tdcy.framework.mail.entity.MailAttachLocalFile;
import com.tdcy.framework.mail.entity.MailAttachURL;
import com.tdcy.framework.mail.entity.MailSenderInfo;

public class Test {
	public static void main(String[] args) throws MalformedURLException, FileNotFoundException {
		// 这个类主要是设置邮件
		// MailSenderInfo mailInfo = new MailSenderInfo();
		// mailInfo.setMailServerHost("smtp.163.com");
		// mailInfo.setMailServerPort("25");
		// mailInfo.setValidate(true);
		// mailInfo.setUserName("cheny523730098@163.com");
		// mailInfo.setPassword("ai19871129");// 您的邮箱密码
		// mailInfo.setFromAddress("cheny523730098@163.com");
		// mailInfo.setToAddress("523730098@qq.com");
		// mailInfo.setSubject("设置邮箱标题");
		// mailInfo.setContent("设置邮箱内容");
		// mailInfo.addAttachFile("c:\\测试.txt");
		// // 这个类主要来发送邮件
		// SimpleMailSender sms = new SimpleMailSender();
		// sms.sendMail(mailInfo);

		MailSenderInfo mailInfo = new MailSenderInfo();
		// mailInfo.setMailServerHost("smtp.exmail.qq.com");
		// mailInfo.setMailServerPort("465");
		// mailInfo.setValidate(true);
		mailInfo.setUserName("cheny@9rsoft.com.cn");
		mailInfo.setPassword("Abcd1234");// 您的邮箱密码

		// 设置发送给多人
		mailInfo.setFromAddress("cheny@9rsoft.com.cn");
		mailInfo.addToAddress("cheny@9rsoft.com.cn");
		mailInfo.addToAddress("523730098@qq.com");
		mailInfo.setSubject("设置邮箱标题");
		mailInfo.setContent("设置邮箱内容");

		// 设置添加附件
		mailInfo.addAttachFile(new MailAttachLocalFile("c:\\测试.txt", "测试.txt"));
		mailInfo.addAttachFile(new MailAttachLocalFile("c:\\welcome.jpg", "welcome.jpg"));
		mailInfo.addAttachFile(new MailAttachLocalFile("c:\\测试\\测试.txt", "测试.txt"));

		URL url = new URL("http://192.168.30.169:8080/test/attach_files/1.jpg");
		mailInfo.addAttachFile(new MailAttachURL(url, "1.jpg"));
		mailInfo.addAttachFile(new MailAttachURL("http://192.168.30.169:8080/test/attach_files/1.jpg", "1.jpg"));

		// true为是否使用配置文件
		SimpleMailSender sms = new SimpleMailSender(true);
		sms.setSmtpType("smtps");
		sms.setDebug(true);
		sms.sendMail(mailInfo);
	}
}
