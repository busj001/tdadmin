package com.tdcy.framework.mail;

import java.util.Properties;

import com.tdcy.framework.mail.exception.HHMailException;

public class PropertiesHelper {
	/**
	 * 加载properties文件
	 * 
	 * @Description loadProperties
	 * @date 2014-6-13
	 * @param path
	 * @return
	 * @return Properties
	 * @exception
	 */
	public static Properties loadProperties(String path) {
		Properties prop = new Properties();
		// 加载文件
		try {
			prop.load(PropertiesHelper.class.getResourceAsStream("/" + path));
		} catch (Exception e) {
			throw new HHMailException(e);
		}

		return prop;
	}

}
