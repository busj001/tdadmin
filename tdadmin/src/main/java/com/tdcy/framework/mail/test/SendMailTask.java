package com.tdcy.framework.mail.test;

import com.tdcy.framework.mail.SimpleMailSender;
import com.tdcy.framework.mail.entity.MailSenderInfo;

public class SendMailTask implements Runnable {
	public void run() {
		MailSenderInfo mailInfo = new MailSenderInfo();
		mailInfo.setUserName("c@upbase.com");
		mailInfo.setPassword("123456");// 您的邮箱密码

		// 设置发送给多人
		mailInfo.setFromAddress("c@upbase.com");
		mailInfo.addToAddress("a@upbase.com");
		mailInfo.setSubject("设置邮箱标题");
		mailInfo.setContent("设置邮箱内容");

		// 设置添加附件
		// mailInfo.addAttachFile(new MailAttachLocalFile("c:\\测试.txt",
		// "测试.txt"));
		// mailInfo.addAttachFile(new MailAttachLocalFile("c:\\welcome.jpg",
		// "welcome.jpg"));
		// mailInfo.addAttachFile(new MailAttachLocalFile("c:\\测试\\测试.txt",
		// "测试.txt"));

		// true为是否使用配置文件
		SimpleMailSender sms = new SimpleMailSender(true);
		sms.setSmtpType("smtp");
		sms.setDebug(true);
		sms.sendMail(mailInfo);
	}

}
