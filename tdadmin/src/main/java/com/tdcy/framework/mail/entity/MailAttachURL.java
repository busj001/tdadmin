package com.tdcy.framework.mail.entity;

import java.net.URL;

public class MailAttachURL extends MailAttachFile{
	// 上传的文件名
	private String urlPath;

	private URL url;

	public String getUrlPath() {
		return urlPath;
	}

	public void setUrlPath(String urlPath) {
		this.urlPath = urlPath;
	}

	public URL getUrl() {
		return url;
	}

	public void setUrl(URL url) {
		this.url = url;
	}

	public MailAttachURL(URL url,String name) {
		super(name);
		this.url = url;
	}

	public MailAttachURL(String urlPath,String name) {
		super(name);
		this.urlPath = urlPath;
	}
}
