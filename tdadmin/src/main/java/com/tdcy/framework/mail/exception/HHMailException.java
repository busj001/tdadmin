package com.tdcy.framework.mail.exception;

public class HHMailException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	private Throwable _rootCause;

	public HHMailException() {
		super();
	}

	public HHMailException(Throwable e) {
		super(e.getMessage(), e);

	}

	public HHMailException(String errMsg, Throwable e) {
		super(errMsg, e);
		if (e instanceof HHMailException) {
			_rootCause = ((HHMailException) e)._rootCause;
		} else {
			_rootCause = e;
		}
	}

	public HHMailException(String errMsg) {
		this(errMsg, null);
	}

	public Throwable getRootCause() {
		return _rootCause;
	}

	public boolean isException() {
		return _rootCause != null;
	}
}