package com.tdcy.framework.exception;

/**
 * <p>
 * Telpay顶级异常类，�?���?��获的异常从此类继�?
 * </p>
 * 
 * @version 1.0
 */
public class BaseException extends RuntimeException {

	private static final long serialVersionUID = 6500971673479091891L;

	private String errorCode;

	private String message;

	public BaseException(String message) {
		this.message = message;
	}

	public BaseException(String message, Throwable cause) {
		super(cause);
		this.message = message;
	}

	public BaseException(String errorCode, String message) {
		this.errorCode = errorCode;
		this.message = message;
	}

	public BaseException(String errorCode, String message, Throwable cause) {
		super(cause);
		this.errorCode = errorCode;
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
}
