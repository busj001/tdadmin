package com.tdcy.framework.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.tdcy.framework.filter.bean.ParameterRequestWrapper;

public class ParameterFilter implements Filter {

	public void destroy() {
		// TODO Auto-generated method stub

	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		ServletRequest req = request;
		if (request instanceof HttpServletRequest) {
			System.out.println(((HttpServletRequest) request).getRequestURL());
			ParameterRequestWrapper prw = new ParameterRequestWrapper(
					(HttpServletRequest) request);
			req = prw;
		}

		// request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		chain.doFilter(req, response);
	}

	public void init(FilterConfig filterconfig) throws ServletException {
		// TODO Auto-generated method stub

	}

}
