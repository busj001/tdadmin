package com.tdcy.framework.dao.metadata;


/**
 * Pair类.
 * 
 * @param <FIRST>
 *            the generic type
 * @param <SECOND>
 *            the generic type
 */
public final class Pair<FIRST, SECOND> {

	/** The first. */
	final FIRST first;

	/** The second. */
	final SECOND second;

	/**
	 * Instantiates a new pair.
	 * 
	 * @param first
	 *            the first
	 * @param second
	 *            the second
	 */
	public Pair(FIRST first, SECOND second) {
		this.first = first;
		this.second = second;
	}

	/**
	 * Gets the first.
	 * 
	 * @return the first
	 */
	public FIRST getFirst() {
		return first;
	}

	/**
	 * Gets the second.
	 * 
	 * @return the second
	 */
	public SECOND getSecond() {
		return second;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return 17 * ((first != null) ? first.hashCode() : 0) + 17 * ((second != null) ? second.hashCode() : 0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Pair<?, ?>)) {
			return false;
		}

		Pair<?, ?> that = (Pair<?, ?>) o;
		return equal(this.first, that.first) && equal(this.second, that.second);
	}

	/**
	 * Equal.
	 * 
	 * @param a
	 *            the a
	 * @param b
	 *            the b
	 * @return true, if successful
	 */
	private static boolean equal(Object a, Object b) {
		return a == b || (a != null && a.equals(b));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("{%s,%s}", first, second);
	}
}