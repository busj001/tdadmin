package com.tdcy.framework.dao.sqlxml;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

/**
 * 绝望的八皮
 * 
 * @author 绝望的八皮 2012-9-29
 */
public class SqlInXmlPlugin implements InitializingBean, DisposableBean {
	public boolean start() {
		try {
			SqlManager.parseSqlXml();
		} catch (Exception e) {
			new RuntimeException(e);
		}
		return true;
	}

	public boolean stop() {
		SqlManager.clearSqlMap();
		return true;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		start();
	}

	@Override
	public void destroy() throws Exception {
		stop();
	}
}
