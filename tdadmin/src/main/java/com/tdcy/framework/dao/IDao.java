package com.tdcy.framework.dao;

import java.util.List;
import java.util.Map;

import com.tdcy.framework.bean.PageInfo;
import com.tdcy.framework.exception.BaseException;

@SuppressWarnings("unchecked")
public interface IDao {
	public static final String RAWTYPES = "rawtypes";
	
	/**
	 * 通过sql查询，以数组返回
	 * 
	 * @param sql
	 *            查询sql
	 * @return List<Object[]>
	 */
	public List<Object[]> execSqlQueryToArrays(String sql);

	/**
	 * 通过sql查询，以数组返回
	 * 
	 * @param sql
	 *            查询sql
	 * @param params
	 *            查询参数
	 * @return List<Object[]>
	 */
	public List<Object[]> execSqlQueryToArrays(String sql, Object... params);

	/**
	 * 通过sql查询，以Map返回
	 * 
	 * @param sql
	 *            查询sql
	 * @return List<Map>
	 */
	public List<Map> execSqlQueryToMap(String sqlStr);

	/**
	 * 通过sql查询，以Map返回
	 * 
	 * @param sql
	 *            查询sql
	 * @param params
	 *            查询参数
	 * @return List<Map>
	 */
	public List<Map> execSqlQueryToMap(String sql, Object... params);

	/**
	 * 通过sql查询，以List<Class> 返回
	 * 
	 * @param clazz
	 *            类名
	 * @param sql
	 *            查询sql
	 * @return List<Class>
	 */
	public <T> List<T> execSqlQuery(Class clazz, String sql);

	/**
	 * 通过sql查询，以List<Class> 返回
	 * 
	 * @param clazz
	 *            类名
	 * @param sql
	 *            查询sql
	 * @param params
	 * @return List<Class>
	 */
	public <T> List<T> execSqlQuery(Class clazz, String sql, Object... params);


	/**
	 * 删除
	 * 
	 * @param object
	 * @return boolean
	 */
	public boolean delete(Object object) throws BaseException;

	/**
	 * 通过主键删除
	 * 
	 * @param clz
	 *            �?
	 * @param primaryKey
	 *            主键，可以是复合主键
	 * @return boolean
	 */
	public boolean deleteByPrimaryKey(Class clz, Object... primaryKeyValue);
	
	public boolean deleteByPrimaryKeyabsolute(Class cls, Object... primaryKeyValue) ;

	/**
	 * 保存
	 * 
	 * @param object
	 * @return boolean
	 */
	public <T> T save(T object) throws BaseException;

	/**
	 * 保存
	 * 
	 * @param object
	 * @return boolean
	 */
	public void save(Map<String, Object> obj, String tableName) throws BaseException;

	/**
	 * 更新
	 * 
	 * @param object
	 * @return boolean
	 */
	public <T> T update(T object) throws BaseException;

	/**
	 * 通过主键查询对象，可以是复合主键
	 * 
	 * 复合主键不要随便更换EO类中字段顺序，如果按主键查询时，是按字段顺序赋�?
	 * 
	 * @param primaryKeyValue
	 *            主键
	 * @return Object
	 */
	public <T> T findByPrimaryKey(Class cls, Object... primaryKeyValue);

	/**
	 * 
	 * @param sql
	 * @param args
	 * @return
	 */
	public int execSqlUpdate(String sql, Object... args);


	/**
	 * 批处更新
	 * 
	 * @param sql
	 *            更新sql
	 * @param args
	 *            参数
	 * @return int[]
	 */
	public int[] execSqlUpdateBatch(String sql, final int size, final Object[]... args);

	public int[] execSqlUpdateBatch(String sql, final int size, final List<Object[]> args);

	/**
	 * 更新
	 * 
	 * @param sql
	 *            更新Sql
	 * @return
	 */
	public int execSqlUpdate(String sql);

	/**
	 * 分页查询
	 * 
	 * @param sql
	 * @param cls
	 * @param pageSize
	 * @param curPageNO
	 * @return
	 */

	public PageInfo queryRecordByClassForPageInfo(String sql, Class cls, PageInfo pageinfo);


	public PageInfo queryRecordByClassForPageInfo(String sql, Class cls, PageInfo pageinfo, Object... args);

	/**
	 * 分页查询
	 * 
	 * @param sql
	 * @param cls
	 * @param pageSize
	 * @param curPageNO
	 * @return
	 */

	public PageInfo queryRecordByClassForPageInfo(String sql, Class cls, int pageSize, int curPageNO);


	/**
	 * 分页查询
	 * 
	 * @param sql
	 * @param cls
	 * @param pageSize
	 * @param curPageNO
	 * @return
	 */

	public List<Map> queryRecordByMapForPage(String sql, PageInfo pageinfo);

	/**
	 * 分页查询
	 * 
	 * @param sql
	 * @param pageSize
	 * @param curPageNO
	 * @return
	 */
	public List<Map> queryRecordByMapForPage(String sql, int pageSize, int curPageNO);

	/**
	 * 分页查询
	 * 
	 * @param sql
	 * @param pageSize
	 * @param curPageNO
	 * @param args
	 * @return
	 */
	public List<Map> queryRecordByMapForPage(String sql, int pageSize, int curPageNO, Object... args);

	/**
	 * 返回int 查询
	 * 
	 * @param sql
	 * @return
	 */
	public int queryForInt(String sql);

	/**
	 * 返回int 查询
	 * 
	 * @param sql
	 * @return
	 */
	public int queryForInt(String sql, Object... args);


	/**
	 * 查询单个对象
	 * 
	 * @param <T>
	 * @param clazz
	 * @param sql
	 * @return
	 */
	public <T> T execSqlQueryOne(Class clazz, String sql);

	/**
	 * 查询单个对象
	 * 
	 * @param <T>
	 * @param clazz
	 * @param sql
	 * @return
	 */
	public <T> T execSqlQueryOne(Class clazz, String sql, Object... args);

	public Map execSqlQueryOneToMap( String sql);
	
	public Map execSqlQueryOneToMap( String sql, Object... args) ;

	/**
	 * 查询浮点数据
	 * 
	 * @param sql
	 * @param paramterTable
	 * @return
	 */
	public double queryForDouble(String sql);

	/**
	 * 
	 * Description: 调用jdbcTemplate.queryForList，�?合原生类�?
	 * 
	 * @author: LiuWenzhao
	 * @version: 2013-12-11 上午11:16:37
	 */
	public <T> List<T> queryForList(String sql, Class<T> c);

	/**
	 * 
	 * Description: 调用jdbcTemplate.queryForObject
	 * 
	 * @author: LiuWenzhao
	 * @version: 2013-12-11 上午11:17:29
	 */
	public <T> T queryForObj(Class cls, String sql);


	/**
	 * 
	 * Description: 根据Class的table注解返回表的全部数据
	 * 
	 * @author: LiuWenzhao
	 * @version: 2013-12-11 上午10:17:30
	 */
	public <T> List<T> findTableRecords(Class<T> c);

	public Map executeCall(final String sql, final Map inparamMap, final Map outparamMap);

}
