package com.tdcy.framework.dao.metadata;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.StringTokenizer;

/**
 * 列定义.
 */
public class ColumnMetadata {

	/** The name. */
	private final String name;

	/** The type name. */
	private final String typeName;

	/** The column size. */
	private final int columnSize;

	/** The decimal digits. */
	private final int decimalDigits;

	/** The is nullable. */
	private final String isNullable;

	/** The type code. */
	private final int typeCode;

	/** The default value. */
	private final String defaultValue;

	/**
	 * Instantiates a new column meta data.
	 * 
	 * @param rs
	 *            the rs
	 * @throws SQLException
	 *             the sQL exception
	 */
	ColumnMetadata(ResultSet rs) throws SQLException {
		name = rs.getString("COLUMN_NAME");
		columnSize = rs.getInt("COLUMN_SIZE");
		decimalDigits = rs.getInt("DECIMAL_DIGITS");
		String def = rs.getString("COLUMN_DEF");
		if (def != null) {
			defaultValue = def.trim();
		} else {
			defaultValue = null;
		}
		isNullable = rs.getString("IS_NULLABLE");
		typeCode = rs.getInt("DATA_TYPE");
		typeName = new StringTokenizer(rs.getString("TYPE_NAME"), "() ").nextToken();
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets the type name.
	 * 
	 * @return the type name
	 */
	public String getTypeName() {
		return typeName;
	}

	/**
	 * Gets the column size.
	 * 
	 * @return the column size
	 */
	public int getColumnSize() {
		return columnSize;
	}

	/**
	 * Gets the decimal digits.
	 * 
	 * @return the decimal digits
	 */
	public int getDecimalDigits() {
		return decimalDigits;
	}

	/**
	 * Gets the nullable.
	 * 
	 * @return the nullable
	 */
	public String getNullable() {
		return isNullable;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "ColumnMetadata(" + name + ')';
	}

	/**
	 * Gets the type code.
	 * 
	 * @return the type code
	 */
	public int getTypeCode() {
		return typeCode;
	}

	/**
	 * Gets the default value.
	 * 
	 * @return the default value
	 */
	public String getDefaultValue() {
		return defaultValue;
	}
}
