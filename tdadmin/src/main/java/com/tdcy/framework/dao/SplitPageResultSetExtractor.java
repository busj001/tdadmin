package com.tdcy.framework.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.util.Assert;

@SuppressWarnings("rawtypes")
public class SplitPageResultSetExtractor implements ResultSetExtractor {
	private final int startIndex;// 起始行号

	private final int pageSize;// 每页记录

	private final RowMapper rowMapper;// 行包装器

	public SplitPageResultSetExtractor(RowMapper rowMapper, int startIndex,
			int pageSize) {
		Assert.notNull(rowMapper, "RowMapper is required");
		this.rowMapper = rowMapper;
		this.startIndex = startIndex;
		this.pageSize = pageSize;
	}

	/**
	 * 处理结果集合,被接口自动调用，该类外边不应该调
	 */
	@SuppressWarnings("unchecked")
	public Object extractData(ResultSet rs) throws SQLException,
			DataAccessException {
		List result = new ArrayList();
		rs.first();
		rs.relative(startIndex - 1);// 按相对行数（或正或负）移动光
		int count = 0;
		while (rs.next()) {
			count++;
			result.add(this.rowMapper.mapRow(rs, startIndex + count));
			if (count == pageSize) {
				break;
			}
		}
		return result;
	}

	/**
	 * 没有使用该方如果数据量较就无法查询出数据
	 */
	public Object extractDatas(ResultSet rs) throws SQLException,
			DataAccessException {
		List result = new ArrayList();
		int rowNum = 0;
		int end = startIndex + pageSize;
		point: while (rs.next()) {
			++rowNum;
			if (rowNum < startIndex) {
				continue point;
			} else if (rowNum >= end) {
				break point;
			} else {
				result.add(this.rowMapper.mapRow(rs, rowNum));
			}
		}
		return result;
	}
}