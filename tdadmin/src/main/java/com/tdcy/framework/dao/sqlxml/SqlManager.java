package com.tdcy.framework.dao.sqlxml;

import java.io.File;
import java.io.FileFilter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tdcy.framework.util.xmlu.XmlEnger;
import com.tdcy.framework.util.xmlu.entity.NodeEntity;

import freemarker.template.Template;

public class SqlManager {
	private static Map<String, Object> sqlMap = new HashMap<String, Object>();

	/***
	 * 从sql.xml中获取对应的Sqlgroup下的sql
	 * 
	 * @param groupNameAndsqlId
	 *            如groupname.sqlid
	 * @return
	 */
	public static String sql(String groupNameAndsqlId) {
		return (String) sqlMap.get(groupNameAndsqlId);
	}

	/**
	 * 依据所传参数生成sql<br>
	 * 使用freemarker语法
	 * 
	 * @param groupNameAndsqlId
	 * @param paras
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static String sql(String groupNameAndsqlId, Map<String, Object> paras) {
		try {
			StringReader reader = new StringReader(sql(groupNameAndsqlId));
			Template temp = new Template("", reader);
			StringWriter out = new StringWriter();
			temp.process(paras, out);
			return out.toString();
		} catch (Exception e) {
		}
		return null;
	}

	static void clearSqlMap() {
		sqlMap.clear();
	}

	/** 直接在classes目录下读取 xxxsql.xml */
	static void parseSqlXml() {
		parseSqlXml(new File(SqlManager.class.getClassLoader().getResource("").getFile()));
	}

	/** dir目录下读取 xxxsql.xml */
	static void parseSqlXml(File dir) {
		File[] files = dir.listFiles(new FileFilter() {
			public boolean accept(File pathname) {
				if (pathname.getName().endsWith("sql.xml")) {
					return true;
				}
				return false;
			}
		});
		for (File f : files) {
			XmlEnger e = new XmlEnger();
			try {
				e.setXmlFile(f);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			List<NodeEntity> nodeList = e.getNodeList("sqlGroupList.sqlGroup");

			for (NodeEntity ne : nodeList) {
				String name = ne.getAttri("name");
				List<NodeEntity> nodeList2 = e.getNodeListRePath(ne, "sql");

				List<String> sqlIdList = new ArrayList<String>();
				for (NodeEntity ne2 : nodeList2) {
					String id = ne2.getAttri("id");
					String sqltext = ne2.getText().trim();
					sqlMap.put(name + "." + id, sqltext);
					sqlIdList.add(id);

				}
				sqlMap.put(name, sqlIdList);// 当前group下有多少sql item id
			}

		}
	}

	public static void main(String[] args) {
		parseSqlXml();// new
						// File("D:/workspaces/eclipse3.7.2/easyUIJFinal/WebContent/WEB-INF/report"));
		System.out.println(sqlMap);
	}

}