package com.tdcy.framework.util;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * 异常辅助类.
 */
public abstract class ExceptionUtils {

	/**
	 * Instantiates a new exception helper.
	 */
	private ExceptionUtils() {

	}

	/**
	 * 获取异常字符串.
	 * 
	 * @param t
	 *            异常对象
	 * 
	 * @return 异常描述字符串
	 */
	public static String getThrowableStr(Throwable t) {
		if (t == null) {
			return "";
		}

		try {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);

			t.printStackTrace(pw);
			pw.flush();
			return sw.toString();
		} catch (Exception ex) {
			return ex.toString();
		}
	}

	/**
	 * 获取错误信息.
	 * 
	 * @param t
	 *            the t
	 * @return the error str
	 */
	public static String getErrorStr(Throwable t) {
		if (t == null) {
			return "";
		}

		Throwable inner = t;
		while (inner.getCause() != null) {
			inner = inner.getCause();
		}

		String msg = inner.getMessage();
		if (StringUtils.isEmpty(msg)) {
			msg = inner.getClass().getName();
		}
		return msg;
	}
}
