package com.tdcy.framework.util;


/**
 * <p>数组工具类</p>
 * <p><b>最后修改时间：</b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2010-12-17下午02:36:31</p>
 * @author zhengwei.bu
 * @version 1.0
 */
public class ArrayUtils {
	
	public static boolean isEmpty(Object[] args) {
		return args == null || args.length == 0 ? true : false;
	}
	
	public static boolean isNotEmpty(Object[] args) {
		return !isEmpty(args);
	}
	
}
