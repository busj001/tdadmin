package com.tdcy.framework.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class SessionUtils {
	public static Object getSesion(HttpSession session, String key) {
		return session.getAttribute(key);
	}

	public static void setSesion(HttpSession session, String key, Object value) {
		session.setAttribute(key, value);
	}

	/**
	 * �?session 设置属
	 * 
	 * @param name
	 *            the name
	 * @param value
	 *            the value
	 */
	public static void setSession(HttpServletRequest request, final Object name, final Object value) {
		request.getSession().setAttribute((String) name, value);
	}

	/**
	 * 读取 session 中的属
	 * 
	 * @param name
	 *            session
	 * @return the session
	 */
	public  static Object getSession(HttpServletRequest request, final String name) {
		return getSession(request, name, null);
	}

	/**
	 * 读取 session 中的属
	 * 
	 * @param name
	 *            session
	 * @param defaultValue
	 *            the default value
	 * @return the session
	 */
	public  static Object getSession(HttpServletRequest request, final String name, final Object defaultValue) {
		Object value = request.getSession().getAttribute(name);
		return value == null ? defaultValue : value;
	}

	/**
	 * 读取 session 中的属
	 * 
	 * @param name
	 *            session
	 * @return the session
	 */
	public  static String getSessionValue(HttpServletRequest request, final String name) {
		return getSessionValue(request, name, null);
	}

	/**
	 * 读取 session 中的属
	 * 
	 * @param name
	 *            session
	 * @param defaultValue
	 *            the default value
	 * @return the session
	 */
	public  static String getSessionValue(HttpServletRequest request, final String name, final Object defaultValue) {
		Object value = request.getSession().getAttribute(name);

		String ret = "";
		if (value != null) {
			ret = value.toString();
		} else {
			ret = (String) defaultValue;
		}
		return ret;
	}

}
