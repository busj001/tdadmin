package com.tdcy.framework.util.json;

import java.util.Date;

import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonValueProcessor;

import com.tdcy.framework.util.DateUtils;

/**
 * The Class DateJsonValueProcessor.
 */
public class DateJsonValueProcessor implements JsonValueProcessor {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.sf.json.processors.JsonValueProcessor#processArrayValue(java.lang
	 * .Object, net.sf.json.JsonConfig)
	 */
	public Object processArrayValue(Object value, JsonConfig jsonConfig) {
		String[] obj = {};
		if (value instanceof java.util.Date[]) {
			Date[] dates = (Date[]) value;
			obj = new String[dates.length];
			for (int i = 0; i < dates.length; i++) {
				obj[i] = DateUtils.datetimeToString(dates[i]);
			}
		}

		return obj;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.sf.json.processors.JsonValueProcessor#processObjectValue(java.lang
	 * .String, java.lang.Object, net.sf.json.JsonConfig)
	 */
	public Object processObjectValue(String key, Object value, JsonConfig jsonConfig) {
		return process(value);
	}

	/**
	 * Process.
	 * 
	 * @param value
	 *            the value
	 * @return the object
	 */
	private Object process(Object value) {
		if (value == null) {
			return null;
		}
		if (value instanceof java.util.Date) {
			return DateUtils.datetimeToString((Date) value);
		} else if (value instanceof java.sql.Date) {
			return DateUtils.datetimeToString(DateUtils.toDate((java.sql.Date) value));
		} else if (value instanceof java.sql.Timestamp) {
			return DateUtils.datetimeToString(DateUtils.toDate((java.sql.Timestamp) value));
		} else {
			return null;
		}
	}
}