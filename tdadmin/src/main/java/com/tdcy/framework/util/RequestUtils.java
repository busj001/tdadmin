package com.tdcy.framework.util;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.tdcy.framework.bean.PageInfo;

public class RequestUtils {

	/**
	 * 获取参数(
	 * 
	 * @param name
	 *            参数
	 * @return 参数
	 */
	public static String getParameter(HttpServletRequest request, final String name) {
		return getParameter(request, name, "");
	}

	/**
	 * 获取参数.
	 * 
	 * @param name
	 *            参数
	 * @param defaultValue
	 *            缺省
	 * @return 参数
	 */
	public static String getParameter(HttpServletRequest request, final String name, final String defaultValue) {
		String value = request.getParameter(name);
		if (value == null) {
			return defaultValue;
		}
		if (value != null) {
			value = value.trim();
		}
		return value;
	}

	/**
	 * 获取参数(返回int
	 * 
	 * @param name
	 *            参数
	 * @return the parameter int
	 */
	public static int getParameterInt(HttpServletRequest request, final String name) {
		return getParameterInt(request, name, 0);
	}

	/**
	 * 获取参数（返回int).
	 * 
	 * @param nam
	 *            参数
	 * @param defaultValue
	 *            the default value
	 * @return the parameter int
	 */
	public static int getParameterInt(HttpServletRequest request, final String name, final int defaultValue) {
		String s = getParameter(request, name);

		if (s == "") {
			return defaultValue;
		} else {
			try {
				return Integer.parseInt(s);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}

		return defaultValue;
	}

	/**
	 * 获取参数数组.
	 * 
	 * @param name
	 *            参数
	 * @return 参数
	 */
	public static String[] getParameterValues(HttpServletRequest request, final String name) {
		String[] values = request.getParameterValues(name);
		for (int i = 0; i < values.length; i++) {
			if (values[i] != null) {
				values[i] = values[i].trim();
			}
		}
		return values;
	}

	/**
	 * 获
	 * 
	 * @param request
	 *            请求对象
	 * @return the text content type
	 */
	public static Map<String, String> getAllParameters(HttpServletRequest request) {
		Map<String, String> map = new HashMap<String, String>();
		if (request == null) {
			return map;
		}

		Set<String> set = new HashSet<String>();
		Enumeration<String> e = request.getParameterNames();
		while (e.hasMoreElements()) {
			String key = e.nextElement().toString();
			if (key.startsWith("__multiselect_")) {
				set.add(key.substring("__multiselect_".length()));
			}
			map.put(key, getParameter(request, key));
		}

		for (Iterator<String> it = set.iterator(); it.hasNext();) {
			String key = it.next().toString();
			map.put(key, getParameterValues(request, key).toString());
		}

		return map;
	}

	/**
	 * 
	 * @param name
	 *            属
	 * @param value
	 *            属
	 */
	public static void setAttribute(HttpServletRequest request, final String name, final Object value) {
		request.setAttribute(name, value);
	}

	/**
	 * 获取 request 的属
	 * 
	 * @param name
	 *            属
	 * @return the attribute
	 */
	public static Object getAttribute(HttpServletRequest request, final String name) {
		return getAttribute(request, name, null);
	}

	/**
	 * 获取 request 的属
	 * 
	 * @param name
	 *            属
	 * @param defaultValue
	 *            缺省
	 * @return the attribute
	 */
	public static Object getAttribute(HttpServletRequest request, final String name, final Object defaultValue) {
		Object value = request.getAttribute(name);
		return value == null ? defaultValue : value;
	}

	/**
	 * 读取 session 中的属
	 * 
	 * @param name
	 *            session�?
	 * @return the session
	 */
	public static String getAttributeValue(HttpServletRequest request, final String name) {
		return getAttributeValue(request, name, null);
	}

	/**
	 * 读取 session 中的属
	 * 
	 * @param name
	 *            session
	 * @param defaultValue
	 *            the default value
	 * @return the session
	 */
	public static String getAttributeValue(HttpServletRequest request, final String name, final Object defaultValue) {
		Object value = request.getAttribute(name);

		String ret = "";
		if (value != null) {
			ret = value.toString();
		} else {
			ret = (String) defaultValue;
		}
		return ret;
	}

	/**
	 * 读取 session 中的属
	 * 
	 * @param name
	 *            session
	 * @param defaultValue
	 *            the default value
	 * @return the session
	 */
	public static PageInfo getPageParam(HttpServletRequest request) {
		String pageno = request.getParameter("page");
		String pagesize = request.getParameter("rows");
		PageInfo pageinfo = new PageInfo();
		pageinfo.setPageSize(Integer.parseInt(pagesize));
		pageinfo.setCurPageNO(Integer.parseInt(pageno));
		return pageinfo;
	}
	
	public static boolean isRestRequest(HttpServletRequest request)  {
		String uri = request.getRequestURI();
		String restflag = request.getParameter("restflag");
		if ("true".equals(restflag)) {
			return true;
		}
		
		if(uri.startsWith("rest/")){
			return true;
		}
		
		return false;
	}
}
