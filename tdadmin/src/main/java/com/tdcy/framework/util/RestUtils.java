package com.tdcy.framework.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * web辅助类.
 */
@SuppressWarnings(value = { "unchecked", "rawtypes" })
public abstract class RestUtils {
	private static Logger log = LoggerFactory.getLogger(RestUtils.class);

	private RestUtils() {

	}
	public static Map<String,String> getRestParam(HttpServletRequest request) {
		return RequestUtils.getAllParameters(request);
	}
	
	public static String getRestParam(HttpServletRequest request,String key) {
		return RequestUtils.getParameter(request, key);
	}

	/**
	 * 输出JSON错误信息.
	 * 
	 * @param response
	 *            响应对象
	 * @param errormsg
	 *            错误信息
	 * @param t
	 *            异常对象
	 */
	public static Map createJSONError(final Throwable t) {
		return createJSONError(null, t);
	}

	/**
	 * 输出JSON错误信息.
	 * 
	 * @param response
	 *            响应对象
	 * @param errormsg
	 *            错误信息
	 * @param t
	 *            异常对象
	 */
	public static Map createJSONError(final String errormsg) {
		return createJSONError(errormsg, null);
	}

	/**
	 * 输出JSON错误信息.
	 * 
	 * @param response
	 *            响应对象
	 * @param errormsg
	 *            错误信息
	 * @param t
	 *            异常对象
	 */
	public static Map createJSONError(final String errormsg, final Throwable t) {
		Map map = new HashMap();
		map.put("errorcode", t == null ? "1" : "2");
		if (StringUtils.isEmpty(errormsg)) {
			map.put("errormsg", t == null ? "" : t.getMessage());
		} else {
			map.put("errormsg", errormsg);
		}

		map.put("data", null);

		return map;
	}

	/**
	 * 输出JSON返回值.
	 * 
	 * @param response
	 *            响应对象
	 * @param errormsg
	 *            提示信息
	 * @param value
	 *            参数对象
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static Map createJSONSuccess(final String errormsg) {
		return createJSONSuccess(errormsg, null);
	}

	/**
	 * 输出JSON返回值.
	 * 
	 * @param response
	 *            响应对象
	 * @param errormsg
	 *            提示信息
	 * @param value
	 *            参数对象
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static Map createJSONSuccess(final String errormsg, final Object value) {
		Map map = new HashMap();
		map.put("errorcode", "0");
		map.put("errormsg", errormsg);
		map.put("data", value);

		return map;
	}
}
