package com.tdcy.framework.util.xmlu;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.xml.sax.SAXException;

import com.tdcy.framework.util.xmlu.entity.NodeEntity;

public class TestTools {
	public static void main(String[] args) throws SAXException, IOException, Exception {
		testReadXml();
	}

	public static void testReadXml() throws SAXException, IOException, Exception {
		XmlEnger e = new XmlEnger();

		e.setXmlFile(new File("E:\\workbench2\\lxcloud\\src\\main\\resources\\sql.xml"));

		// 根据path查找列表
		List<NodeEntity> nodeList = e.getNodeList("sqlGroupList.sqlGroup");
		System.out.println(nodeList);

		for (NodeEntity ne : nodeList) {
			List<NodeEntity> nodeList2 = e.getNodeListRePath(ne, "sql");
			System.out.println(nodeList2);
		}

	}
}
