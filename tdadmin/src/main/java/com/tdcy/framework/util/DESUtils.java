package com.tdcy.framework.util;

import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;
import java.util.Date;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import org.apache.commons.codec.binary.Hex;

import com.tdcy.framework.exception.BaseException;

/**
 * 数据加密解密处理
 */
public class DESUtils {


    /**
     * 加密
     * @param src
     * @param key
     * @return
     * @throws Exception
     */
    public static byte[] encrypt(byte[] key,byte[] src) throws Exception {
        // DES算法要求有一个可信任的随机数源
        SecureRandom sr = new SecureRandom();
        // 从原始密匙数据创建DESKeySpec对象
        DESKeySpec dks = new DESKeySpec(key);
        // 创建一个密匙工厂，然后用它把DESKeySpec转换成
        // 一个SecretKey对象
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
        SecretKey securekey = keyFactory.generateSecret(dks);
        // Cipher对象实际完成加密操作
        Cipher cipher = Cipher.getInstance("DES");
        // 用密匙初始化Cipher对象
        cipher.init(Cipher.ENCRYPT_MODE, securekey, sr);
        // 现在，获取数据并加密
        // 正式执行加密操作
        return cipher.doFinal(src);
    }

    /**
     * 加密函数
     * @param src
     * @param key
     * @return
     * @throws UnsupportedEncodingException 
     */
    public static String   encode(String key,String src){
    	try{
    		return new String(Hex.encodeHex(encrypt(key.getBytes(),src.getBytes("UTF8"))));
    	}catch(Exception e ){
    		throw new BaseException(e.getLocalizedMessage());
    	}
        
    }

    /**
     * 解密
     * @param src
     * @param key
     * @return
     * @throws Exception
     */
    public static byte[] decrypt(byte[] key,byte[] src) throws Exception {
        // DES算法要求有一个可信任的随机数源
        SecureRandom sr = new SecureRandom();
        // 从原始密匙数据创建一个DESKeySpec对象
        DESKeySpec dks = new DESKeySpec(key);
        // 创建一个密匙工厂，然后用它把DESKeySpec对象转换成
        // 一个SecretKey对象
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
        SecretKey securekey = keyFactory.generateSecret(dks);
        // Cipher对象实际完成解密操作
        Cipher cipher = Cipher.getInstance("DES");
        // 用密匙初始化Cipher对象
        cipher.init(Cipher.DECRYPT_MODE, securekey, sr);
        // 现在，获取数据并解密
        // 正式执行解密操作
        return cipher.doFinal(src);
    }

    /**
     * 解密函数
     * @param src
     * @param key
     * @return
     * @throws Exception
     */
    public static String   decode(String key,String src) throws Exception {
        return new String(decrypt(key.getBytes(),Hex.decodeHex(src.toCharArray())),"UTF8");
    }


    public static void main(String[] args)
    {
        String a="12323……（……4……）测试程序=";
        
        Date newDate = new Date();
       
        
        try {
            a= encode("hhdatabase11", a);
            System.out.println(a);
            a= decode("hhdatabase11", a);
            System.out.println(a);
            
            String mi_userCode = DESUtils.encode("manofthehour","2390" + "#" + newDate.getTime());
            
            System.out.println("******************" + mi_userCode);
            
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

}
