package com.tdcy.framework.util;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.ServletContext;

import com.tdcy.framework.exception.BaseException;

public class SysUtils {
	public static final String HTML_EMPTY_ENCODE = "%20";

	public static void main(String[] args) throws URISyntaxException {
		System.out.println(getClassesPath());
	}

	/**
	 * 
	 * @param name
	 * @return
	 * @throws URISyntaxException
	 */
	public static String getClassesPath() {
		try {
			String classDir = SysUtils.class.getClassLoader().getResource("").toURI().getPath();
			classDir = classDir.replaceAll(HTML_EMPTY_ENCODE, " ");
			return classDir;
		} catch (Exception e) {
			e.printStackTrace();
			throw new BaseException("获取异常", e);
		}
	}

	public static String getClazzPathByAbsoultPath(String absoutPath) {
		String classPath = getClassesPath();
		int endIndex = absoutPath.indexOf(".class");
		int startindex = classPath.length();
		String clazz = absoutPath.substring(startindex, endIndex);
		clazz = clazz.replaceAll("/", ".");
		clazz = clazz.replaceAll(HTML_EMPTY_ENCODE, " ");
		return clazz;
	}

	/**
	 * 获取web-inf目录
	 * 
	 * @return
	 * @throws IllegalAccessException
	 */
	public static String getWebInfPath() throws IllegalAccessException {
		PathUtil p = new PathUtil();
		return p.getWebInfPath();
	}

	/**
	 * 获取webroot目录
	 * 
	 * @return
	 * @throws IllegalAccessException
	 */
	public static String getWebRoot() throws IllegalAccessException {
		PathUtil p = new PathUtil();
		return p.getWebRoot();
	}

	public static String getRealPath(ServletContext ctx, String path) {

		if (!path.startsWith("/"))
			path = "/" + path;

		String rpath = ctx.getRealPath(path);
		try {
			if (null == rpath || "".equals(rpath))
				rpath = new File(new URI(ctx.getResource(path).toString())).getPath();
		} catch (java.net.MalformedURLException e) {
			e.printStackTrace();
			return "";
		} catch (java.net.URISyntaxException e) {
			e.printStackTrace();
			return "";
		}
		String os = System.getProperty("os.name");
		boolean isWIN = (os.toLowerCase().indexOf("windows") >= 0);

		if (isWIN)
			if (!rpath.endsWith("\\"))
				rpath += "\\";
			else if (!rpath.endsWith("/"))
				rpath += "/";
		return rpath;
	}

	public static String getResource(ServletContext ctx, String path) {

		if (!path.startsWith("/"))
			path = "/" + path;

		String ipath = "";
		try {
			ipath = ctx.getResource(path).toString();
			if (null == ipath || "".equals(ipath) || ipath.trim().toLowerCase().startsWith("jndi"))
				ipath = ctx.getRealPath(path);
			else
				ipath = new File(new URI(ipath)).getPath();
		} catch (java.net.MalformedURLException e) {
			e.printStackTrace();
			return "";
		} catch (java.net.URISyntaxException e) {
			e.printStackTrace();
			return "";
		}
		String os = System.getProperty("os.name");
		boolean isWIN = (os.toLowerCase().indexOf("windows") >= 0);

		if (isWIN)
			if (!ipath.endsWith("\\"))
				ipath += "\\";
			else if (!ipath.endsWith("/"))
				ipath += "/";
		return ipath;
	}

	public static String getWebClassesPath() {
		PathUtil p = new PathUtil();
		try {
			return p.getWebInfPath();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		return "";
	}
}

class PathUtil {
	public String getWebClassesPath() {
		String path = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		path = path.replaceAll(SysUtils.HTML_EMPTY_ENCODE, " ");
		return path;

	}

	public String getWebInfPath() throws IllegalAccessException {
		String path = getWebClassesPath();
		if (path.indexOf("WEB-INF") > 0) {
			path = path.substring(0, path.indexOf("WEB-INF") + 8);
		} else {
			throw new IllegalAccessException("路径获取错误");
		}
		path = path.replaceAll(SysUtils.HTML_EMPTY_ENCODE, " ");
		return path;
	}

	public String getWebRoot() throws IllegalAccessException {
		String path = getWebClassesPath();
		if (path.indexOf("WEB-INF") > 0) {
			path = path.substring(0, path.indexOf("WEB-INF"));
		} else {
			throw new IllegalAccessException("路径获取错误");
		}
		path = path.replaceAll(SysUtils.HTML_EMPTY_ENCODE, " ");
		return path;
	}
}
