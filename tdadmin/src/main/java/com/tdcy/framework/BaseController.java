package com.tdcy.framework;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.request.WebRequest;

import com.tdcy.framework.util.DateConvertEditor;

public class BaseController {
	public Logger logger = LoggerFactory.getLogger(BaseController.class);

	@InitBinder
	public void initBinder(WebDataBinder binder, WebRequest request) {
		binder.registerCustomEditor(Date.class, new DateConvertEditor());
	}

	/**
	 * 
	 * @param name
	 *            the name
	 * @param value
	 *            the value
	 * @throws IOException
	 */
	public void sendRedirect(HttpServletResponse response, final String url) throws IOException {
		response.sendRedirect(url);
	}
	
	   /**
		  * 获取 HttpServletRequest
		  */
		 public  HttpServletResponse getResponse() {
		     HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
		     return response;
		 }
		
		 /**
		  * 获取 包装防Xss Sql注入的 HttpServletRequest
		  * @return request
		  */
		 public  HttpServletRequest getRequest() {
		     HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		     return request;
		 }

}
