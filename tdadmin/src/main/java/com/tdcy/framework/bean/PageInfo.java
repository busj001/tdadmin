package com.tdcy.framework.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({ "unchecked", "serial" })
public class PageInfo<T> implements Serializable {

	private int totalRecord;// 总记录
	private int totalPage;// 总页
	private int pageSize;// 每页记录条数
	private int curPageNO;// 当前页码

	private List<T> resultsList;// 结果

	public int getTotalRecord() {
		return totalRecord;
	}

	public void setTotalRecord(int totalRecord) {
		this.totalRecord = totalRecord;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getCurPageNO() {
		return curPageNO;
	}

	public void setCurPageNO(int curPageNO) {
		this.curPageNO = curPageNO;
	}

	public List<T> getResultsList() {
		return resultsList;
	}

	public void setResultsList(List<T> resultsList) {
		this.resultsList = resultsList;
	}

	public List<Integer> generatePageList() {
		List<Integer> pageList = new ArrayList<Integer>();
		if (this.getTotalPage() == 0) {
			pageList.add(1);
		} else if (this.getTotalPage() < 10) {
			for (int i = 1; i <= this.getTotalPage(); i++)
				pageList.add(i);
		} else {
			if (this.getCurPageNO() < 6) {
				for (int i = 1; i < 9; i++)
					pageList.add(i);
				pageList.add(-1);
			} else if (this.getTotalPage() - this.getCurPageNO() < 4) {
				pageList.add(1);
				pageList.add(2);
				pageList.add(-1);
				for (int i = this.getTotalPage() - 4; i <= this.getTotalPage(); i++)
					pageList.add(i);
			} else {
				pageList.add(-1);
				for (int i = this.getCurPageNO() - 2; i <= this.getCurPageNO() + 2; i++)
					pageList.add(i);
				pageList.add(-1);
			}
		}
		return pageList;
	}
}
