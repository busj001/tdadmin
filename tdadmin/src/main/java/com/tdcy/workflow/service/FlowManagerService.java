package com.tdcy.workflow.service;

import java.util.Map;

import org.activiti.engine.IdentityService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FlowManagerService {

	@Autowired
	private TaskService tasksService;
	@Autowired
	private IdentityService identityService;
	@Autowired
	private RuntimeService runtimeService;

	/**
	 * 办理流程到下一步
	 * 
	 * @param taskId
	 * @param userId
	 * @param formProperties
	 * @return
	 */
	public void completeTask(String taskId, Map<String, Object> formProperties) {
		tasksService.complete(taskId, formProperties);
	}

	/**
	 * 启动流程
	 *
	 */
	public ProcessInstance startWorkflow(String flowkey, Map<String, Object> variables) {
		// currentuser = SecurityUserHolder.getCurrentUser();
		// 用来设置启动流程的人员ID，引擎会自动把用户ID保存到activiti:initiator中
		// identityService.setAuthenticatedUserId(currentuser.getUserId().toString());
		ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(flowkey, variables);
		String processInstanceId = processInstance.getId();
		return processInstance;
	}
}
