package com.tdcy.workflow.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.tdcy.workflow.dao.WorkFlowDao;

/**
 * 工作流跟踪相关Service
 *
 * @author HenryYan
 */
@Service
public class WorkFlowService {

	@Resource
	protected WorkFlowDao workFlowDao;

}
