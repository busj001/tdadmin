package com.tdcy.workflow.controller;

import javax.servlet.http.HttpServletRequest;

import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tdcy.framework.util.WebUtils;
import com.tdcy.workflow.service.WorkFlowService;

@Controller
@RequestMapping(value = "/workflow")
public class WorkFlowControl {
	@Autowired
	private WorkFlowService workFlowService;
	@Autowired
	private TaskService tasksService;

	/**
	 * 根据流程实例ID，获取流程定义ID
	 */
	@ResponseBody
	@RequestMapping(value = "/getFlowDefId")
	public Object getFlowDefId(HttpServletRequest request, @RequestParam("procInstId") String procInstId) {
		try {
			Task task = tasksService.createTaskQuery().executionId(procInstId).singleResult();
			return WebUtils.createJSONSuccess("获取成功", task.getProcessDefinitionId());
		} catch (Exception e) {
			return WebUtils.createJSONError(e);
		}
	}
}
