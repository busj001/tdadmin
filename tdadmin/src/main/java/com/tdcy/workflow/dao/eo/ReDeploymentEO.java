package com.tdcy.workflow.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;
/**
* @Description EO类
*/
@Table(name = "act_re_deployment")
public class ReDeploymentEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "id_")
	private String id;
	/**
	* 
	*/
	@Column(name = "name_")
	private String name;
	/**
	* 
	*/
	@Column(name = "category_")
	private String category;
	/**
	* 
	*/
	@Column(name = "tenant_id_")
	private String tenantId;
	/**
	* 
	*/
	@Column(name = "deploy_time_")
	private java.util.Date deployTime;
	/**
	* 获取 
	*/ 
	public String getId() {
		return id;
	}
	/**
	* 设置
	*/
	public void setId(String id) {
		this.id = id;
	}
	/**
	* 获取 
	*/ 
	public String getName() {
		return name;
	}
	/**
	* 设置
	*/
	public void setName(String name) {
		this.name = name;
	}
	/**
	* 获取 
	*/ 
	public String getCategory() {
		return category;
	}
	/**
	* 设置
	*/
	public void setCategory(String category) {
		this.category = category;
	}
	/**
	* 获取 
	*/ 
	public String getTenantId() {
		return tenantId;
	}
	/**
	* 设置
	*/
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getDeployTime() {
		return deployTime;
	}
	/**
	* 设置
	*/
	public void setDeployTime(java.util.Date deployTime) {
		this.deployTime = deployTime;
	}
}

