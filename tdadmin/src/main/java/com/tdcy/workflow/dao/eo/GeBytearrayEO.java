package com.tdcy.workflow.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;
/**
* @Description EO类
*/
@Table(name = "act_ge_bytearray")
public class GeBytearrayEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "id_")
	private String id;
	/**
	* 
	*/
	@Column(name = "rev_")
	private Integer rev;
	/**
	* 
	*/
	@Column(name = "name_")
	private String name;
	/**
	* 
	*/
	@Column(name = "deployment_id_")
	private String deploymentId;
	/**
	* 
	*/
	@Column(name = "bytes_")
	private byte[] bytes;
	/**
	* 
	*/
	@Column(name = "generated_")
	private Integer generated;
	/**
	* 获取 
	*/ 
	public String getId() {
		return id;
	}
	/**
	* 设置
	*/
	public void setId(String id) {
		this.id = id;
	}
	/**
	* 获取 
	*/ 
	public Integer getRev() {
		return rev;
	}
	/**
	* 设置
	*/
	public void setRev(Integer rev) {
		this.rev = rev;
	}
	/**
	* 获取 
	*/ 
	public String getName() {
		return name;
	}
	/**
	* 设置
	*/
	public void setName(String name) {
		this.name = name;
	}
	/**
	* 获取 
	*/ 
	public String getDeploymentId() {
		return deploymentId;
	}
	/**
	* 设置
	*/
	public void setDeploymentId(String deploymentId) {
		this.deploymentId = deploymentId;
	}
	/**
	* 获取 
	*/ 
	public byte[] getBytes() {
		return bytes;
	}
	/**
	* 设置
	*/
	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}
	/**
	* 获取 
	*/ 
	public Integer getGenerated() {
		return generated;
	}
	/**
	* 设置
	*/
	public void setGenerated(Integer generated) {
		this.generated = generated;
	}
}

