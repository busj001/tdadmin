package com.tdcy.workflow.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;
/**
* @Description EO类
*/
@Table(name = "act_id_user")
public class IdUserEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "id_")
	private String id;
	/**
	* 
	*/
	@Column(name = "rev_")
	private Integer rev;
	/**
	* 
	*/
	@Column(name = "first_")
	private String first;
	/**
	* 
	*/
	@Column(name = "last_")
	private String last;
	/**
	* 
	*/
	@Column(name = "email_")
	private String email;
	/**
	* 
	*/
	@Column(name = "pwd_")
	private String pwd;
	/**
	* 
	*/
	@Column(name = "picture_id_")
	private String pictureId;
	/**
	* 获取 
	*/ 
	public String getId() {
		return id;
	}
	/**
	* 设置
	*/
	public void setId(String id) {
		this.id = id;
	}
	/**
	* 获取 
	*/ 
	public Integer getRev() {
		return rev;
	}
	/**
	* 设置
	*/
	public void setRev(Integer rev) {
		this.rev = rev;
	}
	/**
	* 获取 
	*/ 
	public String getFirst() {
		return first;
	}
	/**
	* 设置
	*/
	public void setFirst(String first) {
		this.first = first;
	}
	/**
	* 获取 
	*/ 
	public String getLast() {
		return last;
	}
	/**
	* 设置
	*/
	public void setLast(String last) {
		this.last = last;
	}
	/**
	* 获取 
	*/ 
	public String getEmail() {
		return email;
	}
	/**
	* 设置
	*/
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	* 获取 
	*/ 
	public String getPwd() {
		return pwd;
	}
	/**
	* 设置
	*/
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	/**
	* 获取 
	*/ 
	public String getPictureId() {
		return pictureId;
	}
	/**
	* 设置
	*/
	public void setPictureId(String pictureId) {
		this.pictureId = pictureId;
	}
}

