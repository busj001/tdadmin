package com.tdcy.workflow.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;
/**
* @Description EO类
*/
@Table(name = "act_evt_log")
public class EvtLogEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "log_nr_")
	private Integer logNr;
	/**
	* 
	*/
	@Column(name = "type_")
	private String type;
	/**
	* 
	*/
	@Column(name = "proc_def_id_")
	private String procDefId;
	/**
	* 
	*/
	@Column(name = "proc_inst_id_")
	private String procInstId;
	/**
	* 
	*/
	@Column(name = "execution_id_")
	private String executionId;
	/**
	* 
	*/
	@Column(name = "task_id_")
	private String taskId;
	/**
	* 
	*/
	@Column(name = "time_stamp_")
	private java.util.Date timeStamp;
	/**
	* 
	*/
	@Column(name = "user_id_")
	private String userId;
	/**
	* 
	*/
	@Column(name = "data_")
	private byte[] data;
	/**
	* 
	*/
	@Column(name = "lock_owner_")
	private String lockOwner;
	/**
	* 
	*/
	@Column(name = "lock_time_")
	private java.util.Date lockTime;
	/**
	* 
	*/
	@Column(name = "is_processed_")
	private Integer isProcessed;
	/**
	* 获取 
	*/ 
	public Integer getLogNr() {
		return logNr;
	}
	/**
	* 设置
	*/
	public void setLogNr(Integer logNr) {
		this.logNr = logNr;
	}
	/**
	* 获取 
	*/ 
	public String getType() {
		return type;
	}
	/**
	* 设置
	*/
	public void setType(String type) {
		this.type = type;
	}
	/**
	* 获取 
	*/ 
	public String getProcDefId() {
		return procDefId;
	}
	/**
	* 设置
	*/
	public void setProcDefId(String procDefId) {
		this.procDefId = procDefId;
	}
	/**
	* 获取 
	*/ 
	public String getProcInstId() {
		return procInstId;
	}
	/**
	* 设置
	*/
	public void setProcInstId(String procInstId) {
		this.procInstId = procInstId;
	}
	/**
	* 获取 
	*/ 
	public String getExecutionId() {
		return executionId;
	}
	/**
	* 设置
	*/
	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}
	/**
	* 获取 
	*/ 
	public String getTaskId() {
		return taskId;
	}
	/**
	* 设置
	*/
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getTimeStamp() {
		return timeStamp;
	}
	/**
	* 设置
	*/
	public void setTimeStamp(java.util.Date timeStamp) {
		this.timeStamp = timeStamp;
	}
	/**
	* 获取 
	*/ 
	public String getUserId() {
		return userId;
	}
	/**
	* 设置
	*/
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	* 获取 
	*/ 
	public byte[] getData() {
		return data;
	}
	/**
	* 设置
	*/
	public void setData(byte[] data) {
		this.data = data;
	}
	/**
	* 获取 
	*/ 
	public String getLockOwner() {
		return lockOwner;
	}
	/**
	* 设置
	*/
	public void setLockOwner(String lockOwner) {
		this.lockOwner = lockOwner;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getLockTime() {
		return lockTime;
	}
	/**
	* 设置
	*/
	public void setLockTime(java.util.Date lockTime) {
		this.lockTime = lockTime;
	}
	/**
	* 获取 
	*/ 
	public Integer getIsProcessed() {
		return isProcessed;
	}
	/**
	* 设置
	*/
	public void setIsProcessed(Integer isProcessed) {
		this.isProcessed = isProcessed;
	}
}

