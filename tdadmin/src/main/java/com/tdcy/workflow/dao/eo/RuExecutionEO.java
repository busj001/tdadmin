package com.tdcy.workflow.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;
/**
* @Description EO类
*/
@Table(name = "act_ru_execution")
public class RuExecutionEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "id_")
	private String id;
	/**
	* 
	*/
	@Column(name = "rev_")
	private Integer rev;
	/**
	* 
	*/
	@Column(name = "proc_inst_id_")
	private String procInstId;
	/**
	* 
	*/
	@Column(name = "business_key_")
	private String businessKey;
	/**
	* 
	*/
	@Column(name = "parent_id_")
	private String parentId;
	/**
	* 
	*/
	@Column(name = "proc_def_id_")
	private String procDefId;
	/**
	* 
	*/
	@Column(name = "super_exec_")
	private String superExec;
	/**
	* 
	*/
	@Column(name = "act_id_")
	private String actId;
	/**
	* 
	*/
	@Column(name = "is_active_")
	private Integer isActive;
	/**
	* 
	*/
	@Column(name = "is_concurrent_")
	private Integer isConcurrent;
	/**
	* 
	*/
	@Column(name = "is_scope_")
	private Integer isScope;
	/**
	* 
	*/
	@Column(name = "is_event_scope_")
	private Integer isEventScope;
	/**
	* 
	*/
	@Column(name = "suspension_state_")
	private Integer suspensionState;
	/**
	* 
	*/
	@Column(name = "cached_ent_state_")
	private Integer cachedEntState;
	/**
	* 
	*/
	@Column(name = "tenant_id_")
	private String tenantId;
	/**
	* 
	*/
	@Column(name = "name_")
	private String name;
	/**
	* 
	*/
	@Column(name = "lock_time_")
	private java.util.Date lockTime;
	/**
	* 获取 
	*/ 
	public String getId() {
		return id;
	}
	/**
	* 设置
	*/
	public void setId(String id) {
		this.id = id;
	}
	/**
	* 获取 
	*/ 
	public Integer getRev() {
		return rev;
	}
	/**
	* 设置
	*/
	public void setRev(Integer rev) {
		this.rev = rev;
	}
	/**
	* 获取 
	*/ 
	public String getProcInstId() {
		return procInstId;
	}
	/**
	* 设置
	*/
	public void setProcInstId(String procInstId) {
		this.procInstId = procInstId;
	}
	/**
	* 获取 
	*/ 
	public String getBusinessKey() {
		return businessKey;
	}
	/**
	* 设置
	*/
	public void setBusinessKey(String businessKey) {
		this.businessKey = businessKey;
	}
	/**
	* 获取 
	*/ 
	public String getParentId() {
		return parentId;
	}
	/**
	* 设置
	*/
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	/**
	* 获取 
	*/ 
	public String getProcDefId() {
		return procDefId;
	}
	/**
	* 设置
	*/
	public void setProcDefId(String procDefId) {
		this.procDefId = procDefId;
	}
	/**
	* 获取 
	*/ 
	public String getSuperExec() {
		return superExec;
	}
	/**
	* 设置
	*/
	public void setSuperExec(String superExec) {
		this.superExec = superExec;
	}
	/**
	* 获取 
	*/ 
	public String getActId() {
		return actId;
	}
	/**
	* 设置
	*/
	public void setActId(String actId) {
		this.actId = actId;
	}
	/**
	* 获取 
	*/ 
	public Integer getIsActive() {
		return isActive;
	}
	/**
	* 设置
	*/
	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}
	/**
	* 获取 
	*/ 
	public Integer getIsConcurrent() {
		return isConcurrent;
	}
	/**
	* 设置
	*/
	public void setIsConcurrent(Integer isConcurrent) {
		this.isConcurrent = isConcurrent;
	}
	/**
	* 获取 
	*/ 
	public Integer getIsScope() {
		return isScope;
	}
	/**
	* 设置
	*/
	public void setIsScope(Integer isScope) {
		this.isScope = isScope;
	}
	/**
	* 获取 
	*/ 
	public Integer getIsEventScope() {
		return isEventScope;
	}
	/**
	* 设置
	*/
	public void setIsEventScope(Integer isEventScope) {
		this.isEventScope = isEventScope;
	}
	/**
	* 获取 
	*/ 
	public Integer getSuspensionState() {
		return suspensionState;
	}
	/**
	* 设置
	*/
	public void setSuspensionState(Integer suspensionState) {
		this.suspensionState = suspensionState;
	}
	/**
	* 获取 
	*/ 
	public Integer getCachedEntState() {
		return cachedEntState;
	}
	/**
	* 设置
	*/
	public void setCachedEntState(Integer cachedEntState) {
		this.cachedEntState = cachedEntState;
	}
	/**
	* 获取 
	*/ 
	public String getTenantId() {
		return tenantId;
	}
	/**
	* 设置
	*/
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	/**
	* 获取 
	*/ 
	public String getName() {
		return name;
	}
	/**
	* 设置
	*/
	public void setName(String name) {
		this.name = name;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getLockTime() {
		return lockTime;
	}
	/**
	* 设置
	*/
	public void setLockTime(java.util.Date lockTime) {
		this.lockTime = lockTime;
	}
}

