package com.tdcy.workflow.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;
/**
* @Description EO类
*/
@Table(name = "act_id_membership")
public class IdMembershipEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "user_id_")
	private String userId;
	/**
	* 
	*/
	@Column(name = "group_id_")
	private String groupId;
	/**
	* 获取 
	*/ 
	public String getUserId() {
		return userId;
	}
	/**
	* 设置
	*/
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	* 获取 
	*/ 
	public String getGroupId() {
		return groupId;
	}
	/**
	* 设置
	*/
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
}

