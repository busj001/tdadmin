package com.tdcy.workflow.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;
/**
* @Description EO类
*/
@Table(name = "act_hi_varinst")
public class HiVarinstEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "id_")
	private String id;
	/**
	* 
	*/
	@Column(name = "proc_inst_id_")
	private String procInstId;
	/**
	* 
	*/
	@Column(name = "execution_id_")
	private String executionId;
	/**
	* 
	*/
	@Column(name = "task_id_")
	private String taskId;
	/**
	* 
	*/
	@Column(name = "name_")
	private String name;
	/**
	* 
	*/
	@Column(name = "var_type_")
	private String varType;
	/**
	* 
	*/
	@Column(name = "rev_")
	private Integer rev;
	/**
	* 
	*/
	@Column(name = "bytearray_id_")
	private String bytearrayId;
	/**
	* 
	*/
	@Column(name = "double_")
	private Double doubleX;
	/**
	* 
	*/
	@Column(name = "long_")
	private Integer longX;
	/**
	* 
	*/
	@Column(name = "text_")
	private String text;
	/**
	* 
	*/
	@Column(name = "text2_")
	private String text2;
	/**
	* 
	*/
	@Column(name = "create_time_")
	private java.util.Date createTime;
	/**
	* 
	*/
	@Column(name = "last_updated_time_")
	private java.util.Date lastUpdatedTime;
	/**
	* 获取 
	*/ 
	public String getId() {
		return id;
	}
	/**
	* 设置
	*/
	public void setId(String id) {
		this.id = id;
	}
	/**
	* 获取 
	*/ 
	public String getProcInstId() {
		return procInstId;
	}
	/**
	* 设置
	*/
	public void setProcInstId(String procInstId) {
		this.procInstId = procInstId;
	}
	/**
	* 获取 
	*/ 
	public String getExecutionId() {
		return executionId;
	}
	/**
	* 设置
	*/
	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}
	/**
	* 获取 
	*/ 
	public String getTaskId() {
		return taskId;
	}
	/**
	* 设置
	*/
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	/**
	* 获取 
	*/ 
	public String getName() {
		return name;
	}
	/**
	* 设置
	*/
	public void setName(String name) {
		this.name = name;
	}
	/**
	* 获取 
	*/ 
	public String getVarType() {
		return varType;
	}
	/**
	* 设置
	*/
	public void setVarType(String varType) {
		this.varType = varType;
	}
	/**
	* 获取 
	*/ 
	public Integer getRev() {
		return rev;
	}
	/**
	* 设置
	*/
	public void setRev(Integer rev) {
		this.rev = rev;
	}
	/**
	* 获取 
	*/ 
	public String getBytearrayId() {
		return bytearrayId;
	}
	/**
	* 设置
	*/
	public void setBytearrayId(String bytearrayId) {
		this.bytearrayId = bytearrayId;
	}
	
	public Double getDoubleX() {
		return doubleX;
	}
	public void setDoubleX(Double doubleX) {
		this.doubleX = doubleX;
	}
	public Integer getLongX() {
		return longX;
	}
	public void setLongX(Integer longX) {
		this.longX = longX;
	}
	/**
	* 获取 
	*/ 
	public String getText() {
		return text;
	}
	/**
	* 设置
	*/
	public void setText(String text) {
		this.text = text;
	}
	/**
	* 获取 
	*/ 
	public String getText2() {
		return text2;
	}
	/**
	* 设置
	*/
	public void setText2(String text2) {
		this.text2 = text2;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getCreateTime() {
		return createTime;
	}
	/**
	* 设置
	*/
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getLastUpdatedTime() {
		return lastUpdatedTime;
	}
	/**
	* 设置
	*/
	public void setLastUpdatedTime(java.util.Date lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}
}

