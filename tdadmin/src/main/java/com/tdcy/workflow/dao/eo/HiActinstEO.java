package com.tdcy.workflow.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;
/**
* @Description EO类
*/
@Table(name = "act_hi_actinst")
public class HiActinstEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "id_")
	private String id;
	/**
	* 
	*/
	@Column(name = "proc_def_id_")
	private String procDefId;
	/**
	* 
	*/
	@Column(name = "proc_inst_id_")
	private String procInstId;
	/**
	* 
	*/
	@Column(name = "execution_id_")
	private String executionId;
	/**
	* 
	*/
	@Column(name = "act_id_")
	private String actId;
	/**
	* 
	*/
	@Column(name = "task_id_")
	private String taskId;
	/**
	* 
	*/
	@Column(name = "call_proc_inst_id_")
	private String callProcInstId;
	/**
	* 
	*/
	@Column(name = "act_name_")
	private String actName;
	/**
	* 
	*/
	@Column(name = "act_type_")
	private String actType;
	/**
	* 
	*/
	@Column(name = "assignee_")
	private String assignee;
	/**
	* 
	*/
	@Column(name = "start_time_")
	private java.util.Date startTime;
	/**
	* 
	*/
	@Column(name = "end_time_")
	private java.util.Date endTime;
	/**
	* 
	*/
	@Column(name = "duration_")
	private Integer duration;
	/**
	* 
	*/
	@Column(name = "tenant_id_")
	private String tenantId;
	/**
	* 获取 
	*/ 
	public String getId() {
		return id;
	}
	/**
	* 设置
	*/
	public void setId(String id) {
		this.id = id;
	}
	/**
	* 获取 
	*/ 
	public String getProcDefId() {
		return procDefId;
	}
	/**
	* 设置
	*/
	public void setProcDefId(String procDefId) {
		this.procDefId = procDefId;
	}
	/**
	* 获取 
	*/ 
	public String getProcInstId() {
		return procInstId;
	}
	/**
	* 设置
	*/
	public void setProcInstId(String procInstId) {
		this.procInstId = procInstId;
	}
	/**
	* 获取 
	*/ 
	public String getExecutionId() {
		return executionId;
	}
	/**
	* 设置
	*/
	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}
	/**
	* 获取 
	*/ 
	public String getActId() {
		return actId;
	}
	/**
	* 设置
	*/
	public void setActId(String actId) {
		this.actId = actId;
	}
	/**
	* 获取 
	*/ 
	public String getTaskId() {
		return taskId;
	}
	/**
	* 设置
	*/
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	/**
	* 获取 
	*/ 
	public String getCallProcInstId() {
		return callProcInstId;
	}
	/**
	* 设置
	*/
	public void setCallProcInstId(String callProcInstId) {
		this.callProcInstId = callProcInstId;
	}
	/**
	* 获取 
	*/ 
	public String getActName() {
		return actName;
	}
	/**
	* 设置
	*/
	public void setActName(String actName) {
		this.actName = actName;
	}
	/**
	* 获取 
	*/ 
	public String getActType() {
		return actType;
	}
	/**
	* 设置
	*/
	public void setActType(String actType) {
		this.actType = actType;
	}
	/**
	* 获取 
	*/ 
	public String getAssignee() {
		return assignee;
	}
	/**
	* 设置
	*/
	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getStartTime() {
		return startTime;
	}
	/**
	* 设置
	*/
	public void setStartTime(java.util.Date startTime) {
		this.startTime = startTime;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getEndTime() {
		return endTime;
	}
	/**
	* 设置
	*/
	public void setEndTime(java.util.Date endTime) {
		this.endTime = endTime;
	}
	/**
	* 获取 
	*/ 
	public Integer getDuration() {
		return duration;
	}
	/**
	* 设置
	*/
	public void setDuration(Integer duration) {
		this.duration = duration;
	}
	/**
	* 获取 
	*/ 
	public String getTenantId() {
		return tenantId;
	}
	/**
	* 设置
	*/
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
}

