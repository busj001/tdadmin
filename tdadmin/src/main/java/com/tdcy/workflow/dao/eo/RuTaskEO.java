package com.tdcy.workflow.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;
/**
* @Description EO类
*/
@Table(name = "act_ru_task")
public class RuTaskEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "id_")
	private String id;
	/**
	* 
	*/
	@Column(name = "rev_")
	private Integer rev;
	/**
	* 
	*/
	@Column(name = "execution_id_")
	private String executionId;
	/**
	* 
	*/
	@Column(name = "proc_inst_id_")
	private String procInstId;
	/**
	* 
	*/
	@Column(name = "proc_def_id_")
	private String procDefId;
	/**
	* 
	*/
	@Column(name = "name_")
	private String name;
	/**
	* 
	*/
	@Column(name = "parent_task_id_")
	private String parentTaskId;
	/**
	* 
	*/
	@Column(name = "description_")
	private String description;
	/**
	* 
	*/
	@Column(name = "task_def_key_")
	private String taskDefKey;
	/**
	* 
	*/
	@Column(name = "owner_")
	private String owner;
	/**
	* 
	*/
	@Column(name = "assignee_")
	private String assignee;
	/**
	* 
	*/
	@Column(name = "delegation_")
	private String delegation;
	/**
	* 
	*/
	@Column(name = "priority_")
	private Integer priority;
	/**
	* 
	*/
	@Column(name = "create_time_")
	private java.util.Date createTime;
	/**
	* 
	*/
	@Column(name = "due_date_")
	private java.util.Date dueDate;
	/**
	* 
	*/
	@Column(name = "category_")
	private String category;
	/**
	* 
	*/
	@Column(name = "suspension_state_")
	private Integer suspensionState;
	/**
	* 
	*/
	@Column(name = "tenant_id_")
	private String tenantId;
	/**
	* 
	*/
	@Column(name = "form_key_")
	private String formKey;
	/**
	* 获取 
	*/ 
	public String getId() {
		return id;
	}
	/**
	* 设置
	*/
	public void setId(String id) {
		this.id = id;
	}
	/**
	* 获取 
	*/ 
	public Integer getRev() {
		return rev;
	}
	/**
	* 设置
	*/
	public void setRev(Integer rev) {
		this.rev = rev;
	}
	/**
	* 获取 
	*/ 
	public String getExecutionId() {
		return executionId;
	}
	/**
	* 设置
	*/
	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}
	/**
	* 获取 
	*/ 
	public String getProcInstId() {
		return procInstId;
	}
	/**
	* 设置
	*/
	public void setProcInstId(String procInstId) {
		this.procInstId = procInstId;
	}
	/**
	* 获取 
	*/ 
	public String getProcDefId() {
		return procDefId;
	}
	/**
	* 设置
	*/
	public void setProcDefId(String procDefId) {
		this.procDefId = procDefId;
	}
	/**
	* 获取 
	*/ 
	public String getName() {
		return name;
	}
	/**
	* 设置
	*/
	public void setName(String name) {
		this.name = name;
	}
	/**
	* 获取 
	*/ 
	public String getParentTaskId() {
		return parentTaskId;
	}
	/**
	* 设置
	*/
	public void setParentTaskId(String parentTaskId) {
		this.parentTaskId = parentTaskId;
	}
	/**
	* 获取 
	*/ 
	public String getDescription() {
		return description;
	}
	/**
	* 设置
	*/
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	* 获取 
	*/ 
	public String getTaskDefKey() {
		return taskDefKey;
	}
	/**
	* 设置
	*/
	public void setTaskDefKey(String taskDefKey) {
		this.taskDefKey = taskDefKey;
	}
	/**
	* 获取 
	*/ 
	public String getOwner() {
		return owner;
	}
	/**
	* 设置
	*/
	public void setOwner(String owner) {
		this.owner = owner;
	}
	/**
	* 获取 
	*/ 
	public String getAssignee() {
		return assignee;
	}
	/**
	* 设置
	*/
	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}
	/**
	* 获取 
	*/ 
	public String getDelegation() {
		return delegation;
	}
	/**
	* 设置
	*/
	public void setDelegation(String delegation) {
		this.delegation = delegation;
	}
	/**
	* 获取 
	*/ 
	public Integer getPriority() {
		return priority;
	}
	/**
	* 设置
	*/
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getCreateTime() {
		return createTime;
	}
	/**
	* 设置
	*/
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getDueDate() {
		return dueDate;
	}
	/**
	* 设置
	*/
	public void setDueDate(java.util.Date dueDate) {
		this.dueDate = dueDate;
	}
	/**
	* 获取 
	*/ 
	public String getCategory() {
		return category;
	}
	/**
	* 设置
	*/
	public void setCategory(String category) {
		this.category = category;
	}
	/**
	* 获取 
	*/ 
	public Integer getSuspensionState() {
		return suspensionState;
	}
	/**
	* 设置
	*/
	public void setSuspensionState(Integer suspensionState) {
		this.suspensionState = suspensionState;
	}
	/**
	* 获取 
	*/ 
	public String getTenantId() {
		return tenantId;
	}
	/**
	* 设置
	*/
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	/**
	* 获取 
	*/ 
	public String getFormKey() {
		return formKey;
	}
	/**
	* 设置
	*/
	public void setFormKey(String formKey) {
		this.formKey = formKey;
	}
}

