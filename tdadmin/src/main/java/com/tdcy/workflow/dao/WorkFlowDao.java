package com.tdcy.workflow.dao;

import org.activiti.engine.task.Task;

import com.tdcy.framework.bean.PageInfo;
import com.tdcy.framework.dao.IDao;

public interface WorkFlowDao extends IDao {

	/**
	 * 获取待办任务列表
	 * 
	 * @param userId
	 * @param page
	 * @param limit
	 * @return
	 */
	public PageInfo<Task> getTodoTaskList(Integer userId, int page, int limit);

	/**
	 * 获取参与的任务列表
	 * 
	 * @param userId
	 * @param page
	 * @param limit
	 * @return
	 */
	public PageInfo<Task> getPartTaskList(Integer userId, int page, int limit);

}
