package com.tdcy.workflow.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;
/**
* @Description EO类
*/
@Table(name = "act_ru_event_subscr")
public class RuEventSubscrEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "id_")
	private String id;
	/**
	* 
	*/
	@Column(name = "rev_")
	private Integer rev;
	/**
	* 
	*/
	@Column(name = "event_type_")
	private String eventType;
	/**
	* 
	*/
	@Column(name = "event_name_")
	private String eventName;
	/**
	* 
	*/
	@Column(name = "execution_id_")
	private String executionId;
	/**
	* 
	*/
	@Column(name = "proc_inst_id_")
	private String procInstId;
	/**
	* 
	*/
	@Column(name = "activity_id_")
	private String activityId;
	/**
	* 
	*/
	@Column(name = "configuration_")
	private String configuration;
	/**
	* 
	*/
	@Column(name = "created_")
	private java.util.Date created;
	/**
	* 
	*/
	@Column(name = "proc_def_id_")
	private String procDefId;
	/**
	* 
	*/
	@Column(name = "tenant_id_")
	private String tenantId;
	/**
	* 获取 
	*/ 
	public String getId() {
		return id;
	}
	/**
	* 设置
	*/
	public void setId(String id) {
		this.id = id;
	}
	/**
	* 获取 
	*/ 
	public Integer getRev() {
		return rev;
	}
	/**
	* 设置
	*/
	public void setRev(Integer rev) {
		this.rev = rev;
	}
	/**
	* 获取 
	*/ 
	public String getEventType() {
		return eventType;
	}
	/**
	* 设置
	*/
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	/**
	* 获取 
	*/ 
	public String getEventName() {
		return eventName;
	}
	/**
	* 设置
	*/
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	/**
	* 获取 
	*/ 
	public String getExecutionId() {
		return executionId;
	}
	/**
	* 设置
	*/
	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}
	/**
	* 获取 
	*/ 
	public String getProcInstId() {
		return procInstId;
	}
	/**
	* 设置
	*/
	public void setProcInstId(String procInstId) {
		this.procInstId = procInstId;
	}
	/**
	* 获取 
	*/ 
	public String getActivityId() {
		return activityId;
	}
	/**
	* 设置
	*/
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	/**
	* 获取 
	*/ 
	public String getConfiguration() {
		return configuration;
	}
	/**
	* 设置
	*/
	public void setConfiguration(String configuration) {
		this.configuration = configuration;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getCreated() {
		return created;
	}
	/**
	* 设置
	*/
	public void setCreated(java.util.Date created) {
		this.created = created;
	}
	/**
	* 获取 
	*/ 
	public String getProcDefId() {
		return procDefId;
	}
	/**
	* 设置
	*/
	public void setProcDefId(String procDefId) {
		this.procDefId = procDefId;
	}
	/**
	* 获取 
	*/ 
	public String getTenantId() {
		return tenantId;
	}
	/**
	* 设置
	*/
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
}

