package com.tdcy.workflow.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;
/**
* @Description EO类
*/
@Table(name = "act_id_info")
public class IdInfoEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "id_")
	private String id;
	/**
	* 
	*/
	@Column(name = "rev_")
	private Integer rev;
	/**
	* 
	*/
	@Column(name = "user_id_")
	private String userId;
	/**
	* 
	*/
	@Column(name = "type_")
	private String type;
	/**
	* 
	*/
	@Column(name = "key_")
	private String key;
	/**
	* 
	*/
	@Column(name = "value_")
	private String value;
	/**
	* 
	*/
	@Column(name = "password_")
	private byte[] password;
	/**
	* 
	*/
	@Column(name = "parent_id_")
	private String parentId;
	/**
	* 获取 
	*/ 
	public String getId() {
		return id;
	}
	/**
	* 设置
	*/
	public void setId(String id) {
		this.id = id;
	}
	/**
	* 获取 
	*/ 
	public Integer getRev() {
		return rev;
	}
	/**
	* 设置
	*/
	public void setRev(Integer rev) {
		this.rev = rev;
	}
	/**
	* 获取 
	*/ 
	public String getUserId() {
		return userId;
	}
	/**
	* 设置
	*/
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	* 获取 
	*/ 
	public String getType() {
		return type;
	}
	/**
	* 设置
	*/
	public void setType(String type) {
		this.type = type;
	}
	/**
	* 获取 
	*/ 
	public String getKey() {
		return key;
	}
	/**
	* 设置
	*/
	public void setKey(String key) {
		this.key = key;
	}
	/**
	* 获取 
	*/ 
	public String getValue() {
		return value;
	}
	/**
	* 设置
	*/
	public void setValue(String value) {
		this.value = value;
	}
	/**
	* 获取 
	*/ 
	public byte[] getPassword() {
		return password;
	}
	/**
	* 设置
	*/
	public void setPassword(byte[] password) {
		this.password = password;
	}
	/**
	* 获取 
	*/ 
	public String getParentId() {
		return parentId;
	}
	/**
	* 设置
	*/
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
}

