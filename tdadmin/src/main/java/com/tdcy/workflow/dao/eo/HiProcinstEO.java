package com.tdcy.workflow.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;
/**
* @Description EO类
*/
@Table(name = "act_hi_procinst")
public class HiProcinstEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "id_")
	private String id;
	/**
	* 
	*/
	@Column(name = "proc_inst_id_")
	private String procInstId;
	/**
	* 
	*/
	@Column(name = "business_key_")
	private String businessKey;
	/**
	* 
	*/
	@Column(name = "proc_def_id_")
	private String procDefId;
	/**
	* 
	*/
	@Column(name = "start_time_")
	private java.util.Date startTime;
	/**
	* 
	*/
	@Column(name = "end_time_")
	private java.util.Date endTime;
	/**
	* 
	*/
	@Column(name = "duration_")
	private Integer duration;
	/**
	* 
	*/
	@Column(name = "start_user_id_")
	private String startUserId;
	/**
	* 
	*/
	@Column(name = "start_act_id_")
	private String startActId;
	/**
	* 
	*/
	@Column(name = "end_act_id_")
	private String endActId;
	/**
	* 
	*/
	@Column(name = "super_process_instance_id_")
	private String superProcessInstanceId;
	/**
	* 
	*/
	@Column(name = "delete_reason_")
	private String deleteReason;
	/**
	* 
	*/
	@Column(name = "tenant_id_")
	private String tenantId;
	/**
	* 
	*/
	@Column(name = "name_")
	private String name;
	/**
	* 获取 
	*/ 
	public String getId() {
		return id;
	}
	/**
	* 设置
	*/
	public void setId(String id) {
		this.id = id;
	}
	/**
	* 获取 
	*/ 
	public String getProcInstId() {
		return procInstId;
	}
	/**
	* 设置
	*/
	public void setProcInstId(String procInstId) {
		this.procInstId = procInstId;
	}
	/**
	* 获取 
	*/ 
	public String getBusinessKey() {
		return businessKey;
	}
	/**
	* 设置
	*/
	public void setBusinessKey(String businessKey) {
		this.businessKey = businessKey;
	}
	/**
	* 获取 
	*/ 
	public String getProcDefId() {
		return procDefId;
	}
	/**
	* 设置
	*/
	public void setProcDefId(String procDefId) {
		this.procDefId = procDefId;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getStartTime() {
		return startTime;
	}
	/**
	* 设置
	*/
	public void setStartTime(java.util.Date startTime) {
		this.startTime = startTime;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getEndTime() {
		return endTime;
	}
	/**
	* 设置
	*/
	public void setEndTime(java.util.Date endTime) {
		this.endTime = endTime;
	}
	/**
	* 获取 
	*/ 
	public Integer getDuration() {
		return duration;
	}
	/**
	* 设置
	*/
	public void setDuration(Integer duration) {
		this.duration = duration;
	}
	/**
	* 获取 
	*/ 
	public String getStartUserId() {
		return startUserId;
	}
	/**
	* 设置
	*/
	public void setStartUserId(String startUserId) {
		this.startUserId = startUserId;
	}
	/**
	* 获取 
	*/ 
	public String getStartActId() {
		return startActId;
	}
	/**
	* 设置
	*/
	public void setStartActId(String startActId) {
		this.startActId = startActId;
	}
	/**
	* 获取 
	*/ 
	public String getEndActId() {
		return endActId;
	}
	/**
	* 设置
	*/
	public void setEndActId(String endActId) {
		this.endActId = endActId;
	}
	/**
	* 获取 
	*/ 
	public String getSuperProcessInstanceId() {
		return superProcessInstanceId;
	}
	/**
	* 设置
	*/
	public void setSuperProcessInstanceId(String superProcessInstanceId) {
		this.superProcessInstanceId = superProcessInstanceId;
	}
	/**
	* 获取 
	*/ 
	public String getDeleteReason() {
		return deleteReason;
	}
	/**
	* 设置
	*/
	public void setDeleteReason(String deleteReason) {
		this.deleteReason = deleteReason;
	}
	/**
	* 获取 
	*/ 
	public String getTenantId() {
		return tenantId;
	}
	/**
	* 设置
	*/
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	/**
	* 获取 
	*/ 
	public String getName() {
		return name;
	}
	/**
	* 设置
	*/
	public void setName(String name) {
		this.name = name;
	}
}

