package com.tdcy.workflow.dao.impl;

import org.activiti.engine.task.Task;
import org.springframework.stereotype.Service;

import com.tdcy.framework.bean.PageInfo;
import com.tdcy.framework.dao.DaoImpl;
import com.tdcy.workflow.dao.WorkFlowDao;

@Service
public class WorkFlowDaoImpl extends DaoImpl implements WorkFlowDao {

	@Override
	public PageInfo<Task> getTodoTaskList(Integer userId, int page, int limit) {
		return null;
	}

	@Override
	public PageInfo<Task> getPartTaskList(Integer userId, int page, int limit) {
		return null;
	}

}
