package com.tdcy.workflow.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;
/**
* @Description EO类
*/
@Table(name = "act_ru_identitylink")
public class RuIdentitylinkEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "id_")
	private String id;
	/**
	* 
	*/
	@Column(name = "rev_")
	private Integer rev;
	/**
	* 
	*/
	@Column(name = "group_id_")
	private String groupId;
	/**
	* 
	*/
	@Column(name = "type_")
	private String type;
	/**
	* 
	*/
	@Column(name = "user_id_")
	private String userId;
	/**
	* 
	*/
	@Column(name = "task_id_")
	private String taskId;
	/**
	* 
	*/
	@Column(name = "proc_inst_id_")
	private String procInstId;
	/**
	* 
	*/
	@Column(name = "proc_def_id_")
	private String procDefId;
	/**
	* 获取 
	*/ 
	public String getId() {
		return id;
	}
	/**
	* 设置
	*/
	public void setId(String id) {
		this.id = id;
	}
	/**
	* 获取 
	*/ 
	public Integer getRev() {
		return rev;
	}
	/**
	* 设置
	*/
	public void setRev(Integer rev) {
		this.rev = rev;
	}
	/**
	* 获取 
	*/ 
	public String getGroupId() {
		return groupId;
	}
	/**
	* 设置
	*/
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	/**
	* 获取 
	*/ 
	public String getType() {
		return type;
	}
	/**
	* 设置
	*/
	public void setType(String type) {
		this.type = type;
	}
	/**
	* 获取 
	*/ 
	public String getUserId() {
		return userId;
	}
	/**
	* 设置
	*/
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	* 获取 
	*/ 
	public String getTaskId() {
		return taskId;
	}
	/**
	* 设置
	*/
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	/**
	* 获取 
	*/ 
	public String getProcInstId() {
		return procInstId;
	}
	/**
	* 设置
	*/
	public void setProcInstId(String procInstId) {
		this.procInstId = procInstId;
	}
	/**
	* 获取 
	*/ 
	public String getProcDefId() {
		return procDefId;
	}
	/**
	* 设置
	*/
	public void setProcDefId(String procDefId) {
		this.procDefId = procDefId;
	}
}

