package com.tdcy.workflow.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;
/**
* @Description EO类
*/
@Table(name = "act_ru_job")
public class RuJobEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "id_")
	private String id;
	/**
	* 
	*/
	@Column(name = "rev_")
	private Integer rev;
	/**
	* 
	*/
	@Column(name = "type_")
	private String type;
	/**
	* 
	*/
	@Column(name = "lock_exp_time_")
	private java.util.Date lockExpTime;
	/**
	* 
	*/
	@Column(name = "lock_owner_")
	private String lockOwner;
	/**
	* 
	*/
	@Column(name = "exclusive_")
	private Integer exclusive;
	/**
	* 
	*/
	@Column(name = "execution_id_")
	private String executionId;
	/**
	* 
	*/
	@Column(name = "process_instance_id_")
	private String processInstanceId;
	/**
	* 
	*/
	@Column(name = "proc_def_id_")
	private String procDefId;
	/**
	* 
	*/
	@Column(name = "retries_")
	private Integer retries;
	/**
	* 
	*/
	@Column(name = "exception_stack_id_")
	private String exceptionStackId;
	/**
	* 
	*/
	@Column(name = "exception_msg_")
	private String exceptionMsg;
	/**
	* 
	*/
	@Column(name = "duedate_")
	private java.util.Date duedate;
	/**
	* 
	*/
	@Column(name = "repeat_")
	private String repeat;
	/**
	* 
	*/
	@Column(name = "handler_type_")
	private String handlerType;
	/**
	* 
	*/
	@Column(name = "handler_cfg_")
	private String handlerCfg;
	/**
	* 
	*/
	@Column(name = "tenant_id_")
	private String tenantId;
	/**
	* 获取 
	*/ 
	public String getId() {
		return id;
	}
	/**
	* 设置
	*/
	public void setId(String id) {
		this.id = id;
	}
	/**
	* 获取 
	*/ 
	public Integer getRev() {
		return rev;
	}
	/**
	* 设置
	*/
	public void setRev(Integer rev) {
		this.rev = rev;
	}
	/**
	* 获取 
	*/ 
	public String getType() {
		return type;
	}
	/**
	* 设置
	*/
	public void setType(String type) {
		this.type = type;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getLockExpTime() {
		return lockExpTime;
	}
	/**
	* 设置
	*/
	public void setLockExpTime(java.util.Date lockExpTime) {
		this.lockExpTime = lockExpTime;
	}
	/**
	* 获取 
	*/ 
	public String getLockOwner() {
		return lockOwner;
	}
	/**
	* 设置
	*/
	public void setLockOwner(String lockOwner) {
		this.lockOwner = lockOwner;
	}
	/**
	* 获取 
	*/ 
	public Integer getExclusive() {
		return exclusive;
	}
	/**
	* 设置
	*/
	public void setExclusive(Integer exclusive) {
		this.exclusive = exclusive;
	}
	/**
	* 获取 
	*/ 
	public String getExecutionId() {
		return executionId;
	}
	/**
	* 设置
	*/
	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}
	/**
	* 获取 
	*/ 
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	/**
	* 设置
	*/
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	/**
	* 获取 
	*/ 
	public String getProcDefId() {
		return procDefId;
	}
	/**
	* 设置
	*/
	public void setProcDefId(String procDefId) {
		this.procDefId = procDefId;
	}
	/**
	* 获取 
	*/ 
	public Integer getRetries() {
		return retries;
	}
	/**
	* 设置
	*/
	public void setRetries(Integer retries) {
		this.retries = retries;
	}
	/**
	* 获取 
	*/ 
	public String getExceptionStackId() {
		return exceptionStackId;
	}
	/**
	* 设置
	*/
	public void setExceptionStackId(String exceptionStackId) {
		this.exceptionStackId = exceptionStackId;
	}
	/**
	* 获取 
	*/ 
	public String getExceptionMsg() {
		return exceptionMsg;
	}
	/**
	* 设置
	*/
	public void setExceptionMsg(String exceptionMsg) {
		this.exceptionMsg = exceptionMsg;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getDuedate() {
		return duedate;
	}
	/**
	* 设置
	*/
	public void setDuedate(java.util.Date duedate) {
		this.duedate = duedate;
	}
	/**
	* 获取 
	*/ 
	public String getRepeat() {
		return repeat;
	}
	/**
	* 设置
	*/
	public void setRepeat(String repeat) {
		this.repeat = repeat;
	}
	/**
	* 获取 
	*/ 
	public String getHandlerType() {
		return handlerType;
	}
	/**
	* 设置
	*/
	public void setHandlerType(String handlerType) {
		this.handlerType = handlerType;
	}
	/**
	* 获取 
	*/ 
	public String getHandlerCfg() {
		return handlerCfg;
	}
	/**
	* 设置
	*/
	public void setHandlerCfg(String handlerCfg) {
		this.handlerCfg = handlerCfg;
	}
	/**
	* 获取 
	*/ 
	public String getTenantId() {
		return tenantId;
	}
	/**
	* 设置
	*/
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
}

