package com.tdcy.workflow.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;
/**
* @Description EO类
*/
@Table(name = "act_hi_comment")
public class HiCommentEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "id_")
	private String id;
	/**
	* 
	*/
	@Column(name = "type_")
	private String type;
	/**
	* 
	*/
	@Column(name = "time_")
	private java.util.Date time;
	/**
	* 
	*/
	@Column(name = "user_id_")
	private String userId;
	/**
	* 
	*/
	@Column(name = "task_id_")
	private String taskId;
	/**
	* 
	*/
	@Column(name = "proc_inst_id_")
	private String procInstId;
	/**
	* 
	*/
	@Column(name = "action_")
	private String action;
	/**
	* 
	*/
	@Column(name = "message_")
	private String message;
	/**
	* 
	*/
	@Column(name = "full_msg_")
	private byte[] fullMsg;
	/**
	* 获取 
	*/ 
	public String getId() {
		return id;
	}
	/**
	* 设置
	*/
	public void setId(String id) {
		this.id = id;
	}
	/**
	* 获取 
	*/ 
	public String getType() {
		return type;
	}
	/**
	* 设置
	*/
	public void setType(String type) {
		this.type = type;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getTime() {
		return time;
	}
	/**
	* 设置
	*/
	public void setTime(java.util.Date time) {
		this.time = time;
	}
	/**
	* 获取 
	*/ 
	public String getUserId() {
		return userId;
	}
	/**
	* 设置
	*/
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	* 获取 
	*/ 
	public String getTaskId() {
		return taskId;
	}
	/**
	* 设置
	*/
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	/**
	* 获取 
	*/ 
	public String getProcInstId() {
		return procInstId;
	}
	/**
	* 设置
	*/
	public void setProcInstId(String procInstId) {
		this.procInstId = procInstId;
	}
	/**
	* 获取 
	*/ 
	public String getAction() {
		return action;
	}
	/**
	* 设置
	*/
	public void setAction(String action) {
		this.action = action;
	}
	/**
	* 获取 
	*/ 
	public String getMessage() {
		return message;
	}
	/**
	* 设置
	*/
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	* 获取 
	*/ 
	public byte[] getFullMsg() {
		return fullMsg;
	}
	/**
	* 设置
	*/
	public void setFullMsg(byte[] fullMsg) {
		this.fullMsg = fullMsg;
	}
}

