package com.tdcy.workflow.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;
/**
* @Description EO类
*/
@Table(name = "act_hi_taskinst")
public class HiTaskinstEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "id_")
	private String id;
	/**
	* 
	*/
	@Column(name = "proc_def_id_")
	private String procDefId;
	/**
	* 
	*/
	@Column(name = "task_def_key_")
	private String taskDefKey;
	/**
	* 
	*/
	@Column(name = "proc_inst_id_")
	private String procInstId;
	/**
	* 
	*/
	@Column(name = "execution_id_")
	private String executionId;
	/**
	* 
	*/
	@Column(name = "name_")
	private String name;
	/**
	* 
	*/
	@Column(name = "parent_task_id_")
	private String parentTaskId;
	/**
	* 
	*/
	@Column(name = "description_")
	private String description;
	/**
	* 
	*/
	@Column(name = "owner_")
	private String owner;
	/**
	* 
	*/
	@Column(name = "assignee_")
	private String assignee;
	/**
	* 
	*/
	@Column(name = "start_time_")
	private java.util.Date startTime;
	/**
	* 
	*/
	@Column(name = "claim_time_")
	private java.util.Date claimTime;
	/**
	* 
	*/
	@Column(name = "end_time_")
	private java.util.Date endTime;
	/**
	* 
	*/
	@Column(name = "duration_")
	private Integer duration;
	/**
	* 
	*/
	@Column(name = "delete_reason_")
	private String deleteReason;
	/**
	* 
	*/
	@Column(name = "priority_")
	private Integer priority;
	/**
	* 
	*/
	@Column(name = "due_date_")
	private java.util.Date dueDate;
	/**
	* 
	*/
	@Column(name = "form_key_")
	private String formKey;
	/**
	* 
	*/
	@Column(name = "category_")
	private String category;
	/**
	* 
	*/
	@Column(name = "tenant_id_")
	private String tenantId;
	/**
	* 获取 
	*/ 
	public String getId() {
		return id;
	}
	/**
	* 设置
	*/
	public void setId(String id) {
		this.id = id;
	}
	/**
	* 获取 
	*/ 
	public String getProcDefId() {
		return procDefId;
	}
	/**
	* 设置
	*/
	public void setProcDefId(String procDefId) {
		this.procDefId = procDefId;
	}
	/**
	* 获取 
	*/ 
	public String getTaskDefKey() {
		return taskDefKey;
	}
	/**
	* 设置
	*/
	public void setTaskDefKey(String taskDefKey) {
		this.taskDefKey = taskDefKey;
	}
	/**
	* 获取 
	*/ 
	public String getProcInstId() {
		return procInstId;
	}
	/**
	* 设置
	*/
	public void setProcInstId(String procInstId) {
		this.procInstId = procInstId;
	}
	/**
	* 获取 
	*/ 
	public String getExecutionId() {
		return executionId;
	}
	/**
	* 设置
	*/
	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}
	/**
	* 获取 
	*/ 
	public String getName() {
		return name;
	}
	/**
	* 设置
	*/
	public void setName(String name) {
		this.name = name;
	}
	/**
	* 获取 
	*/ 
	public String getParentTaskId() {
		return parentTaskId;
	}
	/**
	* 设置
	*/
	public void setParentTaskId(String parentTaskId) {
		this.parentTaskId = parentTaskId;
	}
	/**
	* 获取 
	*/ 
	public String getDescription() {
		return description;
	}
	/**
	* 设置
	*/
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	* 获取 
	*/ 
	public String getOwner() {
		return owner;
	}
	/**
	* 设置
	*/
	public void setOwner(String owner) {
		this.owner = owner;
	}
	/**
	* 获取 
	*/ 
	public String getAssignee() {
		return assignee;
	}
	/**
	* 设置
	*/
	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getStartTime() {
		return startTime;
	}
	/**
	* 设置
	*/
	public void setStartTime(java.util.Date startTime) {
		this.startTime = startTime;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getClaimTime() {
		return claimTime;
	}
	/**
	* 设置
	*/
	public void setClaimTime(java.util.Date claimTime) {
		this.claimTime = claimTime;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getEndTime() {
		return endTime;
	}
	/**
	* 设置
	*/
	public void setEndTime(java.util.Date endTime) {
		this.endTime = endTime;
	}
	/**
	* 获取 
	*/ 
	public Integer getDuration() {
		return duration;
	}
	/**
	* 设置
	*/
	public void setDuration(Integer duration) {
		this.duration = duration;
	}
	/**
	* 获取 
	*/ 
	public String getDeleteReason() {
		return deleteReason;
	}
	/**
	* 设置
	*/
	public void setDeleteReason(String deleteReason) {
		this.deleteReason = deleteReason;
	}
	/**
	* 获取 
	*/ 
	public Integer getPriority() {
		return priority;
	}
	/**
	* 设置
	*/
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getDueDate() {
		return dueDate;
	}
	/**
	* 设置
	*/
	public void setDueDate(java.util.Date dueDate) {
		this.dueDate = dueDate;
	}
	/**
	* 获取 
	*/ 
	public String getFormKey() {
		return formKey;
	}
	/**
	* 设置
	*/
	public void setFormKey(String formKey) {
		this.formKey = formKey;
	}
	/**
	* 获取 
	*/ 
	public String getCategory() {
		return category;
	}
	/**
	* 设置
	*/
	public void setCategory(String category) {
		this.category = category;
	}
	/**
	* 获取 
	*/ 
	public String getTenantId() {
		return tenantId;
	}
	/**
	* 设置
	*/
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
}

