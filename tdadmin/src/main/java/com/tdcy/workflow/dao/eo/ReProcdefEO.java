package com.tdcy.workflow.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;
/**
* @Description EO类
*/
@Table(name = "act_re_procdef")
public class ReProcdefEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "id_")
	private String id;
	/**
	* 
	*/
	@Column(name = "rev_")
	private Integer rev;
	/**
	* 
	*/
	@Column(name = "category_")
	private String category;
	/**
	* 
	*/
	@Column(name = "name_")
	private String name;
	/**
	* 
	*/
	@Column(name = "key_")
	private String key;
	/**
	* 
	*/
	@Column(name = "version_")
	private Integer version;
	/**
	* 
	*/
	@Column(name = "deployment_id_")
	private String deploymentId;
	/**
	* 
	*/
	@Column(name = "resource_name_")
	private String resourceName;
	/**
	* 
	*/
	@Column(name = "dgrm_resource_name_")
	private String dgrmResourceName;
	/**
	* 
	*/
	@Column(name = "description_")
	private String description;
	/**
	* 
	*/
	@Column(name = "has_start_form_key_")
	private Integer hasStartFormKey;
	/**
	* 
	*/
	@Column(name = "has_graphical_notation_")
	private Integer hasGraphicalNotation;
	/**
	* 
	*/
	@Column(name = "suspension_state_")
	private Integer suspensionState;
	/**
	* 
	*/
	@Column(name = "tenant_id_")
	private String tenantId;
	/**
	* 获取 
	*/ 
	public String getId() {
		return id;
	}
	/**
	* 设置
	*/
	public void setId(String id) {
		this.id = id;
	}
	/**
	* 获取 
	*/ 
	public Integer getRev() {
		return rev;
	}
	/**
	* 设置
	*/
	public void setRev(Integer rev) {
		this.rev = rev;
	}
	/**
	* 获取 
	*/ 
	public String getCategory() {
		return category;
	}
	/**
	* 设置
	*/
	public void setCategory(String category) {
		this.category = category;
	}
	/**
	* 获取 
	*/ 
	public String getName() {
		return name;
	}
	/**
	* 设置
	*/
	public void setName(String name) {
		this.name = name;
	}
	/**
	* 获取 
	*/ 
	public String getKey() {
		return key;
	}
	/**
	* 设置
	*/
	public void setKey(String key) {
		this.key = key;
	}
	/**
	* 获取 
	*/ 
	public Integer getVersion() {
		return version;
	}
	/**
	* 设置
	*/
	public void setVersion(Integer version) {
		this.version = version;
	}
	/**
	* 获取 
	*/ 
	public String getDeploymentId() {
		return deploymentId;
	}
	/**
	* 设置
	*/
	public void setDeploymentId(String deploymentId) {
		this.deploymentId = deploymentId;
	}
	/**
	* 获取 
	*/ 
	public String getResourceName() {
		return resourceName;
	}
	/**
	* 设置
	*/
	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}
	/**
	* 获取 
	*/ 
	public String getDgrmResourceName() {
		return dgrmResourceName;
	}
	/**
	* 设置
	*/
	public void setDgrmResourceName(String dgrmResourceName) {
		this.dgrmResourceName = dgrmResourceName;
	}
	/**
	* 获取 
	*/ 
	public String getDescription() {
		return description;
	}
	/**
	* 设置
	*/
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	* 获取 
	*/ 
	public Integer getHasStartFormKey() {
		return hasStartFormKey;
	}
	/**
	* 设置
	*/
	public void setHasStartFormKey(Integer hasStartFormKey) {
		this.hasStartFormKey = hasStartFormKey;
	}
	/**
	* 获取 
	*/ 
	public Integer getHasGraphicalNotation() {
		return hasGraphicalNotation;
	}
	/**
	* 设置
	*/
	public void setHasGraphicalNotation(Integer hasGraphicalNotation) {
		this.hasGraphicalNotation = hasGraphicalNotation;
	}
	/**
	* 获取 
	*/ 
	public Integer getSuspensionState() {
		return suspensionState;
	}
	/**
	* 设置
	*/
	public void setSuspensionState(Integer suspensionState) {
		this.suspensionState = suspensionState;
	}
	/**
	* 获取 
	*/ 
	public String getTenantId() {
		return tenantId;
	}
	/**
	* 设置
	*/
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
}

