package com.tdcy.workflow.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;
/**
* @Description EO类
*/
@Table(name = "act_hi_attachment")
public class HiAttachmentEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "id_")
	private String id;
	/**
	* 
	*/
	@Column(name = "rev_")
	private Integer rev;
	/**
	* 
	*/
	@Column(name = "user_id_")
	private String userId;
	/**
	* 
	*/
	@Column(name = "name_")
	private String name;
	/**
	* 
	*/
	@Column(name = "description_")
	private String description;
	/**
	* 
	*/
	@Column(name = "type_")
	private String type;
	/**
	* 
	*/
	@Column(name = "task_id_")
	private String taskId;
	/**
	* 
	*/
	@Column(name = "proc_inst_id_")
	private String procInstId;
	/**
	* 
	*/
	@Column(name = "url_")
	private String url;
	/**
	* 
	*/
	@Column(name = "content_id_")
	private String contentId;
	/**
	* 
	*/
	@Column(name = "time_")
	private java.util.Date time;
	/**
	* 获取 
	*/ 
	public String getId() {
		return id;
	}
	/**
	* 设置
	*/
	public void setId(String id) {
		this.id = id;
	}
	/**
	* 获取 
	*/ 
	public Integer getRev() {
		return rev;
	}
	/**
	* 设置
	*/
	public void setRev(Integer rev) {
		this.rev = rev;
	}
	/**
	* 获取 
	*/ 
	public String getUserId() {
		return userId;
	}
	/**
	* 设置
	*/
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	* 获取 
	*/ 
	public String getName() {
		return name;
	}
	/**
	* 设置
	*/
	public void setName(String name) {
		this.name = name;
	}
	/**
	* 获取 
	*/ 
	public String getDescription() {
		return description;
	}
	/**
	* 设置
	*/
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	* 获取 
	*/ 
	public String getType() {
		return type;
	}
	/**
	* 设置
	*/
	public void setType(String type) {
		this.type = type;
	}
	/**
	* 获取 
	*/ 
	public String getTaskId() {
		return taskId;
	}
	/**
	* 设置
	*/
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	/**
	* 获取 
	*/ 
	public String getProcInstId() {
		return procInstId;
	}
	/**
	* 设置
	*/
	public void setProcInstId(String procInstId) {
		this.procInstId = procInstId;
	}
	/**
	* 获取 
	*/ 
	public String getUrl() {
		return url;
	}
	/**
	* 设置
	*/
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	* 获取 
	*/ 
	public String getContentId() {
		return contentId;
	}
	/**
	* 设置
	*/
	public void setContentId(String contentId) {
		this.contentId = contentId;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getTime() {
		return time;
	}
	/**
	* 设置
	*/
	public void setTime(java.util.Date time) {
		this.time = time;
	}
}

