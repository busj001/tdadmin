package com.tdcy.workflow.dao.eo;

import java.io.Serializable;

import com.tdcy.framework.annotation.Column;
import com.tdcy.framework.annotation.Id;
import com.tdcy.framework.annotation.Table;
/**
* @Description EO类
*/
@Table(name = "act_re_model")
public class ReModelEO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	* 
	*/
	@Id
	@Column(name = "id_")
	private String id;
	/**
	* 
	*/
	@Column(name = "rev_")
	private Integer rev;
	/**
	* 
	*/
	@Column(name = "name_")
	private String name;
	/**
	* 
	*/
	@Column(name = "key_")
	private String key;
	/**
	* 
	*/
	@Column(name = "category_")
	private String category;
	/**
	* 
	*/
	@Column(name = "create_time_")
	private java.util.Date createTime;
	/**
	* 
	*/
	@Column(name = "last_update_time_")
	private java.util.Date lastUpdateTime;
	/**
	* 
	*/
	@Column(name = "version_")
	private Integer version;
	/**
	* 
	*/
	@Column(name = "meta_info_")
	private String metaInfo;
	/**
	* 
	*/
	@Column(name = "deployment_id_")
	private String deploymentId;
	/**
	* 
	*/
	@Column(name = "editor_source_value_id_")
	private String editorSourceValueId;
	/**
	* 
	*/
	@Column(name = "editor_source_extra_value_id_")
	private String editorSourceExtraValueId;
	/**
	* 
	*/
	@Column(name = "tenant_id_")
	private String tenantId;
	/**
	* 获取 
	*/ 
	public String getId() {
		return id;
	}
	/**
	* 设置
	*/
	public void setId(String id) {
		this.id = id;
	}
	/**
	* 获取 
	*/ 
	public Integer getRev() {
		return rev;
	}
	/**
	* 设置
	*/
	public void setRev(Integer rev) {
		this.rev = rev;
	}
	/**
	* 获取 
	*/ 
	public String getName() {
		return name;
	}
	/**
	* 设置
	*/
	public void setName(String name) {
		this.name = name;
	}
	/**
	* 获取 
	*/ 
	public String getKey() {
		return key;
	}
	/**
	* 设置
	*/
	public void setKey(String key) {
		this.key = key;
	}
	/**
	* 获取 
	*/ 
	public String getCategory() {
		return category;
	}
	/**
	* 设置
	*/
	public void setCategory(String category) {
		this.category = category;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getCreateTime() {
		return createTime;
	}
	/**
	* 设置
	*/
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	/**
	* 获取 
	*/ 
	public java.util.Date getLastUpdateTime() {
		return lastUpdateTime;
	}
	/**
	* 设置
	*/
	public void setLastUpdateTime(java.util.Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	/**
	* 获取 
	*/ 
	public Integer getVersion() {
		return version;
	}
	/**
	* 设置
	*/
	public void setVersion(Integer version) {
		this.version = version;
	}
	/**
	* 获取 
	*/ 
	public String getMetaInfo() {
		return metaInfo;
	}
	/**
	* 设置
	*/
	public void setMetaInfo(String metaInfo) {
		this.metaInfo = metaInfo;
	}
	/**
	* 获取 
	*/ 
	public String getDeploymentId() {
		return deploymentId;
	}
	/**
	* 设置
	*/
	public void setDeploymentId(String deploymentId) {
		this.deploymentId = deploymentId;
	}
	/**
	* 获取 
	*/ 
	public String getEditorSourceValueId() {
		return editorSourceValueId;
	}
	/**
	* 设置
	*/
	public void setEditorSourceValueId(String editorSourceValueId) {
		this.editorSourceValueId = editorSourceValueId;
	}
	/**
	* 获取 
	*/ 
	public String getEditorSourceExtraValueId() {
		return editorSourceExtraValueId;
	}
	/**
	* 设置
	*/
	public void setEditorSourceExtraValueId(String editorSourceExtraValueId) {
		this.editorSourceExtraValueId = editorSourceExtraValueId;
	}
	/**
	* 获取 
	*/ 
	public String getTenantId() {
		return tenantId;
	}
	/**
	* 设置
	*/
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
}

