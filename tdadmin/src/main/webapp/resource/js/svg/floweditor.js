var process = {};		// 当前流程
var curLine = null;		// 当前点击的连线
var curIcon = null;		// 当前点击的图元
var popWin = null;       // 当前的弹出窗口
var drawLineStatus = true; // 编辑状态
var half_image_size = 20;			// 工作流图标的大小的一半值
var	half_act_line_width=25;// 连线长度
var	start_left=50+220;// 开始结点左边起始位置
var	start_top=50;// 结点上边位置

// 上一步操作对象
var preOptObj = null;

var icon_path = "/resource/js/floweditor/img/flow/";	// 当前流程编辑器的图片的相对路径
var icon_inst_path = "/resource/js/floweditor/img/instant/";	// 当前流程编辑器的图片的相对路径

var IconNameImg = {"manual":{icon:"manual.gif",name:"人工活动",icon_run:"manual17.gif",icon_nostart:"manual19.gif",icon_complete:"manual18.gif"},
                   "task":{icon:"task.gif",name:"自动活动",icon_run:"task17.gif",icon_nostart:"task19.gif",icon_complete:"task18.gif"},
                   "parallel":{icon:"parallel.gif",name:"并行路由",icon_run:"parallel17.gif",icon_nostart:"parallel19.gif",icon_complete:"parallel18.gif"},
                   "exclusive":{icon:"exclusive.gif",name:"独占路由",icon_run:"exclusive17.gif",icon_nostart:"exclusive19.gif",icon_complete:"exclusive18.gif"},
                   "start":{icon:"start.gif",name:"开始活动",icon_run:"start17.gif",icon_nostart:"start19.gif",icon_complete:"start18.gif"},
                   "end":{icon:"end.gif",name:"结束活动",icon_run:"end17.gif",icon_nostart:"end19.gif",icon_complete:"end18.gif"}} ;                   

/**
 * 流程对象
 */
function Process() {
	this.icons = new Array();	// 所有的图元对象
	this.lines = new Array();	// 所有连线对象
	this.processDefID   = '';			// 每个版本对应一个processDefID
	this.processDefCode   = '';			// 每个版本对应一个processDefID
	this.processDefName = '';			// 流程逻辑名称，它下面会有若干版本
	this.rootPath="";
	this.jsonDef={};
	this.fieldDef = {};
	this.jsonBiz = null;
	this.drawObj = null;
	this.isDisplay = false;
	
	this.init = _init_process;				// 连线初始化方法
	this.graphToJson = graphToJson;				// 图形化展示为jason
	this.jsonToGraph = _json_to_graph;				// json转换为图片
	this.createRaphael = _createRaphaelDiv;
	this.findBizNode = _findBizNode;
	this.findBizLine = _findBizLine
	
	// 初始化流程属性
	function _init_process(proIniObj){
			var		proDefId=proIniObj["proDefId"];
			var		proDefCode=proIniObj["proDefCode"];
			var		proDefName=proIniObj["proDefName"];
			var		rootPath=proIniObj["rootPath"];
			var		jsonDef=proIniObj["jsonDef"];
			var		jsonBiz=proIniObj["jsonBiz"];
			this.isDisplay = proIniObj["isDisplay"];
			this.icons = new Array();	// 所有的图元对象
			this.lines = new Array();	// 所有连线对象
			this.processDefID = proDefId;
			this.processDefCode = proDefCode;
			this.processDefName = proDefName;
			this.rootPath=rootPath;
			this.jsonDef = jsonDef;			// 流程逻辑名称，它下面会有若干版本
			this.jsonBiz = jsonBiz;
			this.fieldDef = proIniObj["fieldDef"];
			
			this.createRaphael();
			if(this.isDisplay){
				this.drawObj = SVG('drawing').move(0,0);
			}else{
				this.drawObj = SVG('drawing').move(200,0);
			}
	}

	// 创建画布div
	function _createRaphaelDiv(){
		var height =  $(window).height();
		var width = $(window).width();
		var tobj = document.getElementById("drawing");
		if(tobj==null){
			var ihtml = "<div id='drawing' style='height:"+height+"px;width:"+width+"px;position:absolute;'>";
			ihtml += "</div>";
			document.body.insertAdjacentHTML("afterBegin",ihtml);
		}
	
	} 
	
	// 将图元转换为jason
	function graphToJson() {
			var jNodes=[];
			var nodeNum=this.icons.length;
			var	rsLines={};
			for(var i=0;i<this.icons.length;i++){
				rsLines=getInOutLines(this.icons[i]);
				this.icons[i].inLines=rsLines["inLines"];
				this.icons[i].outLines=rsLines["outLines"];
				this.icons[i].prevActs=rsLines["prevActs"];
				this.icons[i].nextActs=rsLines["nextActs"];
			}
			makeActsTotal(process,"next");
			makeActsTotal(process,"prev");
			// 清理重复节点
			for(var i=0;i<nodeNum;i++)      {
			  insertObjInArr(jNodes,this.icons[i].toJson());
			}      
			var jLines=[];
			var lineNum=this.lines.length;
			
			var	polyDot=null;
			// 处理连线上的polydot属性
			for(var i=0;i<lineNum;i++){
				polyDot=getPolyDot4Line(this.lines[i].points);
				this.lines[i].polyDot=polyDot;
			}
			
			// 依次将line对象转换为json
			for(var i=0;i<lineNum;i++)      {
			   insertObjInArr(jLines,this.lines[i].toJson());
			}
			var	graphCount=nodeNum+lineNum;
			var json={id:this.processDefID,code:this.processDefCode,name:this.processDefName,count:graphCount,nodes:jNodes,lines:jLines}           
			return JSONDef.encode(json);
	}
	
	// 将jason-Act转换为图元
	function _json_to_graph() {
		var	processDefID=this.processDefID;
		var	processDefName=this.processDefName;
		var	actNodes=this.jsonDef.nodes;
		var iconNum=actNodes.length;
		clear(this);
		// 以下处理图元
		var len = this.icons.length;
		var type='';
		var activityName='';
		var activityId='';
		var x='';
		var y='';
		var	iconIniObj={};
		for(var i=0;i<iconNum;i++)   {
			len = this.icons.length;
			// 创建新的图元
			this.icons[len] = new Icon();
			iconIniObj["rootPath"]=this.rootPath;
			iconIniObj["type"]=actNodes[i].type;
			if(this.isDisplay){
				iconIniObj["x"]=actNodes[i].left-200;
			}else{
				iconIniObj["x"]=actNodes[i].left;
			}
			iconIniObj["y"]=actNodes[i].top;
			iconIniObj["activityName"]=actNodes[i].name;
			iconIniObj["activityId"]=actNodes[i].id;
			this.icons[len].init(iconIniObj,this);
		}   
	   	// 以下处理线条
	   	var jLines=this.jsonDef.lines;
	   	var lineNum=jLines.length;
	   	var line="";
		var linelength = this.lines.length;
		var	iconBegin=null;
		var	iconEnd=null;
		var	lineIniObj={};
	   	for(var i=0;i<lineNum;i++)   {
	   		linelength = this.lines.length;
			this.lines[linelength] = new Line();
			lineId=jLines[i].id;
			iconBegin=null;
			iconEnd=null;
			iconBegin	=getIconById(jLines[i].from,this);
			iconEnd		=getIconById(jLines[i].to,this);
			lineIniObj["id"]=jLines[i].id;
			lineIniObj["name"]=jLines[i].name;
            lineIniObj["condType"]=jLines[i].condType;
			lineIniObj["condition"]=jLines[i].condition;
			lineIniObj["color"]="black";
			lineIniObj["polyDot"]=jLines[i].polyDot;
			this.lines[linelength].init(iconBegin, iconEnd,lineIniObj,this);
	   }
	   	
	   	// 处理业务显示
	   	if(this.jsonBiz){
	   		for(var i=0;i<this.icons.length;i++){
	   			var icon = this.icons[i];
	   			var actId = icon.activityId;
	   			var type = icon.type;
	   			var node = this.findBizNode(actId);
	   			if(node){
	   				var status = node["actStatus"];
	   				if(status == "0"){
	   					icon.img.load(this.rootPath+icon_inst_path + IconNameImg[type].icon_run,40,40);
	   				}else if(status == "1"){
	   					icon.img.load(this.rootPath+icon_inst_path + IconNameImg[type].icon_run,40,40);
	   				}else if(status == "2"){
	   					icon.img.load(this.rootPath+icon_inst_path + IconNameImg[type].icon_complete,40,40);
	   				}
	   				
	   				icon.activityInstId = node["actInstId"];
	   			}else{
	   				icon.img.load(this.rootPath+icon_inst_path + IconNameImg[type].icon_nostart,40,40);
	   			}
	   		}
	   		
	   		for(var i=0;i<this.lines.length;i++){
	   			var line = this.lines[i];
	   			var preId = line.iconBegin.activityInstId;
	   			var nextId = line.iconEnd.activityInstId;
	   			if(preId && nextId){
	   				var findLine = this.findBizLine(preId,nextId);
		   			if(findLine){
		   				var color = "#CA5C2C";
		   				line.polyline.stroke(color);
		   				line.maker.attr({ fill: color });
		   			}
	   			}
	   		}
	   	}
	}
	
	function _findBizNode(activityId){
		var actNodes = this.jsonBiz["actNodes"];
		
		var retNodes ;
		for(var i=0;i<actNodes.length;i++){
   			var actDefId = actNodes[i]["actDefId"] ;
   			if(actDefId == activityId){
   				retNodes =  actNodes[i];
   				break;
   			}
   		}
		return retNodes;
	}
	
	function _findBizLine(preId,nextId){
		var actLines = this.jsonBiz["actLines"];
		
		var retLine ;
		for(var i=0;i<actLines.length;i++){
   			var preActId = actLines[i]["preActInstId"] ;
   			var nextActId = actLines[i]["nextActInstId"] ;
   			if(preActId == preId && nextId==nextActId){
   				retLine =  actLines[i];
   				break;
   			}
   		}
		return retLine;
	}
}

/**
 * 图元对象
 */
function Icon(){
	this.activityId;	// 环节图标在数据库中的主键，对应state的activityId
	this.activityCode;	// 环节图标在数据库中的主键代码
	this.activityInstId; //该图标实例化后的id，对应数据库的activity_inst_id
	this.displayName;	// 环节显示名称，对应state的activityName
	this.activityName;	// 环节显示名称，对应state的activityName
	this.type;			// 类型
	this.inLines="";		// 流入的线
	this.outLines="";		// 流出的线
	this.prevActs="";// 当前节点的直接前驱
	this.nextActs="";// 当前节点的直接后继
	this.prevActsTotal="";// 当前节点的直接前驱--集合，包括前驱的前驱
	this.nextActsTotal="";// 当前节点的直接后继--集合，包括后继的后继
	// 图元的图片对象
	this.img = "";
	// 图元中的文字对象
	this.text = "";
	this.process;			// 类型
	this.beginLine = new Array();	// 以图元开始的连线
	this.endLine = new Array();		// 以图元结束的连线
	this.init = _init_icon;			// 初始化图元方法
	this.createLine = _create_line;	// 创建连线方法
	this.redrawLine = _redraw_line;	// 重新画与图元连接的连线方法
	this.moveText = _move_text;		// 托拽图元后移动文字对象方法
	this.setDisplayName = _set_displayName; // 设置显示名称
	this.toJson=_to_jSon;
	this.jsonTo=_jSon_to;
	
	function _init_icon(iconInitObj,parentProcess){
		var	rootPath=iconInitObj["rootPath"];
		var	type=iconInitObj["type"]; 
		var	x=iconInitObj["x"]; 
		var	y=iconInitObj["y"]; 
		var	activityName=iconInitObj["activityName"]; 
		var	activityId=iconInitObj["activityId"];
		var	activityCode=iconInitObj["activityCode"];
		// 初始化图元
		this.type = type;
		this.activityId = activityId;
		this.activityCode = activityCode;
		this.id = activityId;
		this.activityName =activityName;
		this.displayName ="";
		if (activityName != null){
			this.displayName = activityName;
		}
		
		this.process=parentProcess;
		this.img =  this.process.drawObj.image(rootPath+icon_path + IconNameImg[type].icon,40,40)
		this.img.move(x,y);
		this.img.icon = this; 
		
		this.text= this.process.drawObj.text(this.displayName);
		this.text.icon  = this;
		this.moveText();
		this.img.draggable({"enable":false});
		
		if(!this.process.isDisplay){
			// 定义图片对象的开始托拽事件
			this.img.dragstart = function(delta,event){
				event = event || window.event;
				
				// $("#ttt").val(delta.x+"
				// ,"+this.x()+","+delta.y+","+this.y()+","+event.pageX+"\r\n");
				
				drag_icon_x=event.pageX;
				drag_icon_y=event.pageY;
			};
			
			// 定义图片对象的托拽事件
			this.img.dragmove  = function(delta,event){
				// $("#ttt").val("aaa");
				event = event || window.event;
				
				if (drawLineStatus && this.icon.type == "end"){
					return ;
				}
				if (!drawLineStatus) {
					this.x(event.pageX);
					this.y(event.pageY);
				}
			};
			
			// 定义图片对象的托拽结束
			this.img.dragend  = function(delta,event){
				event = event || window.event;
				// $("#ttt").val($("#ttt").val()+delta.x+"
				// ,"+this.x()+","+delta.y+","+this.y()+","+event.pageX+"\r\n");
				if (drawLineStatus){
					// 当在图元上方释放鼠标的时候，在两个节点之间画线
					this.icon.createLine(this, event.pageX, event.pageY);
				}
				// 可以拖动时，拖动图标后和图标连接的线需要重新绘制
				else  {
					this.x(event.pageX-20);
					this.y(event.pageY-20);
					this.icon.redrawLine();
				}
			};
			
			// 定义图片对象的右键菜单，并且将此图元对应的一些相关信息传给右键菜单对应的方法
			this.img.on("dblclick",function(event) {
				curIcon = this.icon;
				showMenu('icon', event);
				return false;
			});
		}
	};
	
	function _create_line(obj, x, y) {
		// 查找线结束的图元
		var iconEnd = findIcon(x, y,this.process);
		if (iconEnd != null && iconEnd != this) {
			var linelength = this.process.lines.length;
			var	lineIniObj={};
			for(var i =0;i<linelength;i++){
				if(this.process.lines[i].iconBegin==this && this.process.lines[i].iconEnd==iconEnd){
					AppKit.showMessage("两个节点之间，同一方向只能有一条线");
					return;
				}
			}
			var	lineMax=getLineMaxCount(this.process);
			this.process.lines[linelength] = new Line();
			lineIniObj["id"]="line_"+(lineMax+1);
			lineIniObj["name"]="";
			lineIniObj["condType"]="normal";
			lineIniObj["condition"]="";
			lineIniObj["color"]="black";
			this.process.lines[linelength].init(this, iconEnd,lineIniObj,this.process);
		}
	}

	function _redraw_line() {
		var i=0;
		for (i=0; i< this.beginLine.length;i++)
			this.beginLine[i].redraw();
		for (i=0; i< this.endLine.length; i++)
			this.endLine[i].redraw();
		this.moveText();
	}
	
	function _move_text() {
		this.text.x(this.img.x() + (half_image_size - charLength(this.displayName) * 5)+20)
		this.text.y(this.img.y() + half_image_size * 2 + 2);
		this.text.width(charLength(this.displayName) * 10);
	}
	
	function _set_displayName(value) {
		this.displayName = value;
		this.text.text(value);
	}

	function _to_jSon(){
		var	i_left=parseInt(this.img.x());
		var	i_top=parseInt(this.img.y());
		var json=   {
		   id:this.activityId,
		   code:this.activityCode,
		   name:this.activityName,
		   type:this.type,
		   shape:this.type,
		   inLines:this.inLines,
		   outLines:this.outLines,
		   prevActs:this.prevActs,
		   nextActs:this.nextActs,
		   prevActsTotal:this.prevActsTotal,
		   nextActsTotal:this.nextActsTotal,
		   left:i_left+"",
		   top:i_top+""
		}   ;
		return json;
	}
	function _jSon_to(json){
		this.activityId=json.id;
		this.activityName=json.name;
		this.type=json.type;
		this.img.x(json.left);
		this.img.y(json.top);
	}
}
/**
 * 根据坐标x,y计算当前坐标是否在某个图元上
 */
function findIcon(x, y, process) {
	for (var i=0; i<process.icons.length; i++) {
	// $("#ttt").val($("#ttt").val()+"x:"+x+",y:"+y+"..............x():"+process.icons[i].img.x()+",y():"+process.icons[i].img.y()+",name:"+process.icons[i].displayName+"\r\n");
		if (x > process.icons[i].img.x() && x < (process.icons[i].img.x()+40) && y > process.icons[i].img.y() && y <(process.icons[i].img.y() + 40))
			return process.icons[i];
	}
	return null;
}

/**
 * 连线对象
 */
function Line() 
{
	this.iconBegin;		// 连线开始图元对象
	this.iconEnd;		// 连线结束图元对象
	this.begin;			// 连线开始坐标（开始图元的中心点）
	this.end;			// 连线结束坐标（结束图元的中心点）
	this.displayName;	// 连线上的文字
	this.color;			// 连线颜色
	this.id;
	this.name;
	this.process;
	this.polyline;   // 线
	this.maker;

	this.text = "";;
	this.points = new Array();	// 连线中的转折点对象
	this.polyDot=null;// 连线转折点，转为json对象
	this.init = _init_line;				// 连线初始化方法
	this.show = _show_line;				// 连线显示方法
	this.getPath = _get_line_path;
	this.getTextPath = _get_text_path;
	this.draw = _draw;					// 绘制连线方法
	this.drawLine = _draw_line;			// 画线方法
	this.drawText = _draw_text;			// 绘制连线文字
	this.toJson=_to_jSon;
	this.jsonTo=_jSon_to;
	this.redraw = _redraw;					// 重新绘制连线方法
	this.setDisplayName = _set_displayName; 
	this.startDrag = false;					// 连线是否被托拽
	// initiate line
	function _init_line(iconBegin, iconEnd,lineIniObj,parentProcess) {
		this.iconBegin = iconBegin;
		this.iconEnd = iconEnd;
		this.begin = new Point(this.iconBegin.img.x()+ half_image_size, this.iconBegin.img.y() + half_image_size);		
		this.end = new Point(this.iconEnd.img.x() + half_image_size, this.iconEnd.img.y() + half_image_size);	
		var	lineId=lineIniObj["id"]; 
		var	color=lineIniObj["color"];
        var	condType=lineIniObj["condType"];
		var	condition=lineIniObj["condition"];
		var	polyDot=lineIniObj["polyDot"];
	
		if(condType!=null)
			this.condType=condType;
		if(condition!=null)
			this.condition=condition;
		var	name=lineIniObj["name"];
		if (color != null)
			this.color = color;
		else
			this.color = "black";
		this.id=lineId;
		if(!AppKit.isEmptyVal(name)){
			this.name = name;
		}else{
			this.name = "连线";
		}
		if(polyDot!=null && polyDot!=undefined){
			this.polyDot=polyDot;	
			this.points=polyDot2Points(polyDot);
		}
		
		this.process=parentProcess;
		this.text = this.process.drawObj.text("条件");
		this.setDisplayName(this.name,this.condition);
		
		this.iconBegin.beginLine.push(this);
		this.iconEnd.endLine.push(this);
		this.draw();
		this.show();
	}
	function _redraw() {
		this.polyline.remove();
		this.begin = new Point(this.iconBegin.img.x() + half_image_size, this.iconBegin.img.y() + half_image_size);
		this.end = new Point(this.iconEnd.img.x() + half_image_size, this.iconEnd.img.y() + half_image_size);
		this.draw();
		this.show();
	}
	function _show_line() {
	}
	
	function _draw() {				
		this.drawLine();		
		this.drawText();
	}
	function _draw_text() {
		var sumX = 0;
		var sumY = 0;
		var computedTextPosition = false;
		var curBegin = new Point(this.begin.x, this.begin.y);
		var curEnd = new Point(this.end.x, this.end.y);
		if (this.points.length == 0) {
			curBegin = decreaseBegin(curBegin, curEnd);
			curEnd = decrease(curBegin, curEnd);
			sumX = parseInt((curBegin.x + curEnd.x) / 2);
			sumY = parseInt((curBegin.y + curEnd.y) / 2);
		} else {
			if ((this.points.length % 2) == 0) {
				var p = parseInt(this.points.length/2)-1;
				sumX = parseInt((this.points[p].x + this.points[p+1].x) / 2);
				sumY = parseInt((this.points[p].y + this.points[p+1].y) / 2);
			} else {
				var p = parseInt(this.points.length/2);
				sumX = this.points[p].x;
				sumY = this.points[p].y;
			}
		}
		this.text.x((sumX - charLength(this.displayName) * 5+20));
		this.text.y(sumY);
		this.text.width(charLength(this.displayName) * 10); 
		this.text.line = this;
		this.text.plot(this.getPath());
		
		if(!this.process.isDisplay){
			this.text.on("dblclick",function(event){
				curLine = this.line;
				showMenu('line', event);
				return false;
			});
		}
	}
	function _set_displayName(value,condition) {
		this.displayName = value;
		if(condition){
			// value = value+"("+condition+")";
		}
		this.text.tspan(value);
	}
	function _to_jSon(){
		var json=   {
		    id:this.id,
		    name:this.name,
		    type:"line",
		    shape:"line",
		    condType:this.condType,
		    condition:this.condition,	
		    from:this.iconBegin.id,
		    to:this.iconEnd.id,
		    fromx:this.begin.x+"",
		    fromy:this.begin.y+"",
		    tox:this.end.x+"",
		    toy:this.end.y+"",
		    polyDot:this.polyDot,
		    property:[]
		 } ;
		
		 return json;
	}	
	function _jSon_to(json){
		this.id=json.id;
		this.name=json.name;
	}
	function _draw_line() {
		var line = this.process.drawObj.polyline(this.getPath()).fill('none').stroke({ width: 2,color:this.color });
		var maker = this.process.drawObj.marker(10, 10, function(add) {
			 add.path("M0,2 L0,7 L4,5 L0 2");
		});
		maker.attr({ fill: this.color });
		line.marker("end",maker);
		line.line = this;
	  	
		if(!this.process.isDisplay){
			line.draggable({"enable":false});
			// 定义图片对象的开始托拽事件
			line.dragstart = function(delta,event){
				if (drawLineStatus ){
					return ;
				}
				
				 event = event || window.event;
			     curLine = this.line; 	 
			     this.drag_point=new Point(event.pageX,event.pageY);
			     this.style.cursor='move';		
			};
			
			// 定义图片对象的托拽事件
			line.dragmove  = function(delta,event){
				if (drawLineStatus ){
					return ;
				}
				 event = event || window.event;
			     this.style.cursor='move';
			};
			
			// 定义图片对象的托拽结束
			line.dragend  = function(delta,event){
				if (drawLineStatus ){
					return ;
				}
				event = event || window.event;
			    if(this.drag_point!=null){       
			       var np = new Point(event.pageX,event.pageY);
			       addDragPointPosition(curLine,this.drag_point,np);
				   curLine.redraw();     		      
				   this.style.cursor='default';
			    }
			};
			
			line.on("dblclick",function(event) {
				event = event || window.event;
				curLine = this.line;
				showMenu('line', event);
				return false;
			});
		}
		
		this.maker = maker;
		this.polyline = line;
	} 
	
	function _get_text_path(){
		var sumX = 0;
		var sumY = 0;
		var computedTextPosition = false;
		var curBegin = new Point(this.begin.x, this.begin.y);
		var curEnd = new Point(this.end.x, this.end.y);
		
		if (this.points.length == 0) {
			curBegin = decreaseBegin(curBegin, curEnd);
			curEnd = decrease(curBegin, curEnd);
			sumX = parseInt((curBegin.x + curEnd.x) / 2);
			sumY = parseInt((curBegin.y + curEnd.y) / 2);
		} else {
			if ((this.points.length % 2) == 0) {
				var p = parseInt(this.points.length/2)-1;
				sumX = parseInt((this.points[p].x + this.points[p+1].x) / 2);
				sumY = parseInt((this.points[p].y + this.points[p+1].y) / 2);
			} else {
				var p = parseInt(this.points.length/2);
				sumX = this.points[p].x;
				sumY = this.points[p].y;
			}
		}
		
		var middlePoint = new Point(sumX, sumY);
		if (this.points.length == 0) {
			curBegin = decreaseBegin(curBegin, middlePoint);
			curEnd = decrease(curBegin, middlePoint);
			sumX = parseInt((curBegin.x + curEnd.x) / 2);
			sumY = parseInt((curBegin.y + curEnd.y) / 2);
		} else {
			if ((this.points.length % 2) == 0) {
				var p = parseInt(this.points.length/2)-1;
				sumX = parseInt((this.points[p].x + this.points[p+1].x) / 2);
				sumY = parseInt((this.points[p].y + this.points[p+1].y) / 2);
			} else {
				var p = parseInt(this.points.length/2);
				sumX = this.points[p].x;
				sumY = this.points[p].y;
			}
		}
		
		var startPoint = new Point(sumX, sumY);
	   if(this.begin.x  == this.end.x && this.begin.y==this.end.y){
	      return calSelfToSelf(this,this.begin);
	   }else{
		   var ps = this.points;   
		   var bp = decreaseBegin(startPoint,(ps.length>0)?ps[0]:this.end);
		   var linePath = "M "+ bp.x+","+bp.y;
		   for (var i=0; i < ps.length; i++) {
			 	var p = ps[i];
			    linePath = " L "+linePath+" "+p.x+","+ p.y;
		   }
		   var ep = decrease((ps.length>0)?ps[ps.length-1]:this.begin,this.end);
		   linePath = linePath+" L "+ep.x+","+ep.y;
		   return linePath;
	   }
	}

	function _get_line_path(){
	   if(this.begin.x  == this.end.x && this.begin.y==this.end.y){
	      return calSelfToSelf(this,this.begin);
	   }else{
		   var ps = this.points;   
		   var bp = decreaseBegin(this.begin,(ps.length>0)?ps[0]:this.end);
		   var linePath = bp.x+","+bp.y;
		   for (var i=0; i < ps.length; i++) {
			 	var p = ps[i];
			    linePath = linePath+" "+p.x+","+ p.y;
		   }
		   var ep = decrease((ps.length>0)?ps[ps.length-1]:this.begin,this.end);
		   linePath = linePath+" "+ep.x+","+ep.y;
		   return linePath;
	   }
	}
}

/**
 * 坐标对象包含x,y位置
 */
function Point(x, y){
	this.x = x;
	this.y = y;
}

/**
 * 计算连线开始位置（因为连线的开始坐标在图元中，连线开始位置不能在图元中间，需要重新计算）
 */
function decreaseBegin(begin, end)
{
	var len = half_image_size;
	var dx = end.x - begin.x;
	var dy = end.y - begin.y;
	var xIsLarge = false;
	var yIsLarge = false;
	if (dx < 0)
		xIsLarge = true;
	if (dy < 0)
		yIsLarge = true;
	if (dx == 0 && dy == 0)
		return begin;
	if (dx == 0)
		if (yIsLarge)
			return new Point(begin.x, begin.y - len);
		else
			return new Point(begin.x, begin.y + len);
	if (dy == 0)
		if (xIsLarge)
			return new Point(begin.x - len, begin.y);
		else
			return new Point(begin.x + len, begin.y);

	dx = Math.abs(dx);
	dy = Math.abs(dy);
	var decline = parseInt(Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2)));
	var _x = parseInt((len * dx) / decline);
	var _y = parseInt((len * dy) / decline);
	if (xIsLarge && yIsLarge)
		return new Point(begin.x - _x, begin.y - _y);
	if (xIsLarge)
		return new Point(begin.x - _x, begin.y + _y);
	if (yIsLarge)
		return new Point(begin.x + _x, begin.y - _y);
	else
		return new Point(begin.x + _x, begin.y + _y); 
}

/**
 * 计算连线结束位置（因为连线的结束坐标在图元中，连线结束位置不能在图元中间，需要重新计算）
 */
function decrease(ptBegin, ptEnd)
{
	var len = half_image_size;
	var dx = ptEnd.x - ptBegin.x;
	var dy = ptEnd.y - ptBegin.y;
	var xIsLarge = false;
	var yIsLarge = false;
	if (dx < 0)
		xIsLarge = true;
	if (dy < 0)
		yIsLarge = true;
	if (dx == 0 && dy == 0)
		return ptEnd;
	if (dx == 0)
		if (yIsLarge)
			return new Point(ptEnd.x, ptEnd.y + len);
		else
			return new Point(ptEnd.x, ptEnd.y - len);
	if (dy == 0)
		if (xIsLarge)
			return new Point(ptEnd.x + len, ptEnd.y);
		else
			return new Point(ptEnd.x - len, ptEnd.y);
	dx = Math.abs(dx);
	dy = Math.abs(dy);
	var decline = parseInt(Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2)));
	var _x = parseInt((len * dx) / decline);
	var _y = parseInt((len * dy) / decline);
	if (xIsLarge && yIsLarge)
		return new Point(ptEnd.x + _x, ptEnd.y + _y);
	if (xIsLarge)
		return new Point(ptEnd.x + _x, ptEnd.y - _y);
	if (yIsLarge)
		return new Point(ptEnd.x - _x, ptEnd.y + _y);
	else
		return new Point(ptEnd.x - _x, ptEnd.y - _y);
}

/**
 * 计算字符串的字节长度
 */
function charLength(str) {
    if( str == null || str ==  "" ) return 0;
    var totalCount = 0;
    for (i = 0; i< str.length; i++) {
        if (str.charCodeAt(i) > 127) 
            totalCount += 2;
        else
            totalCount++ ;
    }
    return totalCount;
}
// 近似值判断
function dp(p1,p2){
   return  Math.sqrt((p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y));   
}
function pointInLine(p,p1,p2){     
   var lap =2;
   return Math.abs(dp(p,p1)+dp(p,p2)-dp(p1,p2))<lap;
}
function pointNearPoint(p,p1){
   var dx =10;
   if(p!=null&&p1!=null){
      return dp(p,p1)<dx;
   }else{
     return false;
   }
}   

/**
 * 根据开始拖动的点位置把最后结束的位置加入到线的Points中 算法:判断拖动的起始点在开始节点和折点组成的那条线段上，来增加折点
 * 或者判断移动是否是折点本身，平且考虑折点被移动到相邻折点组成的线段上。
 */
function addDragPointPosition(line,op,np){
    // debugger;
     var begin,end;     
     var pts = line.points;
     var len = pts.length;
     var flag = false;// 找到拖动点所在的线段
     // moveflag：如果移动的是折点为true否则为false
     // delflag：如果折点被移动到相邻折点组成的线段上true否则为false
     var moveflag = false,delflag = false;
     
     var i = -1;
     if(len>0){
     for(;i<len;i++){
     // begin = (i==-1)?decreaseBegin(line.begin,pts[0]):pts[i]; //如果i=-1
		// 线段为开始图元到第一个折点
     // end = (i==len-1)?decrease(pts[len-1],line.end):pts[i+1];//如果i=len-1
		// 线段为最后一个折点到结束节点
         begin = (i==-1)?line.begin:pts[i]; // 如果i=-1 线段为开始图元到第一个折点
         end = (i==len-1)?line.end:pts[i+1];// 如果i=len-1 线段为最后一个折点到结束节点
         if(pointNearPoint(op,pts[i+1])){
            moveflag = true;
            if(i<len-1){
	            var p =(i==len-2)? line.end:pts[i+2];            
	            if(pointInLine(op,begin,p)){
	               delflag = true;
	            }
            }
            i++;
            break;
         }else{
	         if(pointInLine(op,begin,end)){
	            flag = true;
	            i++;
	            break;
	         }
         }
     }
     }              
     
     if(moveflag){
       // 移动折点
       if(delflag){
         pts.remove(pts[i]);
       }else{
         pts[i] = np;
       }
       
     }else{
      // 如果都没有找到拖动点的位置，则认为拖动点在最后一个点和结束点之间
     if(!flag){
          pts.push(np);
      }else{        
         for(var j=len;j>i;j--){
            pts[j]=pts[j-1];
         }
         pts[i]=np;         
       }   
    }              
}

/**
 * 删除当前点中的连线
 */
function removeCurLine(process) {
	if (curLine != null) {
		delLine(curLine,process);
	}
}

/**
 * 删除当前点中的图元
 */
function removeCurIcon(process) {
	if (curIcon != null) {
		delIcon(curIcon,process);
	}
}
/**
 * 删除连线
 */
function delLine(line,process) {
	line.iconBegin.beginLine.remove(line);
	line.iconEnd.endLine.remove(line);
	
	line.polyline.remove();
	removeText(line.text);
	process.lines.remove(line);
}
/**
 * 删除图元
 */
function delIcon(icon,process) {
	// 不能删除开始和结束活动
	for (var i=(icon.beginLine.length-1); i>=0; i--) {
		delLine(icon.beginLine[i],process);
	}
	for (var i=(icon.endLine.length-1); i>=0; i--) {
		delLine(icon.endLine[i],process);
	}
	icon.img.remove();
	icon.text.remove();
	process.icons.remove(icon);	
}
function removeAllChild(obj) {
	for (var i=obj.childNodes.length-1; i>=0; i--) obj.removeChild(obj.childNodes[i]);
}

// 删除线
function removeLine(obj) {
	obj.line = null;
	obj.remove();
}

// 删除图元
function removeIcon(obj) {
	obj.icon = null;
	obj.remove();
}

// 删除文本
function removeText(obj) {
	obj.line = null;
	obj.icon = null;
	obj.onchange = null;
	obj.remove();
}

/**
 * 根据传入的id显示右键菜单
 */
function showMenu(id, event){
    if('line' == id){
        popMenu('line',200,75, event);
    }
     if('icon' == id){
        popMenu('icon',150,70, event);
    }
    return false;
}

function addIconDiv(){
	var ihtml  = "";
	
	ihtml += "<div id='icondiv' style='display:none;'>";
	ihtml += "<table id='icontable' width='100%' height='30'  border='1' bgcolor='#cccccc' style='border:thin;' cellspacing='0'>";
	ihtml += "<tr><td style='cursor:hand;border:outset 1;FONT-SIZE:12px;' align='center' onclick='FlowKit.delAct(event);'>删除活动</td></tr>";
	ihtml += "</table></div>";

	document.body.insertAdjacentHTML("afterEnd",ihtml);
}


function addLineDiv(){
	var ihtml  = "";

	ihtml += "<div id='linediv' style='display:none;'>";
	ihtml += " <table id='linetable' border='1' width='100%' height='60' bgcolor='#cccccc' style='border:thin' cellspacing='0'>";
	ihtml += " <tr> <td style='cursor:hand;border:outset 1;FONT-SIZE:12px;' align='center' id='mdelline'  onclick='FlowKit.delLineFromGraph(event);'>删除连线</td> </tr>";
	ihtml += "<tr> <td style='cursor:hand;border:outset 1;FONT-SIZE:12px;' align='center' id='mdelline'  onclick='FlowKit.showCondLine4Graph(event);'>设置条件信息</td> </tr>";
	ihtml += "</table></div>";
	
	document.body.insertAdjacentHTML("afterEnd",ihtml);
}


function addSetDiv(){
	var ihtml  = "";
	
	ihtml += "<div id='condiv' style='display:none;'><div  width='100%' height='100%' id='contable'>";
	ihtml += "<table  class='viewdatagrid' >";
	ihtml += "<tr >";
	ihtml += "<th style='text-align:center'>字段定义</th>";
	ihtml += "<th style='text-align:center'>字段名称</th>";
	ihtml += "</tr>";
	
	for(var i=0;i< process.fieldDef.length;i++){
		ihtml += "<tr>";
		ihtml += "<td style='text-align:center'>"+process.fieldDef[i].field_name+"</td>";
		ihtml += "<td style='text-align:center'>"+process.fieldDef[i].field_display+"</td>";
		ihtml += "</tr>";
	}
	
	ihtml += "</table><br>";
	ihtml += "<table  border='1'  bgcolor='#cccccc' style='border:solid 1px #95B8E7;' cellspacing='0' class='viewdatagrid'>";
	ihtml += "<tr> <th style='width:50px;'>名称：</th><td style='padding: 3px;'> <input id='conname' name='conname' style='width:150px;padding-bottom:3px;'/></td>  </tr>";
	ihtml += "<tr> <th>条件：</th><td style='padding: 3px;'>  <input id='condition' name='condition' style='width:200px;padding-bottom:3px;'/></td>  </tr>";
	ihtml += "<tr><td colspan='2'> <div class='buttonline'> <a id='btnConfirmCon' href='javascript:setCondLine();'><span>确认</span></a>  </div></td> </tr>";
	ihtml += "</table></div></div>";
	document.body.insertAdjacentHTML("afterEnd",ihtml);
}

/**
 * 显示弹出菜单 menuDiv:右键菜单的内容 width:行显示的宽度
 * rowControlString:行控制字符串，0表示不显示，1表示显示，如“101”，则表示第1、3行显示，第2行不显示
 */
function popMenu(menuDiv,width,height, event){
	event = event || window.event;
	var win = '';
	var rowObjs ;
	
    // 设置弹出菜单的内容
    if(menuDiv=="icon")	{
    	var obj = document.getElementById("icontable");
    	if(!obj){
    		addIconDiv();
    	}
    	
    	win = $("#icontable").window( {
    		title : '图标属性',
    		width : width,
    		height : height,
    		top : event.clientY,
    		left : event.clientX-1,
    		shadow : true,
    		modal : true,
    		closed : true,
    		minimizable : false,
    		maximizable : false,
    		collapsible : false
    	});
    	 rowObjs=document.getElementById("icontable").rows;
    }else if(menuDiv=="line")	{
    	var obj = document.getElementById("linetable");
    	if(! obj){
    		addLineDiv();
    	}
    	win = $("#linetable").window( {
    		title : '连线属性',
    		width : width,
    		height : height,
    		top : event.clientY,
    		left : event.clientX-1,
    		shadow : true,
    		modal : true,
    		closed : true,
    		minimizable : false,
    		maximizable : false,
    		collapsible : false
    	});
    	 rowObjs=document.getElementById("linetable").rows;
    }
    // 获得弹出菜单的行数
    var rowCount=rowObjs.length;
    // 循环设置每行的属性
    for(var i=0;i<rowObjs.length;i++){
    	rowObjs[i].cells[0].style.background="#ffffff";
    	rowObjs[i].cells[0].style.color="black";
    	rowObjs[i].cells[0].style.border="solid 1px #95B8E7";
    	
        // 设置鼠标滑入该行时的效果
        rowObjs[i].cells[0].onmouseover=function(){
            this.style.background="#818181";
            this.style.color="white";
        }
        
        // 设置鼠标滑出该行时的效果
        rowObjs[i].cells[0].onmouseout=function(){
            this.style.background="#ffffff";
            this.style.color="black";
        }
    }
    // 显示菜单
    win.window('open');
    popWin = win; 
    return true;
}
function showSetCon(event,width,heigth){
	if(popWin){
		popWin.window("close");
	}
	event = event || window.event;
	
	var obj = document.getElementById("contable");
	if(!obj)	{
		addSetDiv();
	}
	var win = $("#contable").window( {
		title : '设置属性',
		width : width,
		height : heigth,
		top : event.pageY,
		left : event.pageX-1,
		shadow : true,
		modal : true,
		closed : true,
		minimizable : false,
		maximizable : false,
		collapsible : false
	});
	$("#condition").val(curLine.condition);
	$("#conname").val(curLine.name);
    // 显示菜单
    win.window('open');
    popWin = win; 
    return true;
}


function setCondLine(){
	var condition = $("#condition").val();
	var name = $("#conname").val();
	if(!name){
		AppKit.showMessage("显示名称不能为空");
		return;
	}
	if(curLine!=null){
		curLine.condition = condition;
		curLine.name = name;
		curLine.setDisplayName(curLine.name,curLine.condition);
		
		if(popWin){
			popWin.window("close");
		}
		curLine.show();
	}
};

function clear(process) {
		// 显示当前图标总数和连线总数
		for (var i=0; i<process.icons.length; i++) {
			process.icons[i].img.remove();
			process.icons[i].text.remove();
		}
		for (var i=0; i<process.lines.length; i++) {
			process.lines[i].polyline.remove()
			process.lines[i].text.remove();
		}
		process.icons = new Array();
		process.lines = new Array();
}
	JSONDef=new function(){this.decode=function(){var filter,result,self,tmp;if($$("toString")){switch(arguments.length){case 2:self=arguments[0];filter=arguments[1];break;case 1:if($[typeof arguments[0]](arguments[0])===Function){self=this;filter=arguments[0];}else self=arguments[0];break;default:self=this;break;};if(rc.test(self)){try{result=e("(".concat(self,")"));if(filter&&result!==null&&(tmp=$[typeof result](result))&&(tmp===Array||tmp===Object)){for(self in result)result[self]=v(self,result)?filter(self,result[self]):result[self];}}catch(z){}}else{alert("bad data");}};return result;};this.encode=function(){var self=arguments.length?arguments[0]:this,result,tmp;if(self===null)result="null";else if(self!==undefined&&(tmp=$[typeof self](self))){switch(tmp){case Array:result=[];for(var i=0,j=0,k=self.length;j<k;j++){if(self[j]!==undefined&&(tmp=JSONDef.encode(self[j])))result[i++]=tmp;};result="[".concat(result.join(","),"]");break;case Boolean:result=String(self);break;case Date:result='"'.concat(self.getFullYear(),'-',d(self.getMonth()+1),'-',d(self.getDate()),'T',d(self.getHours()),':',d(self.getMinutes()),':',d(self.getSeconds()),'"');break;case Function:break;case Number:result=isFinite(self)?String(self):"null";break;case String:result='"'.concat(self.replace(rs,s).replace(ru,u),'"');break;default:var i=0,key;result=[];for(key in self){if(self[key]!==undefined&&(tmp=JSONDef.encode(self[key])))result[i++]='"'.concat(key.replace(rs,s).replace(ru,u),'":',tmp);};result="{".concat(result.join(","),"}");break;}};return result;};this.toDate=function(){var self=arguments.length?arguments[0]:this,result;if(rd.test(self)){result=new Date;result.setHours(i(self,11,2));result.setMinutes(i(self,14,2));result.setSeconds(i(self,17,2));result.setMonth(i(self,5,2)-1);result.setDate(i(self,8,2));result.setFullYear(i(self,0,4));}else if(rt.test(self))result=new Date(self*1000);return result;};var c={"\b":"b","\t":"t","\n":"n","\f":"f","\r":"r",'"':'"',"\\":"\\","/":"/"},d=function(n){return n<10?"0".concat(n):n},e=function(c,f,e){e=eval;delete eval;if(typeof eval==="undefined")eval=e;f=eval(""+c);eval=e;return f},i=function(e,p,l){return 1*e.substr(p,l)},p=["","000","00","0",""],rc=null,rd=/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}$/,rs=/(\x5c|\x2F|\x22|[\x0c-\x0d]|[\x08-\x0a])/g,rt=/^([0-9]+|[0-9]+[,\.][0-9]{1,3})$/,ru=/([\x00-\x07]|\x0b|[\x0e-\x1f])/g,s=function(i,d){return "\\".concat(c[d])},u=function(i,d){var n=d.charCodeAt(0).toString(16);return "\\u".concat(p[n.length],n)},v=function(k,v){return $[typeof result](result)!==Function&&(v.hasOwnProperty?v.hasOwnProperty(k):v.constructor.prototype[k]!==v[k])},$={"boolean":function(){return Boolean},"function":function(){return Function},"number":function(){return Number},"object":function(o){return o instanceof o.constructor?o.constructor:null},"string":function(){return String},"undefined":function(){return null}},$$=function(m){function $(c,t){t=c[m];delete c[m];try{e(c)}catch(z){c[m]=t;return 1}};return $(Array)&&$(Object)};try{rc=new RegExp('^("(\\\\.|[^"\\\\\\n\\r])*?"|[,:{}\\[\\]0-9.\\-+Eaeflnr-u \\n\\r\\t])+?$')}catch(z){rc=/^(true|false|null|\[.*\]|\{.*\}|".*"|\d+|\d+\.\d+)$/}};
	function insertObjInArr (arr,s){if(arr==null)arr=[];arr[arr.length]=s;return arr;}
	function removeObjInArr (arr,s){var tArr=null;var count=0;if(arr!=null){tArr=[];var num=arr.length;for(var i=0;i<num;i++){if(arr[i]!=s){tArr[count]=arr[i];count++;}}}return tArr;}
	// 根据当前线的属性获得线的起始图元
	function getIconById(iconId,process){
		var	len = process.icons.length;	
		var	tempIcon=null;
		for(var j=0;j<len;j++){
			tempIcon=process.icons[j];
			if(tempIcon.id==iconId || tempIcon.activityId==iconId){
				break;
			}
		}
		return tempIcon;
	}
	
	// 根据当前线的属性获得线的起始图元的坐标+图元
	function getIconIndexById(iconId,process){
		var	len = process.icons.length;	
		var	iconIndex=-1;
		for(var j=0;j<len;j++){
			tempIcon=process.icons[j];
			if(tempIcon.id==iconId || tempIcon.activityId==iconId){
				iconIndex=j;
				break;
			}
		}
		var	retObj={};
		retObj["index"]=iconIndex;
		retObj["icon"]=tempIcon;
		return retObj;
	}
	
	// 根据当前图元的流入或者输出线条集合，in表示流入线条，out表示流出线条，同时得出前驱和后继节点
	function getInOutLines(iconObj){
		var	rsLines={};
		var	inLines="";
		var	outLines="";
		var	prevActs="";
		var	nextActs="";
		for(var j=0;j<iconObj.endLine.length;j++){
			inLines=inLines+iconObj.endLine[j].id+";";
			if(iconObj.endLine[j].iconBegin.activityId!=undefined && iconObj.endLine[j].iconBegin.activityId!=null && iconObj.endLine[j].iconBegin.activityId!="")
				prevActs=prevActs+iconObj.endLine[j].iconBegin.activityId+";"
		}
		for(var j=0;j<iconObj.beginLine.length;j++){
			outLines=outLines+iconObj.beginLine[j].id+";";
			if(iconObj.beginLine[j].iconEnd.activityId!=undefined && iconObj.beginLine[j].iconEnd.activityId!=null && iconObj.beginLine[j].iconEnd.activityId!="")
				nextActs=nextActs+iconObj.beginLine[j].iconEnd.activityId+";"
		}
		rsLines["inLines"]=inLines;
		rsLines["outLines"]=outLines;
		rsLines["prevActs"]=prevActs;
		rsLines["nextActs"]=nextActs;
		return rsLines;
	}
	
	// 获取连线Max值
	function getLineMaxCount(process){
	    var		iCount=0;
	    var		maxCount=0;;
	    var		iIndex=0;
	    for(var i=0;i< process.lines.length;i++){
	         iIndex="line".length+1;
	         iCount=parseInt(process.lines[i].id.substr(iIndex));
	         if(iCount>maxCount)
	         	maxCount=iCount;
	    }
	    return maxCount;
	}	
 
	// 遍历节点，处理每个节点的前驱或者后继集合
	function makeActsTotal(process,adjFlag){
		var		len=process.icons.length;
		var		iconVisited=new Array();
		var		actsTotal="";
		for(var	i=0;i<len;i++){
			for(var	j=0;j<len;j++){
				iconVisited[j]=false;
			}
			if(!iconVisited[i]) {
				DFSProcess(process,i,iconVisited,adjFlag);
			}
		}
	}
	/**
	 * 深度优先搜索算法
	 */    
	function  DFSProcess(process,iconIndex,iconVisited,adjFlag){
		var	tmpActsTotal="";
		var	actsTotal="";
		if(iconVisited[iconIndex]) {
		 	return actsTotal;
		}
		iconVisited[iconIndex]=true;
		 var	adjObjArr=new Array();
		if(adjFlag=="prev"){
			if(process.icons[iconIndex].prevActs==null || process.icons[iconIndex].prevActs=="" || process.icons[iconIndex].prevActs==undefined){
				return "";
			}
			adjObjArr=process.icons[iconIndex].prevActs.split(";")
		}
		if(adjFlag=="next"){
			if(process.icons[iconIndex].nextActs==null || process.icons[iconIndex].nextActs=="" || process.icons[iconIndex].nextActs==undefined){
				return "";
			}
			adjObjArr=process.icons[iconIndex].nextActs.split(";")
		}
		 var	tmpIndex=-1;
		 var	tempIconObj=null;
		 var	tmpObj=null;
		 for(var i=0;i<adjObjArr.length;i++){
		 	 if(adjObjArr[i]=="" || adjObjArr[i]==null || adjObjArr[i]==undefined) continue;
		 	 tmpObj=getIconIndexById(adjObjArr[i],process);
		 	 if(tmpObj==null || tmpObj=="" || tmpObj==undefined || tmpObj["index"]==undefined ||tmpObj["index"]==null || tmpObj["index"]=="") continue;
		 	 tmpIndex=tmpObj["index"];
		 	 if(tmpIndex<0) continue;
		 	 tempIconObj=tmpObj["icon"];
		 	if(!iconVisited[tmpIndex]) {
			 	tmpActsTotal=DFSProcess(process,tmpIndex,iconVisited,adjFlag);
				tmpActsTotal=dealActsTotal(adjObjArr[i],tmpActsTotal)
				if(adjFlag=="prev"){
					tempIconObj.prevActsTotal=tmpActsTotal;
				}
				if(adjFlag=="next"){
					tempIconObj.nextActsTotal=tmpActsTotal;
				}
		 	}
			if(adjFlag=="prev"){
				actsTotal+=tempIconObj.prevActsTotal;
			}
			if(adjFlag=="next"){
				actsTotal+=tempIconObj.nextActsTotal;
			}
		 }
		if(adjFlag=="prev"){
			actsTotal+=process.icons[iconIndex].prevActs;
		}
		if(adjFlag=="next"){
			actsTotal+=process.icons[iconIndex].nextActs;
		}
		 return actsTotal;
	}

	// 获取活动坐标位置
   function getActPosition(actDeal){
		var	actIndex=actDeal["index"];
		var	rowCount=actDeal["row_count"];
		var	preActLeft=actDeal["pre_act_left"];
		var	preActTop=actDeal["pre_act_top"];
		var	actHeight=2*half_image_size;
		var	actWidth=2*half_image_size;
		var	actLineWidth=half_act_line_width*2;
		var	preDirectIndex=actDeal["pre_direct_index"];
		var	direcFlag=actDeal["direct_flag"];
		var	actLeft="";
		var	actTop="";
		if(actIndex== preDirectIndex+rowCount-1){// 该条件表明为转折节点，线条要由向右方向（向下方向）转为向下方向（向右方向）
			if(direcFlag=="right"){// 如果为向右方向
				if(actIndex>0)
					actLeft=preActLeft+actWidth+actLineWidth;
				else	
					actLeft=preActLeft;
				actTop=preActTop;
				direcFlag="down";
			}else	{// 如果为向下方向
				actLeft=preActLeft;
				actTop=preActTop+actHeight+actLineWidth;
				direcFlag="right";
			}
			preDirectIndex=actIndex;
		}else	{// 该条件表明为线条不转折持续前一个线条的方向
			if(direcFlag=="right"){
				if(actIndex>0)
					actLeft=preActLeft+actWidth+actLineWidth;
				else	
					actLeft=preActLeft;
				actTop=preActTop;
			}else	{
				actLeft=preActLeft;
				actTop=preActTop+actHeight+actLineWidth;
			}
		}
		var	rsObj={};
		rsObj["act_left"]= actLeft;
		rsObj["act_top"]= actTop;
		rsObj["direct_flag"]= direcFlag;
		rsObj["pre_direct_index"]= preDirectIndex;
		return rsObj;
   }
   // 处理活动后继前驱节点，去除多余部分
	function	dealActsTotal(curAct,actsTotal){
		var		arrActs=actsTotal.split(";");
		var		tempActs=actsTotal;
		var		dealedActs="";
		var		inFlag=false;
		for(var	i=0;i<arrActs.length;i++){
			var		arrDealed=dealedActs.split(";");
			inFlag=false;
			if(arrActs[i]==curAct) {
				inFlag=true;
			} else	{
				for(var	j=0;j<arrDealed.length;j++){
					if(arrActs[i]==arrDealed[j]) {
						inFlag=true;
						break;
					}
				}
			}
			if(!inFlag){
				dealedActs+=arrActs[i]+";";
			}
		}
		return dealedActs;
	}

/**
 * 给js 的Array对象增加一个remove的方法，可以直接删除数组中指定的对象
 */
Array.prototype.remove = function(obj) {
	var c = 0;
	for(var i=0,n=0; i<this.length; i++)
	{
		if (this[i] != obj) {
			this[n++] = this[i];
		} else {
			c++;
		}
	}
	this.length -= c;
}

// 处理line连线上的polyDot
function getPolyDot4Line(points){
	var	polyDot=new Array();
	var	x="";
	var	y="";
	for(var i=0;i<points.length;i++){
		polyDot[i]={x:points[i].x,y:points[i].y};
	}
	return polyDot;
}
// 处理line连线上的polyDot
function polyDot2Points(polyDots){
	var	points=new Array();
	var	x="";
	var	y="";
	for(var i=0;i<polyDots.length;i++){
		points[i]=new Point(polyDots[i].x,polyDots[i].y);
	}
	return points;
}
