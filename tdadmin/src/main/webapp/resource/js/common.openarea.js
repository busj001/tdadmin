function AreaKitClass(){
	
}

AreaKitClass.prototype.initarea=function(){
	var url = "../common/getallarea";
	AppKit.postJSON(url,"",this.initareaback);
}

AreaKitClass.prototype.initopenarea=function(){
	var url = "../common/getallopenarea";
	AppKit.postJSON(url,"",this.initareaback);
}

AreaKitClass.prototype.initareaback=function(json){
	if(!AppKit.checkJSONResult(json)){
		return;
	}
	
	var area = json.data;

	
	for(var i=0;i<area.length;i++){
		var d = area[i];
		var l = d["areaLevel"];
		var a = new Array();
		a[0] = d["areaCode"]+"";
		a[1] = d["areaName"];
		if(l == "1"){
			AreaKit.address['0'].push(a);
		}else if(l == "2"){
			AreaKit.address['1'].push(a);
		}else if(l == "3"){
			AreaKit.address['2'].push(a);
		}
	}
}

AreaKitClass.prototype.getAddress=function(regionId,type) {
	  var array = {};
	  if(type==0) {//省级列表
		var obj = AreaKit.address['0'];
		var len = obj.length;
		for(var i=0; i<len; i++) {
			var key = obj[i][0];
			var value = obj[i][1];
			array[key] = value;
		}
	  } else if(type==1) {//市级列表
		var str = regionId.substring(0,2);
		var obj = AreaKit.address['1'];
		var len = obj.length;
		for(var i=0; i<len; i++) {
			var key = obj[i][0];
			var value = obj[i][1];
			if(key.substring(0,2)==str) {array[key] = value;}
		}
	  } else if(type==2) {//区县级列表
		var str = regionId.substring(0,4);
		var obj = AreaKit.address['2'];
		var len = obj.length;
		for(var i=0; i<len; i++) {
			var key = obj[i][0];
			var value = obj[i][1];
			if(key.substring(0,4)==str) {array[key] = value;}
		}
	  }
	  return array;
	}


AreaKitClass.prototype.getAllAreaName= function(province,city,area) {
	
	alert(province);
	
	if(area!=""){
		return getAreaName(area);
	}
	
	if(city!=""){
		return getAreaName(city);
	}
	
	if(province!=""){
		return getAreaName(province);
	}
};


AreaKitClass.prototype.getAreaName = function(area) {
	if(!area){
		return;
	}
	area = area.toString();
	var prov = area.substring(0,2)+"0000";
	var city = area.substring(0,4)+"00";
	var area = area.substring(0,6);
	
	var name ="";
	
	var obj = AreaKit.address['0'];
	var len = obj.length;
	for(var i=0; i<len; i++) {
		var key = obj[i][0];
		var value = obj[i][1];
		if(key == prov){
			name += value;
		}
	}
	
	var obj = AreaKit.address['1'];
	var len = obj.length;
	for(var i=0; i<len; i++) {
		var key = obj[i][0];
		var value = obj[i][1];
		if(key == city){
			name += value;
		}
	}
	
	var obj = AreaKit.address['2'];
	var len = obj.length;
	for(var i=0; i<len; i++) {
		var key = obj[i][0];
		var value = obj[i][1];
		if(key == area){
			name += value;
		}
	}
	
	return name;
};

AreaKitClass.prototype.getAreaCode = function() {
	var ajson = {};
	ajson[$("#id_areaSelect1").attr("name")] =  $("#id_areaSelect1").val();
	ajson[$("#id_citySelect1").attr("name")] =  $("#id_citySelect1").val();
	ajson[$("#id_provSelect1").attr("name")] =  $("#id_provSelect1").val();
	return ajson;
};

AreaKitClass.prototype.initArea = function() {
	loadProvince1("110000");
};

AreaKitClass.prototype.setArea = function(prov,city,area) {
	loadProvince1(area);
	$("#id_provSelect1").val(prov);
	$("#id_citySelect1").val(city);
	$("#id_areaSelect1").val(area);
};



AreaKitClass.prototype.getCityCode = function() {
	var ajson = {};
	ajson[$("#id_provSelect2").attr("name")] =  $("#id_provSelect2").val();
	ajson[$("#id_citySelect2").attr("name")] =  $("#id_citySelect2").val();
	return ajson;
};


AreaKitClass.prototype.initCity = function() {
	loadProvince2("110000");
};

AreaKitClass.prototype.setCity = function(prov,city) {
	$("#id_provSelect2").val(prov);
	$("#id_citySelect2").val(city);
};


AreaKit=new AreaKitClass();
AreaKit.address = {};
AreaKit.address['0'] = new Array();
AreaKit.address['1'] = new Array();
AreaKit.address['2'] = new Array();
AreaKit.initopenarea();


