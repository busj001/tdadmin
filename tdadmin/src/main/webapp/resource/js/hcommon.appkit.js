function AppKitClass(){
	this.setHeight = 0;
	this.winHeight = 0;
	this.winWidth = 0;
	this.defaultGridWidth=320;
	this.isEasyUI=true;
	this.winarray = new Array();
	this.appPath = null;
}

// 跳转
AppKitClass.prototype.link = function(url) {
	document.location = url;
};

AppKitClass.prototype.setAppPath = function(path) {
	this.appPath = path;
};

AppKitClass.prototype.getWinWidth=function(){
	return  $(parent.window).width();;
}


AppKitClass.prototype.getWinHeight=function(){
	return $(parent.window).height();
}

AppKitClass.prototype.getCurWinWidth=function(){
	return  $(window).width();;
}


AppKitClass.prototype.getCurWinHeight=function(){
	return $(window).height();
}

//弹出窗口
AppKitClass.prototype.openWindow = function(url, title, width,heigth,data,hideTitle) {
	var titleinfo =  [title, 'background-color:#019e97;'];
	if(hideTitle){
		titleinfo = false;
	}
	var op = {
	    type: 2,
	    title:titleinfo,
	    shadeClose: false,
	    shade: [0.3, '#393D49'],
	    area: [width+"px",heigth+"px"],
	    content: url //iframe的url
	};
	var selfwin = window.self;
	var topwin = this.getTopWindow();
	var index = topwin.showWindow(op); 
	
	var winjson = {"index":index,"win":selfwin,"data":data};
	
	topwin.getAppKit().winarray.push(winjson);
}

//弹出窗口
AppKitClass.prototype.openUnMoralWindow = function(url, title, width,heigth,data,hideTitle) {
	var titleinfo =  [title, 'background-color:#019e97;'];
	if(hideTitle){
		titleinfo = false;
	}
	var op = {
	    type: 2,
	    title:titleinfo,
	    shadeClose: false,
	    shade: 0,
	    area: [width+"px",heigth+"px"],
	    content: url //iframe的url
	};
	var selfwin = window.self;
	var topwin = this.getTopWindow();
	var index = topwin.showWindow(op); 
	
	var winjson = {"index":index,"win":selfwin,"data":data};
	
	topwin.getAppKit().winarray.push(winjson);
}

//弹出窗口
AppKitClass.prototype.openDivWindow = function(divid, title, width,heigth,data,hideTitle) {
	var titleinfo =  [title, 'background-color:#019e97;'];
	if(hideTitle){
		titleinfo = false;
	}
	var op = {
	    type: 1,
	    title:titleinfo,
	    shadeClose: false,
	    shade: [0.3, '#393D49'],
	    area: [width+"px",heigth+"px"],
	    content: $("#"+divid) //iframe的url
	};
	var selfwin = window.self;
	var topwin = this.getTopWindow();
	var index = topwin.showWindow(op); 
	
	var winjson = {"index":index,"win":selfwin,"data":data};
	
	topwin.getAppKit().winarray.push(winjson);
}

//弹出窗口
AppKitClass.prototype.openContentWindow = function(html, title, width,heigth,data,hideTitle) {
	var titleinfo =  [title, 'background-color:#019e97;'];
	if(hideTitle){
		titleinfo = false;
	}
	var op = {
	    type: 1,
	    title:titleinfo,
	    shadeClose: false,
	    shade: [0.3, '#393D49'],
	    area: [width+"px",heigth+"px"],
	    content: html //iframe的url
	};
	var selfwin = window.self;
	var topwin = this.getTopWindow();
	var index = topwin.showWindow(op); 
	
	var winjson = {"index":index,"win":selfwin,"data":data};
	
	topwin.getAppKit().winarray.push(winjson);
}



AppKitClass.prototype.getTopWindow=function(){
	 var obj=window.self;
	  while(true)
	  {
	   if(obj.document.getElementById("topflag"))
	   {
		   return obj;
	   }
	   	 obj=obj.window.parent;
	   }
}

AppKitClass.prototype.getInvokeWindowData=function(){
	var json = this.getInvokeWindowJson();
	//alert(AppKit.tostring(json));
	var data = json["data"];
	return data;
}

AppKitClass.prototype.getInvokeWindow=function(){
	var json = this.getInvokeWindowJson();
	var win = json["win"];
	return win;
}

AppKitClass.prototype.closeWindow = function() {
	var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
	var topwin = this.getTopWindow();
	for(var i=0;i<topwin.getAppKit().winarray.length;i++){
		var obj = topwin.getAppKit().winarray[i];
		var idx=obj["index"];
		if(index == idx){
			topwin[i]=null;
			break;
		}
	}
	
	parent.layer.close(index); //再执行关闭  
}


AppKitClass.prototype.getInvokeWindowJson = function() {
	var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
	var topwin = this.getTopWindow();
	var json = null;
	for(var i=0;i<topwin.getAppKit().winarray.length;i++){
		var obj = topwin.getAppKit().winarray[i];
		var idx=obj["index"];
		if(index == idx){
			json = obj;
			break;
		}
	}
	return json;
}




AppKitClass.prototype.getWinData = function(wName) {
}

AppKitClass.prototype.postJSON = function(url, data, callback) {
	this.callback = callback;
	
	if (jQuery.isFunction(data)) {
		callback = data;
		data = {};
	}
	var that = this;
	this.showRunning();
	return jQuery.ajax( {
		type : "POST",
		url : url,
		contentType : "application/x-www-form-urlencoded; charset=UTF-8",
		data : data,
		dataType : "json",
		processData : true,
		global : false,
		async:true,
		cache : false,
		error : function(xhr, st, e) {
			that.hideRunning();
			var msg = null;
			if (xhr.status != "200") {
				msg = "错误码:" + xhr.status + " 错误文本:" + xhr.statusText || "";
			} else {
				msg = xhr.responseText;
			}
			AppKit.showMessage(msg);
		},
		success :function(json){
			that.hideRunning();
			if(callback){
				callback(json);
			}
		} 
	});
}

AppKitClass.prototype.checkJSONResult = function(json, quietly,callback) {
	var errType = "9";
	var errMsg = "未确认的错误";
	if (typeof (json) !== 'undefined' && json !== null) {
		if (typeof (json.errortype) !== 'undefined' && json.errortype !== null) {
			errType = json.errortype;
			if (errType != "0") {
				errMsg = json.message;
			}
		}
	}
	
	if (errType != "0" && !quietly) {
		this.showTipMessage(errMsg,"",callback);
	}

	return errType === "0" ? true : false;
}


// 弹出确认框
AppKitClass.prototype.showConfirmMessage = function(callback, msg, title, cancelCallBack) {
	
	if (this.isnull(title) || !(this.isvalue(title))) {
		title = "温馨提示";
	}
	if (this.isnull(msg) || !(this.isvalue(msg))) {
		msg = "确定要执行该操作?";
	}
	var titleinfo =  [title, 'background-color:#019e97;'];
	
	layer.confirm(msg, {
		icon:3,
		title:titleinfo,
	    btn: ['确定','取消'], //按钮
	    shade: false //不显示遮罩
	}, function(index){
		layer.close(index);
		if(callback){
			callback();
		}
	}, function(){
		if(cancelCallBack){
			cancelCallBack();
		}
	});
}

// 弹出警告框
AppKitClass.prototype.showMessage = function(msg, title, callback) {
	if (this.isnull(title) || !(this.isvalue(title))) {
		title = "温馨提示";
	}
	var titleinfo =  [title, 'background-color:#019e97;'];
	
	layer.alert(msg,{icon:1,title:titleinfo}, function(index){
		layer.close(index);
		if(callback){
			callback();
		}
	});    
}

//弹出提示框
AppKitClass.prototype.showTipMessage = function(msg, title,callback,timeout) {
	if (this.isnull(title) || !(this.isvalue(title))) {
		title = "温馨提示";
	}
	var titleinfo =  [title, 'background-color:#019e97;'];
	
	if(!timeout){
		timeout = 1000;
	}
	layer.msg(msg, {
		title:titleinfo,
		//offset: 'rb',
	    icon: 1,
	    shade:0.3,
	    time: timeout //2秒关闭（如果不配置，默认是3秒）
	},function(){
		if(callback){
			callback();
		}
	}); 
}


AppKitClass.prototype.showRunning=function() {
	this.loadingIndex = layer.load(1, {shade: 0.3}); //0代表加载的风格，支持0-2
}

AppKitClass.prototype.hideRunning=function() {
	layer.close(this.loadingIndex); //又换了种风格，并且设定最长等待10秒 
}


AppKitClass.prototype.formTipValidate=function(id) {
	  this.validateForm = $("#"+id).Validform({
			tiptype:function(msg,o,cssctl){
				if(!o.obj.is("form")){
					var objtip=o.obj.siblings(".Validform_checktip");
					cssctl(objtip,o.type);
					objtip.text(msg);
				}
			}
		});
	  
	  return this.validateForm;
}

AppKitClass.prototype.getQueryParam=function(formid,queryField) {
	 var queryParams = {};
    var queryFields=queryField;
    queryParams['queryFields'] = queryFields;
    //普通的查询
    $('#'+formid).find(":input").each(function() {
		var val = $(this).val();
		if (queryParams[$(this).attr('name')]) {
			val = queryParams[$(this).attr('name')] + "," + $(this).val();
		}
		queryParams[$(this).attr('name')] = val;
	});

	// 普通的查询
	$('#'+formid).find(":input").each(function() {
		var condition = $(this).attr('condition');
		if (!condition) {
			condition = "";
		}
		var key = "query." + $(this).attr('name') + "||" + condition;
		queryParams[key] = queryParams[$(this).attr('name')];
	});
	return queryParams;
   
}



// 判断变量是否为空
AppKitClass.prototype.isEmptyVal = function(val) {
	if (val == null) {
		return true;
	}

	if (typeof (val) == 'undefined') {
		return true;
	}

	if (val == "") {
		return true;
	}
	var regu = "^[ ]+$";
	var re = new RegExp(regu);
	return re.test(val);
};

// 获取页面对象
AppKitClass.prototype.obj = function(name) {
	document.getElementById(name);
};

// 获取页面对象的值
AppKitClass.prototype.val = function(name, value) {
	var o = this.obj(name);
	if (o == null) {
		return "";
	}
	if (value != undefined) {
		o.value = value;
	}
	return o.value;
};

// 对页面对象设置焦点
AppKitClass.prototype.focus = function(name) {
	var o = this.obj(name);
	if (o == null) {
		return;
	}
	window.setTimeout(function() {
		o.focus();
	}, 0);
};

// 显示页面对象
AppKitClass.prototype.show = function(name) {
	var o = this.obj(name);
	if (o == null) {
		return;
	}

	o.style.display = "";
};

// 隐藏页面对象
AppKitClass.prototype.hide = function(name) {
	var o = this.obj(name);
	if (o == null) {
		return;
	}

	o.style.display = "none";
};

// 获取数组的长度
AppKitClass.prototype.getarraysize = function(o) {
	return this.isarray(o) ? o.length : -1;
};

// 将object对象转化为数组
AppKitClass.prototype.getarray = function(o) {
	return this.isarray(o) ? o : new Array();
};

// 获取字符串
AppKitClass.prototype.getstring = function(str) {
	return this.isvalue(str) ? String(str) : '';
};

// 获取字符串
AppKitClass.prototype.tostring = function(o) {
	var str = "";
	if (this.isvalue(o)) {
		var t = Object.prototype.toString.apply(o);

		if (t === '[object Array]') {
			var a = [];
			for ( var i = 0; i < o.length; i++) {
				a.push(this.tostring(o[i]));
			}
			str = '[' + a.join(',') + ']';

			a.length = 0;
			a = null;
		} else if (t === '[object Date]') {
			var y = o.getYear();
			if (y < 1900) {
				y += 1900;
			}
			var m = o.getMonth() + 1;
			str = y + "-" + this.lpad(m, 2, '0') + "-"
					+ this.lpad(o.getDate(), 2, '0') + " "
					+ this.lpad(o.getHours(), 2, '0') + ":"
					+ this.lpad(o.getMinutes(), 2, '0') + ":"
					+ this.lpad(o.getSeconds(), 2, '0');
		} else if (t === '[object Object]') {
			var a = [], k;
			for (k in o) {
				var vt = Object.prototype.toString.apply(o[k]);
				if (vt === '[object Array]' || vt === '[object Object]') {
					a.push('"' + k + '":' + this.tostring(o[k]));
				} else {
					a.push('"' + k + '":"' + this.tostring(o[k]) + '"');
				}
			}
			str = '{' + a.join(',') + '}';

			a.length = 0;
			a = null;
		} else {
			str = String(o);
		}
	}

	return str;
};

// 将json字符串转化成json对象
AppKitClass.prototype.tojson = function(str) {
	return eval("(" + str + ")");
};

// 左填充
AppKitClass.prototype.lpad = function(str, len, pad) {
	str = this.getstring(str);
	if (typeof (len) === "undefined") {
		var len = 0;
	}
	if (typeof (pad) === "undefined") {
		var pad = ' ';
	}

	if (len + 1 >= str.length) {
		str = Array(len + 1 - str.length).join(pad) + str;
	}

	return str;
};

// 右填充
AppKitClass.prototype.rpad = function(str, len, pad) {
	str = this.getstring(str);
	if (typeof (len) === "undefined") {
		var len = 0;
	}
	if (typeof (pad) === "undefined") {
		var pad = ' ';
	}

	if (len + 1 >= str.length) {
		str = str + Array(len + 1 - str.length).join(pad);
	}

	return str;
};



//长度
AppKitClass.prototype.length = function(text) {
	var len = 0;
	if(text){
		len = text.length;
	}
	return len;
};
// 消除空格
AppKitClass.prototype.trim = function(text) {
	return (text || "").replace(/^\s+|\s+$/g, "");
};

// 消除空格
AppKitClass.prototype.joinList = function(jsonarray, colName, sperator) {
	var strArray = new Array();
	if (jsonarray && jsonarray.length > 0) {
		for ( var i = 0; i < jsonarray.length; i++) {
			var js = jsonarray[i];
			if (js[colName]) {
				strArray.push(js[colName]);
			}
		}
	}
	return strArray.join(sperator);
};

AppKitClass.prototype.showLoading = function(show, deplay, message) {
	var loadingcss = {
		border : '1px solid #aaccee',
		padding : '10px',
		width : '220px',
		backgroundColor : '#ffffff',
		color : '#07519a'
	};

	var overlaycss = {
		backgroundColor : '#e6e6e6'
	};

	try {
		if (show === undefined || show === null) {
			show = true;
		}

		if (!message) {
			message = "程序正在运行，请稍候...";
		}

		if (show == true) {
			$.blockUI( {
						message : '<div id="divLoading"><img src="<%=path%>/resource/images/busy.gif" style="vertical-align : middle;"/><span style="font-size:12px;font-weight:bold;padding-left:3px;vertical-align : middle;">' + message + '</span></div>',
						css : loadingcss,
						overlayCSS : overlaycss
					});
		} else {
			var option = {};
			if (deplay === undefined || deplay === null) {
				deplay = 0;
			}
			option.fadeOut = deplay;
			$.unblockUI(option);

			$(document).css('cursor', 'auto');
		}
	} catch (e) {
		alert(e.message);
	}
}

AppKitClass.prototype.transferJsonToArray=function(jsoninfo) {
	if (!jsoninfo) {
		return;
	}
	jsoninfo = jsoninfo.replace("{", "");
	jsoninfo = jsoninfo.replace("}", "");
	return jsoninfo.split(",");
}


AppKitClass.prototype.formateDate=function(date,formatStr) {
	  var str = formatStr;    
      var Week = ['日','一','二','三','四','五','六'];   
        str=str.replace(/yyyy|YYYY/,date.getFullYear());    
        str=str.replace(/yy|YY/,(date.getYear() % 100)>9?(date.getYear() % 100).toString():(date.getYear() % 100));    
	    str=str.replace(/MM/,date.getMonth()>8?date.getMonth()+1:"0"+(date.getMonth()+1));    
	    str=str.replace(/M/g,date.getMonth());    
	    str=str.replace(/w|W/g,Week[date.getDay()]);    
	    str=str.replace(/dd|DD/,date.getDate()>9?date.getDate().toString():"0"+date.getDate());    
	    str=str.replace(/d|D/g,date.getDate());    
	    str=str.replace(/hh|HH/,date.getHours()>9?date.getHours().toString():"0"+date.getHours());    
	    str=str.replace(/h|H/g,date.getHours());    
	    str=str.replace(/mm/,date.getMinutes()>9?date.getMinutes().toString():"0"+date.getMinutes());    
	    str=str.replace(/m/g,date.getMinutes());    
	    str=str.replace(/ss|SS/,date.getSeconds()>9?date.getSeconds().toString():"0"+date.getSeconds());    
	    str=str.replace(/s|S/g,date.getSeconds()); 
	    str=str.replace(/i|I/g,date.getMilliseconds());  
	    return str; 
}

AppKitClass.prototype.getCurDate=function(){
	var str="yyyy-MM-dd";
	var now = this.formateDate(new Date(),str);
	return now;
}

AppKitClass.prototype.getCurDateTime=function(){
	var str="yyyy-MM-dd HH:mm:ss";
	var now =  this.formateDate(new Date(),str);
	return now;
}

AppKitClass.prototype.getCurTime=function(){
	var str="HH:mm:ss";
	var now =  this.formateDate(new Date(),str);
	return now;
}

AppKitClass.prototype.mergeJson=function(json1,json2){
	var json = {};
	
	if(json1){
		for(var i in json1){
			json[i] = json1[i];
		}
	}
	
	if(json2){
		for(var i in json2){
			json[i] = json2[i];
		}
	}
	return json;
}


AppKitClass.prototype.mergeJsonArray=function(json1,json2){
	var jsonArray = new Array();
	
	if(json1){
		for(var i=0;i<json1.length;i++){
			jsonArray.push(json1[i]);
		}
	}
	
	if(json2){
		for(var i=0;i<json2.length;i++){
			jsonArray.push(json2[i]);
		}
	}
	return jsonArray;
}


AppKitClass.prototype.isempty = function(val) {
	// 判断变量是否为空
	if (val == null) {
		return true;
	}

	if (typeof (val) == 'undefined') {
		return true;
	}

	if (val == "") {
		return true;
	}
	var regu = "^[ ]+$";
	var re = new RegExp(regu);
	return re.test(val);
};

//判断变量是否是一个值
AppKitClass.prototype.isvalue = function(o) {
	return typeof (o) !== 'undefined' && o !== null ? true : false;
};

//判断变量是否为一个数组
AppKitClass.prototype.isarray = function(o) {
	return this.isvalue(o) ? Object.prototype.toString.apply(o) === '[object Array]'
			: false;
};

//判断是否为null
AppKitClass.prototype.isnull = function(str) {
	if (str == "") {
		return true;
	}
	var regu = "^[ ]+$";
	var re = new RegExp(regu);
	return re.test(str);
};

//判断是否为数字
AppKitClass.prototype.isnum = function(str) {
	var regu = /^(\d+)$/;
	return regu.test(str);
};

//判断是否为整型
AppKitClass.prototype.isint = function(str) {
	var regu = /^[-]{0,1}[0-9]{1,}$/;
	return regu.test(this.val(str));
};

//判断是否为浮点型
AppKitClass.prototype.isdec = function(str) {
	if (this.isint(v))
		return true;
	var re = /^[-]{0,1}(\d+)[\.]+(\d+)$/;
	if (re.test(str)) {
		if (RegExp.$1 == 0 && RegExp.$2 == 0)
			return false;
		return true;
	} else {
		return false;
	}
};

AppKitClass.prototype.isEmail = function(str) {
	var myReg = /^[-_A-Za-z0-9]+@([_A-Za-z0-9]+\.)+[A-Za-z0-9]{2,3}$/; 
	if(myReg.test(str)) return true; 
	return false; 
};



AppKitClass.prototype.isip = function(strIP) {
	if (isNull(strIP)) return false;
	var re=/^(\d+)\.(\d+)\.(\d+)\.(\d+)$/g //匹配IP地址的正则表达式
	if(re.test(strIP))
	{
	if( RegExp.$1 <256 && RegExp.$2<256 && RegExp.$3<256 && RegExp.$4<256) return true;
	}
	return false; 
};

AppKitClass.prototype.renderRadios = function(id,array,valuekey,showkey,name,required) {
	var html = "";
	for(var i=0;i<array.length;i++){
		var value = array[i][valuekey];
		var show = array[i][showkey];
		html += "<input  type='radio' value='"+value+"' name='"+name+"' required='"+required+"'> "+show+" </input>";
	}
	$("#"+id).html(html);
}

AppKitClass.prototype.renderCheckBoxs = function(id,array,valuekey,showkey,name,required) {
	var html = "";
	for(var i=0;i<array.length;i++){
		var value = array[i][valuekey];
		var show = array[i][showkey];
		html += "<input style='vertical-align: middle;' type='checkbox' value='"+value+"' name='"+name+"' required='"+required+"'> "+show+" </input>";
	}
	$("#"+id).html(html);
}

AppKitClass.prototype.setCheckBoxValues = function(name,value) {
	var valuearray;
	if(value){
		valuearray = toString(value).split(',');
	}
	
	if(valuearray){
		for(var i=0;i<valuearray.length;i++){
			$("input[name='"+name+"']").each(function(){
				if($(this).attr('value') == valuearray[i]){
					$(this).attr("checked","true"); 
				}
			});
		}
	}
}
AppKitClass.prototype.getCheckBoxValues = function(name) {
	var chk_value =[]; 
	$('input[name="'+name+'"]:checked').each(function(){ 
		chk_value.push($(this).val()); 
	}); 
	if(chk_value.length==0){
		return null;
	}else{
		return chk_value.join(",");
	}
}


AppKitClass.prototype.setRadioValues = function(name,value) {
	$("input[name='"+name+"'][value="+value+"]").attr("checked",true); 
}

AppKitClass.prototype.getRadioValues = function(name) {
	var value =$("input[name='"+name+"']:checked").val();
	return value;
}

AppKitClass.prototype.getCodeFormatter = function(val,rec,json) {
	 var retval = val;

     for(var i in json){
         if(val == json[i].dataValue){
             retval = json[i].displayValue;
          }    
     }
     return retval;
};

AppKitClass.prototype.getCodeCmtFormatter = function(val,rec,json,data,name) {
	 var retval = val;

    for(var i in json){
        if(val == json[i][data]){
            retval = json[i][name];
         }    
    }
    return retval;
};

AppKitClass.prototype.getDateFormatter = function(val,rec) {
	 var retval = val;
	 
	 if(val){
		 retval = val.substring(0,10);
	 }
   return retval;
};


AppKitClass.prototype.getTimeFormatter = function(val,rec) {
	 var retval = val;
	 
	 if(val){
		 retval = val.substring(11,16);
	 }
    return retval;
};

AppKitClass.prototype.getMoneyFormatter = function(val,rec) {
	if(val){
		return parseFloat(val).toFixed(2);
	}
	return "0.00";
};


AppKitClass.prototype.getMoneyZeroFormatter = function(val,rec) {
	if(val){
		return parseFloat(val).toFixed(2);
	}
	return parseFloat(0).toFixed(2);
};

AppKitClass.prototype.parseJsonToParam = function(json) {
	var paraArray  = new Array();
	for(var i in json){
		if(json[i]){
			paraArray.push(i+"="+json[i]);
		}
	}
	return paraArray.join("&");
};


AppKit=new AppKitClass();

function getAppKit(){
	return AppKit;
}

function running(){
	AppKit.showRunning(true);
}

$.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [ o[this.name] ];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

String.prototype.startWith=function(str){  
    if(str==null||str==""||this.length==0||str.length>this.length)  
      return false;  
    if(this.substr(0,str.length)==str)  
      return true;  
    else  
      return false;  
    return true;  
}  
String.prototype.endWith=function(str){  
    if(str==null||str==""||this.length==0||str.length>this.length)  
      return false;  
    if(this.substring(this.length-str.length)==str)  
      return true;  
    else  
      return false;  
    return true;  
}  