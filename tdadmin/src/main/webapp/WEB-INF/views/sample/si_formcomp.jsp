<!DOCTYPE html>
<%@page import="com.tdcy.framework.util.SessionUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="biztag"%>

<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>配置管理中心</title>

	<meta name="viewport" content="widtd=device-widtd, initial-scale=1.0" />
	<jsp:include page="../hcommon.jsp"></jsp:include>
</head>
<script type="text/javascript">
	var curSearchInfo;
	var url;

	$(function() {
		 AppKit.formTipValidate("userForm");
		  $('#dateDiv').datepicker({
			     keyboardNavigation: false,
	           forceParse: false,
	           autoclose: true
		   });
	});
	
	function getform() {
		var info = $("#userForm").serializeObject();

		alert(AppKit.tostring(info));
	}
	
	function validateform() {
		var flag = $("#userForm").Validform();


console.log(flag);
	}
</script>
<body class="white-bg" style="margin-left: 5px;margin-top: 5px;" >
 <table class="table table-bordered  table-condensed dataTables-example dataTable no-footer " >
    <tbody>
      <tr>
        <td class="width-15 active text-right">
          <label>用户名:</label></td>
        <td class="width-35">test</td>
        <td class="width-15 active text-right">
          <label>
            姓名:</label></td>
	        <td class="width-35">
	        test
	        </td>
      </tr>
      <tr>
        <td class="width-15 active text-right">
          <label>
            <font color="red">*</font>邮箱:</label></td>
        <td class="width-35">
          <input name="email" class="form-control" datatype="e" nullmsg="请输入姓名！"  />
          <label class="Validform_checktip"></label>
        </td>
        <td class="width-15 active text-right">
          <label>
            <font color="red">*</font>联系电话:</label></td>
        <td class="width-35">ffffffffff
          <label class="Validform_checktip"></label>
        </td>
      </tr>
      <tr>
        <td class="active">
          <label class="pull-right">
            <font color="red">*</font>用户角色:</label></td>
        <td colspan="3">fffffffffff
      </tr>
      <tr>
        <td class="width-15 active">
          <label class="pull-right">组织机构:</label></td>
        <td colspan="3">
        kkkkkkkkkkkkk
      </tr>
    </tbody>
  </table>
  
	<form id="userForm" method="post" class="form-horizontal">
	  <table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
	    <tbody>
	      <tr>
	        <td class="width-15 active text-right">
	          <label>用户名:</label></td>
	        <td class="width-35">test</td>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>必输输入框:</label></td>
	        <td class="width-35">
	          <input name="realname" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
	        <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>邮箱:</label></td>
	        <td class="width-35">
	          <input name="email" class="form-control" datatype="e" errormsg="请输入邮箱！"  />
	          <div class="Validform_checktip"></div>
	        </td>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>手机:</label></td>
	        <td class="width-35">
	          <input name="phone" class="form-control" datatype="m" errormsg="请输入手机号码！" />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
	      <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>长度限制输入框:</label></td>
	        <td class="width-35">
	          <input name="email" class="form-control" datatype="*6-16" errormsg="请输入6到16位字符！"  />
	          <div class="Validform_checktip"></div>
	        </td>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>数字输入框:</label></td>
	        <td class="width-35">
	          <input name="phone" class="form-control" datatype="n" errormsg="请输入数字！" />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
	      <tr>
	        <td class="active">
	          <label class="pull-right">
	            <font color="red">*</font>日期：</label></td>
	        <td colspan="3">
	         <biztag:Comp-Date id="test" name="test"/>
	      </tr>
	        <tr>
	        <td class="width-15 active">
	          <label class="pull-right">下拉框：</label></td>
	        <td colspan="3">
	        <select class="form-control">
	        <option value="1">test</option>
	        </select>
	      </tr>
	        <tr>
	        <td class="width-15 active">
	          <label class="pull-right">radio</label></td>
	        <td colspan="3">
	       <input id="isshow1" name="isshow" class="i-checks required" type="radio" value="1" checked="checked">男
	       <input id="isshow2" name="isshow" class="i-checks required" type="radio" value="1" checked="checked">女
	        </td>
	      </tr>
	        <tr>
	        <td class="width-15 active">
	          <label class="pull-right">checkbox</label></td>
	        <td colspan="3">
	           
	           <input type="checkbox" value="Controller" name="generatorKeys" checked="checked">打篮球
	           </td>
	      </tr>
	      
	      <tr>
	        <td class="width-15 active">
	          <label class="pull-right">码表checkbox</label></td>
	        <td colspan="3">
	           <biztag:Comp-CodeCheckBox name="test" code="bank_name" defaultValue="1"/>
	           </td>
	      </tr>
	      
	      <tr>
	        <td class="width-15 active">
	          <label class="pull-right">码表radio</label></td>
	        <td colspan="3">
	   		  <biztag:Comp-CodeRadio name="test" code="bank_name" defaultValue="1"/>
	           </td>
	      </tr>
	      
	      <tr>
	        <td class="width-15 active">
	          <label class="pull-right">码表下拉</label></td>
	        <td colspan="3">
	    		 <biztag:Comp-CodeSelect code="bank_name" name="test" defaultValue="1"/>
	           </td>
	      </tr>
	      
	       <tr>
	        <td class="width-15 active">
	          <label class="pull-right">码表下拉</label></td>
	        <td colspan="3">
	        <biztag:Comp-DataSelect defaultValue="0" name="test" data="[{'dataValue' : 'all','displayValue' : '所有'}]"/>
	           </td>
	      </tr>
	      
	       <tr>
	        <td class="width-15 active">
	          <label class="pull-right">上传文件</label></td>
	        <td colspan="3">
	        <biztag:uploadFile saveUrl="${path}/file/uploadFile" extensions="*"/>
	           </td>
	      </tr>
	      
	       <tr>
	        <td class="width-15 active">
	          <label class="pull-right">上传图片</label></td>
	        <td colspan="3">
	        <biztag:uploadImage saveUrl="${path}/file/uploadFile" inputName="test"/>
	           </td>
	      </tr>
	      
	       <tr>
	        <td class="width-15 active">
	          <label class="pull-right">选择部门</label></td>
	        <td colspan="3">
	       		 <biztag:selectDept singleSelect="true"/>
	        </td>
	      </tr>
	    </tbody>
	  </table>
	</form>
	
	<div style="text-align: center;">
	     <button class="btn btn-sm btn-primary" onclick="validateform()"><i class="fa fa-plus"></i>验证</button>
		 <button class="btn btn-sm btn-primary" onclick="getform()"><i class="fa fa-plus"></i>获取表单数据</button>
	</div>
</body>
</html>