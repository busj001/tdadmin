<!DOCTYPE html>
<%@page import="com.tdcy.framework.util.SessionUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>配置管理中心</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jsp:include page="../hcommon.jsp"></jsp:include>
	<script type="text/javascript" src="${path }/resource/js/jquery.md5.js"></script>
<script type="text/javascript" src="${path }/resource/js/jquery.base64.js"></script>
</head>
<style type="text/css">
.body {
	padding: 20px;
	FONT: 12px/16px Tahoma, Arial, sans-serif;
}

.center {
	text-align: center;
}

.textarea {
	width: 99%;
	border: 1px solid #c5e2f2;
	overflow: visible;
	margin-top: 10px;
	margin-bottom: 10px;
	height: 200px;
}

.button {
	VERTICAL-ALIGN: middle;
	BORDER-BOTTOM: #c5e2f2 1px solid;
	BORDER-LEFT: #c5e2f2 1px solid;
	WIDTH: 90px;
	MARGIN-BOTTOM: 5px;
	BACKGROUND: #cde4f2;
	HEIGHT: 30px;
	MARGIN-LEFT: 5px;
	BORDER-TOP: #c5e2f2 1px solid;
	CURSOR: pointer;
	BORDER-RIGHT: #c5e2f2 1px solid;
}
</style>
<script type="text/javascript">
	$(function() {
		
	});
	
	function submitData(){
		var action = $('#action').val();
		if (action.length == 0) {
			alert('请输入action名称！');
			$('#action').focus();
			return;
		}
		
		var param = $('#param').val();
		if (param.length == 0) {
			alert('请输入请求参数！');
			$('#param').focus();
			return;
		}
		var param = AppKit.tojson(param);
		var appid = $("#appid").val();
		appid = $.base64.encode(appid);
		var timestamp = AppKit.formateDate(new Date(),"yyyyMMddhhmmss");
		var str = $("#username").val()+$("#password").val()+timestamp;
		var sign = $.md5(str);
		//var data = "params=" + param;
		//var data = param;
		var data = {};
		for(var i in param){
			if(AppKit.isarray(param[i])){
				data[i] = JSON.stringify(param[i]);
			}else{
				data[i] = param[i];
			}
		}
		var url = ".."+action+"?restflag=true&sign="+sign+"&timestamp="+timestamp+"&appid="+appid;
		$("#param2").val(url);
		
		AppKit.postJSON(url,data, function(json){
			$("#result").val(AppKit.tostring(json));
		})
	}
</script>
<body>
	<div align="center" class="container">
		<div align="left" >
		调用action名称：<input id="action" name="action" value="/rest/test" class="input" style="width: 300px;"></input>
	    	<br>AppID:<input id="appid" name="appid" value="APP123121" class="input"></input>
	    	<br>用户名：<input id="username" name="username" value="test" class="input"></input>
	    	<br>令牌：<input id="password" name="password" value="E10ADC3949BA59ABBE56E057F20F883E" class="input"></input>
	    	
		</div>
	    <div align="left" style="float: left;width: 40%;">
	        <textarea id="param" name="param" class="textarea" rows="15"
	        title="输入参数">
{"memid":"1"}</textarea>
	    </div>
	    <div style="float: left;padding-left: 10px;width: 40%;">
	        <textarea id="param2" name="param2" class="textarea" rows="15" 
	        title="实际提交参数">

	        </textarea>
	    </div>
	    <div style="clear: both;"></div>
	    <div>
	        <input class="button" id="btnSubmit" type="button" value="提交数据" onclick="submitData()" />
	    </div>
	    
	    <div>
	        <textarea id="result" name="result" class="textarea" rows="15" cols="120"
	        title="输出参数">

	        </textarea>
	    </div>
	    <div id="testhtml">
	    </div>
    </div>
</body>
</html>