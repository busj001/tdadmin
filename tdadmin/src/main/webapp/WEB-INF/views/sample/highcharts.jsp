<!DOCTYPE html>
<%@page import="com.tdcy.framework.util.SessionUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>配置管理中心</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jsp:include page="../hcommon.jsp"></jsp:include>
</head>
<link href="${path }/resource/js/easyui/themes/metro-blue/easyui.css" rel="stylesheet" type="text/css" />
<script src="${path }/resource/js/easyui/jquery.easyui.min.js"></script>

<script type="text/javascript" src="${path }/resource/js/Highcharts/highcharts.js"></script>
<script type="text/javascript" src="${path }/resource/js/Highcharts/modules/exporting.js"></script>
<script type="text/javascript" src="${path }/resource/js/Highcharts/highcharts-3d.js"></script>
<script type="text/javascript" src="${path }/resource/js/Highcharts/modules/data.js"></script>
<script type="text/javascript" src="${path }/resource/js/Highcharts/highcharts-more.js"></script>

<body style="width: 100%;height:100%;overflow:hidden;">
<div >
<table border="0">
	<tr align="center">
		<td>
		  	<div id="tabs" class="easyui-tabs" style="width: 1600px;" data-options="tabHeight:44">
				<div title="3D饼图" style="padding:10px;">
					  <div id="3DPIE" style="width: 1000px; height: 600px; margin: 0 auto"></div>
				</div>
				<script type="text/javascript"> 
				//start 3D饼图
				$(function () {
				    $('#3DPIE').highcharts({
				        chart: {
				            type: 'pie',
				            options3d: {
				                enabled: true,
				                alpha: 45,
				                beta: 0
				            }
				        },
				        title: {
				            text: 'Browser market shares at a specific website, 2014'
				        },
				        tooltip: {
				            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
				        },
				        plotOptions: {
				            pie: {
				                allowPointSelect: true,
				                cursor: 'pointer',
				                depth: 35,
				                dataLabels: {
				                    enabled: true,
				                    format: '{point.name}'
				                }
				            }
				        },
				        series: [{
				            type: 'pie',
				            name: 'Browser share',
				            data: [
				                ['Firefox',   45.0],
				                ['IE',       26.8],
				                {
				                    name: 'Chrome',
				                    y: 12.8,
				                    sliced: true,
				                    selected: true
				                },
				                ['Safari',    8.5],
				                ['Opera',     6.2],
				                ['Others',   0.7]
				            ]
				        }]
				    });
				});//end 3D饼图
				</script>
				<div title="3D双饼图" style="padding:10px;">
					   <div id="3DTWOPIE" style="width: 1000px; height: 600px;"></div>
				</div>
				<script type="text/javascript">
				//start  3D two pie
				$(function () {
				    $('#3DTWOPIE').highcharts({
				        chart: {
				            type: 'pie',
				            options3d: {
				                enabled: true,
				                alpha: 45
				            }
				        },
				        title: {
				            text: 'Contents of Highsoft\'s weekly fruit delivery'
				        },
				        subtitle: {
				            text: '3D donut in Highcharts'
				        },
				        plotOptions: {
				            pie: {
				                innerSize: 100,
				                depth: 45
				            }
				        },
				        series: [{
				            name: 'Delivered amount',
				            data: [
				                ['Bananas', 8],
				                ['Kiwi', 3],
				                ['Mixed nuts', 1],
				                ['Oranges', 6],
				                ['Apples', 8],
				                ['Pears', 4], 
				                ['Clementines', 4],
				                ['Reddish (bag)', 1],
				                ['Grapes (bunch)', 1]
				            ]
				        }]
				    });
				});
				//end 3D two pie
				</script>
				<div title="柱状图" style="padding:10px;">
					   <div id="zhuzhuangtu" style="width: 1000px; height: 600px;"></div>
				</div>
				<script type="text/javascript">
				// start 柱状图
				$(function () {
				    $('#zhuzhuangtu').highcharts({
				        chart: {
				            type: 'column'
				        },
				        title: {
				            text: 'Monthly Average Rainfall'
				        },
				        subtitle: {
				            text: 'Source: WorldClimate.com'
				        },
				        xAxis: {
				            categories: [
				                'Jan',
				                'Feb',
				                'Mar',
				                'Apr',
				                'May',
				                'Jun',
				                'Jul',
				                'Aug',
				                'Sep',
				                'Oct',
				                'Nov',
				                'Dec'
				            ]
				        },
				        yAxis: {
				            min: 0,
				            title: {
				                text: 'Rainfall (mm)'
				            }
				        },
				        tooltip: {
				            headerFormat: '<span style="font-size:10px">{point.key}</span>',
				            pointFormat: '' +
				                '',
				            footerFormat: '<table><tbody><tr><td style="color:{series.color};padding:0">{series.name}: </td><td style="padding:0"><b>{point.y:.1f} mm</b></td></tr></tbody></table>',
				            shared: true,
				            useHTML: true
				        },
				        plotOptions: {
				            column: {
				                pointPadding: 0.2,
				                borderWidth: 0
				            }
				        },
				        series: [{
				            name: 'Tokyo',
				            data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]

				        }, {
				            name: 'New York',
				            data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]

				        }, {
				            name: 'London',
				            data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]

				        }, {
				            name: 'Berlin',
				            data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]

				        }]
				    });
				});
				//end 柱状图
				</script>
				<div title="包含负值的柱状图" style="padding:10px;">
					   <div id="fuzhuzhuangtu" style="width: 1000px; height: 600px;"></div>
				</div>
				<script type="text/javascript">
				//start 包含负值的柱状图
				$(function () {
				    $('#fuzhuzhuangtu').highcharts({
				        chart: {
				            type: 'column'
				        },
				        title: {
				            text: 'Column chart with negative values'
				        },
				        xAxis: {
				            categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
				        },
				        credits: {
				            enabled: false
				        },
				        series: [{
				            name: 'John',
				            data: [5, 3, 4, 7, 2]
				        }, {
				            name: 'Jane',
				            data: [2, -2, -3, 2, 1]
				        }, {
				            name: 'Joe',
				            data: [3, 4, 4, -2, 5]
				        }]
				    });
				});				
				//end 包含负值的柱状图
				
				</script>
				<div title="通过表格获取值的柱状图" style="padding:10px;">
					   <div id="zhuzhuangtufromtable" style="width: 1000px; height: 600px;"></div>
					  <table id="datatable" style="margin-left:20px;" class="table table-bordered table-striped">
					    <thead>
					      <tr>
					        <th></th>
					        <th>Jane</th>
					        <th>John</th>
					      </tr>
					    </thead>
					    <tbody>
					      <tr>
					        <th>Apples</th>
					        <td>3</td>
					        <td>4</td>
					      </tr>
					      <tr>
					        <th>Pears</th>
					        <td>2</td>
					        <td>0</td>
					      </tr>
					      <tr>
					        <th>Plums</th>
					        <td>5</td>
					        <td>11</td>
					      </tr>
					      <tr>
					        <th>Bananas</th>
					        <td>1</td>
					        <td>1</td>
					      </tr>
					      <tr>
					        <th>Oranges</th>
					        <td>2</td>
					        <td>4</td>
					      </tr>
					    </tbody>
					  </table>
				</div>
				<script type="text/javascript">
				//start 通过表格获取值的柱状图
				$(document).ready(function(){
						
				    $('#zhuzhuangtufromtable').highcharts({
				        data: {
				            table: document.getElementById('datatable')
				        },
				        chart: {
				            type: 'column'
				        },
				        title: {
				            text: 'Data extracted from a HTML table in the page'
				        },
				        yAxis: {
				            allowDecimals: false,
				            title: {
				                text: 'Units'
				            }
				        },
				        tooltip: {
				            formatter: function() {
				                return '<b>'+ this.series.name +'</b><br/>'+
				                    this.y +' '+ this.x;
				            }
				        }
				    });
				});		
				//end 通过表格获取值的柱状图
				
				</script>
				
				<div title="显示值的柱状图" style="padding:10px;">
					   <div id="displayvaluezhuzhuangtu" style="width: 1000px; height: 600px;"></div>
				</div>
				<script type="text/javascript">
				//start 显示值柱状图
				$(function () {
				    $('#displayvaluezhuzhuangtu').highcharts({
				        chart: {
				            type: 'column',
				            margin: [ 50, 50, 100, 80]
				        },
				        title: {
				            text: 'Worlds largest cities per 2008'
				        },
				        xAxis: {
				            categories: [
				                'Tokyo',
				                'Jakarta',
				                'New York',
				                'Seoul',
				                'Manila',
				                'Mumbai',
				                'Sao Paulo',
				                'Mexico City',
				                'Dehli',
				                'Osaka',
				                'Cairo',
				                'Kolkata',
				                'Los Angeles',
				                'Shanghai',
				                'Moscow',
				                'Beijing',
				                'Buenos Aires',
				                'Guangzhou',
				                'Shenzhen',
				                'Istanbul'
				            ],
				            labels: {
				                rotation: -45,
				                align: 'right',
				                style: {
				                    fontSize: '13px',
				                    fontFamily: 'Verdana, sans-serif'
				                }
				            }
				        },
				        yAxis: {
				            min: 0,
				            title: {
				                text: 'Population (millions)'
				            }
				        },
				        legend: {
				            enabled: false
				        },
				        tooltip: {
				            pointFormat: 'Population in 2008: <b>{point.y:.1f} millions</b>',
				        },
				        series: [{
				            name: 'Population',
				            data: [34.4, 21.8, 20.1, 20, 19.6, 19.5, 19.1, 18.4, 18,
				                17.3, 16.8, 15, 14.7, 14.5, 13.3, 12.8, 12.4, 11.8,
				                11.7, 11.2],
				            dataLabels: {
				                enabled: true,
				                rotation: -90,
				                color: '#FFFFFF',
				                align: 'right',
				                x: 4,
				                y: 10,
				                style: {
				                    fontSize: '13px',
				                    fontFamily: 'Verdana, sans-serif',
				                    textShadow: '0 0 3px black'
				                }
				            }
				        }]
				    });
				});
				// end 显示值柱状图
				</script>
				<div title="柱形堆叠图" style="padding:10px;">
					   <div id="zhuxingduidietu" style="width: 1000px; height: 600px;"></div>
				</div>
				<script type="text/javascript">
				//start 柱形堆叠图
				$(function () {
				    $('#zhuxingduidietu').highcharts({
				        chart: {
				            type: 'column'
				        },
				        title: {
				            text: 'Stacked column chart'
				        },
				        xAxis: {
				            categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
				        },
				        yAxis: {
				            min: 0,
				            title: {
				                text: 'Total fruit consumption'
				            },
				            stackLabels: {
				                enabled: true,
				                style: {
				                    fontWeight: 'bold',
				                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
				                }
				            }
				        },
				        legend: {
				            align: 'right',
				            x: -70,
				            verticalAlign: 'top',
				            y: 20,
				            floating: true,
				            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white',
				            borderColor: '#CCC',
				            borderWidth: 1,
				            shadow: false
				        },
				        tooltip: {
				            formatter: function() {
				                return '<b>'+ this.x +'</b><br/>'+
				                    this.series.name +': '+ this.y +'<br/>'+
				                    'Total: '+ this.point.stackTotal;
				            }
				        },
				        plotOptions: {
				            column: {
				                stacking: 'normal',
				                dataLabels: {
				                    enabled: true,
				                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
				                }
				            }
				        },
				        series: [{
				            name: 'John',
				            data: [5, 3, 4, 7, 2]
				        }, {
				            name: 'Jane',
				            data: [2, 2, 3, 2, 1]
				        }, {
				            name: 'Joe',
				            data: [3, 4, 4, 2, 5]
				        }]
				    });
				});				

				//end 柱形堆叠图
				
				
				</script>
				<div title="分组柱形堆叠图" style="padding:10px;">
					   <div id="fenzuzhuxingduidietu" style="width: 1000px; height: 600px;"></div>
				</div>
				<script type="text/javascript">
				//start 分组柱形堆叠图
				$(function () {
				    $('#fenzuzhuxingduidietu').highcharts({
				        chart: {
				            type: 'column'
				        },
				        title: {
				            text: 'Total fruit consumtion, grouped by gender'
				        },

				        xAxis: {
				            categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
				        },

				        yAxis: {
				            allowDecimals: false,
				            min: 0,
				            title: {
				                text: 'Number of fruits'
				            }
				        },

				        tooltip: {
				            formatter: function() {
				                return '<b>'+ this.x +'</b><br/>'+
				                    this.series.name +': '+ this.y +'<br/>'+
				                    'Total: '+ this.point.stackTotal;
				            }
				        },

				        plotOptions: {
				            column: {
				                stacking: 'normal'
				            }
				        },

				        series: [{
				            name: 'John',
				            data: [5, 3, 4, 7, 2],
				            stack: 'male'
				        }, {
				            name: 'Joe',
				            data: [3, 4, 4, 2, 5],
				            stack: 'male'
				        }, {
				            name: 'Jane',
				            data: [2, 5, 6, 2, 1],
				            stack: 'female'
				        }, {
				            name: 'Janet',
				            data: [3, 0, 4, 4, 3],
				            stack: 'female'
				        }]
				    });
				});				
				//end 柱形堆叠图
				
				</script>
				<div title="百分比堆栈柱状图" style="padding:10px;">
					   <div id="percentduizhanzhuzhuangtu" style="width: 1000px; height: 600px;"></div>
				</div>
				<script type="text/javascript">
				$(function () {
				    $('#percentduizhanzhuzhuangtu').highcharts({
				        chart: {
				            type: 'column'
				        },
				        title: {
				            text: 'Stacked column chart'
				        },
				        xAxis: {
				            categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
				        },
				        yAxis: {
				            min: 0,
				            title: {
				                text: 'Total fruit consumption'
				            }
				        },
				        tooltip: {
				            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
				            shared: true
				        },
				        plotOptions: {
				            column: {
				                stacking: 'percent'
				            }
				        },
				            series: [{
				            name: 'John',
				            data: [5, 3, 4, 7, 2]
				        }, {
				            name: 'Jane',
				            data: [2, 2, 3, 2, 1]
				        }, {
				            name: 'Joe',
				            data: [3, 4, 4, 2, 5]
				        }]
				    });
				});				
				</script>
				
				
				<div title="条形图" style="padding:10px;">
					   <div id="tiaoxingtu" style="width: 1000px; height: 600px;"></div>
				</div>
				<script type="text/javascript">
					$(function () {                                                                
					    $('#tiaoxingtu').highcharts({                                           
					        chart: {                                                           
					            type: 'bar'                                                    
					        },                                                                 
					        title: {                                                           
					            text: 'Historic World Population by Region'                    
					        },                                                                 
					        subtitle: {                                                        
					            text: 'Source: Wikipedia.org'                                  
					        },                                                                 
					        xAxis: {                                                           
					            categories: ['Africa', 'America', 'Asia', 'Europe', 'Oceania'],
					            title: {                                                       
					                text: null                                                 
					            }                                                              
					        },                                                                 
					        yAxis: {                                                           
					            min: 0,                                                        
					            title: {                                                       
					                text: 'Population (millions)',                             
					                align: 'high'                                              
					            },                                                             
					            labels: {                                                      
					                overflow: 'justify'                                        
					            }                                                              
					        },                                                                 
					        tooltip: {                                                         
					            valueSuffix: ' millions'                                       
					        },                                                                 
					        plotOptions: {                                                     
					            bar: {                                                         
					                dataLabels: {                                              
					                    enabled: true                                          
					                }                                                          
					            }                                                              
					        },                                                                 
					        legend: {                                                          
					            layout: 'vertical',                                            
					            align: 'right',                                                
					            verticalAlign: 'top',                                          
					            x: -40,                                                        
					            y: 100,                                                        
					            floating: true,                                                
					            borderWidth: 1,                                                
					            backgroundColor: '#FFFFFF',                                    
					            shadow: true                                                   
					        },                                                                 
					        credits: {                                                         
					            enabled: false                                                 
					        },                                                                 
					        series: [{                                                         
					            name: 'Year 1800',                                             
					            data: [107, 31, 635, 203, 2]                                   
					        }, {                                                               
					            name: 'Year 1900',                                             
					            data: [133, 156, 947, 408, 6]                                  
					        }, {                                                               
					            name: 'Year 2008',                                             
					            data: [973, 914, 4054, 732, 34]                                
					        }]                                                                 
					    });                                                                    
					});                                                                                                                                              						
				</script>
				<div title="堆叠条形图" style="padding:10px;">
					   <div id="duitietiaoxingtu" style="width: 1000px; height: 600px;"></div>
				</div>
				<script type="text/javascript">
				$(function () {
				    $('#duitietiaoxingtu').highcharts({
				        chart: {
				            type: 'bar'
				        },
				        title: {
				            text: 'Stacked bar chart'
				        },
				        xAxis: {
				            categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
				        },
				        yAxis: {
				            min: 0,
				            title: {
				                text: 'Total fruit consumption'
				            }
				        },
				        legend: {
				            backgroundColor: '#FFFFFF',
				            reversed: true
				        },
				        plotOptions: {
				            series: {
				                stacking: 'normal'
				            }
				        },
				            series: [{
				            name: 'John',
				            data: [5, 3, 4, 7, 2]
				        }, {
				            name: 'Jane',
				            data: [2, 2, 3, 2, 1]
				        }, {
				            name: 'Joe',
				            data: [3, 4, 4, 2, 5]
				        }]
				    });
				});				                                                                                                                                          						
				</script>
				<div title="饼图" style="padding:10px;">
					   <div id="pietu" style="width: 1000px; height: 600px;"></div>
				</div>
				<script type="text/javascript">
				$(function () {
				    $('#pietu').highcharts({
				        chart: {
				            plotBackgroundColor: null,
				            plotBorderWidth: null,
				            plotShadow: false
				        },
				        title: {
				            text: 'Browser market shares at a specific website, 2010'
				        },
				        tooltip: {
				    	    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
				        },
				        plotOptions: {
				            pie: {
				                allowPointSelect: true,
				                cursor: 'pointer',
				                dataLabels: {
				                    enabled: true,
				                    color: '#000000',
				                    connectorColor: '#000000',
				                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
				                }
				            }
				        },
				        series: [{
				            type: 'pie',
				            name: 'Browser share',
				            data: [
				                ['Firefox',   45.0],
				                ['IE',       26.8],
				                {
				                    name: 'Chrome',
				                    y: 12.8,
				                    sliced: true,
				                    selected: true
				                },
				                ['Safari',    8.5],
				                ['Opera',     6.2],
				                ['Others',   0.7]
				            ]
				        }]
				    });
				});						                                                                                                                                          						
				</script>
				
				<div title="带图例饼图" style="padding:10px;">
					   <div id="daituliepietu" style="width: 1000px; height: 600px;"></div>
				</div>
				<script type="text/javascript">
				$(function () {
				    var chart;
				    $(document).ready(function () {
				    	// Build the chart
				        $('#daituliepietu').highcharts({
				            chart: {
				                plotBackgroundColor: null,
				                plotBorderWidth: null,
				                plotShadow: false
				            },
				            title: {
				                text: 'Browser market shares at a specific website, 2014'
				            },
				            tooltip: {
				        	    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
				            },
				            plotOptions: {
				                pie: {
				                    allowPointSelect: true,
				                    cursor: 'pointer',
				                    dataLabels: {
				                        enabled: false
				                    },
				                    showInLegend: true
				                }
				            },
				            series: [{
				                type: 'pie',
				                name: 'Browser share',
				                data: [
				                    ['Firefox',   45.0],
				                    ['IE',       26.8],
				                    {
				                        name: 'Chrome',
				                        y: 12.8,
				                        sliced: true,
				                        selected: true
				                    },
				                    ['Safari',    8.5],
				                    ['Opera',     6.2],
				                    ['Others',   0.7]
				                ]
				            }]
				        });
				    });
				    
				});						                                                                                                                                          						
				</script>
				
				
				<div title="双饼图" style="padding:10px;">
					   <div id="shuangpietu" style="width: 1000px; height: 600px;"></div>
				</div>
				<script type="text/javascript">
				$(function () {
				    
			        var colors = Highcharts.getOptions().colors,
			            categories = ['MSIE', 'Firefox', 'Chrome', 'Safari', 'Opera'],
			            name = 'Browser brands',
			            data = [{
			                    y: 55.11,
			                    color: colors[0],
			                    drilldown: {
			                        name: 'MSIE versions',
			                        categories: ['MSIE 6.0', 'MSIE 7.0', 'MSIE 8.0', 'MSIE 9.0'],
			                        data: [10.85, 7.35, 33.06, 2.81],
			                        color: colors[0]
			                    }
			                }, {
			                    y: 21.63,
			                    color: colors[1],
			                    drilldown: {
			                        name: 'Firefox versions',
			                        categories: ['Firefox 2.0', 'Firefox 3.0', 'Firefox 3.5', 'Firefox 3.6', 'Firefox 4.0'],
			                        data: [0.20, 0.83, 1.58, 13.12, 5.43],
			                        color: colors[1]
			                    }
			                }, {
			                    y: 11.94,
			                    color: colors[2],
			                    drilldown: {
			                        name: 'Chrome versions',
			                        categories: ['Chrome 5.0', 'Chrome 6.0', 'Chrome 7.0', 'Chrome 8.0', 'Chrome 9.0',
			                            'Chrome 10.0', 'Chrome 11.0', 'Chrome 12.0'],
			                        data: [0.12, 0.19, 0.12, 0.36, 0.32, 9.91, 0.50, 0.22],
			                        color: colors[2]
			                    }
			                }, {
			                    y: 7.15,
			                    color: colors[3],
			                    drilldown: {
			                        name: 'Safari versions',
			                        categories: ['Safari 5.0', 'Safari 4.0', 'Safari Win 5.0', 'Safari 4.1', 'Safari/Maxthon',
			                            'Safari 3.1', 'Safari 4.1'],
			                        data: [4.55, 1.42, 0.23, 0.21, 0.20, 0.19, 0.14],
			                        color: colors[3]
			                    }
			                }, {
			                    y: 2.14,
			                    color: colors[4],
			                    drilldown: {
			                        name: 'Opera versions',
			                        categories: ['Opera 9.x', 'Opera 10.x', 'Opera 11.x'],
			                        data: [ 0.12, 0.37, 1.65],
			                        color: colors[4]
			                    }
			                }];
			    
			    
			        // Build the data arrays
			        var browserData = [];
			        var versionsData = [];
			        for (var i = 0; i < data.length; i++) {
			    
			            // add browser data
			            browserData.push({
			                name: categories[i],
			                y: data[i].y,
			                color: data[i].color
			            });
			    
			            // add version data
			            for (var j = 0; j < data[i].drilldown.data.length; j++) {
			                var brightness = 0.2 - (j / data[i].drilldown.data.length) / 5 ;
			                versionsData.push({
			                    name: data[i].drilldown.categories[j],
			                    y: data[i].drilldown.data[j],
			                    color: Highcharts.Color(data[i].color).brighten(brightness).get()
			                });
			            }
			        }
			    
			        // Create the chart
			        $('#shuangpietu').highcharts({
			            chart: {
			                type: 'pie'
			            },
			            title: {
			                text: 'Browser market share, April, 2011'
			            },
			            yAxis: {
			                title: {
			                    text: 'Total percent market share'
			                }
			            },
			            plotOptions: {
			                pie: {
			                    shadow: false,
			                    center: ['50%', '50%']
			                }
			            },
			            tooltip: {
			        	    valueSuffix: '%'
			            },
			            series: [{
			                name: 'Browsers',
			                data: browserData,
			                size: '60%',
			                dataLabels: {
			                    formatter: function() {
			                        return this.y > 5 ? this.point.name : null;
			                    },
			                    color: 'white',
			                    distance: -30
			                }
			            }, {
			                name: 'Versions',
			                data: versionsData,
			                size: '80%',
			                innerSize: '60%',
			                dataLabels: {
			                    formatter: function() {
			                        // display only if larger than 1
			                        return this.y > 1 ? '<b>'+ this.point.name +':</b> '+ this.y +'%'  : null;
			                    }
			                }
			            }]
			        });
			    });
			    				                                                                                                                                          						
				</script>
				<div title="动态刷新" style="padding:10px;">
					   <div id="dongtaishuaxin" style="width: 1000px; height: 600px;"></div>
				</div>
				<script type="text/javascript">
				$(function () {                                                                     
				    $(document).ready(function() {                                                  
				        Highcharts.setOptions({                                                     
				            global: {                                                               
				                useUTC: false                                                       
				            }                                                                       
				        });                                                                         
				                                                                                    
				        var chart;                                                                  
				        $('#dongtaishuaxin').highcharts({                                                
				            chart: {                                                                
				                type: 'spline',                                                     
				                animation: Highcharts.svg, // don't animate in old IE               
				                marginRight: 10,                                                    
				                events: {                                                           
				                    load: function() {                                              
				                                                                                    
				                        // set up the updating of the chart each second             
				                        var series = this.series[0];                                
				                        setInterval(function() {                                    
				                            var x = (new Date()).getTime(), // current time         
				                                y = Math.random();                                  
				                            series.addPoint([x, y], true, true);                    
				                        }, 1000);                                                   
				                    }                                                               
				                }                                                                   
				            },                                                                      
				            title: {                                                                
				                text: 'Live random data'                                            
				            },                                                                      
				            xAxis: {                                                                
				                type: 'datetime',                                                   
				                tickPixelInterval: 150                                              
				            },                                                                      
				            yAxis: {                                                                
				                title: {                                                            
				                    text: 'Value'                                                   
				                },                                                                  
				                plotLines: [{                                                       
				                    value: 0,                                                       
				                    width: 1,                                                       
				                    color: '#808080'                                                
				                }]                                                                  
				            },                                                                      
				            tooltip: {                                                              
				                formatter: function() {                                             
				                        return '<b>'+ this.series.name +'</b><br>'+                
				                        Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) +'<br>'+
				                        Highcharts.numberFormat(this.y, 2);                         
				                }                                                                   
				            },                                                                      
				            legend: {                                                               
				                enabled: false                                                      
				            },                                                                      
				            exporting: {                                                            
				                enabled: false                                                      
				            },                                                                      
				            series: [{                                                              
				                name: 'Random data',                                                
				                data: (function() {                                                 
				                    // generate an array of random data                             
				                    var data = [],                                                  
				                        time = (new Date()).getTime(),                              
				                        i;                                                          
				                                                                                    
				                    for (i = -19; i <= 0; i++) {                                    
				                        data.push({                                                 
				                            x: time + i * 1000,                                     
				                            y: Math.random()                                        
				                        });                                                         
				                    }                                                               
				                    return data;                                                    
				                })()                                                                
				            }]                                                                      
				        });                                                                         
				    });  
				});     			                                                                                                                                          						
				</script>
				
				
				<div title="点击按钮切换图表" style="padding:10px;">
					   <div id="qiehuantu" style="width: 1000px; height: 600px;"></div>
					   <div style="margin:10px 0px 10px 20px;">
						      点击按钮切换图表：
						      <button class="change">line</button>
						      <button class="change">spline</button>
						      <button class="change">pie</button>
						      <button class="change">area</button>
						      <button class="change">column</button>
						      <button class="change">areaspline</button>
						      <button class="change">bar</button>
						      <button class="change">scatter</button>
						  </div>
				</div>
				<script type="text/javascript">
				var options = {
						chart: {
						    plotBackgroundColor: null,
						    plotBorderWidth: null,
						    plotShadow: false,
						    renderTo:'qiehuantu'
						},
						title: {
						    text: 'Browser market shares at a specific website, 2010'
						},
						tooltip: {
						  pointFormat: '{series.name}: <b>{point.y}</b>'
						},
						labels:{
							items:[{
								html:'<a href="http://www.52wulian.org" target="_blank">HighCharts</a>',
								style: {
									left:'532px',
									top:'160px',				
								}
							}],
							style:{
								color:'red',
								fontSize:45,
								fontWeight:'bold',
								zIndex:1000
							}
						},
						series: [{
						    name: 'Browser share',
						    data: [
						        ['Firefox',   45.0],
						        ['IE',       26.8],
						        {
						            name: 'Chrome',
						            y: 12.8,
						            sliced: true,
						            selected: true
						        },
						        ['Safari',    8.5],
						        ['Opera',     6.2],
						        ['Others',   0.7]
						    ]
						}]
					};

					$(document).ready(function(){
						var chart = new Highcharts.Chart(options);
						
						$("button.change").click(function(){
							var type = $(this).html();
							
							if(type == "pie") {
								options.tooltip.pointFormat = '{series.name}: <b>{point.percentage:.1f}%</b>';
							}else {
								options.tooltip.pointFormat = '{series.name}: <b>{point.y}</b>';
							}
							options.chart.type = type;
							chart = new Highcharts.Chart(options);
						});
					});							                                                                                                                                          						
				</script>
				
				<div title="点击+增加一个点" style="padding:10px;">
					   <div id="clickandpoint" style="width: 1000px; height: 600px;"></div>
				</div>
				<script type="text/javascript">
				$(function () {
			        $('#clickandpoint').highcharts({
			            chart: {
			                type: 'scatter',
			                margin: [70, 50, 60, 80],
			                events: {
			                    click: function(e) {
			                        // find the clicked values and the series
			                        var x = e.xAxis[0].value,
			                            y = e.yAxis[0].value,
			                            series = this.series[0];
			    
			                        // Add it
			                        series.addPoint([x, y]);
			    
			                    }
			                }
			            },
			            title: {
			                text: 'User supplied data'
			            },
			            subtitle: {
			                text: 'Click the plot area to add a point. Click a point to remove it.'
			            },
			            xAxis: {
			                gridLineWidth: 1,
			                minPadding: 0.2,
			                maxPadding: 0.2,
			                maxZoom: 60
			            },
			            yAxis: {
			                title: {
			                    text: 'Value'
			                },
			                minPadding: 0.2,
			                maxPadding: 0.2,
			                maxZoom: 60,
			                plotLines: [{
			                    value: 0,
			                    width: 1,
			                    color: '#808080'
			                }]
			            },
			            legend: {
			                enabled: false
			            },
			            exporting: {
			                enabled: false
			            },
			            plotOptions: {
			                series: {
			                    lineWidth: 1,
			                    point: {
			                        events: {
			                            'click': function() {
			                                if (this.series.data.length > 1) this.remove();
			                            }
			                        }
			                    }
			                }
			            },
			            series: [{
			                data: [[20, 20], [80, 80]]
			            }]
			        });
			    });	                                                                                                                                          						
				</script>
				<div title="柱状、曲线、饼图混合图" style="padding:10px;">
					   <div id="Columnlineandpie" style="width: 1000px; height: 600px;"></div>
				</div>
				<script type="text/javascript">
				$(function () {
				    $('#Columnlineandpie').highcharts({
				        title: {
				            text: 'Combination chart'
				        },
				        xAxis: {
				            categories: ['Apples', 'Oranges', 'Pears', 'Bananas', 'Plums']
				        },
				        labels: {
				            items: [{
				                html: 'Total fruit consumption',
				                style: {
				                    left: '50px',
				                    top: '18px',
				                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
				                }
				            }]
				        },
				        series: [{
				            type: 'column',
				            name: 'Jane',
				            data: [3, 2, 1, 3, 4]
				        }, {
				            type: 'column',
				            name: 'John',
				            data: [2, 3, 5, 7, 6]
				        }, {
				            type: 'column',
				            name: 'Joe',
				            data: [4, 3, 3, 9, 0]
				        }, {
				            type: 'spline',
				            name: 'Average',
				            data: [3, 2.67, 3, 6.33, 3.33],
				            marker: {
				            	lineWidth: 2,
				            	lineColor: Highcharts.getOptions().colors[3],
				            	fillColor: 'white'
				            }
				        }, {
				            type: 'pie',
				            name: 'Total consumption',
				            data: [{
				                name: 'Jane',
				                y: 13,
				                color: Highcharts.getOptions().colors[0] // Jane's color
				            }, {
				                name: 'John',
				                y: 23,
				                color: Highcharts.getOptions().colors[1] // John's color
				            }, {
				                name: 'Joe',
				                y: 19,
				                color: Highcharts.getOptions().colors[2] // Joe's color
				            }],
				            center: [100, 80],
				            size: 100,
				            showInLegend: false,
				            dataLabels: {
				                enabled: false
				            }
				        }]
				    });
				});                                                                                                                                    						
				</script>
			</div>
		</td>	
	</tr>
</table>
</div>
</body>
</html>
