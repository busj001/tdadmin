<!DOCTYPE html>
<%@page import="com.tdcy.framework.util.SessionUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>配置管理中心</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jsp:include page="../hcommon.jsp"></jsp:include>
</head>
<script type="text/javascript">
var map=null;
var local=null;
var centermaker=null;
var iscreatr=false;

$(function() {
	loadScript();
});

function loadScript() {
   var script = document.createElement("script");
   script.src = "http://api.map.baidu.com/api?v=1.4&callback=initialize&key=gAOmcuPv1N5rHAFbkM6yqtSG";
   document.body.appendChild(script);
}

function initialize() {
	//---------------------------------------------基础示例---------------------------------------------
	 map = new BMap.Map("allmap",{minZoom:4,maxZoom:20});            // 创建Map实例
	//map.centerAndZoom(new BMap.Point(116.4035,39.915),15);  //初始化时，即可设置中心点和地图缩放级别。
	map.centerAndZoom("长沙县",13);                     // 初始化地图,设置中心点坐标和地图级别。
	map.enableScrollWheelZoom(true);//鼠标滑动轮子可以滚动

	
	map.addEventListener("click", function(e){
		if(iscreatr)return;
	    iscreatr=true;
		
		 var point = new BMap.Point(e.point.lng ,e.point.lat);//默认
		 // 创建标注对象并添加到地图  
		 clickmaker = new BMap.Marker(point);  
		 var label = new BMap.Label("我是可以拖动的",{offset:new BMap.Size(20,-10)});
		 clickmaker.setLabel(label)
		 map.addOverlay(clickmaker);  
		 clickmaker.enableDragging();    //可拖拽
		 document.getElementById("r-result").value = e.point.lng + ", " + e.point.lat;//打印拖动结束坐标
		 
		 clickmaker.addEventListener("dragend", function(e){ 
				document.getElementById("r-result").value = e.point.lng + ", " + e.point.lat;//打印拖动结束坐标
		 })
	});
	
	map.addEventListener("tilesloaded",function(){
			addmaker();
	});
	
	 map.addEventListener("dragging", function(e){ 
		addmaker();
	 })
}


function search_address(){
	local = new BMap.LocalSearch(map, {
	  renderOptions:{map: map}
	});
	var v=document.getElementById("keyword").value;
	local.search(v);
}


function addmaker(){
	map.removeOverlay(centermaker); 
	
	var myIcon = new BMap.Icon("ma.png", new BMap.Size(96, 96));   

	var point =map.getCenter();

	document.getElementById("c-result").value = point.lng + ", " + point.lat;//打印拖动结束坐标
	
	centermaker = new BMap.Marker(point, {icon: myIcon});       // 创建标注
	map.addOverlay(centermaker);                     // 将标注添加到地图中
}
</script>
<body>
	<div style="width: 100%; height: 100%" class="container">
		<div class="easyui-panel">
			<table border="0" cellspacing="0" cellpadding="0"
				class="viewdatagrid">
				<tr>
					<th style="width: 80px">输入地点：</th>
					<td><input type="text" name="keyword" id="keyword"/></td>
					<th style="width: 80px">打印坐标：</th>
					<td><input type="text" class="addres_keyword" name="r-result" id="r-result"></td>
					<th style="width: 80px">当前坐标：</th>
					<td><input type="text" class="addres_keyword" name="c-result" id="c-result"></td>
				</tr>
				<tr>
					<td colspan="6" class="btngroup"
						style="text-align: left; padding: 10px 0px 10px 0px">
						<a href="javascript:search_address();" ><span>搜索</span></a>
					</td>
				</tr>
			</table>
		</div>
		<div class="easyui-panel" style="margin-top: 3px;">
			<div id="allmap" style="width: 100%; height: 500px;"></div>
		</div>
	</div>
</body>
</html>