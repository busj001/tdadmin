<!DOCTYPE html>
<%@page import="com.tdcy.sys.util.CodeTableUtils"%>
<%@page import="com.tdcy.framework.util.SessionUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>  
<%@ taglib tagdir="/WEB-INF/tags" prefix="biztag"%>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jsp:include page="../../hcommon.jsp"></jsp:include>
	
</head>
<body class="gray-bg skin-1">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>
						会员基本信息列表</h5>
					</div>
					<div class="ibox-content">
					    <div>
							<!-- 查询条件 -->
							<form id="queryForm">
							<div  class="row">
								<div  class="col-sm-12" style="padding-bottom: 10px;">
								 	<div class="form-inline">
								 		<div class="form-group">
								 	          <label class="control-label">测试：</label>
								 	          <input id="startTime" name="startTime" class="form-control"    />  
								 	    </div>
								 	    <div class="form-group">
								 	          <label class="control-label">测试：</label>
								 	          <input id="startTime" name="startTime" class="form-control"     />  
								 	    </div>
									</div>
								</div>
							</div>
							</form>
							
							<!-- 工具栏 -->
							<div class="row">
								<div class="col-sm-12">	
									<div class="pull-left">
									      <button class="btn btn-sm btn btn-sm btn-primary" onclick="openAdd()"><i class="fa fa fa-plus"></i>新增</button>
									</div>
									<div class="pull-right">
										 <button class="btn btn-sm btn btn-sm btn-primary" onclick="search()"><i class="fa fa-search"></i> 搜索</button>
									</div>
								</div>
							</div>
							<div class="jqGrid_wrapper uadmin-grid-margin" >
						        <table id="queryGrid">
						        </table>
						        <div id="pager_queryGrid"></div>
							</div>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</div>
</body>
<biztag:Comp-CodeFormatter code="mem_status" formatterName="mem_statusFormatter"/>
<biztag:Comp-CodeFormatter code="sex" formatterName="sexFormatter"/>

<script type="text/javascript">

$(document).ready(function () {
	 $.jgrid.defaults.width = 960;
	 $.jgrid.defaults.responsive = true;
	 $.jgrid.defaults.styleUI = 'Bootstrap';
	 $.jgrid.ajaxOptions.type = 'post';
	 initmodelGridIdTable();
	 initUI();
});

function initUI(){
}

/**
*初始化表单
*/
function initmodelGridIdTable() {
	var queryParams = {};
	var colModel=[];
	
	   colModel.push({label: '操作', name: '', align: 'center',width: 200,sortable:true,formatter:operFormatter});
	   colModel.push({label: '会员编码 ', name: 'memid', align: 'center',width: 120,hidden:true});
	   
	   colModel.push({label: '会员唯一码', name: 'memNo', align: 'center',width: 120,sortable:true});
	   colModel.push({label: '会员姓名', name: 'memName', align: 'center',width: 120,sortable:true});
	   colModel.push({label: '会员性别  ', name: 'sex', align: 'center',width: 120,sortable:true,formatter:sexFormatter});
	   colModel.push({label: '证件类型', name: 'memVerType', align: 'center',width: 120,sortable:true});
	   colModel.push({label: '证件号', name: 'memVerNo', align: 'center',width: 120,sortable:true});
	   colModel.push({label: '会员所在省', name: 'memProv', align: 'center',width: 120,sortable:true});
	   colModel.push({label: '会员所在市', name: 'memCity', align: 'center',width: 120,sortable:true});
	   colModel.push({label: '所在行政区划', name: 'memArea', align: 'center',width: 120,sortable:true});
	   colModel.push({label: '地址', name: 'memAddress', align: 'center',width: 120,sortable:true});
	   colModel.push({label: '会员邮箱', name: 'memEmail', align: 'center',width: 120,sortable:true});
	   colModel.push({label: '电话', name: 'memPhone', align: 'center',width: 120,sortable:true});
	   colModel.push({label: '会员状态', name: 'memStatus', align: 'center',width: 120,sortable:true,formatter:mem_statusFormatter});
	   colModel.push({label: '创建时间', name: 'createTime', align: 'center',width: 120,sortable:true});
						
	var settings={
       postData: queryParams,
       styleUI : 'Bootstrap',
	    mtype: "post",
	    url: "${path}/hy/getMemBaseinfoPage",
	    prmNames:{//请求参数格式预处理
	          page:"page",
	          rows:"rows", 
	          sort:"sort",
	          order: "order"
	    },
       datatype: "json",
       colModel:colModel,
       pageable: true,
	    page: 1,
       rowNum: 10,
       pager: "#pager_queryGrid",
       multiSort: true,
	    sortable: true,
	    sortname: "id",
	    sortorder: "asc",
	    multiselect: false,
	    shrinkToFit: true,
	    height: 550,
	    shrinkToFit: true,
	   
	    jsonReader: {//返回参数格式处理
	        root: "data.resultsList",
	        page: "data.curPageNO",
	        total: "data.totalPage",
	        records: "data.totalRecord"
	    },
	    rownumbers: false,
	    multikey: "true",
	    autowidth: true,
      };
      $("#queryGrid").jqGrid(settings);
  };
  
	function operFormatter(value, options, row){
		 var href="";
		//<shiro:hasPermission name="MemBaseinfo:update">
	    href +="<a href=\"#\" class=\"btn btn-xs btn-primary\"  onclick=\"openUpdate('"+row.memid+"')\">修改</a>&nbsp&nbsp";
	 	href +="<a href=\"#\" class=\"btn btn-xs btn-danger\"  onclick=\"openDel('"+row.memid+"')\" >删除</a>&nbsp&nbsp";
	 	//</shiro:hasPermission> 
	    href +="<a href=\"#\" class=\"btn btn-xs btn-info\"  onclick=\"openDetail('"+row.memid+"')\">详情</a>&nbsp&nbsp";

	 	return href;
}
	
	function openAdd(){
		var url = "${path}/views/sample/member/MemBaseinfoDetail";
		AppKit.openWindow(url, "新增会员基本信息", 980, 570,{"operFlag":"add"});
	}
	
	function openUpdate(memid){
		var url = "${path}/views/sample/member/MemBaseinfoDetail";
		AppKit.openWindow(url, "修改会员基本信息", 980, 570,{"operFlag":"edit","memid":memid});
	}
	
	function openDetail(memid){
		var url = "${path}/views/sample/member/MemBaseinfoDetail";
		AppKit.openWindow(url, "详情会员基本信息", 980, 570,{"operFlag":"detail","memid":memid});
	}
	
	function openDel(memid){
		if (memid) {
				AppKit.showConfirmMessage(function() {
					var url ="${path}/hy/delMemBaseinfo";
					AppKit.postJSON(url, {"memid":memid}, delSuccess);
				});
			
		} else {
			AppKit.showTipMessage("请选择记录");
		}
	}
	
	function delSuccess(json) {
		if (!AppKit.checkJSONResult(json)) {
			return;
		}
		AppKit.showTipMessage(json.message,"",function(){
			reloadTable();
		});
	}
	
	function reloadTable(){
		$("#queryGrid").trigger("reloadGrid"); //重新载入    
	}
	
	function search(){
		var queryParams  = $("#queryForm").serializeObject();
		console.log(queryParams);
	   //传入查询条件参数  
	   $("#queryGrid").jqGrid('setGridParam',{  
	       datatype:'json',  
	       postData:queryParams, //发送数据  
	       page:1  
	   }).trigger("reloadGrid"); //重新载入    
	}
</script>
</html>
