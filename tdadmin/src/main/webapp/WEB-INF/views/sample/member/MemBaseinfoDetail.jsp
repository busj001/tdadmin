<!DOCTYPE html>
<%@page import="com.tdcy.sys.util.CodeTableUtils"%>
<%@page import="com.tdcy.framework.util.SessionUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="biztag"%>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jsp:include page="../../hcommon.jsp"></jsp:include>
</head>

<body class="white-bg" style="margin-left: 5px;margin-top: 5px;" >
	<form id="saveform" method="post" class="form-horizontal">
	  <table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
	    <tbody>
	     			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>会员编码  ：</label></td>
	        <td class="width-35">
	          <input id="memid" name="memid" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>会员唯一码 ：</label></td>
	        <td class="width-35">
	          <input id="memNo" name="memNo" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>会员姓名 ：</label></td>
	        <td class="width-35">
	          <input id="memName" name="memName" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>会员拼音码 ：</label></td>
	        <td class="width-35">
	          <input id="memPinyin" name="memPinyin" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>会员级别 ：</label></td>
	        <td class="width-35">
	          <input id="memLevel" name="memLevel" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font> ：</label></td>
	        <td class="width-35">
	          <input id="memQuarity" name="memQuarity" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>会员性别  1: 男  2： 女  0：未知 ：</label></td>
	        <td class="width-35">
	          <input id="sex" name="sex" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font> ：</label></td>
	        <td class="width-35">
	          <input id="activeWay" name="activeWay" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>证件类型 ：</label></td>
	        <td class="width-35">
	          <input id="memVerType" name="memVerType" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>证件号 ：</label></td>
	        <td class="width-35">
	          <input id="memVerNo" name="memVerNo" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>会员所在省 ：</label></td>
	        <td class="width-35">
	          <input id="memProv" name="memProv" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>会员所在市 ：</label></td>
	        <td class="width-35">
	          <input id="memCity" name="memCity" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>所在行政区划 ：</label></td>
	        <td class="width-35">
	          <input id="memArea" name="memArea" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>地址 ：</label></td>
	        <td class="width-35">
	          <input id="memAddress" name="memAddress" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>会员邮箱 ：</label></td>
	        <td class="width-35">
	          <input id="memEmail" name="memEmail" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>电话 ：</label></td>
	        <td class="width-35">
	          <input id="memPhone" name="memPhone" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>会员卡状态 ：</label></td>
	        <td class="width-35">
	          <input id="memCardStatus" name="memCardStatus" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>会员状态 ：</label></td>
	        <td class="width-35">
	          <input id="memStatus" name="memStatus" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>是否通过购买卡方式注册 ：</label></td>
	        <td class="width-35">
	          <input id="memAddtype" name="memAddtype" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>备注 ：</label></td>
	        <td class="width-35">
	          <input id="remark" name="remark" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>创建时间 ：</label></td>
	        <td class="width-35">
	          <input id="createTime" name="createTime" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>最新更新时间 ：</label></td>
	        <td class="width-35">
	          <input id="udpateTime" name="udpateTime" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>有效标志（1：有效  0：无效） ：</label></td>
	        <td class="width-35">
	          <input id="validFlag" name="validFlag" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font> ：</label></td>
	        <td class="width-35">
	          <input id="bankCode" name="bankCode" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>记录创建人 ：</label></td>
	        <td class="width-35">
	          <input id="createor" name="createor" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font> ：</label></td>
	        <td class="width-35">
	          <input id="bankNum" name="bankNum" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font> ：</label></td>
	        <td class="width-35">
	          <input id="bankOpenuserName" name="bankOpenuserName" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font> ：</label></td>
	        <td class="width-35">
	          <input id="bankOpenAddress" name="bankOpenAddress" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font> ：</label></td>
	        <td class="width-35">
	          <input id="nickname" name="nickname" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font> ：</label></td>
	        <td class="width-35">
	          <input id="tags" name="tags" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font> ：</label></td>
	        <td class="width-35">
	          <input id="avatarpicId" name="avatarpicId" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>详细地址 ：</label></td>
	        <td class="width-35">
	          <input id="mdDetail" name="mdDetail" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>预申请id ：</label></td>
	        <td class="width-35">
	          <input id="memapplyid" name="memapplyid" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font> ：</label></td>
	        <td class="width-35">
	          <input id="initFlag" name="initFlag" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
			 <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font> ：</label></td>
	        <td class="width-35">
	          <input id="idcardPhotoid" name="idcardPhotoid" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
	    </tbody>
	  </table>
	</form>
	
	<div style="text-align: center;">
	 <button id="sbtn" class="btn btn-sm btn-primary" onclick="save();return false;"><i class="fa fa-plus"></i>保存</button>
	</div>
</body>


<script type="text/javascript">
	var data = AppKit.getInvokeWindowData();
	var operFlag=data.operFlag;
	var memid = data.memid;
	var formValid = null;
	
	$(document).ready(function() {
	    formValid = AppKit.formTipValidate("saveform");
	
		if(operFlag=="edit" ||operFlag=="detail"  ){
			$("#sbtn").show();
			 var url ="${path}/hy/getMemBaseinfoInfo";
			 AppKit.postJSON(url, {"memid": memid}, querySuccess);
		}else{
			
		}
		
		if(operFlag=="detail" ){
			$("#sbtn").hide();
		}
	});

	function querySuccess(json)
	{
		if(!AppKit.checkJSONResult(json)){
	   		 return;
    	}	
		for(var i in json.data){
			$("#"+i).val(json.data[i]);
		}
	}
	
	function save()
	{
		var flag = formValid.check();
		if(!flag){
			return;
		}
		
		var info= $("#saveform").serializeObject();
		var url = "";
		if(operFlag == "add"){
			 url="${path}/hy/addMemBaseinfo";
		}else if(operFlag == "update"){
			info["memid"] = memid;
			url="${path}/hy/updateMemBaseinfo";
		}
	
		AppKit.postJSON(url,info, saveSuccess);
	}
	
	function saveSuccess(json){
		if(!AppKit.checkJSONResult(json)){
			return;
		}
		AppKit.showTipMessage(json.message,"",function(){
			AppKit.getInvokeWindow().reloadTable();
			AppKit.closeWindow();
		});
		
	}
	
</script>
</html>
