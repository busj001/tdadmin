<!DOCTYPE html>
<%@page import="com.tdcy.framework.util.SessionUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>配置管理中心</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jsp:include page="../hcommon.jsp"></jsp:include>
	<script type="text/javascript" src="${path }/resource/js/jquery.hotkeys.js"></script>
</head>
<script type="text/javascript">

	$(function() {
		  $('#platform-details').html('<code>' + navigator.userAgent + '</code>');

        $.hotkeys.add('f1',function (){$('#key').val("f1")});
        $.hotkeys.add('f2',function (){$('#key').val("f2")});
        $.hotkeys.add('f3',function (){$('#key').val("f3")});
        $.hotkeys.add('f4',function (){$('#key').val("f4")});
        $.hotkeys.add('f5',function (){$('#key').val("f5")});
        $.hotkeys.add('f6',function (){$('#key').val("f6")});
        $.hotkeys.add('f7',function (){$('#key').val("f7")});
        $.hotkeys.add('f8',function (){$('#key').val("f8")});
        $.hotkeys.add('f9',function (){$('#key').val("f9")});
        $.hotkeys.add('f10',function (){$('#key').val("f10")});
        $.hotkeys.add('f11',function (){$('#key').val("f11")});
        $.hotkeys.add('f12',function (){$('#key').val("f12")});
	});
	
	function seforc(){
		$("#ss").focus();
	}
</script>
<body>
	<div>
		<div class="easyui-panel">
				<table border="0" cellspacing="0" cellpadding="0"
					class="viewdatagrid">
					<tr>
						<th style="width: 80px">按下F1-F12键：</th>
						<td><input type="text" id="key" name="key"
							class="easyui-validatebox"  /></td>
					</tr>
					<tr>
						<th style="width: 80px">设置焦点：</th>
						<td><input type="text" id="ss" name="key"
							class="easyui-validatebox"  /></td>
					</tr>
					<tr>
						<th style="width: 80px">tab键切换input焦点：</th>
						<td><input type="text" name="key" tabindex="1"
							class="easyui-validatebox"  />
							<input type="text" name="key" tabindex="2"
							class="easyui-validatebox"  />
							<input type="text" name="key" tabindex="3"
							class="easyui-validatebox"  />
							<input type="text" name="key" tabindex="4"
							class="easyui-validatebox"  />
							<input type="text" name="key" tabindex="5"
							class="easyui-validatebox"  />
							<input type="text" name="key" tabindex="6"
							class="easyui-validatebox"  />
							<input type="text" name="key" tabindex="7"
							class="easyui-validatebox"  />
							<input type="text" name="key" tabindex="8"
							class="easyui-validatebox"  />
							<input type="text" name="key" tabindex="9"
							class="easyui-validatebox"  /></td>
					</tr>
					<tr>
						<td colspan="6" class="btngroup"
							style="text-align: left; padding: 10px 0px 10px 0px">
							<a href="javascript:seforc();" ><span>设置焦点</span></a>
						</td>
					</tr>
				</table>
		</div>
	</div>
</body>
</html>