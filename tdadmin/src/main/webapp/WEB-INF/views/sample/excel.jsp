<!DOCTYPE html>
<%@page import="com.tdcy.sys.util.CodeTableUtils"%>
<%@page import="com.tdcy.framework.util.SessionUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>  
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>${appName}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jsp:include page="../hcommon.jsp"></jsp:include>
	<%
		String dev_catagoryjson = CodeTableUtils.getCodeBeanJson("dev_catagory");
	%>
</head>
<%
%>
<script type="text/javascript">
	var curSearchInfo;
	var url;
	var dev_catagoryjson = <%=dev_catagoryjson%>;
	$(document).ready(function() {
		curSearchInfo = $("#searchform").serializeObject();	
			
	});
	
	function dosearch() {
		var paramObj = $("#searchform").serializeObject();	
		$("#grid").datagrid("load", paramObj);
	};

	function reset() {
		$("#searchform input").each(function() {
			$(this).val(curSearchInfo[$(this).attr("id")]);
		});
	}
	function doadd(){
		var url = "${path}/views/jsp/baseinfo/machineSerialDetail";
		AppKit.openWindow(url,"添加设备系列", 680, 550,{"operFlag":"add"});
	}
	
	function doedit(){
		var row = $("#grid").datagrid("getSelected");
		if (row){
			var url = "${path}/views/jsp/baseinfo/machineSerialDetail";
			AppKit.openWindow(url,"修改设备系列", 680, 550,{"mserialId":row.mserialId,"operFlag":"update"});
		}else
		{
			AppKit.showTipMessage("请选择要修改的记录");
			return;	
		}	
	}
	
	function opendetail(){
		var row = $("#grid").datagrid("getSelected");
		if (row){
			var url = "${path}/views/jsp/baseinfo/machineSerialDetail";
			AppKit.openWindow(url,"查看设备系列详情", 680, 550,{"mserialId":row.mserialId,"operFlag":"detail"});
		}else
		{
			AppKit.showTipMessage("请选择要修改的记录");
			return;	
		}
		
	}
	
	function dodel()
	{
		var row = $("#grid").datagrid("getSelected");
		if (row){
			 AppKit.showConfirmMessage(function(){
				 var url ="${path}/nt/delMachineSerials";
				 AppKit.postJSON(url, {"machineserialsId": row.mserialId}, delSuccess);
			 }, "是否删除该记录");
		}else
		{
			AppKit.showTipMessage("请选择要删除的记录");
			return;	
		}
	}
	
	function delSuccess(json)
	{
		if(!AppKit.checkJSONResult(json)){
		    return;
	    }	
		var row = $("#grid").datagrid("getSelected");
		var index = $("#grid").datagrid("getRowIndex", row);
		$("#grid").datagrid("deleteRow", index);
		AppKit.showTipMessage(json.message);
	}
	
	 function devCatagoryFormatter(val,rec){
	       return AppKit.getCodeFormatter(val, rec, dev_catagoryjson);
	  }
	 
</script>
<body>
	<div class="container">
		<div style="width: 100%;height: 100%;">
			<table id="grid" class="easyui-datagrid" toolbar="#toolbar"  style="width: 99%;"
				data-options="url:'${path}/nt/getAllMachineSerials' 
			,width:'auto'
			,title:'设备系列列表'	
			,pagination:true
			,rownumbers:true
			,fitColumns:true
			,nowarp:false
			,border: true
			,striped:true
			,singleSelect:true
			,pageSize:15
			,pageList:[15,30,50]
			,autoRowHeight:true">
				<thead>
					<tr>
					    <th field="mserialId" hidden="true"></th>
					    <th field="ck" checkbox="true" align="center">选择</th>
					    <th field="msName" align="center"  width="30%" sortable="true" >设备系列名称</th>
					    <th field="devCatagory" align="center"  width="30%" sortable="true" formatter="devCatagoryFormatter">设备分类</th>
						<th field="msRemark" align="center"  width="30%" sortable="true" >备注</th>
					</tr>
				</thead>
			</table>
		</div>
		<div id="toolbar" style="background: #fafafa; padding: 3px 5px;">
			<a href="#" class="easyui-linkbutton" iconCls="icon-detail" plain="true"
				onclick="javascript:export()">excel导出</a>
		</div>
	</div>
</body>
</html>
