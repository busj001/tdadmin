<!DOCTYPE html>
<%@page import="com.tdcy.framework.util.SessionUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>配置管理中心</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jsp:include page="../hcommon.jsp"></jsp:include>
</head>
<!-- 自定义js -->
<script type="text/javascript"> 
    $(document).ready(function () {
    	 $.jgrid.defaults.width = 960;
		 $.jgrid.defaults.responsive = true;
		 $.jgrid.defaults.styleUI = 'Bootstrap';
		 $.jgrid.ajaxOptions.type = 'post';
    	 initmodelGridIdTable();
    });
    
    /**
    *初始化表单
    */
    function initmodelGridIdTable() {
    	var queryParams = {};
		var colModel=[];
		var colSuspendedItem={label: '操作', name: 'userId', align: 'left',width: 200,sortable:true,formatter:operFormatter};
          colModel.push(colSuspendedItem);
        var colIdItem={label: '登陆名称', name: 'loginUser', align: 'left',width: 120,sortable:true, checkbox:true};
           colModel.push(colIdItem);
        var colDeploymentIdItem={label: '姓名', name: 'userName', align: 'left',width: 120,sortable:true,};
           colModel.push(colDeploymentIdItem);
        var colNameItem={label: '启用标志', name: 'loginAddress', align: 'left',width: 120,sortable:true};
           colModel.push(colNameItem);
        var colKeyItem={label: '创建时间', name: 'loginDate', align: 'left',width: 120,sortable:true};
           colModel.push(colKeyItem);
		var settings={
            postData: queryParams,
            styleUI : 'Bootstrap',
		    mtype: "get",
		    url: "${path }/sys/getloginlog",
		    prmNames:{//请求参数格式预处理
		          page:"page",
		          rows:"rows", 
		          sort:"sort",
		          order: "order"
		    },
            datatype: "json",
            colModel:colModel,
		    pageable: true,
		    page: 1,
            rowNum: 10,
            pager: "#pager_modelGridId",
            multiSort: true,
		    sortable: true,
		    sortname: "id",
		    sortorder: "asc",
		    multiselect: true,
		    shrinkToFit: true,
		    //width: auto,
		    height: 450,
		    shrinkToFit: true,
		   
		    jsonReader: {//返回参数格式处理
		        root: "data.resultsList",
		        page: "data.curPageNO",
		        total: "data.totalPage",
		        records: "data.totalRecord"
		    },
		    rownumbers: false,
		    multikey: "true",
		    autowidth: true,
           };
           $("#modelGridIdGrid").jqGrid(settings);
       };
       
   	function operFormatter(value, options, row){
  		    	 var href="";
  		         href +="<a href=\"#\" class=\"btn btn-xs btn-primary\"  onclick=\"openAdd('"+row.id+"')\">修改</a>&nbsp&nbsp";
  			  	 href +="<a href=\"#\" class=\"btn btn-xs btn-primary\"  onclick=\"openDel('"+row.id+"')\" >删除</a>&nbsp&nbsp";
  			     href +="<a href=\"#\" class=\"btn btn-xs btn-primary\"  onclick=\"openAdd('"+row.id+"')\">详情</a>&nbsp&nbsp";
 		    	 return href;
  		}
   	

   	function openAdd(){
   		var url = "${path}/views/sample/si_window";
   		AppKit.openWindow(url, "添加", "800", "500",{"id":"1"});
   	}
   	
  	function openForm(){
   		var url = "${path}/views/sample/si_form";
   		AppKit.openWindow(url, "测试表单", "800", "500",{"id":"1"});
   	}
  	
   	function openDel(id){
   		AppKit.showConfirmMessage(function(){
   			var url = '${adminPath}/workflow/model/delete/'+id;
   			AppKit.postJSON(url, null, function(json){
   				 AppKit.showTipMessage(json.msg, "提示", function(){
    				reloadTable();
    			 });
   			})
   		}, "确定要删除该记录?", "提示");
   	}
   	
   	function reloadTable(){
   		$("#modelGridIdGrid").trigger("reloadGrid"); //重新载入    
   	}
   	
   	function search(){
   		var queryParams =AppKit.getQueryParam("modelGridIdGridQueryForm","id,name,dbtype"); 
        //传入查询条件参数  
        $("#modelGridIdGrid").jqGrid('setGridParam',{  
            datatype:'json',  
            postData:queryParams, //发送数据  
            page:1  
        }).trigger("reloadGrid"); //重新载入    
   	}
   	
	function iChecks()
	{
		$(".i-checks").iCheck({
	           checkboxClass: 'icheckbox_square-green',
	           radioClass: 'iradio_square-green',
	       });
	}
   </script>

</head>
<body class="gray-bg skin-1">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>
						模型列表</h5>
					</div>
					<div class="ibox-content">
					    <div>
							<!-- 查询条件 -->
							<div id="modelGridIdGridQueryForm" class="row">
								<div  class="col-sm-12" style="padding-bottom: 10px;">
								 	<div class="form-inline">
								 		<div class="form-group">
								 	          <label class="control-label">ID：</label>
								 	           <input class="form-control" name="id"  />
								 	    </div>
								 	    <div class="form-group">
								 	          <label class="control-label">名称：</label>
								 	          <input class="form-control" name="name" />
								 	    </div>
								 	     <div class="form-group">
								 	          <label class="control-label">测试下拉：</label>
								 	          <select class="form-control" name="dbtype"></select>
								 	    </div>
									</div>
								</div>
							</div>
							
							<!-- 工具栏 -->
							<div class="row">
								<div class="col-sm-12">	
									<div class="pull-left">
									      <button class="btn btn-sm btn btn-sm btn-primary" onclick="openAdd()"><i class="fa fa fa-plus"></i>添加</button>
									        <button class="btn btn-sm btn btn-sm btn-primary" onclick="openForm()"><i class="fa fa fa-plus"></i>测试表单</button>
									</div>
									<div class="pull-right">
										 <button class="btn btn-sm btn-info" onclick="search()"><i class="fa fa-search"></i> 搜索</button>
									</div>
								</div>
							</div>
							<div class="jqGrid_wrapper uadmin-grid-margin" >
						        <table id="modelGridIdGrid">
						        </table>
						        <div id="pager_modelGridId"></div>
							</div>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</div>
</body>
</html>

           