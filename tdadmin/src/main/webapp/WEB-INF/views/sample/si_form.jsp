<!DOCTYPE html>
<%@page import="com.tdcy.framework.util.SessionUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>配置管理中心</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jsp:include page="../hcommon.jsp"></jsp:include>
</head>
<script type="text/javascript">
	$(document).ready(function() {
	    AppKit.formTipValidate("createModelForm");
	});


	function getparentdata(){
		var data  = AppKit.getInvokeWindowData();
		alert(AppKit.tostring(data));
	}
	function opennew() {
		var url = '${path}/views/jsp/system/bulletinAdd.jsp';
		AppKit.openWindow("detail_modal", url, 580, 470, '新增用户', $('#grid'));
	}
	
	function opennewparam() {
		var  url = '${path}/views/jsp/system/userUpdate.jsp?showType=detail&login_user=admin';
		 AppKit.openWindow("detail_modal", url, 670, 470, '用户详情');
	}
	
	function getparent(){
		var prentwin  = AppKit.getInvokeWindow();
		var v = prentwin.$("#textinput").val();
		alert(v);
	}
	
	function setparent(){
		var prentwin  = AppKit.getInvokeWindow();
		var v = prentwin.$("#textinput2").val("13322");
	}
	
	function closeWin(){
		AppKit.closeWindow();
	}
	function opennew() {
		var url = '${path}/sample/opentestwindow2';
		AppKit.openWindow(url,'新增用户', 580, 470);
	}
</script>
<body class="white-bg"   >
    <form id="createModelForm"  class="form-horizontal">
   		 <div  class="page-content">
		 <table  class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
		      <tr>
		         <td  class=" active text-right" style="width:20%;">	
		              <label><font color="red">*</font>名称:</label>
		         </td>
		         <td >
		             <input path="name" class="form-control" nested="false"   datatype="*" nullmsg="请输入名称！" htmlEscape="false" />
		             <label class="Validform_checktip"></label>
		         </td>
		      </tr>
		      <tr>
		         <td  class=" active text-right">	
		              <label><font color="red">*</font>KEY:</label>
		         </td>
		         <td>
		             <input path="key" class="form-control" nested="false"  datatype="*" nullmsg="请输入KEY！" htmlEscape="false" />
		             <label class="Validform_checktip"></label>
		         </td>
		      </tr>
		      <tr>
		         <td  class=" active text-right">	
		              <label>描述:</label>
		         </td>
		         <td class=""  >
		              <textarea  path="description" class="form-control " nested="false" datatype="*"  htmlEscape="false" rows="3" maxlength="200" >
		              </textarea>
		         </td>
		      </tr>
		   </tbody>
		</table>  
		</div>
	</form>
	<div style="text-align: center;">
		 <button class="btn btn-sm btn-primary" onclick="save()"><i class="fa fa-plus"></i> 修改</button>
	</div> 
</body>
</html>