<!DOCTYPE html>
<%@page import="com.tdcy.framework.util.SessionUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>配置管理中心</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jsp:include page="../hcommon.jsp"></jsp:include>
</head>
<script type="text/javascript">
	function opennew() {
		var url = '${path}/views/sample/opentestwindow';
		AppKit.openWindow(url,'新增用户', 580, 470);
	}
	
	function opennew2() {
		var url = '${path}/views/sample/opentestwindow';
		var data = {"testdata":"testdata"};
		AppKit.openWindow(url,'新增用户', 580, 470, data);
	}
	
	function opennew3() {
		layer.open({
			    type: 1,
			    title:"测试",
			    shadeClose: false,
			    shade: [0.3, '#393D49'],
			    content: $("#showdiv")
		})
	}
	
	function opennew4() {
		var url = '${path}/views/sample/opentestwindow';
		var data = {"testdata":"testdata"};
		AppKit.openUnMoralWindow(url,'非模态窗口', 580, 470, data);
	}
	
	function opennew5() {
		AppKit.showMessage("提示消息");
	}
	
	function opennew6() {
		AppKit.showTipMessage("提示消息自动消失","",function(){
			alert("消失后回调");
		});
	}
	
	function opennew7() {
		AppKit.showConfirmMessage(function() {
	        alert("确认");
	    },'确定删除吗?',"",function(){
	    	alert("取消");
	    });
	}
	
	
	function modifiy(){
		$("#sq1").val("ffffff");
	}
</script>
<body class="white-bg" style="margin-left: 5px;margin-top: 5px;" >
 <table class="table table-bordered  table-condensed dataTables-example dataTable no-footer " >
    <tbody>
      <tr>
	        <td class="width-100" colspan="2">
		         <input id="textinput"  value="11111111">
				 <input id="textinput2" >
	        </td>
      </tr>
      <tr>
        <td class="width-100">
          <button class="btn btn-sm btn-primary" onclick="opennew()">弹出窗口不带参数</button>
          <button class="btn btn-sm btn-primary" onclick="opennew2()">弹出窗口带参数</button>
          <button class="btn btn-sm btn-primary" onclick="opennew5()">提示</button>
          <button class="btn btn-sm btn-primary" onclick="opennew6()">提示后自动关闭</button>
          <button class="btn btn-sm btn-primary" onclick="opennew7()">确认消息</button>
        </td>
      </tr>
      </tbody>
    </table>
</body>
</html>