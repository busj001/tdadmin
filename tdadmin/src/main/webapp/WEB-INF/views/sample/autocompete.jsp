<!DOCTYPE html>
<%@page import="com.tdcy.framework.util.SessionUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>配置管理中心</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jsp:include page="../hcommon.jsp"></jsp:include>
	<link rel='stylesheet' type='text/css' href='${path }/resource/js/autocomplete/jquery.autocomplete.css' />
	<script type="text/javascript" src="${path }/resource/js/autocomplete/jquery.autocomplete.js"></script>
</head>
<script type="text/javascript">
	$(function() {
		var array = new Array();
		array.push({"id":"111","name":"AAAAA"});
		array.push({"id":"22","name":"AAAAAV"});
		array.push({"id":"11331","name":"AAAAAB"});
		array.push({"id":"1141","name":"AAAAAD"});
		array.push({"id":"14411","name":"DAAA"});
		array.push({"id":"1151","name":"DAAA"});
		array.push({"id":"1511","name":"BAAAA"});
		
		$("#selectarea").autocomplete(array, {
			formatItem:function(row, i, max){
				return row.name;
			},
			formatResult:function(row, i, max){
				return row.name;
			},
			formatMatch:function(row, i, max){
				return row.name;
			}
			}).result(function(event, data, formatted) {
				var id = data.id;
				var isa = id.split("_");
				$("#cityId").val(isa[0]);
				$("#areaId").val(isa[1]);
				$("#villageId").val(isa[2]);
				
			}
		);
		
	});
</script>
<body>
	<div>
		<div class="easyui-panel">
			<form id="searchform">
				<table border="0" cellspacing="0" cellpadding="0"
					class="viewdatagrid">
					<tr>
						<th style="width: 80px">选择：</th>
						<td> <input id="selectarea" name="villageName"  class="aInput" ></td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</body>
</html>