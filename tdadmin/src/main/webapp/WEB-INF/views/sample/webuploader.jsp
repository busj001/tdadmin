<!DOCTYPE html>
<%@page import="com.tdcy.framework.util.SessionUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>配置管理中心</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jsp:include page="../hcommon.jsp"></jsp:include>
	
	<!--引入CSS-->
	<link rel="stylesheet" type="text/css" href="${path }/resource/js/webuploader/webuploader.css">
	<link rel="stylesheet" type="text/css" href="${path }/resource/js/webuploader/webuploader-demo.css">
	<!--引入JS-->
	<script type="text/javascript" src="${path }/resource/js/webuploader/webuploader.js"></script>
</head>
<script type="text/javascript">
//文件上传
jQuery(function() {
    var $  = jQuery;
    var $list = $('#thelist');
    var $btn = $('#ctlBtn');
    var state = 'pending';
    var uploader;

    uploader = WebUploader.create({
        // 不压缩image
        resize: false,
         // swf文件路径
     	swf: '${path }/resource/js/webuploader/Uploader.swf',
     	// 文件接收服务端。
     	server: '${path}/file/uploadFile',
        // 选择文件的按钮。可选。
        // 内部根据当前运行是创建，可能是input元素，也可能是flash.
        pick: '#picker'
    });

    // 当有文件添加进来的时候
    uploader.on( 'fileQueued', function( file ) {
        $list.append( '<div id="' + file.id + '" class="item">' +
            '<h4 class="info">' + file.name + '</h4>' +
            '<p class="state">等待上传...</p>' +
        '</div>' );
    });

    // 文件上传过程中创建进度条实时显示。
    uploader.on( 'uploadProgress', function( file, percentage ) {
        var $li = $( '#'+file.id );
        var $percent = $li.find('.progress .progress-bar');

        // 避免重复创建
        if ( !$percent.length ) {
            $percent = $('<div class="progress progress-striped active">' +
              '<div class="progress-bar" role="progressbar" style="width: 0%">' +
              '</div>' +
            '</div>').appendTo( $li ).find('.progress-bar');
        }

        $li.find('p.state').text('上传中');

        $percent.css( 'width', percentage * 100 + '%' );
    });

    uploader.on( 'uploadSuccess', function( file ) {
        $( '#'+file.id ).find('p.state').text('已上传');
    });

    uploader.on( 'uploadError', function( file ) {
        $( '#'+file.id ).find('p.state').text('上传出错');
    });

    uploader.on( 'uploadComplete', function( file ) {
        $( '#'+file.id ).find('.progress').fadeOut();
    });

    uploader.on( 'all', function( type ) {
        if ( type === 'startUpload' ) {
            state = 'uploading';
        } else if ( type === 'stopUpload' ) {
            state = 'paused';
        } else if ( type === 'uploadFinished' ) {
            state = 'done';
        }

        if ( state === 'uploading' ) {
            $btn.text('暂停上传');
        } else {
            $btn.text('开始上传');
        }
    });

    $btn.on( 'click', function() {
        if ( state === 'uploading' ) {
            uploader.stop();
        } else {
            uploader.upload();
        }
    });
});



//图片上传demo
jQuery(function() {
	var $ = jQuery;
    var $list = $('#fileList');
    var $btn = $('#imgctlBtn');
    var state = 'pending';
     // 优化retina, 在retina下这个值是2
    var  ratio = window.devicePixelRatio || 1;
     // 缩略图大小
    var thumbnailWidth = 100 * ratio;
    var thumbnailHeight = 100 * ratio;
     // Web Uploader实例
    var  uploader;

	 // 初始化Web Uploader
	 uploader = WebUploader.create({
	     // 自动上传。
	     auto:false,
	
	     // swf文件路径
	     swf: '${path }/resource/js/webuploader/Uploader.swf',
	
	     // 文件接收服务端。
	     server: '${path}/file/uploadFile',
	
	     // 选择文件的按钮。可选。
	     // 内部根据当前运行是创建，可能是input元素，也可能是flash.
	     pick: '#filePicker',
	
	     // 只允许选择文件，可选。
	     accept: {
	         title: 'Images',
	         extensions: 'gif,jpg,jpeg,bmp,png',
	         mimeTypes: 'image/*'
	     }
	 });

	 // 当有文件添加进来的时候
	 uploader.on('fileQueued', function( file ) {
	     var $li = $(
	             '<div id="' + file.id + '" class="file-item thumbnail">' +
	                 '<img>' +
	                 '<div class="info">' + file.name + '</div>' +
	                 '<p class="state">等待上传...</p>' +
	             '</div>'
	             );
	     var $img = $li.find('img');
	     $list.append( $li );
	
	     // 创建缩略图
	     uploader.makeThumb( file, function( error, src ) {
	         if ( error ) {
	             $img.replaceWith('<span>不能预览</span>');
	             return;
	         }
	
	         $img.attr('src', src );
	     }, thumbnailWidth, thumbnailHeight );
	 });

	 // 文件上传过程中创建进度条实时显示。
	 uploader.on('uploadProgress', function( file, percentage ) {
	     var $li = $('#'+file.id );
	     var $percent = $li.find('.progress span');
	     $li.find('p.state').html('上传中');
	     // 避免重复创建
	     if ( !$percent.length ) {
	         $percent = $('<p class="progress"><span></span></p>').appendTo($li).find('span');
	     }
	
	     $percent.css('width', percentage * 100 + '%');
	 });
	 
	uploader.on( 'uploadSuccess', function( file ) {
        $('#'+file.id ).find('p.state').html('上传成功[<a style="color:blue" href="javascript:delimg(\''+file.id+'\')">删除</a>]');
     });

    uploader.on( 'uploadError', function( file ) {
        $('#'+file.id ).find('p.state').html('上传出错');
    });

    uploader.on( 'uploadComplete', function( file ) {
        $('#'+file.id ).find('.progress').fadeOut();
    });

    uploader.on( 'all', function( type ) {
        if ( type === 'startUpload' ) {
            state = 'uploading';
        } else if ( type === 'stopUpload' ) {
            state = 'paused';
        } else if ( type === 'uploadFinished' ) {
            state = 'done';
        }

        if ( state === 'uploading' ) {
            $btn.text('暂停上传');
        } else {
            $btn.text('开始上传');
        }
    });

    $btn.on( 'click', function() {
        if ( state === 'uploading' ) {
            uploader.stop();
        } else {
            uploader.upload();
        }
    });
});


function delimg(fileid){
	$("#"+fileid).remove();
}
</script>
<body>
	<div class="container">
		<div class="easyui-panel" title="上传文件">
			<div id="uploader" >
			    <!--用来存放文件信息-->
			    <div id="thelist" class="uploader-list"></div>
			    <div class="btns">
			        <div id="picker">选择文件</div>
			        <button id="ctlBtn" class="easyui-linkbutton">开始上传</button>
			    </div>
			</div>
		</div>
		<div style="height: 5px;"></div>
		<div class="easyui-panel" title="上传图片">
			<div id="uploader-demo">
			    <!--用来存放item-->
			    <div id="fileList" class="uploader-list"></div>
			    	
			     <div class="btns">
				    <div id="filePicker">选择图片</div>
				    <a id="imgctlBtn" class="easyui-linkbutton">开始上传</a>
			    </div>
			</div>
		</div>
	</div>
</html>