<!DOCTYPE html>
<%@page import="com.tdcy.framework.util.SessionUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>配置管理中心</title>
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jsp:include page="../hcommon.jsp"></jsp:include>
	<script type="text/javascript">
	var basePath="${basePath}"
	window.UMEDITOR_HOME_URL= "${path}/resource/js/umeditor/";
	</script>
	<link rel='stylesheet' type='text/css' href='${path }/resource/js/umeditor/themes/default/css/umeditor.css' />
	<script type="text/javascript" src="${path }/resource/js/umeditor/third-party/template.min.js"></script>
	<script type="text/javascript"  src="${path }/resource/js/umeditor/umeditor.config.js"></script>
	<script type="text/javascript"  src="${path }/resource/js/umeditor/umeditor.js"></script>
	<script type="text/javascript"  src="${path }/resource/js/umeditor/umeditor.js"></script>
	 <script type="text/javascript" src="${path }/resource/js/umeditor/lang/zh-cn/zh-cn.js"></script>
</head>

<script type="text/javascript">
	var ue  = null;
	$(document).ready(function(){
		  ue = UM.getEditor('editor');
	});
	
	function getContent(){
		var content = ue.getContent();
		alert(content);
	}
	
	function getText(){
		var content = ue.getPlainTxt();
		alert(content);
	}
	
	function setContent(){
		ue.setContent("设置内容");
	}
</script>
<body>
	<div class="container">
   	 <!-- 加载编辑器的容器 -->
	    <div id="editor">
		  
	     </div>
       <div>
	    	<a class="easyui-linkbutton" href="javascript:getContent();" ><span>获取内容</span></a>
	    	<a class="easyui-linkbutton" href="javascript:getText();" ><span>获取文本</span></a>
	    	<a class="easyui-linkbutton" href="javascript:setContent();" ><span>设置内容</span></a>
	    </div>
     </div>
</body>
</html>