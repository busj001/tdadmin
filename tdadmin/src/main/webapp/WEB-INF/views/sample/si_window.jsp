<!DOCTYPE html>
<%@page import="com.tdcy.framework.util.SessionUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>配置管理中心</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jsp:include page="../hcommon.jsp"></jsp:include>
</head>
<script type="text/javascript">
	function getparentdata(){
		var data  = AppKit.getInvokeWindowData();
		alert(AppKit.tostring(data));
	}
	function opennew() {
		var url = '${path}/views/jsp/system/bulletinAdd.jsp';
		AppKit.openWindow("detail_modal", url, 580, 470, '新增用户', $('#grid'));
	}
	
	function opennewparam() {
		var  url = '${path}/views/jsp/system/userUpdate.jsp?showType=detail&login_user=admin';
		 AppKit.openWindow("detail_modal", url, 670, 470, '用户详情');
	}
	
	function getparent(){
		var prentwin  = AppKit.getInvokeWindow();
		var v = prentwin.$("#textinput").val();
		alert(v);
	}
	
	function setparent(){
		var prentwin  = AppKit.getInvokeWindow();
		var v = prentwin.$("#textinput2").val("13322");
	}
	
	function closeWin(){
		AppKit.closeWindow();
	}
	function opennew() {
		var url = '${path}/sample/opentestwindow2';
		AppKit.openWindow(url,'新增用户', 580, 470);
	}
</script>
<body>
	<div>
		<div class="easyui-panel">
			<form method="get" id="searchform">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" class="viewdatagrid">
					<tr>
						<td  class="buttonline" style="text-align: left;padding: 10px 0px 10px 0px">
						    <input id="textinput"  value="11111111">
							<input id="textinput2" >
						</td>
					</tr>
					<tr>
						<td  style="text-align: left;padding: 10px 0px 10px 0px">
							<a class="easyui-linkbutton" href="javascript:getparentdata();" class="search"><span>获取父窗口的传值</span></a><br>
						</td>
					</tr>
					<tr>
						<td  style="text-align: left;padding: 10px 0px 10px 0px">
							<a class="easyui-linkbutton" href="javascript:getparent();" class="search"><span>获取父窗口控件值</span></a><br>
						</td>
					</tr>
					<tr>
						<td  style="text-align: left;padding: 10px 0px 10px 0px">
							<a class="easyui-linkbutton" href="javascript:setparent();" class="reset"><span>设置父窗口空间值</span></a>
						</td>
					</tr>
					<tr>
						<td  style="text-align: left;padding: 10px 0px 10px 0px">
							<a class="easyui-linkbutton" href="javascript:closeWin();" class="reset"><span>关闭窗口</span></a>
						</td>
					</tr>
					<tr>
						<td  style="text-align: left;padding: 10px 0px 10px 0px">
							<a class="easyui-linkbutton" 	href="javascript:opennew();" class="search"><span>弹出窗口中弹出子窗口</span></a><br>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</body>
</html>