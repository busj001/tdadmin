<!DOCTYPE html>
<%@page import="com.tdcy.framework.util.StringUtils"%>
<html lang="en">
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<head>
        <%
            Object error = request.getAttribute("shrErrors");
            String errormsg = "";
            if(error != null){
                errormsg = (String)error;
            }
            if(StringUtils.isEmpty(errormsg)){
                errormsg= request.getParameter("shrErrors");
            }
            if(StringUtils.isEmpty(errormsg)){
             errormsg = "系统出现异常";
            }
        %>
        <jsp:include page="./hcommon.jsp"></jsp:include>
    </head>
<body style="background:#edf6fa;">
	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">首页</a></li>
    <li><a href="#">错误提示</a></li>
    </ul>
    </div>
    
    <div class="error">
    
    <h2>非常遗憾，您访问的页面出现错误</h2>
    <p><%=errormsg %></p>
    <div class="reindex"><a href="./login" target="_parent">返回首页</a></div>
    </div>
</body>
</html>
