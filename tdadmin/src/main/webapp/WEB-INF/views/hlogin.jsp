<!DOCTYPE html>
<%@page import="com.tdcy.framework.util.DateUtils"%>
<%@page import="com.tdcy.sys.util.ParamUtils"%>
<html lang="en">
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	request.setAttribute("path", path);
	request.setAttribute("basePath", basePath);
	
	
	String appName = ParamUtils.getParamValue("application_name");
	String companyName = ParamUtils.getParamValue("company_name");
	String currentYear = DateUtils.getCurrentYear();
	request.setAttribute("appName", appName);
	request.setAttribute("companyName", companyName);
	request.setAttribute("currentYear", currentYear);
	
	String checkcodeflag = ParamUtils.getParamValue("checkcode_flag");
%>
    <title>登录</title>
    <link href="${path }/resource/css/h/bootstrap.min.css" rel="stylesheet">
    <link href="${path }/resource/css/h/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="${path }/resource/css/h/animate.min.css" rel="stylesheet">
    <link href="${path }/resource/css/h/style.min.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
	<script src="${path }/resource/js/jquery-2.0.3.min.js"></script>
	<script src="${path }/resource/js/h/bootstrap.min.js"></script>
	<script src="${path }/resource/js/jquery-migrate-1.1.0.js"></script>
	<script src="${path }/resource/js/layer/layer.js"></script>
	<script src="${path }/resource/js/hcommon.appkit.js"></script>
	<script src="${path }/resource/js/jquery.md5.js"></script>
	<script src="${path }/resource/js/jquery.hotkeys.js"></script>
<script>
if(window.top!==window.self){window.top.location=window.location};
 var checkcodeflag = "<%=checkcodeflag%>";
$(function(){
	$("#tipinfo").html("");
	 $.hotkeys.add('return',function (){
    	 login();});
	
    if(checkcodeflag == "1"){
    	changeVerify();
    }
});  

function login(){
	$("#tipinfo").html("");
	
	var formobj = $("#adminform").serializeObject();
	
	  if(AppKit.isEmptyVal(formobj["loginUser"])){
	    	$("#tipinfo").html("用户名不能为空");
	    	return;
	   }
	  
	  if(AppKit.isEmptyVal(formobj["password"])){
	    	$("#tipinfo").html("密码不能为空");
	    	return;
	   }
	  
	formobj["password"] =  $.md5(formobj["password"]);
	formobj["verifycode"] = $("#verifycode").val();
	 if(checkcodeflag == "1"){
	    if(AppKit.isEmptyVal(formobj["verifycode"])){
	    	$("#tipinfo").html("验证码不能为空");
	    	return;
	    }
	  }
	 
	   var url = "${path}/login/checkLogin";
       AppKit.postJSON(url, formobj, loginSuccess); 
}

function loginSuccess(json) {
      if (!AppKit.checkJSONResult(json,true)) {
		  $("#tipinfo").html(json.message);
           return;
      }
      
      location.href="${path}/sys/main";
  }
  
  function getcheckcode(){
	  var url = "${path}/login/verifycode";
      AppKit.postJSON(url, "", function(json){
    	  if (!AppKit.checkJSONResult(json,true)) {
    		  $("#tipinfo").show();
    		  $("#tipinfo").html(json.message);
              return;
          }
    	  
    	  var data = json.data;
    	  $("#adminyshow").html(data);
      }); 
  }
  
  function changeVerify(){
		$("#verifyImage").attr("src",function(){
			var timesamp = new Date().getTime();
			return "${path}/login/verifycodeImage?timesamp="+timesamp;
		});
	}
</script>
</head>
<body class="gray-bg" style="background: url(${path}/resource/images/login-background.jpg) no-repeat center fixed;-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;">
    <div class="middle-box text-center loginscreen  animated fadeInDown" style="padding-top: 120px;">
        <div>
            <div>
                <h1 class="logo-name">TD</h1>
            </div>
            <h3>欢迎使用 ${appName }</h3>

            <form class="m-t" role="form" id="adminform">
             <p id="tipinfo" class="" style="color: red;"></p>
                <div class="form-group">
                    <input  id="username" name="loginUser" type="text" class="form-control" placeholder="用户名" required="" value="admin">
                </div>
                <div class="form-group">
                    <input id="userpassword"  name="password" type="password" class="form-control" placeholder="密码" required="" value="123456">
                </div>
                <%
                	if("1".equals(checkcodeflag)){
                %>
                <div class="form-group">
                 <div class="form-inline">
                	 <input id="verifycode"  name="verifycode" type="text" class="form-control " > 
			         <img name="verifyImage" id="verifyImage"  title="点击图片，刷新验证码" onclick="changeVerify()" />
	              </div>  		
                </div>
                <%} %>
            </form>
            <button onclick="login();return false;" class="btn btn-primary block full-width m-b">登 录</button>
            <p class="text-muted text-center"> <a href="login.html#"><small> &copy; ${currentYear } All Rights Reserved. ${companyName }</small></a>
            </p>
            
        </div>
    </div>
</body>
</html>