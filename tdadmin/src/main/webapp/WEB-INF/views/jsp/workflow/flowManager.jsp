<!DOCTYPE html>
<%@page import="com.tdcy.framework.util.SessionUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>配置管理中心</title>
	<jsp:include page="../../hcommon.jsp"></jsp:include>
</head>

<body class="gray-bg skin-1">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>
						已部署流程</h5>
					</div>
					<div class="ibox-content">
					    <div>
							<!-- 工具栏 -->
							<div class="row">
								<div class="col-sm-12">	
								</div>
							</div>
							<div class="jqGrid_wrapper uadmin-grid-margin" >
						        <table id="queryGrid">
						        </table>
						        <div id="pager_queryGrid"></div>
							</div>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</div>
</body>

<script type="text/javascript"> 
    $(document).ready(function () {
    	 $.jgrid.defaults.width = 960;
		 $.jgrid.defaults.responsive = true;
		 $.jgrid.defaults.styleUI = 'Bootstrap';
		 $.jgrid.ajaxOptions.type = 'post';
    	 initmodelGridIdTable();
    });
    
    /**
    *初始化表单
    */
    function initmodelGridIdTable() {
    	var queryParams = {};
		var colModel=[];
		
        var col1={label: 'ProcessDefinitionId', name: 'id', align: 'center',width: 120,sortable:true, checkbox:true};
           colModel.push(col1);
        var col2={label: 'DeploymentId', name: 'deploymentId', align: 'center',width: 120,sortable:true,};
           colModel.push(col2);
        var col3={label: '名称', name: 'name', align: 'center',width: 120,sortable:true};
           colModel.push(col3);
        var col3={label: 'KEY', name: 'key', align: 'center',width: 120,sortable:true};
           colModel.push(col3);
        var col3={label: '版本号', name: 'version', align: 'center',width: 120,sortable:true};
           colModel.push(col3);
        var col3={label: '部署时间', name: 'deploymentTime', align: 'center',width: 120,sortable:true};
           colModel.push(col3);
         var col3={label: '图片', name: 'diagramResourceName', align: 'center',width: 120,sortable:true,formatter:imageFormatter};
           colModel.push(col3);
         var col3={label: 'XML', name: 'resourceName', align: 'center',width: 120,sortable:true,formatter:xmlFormatter};
           colModel.push(col3);
        var col3={label: '是否挂起', name: 'suspended', align: 'center',width: 120,sortable:true,formatter:susFormatter};
           colModel.push(col3);
        var col5={label: '操作', name: 'id', align: 'center',width: 200,sortable:true,formatter:operFormatter};
           colModel.push(col5);
           
		var settings={
            postData: queryParams,
            styleUI : 'Bootstrap',
		    mtype: "post",
		    url: "${path}/workflow/getProcessList",
		    prmNames:{//请求参数格式预处理
		          page:"page",
		          rows:"rows", 
		          sort:"sort",
		          order: "order"
		    },
            datatype: "json",
            colModel:colModel,
		    pageable: false,
		    page: 1,
		    rowNum: '-1',
            multiSort: true,
		    sortable: true,
		    sortname: "id",
		    sortorder: "asc",
		    multiselect: false,
		    shrinkToFit: true,
		    height: 550,
		    shrinkToFit: true,
		   
		    jsonReader: {//返回参数格式处理
		        root: "data"
		    },
		    
		    rownumbers: false,
		    multikey: "true",
		    autowidth: true,
           };
           $("#queryGrid").jqGrid(settings);
       };
       
   	function imageFormatter(value, options, row){
   	 var href="";
   	href +="<a href=\"${path}/workflow/readProcessDefinition?processDefinitionId="+row["id"]+"&resourceType=image\" class=\"btn btn-xs btn-primary\" target=\"_blank\"   >"+row["diagramResourceName"]+"</a>&nbsp&nbsp";
  	 	return href;
	}
   	
	function xmlFormatter(value, options, row){
   	 var href="";
   	href +="<a href=\"${path}/workflow/readProcessDefinition?processDefinitionId="+row["id"]+"&resourceType=xml\" class=\"btn btn-xs btn-primary\" target=\"_blank\"   >"+row["resourceName"]+"</a>&nbsp&nbsp";
  	 	return href;
	}
	
	function operFormatter(value, options, row){
   	 var href="";
        href +="<a href=\"#\" class=\"btn btn-xs btn-danger\"  onclick=\"openDel('"+row.deploymentId+"')\">删除</a>&nbsp&nbsp";
        href +="<a href=\"#\" class=\"btn btn-xs btn-primary\"  onclick=\"openConvert('"+row.id+"')\">转为MODEL</a>&nbsp&nbsp";
  	 	return href;
	}
       
   	function susFormatter(value, options, row){
   	 var href="";
   		if(value){
   		 href +="<a href=\"#\" class=\"btn btn-xs btn-primary\"  onclick=\"openSuspended('"+row.id+"','active')\">激活</a>&nbsp&nbsp";
   		}else{
   		 href +="<a href=\"#\" class=\"btn btn-xs btn-danger\"  onclick=\"openSuspended('"+row.id+"','suspend')\">挂起</a>&nbsp&nbsp";
   		}
   	 	return href;
	}
  	
	function openSuspended(id,type){
   		if (id) {
   				AppKit.showConfirmMessage(function() {
   					url = '${path}/workflow/updateProcessState?processDefinitionId='+id+'&state=' + type;
   					AppKit.postJSON(url, "", suspendedSuccess);
   				});
   			
   		} else {
   			AppKit.showTipMessage("请选择记录");
   		}
   	}
   	
   	function suspendedSuccess(json) {
   		if (!AppKit.checkJSONResult(json)) {
   			return;
   		}
   		AppKit.showTipMessage(json.message,"",function(){
   			reloadTable();
   		});
   	}
   	
  	
   	function openDel(deploymentId){
   		if (deploymentId) {
   				AppKit.showConfirmMessage(function() {
   					url = '${path}/workflow/deleteProcess?deploymentId=' + deploymentId;
   					AppKit.postJSON(url, "", delSuccess);
   				});
   			
   		} else {
   			AppKit.showTipMessage("请选择记录");
   		}
   	}
   	
   	function delSuccess(json) {
   		if (!AppKit.checkJSONResult(json)) {
   			return;
   		}
   		AppKit.showTipMessage(json.message,"",function(){
   			reloadTable();
   		});
   	}
   	
 	function openConvert(id){
   		if (id) {
   				AppKit.showConfirmMessage(function() {
   					url = '${path}/workflow/convertToModel?processDefinitionId=' + id;
   					AppKit.postJSON(url, "", convertSuccess);
   				});
   			
   		} else {
   			AppKit.showTipMessage("请选择记录");
   		}
   	}
   	
   	function convertSuccess(json) {
   		if (!AppKit.checkJSONResult(json)) {
   			return;
   		}
   		AppKit.showTipMessage(json.message,"",function(){
   			reloadTable();
   		});
   	}
   	
   	function reloadTable(){
   		$("#queryGrid").trigger("reloadGrid"); //重新载入    
   	}
   </script>
   
</html>