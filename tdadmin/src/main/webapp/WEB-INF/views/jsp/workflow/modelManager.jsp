<!DOCTYPE html>
<%@page import="com.tdcy.framework.util.SessionUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>配置管理中心</title>
	<jsp:include page="../../hcommon.jsp"></jsp:include>
</head>

<body class="gray-bg skin-1">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>
						模型管理</h5>
					</div>
					<div class="ibox-content">
					    <div>
							<!-- 工具栏 -->
							<div class="row">
								<div class="col-sm-12">	
									<div class="pull-left">
									      <button class="btn btn-sm btn btn-sm btn-primary" onclick="openAdd()"><i class="fa fa fa-plus"></i>创建</button>
									</div>
								</div>
							</div>
							<div class="jqGrid_wrapper uadmin-grid-margin" >
						        <table id="queryGrid">
						        </table>
						        <div id="pager_queryGrid"></div>
							</div>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</div>
</body>

<script type="text/javascript"> 
    $(document).ready(function () {
    	 $.jgrid.defaults.width = 960;
		 $.jgrid.defaults.responsive = true;
		 $.jgrid.defaults.styleUI = 'Bootstrap';
		 $.jgrid.ajaxOptions.type = 'post';
    	 initmodelGridIdTable();
    });
    
    /**
    *初始化表单
    */
    function initmodelGridIdTable() {
    	var queryParams = {};
		var colModel=[];
		
        var col1={label: '模型ID', name: 'id', align: 'center',width: 120,sortable:true, checkbox:true};
           colModel.push(col1);
        var col2={label: '模型KEY', name: 'key', align: 'center',width: 120,sortable:true,};
           colModel.push(col2);
        var col3={label: '名称', name: 'name', align: 'center',width: 120,sortable:true};
           colModel.push(col3);
        var col3={label: '版本号', name: 'version', align: 'center',width: 120,sortable:true};
           colModel.push(col3);
        var col3={label: '创建时间', name: 'createTime', align: 'center',width: 120,sortable:true};
           colModel.push(col3);
         var col3={label: '更新时间', name: 'lastUpdateTime', align: 'center',width: 120,sortable:true};
           colModel.push(col3);
         var col3={label: '元数据', name: 'metaInfo', align: 'center',width: 120,sortable:true};
           colModel.push(col3);
        var col5={label: '操作', name: 'id', align: 'center',width: 200,sortable:true,formatter:operFormatter};
           colModel.push(col5);
           
		var settings={
            postData: queryParams,
            styleUI : 'Bootstrap',
		    mtype: "post",
		    url: "${path}/workflow/getModelList",
		    prmNames:{//请求参数格式预处理
		          page:"page",
		          rows:"rows", 
		          sort:"sort",
		          order: "order"
		    },
            datatype: "json",
            colModel:colModel,
		    pageable: false,
		    page: 1,
		    rowNum: '-1',
            multiSort: true,
		    sortable: true,
		    sortname: "id",
		    sortorder: "asc",
		    multiselect: false,
		    shrinkToFit: true,
		    height: 550,
		    shrinkToFit: true,
		   
		    jsonReader: {//返回参数格式处理
		        root: "data"
		    },
		    rownumbers: false,
		    multikey: "true",
		    autowidth: true,
           };
           $("#queryGrid").jqGrid(settings);
       };
       
   	function operFormatter(value, options, row){
    	 var href="";
         href +="<a href=\"#\" class=\"btn btn-xs btn-primary\"  onclick=\"openDeploy('"+row.id+"')\">部署</a>&nbsp&nbsp";
         href +="<a href=\"${path}/resource/activiti/modeler.html?modelId="+row.id+"\" class=\"btn btn-xs btn-primary\"  target=\"_blank\">设计</a>&nbsp&nbsp";
         href +="<a href=\"#\" class=\"btn btn-xs btn-primary\"  onclick=\"openDel('"+row.id+"')\">删除</a>&nbsp&nbsp";
	  	href +="<a href=\"${path}/workflow/exportModel?modelId="+row.id+"&type=bpmn\" class=\"btn btn-xs btn-danger\" target=\"_blank\"   >结构下载</a>&nbsp&nbsp";
   	 	return href;
	}
   	
   	function openAdd(){
   		var url = "${path}/views/jsp/workflow/modelAdd";
   		AppKit.openWindow(url, "添加模型", "800", "500",{"operflag":"add"});
   	}
   	
  	
	function openDeploy(id){
   		if (id) {
   				AppKit.showConfirmMessage(function() {
   					url = '${path}/workflow/deployModel?modelId=' + id;
   					AppKit.postJSON(url, "", deploySuccess);
   				});
   			
   		} else {
   			AppKit.showTipMessage("请选择记录");
   		}
   	}
   	
   	function deploySuccess(json) {
   		if (!AppKit.checkJSONResult(json)) {
   			return;
   		}
   		AppKit.showTipMessage(json.message,"",function(){
   			reloadTable();
   		});
   	}
   	
  	
   	function openDel(id){
   		if (id) {
   				AppKit.showConfirmMessage(function() {
   					url = '${path}/workflow/deleteModel?modelId=' + id;
   					AppKit.postJSON(url, "", delSuccess);
   				});
   			
   		} else {
   			AppKit.showTipMessage("请选择记录");
   		}
   	}
   	
   	function delSuccess(json) {
   		if (!AppKit.checkJSONResult(json)) {
   			return;
   		}
   		AppKit.showTipMessage(json.message,"",function(){
   			reloadTable();
   		});
   	}
   	
   	function reloadTable(){
   		$("#queryGrid").trigger("reloadGrid"); //重新载入    
   	}
   </script>
   
</html>