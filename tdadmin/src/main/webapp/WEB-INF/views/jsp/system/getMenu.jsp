<!DOCTYPE html>
<%@page import="com.tdcy.framework.util.SessionUtils"%>
<%@page import="com.tdcy.sys.util.ParamUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>配置管理中心</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jsp:include page="../../hcommon.jsp"></jsp:include>
	<link rel="stylesheet" type="text/css" href="${path }/resource/js/ztree/css/ztree.css" />
	<link rel="stylesheet" type="text/css" href="${path }/resource/js/ztree/css/zTreeStyle.css" />
	<script src="${path }/resource/js/ztree/js/jquery.ztree.all-3.4.min.js" ></script>
	<script src="${path }/resource/js/ztree/js/jquery.ztree.exhide-3.4.min.js" ></script>
	<%
	String appName = ParamUtils.getParamValue("application_name");
	%>
</head>
<style type="text/css">
html,body {
    overflow: hidden;
    margin:0px;
}
#divTree {
    height: 380px;
    overflow-x: auto;
    overflow-y: scroll;
    text-align: left;
    white-space: nowrap;
}
</style>
<script type="text/javascript">
    var appName = "<%=appName%>";
    var _menuTree;//当前菜单树对象
    var menudata; //所有的菜单信息
    var rootId = "0";
    var oldSelMenuId = "";//已经选中的菜单id
    
    $(document).ready(function() {
    	oldSelMenuId = AppKit.getInvokeWindow().$('#menuUpId').val();
    	menudata= AppKit.getInvokeWindow().getmenudata();
    	buildMenuTree();
    });
    

    function buildMenuTree(){
        try{
        	 var treedata = new Array();
             
             for(var i in menudata){
                 var j = menudata[i];
                 
                 var tree = {"id":j.id,"pId":j.pId,"name":j.name,"data":j.data,"iconSkin":"menu"};

                 if(j.id==oldSelMenuId){
                     tree.checked=true;
                 }
                 
                 treedata.push(tree);
             }
             
             var upSetting = {
                     view: {
                         fontCss: getTreeFont,
                         autoCancelSelected: false,
                         selectedMulti: false,
                         dblClickExpand: false,
                         showLine: false,
                         expandSpeed: 0
                     },
                     data: {
                         simpleData: {
                             enable: true,
                             rootPId: rootId
                         }
                     },
                     callback: {
                         onClick: onTreeUpMenuClick
                     },
                     check: {
                         enable: true,
                         chkStyle: "radio",
                         radioType: "all",
                         autoCheckTrigger: false
                     }
             };
             $.fn.zTree.init($("#tvMenu"), upSetting, treedata);
             _menuTree = $.fn.zTree.getZTreeObj("tvMenu");
         } catch(e){
             alert(e.message);
         }
     }
    
    function getTreeFont(treeId, node) {
        return (!!node.highlight) ? {"background-color":"#ffff96"} : (node.font ? node.font : {"background-color":"transparent"});
    }
    
    function onTreeUpMenuClick(event, treeId, treeNode, clickFlag) {
        _upmenuTree.expandNode(treeNode);
    }
    
    function sub(){
        var selectNodes=_menuTree.getCheckedNodes(true);
        if(selectNodes.length==0){
            AppKit.showTipMessage('请选择菜单！');
            return;
        }
        
        AppKit.getInvokeWindow().$('#menuUpId').val(selectNodes[0].id);
        AppKit.getInvokeWindow().$('#menuUpName').val(selectNodes[0].name);
        AppKit.closeWindow();
        
    }
    function closeWindow(){
        AppKit.closeWindow();
    }
</script>
<body>
      <div id="divMenuTree" style="border:  1px solid #F5FAFC;height: 300px;overflow:auto">
        <ul id="tvMenu" class="ztree"  style="height: 300px;">
        </ul>
   	 </div>
    <div   align="center" style="padding: 3px 0 3px 0">
           <a class="btn btn-sm btn btn-sm btn-primary" href="javascript:sub()"  class="add"><span>确定</span></a>
           <a class="btn btn-sm btn btn-sm btn-primary" href="javascript:closeWindow()"  class="return" ><span>取消</span></a>
    </div>
    
</body>
</html>