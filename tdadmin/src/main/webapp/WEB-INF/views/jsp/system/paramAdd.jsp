<!DOCTYPE html>
<%@page import="com.tdcy.framework.util.SessionUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="biztag"%>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>配置管理中心</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jsp:include page="../../hcommon.jsp"></jsp:include>
</head>
<script type="text/javascript"> 
var data  = AppKit.getInvokeWindowData();
var operflag = data["operflag"];
var formValid = null;

$(function() {
	formValid = AppKit.formTipValidate("addform");
	if(operflag=="update"){
		getinfo();
	}
});

function getinfo(){
	var paramObj =	{"code":data["code"]};
	console.log(paramObj);
	var url = "${path}/common/getparam";
	AppKit.postJSON(url,paramObj,function(json){
		if(!AppKit.checkJSONResult(json)){
	    	return;
	    }
		
		$("#code").val(json.data["code"]);
		$("#value").val(json.data["value"]);
		$("#remark").val(json.data["remark"]);
	});
}

function add(){
	var flag = formValid.check();
	if(!flag){
		return;
	}
	
	var paramObj =	$("#addform").serializeObject();
	console.log(paramObj);
	var url = "";
	if(operflag=="update"){
		url = "${path}/common/updateparam";
	}else{
		url = "${path}/common/addparam";
	}
	AppKit.postJSON(url,paramObj,saveSuccess);
}

function saveSuccess(json){
	if(!AppKit.checkJSONResult(json)){
    	return;
    }
	AppKit.showTipMessage(json.message,"",function(){
		AppKit.getInvokeWindow().reloadTable();
		AppKit.closeWindow();
	});
}
</script>
<body class="white-bg" style="margin-left: 5px;margin-top: 5px;" >
	<form id="addform" method="post" class="form-horizontal">
	  <table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
	    <tbody>
	      <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>参数名称：</label></td>
	        <td class="width-35">
	          <input id="code" name="code" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
	        <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>参数值:</label></td>
	        <td class="width-35">
	          <input id="value" name="value" class="form-control" datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
	      <tr>
	        <td class="width-15 active text-right">
	          <label>
	                              参数描述:</label>
	        </td>
	        <td class="width-35">
	          <textarea name="remark" id="remark" class="form-control" cols="35" rows="3"  ></textarea>
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
	    </tbody>
	  </table>
	  
	
	</form>
	  <div style="text-align: center;">
		 <button id="savebtn" class="btn btn-sm btn-primary" onclick="add();return false;"><i class="fa fa-plus"></i>保存</button>
		</div>
	
</body>
</html>
