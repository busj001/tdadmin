<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head title="选择部门">
	<meta charset="utf-8" />
	<title>中国芝麻开门集团在线商城管理平台</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jsp:include page="../../hcommon.jsp"></jsp:include>
    <link rel="stylesheet" type="text/css" href="${path }/resource/js/ztree/css/zTreeStyle.css">
    <link rel="stylesheet" type="text/css" href="${path }/resource/js/ztree/css/ztree.css">
    <script src="${path }/resource/js/ztree/js/jquery.ztree.all-3.4.min.js" type="text/javascript"></script>
    <script src="${path }/resource/js/ztree/js/jquery.ztree.exhide-3.4.min.js" type="text/javascript"></script>
</head>
<style type="text/css">
html,body {
    overflow: hidden;
}

#divTree {
    height: 290px;
    overflow-x: auto;
    overflow-y: scroll;
    text-align: left;
    white-space: nowrap;
}
</style>
<script type="text/javascript">
	var jsName = "${companyName}";
    var _deptTree;//当前部门树对象
    var deptdata; //所有的部门信息
    
    var callback;
    
    $(document).ready(function() {
    	setTimeout(function(){
    		 callback  = AppKit.getInvokeWindowData();
		     getDeptTree();
    	},10);
    });
    
    function getDeptTree(){
    	var url ="${path}/sys/getalldept";
        AppKit.postJSON(url, null , showDeptTree);
    }
    
    function showDeptTree(json){
        if (!AppKit.checkJSONResult(json)) {
            return;
        }
        deptdata = json.data;
        buildDeptTree();
    }
    
    function buildDeptTree(){
       try{
            var treedata = new Array();
            var rootdept = {"id":0,"pId":"","name":jsName,"data":"", open:true, "iconSkin":"system"};
            treedata.push(rootdept);
            for(var i in deptdata){
                var j = deptdata[i];

                var tree = {"id":j.deptId,"pId":j.deptUpId,"name":j.deptName,"data":"","iconSkin":"menu"};
                treedata.push(tree);
            }
            
            var setting = {
                    view: {
                        fontCss: getTreeFont,
                        autoCancelSelected: false,
                        selectedMulti: false,
                        dblClickExpand: false,
                        showLine: true
                    },
                    data: {
                        simpleData: {
                            enable: true,
                            rootPId: "0"
                        }
                    },
                    callback: {
                        
                    },
                    check: {
                        enable: true,
                        chkStyle: "radio",
                        radioType: "all",
                        autoCheckTrigger: false
                    }
            };
        
            $.fn.zTree.init($("#tvDept"), setting, treedata);
            _deptTree = $.fn.zTree.getZTreeObj("tvDept");
        } catch(e){
            alert(e.message);
        }
    }
    function getTreeFont(treeId, node) {
        return (!!node.highlight) ? {"background-color":"#ffff96"} : (node.font ? node.font : {"background-color":"transparent"});
    }
    
    function sub(){
        var selectNodes=_deptTree.getCheckedNodes(true);
        if(selectNodes.length==0){
            AppKit.showTipMessage('请选择部门！');
            return;
        }
        var deptjson = {};
        deptjson["deptId"] = selectNodes[0].id;
        deptjson["deptName"] = selectNodes[0].name;
        
        if(callback){
        	callback(deptjson);
        }
        AppKit.closeWindow();
        
    }
    function closeWindow(){
        AppKit.closeWindow();
    }
</script>
<body>
      <div id="divDeptTree" style="border:  1px solid #F5FAFC;height: 300px;overflow:auto">
         <ul id="tvDept" class="ztree" style=" height: 290px">
         </ul>
     </div>
    <div class="btngroup"  align="center" >
           <a href="javascript:sub()"  class="btn btn-sm btn btn-sm btn-primary"><span>确定</span></a>
           <a href="javascript:closeWindow()"  class="btn btn-sm btn btn-sm btn-primary" ><span>取消</span></a>
    </div>
</body>
</html>