<!DOCTYPE html>
<%@page import="com.tdcy.sys.util.BusiContextExt"%>
<%@page import="com.tdcy.sys.service.bean.LoginInfoBean"%>
<%@page import="com.tdcy.sys.util.ParamUtils"%>
<%@page import="com.tdcy.framework.util.SessionUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>配置管理中心</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jsp:include page="../../hcommon.jsp"></jsp:include>
	<%
		LoginInfoBean lib = BusiContextExt.getLoginInfo();
		String defaultPassword = ParamUtils.getParamValue("default_password");
	%>
</head>
<script type="text/javascript">

$(document).ready(function () {
	 $.jgrid.defaults.width = 960;
	 $.jgrid.defaults.responsive = true;
	 $.jgrid.defaults.styleUI = 'Bootstrap';
	 $.jgrid.ajaxOptions.type = 'post';
	 initmodelGridIdTable();
	 initUI();
});

function initUI(){
}

/**
*初始化表单
*/
function initmodelGridIdTable() {
	var queryParams = {};
	var colModel=[];
   var col1={label: '登陆名', name: 'loginUser', align: 'center',width: 120,sortable:true, checkbox:true};
      colModel.push(col1);
   var col2={label: '姓名', name: 'staffName', align: 'center',width: 120,sortable:true,};
      colModel.push(col2);
      var col3={label: '用户状态', name: 'staffSta', align: 'center',width: 120,sortable:true,formatter:jsonFormatter};
      colModel.push(col3);
      var col4={label: '创建时间', name: 'createTime', align: 'center',width: 120,sortable:true,};
      colModel.push(col4);
      var col5={label: 'staffId ', name: 'staffId', align: 'center',width: 120,sortable:true,hidden:true};
      colModel.push(col5);
	var settings={
       postData: queryParams,
       styleUI : 'Bootstrap',
	    mtype: "post",
	    url: "${path}/sys/querystaff",
	    prmNames:{//请求参数格式预处理
	          page:"page",
	          rows:"rows", 
	          sort:"sort",
	          order: "order"
	    },
       datatype: "json",
       colModel:colModel,
       pageable: false,
	    page: 1,
       rowNum: '-1',
       multiSort: true,
	    sortable: true,
	    sortname: "id",
	    sortorder: "asc",
	    multiselect: true,
	    shrinkToFit: true,
	    height: 550,
	    shrinkToFit: true,
	   
	    jsonReader: {//返回参数格式处理
	        root: "data"
	    },
	    rownumbers: false,
	    multikey: "true",
	    autowidth: true,
      };
      $("#queryGrid").jqGrid(settings);
  };
  
  function jsonFormatter(val,rec){
	    var retval = "";
	       if(val == '1'){ 
	    	   retval = '正常'
	    	}else{
	    	   retval = '禁用';
	       }
	    return retval;
	}
	
	function resetPwd(){
		var ids=$('#queryGrid').jqGrid('getGridParam','selarrrow');
		 var arras = new Array();
         for (var i = 0; i < ids.length; i++) {
        	 var rowData = $("#queryGrid").jqGrid('getRowData',ids[i]);
        	 arras.push("'"+rowData.loginUser+"'");
         }
		if (arras && arras.length>0) {
				AppKit.showConfirmMessage(function() {
					url = '${path}/sys/resetpass?loginUsers=' + arras.join(",");
					AppKit.postJSON(url, "", resetPwdSuccess);
				});
			
		} else {
			AppKit.showTipMessage("请选择记录");
		}
	}
	
	function resetPwdSuccess(json) {
		if (!AppKit.checkJSONResult(json)) {
			return;
		}
		AppKit.showTipMessage(json.message);
	}
	
	function reloadTable(){
		$("#queryGrid").trigger("reloadGrid"); //重新载入    
	}
	
	function search(){
		var queryParams  = $("#queryForm").serializeObject();
		console.log(queryParams);
	   //传入查询条件参数  
	   $("#queryGrid").jqGrid('setGridParam',{  
	       datatype:'json',  
	       postData:queryParams, //发送数据  
	       page:1  
	   }).trigger("reloadGrid"); //重新载入    
	}
	

	function resetpwd1() {
		var rowData = $('#grid').datagrid('getSelections');
		if(rowData.length==0){
			AppKit.showTipMessage("请选择用户");
			return;
		}
		var row = new Array();
		for(var i in rowData ){
			row.push("'"+rowData[i].loginUser+"'");
		}
		if (rowData && rowData.length>0) {
			AppKit.showConfirmMessage(function() {
				url = '${path}/sys/resetpass?loginUsers=' + row.join(",");
				AppKit.postJSON(url, "", resSuccess);
			},"确认将密码重置为["+defaultPassword+"]");
		} else {
			AppKit.showTipMessage("请选择需要进行密码重置的员工！");
		}
	}
</script>
<body class="gray-bg skin-1">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>
						密码管理</h5>
					</div>
					<div class="ibox-content">
					    <div>
							<!-- 查询条件 -->
							<form id="queryForm">
							<div  class="row">
								<div  class="col-sm-12" style="padding-bottom: 10px;">
								 	<div class="form-inline">
								 		<div class="form-group">
								 	          <label class="control-label">工号：</label>
								 	           <input id="loginUser" name="loginUser" class="form-control"    />  
								 	    </div>
								 	    
								 	    <div class="form-group">
								 	          <label class="control-label">姓名：</label>
								 	           <input id="staffName" name="staffName" class="form-control"    />  
								 	    </div>
									</div>
								</div>
							</div>
							</form>
							
							<!-- 工具栏 -->
							<div class="row">
								<div class="col-sm-12">	
									<div class="pull-left">
									      <button class="btn btn-sm btn btn-sm btn-primary" onclick="resetPwd()"><i class="fa fa fa-plus"></i>密码重置</button>
									</div>
									<div class="pull-right">
										 <button class="btn btn-sm btn btn-sm btn-primary" onclick="search()"><i class="fa fa-search"></i> 搜索</button>
									</div>
								</div>
							</div>
							<div class="jqGrid_wrapper uadmin-grid-margin" >
						        <table id="queryGrid">
						        </table>
						        <div id="pager_queryGrid"></div>
							</div>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</div>
</body>
</html>