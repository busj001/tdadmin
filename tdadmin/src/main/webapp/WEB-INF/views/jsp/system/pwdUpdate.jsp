<!DOCTYPE html>
<%@page import="com.tdcy.sys.util.BusiContextExt"%>
<%@page import="com.tdcy.sys.service.bean.LoginInfoBean"%>
<%@page import="com.tdcy.sys.util.ParamUtils"%>
<%@page import="com.tdcy.framework.util.SessionUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>配置管理中心</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jsp:include page="../../hcommon.jsp"></jsp:include>
	<%
		LoginInfoBean lib = BusiContextExt.getLoginInfo();
		
	%>
</head>
<script type="text/javascript"> 
var loginUser = "<%=lib.getUserEO().getLoginUser()%>"
function updatePwd(){
	
	var oldpassword  =$('#oldpassword').val();
	var userpassword  =$('#userpassword').val();
	var userpassword2  =$('#userpassword2').val();
	if(userpassword==userpassword2){
		var data = {};
		data["loginuser"] = loginUser;
		data["oldpass"] = $.md5(oldpassword) ;
		data["firstpass"] = $.md5(userpassword);
		data["secondpass"] = $.md5(userpassword2) ;
		AppKit.postJSON("${path}/sys/changepass",data,pwdcallback);
	} else {
		AppKit.showTipMessage("两次密码不一致，请修改!");
	}
}

function pwdcallback(json){
	if(!AppKit.checkJSONResult(json)){
	    return;
    }
    
	//修改后跳入
	AppKit.showTipMessage(json.message,"",function(){
		 AppKit.postJSON('${path}/sys/logout', "" ,logoutBack2); 
    });
}
 
function logoutBack2(json){
    if(!AppKit.checkJSONResult(json)){
        return;
    }
    window.location.href = '${path}/common/showpage?page=login';
}

function closeWindow(){
	AppKit.closeWindow();
}
</script>
	
<table style="border: none" width="100%"  >
	<tr align="top">
		<td>
			<div class="easyui-panel">
			<table cellpadding="5" class="viewdatagrid" style="border: none">
				<tr>
					<th style="width: 80px;">原密码:</th>
					<td><input id="oldpassword" name="oldpassword"
						class="easyui-validatebox" type="password"
						data-options="required:true"></input></td>
				</tr>
				<tr>
					<th>新密码:</th>
					<td><input id="userpassword" name="userpassword"
						class="easyui-validatebox" type="password"
						data-options="required:true"></input></td>
				</tr>
				<tr>
					<th>确认密码:</th>
					<td><input id="userpassword2" name="userpassword2"
						class="easyui-validatebox" type="password"
						data-options="required:true"></input></td>
				</tr>
			</table>
			</div>
		</td>
		 
					 
			 
	</tr>
	
	<tr align="center" style="width: 100%">
		<td class="buttonline" style="text-align: center;padding: 10px 0px 10px 0px" >
					<a href="javascript:updatePwd()"  class="easyui-linkbutton"><span>修改</span></a>
		        	 <a href="javascript:closeWindow()"  class="easyui-linkbutton" ><span>取消</span></a>
		      
		</td>
	</tr>
</table>
</html>
