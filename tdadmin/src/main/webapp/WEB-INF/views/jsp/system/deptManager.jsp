<!DOCTYPE html>
<%@ taglib tagdir="/WEB-INF/tags" prefix="biztag"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>中国芝麻开门集团在线商城管理平台</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jsp:include page="../../hcommon.jsp"></jsp:include>
	<link rel="stylesheet" type="text/css" href="${jsPath }/ztree/css/ztree.css" />
	<link rel="stylesheet" type="text/css" href="${jsPath }/ztree/css/zTreeStyle.css" />
	<script src="${jsPath }/ztree/js/jquery.ztree.all-3.4.min.js" ></script>
	<script src="${jsPath }/ztree/js/jquery.ztree.exhide-3.4.min.js" ></script>
<style type="text/css">
html,body {
	overflow: hidden;
}

#tvDept {
	height: 600px
}

#divTree {
	height: 680px;
	overflow-x: auto;
	overflow-y: scroll;
	text-align: left;
	white-space: nowrap;
	width: 280px
}
</style>
<script type="text/javascript">
	var jsName = "${companyName}";
	var curDeptId; //当前选择的部门id
	var curDeptInfo ;//当前选择的部门信息
	var _deptTree;//当前部门树对象
	var deptdata; //所有的部门信息
	
	var NEWID = "-100";
	
	var reloadFlag = false; //加载树后是否需要重新加载curDeptId对应数据
	
	var formValid = null;

	$(document).ready(function() {
		formValid = AppKit.formTipValidate("deptform");
		curDeptId = NEWID;
		getDeptTree();
	});
	
	function getDeptTree(){
		var url ="${path}/sys/getalldept";
		AppKit.postJSON(url, null , showDeptTree);
	}
	
	function showDeptTree(json){
		if (!AppKit.checkJSONResult(json)) {
            return;
        }
		deptdata = json.data;
		//alert(AppKit.tostring(deptdata));
		buildDeptTree(deptdata);
	}
	
	function buildDeptTree(deptList){
	   try{
		   var treedata = new Array();
	       var rootdept = {"id":0,"pId":"","name":jsName,"data":"", open:true, "iconSkin":"system"};
	       treedata.push(rootdept);
	     	for(var i in deptList){
	     		var j = deptList[i];
	     		var tree = {"id":j.deptId,"pId":j.deptUpId,"name":j.deptName,"data":"","iconSkin":"menu"};
	     		treedata.push(tree);
	     	}
	    	
    	    var setting = {
	    			view: {
	    				fontCss: getTreeFont,
	    				autoCancelSelected: false,
	    				selectedMulti: false,
	    				dblClickExpand: false,
	    				showLine: true
	    			},
	    			data: {
	    				simpleData: {
	    					enable: true,
	    					rootPId: "0"
	    				}
	    			},
					callback: {
						onClick: onTreeClick
					}
	    	};
        
	        $.fn.zTree.init($("#tvDept"), setting, treedata);
	        _deptTree = $.fn.zTree.getZTreeObj("tvDept");
	      if(curDeptId != NEWID && reloadFlag){
	    	  selectDept(curDeptId);
	      }
	    } catch(e){
	    	alert(e.message);
	    }
	}
	
	function getTreeFont(treeId, node) {
    	return (!!node.highlight) ? {"background-color":"#ffff96"} : (node.font ? node.font : {"background-color":"transparent"});
    }
	
	function onTreeClick(event, treeId, treeNode, clickFlag) {
		if(treeNode.id == curDeptId){
			return;
		}
		
    	if(_deptTree){
    		showDeptInfo(treeNode.id);    		
    	}
	}
	
	function showDeptInfo(id){
    	var deptinfo = findDept(id);
// 		alert(AppKit.tostring(deptinfo));
        for(var k in deptinfo) {
        	$('#deptform #' + k).val(deptinfo[k]);
        }
        
        curDeptId = id;
        
        //设置下拉部门的值
        showUpDeptName(deptinfo["deptUpId"]);
        curDeptInfo = $('#deptform').serializeObject();
	 }
	 
	 function showUpDeptName(deptId){
	    	var upDeptName = "";
	    	var upDeptId = "";
	    	var upDept = {};
	    	if(deptId=="0"){
	    		upDept["deptId"] = "0";
	    		upDept["deptUpId"] = "";
	    		upDept["deptName"] = jsName;
	    	}else{
	    		upDept = findDept(deptId);
	    	}
	        if(upDept){
	        	upDeptName = upDept.deptName;
	        	upDeptId = upDept.deptId;
	        }
	        $('#deptform #deptUpName').val(upDeptName);
	        $('#deptform #deptUpId').val(upDeptId);
	  }
	 
	 function findDept(deptId) {
        var dept = null;
        for (var i in deptdata) {
            if (deptdata[i].deptId == deptId) {
                dept = deptdata[i];
                break;
            }
        }

        return dept;
     }
	 
	 function showUpDept() {
		 url = "${path}/views/jsp/system/getDept";
		 AppKit.openWindow(url,'选择上级部门',400,400,function(json){
			  $('#deptform #deptUpName').val(json["deptName"]);
		      $('#deptform #deptUpId').val(json["deptId"]);
		 });
	}
	
    function selectDept(deptId){
    	var node = _deptTree.getNodeByParam("id", deptId, null);
    	if(node){
    		if(node.isParent){
    			_deptTree.expandNode(node, true, false, true, false);	
			} else {
				_deptTree.expandNode(node.getParentNode(), true, false, true, false);
			}
    		_deptTree.selectNode(node, false);
    	}
    } 
    
    
	function doSave() {
		var flag = formValid.check();
		if(!flag){
			return;
		}
		
		var deptinfo = $("#deptform").serializeObject();
		if(deptinfo == curDeptInfo){
			AppKit.showTipMessage("没有修改不要保存");
			return;
		}

		var url = "";
		reloadFlag = true;
		if(curDeptId == NEWID){
			if(AppKit.isnull(deptinfo.deptName) || !AppKit.isvalue(deptinfo.deptName)){
				AppKit.showTipMessage("部门名称不能为空");
				return;
			}
			url ="${path}/sys/adddept";
		}else{
			url ="${path}/sys/updatedept";
			deptinfo["deptId"] = curDeptId;
		}
		AppKit.postJSON(url,deptinfo, saveSuccess);
	}
	
    function saveSuccess(json){
	    if(!AppKit.checkJSONResult(json)){
		    return;
	    }

	    if(json.data){
	    	curDutyId = json.data;
		}
	    
	    curDeptInfo = $('#deptform').serializeObject();
	    //重新构建树
	    getDeptTree();
	    AppKit.showTipMessage(json.message,"",function(){
	    	 reloadFlag = false;
	 	     curDeptId = NEWID;
	 	     setEmptyDept();
	 	   
	    });
    }
	
	function doAdd() {
		setEmptyDept();
		showUpDeptName(curDeptId);
		
		curDeptId = NEWID;
	}

	function doDel() {
		if(curDeptId == NEWID){
			AppKit.showTipMessage("请选择要删除的记录");
			return;
		}

		AppKit.showConfirmMessage(function(){
			var url ="${path}/sys/deldept";
			AppKit.postJSON(url, {"deptId": curDeptId }, delSuccess);
	    },"确认删除吗？","操作提醒");
		
		
	}
	
    function delSuccess(json){
	    if(!AppKit.checkJSONResult(json)){
		    return;
	    }
	    
	    AppKit.showTipMessage(json.message);
	    
	    reloadFlag = false;
	    //重新构建树
	    getDeptTree();
	    curDeptId = NEWID;
	    
	    setEmptyDept();
	    
	   
    }
    
    function setEmptyDept(){
    	$("#deptform input").each(function(){
    		$(this).val("");
    	});
    	
    	$("#deptform textarea").each(function(){
    		$(this).val("");
    	});
    	
    }
    
</script>

<body class="gray-bg skin-1">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>
						部门管理</h5>
					</div>
					<div class="ibox-content">
					    <div>
					    	<div class="row">
								<div class="col-sm-3 col-md-3" >
									  <div id="divDeptTree" style="border:  0px solid #F5FAFC;width: 290px;overflow:auto">
		                                <ul id="tvDept" class="ztree" >
		                                </ul>
		                              </div>
								</div>
								<div  class="col-sm-9 col-md-9">
									<form  id="deptform"  class="form-horizontal" >  
								       <div class="form-group">   
								         <label class="col-sm-2 control-label">部门名称:</label> 
								        <div class="col-sm-9">  
								            <input class="form-control" name="deptName"  id="deptName" />
								       </div>  
								       </div>   
								      <div class="form-group">   
								         <label class="col-sm-2 control-label">上级部门：</label> 
								        <div class="col-sm-9">  
									       	  <div class="form-inline">
									            <input class="form-control" name="deptUpName"  id="deptUpName" readonly="true" style="width: 180px;height: 32px;"/>
												<input name="deptUpId" type="hidden" id="deptUpId" />
												<button class="btn btn-sm btn btn-sm btn-primary" id="btnUpOk" onclick="javascript:showUpDept();return false;" ><span>选择</span></button>
												<div id="divUpMenu" style="display:none; height: 450px; overflow:auto;" align="center">
													<div style="height: 390px; overflow:auto;border: #d6d6dd 1px solid;;">
														<ul id="tvUp" class="ztree"></ul>
													</div>
												</div>
												</div>
								         </div>  
								       </div>  
								       <div class="form-group">   
								        <label for="lastname" class="col-sm-2 control-label">联系方式:</label>   
								        <div class="col-sm-10">  
								         <input class="form-control" name="tel"  id="tel" />
								         </div>  
								       </div> 
								       <div class="form-group">   
								         <label class="col-sm-2 control-label">部门描述：</label>
								        <div class="col-sm-10">  
								           <textarea class="form-control" name="remark"  id="remark" cols="35" rows="3"></textarea>
								         </div>  
								       </div> 
								     </form> 
								     <div style="text-align: center;margin-top: 2px;margin-bottom: 2px;">
										<button class="btn btn-sm btn btn-sm btn-primary" onclick="javascript:doAdd();" ><span>新增</span></button>
										<button class="btn btn-sm btn btn-sm btn-info"  onclick="javascript:doSave();" ><span>保存</span></button>
										<button class="btn btn-sm btn btn-sm btn-danger" onclick="javascript:doDel();" ><span>删除</span></button>
									</div>
     
								</div>
							</div>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</div>
</body>
</html>