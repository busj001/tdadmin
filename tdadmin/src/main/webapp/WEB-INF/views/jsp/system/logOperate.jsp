<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>配置管理中心</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jsp:include page="../../hcommon.jsp"></jsp:include>
</head>
<script type="text/javascript">

$(document).ready(function () {
	 $.jgrid.defaults.width = 960;
	 $.jgrid.defaults.responsive = true;
	 $.jgrid.defaults.styleUI = 'Bootstrap';
	 $.jgrid.ajaxOptions.type = 'post';
	 initQueryGrid();
	 initUI();
});

function initUI(){
	 $('#startdateDiv').datepicker({
	     keyboardNavigation: false,
       forceParse: false,
       autoclose: true
   });
	 
	 $('#enddateDiv').datepicker({
	     keyboardNavigation: false,
       forceParse: false,
       autoclose: true
   });
}

function initQueryGrid() {
	var queryParams = {};
	var colModel=[];
   var col1={label: '登陆名称', name: 'loginUser', align: 'center',width: 120,sortable:true};
      colModel.push(col1);
   var col2={label: '姓名', name: 'userName', align: 'center',width: 120,sortable:true,};
      colModel.push(col2);
      var col3={label: '操作url', name: 'operateUrl', align: 'center',width: 120,sortable:true};
      colModel.push(col3);
   var col5={label: '操作类', name: 'operateClass', align: 'center',width: 200,sortable:true};
      colModel.push(col5);
      var col3={label: '操作方法', name: 'operateMethod', align: 'center',width: 120,sortable:true};
      colModel.push(col3);
      var col3={label: '备注', name: 'remark', align: 'center',width: 120,sortable:true};
      colModel.push(col3);
      var col3={label: '操作时间', name: 'createTime', align: 'center',width: 120,sortable:true};
      colModel.push(col3);
      
	var settings={
       postData: queryParams,
       styleUI : 'Bootstrap',
	    mtype: "post",
	    url: "${path}/sys/getoperlog",
	    prmNames:{//请求参数格式预处理
	          page:"page",
	          rows:"rows", 
	          sort:"sort",
	          order: "order"
	    },
       datatype: "json",
       colModel:colModel,
       pageable: true,
	    page: 1,
       rowNum: 10,
       pager: "#pager_queryGrid",
       multiSort: true,
	    sortable: true,
	    sortname: "id",
	    sortorder: "asc",
	    multiselect: false,
	    shrinkToFit: true,
	    height: 550,
	    shrinkToFit: true,
	   
	    jsonReader: {//返回参数格式处理
	        root: "data.resultsList",
	        page: "data.curPageNO",
	        total: "data.totalPage",
	        records: "data.totalRecord"
	    },
	    rownumbers: false,
	    multikey: "true",
	    autowidth: true,
      };
      $("#queryGrid").jqGrid(settings);
  };
  
	
	function reloadTable(){
		$("#queryGrid").trigger("reloadGrid"); //重新载入    
	}
	
	function search(){
		var queryParams  = $("#queryForm").serializeObject();
	   //传入查询条件参数  
	   $("#queryGrid").jqGrid('setGridParam',{  
	       datatype:'json',  
	       postData:queryParams, //发送数据  
	       page:1  
	   }).trigger("reloadGrid"); //重新载入    
	}
</script>
<body class="gray-bg skin-1">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>
						登录日志列表</h5>
					</div>
					<div class="ibox-content">
					    <div>
							<!-- 查询条件 -->
							<form id="queryForm">
							<div  class="row">
								<div  class="col-sm-12" style="padding-bottom: 10px;">
								 	<div class="form-inline">
								 		<div class="form-group">
								 	          <label class="control-label">登录名：</label>
								               <input id="loginUser" name="loginUser" class="form-control"   />  
								         </div>
								 	    <div class="form-group">
								 	          <label class="control-label">开始日期：</label>
								 	          <div class='input-group date' id='startdateDiv'>  
								                  <input id="startTime" name="startTime" class="form-control"   />  
								                  <span class="input-group-addon">  
								                    <span class="glyphicon glyphicon-calendar"></span>  
								                 </span>  
								             </div>
								 	    </div>
								 	    <div class="form-group">
								 	          <label class="control-label">结束日期：</label>
								             <div class='input-group date' id='enddateDiv'>  
								                  <input id="endTime" name="endTime" class="form-control"     />  
								                  <span class="input-group-addon">  
								                    <span class="glyphicon glyphicon-calendar"></span>  
								                  </span>  
								             </div>
								 	    </div>
									</div>
								</div>
							</div>
							</form>
							
							<!-- 工具栏 -->
							<div class="row">
								<div class="col-sm-12">	
									<div class="pull-left">
									</div>
									<div class="pull-right">
										 <button class="btn btn-sm btn btn-sm btn-primary" onclick="search()"><i class="fa fa-search"></i> 搜索</button>
									</div>
								</div>
							</div>
							<div class="jqGrid_wrapper uadmin-grid-margin" >
						        <table id="queryGrid">
						        </table>
						        <div id="pager_queryGrid"></div>
							</div>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</div>
</body>

</html>