<!DOCTYPE html>
<%@page import="com.tdcy.framework.util.SessionUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>配置管理中心</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jsp:include page="../../hcommon.jsp"></jsp:include>
</head>
<script type="text/javascript"> 
var row = 0;
var formValid = null;
$(document).ready(function(){
	formValid = AppKit.formTipValidate("addform");
	 
	insertRow();
});

function add(){
	var flag = formValid.check();
	if(!flag){
		return;
	}
	
	var paramObj =	$("#addform").serializeObject();

	//codesql和代码配置不能同时有
	var codesql = paramObj.codesql;
	var datavalue = paramObj.data_value;
	var displayvalue  = paramObj.display_value;

	if(AppKit.isEmptyVal(codesql) && AppKit.isEmptyVal(datavalue)  ){
		AppKit.showTipMessage("请填写代码配置或者代码sql中的一项");
		return;
	}
		
	if(!AppKit.isEmptyVal(codesql) && !AppKit.isEmptyVal(datavalue)  ){
		AppKit.showTipMessage("代码配置和代码sql不能同时存在");
		return;
	}

	if(AppKit.isEmptyVal(codesql)){
		if(AppKit.isEmptyVal(datavalue) || AppKit.isEmptyVal(displayvalue) ){
			AppKit.showTipMessage("代码配置有空值存在，非法");
			return;
		}

		if(!AppKit.isarray(datavalue)){
			datavalue =  datavalue.split(",");
		}
		if(!AppKit.isarray(displayvalue)){
			displayvalue =  displayvalue.split(",");
		}

		for(var i=0;i<datavalue.length;i++){
			for(var j=i+1;j<datavalue.length;j++){
				if(datavalue[i] == datavalue[j]){
					AppKit.showTipMessage("不能存在相同代码值:"+datavalue[i]);
					return;
				}
			}
		}
		
		if(!AppKit.isEmptyVal(datavalue) && !AppKit.isEmptyVal(displayvalue)){
			for(var i=0;i<datavalue.length;i++){
				if(AppKit.isEmptyVal(datavalue[i]) || AppKit.isEmptyVal(displayvalue[i])){
					AppKit.showTipMessage("第"+(i+1)+"行代码配置有空值存在，非法");
					return;
				}
			}
		}
		if(!AppKit.isEmptyVal(datavalue) && !AppKit.isEmptyVal(displayvalue)){
			paramObj.data_value =datavalue.join(",");
			paramObj.display_value = displayvalue.join(",");
		}
	}
	
		var url = "${path}/common/addcode";
		AppKit.postJSON(url,paramObj,saveSuccess);
}

function saveSuccess(json){
	if(!AppKit.checkJSONResult(json)){
    	return;
    }
	AppKit.showTipMessage(json.message,"",function(){
		var data = AppKit.getOpenWinObj().window("options").data;
		if(data){
			data.datagrid("load");
		}
		AppKit.closeWindow();
	});
}

function onClickAdd(rowid){
	insertRow();

	$("#addbtn"+rowid).hide();
	$("#delbtn"+rowid).show();
}

function onClickRemove(rowid){
	var pDiv = document.getElementById("codeConfigTable"); 
	var sDiv = document.getElementById("div"+rowid); 
	pDiv.removeChild(sDiv);
}

function insertRow(){
	var ihtml = "";
	ihtml += "<div id=\"div"+row+"\">";
	ihtml += "<span style='display: inline;'><input   id=\"data_value"+row+"\" type=\"text\"  name=\"data_value\"    ></input></span>&nbsp;&nbsp;";
	ihtml += "<span style='display: inline;'><input    id=\"display_value"+row+"\" type=\"text\"  name=\"display_value\"  ></input></span>&nbsp;&nbsp;";
	ihtml += "<span style='display: inline;'><input   id=\"addbtn"+row+"\" type=\"button\" onclick=\"onClickAdd('"+row+"')\" value=\"+\"/><input id=\"delbtn"+row+"\" type=\"button\" onclick=\"onClickRemove('"+row+"')\" style=\"display:none\" value=\"-\"/></span>";
	ihtml += "</div>";
	
	row++;
	document.getElementById("codeConfigTable").insertAdjacentHTML("beforeEnd",ihtml);
}

function closeWindow(){
	AppKit.closeWindow();
}
</script>
<body>
<body class="white-bg" style="margin-left: 5px;margin-top: 5px;" >
	<form id="addform" method="post" class="form-horizontal">
	  <table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
	    <tbody>
	      <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>代码类型：</label></td>
	        <td class="width-35">
	          <input id="code_type" name="code_type" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
	        <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>代码配置:</label></td>
	        <td class="width-35">
	          	<div id="codeConfigTable">
				</div>
	        </td>
	      </tr>
	      <tr>
	        <td class="width-15 active text-right">
	          <label>
	                             代码SQL:</label>
	        </td>
	        <td class="width-35">
	          <textarea name="codesql" id="codesql" class="form-control" cols="35" rows="3"  ></textarea>
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
	    </tbody>
	  </table>
	</form>
	<div style="text-align: center;">
	 代码配置和代码SQL仅能配置一项
	</div>
	<div style="text-align: center;">
	 <button class="btn btn-sm btn-primary" onclick="add();return false;"><i class="fa fa-plus"></i>保存</button>
	</div>
</body>

</body>
</html>
