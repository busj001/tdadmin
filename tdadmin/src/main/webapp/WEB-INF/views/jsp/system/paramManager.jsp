<!DOCTYPE html>
<%@page import="com.tdcy.framework.util.SessionUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>配置管理中心</title>
	<jsp:include page="../../hcommon.jsp"></jsp:include>
</head>

<body class="gray-bg skin-1">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>
						参数列表</h5>
					</div>
					<div class="ibox-content">
					    <div>
							<!-- 查询条件 -->
							<form id="queryForm">
							<div  class="row">
								<div  class="col-sm-12" style="padding-bottom: 10px;">
								 	<div class="form-inline">
								 		<div class="form-group">
								 	          <label class="control-label">参数名称</label>
								 	          <input class="form-control" name="code"  id="code" />
								 	    </div>
									</div>
								</div>
							</div>
							</form>
							
							<!-- 工具栏 -->
							<div class="row">
								<div class="col-sm-12">	
									<div class="pull-left">
									      <button class="btn btn-sm btn btn-sm btn-primary" onclick="openAdd()"><i class="fa fa fa-plus"></i>添加参数</button>
									</div>
									<div class="pull-right">
										 <button class="btn btn-sm btn btn-sm btn-primary" onclick="search()"><i class="fa fa-search"></i> 搜索</button>
									</div>
								</div>
							</div>
							<div class="jqGrid_wrapper uadmin-grid-margin" >
						        <table id="queryGrid">
						        </table>
						        <div id="pager_queryGrid"></div>
							</div>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</div>
</body>

<script type="text/javascript"> 
    $(document).ready(function () {
    	 $.jgrid.defaults.width = 960;
		 $.jgrid.defaults.responsive = true;
		 $.jgrid.defaults.styleUI = 'Bootstrap';
		 $.jgrid.ajaxOptions.type = 'post';
    	 initmodelGridIdTable();
    });
    
    /**
    *初始化表单
    */
    function initmodelGridIdTable() {
    	var queryParams = {};
		var colModel=[];
		
        var col1={label: '参数名称', name: 'code', align: 'center',width: 120,sortable:true, checkbox:true};
           colModel.push(col1);
        var col2={label: '参数值', name: 'value', align: 'center',width: 120,sortable:true,};
           colModel.push(col2);
        var col3={label: '参数描述', name: 'remark', align: 'center',width: 120,sortable:true};
           colModel.push(col3);
        var col5={label: '操作', name: 'code', align: 'center',width: 200,sortable:true,formatter:operFormatter};
           colModel.push(col5);
           
		var settings={
            postData: queryParams,
            styleUI : 'Bootstrap',
		    mtype: "post",
		    url: "${path}/common/getallparam",
		    prmNames:{//请求参数格式预处理
		          page:"page",
		          rows:"rows", 
		          sort:"sort",
		          order: "order"
		    },
            datatype: "json",
            colModel:colModel,
		    pageable: false,
		    page: 1,
		    rowNum: '-1',
            multiSort: true,
		    sortable: true,
		    sortname: "id",
		    sortorder: "asc",
		    multiselect: false,
		    shrinkToFit: true,
		    height: 550,
		    shrinkToFit: true,
		   
		    jsonReader: {//返回参数格式处理
		        root: "data"
		    },
		    rownumbers: false,
		    multikey: "true",
		    autowidth: true,
           };
           $("#queryGrid").jqGrid(settings);
       };
       
   	function operFormatter(value, options, row){
    	 var href="";
         href +="<a href=\"#\" class=\"btn btn-xs btn-primary\"  onclick=\"openUpdate('"+row.code+"')\">修改</a>&nbsp&nbsp";
	  	 href +="<a href=\"#\" class=\"btn btn-xs btn-danger\"  onclick=\"openDel('"+row.code+"')\" >删除</a>&nbsp&nbsp";
	
   	 	return href;
	}
   	
   	function openAdd(){
   		var url = "${path}/views/jsp/system/paramAdd";
   		AppKit.openWindow(url, "添加参数", "800", "500",{"operflag":"add"});
   	}
   	
  	function openUpdate(code){
   		var url = "${path}/views/jsp/system/paramAdd";
   		AppKit.openWindow(url, "修改参数", "800", "500",{"operflag":"update","code":code});
   	}
  	
   	function openDel(code){
   		if (code) {
   				AppKit.showConfirmMessage(function() {
   					url = '${path}/common/delparam?paramKey=' + code;
   					AppKit.postJSON(url, "", delSuccess);
   				});
   			
   		} else {
   			AppKit.showTipMessage("请选择记录");
   		}
   	}
   	
   	function delSuccess(json) {
   		if (!AppKit.checkJSONResult(json)) {
   			return;
   		}
   		AppKit.showTipMessage(json.message,"",function(){
   			reloadTable();
   		});
   	}
   	
   	function reloadTable(){
   		$("#queryGrid").trigger("reloadGrid"); //重新载入    
   	}
   	
   	function search(){
   		var queryParams  = $("#queryForm").serializeObject();
   		console.log(queryParams);
        //传入查询条件参数  
        $("#queryGrid").jqGrid('setGridParam',{  
            datatype:'json',  
            postData:queryParams, //发送数据  
            page:1  
        }).trigger("reloadGrid"); //重新载入    
   	}
   </script>
   
</html>