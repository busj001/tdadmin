<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="com.tdcy.framework.util.SessionUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>公告管理</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jsp:include page="../../hcommon.jsp"></jsp:include>
	<script type="text/javascript">
	var basePath="${basePath}"
	window.UMEDITOR_HOME_URL= "${path}/resource/js/umeditor/";
	</script>
	<link rel='stylesheet' type='text/css' href='${path }/resource/js/umeditor/themes/default/css/umeditor.css' />
	<script type="text/javascript" src="${path }/resource/js/umeditor/third-party/template.min.js"></script>
	<script type="text/javascript"  src="${path }/resource/js/umeditor/umeditor.config.js"></script>
	<script type="text/javascript"  src="${path }/resource/js/umeditor/umeditor.js"></script>
	<script type="text/javascript"  src="${path }/resource/js/umeditor/umeditor.js"></script>
	<script type="text/javascript" src="${path }/resource/js/umeditor/lang/zh-cn/zh-cn.js"></script>

</head>
<script type="text/javascript"> 
var data  = AppKit.getInvokeWindowData();
var oper = data["oper"];
var bid = null;
var ue = null;
var formValid = null;

$(document).ready(function(){
	formValid = AppKit.formTipValidate("addform");
	
	  if(oper=="edit" || oper == "detail"){
		  bid = data["bid"];
		  showuptinfo()
	  }
	  
	  ue = UM.getEditor('container');
});

function showuptinfo(){
	var url = "${path}/sys/getbulletininfo";
	var paramObj = {"bulletinId":bid};
	AppKit.postJSON(url,paramObj,function(json){
		if(oper == "detail"){
			$("#addbtn").hide();
		}
		$("#bulletinTitle").val(json.data["bulletinTitle"]);
		setTimeout(function(){
			setContent(json.data["bulletinContent"]);
		},500)
	});
}

function getContent(){
	var content = ue.getContent();
	return content;
}

function getText(){
	var content = ue.getPlainTxt();
	return content;
}

function setContent(conent){
	ue.setContent(conent);
}

function add(){
	var flag = formValid.check();
	if(!flag){
		return;
	}
	
	var paramObj =	$("#addform").serializeObject();
	paramObj["bulletinContent"] = getContent();
	if(oper == "add"){
		var url = "${path}/sys/addbulletin";
		AppKit.postJSON(url,paramObj,saveSuccess);
	}else if(oper == "edit" ){
		var url = "${path}/sys/updatebulletin";
		paramObj["bulletinId"]=bid;
		AppKit.postJSON(url,paramObj,saveSuccess);
	}
}

function saveSuccess(json){
	if(!AppKit.checkJSONResult(json)){
    	return;
    }
	AppKit.showTipMessage(json.message,"",function(){
		AppKit.getInvokeWindow().reloadTable();
		AppKit.closeWindow();
	});
}

function closeWindow(){
	AppKit.closeWindow();
}
</script>

<body class="white-bg" style="margin-left: 5px;margin-top: 5px;" >
	<form id="addform" method="post" class="form-horizontal">
	  <table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
	    <tbody>
	      <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>公告标题：</label></td>
	        <td class="width-35">
	          <input id="bulletinTitle" name="bulletinTitle" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
	      <tr>
	        <td class="width-15 active text-right">
	          <label>
	                              公告内容：</label>
	        </td>
	        <td class="width-35">
	           <div id="container" style="width: 90%;height: 250px;">
				</div>	
	        </td>
	      </tr>
	    </tbody>
	  </table>
	</form>
	
	<div style="text-align: center;">
	 <button id="addbtn"   class="btn btn-sm btn-primary" onclick="add();return false;"><i class="fa fa-plus"></i>保存</button>
	</div>
</body>

</html>
