'
<!DOCTYPE html>
<%@page import="com.tdcy.sys.util.BusiContextExt"%>
<%@page import="com.tdcy.sys.util.CodeTableUtils"%>
<%@page import="com.tdcy.framework.util.RequestUtils"%>
<%@page import="com.tdcy.sys.util.ParamUtils"%>
<%@page import="com.tdcy.framework.util.SessionUtils"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="biztag"%>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>配置管理中心</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jsp:include page="../../hcommon.jsp"></jsp:include>
	<link rel="stylesheet" type="text/css" href="${path }/resource/js/ztree/css/ztree.css" />
	<link rel="stylesheet" type="text/css" href="${path }/resource/js/ztree/css/zTreeStyle.css" />
	<script src="${path }/resource/js/ztree/js/jquery.ztree.all-3.4.min.js" ></script>
	<script src="${path }/resource/js/ztree/js/jquery.ztree.exhide-3.4.min.js" ></script>
	<%
	    String appName = ParamUtils.getParamValue("application_name");
	%>
</head>
<style type="text/css">
#divFilter{clear: both;vertical-align: middle;text-align: center;}
#tvMenu{height: 400px}
#divTree{height: 480px;overflow-x: auto;overflow-y: scroll;text-align:left;white-space:nowrap;width:250px}
</style>


<body class="gray-bg skin-1">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>
						菜单管理</h5>
					</div>
					<div class="ibox-content">
					    <div>
					    	<div class="row">
								<div class="col-sm-3 col-md-3" >
									  <div id="divMenuTree" style="border:  1px solid #F5FAFC;width: 290px;overflow:auto">
		                                <ul id="tvMenu" class="ztree" >
		                                </ul>
		                              </div>
								</div>
								<div  class="col-sm-9 col-md-9">
									<form  id="menuform"  class="form-horizontal" role="form">  
								       <div class="form-group">   
								         <label class="col-sm-2 control-label">菜单名称:</label> 
								        <div class="col-sm-9">  
								            <input class="form-control" name="menuName"  id="menuName" datatype="*" nullmsg="不能为空"/>
								            <div class="Validform_checktip"></div>
								       </div>  
								       </div>   
								      <div class="form-group">   
								         <label class="col-sm-2 control-label">上级菜单：</label> 
								        <div class="col-sm-9">  
								       	 <div class="form-inline">
								            <input class="form-control" name="menuUpName"  id="menuUpName" readonly="true" style="width: 180px;height: 32px;"/>
											<input name="menuUpId" type="hidden" id="menuUpId" />
											<button class="btn btn-sm btn btn-sm btn-primary" id="btnUpOk" onclick="javascript:showUpMenu();return false;" ><span>选择</span></button>
											<div id="divUpMenu" style="display:none; height: 450px; overflow:auto;" align="center">
												<div style="height: 390px; overflow:auto;border: #d6d6dd 1px solid;;">
													<ul id="tvUp" class="ztree"></ul>
												</div>
											</div>
											</div>
								         </div>  
								       </div>  
								       <div class="form-group">   
								        <label for="lastname" class="col-sm-2 control-label">权限代码:</label>   
								        <div class="col-sm-10">  
								         <input class="form-control" name="permissionCode"  id="permissionCode" />
								         </div>  
								       </div> 
								       <div class="form-group">   
								         <label class="col-sm-2 control-label">菜单类别：</label>
								        <div class="col-sm-10">  
								           <biztag:Comp-CodeSelect defaultValue="1" name="menuType" code="menu_type"/>
								         </div>  
								       </div> 
								       <div class="form-group">   
								          <label class="col-sm-2 control-label">菜单url：</label>
								        <div class="col-sm-10">  
								           <input class="form-control" name="menuUrl"  id="menuUrl" />
								         </div>  
								       </div> 
								       <div class="form-group">   
								          <label class="col-sm-2 control-label">图标：</label>
								        <div class="col-sm-10">  
								            <input class="form-control" name="picName"  id="picName" />
								         </div>  
								       </div> 
								       <div class="form-group">   
								         <label class="col-sm-2 control-label">菜单排序：</label>
								        <div class="col-sm-10">  
								           <input class="form-control" name="menuOrder"  id="menuOrder" />
								         </div>  
								       </div> 
								     </form> 
								     <div style="text-align: center;margin-top: 2px;margin-bottom: 2px;">
										<button class="btn btn-sm btn btn-sm btn-primary" onclick="javascript:doAdd();" ><span>新增</span></button>
										<button class="btn btn-sm btn btn-sm btn-info"  onclick="javascript:doSave();" ><span>保存</span></button>
										<button class="btn btn-sm btn btn-sm btn-danger" onclick="javascript:doDel();" ><span>删除</span></button>
									</div>
     
								</div>
							</div>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</div>
</body>

<script type="text/javascript">
var appName  = "<%=ParamUtils.getParamValue("application_name")%>";
var menudata ;

var curMenuId; //当前选择的菜单id
var _menuTree;//当前菜单树对象
var curMenuInfo ;//当前选择的菜单信息

var NEWID = "-100";
var rootId = "0";

var reloadFlag = false; //加载树后是否需要重新加载curMenuId对应数据

var formValid = null;

function getmenudata(){
	return menudata;
}

$(document).ready(function() {
	formValid = AppKit.formTipValidate("menuform");
	
	resetMenuSize();
	curMenuId = NEWID;
	setEmptyMenu();
	
	getMenuTree();
});

 function resetMenuSize() {
     var winheight = $.browser.opera && $.browser.version > "9.5" ? document.documentElement["clientHeight"] : $(parent.window).height();
     $("#divMenuTree").height(winheight - 275);
 }
 
 function getMenuTree(){
	var url ="${path}/sys/getallmenuAndright";
	AppKit.postJSON(url, null , showMenuTree);
}

function showMenuTree(json){
	if (!AppKit.checkJSONResult(json)) {
        return;
    }
	menudata = json.data;
	buildMenuTree(menudata);
}

function buildMenuTree(menuList){
   try{
        var treedata = new Array();
       // for(var i=0;i<system.length;i++){
        	var rootmenu = {"id":"0","pId":"","name":appName,"data":"", open:true, "iconSkin":"system"};
            treedata.push(rootmenu);
       //}
        
        
    	for(var i in menuList){
    		var j = menuList[i];
    		
    		var tree = {"id":j.menuId,"pId":j.menuUpId,"name":j.menuName,"data":j.menuUrl,"iconSkin":"menu"};
    		if(j.menuType == "2"){
    			tree["font"] = {'color':'blue'};
    		}
    		treedata.push(tree);
    	}
    	//alert(AppKit.tostring(treedata));
	    var setting = {
    			view: {
    				fontCss: getTreeFont,
    				autoCancelSelected: false,
    				selectedMulti: false,
    				dblClickExpand: false,
    				showLine: false
    			},
    			data: {
    				simpleData: {
    					enable: true,
    					rootPId: rootId
    				}
    			},
				callback: {
					onClick: onTreeClick
				}
    	};
	    
	    menudata= treedata;
    
        $.fn.zTree.init($("#tvMenu"), setting, treedata);
        _menuTree = $.fn.zTree.getZTreeObj("tvMenu");
      if(curMenuId != NEWID && reloadFlag){
    	  getMenuInfo(curMenuId);
    	  selectMenu(curMenuId);
      }
    } catch(e){
    	alert(e.message);
    }
}

function getTreeFont(treeId, node) {
	return (!!node.highlight) ? {"background-color":"#ffff96"} : (node.font ? node.font : {"background-color":"transparent"});
}

function onTreeClick(event, treeId, treeNode, clickFlag) {
	if(treeNode.id == curMenuId){
		return;
	}

	if(treeNode.id == rootId){
		setEmptyMenu();
		curMenuId = rootId;
		$('#menuform #menu_up_name').val("");
        $('#menuform #menu_up_id').val("");
        $('#menuform #menu_name').val(appName);
		return ;
    }
	
	if(_menuTree){
		getMenuInfo(treeNode.id);
	}
}

function getMenuInfo(menuid){
	curMenuId = menuid;
	var url ="${path}/sys/getmenuinfo";
	AppKit.postJSON(url, {"menuId": curMenuId }, showMenuInfo);
}
 
 function showMenuInfo(json){
	 //alert(AppKit.tostring(json));
	 if (!AppKit.checkJSONResult(json)) {
            return;
        }
    	
    	var menuinfo = json.data;
        for(var k in menuinfo) {
        	$('#menuform #' + k).val(menuinfo[k]);
        }
        
        //设置下拉菜单的值
        $("#menuform #menuType").combobox('select',menuinfo.menuType);
        $("#menuform #menuVisiable").combobox('select',menuinfo.menuVisiable);
        
        showUpMenuName(menuinfo.menuUpId);
        curMenuInfo = $('#menuform').serializeObject();
 }
 
 function showUpMenuName(menuId){
	 	if(menuId == rootId){
	 		  $('#menuform #menuUpName').val(appName);
		      $('#menuform #menuUpId').val(rootId);
		      return;	
	    }
    	var upMenuName = "";
    	var upMenuId = "";
    	var upMenu = findMenu(menuId);
        if(upMenu){
        	upMenuName = upMenu.name;
        	upMenuId = upMenu.id;
        }
        $('#menuform #menuUpName').val(upMenuName);
        $('#menuform #menuUpId').val(upMenuId);
  }
 
 function findMenu(menuId) {
    var menu = null;
    for (var i in menudata) {
        if (menudata[i].id == menuId) {
            menu = menudata[i];
            break;
        }
    }

    return menu;
 }
 
 function showUpMenu() {
	 if(curMenuId == rootId){
         AppKit.showTipMessage('无法获取上级菜单');
         return;
     }
     
	 url = "${path}/views/jsp/system/getMenu";
	 AppKit.openWindow(url,'选择上级菜单',400,450);
}

function selectMenu(menuId){
	var node = _menuTree.getNodeByParam("id", menuId, null);
	if(node){
		if(node.isParent){
			_menuTree.expandNode(node, true, false, true, false);	
		} else {
			_menuTree.expandNode(node.getParentNode(), true, false, true, false);
		}
		_menuTree.selectNode(node, false);
	}
}

function doSave() {
	var flag = formValid.check();
	if(!flag){
		return;
	}
	
	
	var menuinfo = $("#menuform").serializeObject();
	if(menuinfo == curMenuInfo){
		AppKit.showTipMessage("没有修改不要保存");
		return;
	}
	
	var url = "";
	reloadFlag = true;
	if(curMenuId == NEWID){
		if(AppKit.isnull(menuinfo.menuName) || !AppKit.isvalue(menuinfo.menuName)){
			AppKit.showTipMessage("菜单名称不能为空");
			return;
		}
		url ="${path}/sys/addmenu";
	}else{
		menuinfo["menuId"] = curMenuId;
		url ="${path}/sys/updatemenu";
	}
	
	AppKit.postJSON(url,menuinfo, saveSuccess);
}

function saveSuccess(json){
    if(!AppKit.checkJSONResult(json)){
	    return;
    }
    
    curMenuInfo = $('#menuform').serializeObject();
    if(json.data && json.data.menuId){
	    curMenuId = json.data.menuId;
    }
    
    //重新构建树
    getMenuTree();
    
    AppKit.showTipMessage(json.message);
}

function doAdd() {
	setEmptyMenu();
	
	showUpMenuName(curMenuId);
	
	curMenuId = NEWID;
}

function doDel() {
	if(curMenuId == NEWID ){
		AppKit.showTipMessage("请选择要删除的记录");
		return;
	}
	
	if(curMenuId == "1"){
		AppKit.showTipMessage("不能删除跟菜单");
		return;
	}
	AppKit.showConfirmMessage(function(){
		var url ="${path}/sys/delmenu";
		AppKit.postJSON(url, {"menuId": curMenuId }, delSuccess);
	},"确定要删除?");
}

function delSuccess(json){
    if(!AppKit.checkJSONResult(json)){
	    return;
    }
    
    AppKit.showTipMessage(json.message);
    
    
    var node = _menuTree.getNodeByParam("id", curMenuId, null);
    
    var pnode = node.getParentNode();
    if(pnode){
    	reloadFlag = true;
    	curMenuId = pnode.id;	
    }else{
    	reloadFlag = false;
    	curMenuId = NEWID;	
    }
    
    //重新构建树
    getMenuTree();
   
    
    
    setEmptyMenu();
}

function setEmptyMenu(){
	$("#menuform input").each(function(){
		$(this).val("");
	});
	
	$("#menuform select").each(function(){
		$(this).val("");
	});
	
	$("#menuform textarea").each(function(){
		$(this).val("");
	});
	
	$("#menuOrder").val("1");
	
}

</script>
</body>
</html>
