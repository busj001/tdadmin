<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>${appName}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jsp:include page="../../hcommon.jsp"></jsp:include>
	<script type="text/javascript" src="${jsPath }/jquery.md5.js"></script>
</head>
<script type="text/javascript">
	var formValid = null;
	$(document).ready(function() {
		formValid = AppKit.formTipValidate("searchform");
	});

	function submitUpdatePassword(){
		var flag = formValid.check();
		if(!flag){
			return;
		}
		
		var firstpass= $("#firstpass").val();
		var secondpass= $("#secondpass").val();
		if(firstpass!=secondpass){
			AppKit.showTipMessage("新密码两次输入不相符！");
			return;
			
		}
		
		var info= $('#searchform').serializeObject();
		
		info["firstpass"] =  $.md5(info["firstpass"]);
		info["secondpass"] =  $.md5(info["secondpass"]);
		info["oldpass"] =  $.md5(info["oldpass"]);
		
	
		var url="${path}/sys/updatePassword";
		AppKit.postJSON(url,info, saveSuccess);
	}
	
	function saveSuccess(json){
		if(!AppKit.checkJSONResult(json)){
		    return;
	    }	
		AppKit.showTipMessage(json.message, "", function(){
			AppKit.getInvokeWindow().relogin();
		}, 2000)
	}
</script>
<body class="white-bg" style="margin-left: 5px;margin-top: 5px;" >
	<form id="addform" method="post" class="form-horizontal">
	  <table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
	    <tbody>
	      <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>原密码：</label></td>
	        <td class="width-35">
	          <input type="password" id="oldpass" name="oldpass" class="form-control " datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
	        <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>新密码：</label></td>
	        <td class="width-35">
	          <input type="password" id="firstpass" name="firstpass" class="form-control" datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
	      
	       <tr>
	        <td class="width-15 active text-right">
	          <label>
	            <font color="red">*</font>新密码确认：</label></td>
	        <td class="width-35">
	          <input type="password" id="secondpass" name="secondpass" class="form-control" datatype="*" nullmsg="不能为空"  />
	          <div class="Validform_checktip"></div>
	        </td>
	      </tr>
	      
	    </tbody>
	  </table>
	</form>
	
	<div style="text-align: center;">
	 <button class="btn btn-sm btn-primary" onclick="submitUpdatePassword();return false;"><i class="fa fa-plus"></i>保存</button>
	</div>
</body>
</html>