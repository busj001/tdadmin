<!DOCTYPE html>
<%@page import="com.tdcy.framework.util.DateUtils"%>
<%@page import="com.tdcy.sys.util.ParamUtils"%>
<html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<head>
<link rel="shortcut icon" href="${path}/resource/favicon.ico"> 
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	request.setAttribute("path", path);
	request.setAttribute("basePath", basePath);
	
	
	String appName = ParamUtils.getParamValue("application_name");
	String companyName = ParamUtils.getParamValue("company_name");
	String currentYear = DateUtils.getCurrentYear();
	request.setAttribute("appName", appName);
	request.setAttribute("companyName", companyName);
	request.setAttribute("currentYear", currentYear);
%>
<link href="${path }/resource/css/h/bootstrap.min.css" rel="stylesheet">
<link href="${path }/resource/css/h/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
<link href="${path }/resource/css/h/animate.min.css" rel="stylesheet">
<link href="${path }/resource/css/h/style.min.css" rel="stylesheet">

<script src="${path }/resource/js/jquery-2.0.3.min.js"></script>
<script src="${path }/resource/js/jquery-migrate-1.1.0.js"></script>
<script src="${path }/resource/js/h/bootstrap.min.js"></script>
<script src="${path }/resource/js/h/metisMenu/jquery.metisMenu.js"></script>
<script src="${path }/resource/js/h/slimscroll/jquery.slimscroll.min.js"></script>

<script src="${path }/resource/js/h/pace/pace.min.js"></script>
<script src="${path }/resource/js/layer/layer.js"></script>

<link href="${path }/resource/js/easyui/themes/metro-blue/easyui.css" rel="stylesheet" type="text/css" />
<script src="${path }/resource/js/easyui/jquery.easyui.min.js"></script>
 
 
<script src="${path }/resource/js/h/hplus.min.js"></script>
<script src="${path }/resource/js/h/contabs.min.js"></script>
<script src="${path }/resource/js/hcommon.appkit.js"></script>

<script type="text/javascript" src="http://tajs.qq.com/stats?sId=9051096" charset="UTF-8"></script>
</head>

<body class="fixed-sidebar full-height-layout gray-bg" style="overflow:hidden">
    <div id="wrapper">
    	
        <!--左侧导航开始-->
        <jsp:include page="./tab.jsp"></jsp:include>
        <!--左侧导航结束-->
        
        <!--右侧部分开始-->
        <jsp:include page="./right.jsp"></jsp:include>
        <!--右侧部分结束-->
        
        <!--右侧边栏开始-->
        <jsp:include page="./theme.jsp"></jsp:include>
        <!--右侧边栏结束-->
    </div>
</body>
 <div id='topflag'></div>
 <script type="text/javascript">
 function showWindow(op){
 	return layer.open(op); 
 }

 function getlay(){
 	return 1;
 }
 </script>
</html>
