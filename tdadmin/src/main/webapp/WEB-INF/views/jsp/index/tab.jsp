<%@page import="com.tdcy.sys.service.bean.LoginInfoBean"%>
<%@page import="com.tdcy.sys.util.BusiContextExt"%>
<%@page import="com.tdcy.sys.dao.eo.UserEO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
	LoginInfoBean user = BusiContextExt.getLoginInfo();
	String loginUser = user.getUserEO().getLoginUser();
%>
<script type="text/javascript">
function logout(){
	AppKit.showConfirmMessage(function() {
        var url = '${path}/login/logout';
       location.href = url;
    },'确定退出系统吗？');
}

function showinfo(){
    url = '${path}/views/jsp/index/personInfo';
    AppKit.openWindow(url,  '个人信息',460,350);
}

  
function updatePassword(){
    url = '${path}/views/jsp/index/updatePassword';
    AppKit.openWindow(url,  '修改密码',460,350);
}

</script>
<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="nav-close"><i class="fa fa-times-circle"></i>
    </div>
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <span><img alt="image" class="img-circle" src="${path}/resource/images/profile_small.jpg" width="64px" height="64px"/></span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                       <span class="block m-t-xs"><strong class="font-bold"><%=loginUser %></strong></span>
                        <span class="text-muted text-xs block"><%=user.getRoleName() %><b class="caret"></b></span>
                        </span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="javascript:showinfo()">个人资料</a></li>
                        <li><a  href="javascript:updatePassword()">修改密码</a></li>
                        <li class="divider"></li>
                        <li><a href="javascript:logout()">安全退出</a>
                        </li>
                    </ul>
                </div>
                <div class="logo-element">LX
                </div>
            </li>
            
            <c:forEach items="${menus }" var="menu">
            	<c:choose>
            	 <c:when  test="${empty  menu.children }">
            	 	 <li>
                        <a class="J_menuItem" href="${path}${menu.url}">
                            <i class="fa ${menu.icon}"></i>
                            <span class="nav-label">${menu.name}</span>
                        </a>
                    </li>
            	 </c:when>
            	 <c:otherwise>
            	 	 <li>
                        <a href="#">
                            <i class="fa ${menu.icon}"></i>
                            <span class="nav-label">${menu.name}</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                        	<c:forEach var="submenu" items="${menu.children }">
                        		<c:choose>
	                        		<c:when test="${empty submenu.children}">
	                        			 <li>
	                                        <a class="J_menuItem" href="${path}${submenu.url}">${submenu.name}</a>
	                                    </li>
	                        		</c:when>
	                        		<c:otherwise>
	                        			  <li>
	                                        <a href="#">${submenu.name} <span class="fa arrow"></span></a>
	                                        <ul class="nav nav-third-level">
	                                        	<c:forEach items="${submenu.children }" var="thirdMenu" >
		                                        	<li>
		                                                <a class="J_menuItem" href="${path}${thirdMenu.url}">${thirdMenu.name}</a>
		                                            </li>
	                                        	</c:forEach>
	                                        </ul>
	                                    </li>
	                        		</c:otherwise>
                        		</c:choose>
                        	</c:forEach>
                        </ul>
                    </li>
            	 </c:otherwise>
            	 </c:choose>
            </c:forEach>
        </ul>
    </div>
</nav>