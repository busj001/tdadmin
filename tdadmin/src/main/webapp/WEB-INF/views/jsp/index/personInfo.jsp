<!DOCTYPE html>
<%@page import="com.tdcy.sys.util.BusiContextExt"%>
<%@page import="com.tdcy.sys.service.bean.LoginInfoBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>${appName}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jsp:include page="../../hcommon.jsp"></jsp:include>
	<%
		LoginInfoBean infobean = BusiContextExt.getLoginInfo();
	%>
</head>
<script type="text/javascript">
$(function () {
	
});
</script>
<body class="white-bg" style="margin-left: 5px;margin-top: 5px;" >
	<form id="addform" method="post" class="form-horizontal">
	  <table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
	    <tbody>
	      <tr>
	        <td class="width-15 active text-right">
	          <label>
	            用户类型：</label></td>
	        <td class="width-35">
	          <%=infobean.getUserKindEO().getKindName() %>
	        </td>
	      </tr>
	        <tr>
	        <td class="width-15 active text-right">
	          <label>
	           登录名：</label></td>
	        <td class="width-35">
	          <%=infobean.getUserEO().getLoginUser() %>
	        </td>
	      </tr>
	      <tr>
	        <td class="width-15 active text-right">
	          <label>
	                            角色：</label>
	        </td>
	        <td class="width-35">
	         <%=infobean.getRoleName() %>
	        </td>
	      </tr>
	    </tbody>
	  </table>
	</form>
</body>

</html>