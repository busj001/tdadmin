<!DOCTYPE html>
<%@page import="com.tdcy.sys.util.ParamUtils"%>
<%@page import="com.tdcy.sys.util.BusiContextExt"%>
<html lang="en">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String appName = ParamUtils.getParamValue("application_name");
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<jsp:include page="../../hcommon.jsp"></jsp:include>
<link rel='stylesheet' type="text/css" href='${cssPath }/workbench.css' />
<link rel="stylesheet" href="${cssPath }/workbench.css">
<script type="text/javascript" src="${path }/resource/js/Highcharts/highcharts.js"></script>
<script type="text/javascript" src="${path }/resource/js/Highcharts/modules/exporting.js"></script>
<script type="text/javascript" src="${path }/resource/js/Highcharts/highcharts-3d.js"></script>
<script type="text/javascript" src="${path }/resource/js/Highcharts/modules/data.js"></script>
<script type="text/javascript" src="${path }/resource/js/Highcharts/highcharts-more.js"></script>
<script type="text/javascript">
	
</script>
</head>
<body>
 <div  >
        <div id="hd" style="padding-bottom: 10px;color: #019e97;font-size: 18px;font-weight: bold;padding-left: 20px;text-align: left;">
            您好，欢迎进入<%=appName%>
        </div>
    </div>
 </body>
</html>
