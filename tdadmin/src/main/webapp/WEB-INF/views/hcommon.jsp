<%@page import="com.tdcy.sys.util.ParamUtils"%>
<%@page import="com.tdcy.framework.util.DateUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="biztag"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
request.setAttribute("path", path);
request.setAttribute("basePath", basePath);
String appName = ParamUtils.getParamValue("application_name");
String companyName = ParamUtils.getParamValue("company_name");
String currentYear = DateUtils.getCurrentYear();

request.setAttribute("path", path);
request.setAttribute("basePath", basePath);
request.setAttribute("cssPath", path+"/resource/css");
request.setAttribute("jsPath", path+"/resource/js");
request.setAttribute("imgPath", path+"/resource/images");
request.setAttribute("fontPath", path+"/static/fonts");
request.setAttribute("appName", appName);
request.setAttribute("companyName", companyName);
request.setAttribute("currentYear", currentYear);
%>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE10" />
<meta charset="utf-8" />
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>${appName }</title>

<link rel='stylesheet' type="text/css" href='${cssPath }/base.css' />
<link href="${jsPath}/font-awesome/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<link href="${jsPath}/animate/css/animate.css?v=3.5.2" rel="stylesheet">

<script src="${jsPath }/jquery-2.0.3.min.js" type="text/javascript"></script>
<script  src="${jsPath }/jquery-migrate-1.1.0.js" type="text/javascript"></script>

<!-- bootstrap -->
<link href="${jsPath}/bootstrap/css/bootstrap.min.css?v=v3.2.0" rel="stylesheet">
<script src="${jsPath}/bootstrap/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${jsPath}/peity/jquery.peity.min.js"></script>

<!-- iCheck -->
<link type="text/css" rel="stylesheet" href="${jsPath}/iCheck/skins/all.css">
<script src="${jsPath}/iCheck/icheck.min.js"></script>
<script src="${jsPath}/iCheck/custom.min.js"></script>

<!-- datepicker -->
<link href="${jsPath}/datepicker/datepicker3.css" rel="stylesheet">
<script src="${jsPath}/datepicker/bootstrap-datepicker.js"></script>
 
<!-- jqgrid-->
<link href="${jsPath}/jqgrid/css/ui.jqgrid-bootstrap.css?0820" rel="stylesheet">
<script src="${jsPath}/jqgrid/js/i18n/grid.locale-cn.js?0820"></script>
<script src="${jsPath}/jqgrid/js/jquery.jqGrid.min.js?0820"></script>
<script src="${jsPath}/jqgrid/plugins/jquery.tablednd.js?0820"></script>
       
<!-- Validform -->
<link rel="stylesheet" href="${jsPath}/Validform_v5.3.2/css/style.css" type="text/css" media="all" />
<script type="text/javascript" src="${jsPath}/Validform_v5.3.2/js/Validform_v5.3.2.js"></script>

<!-- 弹框组件 -->
<script src="${jsPath }/layer/layer.js" type="text/javascript" ></script>

<!-- 自定义css和js -->
<link href="${cssPath}/hstyle.css?v=4.1.0" rel="stylesheet">
<script type="text/javascript" src="${jsPath }/hcommon.appkit.js"></script>
<script type="text/javascript">
$(function() {
	AppKit.setAppPath("${path}");
});
</script>

<style>
.ui-th-column{
text-align: center;
}
.layui-layer-title{
color:#fff;
}
</style>
