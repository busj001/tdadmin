<%@ tag pageEncoding="UTF-8" body-content="empty" small-icon=""%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fns" uri="/WEB-INF/tlds/fns.tld" %>

<%@ attribute name="data" required="true"%>  
<%@ attribute name="name" required="true"%>  
<%@ attribute name="defaultValue" required="false"%>  
<%
	String path = request.getContextPath();
%>

<select name="${name}" class="form-control">
    <option value="" >请选择</option>
    <c:if test="${not empty data}">
        <c:forEach items="${fns:jsonToList(data)}" var='dict'>
            <option value='${dict.dataValue}' ${defaultValue==dict.dataValue?'selected':''}>${dict.displayValue}</option>
        </c:forEach>
    </c:if>
</select>
