<%@tag import="com.tdcy.sys.util.CodeTableUtils"%>
<%@ tag pageEncoding="UTF-8" body-content="empty" small-icon=""%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fns" uri="/WEB-INF/tlds/fns.tld" %>

<%@ attribute name="formatterName" required="true"%>  
<%@ attribute name="code" required="true"%>  
<%
	String path = request.getContextPath();
%>
<script type="text/javascript">
var ${code}_json = ${fns:getDictListJson(code)};
function ${formatterName}(val,rec,json){
	return AppKit.getCodeFormatter(val,rec,${code}_json);
}
</script>
