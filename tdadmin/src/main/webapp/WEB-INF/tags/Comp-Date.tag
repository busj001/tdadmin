<%@ tag pageEncoding="UTF-8" body-content="empty" small-icon=""%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fns" uri="/WEB-INF/tlds/fns.tld" %>

<%@ attribute name="id" required="false"%>  
<%@ attribute name="name" required="false"%>  
<%@ attribute name="defaultValue" required="false"%>   
<%
	String path = request.getContextPath();
%>

<div class='input-group date' id='${id }Div'>  
     <input id="${id }" name="${name }" class="form-control"   />  
     <span class="input-group-addon">  
       <span class="glyphicon glyphicon-calendar"></span>  
    </span>  
</div>
<script type="text/javascript">
$('#${id}Div').datepicker({
    keyboardNavigation: false,
  forceParse: false,
  autoclose: true
});
</script>