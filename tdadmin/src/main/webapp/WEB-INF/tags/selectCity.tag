<%@ tag pageEncoding="UTF-8" body-content="empty" small-icon=""%>
<%@ attribute name="target"  required="false"%>
<%@ attribute name="initvalue" %>  
<%@ attribute name="provname" %>  
<%@ attribute name="cityname" %>  
<%
	String path = request.getContextPath();
%>
<script>
var AreaKit=new AreaKitClass("<%=path%>");
AreaKit.address = {};
AreaKit.address['0'] = new Array();
AreaKit.address['1'] = new Array();
AreaKit.address['2'] = new Array();

var initArea ;

$(document).ready(function() {
	AreaKit.initarea(initpage2);
});

function initpage2(){
	var initvalue = "${initvalue}";
	if(initvalue){
		loadProvince2(initvalue);
	}
	var provname = "${provname}";
	if(provname){
		$("#id_provSelect2").attr("name",provname)
	}
	
	var cityname = "${cityname}";
	if(cityname){
		$("#id_citySelect2").attr("name",cityname)
	}
}

function loadProvince2(regionId){
  $("#id_provSelect2").html("");
  $("#id_provSelect2").append("<option value=''>请选择省份</option>");
  var jsonStr = AreaKit.getAddress(regionId,0);
  for(var k in jsonStr) {
	$("#id_provSelect2").append("<option value='"+k+"'>"+jsonStr[k]+"</option>");
  }
  if(regionId.length!=6) {
	$("#id_citySelect2").html("");
    $("#id_citySelect2").append("<option value=''>请选择城市</option>");
  } else {
	 $("#id_provSelect2").val(regionId.substring(0,2)+"0000");
	 loadCity2(regionId);
  }
}

function loadCity2(regionId){
  $("#id_citySelect2").html("");
  $("#id_citySelect2").append("<option value=''>请选择城市</option>");
  if(regionId.length==6) {
	var jsonStr = AreaKit.getAddress(regionId,1);
    for(var k in jsonStr) {
	  $("#id_citySelect2").append("<option value='"+k+"'>"+jsonStr[k]+"</option>");
    }
	var str = regionId.substring(0,2);//四个直辖市
	if(str=="11" || str=="12" || str=="31" || str=="50") {
	   $("#id_citySelect2").val(regionId);
	} else {
	   $("#id_citySelect2").val(regionId.substring(0,4)+"00");
	}
  }
}
</script>
<div>
  <select id="id_provSelect2" name="provSelect2" onChange="loadCity2(this.value);"><option value="">请选择省份</option></select>&nbsp;
  <select id="id_citySelect2" name="citySelect2"><option value="">请选择城市</option></select>&nbsp;
</div>