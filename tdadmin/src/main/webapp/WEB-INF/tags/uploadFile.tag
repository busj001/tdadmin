<%@tag import="com.tdcy.sys.util.CodeTableUtils"%>
<%@ tag pageEncoding="UTF-8" body-content="empty" small-icon=""%>
<%@ attribute name="initvalue" required="false"%>
<%@ attribute name="imageName" required="false"%>  
<%@ attribute name="saveUrl" required="true"%> 
<%@ attribute name="extensions" required="true"%>   
<%
	String path = request.getContextPath();
%>
<!--引入CSS-->
<link rel="stylesheet" type="text/css" href="${path }/resource/js/webuploader/webuploader.css">
<link rel="stylesheet" type="text/css" href="${path }/resource/js/webuploader/webuploader-demo.css">
<!--引入JS-->
<script type="text/javascript" src="${path }/resource/js/webuploader/webuploader.js"></script>
<SCRIPT type="text/javascript">
//文件上传
$(document).ready(function() {
	var filelist = $('#filelist');
	var uploadBtn = $('#ctlBtn');
	var state = 'pending';
	var uploader;

	uploader = WebUploader.create({
		accept : {
		    extensions: '${extensions}'
		},
		auto :true,
		resize : false,
		// swf文件路径
		swf : '${path }/resource/js/webuploader/Uploader.swf',
		// 文件接收服务端。
		server : '${saveUrl}',
		// 选择文件的按钮。可选。
		// 内部根据当前运行是创建，可能是input元素，也可能是flash.
		pick : '#picker'
	});

	// 当有文件添加进来的时候
	uploader.on('fileQueued', function(file) {
		var html = '<div id="' + file.id + '" class="item"><h4 class="info">' + file.name + '</h4><p class="state">等待上传...</p>' + '</div>';
		filelist.append(html);
	});

	// 文件上传过程中创建进度条实时显示。
	uploader.on('uploadProgress',
		function(file, percentage) {
			var fileli = $('#' + file.id);
			var percent = fileli.find('.progress .progress-bar');

			// 避免重复创建
			if (!percent) {
				var percentHtml = '<div id="proccess_'+file.id+'"  class="progress progress-striped active"><div class="progress-bar" role="progressbar" style="width: 0%"></div></div>';
				percent = $(percentHtml).appendTo($('#' + file.id)).find('.progress-bar');
			}

			 $('#' + file.id).find('p.state').text('上传中');

			$("#proccess_"+file.id).css('width', percentage * 100 + '%');
		});

	uploader.on('uploadSuccess', function(file) {
		$('#' + file.id).find('p.state').text('已上传');
	});

	uploader.on('uploadError', function(file) {
		$('#' + file.id).find('p.state').text('上传出错');
	});

	uploader.on('uploadComplete', function(file) {
		$('#' + file.id).find('.progress').fadeOut();
	});

	uploader.on('all', function(type) {
		if (type === 'startUpload') {
			state = 'uploading';
		} else if (type === 'stopUpload') {
			state = 'paused';
		} else if (type === 'uploadFinished') {
			state = 'done';
		}

		if (state === 'uploading') {
			uploadBtn.text('暂停上传');
		} else {
			uploadBtn.text('开始上传');
		}
	});

	uploadBtn.on('click', function() {
		if (state === 'uploading') {
			uploader.stop();
		} else {
			uploader.upload();
		}
	});
});
	</SCRIPT>
<div id="uploader">
	<!--用来存放文件信息-->
	<div id="filelist" class="uploader-list"></div>
	<div class="btns">
		<div id="picker">选择文件</div>
		<button id="ctlBtn" class="easyui-linkbutton">开始上传</button> 
	</div>
</div>
