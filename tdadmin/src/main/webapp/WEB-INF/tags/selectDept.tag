<%@tag import="com.tdcy.biz.utils.CommonUtils"%>
<%@tag import="com.tdcy.sys.util.CodeTableUtils"%>
<%@ tag pageEncoding="UTF-8" body-content="empty" small-icon=""%>
<%@ attribute name="singleSelect" required="true"%>
<%@ attribute name="deptName" required="false"%>  
<%@ attribute name="deptId" required="false"%>
<%@ attribute name="initvalue" required="false"%>
<%
	String path = request.getContextPath();
	String deptjson = CommonUtils.getDeptsJson();
%>
<link rel="stylesheet" type="text/css" href="${path }/resource/js/ztree/css/ztree.css" />
<link rel="stylesheet" type="text/css" href="${path }/resource/js/ztree/css/zTreeStyle.css" />
<script src="${path }/resource/js/ztree/js/jquery.ztree.all-3.4.min.js" ></script>
<script src="${path }/resource/js/ztree/js/jquery.ztree.exhide-3.4.min.js" ></script>
<style>
ul.ztree {margin-top: 10px;border: 1px solid #c3d9e0;background: #fff;width:220px;height:360px;overflow-y:scroll;overflow-x:auto;}
</style>
<SCRIPT type="text/javascript">
	var DeptComp={};
	DeptComp.jsName = "${companyName}";
	DeptComp.singleSelect = "${singleSelect}";
	DeptComp.deptjson = <%=deptjson%>;
	
	$(document).ready(function(){
		var deptName = "${deptName}";
		if(deptName){
			$("#selectDeptName").attr("name",deptName)
		}
		
		var deptId = "${deptId}";
		if(deptId){
			$("#selectDeptId").attr("name",deptId)
		}
		
		DeptComp.showselectDeptTree();
	});
	
	DeptComp.showselectDeptTree = function(){
		DeptComp.buildselectDeptTree(DeptComp.deptjson);
	}
	
	DeptComp.buildselectDeptTree=function(deptList){
		   try{
			   var treedata = new Array();
		       var rootdept = {"id":0,"pId":"","name":DeptComp.jsName,"data":"", open:true, "iconSkin":"system"};
		       treedata.push(rootdept);
		     	for(var i in deptList){
		     		var j = deptList[i];
		     		var tree = {"id":j.deptId,"pId":j.deptUpId,"name":j.deptName,"data":"","iconSkin":"menu"};
		     		treedata.push(tree);
		     	}
		     	
		     	var checkboxsetting = {
		     			check: {
		     				enable: true,
		     				chkboxType: {"Y":"", "N":""}
		     			},
		     			view: {
		     				dblClickExpand: false
		     			},
		     			data: {
		     				simpleData: {
		     					enable: true
		     				}
		     			},
		     			callback: {
		     				beforeClick: DeptComp.beforeClick,
		     				onCheck: DeptComp.onCheck
		     			}
		     		};
		     		var radiosetting = {
		     			check: {
		     				enable: true,
		     				chkStyle: "radio",
		     				radioType: "all"
		     			},
		     			view: {
		     				dblClickExpand: false
		     			},
		     			data: {
		     				simpleData: {
		     					enable: true
		     				}
		     			},
		     			callback: {
		     				onClick: DeptComp.onClick,
		     				onCheck: DeptComp.onCheck
		     			}
		     		};
		    	
		    	
				var setting = null;
				if(DeptComp.singleSelect == "true"){
					setting = radiosetting;
				}else{
					setting = checkboxsetting;
				}
				$.fn.zTree.init($("#selectDeptTree"), setting, treedata);
				
				var initvalue = "${initvalue}";
				if(initvalue){
					DeptComp.setValue(initvalue);
				}
		    } catch(e){
		    	alert(e.message);
		    }
		}
	
	DeptComp.setValue=function(value) {
		var zTree = $.fn.zTree.getZTreeObj("selectDeptTree");
		
		if(value){
			value = value.toString().split(",");
			for(var i=0;i<value.length;i++){
				var node = zTree.getNodeByParam("id", value[i]);
				if(node){
					zTree.checkNode(node, true, null, true);
				}
			}
			
			var nodes = zTree.getCheckedNodes(true);
			var name = "";
			var id = "";
			for (var i=0; i<nodes.length; i++) {
				name += nodes[i].name + ",";
				id += nodes[i].id + ",";
			}
			if (name.length > 0 ) name = name.substring(0, name.length-1);
			if (id.length > 0 ) id = id.substring(0, id.length-1);
			$("#selectDeptName").attr("value", name);
			$("#selectDeptId").attr("value", id);
		}
	}
	
	DeptComp.setEmpty=function() {
		var zTree = $.fn.zTree.getZTreeObj("selectDeptTree");
		zTree.checkAllNodes(false);
		$("#selectDeptName").attr("value", "");
		$("#selectDeptId").attr("value", "");
	}
	
	
	DeptComp.onClick=function(e, treeId, treeNode) {
		var zTree = $.fn.zTree.getZTreeObj("selectDeptTree");
		zTree.checkNode(treeNode, !treeNode.checked, null, true);
		return false;
	}

	DeptComp.beforeClick=function(treeId, treeNode) {
		var zTree = $.fn.zTree.getZTreeObj("selectDeptTree");
		zTree.checkNode(treeNode, !treeNode.checked, null, true);
		return false;
	}
	
	DeptComp.onCheck=function(e, treeId, treeNode) {
		var zTree = $.fn.zTree.getZTreeObj("selectDeptTree");
		
		var nodes = zTree.getCheckedNodes(true);
		var name = "";
		var id = "";
		for (var i=0; i<nodes.length; i++) {
			name += nodes[i].name + ",";
			id += nodes[i].id + ",";
		}
		if (name.length > 0 ) name = name.substring(0, name.length-1);
		if (id.length > 0 ) id = id.substring(0, id.length-1);
		$("#selectDeptName").attr("value", name);
		$("#selectDeptId").attr("value", id);
	}

	DeptComp.showMenu=function() {
		var cityObj = $("#selectDeptName");
		var cityOffset = $("#selectDeptName").offset();
		$("#selectDeptTreeDiv").css({left:cityOffset.left + "px", top:cityOffset.top + cityObj.outerHeight() + "px"}).slideDown("fast");
		$("body").bind("mousedown", DeptComp.onBodyDown);
	}
	DeptComp.hideMenu=function() {
		$("#selectDeptTreeDiv").fadeOut("fast");
		$("body").unbind("mousedown", DeptComp.onBodyDown);
	}
	DeptComp.onBodyDown=function(event) {
		if (!(event.target.id == "deptmenuBtn" || event.target.id == "selectDeptName" || event.target.id == "selectDeptTreeDiv" || $(event.target).parents("#selectDeptTreeDiv").length>0)) {
			DeptComp.hideMenu();
		}
	}
	
	DeptComp.reset=function() {
	$("#selectDeptName").val("");
	$("#selectDeptId").val("");
	}
	</SCRIPT>
<div>
	<div >
 	<ul class="list">
		<input id="selectDeptName" type="text" readonly value="" class="form-control" style="width:180px;height: 32px;float: left;margin-right: 2px;" onclick="DeptComp.showMenu();" ></input>
		<input id="selectDeptId" type="hidden"  ></input>
		<a id="deptmenuBtn" href="#" class="btn btn-sm btn-primary" onclick="DeptComp.showMenu(); return false;">选择</a>
	</ul>
	</div>
</div>

<div id="selectDeptTreeDiv" class="selectDeptTreeDiv" style="display:none; position: absolute; z-index:99999">
	<ul id="selectDeptTree" class="ztree" style="margin-top:0; width:170px; height: 200px;"></ul>
</div>