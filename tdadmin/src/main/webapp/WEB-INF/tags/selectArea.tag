<%@ tag pageEncoding="UTF-8" body-content="empty" small-icon=""%>
<%@ attribute name="target"  required="false"%>
<%@ attribute name="initvalue" %>  
<%@ attribute name="provname" %>  
<%@ attribute name="cityname" %>  
<%@ attribute name="areaname" %>  
<%
	String path = request.getContextPath();
%>
<script type="text/javascript" src="${path }/resource/js/common.area.js"></script>
<script>
var initArea ;
$(document).ready(function() {
	setTimeout(function(){
		initpage1();
	},500)
	
});

function initpage1(){
	var initvalue = "${initvalue}";
	if(initvalue){
		loadProvince1(initvalue);
	}else{
		loadProvince1("");
	}
}

function loadProvince1(regionId){
  $("#id_provSelect1").html("");
  $("#id_provSelect1").append("<option value=''>请选择省份</option>");
  var jsonStr = AreaKit.getAddress(regionId,0);
  for(var k in jsonStr) {
	$("#id_provSelect1").append("<option value='"+k+"'>"+jsonStr[k]+"</option>");
  }
  if(regionId.length!=6) {
	$("#id_citySelect1").html("");
    $("#id_citySelect1").append("<option value=''>请选择城市</option>");
	$("#id_areaSelect1").html("");
    $("#id_areaSelect1").append("<option value=''>请选择区域</option>");
  } else {
	 $("#id_provSelect1").val(regionId.substring(0,2)+"0000");
	 loadCity1(regionId);
  }
}

function loadCity1(regionId){
  $("#id_citySelect1").html("");
  $("#id_citySelect1").append("<option value=''>请选择城市</option>");
  if(regionId.length!=6) {
	$("#id_areaSelect1").html("");
    $("#id_areaSelect1").append("<option value=''>请选择区域</option>");
  } else {
	var jsonStr = AreaKit.getAddress(regionId,1);
    for(var k in jsonStr) {
	  $("#id_citySelect1").append("<option value='"+k+"'>"+jsonStr[k]+"</option>");
    }
	if(regionId.substring(2,6)=="0000") {
	  $("#id_areaSelect1").html("");
      $("#id_areaSelect1").append("<option value=''>请选择区域</option>");
	} else {
	   $("#id_citySelect1").val(regionId.substring(0,4)+"00");
	   loadArea1(regionId);
	}
  }
}

function loadArea1(regionId){
  $("#id_areaSelect1").html("");
  $("#id_areaSelect1").append("<option value=''>请选择区域</option>");
  if(regionId.length==6) {
    var jsonStr = AreaKit.getAddress(regionId,2);
    for(var k in jsonStr) {
	  $("#id_areaSelect1").append("<option value='"+k+"'>"+jsonStr[k]+"</option>");
    }
	if(regionId.substring(4,6)!="00") {$("#id_areaSelect1").val(regionId);}
  }
}
</script>
<div class="form-inline">

  <select id="id_provSelect1" name="${provname}" onChange="loadCity1(this.value);" class="form-control"><option value="">请选择省份</option></select>&nbsp;
  <select id="id_citySelect1" name="${cityname}" onChange="loadArea1(this.value);" class="form-control"><option value="">请选择城市</option></select>&nbsp;
  <select id="id_areaSelect1" name="${areaname}" class="form-control"><option value="">请选择区域</option></select>&nbsp;
</div>