<%@ tag pageEncoding="UTF-8" body-content="empty" small-icon=""%>
<%@ attribute name="singleSelect" required="true"%>  
<%@ attribute name="staffName" required="false"%>  
<%@ attribute name="staffId" required="false"%>
<%@ attribute name="initvalue" required="false"%>
<%
	String path = request.getContextPath();
%>
<link rel="stylesheet" type="text/css" href="${path }/resource/js/ztree/css/ztree.css" />
<link rel="stylesheet" type="text/css" href="${path }/resource/js/ztree/css/zTreeStyle.css" />
<script src="${path }/resource/js/ztree/js/jquery.ztree.all-3.4.min.js" ></script>
<script src="${path }/resource/js/ztree/js/jquery.ztree.exhide-3.4.min.js" ></script>
<style>
ul.ztree {margin-top: 10px;border: 1px solid #c3d9e0;background: #fff;width:220px;height:360px;overflow-y:scroll;overflow-x:auto;}
</style>
<SCRIPT type="text/javascript">
	var DeptEmpComp={};
	DeptEmpComp.jsName = "${companyName}";
	DeptEmpComp.singleSelect = "${singleSelect}";
	

	$(document).ready(function(){
		var staffName = "${staffName}";
		if(staffName){
			$("#selectStaffName").attr("name",staffName)
		}
		
		var staffId = "${staffId}";
		if(staffId){
			$("#selectStaffId").attr("name",staffId)
		}
		
		var url ="${path}/sys/getstafftree";
		AppKit.postJSON(url, null , DeptEmpComp.showDeptEmpTree);
	});
	
	DeptEmpComp.showDeptEmpTree = function(json){
		if (!AppKit.checkJSONResult(json)) {
            return;
        }
		//alert(AppKit.tostring(json.data));
		DeptEmpComp.buildDeptEmpTree(json.data);
	}
	
	DeptEmpComp.buildDeptEmpTree=function(deptList){
		   try{
			   var treedata = new Array();
		       var rootdept = {"id":0,"pId":"","name":DeptEmpComp.jsName,"data":"", open:true, "iconSkin":"system"};
		       treedata.push(rootdept);
		     	for(var i in deptList){
		     		var j = deptList[i];
		     		var tree = {"id":j.staffId,"pId":j.deptId,"name":j.staffName,"data":"","iconSkin":"menu"};
		     		treedata.push(tree);
		     	}
		     	
		     	var checkboxsetting = {
		     			check: {
		     				enable: true,
		     				chkboxType: {"Y":"", "N":""}
		     			},
		     			view: {
		     				dblClickExpand: false
		     			},
		     			data: {
		     				simpleData: {
		     					enable: true
		     				}
		     			},
		     			callback: {
		     				beforeClick: DeptEmpComp.beforeClick,
		     				onCheck: DeptEmpComp.onCheck
		     			}
		     		};
		     		var radiosetting = {
		     			check: {
		     				enable: true,
		     				chkStyle: "radio",
		     				radioType: "all"
		     			},
		     			view: {
		     				dblClickExpand: false
		     			},
		     			data: {
		     				simpleData: {
		     					enable: true
		     				}
		     			},
		     			callback: {
		     				onClick: DeptEmpComp.onClick,
		     				onCheck: DeptEmpComp.onCheck
		     			}
		     		};
		    	
		    	
				var setting = null;
				if(DeptEmpComp.singleSelect == "true"){
					setting = radiosetting;
				}else{
					setting = checkboxsetting;
				}
				$.fn.zTree.init($("#staffTree"), setting, treedata);
				
				var initvalue = "${initvalue}";
				if(initvalue){
					DeptEmpComp.setValue(initvalue);
				}
		    } catch(e){
		    	alert(e.message);
		    }
		}
	
	DeptEmpComp.setValue=function(value) {
		var zTree = $.fn.zTree.getZTreeObj("staffTree");
		
		if(value){
			value = value.toString().split(",");
			for(var i=0;i<value.length;i++){
				var node = zTree.getNodeByParam("id", value[i], null);
				if(node){
					zTree.checkNode(node, true, null, true);
				}
			}
			
			var nodes = zTree.getCheckedNodes(true);
			var name = "";
			var id = "";
			for (var i=0; i<nodes.length; i++) {
				name += nodes[i].name + ",";
				id += nodes[i].id + ",";
			}
			if (name.length > 0 ) name = name.substring(0, name.length-1);
			if (id.length > 0 ) id = id.substring(0, id.length-1);
			$("#selectStaffName").attr("value", name);
			$("#selectStaffId").attr("value", id);
		}
	}
	
	DeptEmpComp.onClick=function(e, treeId, treeNode) {
		var zTree = $.fn.zTree.getZTreeObj("staffTree");
		zTree.checkNode(treeNode, !treeNode.checked, null, true);
		return false;
	}

	DeptEmpComp.beforeClick=function(treeId, treeNode) {
		var zTree = $.fn.zTree.getZTreeObj("staffTree");
		zTree.checkNode(treeNode, !treeNode.checked, null, true);
		return false;
	}
	
	DeptEmpComp.onCheck=function(e, treeId, treeNode) {
		var zTree = $.fn.zTree.getZTreeObj("staffTree");
		var nodes = zTree.getCheckedNodes(true);
		var name = "";
		var id = "";
		for (var i=0; i<nodes.length; i++) {
			name += nodes[i].name + ",";
			id += nodes[i].id + ",";
		}
		if (name.length > 0 ) name = name.substring(0, name.length-1);
		if (id.length > 0 ) id = id.substring(0, id.length-1);
		$("#selectStaffName").attr("value", name);
		$("#selectStaffId").attr("value", id);
	}

	DeptEmpComp.showMenu=function() {
		var cityObj = $("#staffName");
		var cityOffset = $("#staffName").offset();
		$("#staffTreeDiv").css({left:cityOffset.left + "px", top:cityOffset.top + cityObj.outerHeight() + "px"}).slideDown("fast");
		$("body").bind("mousedown", DeptEmpComp.onBodyDown);
	}
	DeptEmpComp.hideMenu=function() {
		$("#staffTreeDiv").fadeOut("fast");
		$("body").unbind("mousedown", DeptEmpComp.onBodyDown);
	}
	DeptEmpComp.onBodyDown=function(event) {
		if (!(event.target.id == "staffmenuBtn" || event.target.id == "staffName" || event.target.id == "staffTreeDiv" || $(event.target).parents("#staffTreeDiv").length>0)) {
			DeptEmpComp.hideMenu();
		}
	}
	</SCRIPT>
<div>
	<div >
 	<ul class="list">
		<input id="selectStaffName" name ="selectStaffName" type="text" readonly value="" style="width:180px;" onclick="DeptEmpComp.showMenu();" ></input>
		<input id="selectStaffId" name = "selectStaffId" type="hidden"  ></input>
		<a id="staffmenuBtn" href="#" class="easyui-linkbutton" onclick="DeptEmpComp.showMenu(); return false;">选择</a>
	</ul>
	</div>
</div>

<div id="staffTreeDiv" class="staffTreeDiv" style="display:none; position: absolute;z-index:99999">
	<ul id="staffTree" class="ztree" style="margin-top:0; width:170px; height: 200px;"></ul>
</div>