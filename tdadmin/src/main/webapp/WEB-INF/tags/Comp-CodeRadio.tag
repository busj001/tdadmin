<%@ tag pageEncoding="UTF-8" body-content="empty" small-icon=""%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fns" uri="/WEB-INF/tlds/fns.tld" %>

<%@ attribute name="code" required="true"%>  
<%@ attribute name="name" required="true"%>  
<%@ attribute name="defaultValue" required="false"%>  

<%
	String path = request.getContextPath();
%>

<c:if test="${not empty code}">
    <c:forEach items="${fns:getCodeList(code)}" var='dict'>
        <input type="radio" name="${name }"  title="${dict.displayValue }"  value="${dict.dataValue }" ${defaultValue==dict.dataValue?'selected':''}> ${dict.displayValue }
    </c:forEach>
</c:if>
