<%@ tag pageEncoding="UTF-8" body-content="empty" small-icon=""%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fns" uri="/WEB-INF/tlds/fns.tld" %>

<%@ attribute name="code" required="true"%>  
<%@ attribute name="name" required="true"%>  
<%@ attribute name="defaultValue" required="false"%>  
<%@ attribute name="onselect" required="false"%>  
<%
	String path = request.getContextPath();
%>
<select  id="${name}" name="${name}" class="form-control" onchange="${onselect }">
    <option value="" >请选择</option>
    <c:if test="${not empty code}">
        <c:forEach items="${fns:getCodeList(code)}" var='dict'>
            <option value='${dict.dataValue}' ${defaultValue==dict.dataValue?'selected':''}>${dict.displayValue}</option>
        </c:forEach>
    </c:if>
</select>
