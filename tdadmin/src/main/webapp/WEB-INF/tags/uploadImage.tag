<%@tag import="com.tdcy.sys.util.CodeTableUtils"%>
<%@ tag pageEncoding="UTF-8" body-content="empty" small-icon=""%>
<%@ attribute name="initvalue" required="false"%>
<%@ attribute name="imageName" required="false"%>
<%@ attribute name="inputName" required="true"%>  
<%@ attribute name="saveUrl" required="true"%>
<%
	String path = request.getContextPath();
%>
<!--引入CSS-->
<link rel="stylesheet" type="text/css" href="${path }/resource/js/webuploader/webuploader.css">
<link rel="stylesheet" type="text/css" href="${path }/resource/js/webuploader/webuploader-demo.css">
<!--引入JS-->
<script type="text/javascript" src="${path }/resource/js/webuploader/webuploader.js"></script>
<SCRIPT type="text/javascript">
	var UploadImageComp={};
	UploadImageComp.imageName = "${imageName}";
	UploadImageComp.state = 'pending';
     // 优化retina, 在retina下这个值是2
    UploadImageComp.ratio = window.devicePixelRatio || 1;
     // 缩略图大小
    UploadImageComp.thumbnailWidth = 100 * UploadImageComp.ratio;
    UploadImageComp.thumbnailHeight = 100 * UploadImageComp.ratio;
     
    // 初始化Web Uploader
    UploadImageComp.uploader = null;
	//图片上传demo
	jQuery(function() {
		UploadImageComp.uploader = WebUploader.create({
		     // 自动上传。
		     auto:true,
		     // swf文件路径
		     swf: '${path }/resource/js/webuploader/Uploader.swf',
		     // 文件接收服务端。
		     server: '${saveUrl}',
		     // 内部根据当前运行是创建，可能是input元素，也可能是flash.
		     pick: '#filePicker',
		     // 只允许选择文件，可选。
		     accept: {
		         title: 'Images',
		         extensions: 'gif,jpg,jpeg,bmp,png',
		         mimeTypes: 'image/jpg,image/jpeg,image/png'
		     }
		 });
	    
		 // 当有文件添加进来的时候
		 UploadImageComp.uploader.on('fileQueued', function( file ) {
			 UploadImageComp.insertLocalImage(file);
		 });

		 // 文件上传过程中创建进度条实时显示。
		 UploadImageComp.uploader.on('uploadProgress', function( file, percentage ) {
		     var percent = $('#'+file.id ).find('.progress span');
		     $("#tip_"+file.id).html('上传中');
		     // 避免重复创建
		     if (!percent ) {
		         percent = $('<p id="proccess_'+file.id+'" class="progress"><span></span></p>').appendTo($("#"+file.id)).find('span');
		     }
		
		     $("#proccess_"+file.id).css('width', percentage * 100 + '%');
		 });
		 
		 UploadImageComp.uploader.on( 'uploadSuccess', function(file ,res) {
	        $('#tip_'+file.id ).html('上传成功[<a style="color:blue" href="javascript:UploadImageComp.delimgid(\''+file.id+'\',\''+res.data.fileid+'\')">删除</a>]');
	        UploadImageComp.insertimgid(res.data.fileid);
	     });

		 UploadImageComp.uploader.on( 'uploadError', function( file ) {
			 $('#tip_'+file.id ).html('上传出错');
	    });

		 UploadImageComp.uploader.on( 'uploadComplete', function( file ) {
	        $('#'+file.id ).find('.progress').fadeOut();
	    });

		 UploadImageComp.uploader.on( 'all', function( type ) {
	        if ( type == 'startUpload' ) {
	        	UploadImageComp.state = 'uploading';
	        } else if ( type == 'stopUpload' ) {
	        	UploadImageComp.state = 'paused';
	        } else if ( type == 'uploadFinished' ) {
	        	UploadImageComp.state = 'done';
	        }

	        if ( UploadImageComp.state == 'uploading' ) {
	            $("#imgctlBtn").text('暂停上传');
	        } else {
	        	 $("#imgctlBtn").text('开始上传');
	        }
	    });

		 if(UploadImageComp.imageName){
	    	$("#mtImgPic").attr("name",UploadImageComp.imageName);
	    }
	});
	
	UploadImageComp.insertLocalImage=function(file){
		  var html = '<div id="div_' + file.id + '" class="file-item thumbnail">' +
	          '<img id="img_'+file.id+'">' +
	          '<div class="info">' + file.name + '</div>' +
	          '<p id="tip_'+file.id+'" class="state">等待上传...</p>' +
	      '</div>';
	     $("#fileList").append(html);
	
	     // 创建缩略图
	     UploadImageComp.uploader.makeThumb( file, function( error, src ) {
	         if (error) {
	             $("#img_"+file.id).replaceWith('<span>不能预览</span>');
	             return;
	         }
			$("#img_"+file.id).attr('src', src );
	     }, UploadImageComp.thumbnailWidth, UploadImageComp.thumbnailHeight );
	}
	
	UploadImageComp.initValue=function(fileids){
		if(fileids){
			var fileida = fileids.split(",");
			for(var i=0;i<fileida.length;i++){
				var fid = fileida[i];
				var url = "${path}/file/downloadFile?fileid="+fid;
				 var html = '<div id="div_' + fid + '" class="file-item thumbnail">' +
			          '<img width="'+UploadImageComp.thumbnailWidth+'" height="'+UploadImageComp.thumbnailHeight+'" id="img_'+fid+'" src="'+url+'">' +
			          '<p class="state">已上传[<a style="color:blue" href="javascript:UploadImageComp.delimgid(\''+fid+'\',\''+fid+'\',true)">删除</a>]</p>' +
			      '</div>';
			     $("#fileList").append(html);
			}
		}
		
		$("#mtImgPic").val(fileids);
	}

	UploadImageComp.insertimgid=function(fileid){
		 var mtPic = $("#mtImgPic").val();
	     var newPicId = AppKit.addImageIdStr(mtPic,fileid);
	     $("#mtImgPic").val(newPicId);
	}
	
	UploadImageComp.delimgid=function(divid,fileid,delserver){
		$("#div_"+divid).remove();
		
		var mtPic = $("#mtImgPic").val();
		var newPicId = AppKit.removeImageIdStr(mtPic,fileid);
		$("#mtImgPic").val(newPicId);
		
		if(delserver){
			var url = "${path}/file/delFile";
			AppKit.postJSON(url,{"fileid":fileid,"deleteFile":"false"}, function(json){});
		}
	}
	</SCRIPT>
<div id="uploader-demo">
    <!--用来存放item-->
    <div id="fileList" class="uploader-list"></div>
    	
     <div class="btns">
	    <div id="filePicker">选择图片</div>
	    <input id="mtImgPic"  name="${inputName }" type="hidden"/>
    </div>
</div>
