package excel;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

public class GenIconCss {

	public static void main(String[] args) throws IOException {
		File f = new File("E:\\workbench2\\lxcloud\\src\\main\\webapp\\resource\\js\\easyui\\themes\\icons");
		File[] fs = f.listFiles();

		File cssFile = new File("E:\\workbench2\\lxcloud\\src\\main\\webapp\\resource\\js\\easyui\\themes\\icon.css");
		FileUtils.deleteQuietly(cssFile);
		StringBuffer sb = new StringBuffer();

		for (File ff : fs) {
			String fname = ff.getName();
			String kname = fname.substring(0, fname.lastIndexOf("."));

			sb.append(".icon-" + kname + "{\r\n");
			sb.append("background:url('icons/" + fname + "') no-repeat center center;\r\n");
			sb.append("}\r\n");
			sb.append("\r\n");

		}
		FileUtils.writeStringToFile(cssFile, sb.toString());
	}
}
