import java.util.Map;

import org.junit.Test;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import redis.clients.jedis.Jedis;

import com.tdcy.framework.util.ContextUtils;

@org.junit.runner.RunWith(SpringJUnit4ClassRunner.class)
@org.springframework.test.context.ContextConfiguration(locations = "classpath:application.xml")
public class Test1 {

	@Test
	public void test21() throws Exception {
		JedisConnectionFactory cf = (JedisConnectionFactory) ContextUtils.getApplicationContext().getBean("connectionFactory");
		StringRedisTemplate redisTemplate = ContextUtils.getApplicationContext().getBean(StringRedisTemplate.class);
		RedisSerializer<String> serializer = redisTemplate.getStringSerializer();
	}

	public static void main(String[] args) {
		Jedis jedis = new Jedis("59.110.138.224", 6379);
		Map<String, String> m = jedis.hgetAll("F0FE6B27318A");
		System.out.println(m);

		String s = jedis.get("F0FE6B27318AHT");
		System.out.println(s);
	}

}
